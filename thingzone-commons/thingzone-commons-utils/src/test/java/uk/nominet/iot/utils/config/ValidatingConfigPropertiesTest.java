/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.utils.config;


import org.hamcrest.Matchers;
import org.junit.Test;

import uk.nominet.iot.utils.error.FatalError;

import java.io.IOException;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class ValidatingConfigPropertiesTest {

    public class ConcreteConfig extends ValidatingConfigProperties {
        ConcreteConfig(Properties properties) {
            super(properties);
        }
        protected String[] getIntegerPropertyNames() { return new String[] { "testInt1", "testInt2" }; }
        protected String[] getStringPropertyNames() { return new String[] { "testString1", "testString2" }; }
        protected String[] getBooleanPropertyNames() { return new String[] { "testBoolean1", "testBoolean2" }; }
        String[] allowedEmptyStrings;
        protected String[] getAllowedEmptyStringProperties() { return allowedEmptyStrings; }
        protected boolean isMaskedProperty(String propertyName) { return propertyName.equals ("maskMe"); }

        protected Properties customValidation(Properties props) {
            if (props.containsKey("custom")) {
                props.remove("custom");
            }
            return props;
        }

        protected Properties preValidate(Properties props) {
            if (props.containsKey("testString1allowedEmpty")) {
                allowedEmptyStrings = new String[] {"testString1"};
                props.remove("testString1allowedEmpty");
            } else {
                allowedEmptyStrings = new String[] {};
            }
            return props;
        }

    }

    final static Properties dummyProps = new Properties();

    static {
        dummyProps.setProperty("testInt1", "42");
        dummyProps.setProperty("testInt2", "43");
        dummyProps.setProperty("testString1", "foo");
        dummyProps.setProperty("testString2", "bar");
        dummyProps.setProperty("testBoolean1", "Y");
        dummyProps.setProperty("testBoolean2", "N");
    }

    @Test
    public void testValidPropertiesStored() throws IOException {
        Properties props = (Properties) dummyProps.clone();
        @SuppressWarnings("unused") ConcreteConfig config = new ConcreteConfig(props);
    }

    @Test
    public void testMissingStringPropertyThrows() throws IOException {
        Properties props = (Properties) dummyProps.clone();
        props.remove("testString1");
        try {
            @SuppressWarnings("unused") ConcreteConfig config = new ConcreteConfig(props);
            fail(); // exception not thrown
        } catch (FatalError e) {
            assertThat(e.getMessage(), Matchers.containsString("Config file missing parameter"));
            assertThat(e.getMessage(), Matchers.containsString("testString1"));
        }
    }

    @Test
    public void testMissingIntPropertyThrows() throws IOException {
        Properties props = (Properties) dummyProps.clone();
        props.remove("testInt1");
        try {
            @SuppressWarnings("unused") ConcreteConfig config = new ConcreteConfig(props);
            fail(); // exception not thrown
        } catch (FatalError e) {
            assertThat(e.getMessage(), Matchers.containsString("Config file missing parameter"));
            assertThat(e.getMessage(), Matchers.containsString("testInt1"));
        }
    }

    @Test
    public void testMissingBooleanPropertyThrows() throws IOException {
        Properties props = (Properties) dummyProps.clone();
        props.remove("testBoolean1");
        try {
            @SuppressWarnings("unused") ConcreteConfig config = new ConcreteConfig(props);
            fail(); // exception not thrown
        } catch (FatalError e) {
            assertThat(e.getMessage(), Matchers.containsString("Config file missing parameter"));
            assertThat(e.getMessage(), Matchers.containsString("testBoolean1"));
        }
    }

    @Test
    public void testBogusIntPropertyThrows() throws IOException {
        Properties props = (Properties) dummyProps.clone();
        props.setProperty("testInt1", "notanumber");
        try {
            @SuppressWarnings("unused") ConcreteConfig config = new ConcreteConfig(props);
            fail(); // exception not thrown
        } catch (FatalError e) {
            assertThat(e.getMessage(), Matchers.containsString("must be an integer"));
            assertThat(e.getMessage(), Matchers.containsString("testInt1"));
        }
    }

    @Test
    public void testBogusBooleanPropertyThrows() throws IOException {
        Properties props = (Properties) dummyProps.clone();
        props.setProperty("testBoolean1", "notaboolean");
        try {
            @SuppressWarnings("unused") ConcreteConfig config = new ConcreteConfig(props);
            fail(); // exception not thrown
        } catch (FatalError e) {
            assertThat(e.getMessage(), Matchers.containsString("must be \"Y\" or \"N\""));
            assertThat(e.getMessage(), Matchers.containsString("testBoolean1"));
        }
    }

    @Test
    public void testAllowedEmptyProperties() throws IOException {
        Properties props = (Properties) dummyProps.clone();
        props.setProperty("testString1allowedEmpty", "dummy");  // trigger to allow testString1 empty
        props.setProperty("testString1", "");
        ConcreteConfig config = new ConcreteConfig(props);
        assertEquals(0, config.getExtraneousProperties().size());
    }


    @Test
    public void testExtraneousProperties() throws IOException {
        Properties props = (Properties) dummyProps.clone();
        props.setProperty("extraneous", "value");
        ConcreteConfig config = new ConcreteConfig(props);
        assertEquals(1, config.getExtraneousProperties().size());
        assertEquals("value", config.getExtraneousProperties().getProperty("extraneous"));
    }

    @Test
    public void testCustomValidation() throws IOException {
        Properties props = (Properties) dummyProps.clone();
        props.setProperty("custom", "value");
        ConcreteConfig config = new ConcreteConfig(props);
        // (custom property should have been swallowed)
        assertEquals(0, config.getExtraneousProperties().size());
    }

}
