/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.utils.dns;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.junit.Before;
import org.junit.Test;

public class IpAddressValidatorTest {
    private Inet4Address sampleIpv4;
    private Inet6Address sampleIpv6;

    @Before
    public void setUp() throws UnknownHostException {
        sampleIpv4 = (Inet4Address) InetAddress.getByName("1.2.3.4");
        sampleIpv6 = (Inet6Address) InetAddress.getByName("dead::d00d");
    }


    @Test
    public void testNonsenseIp(){
        assertFalse (IpAddressValidator.isValidIPv4("nonsense"));
        assertFalse (IpAddressValidator.isValidIPv6("nonsense"));
    }

    @Test
    public void testMixedIp() {
        assertFalse (IpAddressValidator.isValidIPv4("100:1.2.3.4"));
        assertFalse (IpAddressValidator.isValidIPv6("100:1.2.3.4"));
    }

    @Test
    public void testBadIp() {
        assertFalse (IpAddressValidator.isValidIPv4("1.2.3"));
        assertFalse (IpAddressValidator.isValidIPv6("1.2.3"));
        assertFalse (IpAddressValidator.isValidIPv4("dead::head"));
        assertFalse (IpAddressValidator.isValidIPv6("dead::head"));
        
//        assertFalse (IpAddressValidator.isValidIPv6("2001:db8::1"));	// doc prefix
        assertFalse (IpAddressValidator.isValidIPv6("::192.168.0.1"));	// compat
        assertFalse (IpAddressValidator.isValidIPv6("::ffff:192.168.0.1")); //mapped
    }

    @Test
    public void testGoodIp() {
        assertTrue (IpAddressValidator.isValidIPv4("0.0.0.0"));
        assertTrue (IpAddressValidator.isValidIPv4("1.2.3.4"));
        assertTrue (IpAddressValidator.isValidIPv4("255.255.255.255"));
        assertTrue (IpAddressValidator.isValidIPv6("::"));
        assertTrue (IpAddressValidator.isValidIPv6("1:2:3:4:5:6:7:8"));
        assertTrue (IpAddressValidator.isValidIPv6("::1"));
        assertTrue (IpAddressValidator.isValidIPv6("1::"));
        assertTrue (IpAddressValidator.isValidIPv4(sampleIpv4.getHostAddress()));
        assertTrue (IpAddressValidator.isValidIPv6(sampleIpv6.getHostAddress()));
    }

}
