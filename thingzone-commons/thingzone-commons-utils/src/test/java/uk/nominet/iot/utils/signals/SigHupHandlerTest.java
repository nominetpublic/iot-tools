/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.utils.signals;

import org.junit.Ignore;
import org.junit.Test;
import sun.misc.Signal;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;


public class SigHupHandlerTest implements SignalReceiver {

	static boolean sigHupReceived = false;
	public void handleSigHup() {
		sigHupReceived = true;
	}

	final static int TIMEOUT = 1000; // ms
	final static int SPIN_INTERVAL = 10; // ms

	@Ignore // doesn't play nicely with TeamCity
	@Test
	public void handlerIsCalled() throws InterruptedException {
		SigHupHandler.getInstance().register(this);
		Signal.raise(new Signal("HUP"));

		waitForCompletion();
		assertEquals(true, sigHupReceived);
	}

	void waitForCompletion() throws InterruptedException {
		long started = new Date().getTime();
		while (!sigHupReceived && new Date().getTime()-started < TIMEOUT) {
			System.out.println("waiting for hup");
			Thread.sleep(SPIN_INTERVAL);
		}
	}

	@Ignore // doesn't play nicely with TeamCity
	@Test
	public void doubleRegistrationIsOk() throws InterruptedException {
		SigHupHandler.getInstance().register(this);
		SigHupHandler.getInstance().register(this);

		Signal.raise(new Signal("HUP"));

		waitForCompletion();
		assertEquals(true, sigHupReceived);
	}


	// (for running manually - this test will get interrupted by default handler)
	@Ignore
	@Test
	public void handlerNotCalledWhenNotregistered() throws InterruptedException {
		Signal.raise(new Signal("HUP"));
		fail();
	}

	// (for running manually - this test will get interrupted by default handler)
	@Ignore
	@Test
	public void handlerNotCalledWhenUnregistered() throws InterruptedException {
		SigHupHandler.getInstance().register(this);
		SigHupHandler.getInstance().unregister();
		Signal.raise(new Signal("HUP"));
		fail();
	}

}
