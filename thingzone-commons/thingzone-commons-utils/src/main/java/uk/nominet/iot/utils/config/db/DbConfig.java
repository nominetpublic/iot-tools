/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.utils.config.db;

import java.util.*;

public interface DbConfig {
	/* gets the raw properties, as derived from the config file */
    Properties getProperties();

    /* gets the properties, as javax.persistence.jdbc.xxx keys */
    Properties getJavaxPersistenceJdbcProperties();

    // JDBC URL for Oracle
    String getOracleJdbcUrl();

    // JDBC URL for MySQL
    String getMysqlJdbcUrl();

    // raw values from config file 
    List<String> getHostnames();
    int getPort();
    String getSid();
    String getUsername();
    String getPassword();
}
