/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.utils.datasource;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class GenericDataSource<TI, TO> {
    private Logger logger = LoggerFactory.getLogger(GenericDataSource.class);
    protected ReadWriteLock lock = new ReentrantReadWriteLock();
    protected long secsCheckInterval = 600;
    protected long secsReloadInterval = 3600*24*30;   
    protected long lastCheckTime;
    protected long lastReloadTime;
    protected Boolean initRequired = true;
    
    private void lazyLoad() {
        if (initRequired) {
            lock.writeLock().lock();
            try {
                if (initRequired) {
                    logger.info("Lazy loading "+this.getClass().getName());
                    init();
                    initRequired = false;
                }
            }
            finally {
                lock.writeLock().unlock();
            }
        }
    }
    
    protected void init() {
        lastCheckTime = System.currentTimeMillis()/1000;
        lastReloadTime = lastCheckTime - secsReloadInterval - 1;
    }
    
    public void setCheckInterval(int secsCheckInterval) {
        lock.writeLock().lock();
        try {
            this.secsCheckInterval = secsCheckInterval;
        }
        finally {
            lock.writeLock().unlock();
        }
    }
    
    public void setReloadInterval(int secsReloadInterval) {
        lock.writeLock().lock();
        try {
            this.secsReloadInterval = secsReloadInterval;
        }
        finally {
            lock.writeLock().unlock();
        }
    }
    
    protected abstract TO getOutputInternal(TI in); 
    protected abstract void parse();
    protected abstract void tryLoad() throws Exception;
    
    public TO getOutput(TI in) {
        lazyLoad();
        lock.readLock().lock();
        try {
            Long now = System.currentTimeMillis()/1000;
            //check if thread should attempt reload the file
            if (now-lastCheckTime > secsCheckInterval) {
                lock.readLock().unlock();
                lock.writeLock().lock();
                try {
                    try {
                        tryLoad();
                    }
                    catch (Exception e) {
                        logger.error(e.toString());
                    }
                    lastCheckTime = now;
                }
                finally {
                    lock.writeLock().unlock();
                    lock.readLock().lock();
                }
            }
            return getOutputInternal(in);
        }
        finally {
            lock.readLock().unlock();
        }
    }
}
