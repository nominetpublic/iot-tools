/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.utils.jmx;

import java.lang.management.ManagementFactory;

import javax.management.MBeanServer;

import org.eclipse.jetty.jmx.MBeanContainer;

public class JmxServer {
	// singleton: only created if user wants to use JMX
	static JmxServer instance = null;
	static JmxServer getInstance() {
		return (instance != null) ? instance : getOrCreateInstance();
	}
	static synchronized JmxServer getOrCreateInstance() {
		if(instance == null) {
			instance = new JmxServer();
		}
		return instance;
	}

	final MBeanServer mbs;
	final MBeanContainer mbc;

	JmxServer() {
		mbs = ManagementFactory.getPlatformMBeanServer();
		mbc = new MBeanContainer(mbs);
	}
	
	public static MBeanServer getMBeanServer() {
		return getInstance().mbs;
	}
	
	public static MBeanContainer getMBeanContainer() {
		return getInstance().mbc;
	}
}
