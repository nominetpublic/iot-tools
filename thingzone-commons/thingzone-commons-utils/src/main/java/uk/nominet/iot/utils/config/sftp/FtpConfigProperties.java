/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.utils.config.sftp;

import java.util.*;

import uk.nominet.iot.utils.config.ValidatingConfigProperties;


public class FtpConfigProperties extends ValidatingConfigProperties implements FtpConfig {

    FtpConfigProperties(Properties properties) {
        // superclass will validate, calling our overridden methods
        super(properties);
    }

    private final static String[] INTEGER_PROPERTY_NAMES = new String[] {
            "Port",
    };
    private final static String[] STRING_PROPERTY_NAMES = new String[] {
            "Hostname",
            "Username",
            "Password",
    };

    @Override
    protected String[] getIntegerPropertyNames() {
        return INTEGER_PROPERTY_NAMES;
    }

    @Override
    protected String[] getStringPropertyNames() {
        return STRING_PROPERTY_NAMES;
    }

    /**
     * masks things like passwords when logging
     * @param propertyName
     * @return
     */
    @Override
    protected boolean isMaskedProperty(String propertyName) {
        return propertyName.equals ("Password");
    }
    

    @Override
	public int getPort() {
        return intProps.get("Port");
    }
    @Override
	public String getHostname() {
        return stringProps.get("Hostname");
    }
    @Override
	public String getUsername() {
        return stringProps.get("Username");
    }
    @Override
	public String getPassword() {
        return stringProps.get("Password");
    }

}
