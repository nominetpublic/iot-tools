/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.utils.config;

import java.io.*;
import java.util.Properties;

import uk.nominet.iot.utils.error.FatalError;



/**
 * Class to read and populate a Properties instance
 *  with error-checking
 */
public class ReadOnlyPropertyFile {
    private final String pathName;
    private final String fileName;

    /**
     * @param name Name of property file ("path/to/file/basename")
     */
    public ReadOnlyPropertyFile(String name) {
        // split into path & file
        File f = new File (name);

        String pathName = f.getParent();
        if (pathName == null) {
            pathName = ".";    // handle file in current directory
        }
        this.pathName = pathName;
        this.fileName = f.getName();
    }


    /**
     * Read properties from file
     * throws FatalError if unsuccessful
     *
     * @return Properties instance
     */
    public Properties readFromFile() {
        Properties props = new Properties();
        try {
            FileInputStream fIn = new FileInputStream(getName()); 
            try {
                props.load (fIn);
            } finally {
                fIn.close();
            }
        } catch (FileNotFoundException e) {
            throw new FatalError("unable to open file "+ quote(getName()));
        } catch (IOException e) {
            throw new FatalError("unable to read file "+ quote(getName()));
        }
        return props;
    }


    public String getPathName() {
        return pathName;
    }

    public String getFileName() {
        return fileName;
    }

    public String getName() {
        return pathName + File.separator + fileName;
    }

    public void checkPathExists() {
        File path = new File(pathName);
        if (!path.exists()) {
            throw new FatalError("directory " + quote(path.getName()) + " does not exist");
        }
        if (!path.isDirectory()) {
            throw new FatalError("not a directory: " + quote(path.getName()));
        }
    }

    public void checkFileExists() {
        File file = new File(getName());
        if (!file.exists()) {
            throw new FatalError("file " + quote(file.getName()) + " does not exist");
        }
        if (!file.isFile()) {
            throw new FatalError("not a file: " + quote(file.getName()));
        }
    }

    public void checkFileReadable() {
        File file = new File(getName());
        if (!file.canRead()) {
            throw new FatalError("file " + quote(file.getName()) + " is not readable");
        }
    }

    String quote(String s) {
        return "\"" + s + "\"";
    }
}
