/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.utils.config.db;

import java.util.*;

import uk.nominet.iot.utils.config.ValidatingConfigProperties;
import uk.nominet.iot.utils.error.FatalError;


public class DbConfigProperties extends ValidatingConfigProperties implements DbConfig {
	private List<String> hostnames;

    DbConfigProperties(Properties properties) {
        // superclass will validate, calling our overridden methods
        super(properties);
    }

    private final static String[] INTEGER_PROPERTY_NAMES = new String[] {
            "Port",
    };
    private final static String[] STRING_PROPERTY_NAMES = new String[] {
            "SID",
            "Username",
            "Password",
    };

    @Override
    protected Properties customValidation(Properties props) {
        hostnames = new ArrayList<String>();

        // split up the list of hostnames
        if (!props.containsKey("Hostnames")) {
            throw new FatalError("Config file missing parameter \"Hostnames\"");
        }
        String value = props.getProperty("Hostnames");
        hostnames.addAll(Arrays.asList(value.split (",\\s*")));
        props.remove("Hostnames");

        return props;
    }

    
    @Override
    protected String[] getIntegerPropertyNames() {
        return INTEGER_PROPERTY_NAMES;
    }

    @Override
    protected String[] getStringPropertyNames() {
        return STRING_PROPERTY_NAMES;
    }

    /**
     * masks things like passwords when logging
     * @param propertyName
     * @return
     */
    @Override
    protected boolean isMaskedProperty(String propertyName) {
        return propertyName.equals ("Password");
    }
    
/*
    <property name="javax.persistence.jdbc.url"
      value="jdbc:mysql://localhost:3306/iot?characterEncoding=UTF-8&amp;autoReconnect=true" />
    <property name="javax.persistence.jdbc.user" value="iot" />
    <property name="javax.persistence.jdbc.password" value="iot" />
*/
    final static String JDBC_USER="javax.persistence.jdbc.user";
    final static String JDBC_PASSWORD="javax.persistence.jdbc.password";
    final static String JDBC_URL="javax.persistence.jdbc.url";
    
    /* gets the properties, as javax.persistence.jdbc.xxx keys */
    @Override
    public Properties getJavaxPersistenceJdbcProperties() {
    	Properties props = new Properties();
    	props.setProperty(JDBC_USER, getUsername());
    	props.setProperty(JDBC_PASSWORD,getPassword());
    	props.setProperty(JDBC_URL, getMysqlJdbcUrl());
    	return props;
    }
    
    @Override
    public String getOracleJdbcUrl() {
    	return constructOracleJdbcUrl(getHostnames(), getPort(), getSid());
    }

    @Override
    public String getMysqlJdbcUrl() {
    	return String.format("jdbc:mysql://%s:%d/%s?characterEncoding=UTF-8&amp;autoReconnect=true", getHostnames().get(0), getPort(), getSid());
    }

    
    @Override
	public List<String> getHostnames() {
        return hostnames;
    }
    @Override
	public int getPort() {
        return intProps.get("Port");
    }
    @Override
	public String getSid() {
        return stringProps.get("SID");
    }
    @Override
	public String getUsername() {
        return stringProps.get("Username");
    }
    @Override
	public String getPassword() {
        return stringProps.get("Password");
    }
    
    
    /**
     * Returns an Oracle JDBC URL containing multiple hostnames, for connecting to Oracle RAC over TCP
     *
     * @param hostnames   list of database server hostnames
     * @param port        TCP port
     * @param sid         Oracle service name
     * @return
     */
    private String constructOracleJdbcUrl(List<String> hostnames, int port, String sid) {
        if (hostnames.size() < 1) {
            throw new FatalError("need > 1 database hostname in dbConfig option \"dbHostname\"");
        }

        StringBuilder buf = new StringBuilder();
        buf.append("jdbc:oracle:thin:@\n");
        buf.append("  (DESCRIPTION =\n");
        buf.append("    (ADDRESS_LIST =\n");
        for(String hostname: hostnames) {
            buf.append(String.format
                  ("      (ADDRESS= (PROTOCOL = TCP)(HOST = %s)(PORT = %d))\n", hostname, port));
        }
        buf.append("    )\n");
        buf.append("    (CONNECT_DATA =\n");
        buf.append("      (SERVICE_NAME = "+sid+")\n");
        buf.append("    )\n");
        buf.append("  )");
        return buf.toString();
    }

}
