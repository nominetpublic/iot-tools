/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.utils.dns;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.util.regex.Pattern;
import sun.net.util.IPAddressUtil;

/**
 * Identify and validate IPv4 and IPv6 addresses
 */

public class IpAddressValidator {
    // (static class)
    private IpAddressValidator() {}

    /**
     * regex to pre-screen to match an IPv4 address
     * NB this excludes the partial IPv4 addresses that would be accepted
     *  by java.net.Inet4Address ("1.2.3" etc)
     */
    private final static String IPv4match = "^[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+$";
    private final static Pattern patternIPv4match= Pattern.compile (IPv4match);

    /**
     * regex to reject leading zero on any byte
     * - IMHO a bug in java.net.Inet4Address
     */
    private final static String IPv4reject = "(?:^|.*\\.)0[0-9].*";
    private final static Pattern patternIPv4reject= Pattern.compile (IPv4reject);

    /**
     * regex to pre-screen to match an IPv6 address
     * NB this excludes IPv4-mapped/compatible IPv6 addresses
     *  allowed by java.net.Inet6Address
     */
    private final static String IPv6match = "^[0-9a-fA-F:]*$";
    private final static Pattern patternIPv6match= Pattern.compile (IPv6match);

    /**
     * regex to reject leading zero on any quad
     * - IMHO a bug in java.net.Inet6Address
     */
    private final static String IPv6reject = ".*0[0-9a-fA-F]{4}.*";
    private final static Pattern patternIPv6reject= Pattern.compile (IPv6reject);


    /**
     * Checks whether a string represents a valid IPv4 address
     *
     * @param ip_string IPv4 address (as a string) to validate
     * @return <code>true</code> if it is a valid IPv4 address
     */
    public static boolean isValidIPv4(String ip_string) {
        if (!patternIPv4match.matcher(ip_string).matches())
            return false;

        // reject leading zeros
        // (bug in Inet4Address)
        if (patternIPv4reject.matcher(ip_string).matches())
            return false;

        return IPAddressUtil.isIPv4LiteralAddress(ip_string);
    }

    /**
     * Checks whether a string represents a valid IPv6 address
     *
     * @param ip_string IPv6 address (as a string) to validate
     * @return <code>true</code> if it is a valid IPv6 address
     */
    public static boolean isValidIPv6(String ip_string) {
        if (!patternIPv6match.matcher(ip_string).matches())
            return false;

        // require a ":" because Inet6Address allows "1111" and even ""
        if (ip_string.indexOf(":") == -1)
            return false;

        // reject 5-digit segment with leading zero
        // (bug in Inet6Address)
        if (patternIPv6reject.matcher(ip_string).matches())
            return false;

        if (!IPAddressUtil.isIPv6LiteralAddress(ip_string))
            return false;

        // we think it's a plausible IPv6 address, but do a final check that
        // it's not an IPv6 address hiding an IPv4 address
        try {
            // we'll get a ClassCastException if this produces an IPv4
        	@SuppressWarnings("unused")
			Inet6Address ipv6 = (Inet6Address)InetAddress.getByName(ip_string);
            return true;
        }
        catch (Exception e) {   // expecting ClassCastException or UnknownHostException
            return false;
        }
    }
}
