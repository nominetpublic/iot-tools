/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.utils.jmx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.nominet.iot.utils.error.ProgramDefectException;
import uk.nominet.iot.utils.signals.SignalReceiver;

import javax.management.*;
import java.lang.invoke.MethodHandles;

public class ServerControl implements ServerControlMBean {
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	public final static String DEFAULT_OBJECT_NAME= "uk.nominet.research:name=ServerControl";

	SignalReceiver signalReceiver;
	protected ServerControl(SignalReceiver signalReceiver, String name) {
		this.signalReceiver = signalReceiver;

		ObjectName serverControlName = null;
		try {
			serverControlName = new ObjectName(name);
		} catch (MalformedObjectNameException e) {
			throw new ProgramDefectException("Unable to construct JMX object with name="+name, e);
		}

		try {
			JmxServer.getMBeanServer().registerMBean(this, serverControlName);
		} catch (InstanceAlreadyExistsException|MBeanRegistrationException|NotCompliantMBeanException e) {
			throw new ProgramDefectException("Unable to register MBean", e);
		}
	}

	public void reloadConfig() {
		log.info("received JMX command: reloadConfig()");
		signalReceiver.handleSigHup();
	}

	public static ServerControl register(SignalReceiver signalReceiver, String name) {
		return new ServerControl(signalReceiver, name);
	}
	public static ServerControl register(SignalReceiver signalReceiver) {
		return register(signalReceiver, DEFAULT_OBJECT_NAME);
	}

}
