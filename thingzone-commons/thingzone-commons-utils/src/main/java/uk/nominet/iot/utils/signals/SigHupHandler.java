/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.utils.signals;

import sun.misc.Signal;
import sun.misc.SignalHandler;

/**
 * SigHupHandler is a simple wrapper to handle Unix SIGHUP
 *
 * Usage:
 * - implement SignalReceiver.handleSigHup
 * - register to receive signal
 */

public class SigHupHandler implements SignalHandler{
	// singleton
	private static SigHupHandler instance = null;
	protected SigHupHandler() {}  // no
	public synchronized static SigHupHandler getInstance() {
		if(instance == null) {
			instance = new SigHupHandler();
		}
		return instance;
	}

	public final String SIGHUP="HUP";

	// we should replace the original handler if we want to unregister
	private SignalHandler oldHandle = null;

	// currently registered receiver
	SignalReceiver receiver;


	public void register(SignalReceiver receiver) {
		if (receiver == null) {
			throw new NullPointerException("attempt to register a null SignalReceiver");
		}
		this.receiver = receiver;

		// insert ourself into handler chain
		if (oldHandle == null) {
			oldHandle = Signal.handle(new Signal(SIGHUP), this);
		}
	}

	public void unregister() {
		// detach ourself & replace with default handler
		if (oldHandle != null) {
			Signal.handle(new Signal(SIGHUP), oldHandle);
			oldHandle = null;
		}
	}

	@Override
	public void handle(Signal signal) {
		// when we receive signal, pass to receiver
		if (this.receiver != null) {
			this.receiver.handleSigHup();
		}
	}
}
