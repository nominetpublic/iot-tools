/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.utils.config;

import java.util.Properties;



public class ConfigFile {
    Properties properties;

     /**
     * ConfigFile provides all the configuration parameters which we read
     *  from a file.
     *
     * @param fileName Name of file ("path/name") from which to read config
     *  stored as a Java properties, i.e. list of "key=value"
     */
    public ConfigFile(String fileName) {

        ReadOnlyPropertyFile propFile = new ReadOnlyPropertyFile(fileName);

        // filesystem etc checks
        propFile.checkPathExists();
        propFile.checkFileExists();
        propFile.checkFileReadable();

        // read & convert to appropriate values
        Properties properties = propFile.readFromFile();
        setProperties(properties);
    }

    private void setProperties (Properties properties) {
        this.properties = properties;
    }
    public Properties getProperties() {
        return properties;
    }
}
