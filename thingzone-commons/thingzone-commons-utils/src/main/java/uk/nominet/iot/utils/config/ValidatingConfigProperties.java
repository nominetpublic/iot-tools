/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.utils.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nominet.iot.utils.error.FatalError;


import java.util.*;

/**
 * Validating Property handler, for config files.
 * Checks the presence and format of all properties, whether String/Integer/Boolean.
 *
 * Processed properties are stored in typed (String/Integer/Boolean) sets.
 * Booleans are expected to be "Y" or "N"
 *
 * To use, subclass and implement the lists of names expected
 *
 *
 * @see java.util.Properties
 */


public abstract class ValidatingConfigProperties {
    private final static Logger log = LoggerFactory.getLogger(ValidatingConfigProperties.class);

    protected final Map<String, String> stringProps = new HashMap<String, String>();
    protected final Map<String, Integer> intProps = new HashMap<String, Integer>();
    protected final Map<String, Boolean> booleanProps = new HashMap<String, Boolean>();

    protected Properties validatedProperties;
    protected Properties extraneousProperties;

    /**
     * Initialises from a Properties object, and validates, throwing a FatalError on error
     *
     * @param properties  Properties to validate and store
     * @see FatalError
     */
    public ValidatingConfigProperties(Properties properties) {
        Validate(properties);
    }


    void Validate (Properties properties) {

        // clone properties
        Properties props = new Properties();
        props.putAll(properties);

        // allow pre-fudging (also for testing)
        props = preValidate(props);

        // Storing the properties that we expect in lists like
        //  this stops us from forgetting any of them

        // validate the string properties (and those which can be empty)
        Set<String> allowedEmptyStringProperties = new HashSet<String>(Arrays.asList(getAllowedEmptyStringProperties()));

        for (String propName: getStringPropertyNames()) {
            processStringProperty(props, propName, allowedEmptyStringProperties.contains(propName));
        }

        // validate the integer properties
        for (String propName: getIntegerPropertyNames()) {
            processIntegerProperty(props, propName);
        }

        // validate the flag / boolean properties
        for (String propName: getBooleanPropertyNames()) {
            processBooleanProperty(props, propName);
        }

        // any custom validation required
        props = customValidation(props);

        // there shouldn't be anything left!
        if (props.size() >  0) {
            for (String propName: props.stringPropertyNames()) {
                log.warn (String.format ("unexpected config parameter \"%s\"", propName));
            }
            this.extraneousProperties = props;
        }
        else {
            this.extraneousProperties = new Properties();
        }

        // validation was successful; store the properties
        this.validatedProperties = properties;
    }

    /**
     * custom validation (defaults to no-op)
     * @param properties  properties that you want to modify before normal validation
     * @return            properties that you have munged
     */
    protected Properties customValidation(Properties properties) {
        return properties;
    }

    /**
     * custom validation (defaults to no-op)
     * @param properties  properties that you want to process as special cases
     * @return            properties, with any that you have processed removed
     */
    protected Properties preValidate(Properties properties) {
        return properties;
    }
    
    
    /**
     * Specifies expected integer properties (abstract; implement this)
     * @return  list of expected integer properties
     */
    protected String[] getIntegerPropertyNames() {
    	return new String[] {}; // default empty
    }

    /**
     * Specifies expected string properties (abstract; implement this)
     * @return  list of expected string properties
     */
    protected String[] getStringPropertyNames() {
    	return new String[] {}; // default empty
    }

    /**
     * Specifies expected boolean properties ("Y" / "N") (abstract; implement this)
     * @return  list of expected boolean properties
     */
    protected String[] getBooleanPropertyNames() {
    	return new String[] {}; // default empty
    }

    /**
     * Specifies expected properties which can be blank
     * @return  list of allowed blank properties
     */
    protected String[] getAllowedEmptyStringProperties() {
    	return new String[] {}; // default empty
    }


    /**
     * Masks output of value from logging, e.g. for passwords.
     * Default implementation masks nothing, override to specify properties to mask.
     *
     * @param     propertyName  name of property to check
     * @return                  true if property value should be masked
     */
    protected boolean isMaskedProperty(String propertyName) {
        return false;	 // default nont
    }


    protected void processStringProperty(Properties props, String propName, boolean allowedBlank) {
        if (!props.containsKey(propName)) {
            throw new FatalError (String.format ("DbConfig file missing parameter \"%s\"", propName));
        }
        String value = props.getProperty(propName);
        if (value.equals("") && !allowedBlank) {
            throw new FatalError (String.format ("DbConfig file empty parameter \"%s\"", propName));
        }
        stringProps.put(propName, value);

        String loggedValue = isMaskedProperty(propName) ? "****" : value;
        log.info (String.format ("read config: %s = \"%s\"", propName, loggedValue));
        props.remove(propName);
    }

    protected void processIntegerProperty(Properties props, String propName) {
        if (!props.containsKey(propName)) {
            throw new FatalError (String.format ("DbConfig file missing parameter \"%s\"", propName));
        }
        String svalue = props.getProperty(propName);
        try {
            Integer value = Integer.parseInt(svalue);
            intProps.put(propName, value);
            log.info (String.format ("read config: %s = \"%d\"", propName, value));
        } catch (NumberFormatException e) {
            throw new FatalError (String.format ("DbConfig file parameter \"%s\" must be an integer", propName));
        }
        props.remove(propName);
    }

    protected void processBooleanProperty(Properties props, String propName) {
        if (!props.containsKey(propName)) {
            throw new FatalError (String.format ("DbConfig file missing parameter \"%s\"", propName));
        }
        String value = props.getProperty(propName);
        if (!value.equals("Y") && !value.equals("N")) {
            throw new FatalError (String.format ("DbConfig file parameter \"%s\" must be \"Y\" or \"N\"", propName));
        }
        booleanProps.put(propName, value.equals("Y"));
        log.info (String.format ("read config: %s = \"%s\"", propName, value));
        props.remove(propName);
    }

    /**
     * Returns the properties which have been validated
     * @return validated properties
     */
    public Properties getProperties() {
        return validatedProperties;
    }

    /**
     * Returns the properties which were extraneous, not expected
     * @return extraneous properties
     */
    public Properties getExtraneousProperties() {
        return extraneousProperties;
    }

}
