/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry.builders;

import org.jglue.fluentjson.*;

import java.util.*;

public class ObjectBuilder extends JsonBuilderFactory {

    Map<String, Object> objMap;
    ObjectBuilder parent;

    public ObjectBuilder() {
        objMap = new HashMap<>();
    }

    public ObjectBuilder(ObjectBuilder parent) {
        this.parent = parent;
        objMap = new HashMap<>();
    }

    /**
     * Add a boolean property
     * @param prop the property name
     * @param value the value
     */
    public ObjectBuilder add(String prop, Boolean value) throws Exception {
        if (objMap.containsKey(prop))
            throw new Exception("Property already exists");
        objMap.put(prop, value.toString());
        return this;
    }

    /**
     * Add a numeric property
     * @param prop the property name
     * @param value the value
     */
    public ObjectBuilder add(String prop, Number value) throws Exception {
        if (objMap.containsKey(prop))
            throw new Exception("Property already exists");
        objMap.put(prop, value);
        return this;
    }

    /**
     * Add a string value
     * @param prop the property name
     * @param value the value
     */
    public ObjectBuilder add(String prop, String value) throws Exception {
        if (objMap.containsKey(prop))
            throw new Exception("Property already exists");
        objMap.put(prop, value);
        return this;
    }

    /**
     * Add a property
     * @param prop the property name
     */
    public ObjectBuilder add(String prop) throws Exception {
        if (objMap.containsKey(prop))
            throw new Exception("Object already exists");
        ObjectBuilder newObj = new ObjectBuilder(this);
        objMap.put(prop, newObj.build());
        return newObj;
    }

    /**
     * Add an array property
     * @param prop the property name
     * @param values the values
     */
    public ObjectBuilder add(String prop, Object... values) throws Exception {
        if (objMap.containsKey(prop))
            throw new Exception("Array already exists");
        objMap.put(prop, Arrays.asList(values));
        return this;
    }

    /**
     * Build the object
     */
    public Map<String, Object> build() {
        return objMap;
    }

    /**
     * Get the current object
     */
    public ObjectBuilder end() {
        return parent;
    }

    /**
     * Create the object
     */
    public static ObjectBuilder create() {
        return new ObjectBuilder();
    }

}
