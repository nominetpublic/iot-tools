/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry.utils;

import org.apache.http.entity.ContentType;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.utils.Procedure;

import java.util.function.Supplier;


public class Upsertable<T> {
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
    private String name;
    private ContentType contentType;
    private boolean remove;

    private Supplier<?> deleteValue;
    private T value;


    public Upsertable(String name, ContentType contentType, T value) {
        this.name = name;
        this.value = value;
        this.contentType = contentType;
    }

    /**
     *
     * @return removed
     */
    public boolean toRemove() {
        return remove;
    }

    /**
     *
     * @param procedure procedure
     * @return usertable
     */
    public Upsertable<T> ifRemove(Procedure procedure) {
        if (remove) {
            procedure.invoke();
        }

        return this;
    }

    /**
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return content type
     */
    public ContentType getContentType() {
        return contentType;
    }


    /**
     *
     * @return value
     */
    public String getValue() {
        Object valueToUpsert;

        if (!remove) valueToUpsert = value;
        else {
            valueToUpsert = deleteValue.get();
        }

        if (valueToUpsert instanceof String) {
            return valueToUpsert.toString();
        } else {
            return jsonStringSerialiser.writeObject(valueToUpsert);
        }
    }

    /**
     *
     * @param name name
     * @param contentType content type
     * @param value value
     * @return upsertable
     */
    public static <T1> Upsertable<T1> of(String name, ContentType contentType, T1 value) throws Exception {
        if (name == null) throw new Exception("Name can not be null");
        if (value == null) throw new Exception("Value can not be null");
        return new Upsertable<>(name, contentType, value);
    }

    /**
     *
     * @param name name
     * @param contentType contentType
     * @param deleteValue delete value
     * @return upsertable
     */
    public static <T1> Upsertable<T1> remove(String name, ContentType contentType, Supplier<?> deleteValue) {
        Upsertable<T1> upsertable = new Upsertable<>(name, contentType, null);
        upsertable.remove = true;
        upsertable.deleteValue = deleteValue;
        return upsertable;
    }

    @Override
    public String toString() {
        return "Upsertable{" +
                "name='" + name + '\'' +
                ", remove=" + remove +
                ", value=" + value +
                '}';
    }
}
