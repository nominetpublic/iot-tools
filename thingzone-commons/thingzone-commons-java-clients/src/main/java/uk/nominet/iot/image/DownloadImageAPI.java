/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.image;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.http.client.utils.URIBuilder;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;

public class DownloadImageAPI {

    public static final String API_DOWNLOAD = "/images";
    private ImagesvrAPI api;

    public DownloadImageAPI(ImagesvrAPI api) {
        this.api = api;
    }

    public byte[] toBytes(String imageReference) throws IOException, URISyntaxException {
        URIBuilder builder = new URIBuilder(api.generateAPIUrl(String.format("%s/%s", API_DOWNLOAD, imageReference)));

        HttpConnectionManager httpConnectionManager = new MultiThreadedHttpConnectionManager();
        HttpClient httpClient = new HttpClient(httpConnectionManager);

        GetMethod getMethod = new GetMethod(builder.build().toString());
        httpClient.executeMethod(getMethod);
        return getMethod.getResponseBody();
    }


}
