/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import com.google.gson.JsonObject;
import org.apache.http.HttpEntity;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import uk.nominet.iot.utils.APIOperation;
import uk.nominet.iot.utils.APIOperationHelper;
import java.util.Map;

public class PushAPI {
    RegistryAPISession api;

    public PushAPI(RegistryAPISession api) {
        this.api = api;
    }


    /**
     * Add push subscriber to stream
     * @param dataStream data stream key
     * @param method method POST, PUSH
     * @param uri uri
     * @param uriParams uri parameters
     * @param headers headher parameters
     * @param retrySequence retry sequence
     * @throws Exception
     */
    public void setPushSubscriber(String dataStream,
                                  String method,
                                  String uri,
                                  Map<String,String> uriParams,
                                  Map<String,String> headers,
                                  String retrySequence) throws Exception {
        URIBuilder uriBuilder = new URIBuilder(api.generateAPICall("/api/push_subscribers/set"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                HttpEntity entity = MultipartEntityBuilder.create()
                                                          .addTextBody("dataStream", dataStream)
                                                          .addTextBody("method", method)
                                                          .addTextBody("uri", uri)
                                                          .addTextBody("uriParams", api.jsonStringSerialiser.writeObject(uriParams))
                                                          .addTextBody("headers", api.jsonStringSerialiser.writeObject(headers))
                                                          .addTextBody("retrySequence", retrySequence)
                                                          .build();

                return Request.Put(uriBuilder.build())
                              .body(entity)
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
    }

}
