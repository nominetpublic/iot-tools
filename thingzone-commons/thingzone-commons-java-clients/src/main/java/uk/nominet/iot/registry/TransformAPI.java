/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import java.util.List;
import java.util.Optional;

import org.apache.http.HttpEntity;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import com.google.common.collect.ImmutableList;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import uk.nominet.iot.model.registry.RegistryDataStream;
import uk.nominet.iot.model.registry.RegistryTransform;
import uk.nominet.iot.model.registry.RegistryTransformFunction;
import uk.nominet.iot.registry.utils.Upsertable;
import uk.nominet.iot.utils.APIOperation;
import uk.nominet.iot.utils.APIOperationHelper;

public class TransformAPI {
    RegistryAPISession api;

    public TransformAPI(RegistryAPISession api) {
        this.api = api;
    }

    /**
     * List available transform functions
     * @return a list of transform functions
     */
    public List<RegistryTransformFunction> listFunctions() throws Exception {
        URIBuilder uriBuilder = new URIBuilder(api.generateAPICall("/api/transform/list_functions"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Get(uriBuilder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();
        if (responseDocument.has("transformFunctions")) {
            return api.jsonStringSerialiser
                      .getDefaultSerialiser()
                      .fromJson(responseDocument.getAsJsonArray("transformFunctions"),
                                new TypeToken<List<RegistryTransformFunction>>() {}.getType());
        }
        return ImmutableList.of();
    }

    /**
     * Create a new transform function
     * @param name the name of the function
     * @param functionType the content-type of the function
     * @param functionContent the content of the function
     * @param isPublic not implemented - will return 501 not implemented if true
     */
    public void createFunction(String name, String functionType, String functionContent, boolean isPublic) throws Exception {
        URIBuilder uriBuilder = new URIBuilder(api.generateAPICall("/api/transform/create_function"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                HttpEntity entity = MultipartEntityBuilder.create()
                                                          .addTextBody("name", name)
                                                          .addTextBody("functionType", functionType)
                                                          .addTextBody("functionContent", functionContent)
                                                          .addTextBody("isPublic", isPublic ? "true" : "false")
                                                          .build();

                return Request.Put(uriBuilder.build())
                              .body(entity)
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
    }

    /**
     * Get the function matching the name
     * @param name a function name
     * @param isPublic not implemented- will return 501 not implemented if true
     * @return a transform function
     */
    public RegistryTransformFunction getFunction(String name, boolean isPublic) throws Exception {
        URIBuilder uriBuilder = new URIBuilder(api.generateAPICall("/api/transform/get_function"));
        uriBuilder.setParameter("name", name);
        uriBuilder.setParameter("isPublic", isPublic ? "true" : "false");

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Get(uriBuilder.build())
                        .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                        .execute()
                        .handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();
        if (responseDocument.has("function")) {
            return api.jsonStringSerialiser.readObject(responseDocument.get("function"), RegistryTransformFunction.class);
        }
        return null;
    }

    /**
     * Delete a transform function
     * @param name the name of the function to remove
     * @param isPublic not implemented - will throw an error if true
     */
    public void deleteFunction(String name, boolean isPublic) throws Exception {
        URIBuilder uriBuilder = new URIBuilder(api.generateAPICall("/api/transform/delete_function"));
        uriBuilder.setParameter("name", name);
        uriBuilder.setParameter("isPublic", isPublic ? "true" : "false");

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Delete(uriBuilder.build())
                        .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                        .execute()
                        .handleResponse(new JsonResponseHandler());
            }
        });
        apiOperationHelper.getResponseDocument();
    }

    /**
     * Update a transform function
     * @param name the name of the function to update
     * @param functionType the content type of the function
     * @param functionContent the content of the function
     * @param updatedName the new name of the function
     * @param isPublic not implemented - will return 501 not implemented if true
     */
    public void modifyFunction(String name,
                               Optional<String> functionType,
                               Optional<String> functionContent,
                               Optional<String> updatedName,
                               boolean isPublic) throws Exception {
        URIBuilder uriBuilder = new URIBuilder(api.generateAPICall("/api/transform/modify_function"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                MultipartEntityBuilder entityBuilder = MultipartEntityBuilder
                        .create()
                        .addTextBody("name", name)
                        .addTextBody("isPublic", isPublic ? "true" : "false");
                functionType.ifPresent(s -> entityBuilder.addTextBody("functionType", s));
                functionContent.ifPresent(s -> entityBuilder.addTextBody("functionContent", s));
                updatedName.ifPresent(s -> entityBuilder.addTextBody("updatedName", s));

                HttpEntity entity = entityBuilder.build();
                return Request.Put(uriBuilder.build())
                        .body(entity)
                        .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                        .execute()
                        .handleResponse(new JsonResponseHandler());
            }
        });
    }

    /**
     * List all transform keys for transforms you have MODIFY permission on
     * @return a list of device keys
     */
    public List<String> list() throws Exception {
        URIBuilder uriBuilder = new URIBuilder(api.generateAPICall("/api/transform/list"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Get(uriBuilder.build())
                        .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                        .execute()
                        .handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();
        return api.jsonStringSerialiser
                .getDefaultSerialiser()
                .fromJson(responseDocument.get("transforms"), new TypeToken<List<String>>() {}.getType());
    }

    /**
     * Create a new transform
     * @param functionName the name of the function
     * @param functionIsPublic not implemented
     * @param parameterValues the parameters to be passed to the transform function
     * @param transform an external key to use
     * @param name a name for the transform
     * @param type the type category of the transform
     * @param runSchedule a string representation of a run interval
     * @return the key of the new transform
     */
    public String create(String functionName,
                         boolean functionIsPublic,
                         JsonObject parameterValues,
                         Optional<String> transform,
                         Optional<String> name,
                         Optional<String> type,
                         Optional<String> runSchedule) throws Exception {
        URIBuilder uriBuilder = new URIBuilder(api.generateAPICall("/api/transform/create"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                MultipartEntityBuilder entityBuilder =
                        MultipartEntityBuilder.create()
                                              .addTextBody("functionName", functionName)
                                              .addTextBody("functionIsPublic", functionIsPublic ? "true" : "false")
                                              .addTextBody("parameterValues", parameterValues.toString());

                transform.ifPresent(s -> entityBuilder.addTextBody("transform", s));

                name.ifPresent(s -> entityBuilder.addTextBody("name", s));

                type.ifPresent(s -> entityBuilder.addTextBody("type", s));

                runSchedule.ifPresent(s -> entityBuilder.addTextBody("runSchedule", s));

                HttpEntity entity = entityBuilder.build();
                return Request.Post(uriBuilder.build())
                              .body(entity)
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        return apiOperationHelper.getResponseDocument().get("transform").getAsString();
    }

    /**
     * Get information about a device
     * @param transform a transform key
     * @param fullDetails whether to show extra details
     * @param showPermissions whether to show the permissions
     * @return the transform details
     */
    public RegistryTransform get(String transform, boolean fullDetails, boolean showPermissions) throws Exception {
        URIBuilder uriBuilder = new URIBuilder(api.generateAPICall("/api/transform/get"));
        uriBuilder.setParameter("transform", transform);
        if (fullDetails)
            uriBuilder.setParameter("detail", "full");
        if (fullDetails && showPermissions)
            uriBuilder.setParameter("showPermissions", "true");

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Get(uriBuilder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();
        return api.jsonStringSerialiser.readObject(responseDocument.get("transform"), RegistryTransform.class);
    }

    /**
     * Update which function is linked to the transform
     * @param transform a transform key
     * @param functionName the name of the function to add
     * @param functionIsPublic not implemented - will return 501 not implemented if true
     */
    public void updateFunction(String transform, String functionName, boolean functionIsPublic) throws Exception {
        URIBuilder uriBuilder = new URIBuilder(api.generateAPICall("/api/transform/update_function"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                Form form = Form.form()
                                .add("transform", transform)
                                .add("functionName", functionName)
                                .add("functionIsPublic", functionIsPublic ? "true" : "false");

                return Request.Put(uriBuilder.build())
                              .bodyForm(form.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
    }

    /**
     *
     Update the runSchedule of a tranform
     * @param transform a transform key
     * @param runSchedule run schedule string e.g.PT15M
     */
    public void updateRunSchedule(String transform, String runSchedule) throws Exception {
        URIBuilder uriBuilder = new URIBuilder(api.generateAPICall("/api/transform/update_runSchedule"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                Form form = Form.form()
                                .add("transform", transform)
                                .add("runSchedule", runSchedule);

                return Request.Put(uriBuilder.build())
                              .bodyForm(form.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
    }

    /**
     * Update outputStreams on the transform
     * @param transform a transform key
     * @param stream the stream to add - or if null delete
     * @param alias the name of the outputStream on the transform
     */
    public void updateOutputStream(String transform, String stream, String alias) throws Exception {
        URIBuilder uriBuilder = new URIBuilder(api.generateAPICall("/api/transform/update_outputStream"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                Form form = Form.form()
                                .add("transform", transform)
                                .add("stream", stream)
                                .add("alias", alias);

                return Request.Put(uriBuilder.build())
                              .bodyForm(form.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
    }

    /**
     * Update the source streams on the transform
     * @param transform a transform key
     * @param stream the stream to add/update - or if null delete
     * @param alias the name of the source stream on the transform
     * @param trigger if the transform should be triggered on new payloads from this source
     * @param aggregationType the aggregation query language
     * @param aggregationSpec the aggregation query
     */
    public void updateSource(String transform,
                             String stream,
                             String alias,
                             boolean trigger,
                             Optional<String> aggregationType,
                             Optional<String> aggregationSpec) throws Exception {
        URIBuilder uriBuilder = new URIBuilder(api.generateAPICall("/api/transform/update_source"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                Form form = Form.form()
                                .add("transform", transform)
                                .add("stream", stream)
                                .add("alias", alias)
                                .add("trigger", trigger ? "true" : "false");

                aggregationType.ifPresent(s -> form.add("aggregationType", s));
                aggregationSpec.ifPresent(s -> form.add("aggregationSpec", s));

                return Request.Put(uriBuilder.build())
                              .bodyForm(form.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
    }

    /**
     * Update the parameter values for a transform
     * @param transform a transform key
     * @param parameterValues the new parameters
     */
    public void updateParameters(String transform, JsonObject parameterValues) throws Exception {
        URIBuilder uriBuilder = new URIBuilder(api.generateAPICall("/api/transform/update_parameters"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                MultipartEntityBuilder entityBuilder =
                        MultipartEntityBuilder.create()
                                              .addTextBody("transform", transform)
                                              .addTextBody("parameterValues", parameterValues.toString());

                return Request.Put(uriBuilder.build())
                              .body(entityBuilder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
    }

    /**
     * Delete a transform
     * @param transform a transform key
     */
    public void delete(String transform) throws Exception {
        URIBuilder uriBuilder = new URIBuilder(api.generateAPICall("/api/transform/delete"));
        uriBuilder.setParameter("transform", transform);

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Delete(uriBuilder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        apiOperationHelper.getResponseDocument();
    }

    /**
     * Upsert data to the transform - this means that those parts that a specified will overwrite existing values in the transform
     * @param key the transform key
     * @param parts the parts of the transform to upsert
     * @return the full transform details with permissions
     */
    public RegistryDataStream upsert(String key, Upsertable<?>... parts) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/transform/upsert"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
                entityBuilder.addTextBody("transform", key);
                for (Upsertable<?> upsertable : parts) {
                   entityBuilder.addTextBody(upsertable.getName(), upsertable.getValue(), upsertable.getContentType());
                }

                return Request.Put(builder.build())
                              .body(entityBuilder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();
        return api.jsonStringSerialiser
                  .readObject(responseDocument.get("transform"), RegistryTransform.class);
    }
}
