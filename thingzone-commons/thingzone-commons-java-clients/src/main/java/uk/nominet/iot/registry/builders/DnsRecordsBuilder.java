/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry.builders;

import uk.nominet.iot.model.registry.RegistryDNSRecord;

import java.util.ArrayList;
import java.util.List;

public class DnsRecordsBuilder {
    List<RegistryDNSRecord> dnsRecords;

    public DnsRecordsBuilder() {
        dnsRecords = new ArrayList<>();
    }

    /**
     * Add a DNS record
     * @param description description
     * @param subdomain subdomain
     * @param ttl ttl
     * @param rrtype rrtype
     * @param rdata rdata
     */
    public DnsRecordsBuilder addDnsRecord(String description, String subdomain, int ttl, String rrtype,
                                          String rdata) throws Exception {
        RegistryDNSRecord dnsRecord = new RegistryDNSRecord(description, subdomain, ttl, rrtype, rdata);

        if (dnsRecords.indexOf(dnsRecord) != -1)
            throw new Exception("The reference already exists");
        dnsRecords.add(dnsRecord);
        return this;
    }

    public List<RegistryDNSRecord> build() {
        return dnsRecords;
    }

    public static DnsRecordsBuilder create() {
        return new DnsRecordsBuilder();
    }
}
