/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URLEncoder;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;

import com.google.gson.JsonObject;

import uk.nominet.iot.model.registry.RegistryDataStream;
import uk.nominet.iot.registry.stream.StreamBatchHandler;
import uk.nominet.iot.utils.APIOperation;
import uk.nominet.iot.utils.APIOperationHelper;

/**
 * Created by edoardo on 09/02/2017.
 */
public class DumpAPI {
    RegistryAPISession api;

    public DumpAPI(RegistryAPISession registryAPISession) {
        this.api = registryAPISession;
    }

    /**
     * Get the datastream matching the key - requires DUMP_ALL privilege
     * @param dataStream a datastream key
     * @return the datastream
     */
    public  RegistryDataStream datastream(String dataStream) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/dump/datastream"));
        builder.setParameter("dataStream", dataStream);

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Get(builder.build())
                        .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                        .execute().handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();
        return api.jsonStringSerialiser.readObject(responseDocument.get("dataStream"), RegistryDataStream.class);
    }

    /**
     * Get a stream of all entities in the registry - requires DUMP_ALL privilege
     * @param batchSize the number of entities in each batch
     * @return the batch handler
     */
    public StreamBatchHandler all(int batchSize) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/dump/all"));
        URI uri = builder.build();

        HttpURLConnection conn = (HttpURLConnection) uri.toURL().openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("X-FollyBridge-SessionKey", api.getSession().getSessionKey());

        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        return new StreamBatchHandler(rd, batchSize);
    }

    /**
     * Get a stream of all the entities that have changed since the date given - requires DUMP_ALL privilege
     * @param date the date to start getting the changes from
     * @param batchSize the number of entities in each batch
     * @return the batch handler
     */
    public StreamBatchHandler changes(Instant date, int batchSize) throws Exception {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ").withZone(ZoneId.of("UTC"));
        String format = String.format("api/changes/%s", URLEncoder.encode(formatter.format(date), "UTF-8"));
        URIBuilder builder = new URIBuilder(api.generateAPICall(format));
        URI uri = builder.build();

        HttpURLConnection conn = (HttpURLConnection) uri.toURL().openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("X-FollyBridge-SessionKey", api.getSession().getSessionKey());

        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        return new StreamBatchHandler(rd, batchSize);
    }

}
