/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.utils;

import com.google.gson.JsonObject;

public class APIOperationHelper {
    private JsonObject responseDocument;

    public void doWithRetry(int maxAttempts, APIOperation operation) throws Exception {
        Exception latestException = new Exception();

        for (int count = 0; count < maxAttempts; count++) {
            try {
                responseDocument = operation.invoke();
                return;
            } catch (Exception e) {
                latestException = e;
                operation.handleException(e);
            }
        }

      throw latestException;
    }

    public JsonObject getResponseDocument() {
        return responseDocument;
    }

}
