/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry.builders;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.entity.ContentType;

import uk.nominet.iot.model.registry.RegistryDNSRecord;
import uk.nominet.iot.model.registry.RegistryDeviceReference;
import uk.nominet.iot.model.registry.RegistryPushSubscriber;
import uk.nominet.iot.model.registry.RegistryTransformOutputStream;
import uk.nominet.iot.model.registry.RegistryTransformSource;
import uk.nominet.iot.registry.utils.Upsertable;

public class TransformUpsertBuilder {
    public static final String REMOVAL_MSG = "The field is already marked for removal";
    public static final String INITIALISED_MSG = "The field has already been initialised";

    private Upsertable<String> name;
    private Upsertable<String> type;
    private Upsertable<String> functionName;
    private Upsertable<Map<String, Object>> parameterValues;
    private Upsertable<Boolean> multiplexer;
    private Upsertable<String> runSchedule;
    private Upsertable<Map<String, Object>> metadata;
    private Upsertable<List<RegistryDeviceReference>> devices;
    private Upsertable<List<RegistryTransformSource>> sources;
    private Upsertable<List<RegistryTransformOutputStream>> outputStreams;
    private Upsertable<List<RegistryDNSRecord>> dnsRecords;
    private Upsertable<Map<String, List<String>>> userPermissions;
    private Upsertable<Map<String, List<String>>> groupPermissions;
    private Upsertable<List<RegistryPushSubscriber>> pushSubscribers;

    public TransformUpsertBuilder(String key) {
    }

    /**
     * Set the name of the transform
     * @param name the entity name
     */
    public TransformUpsertBuilder setName(String name) throws Exception {
        if (this.name == null)
            this.name = Upsertable.of("name", ContentType.TEXT_PLAIN, name);
        else if (this.name.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set the type of the transform
     * @param type the entity type
     */
    public TransformUpsertBuilder setType(String type) throws Exception {
        if (this.type == null)
            this.type = Upsertable.of("type", ContentType.TEXT_PLAIN, type);
        else if (this.type.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set the name of the function
     * @param functionName the function name
     */
    public TransformUpsertBuilder setFunctionName(String functionName) throws Exception {
        if (this.functionName == null)
            this.functionName = Upsertable.of("functionName", ContentType.TEXT_PLAIN, functionName);
        else if (this.functionName.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set the values of the parameters of the function
     * @param parameterValues the function parameters
     */
    public TransformUpsertBuilder setParameterValues(Map<String, Object> parameterValues) throws Exception {
        if (this.parameterValues == null)
            this.parameterValues = Upsertable.of("parameterValues", ContentType.APPLICATION_JSON, parameterValues);
        else if (this.parameterValues.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set the multiplexer
     * @param multiplexer the multiplexer
     */
    public TransformUpsertBuilder setMultiplexer(Boolean multiplexer) throws Exception {
        if (this.multiplexer == null)
            this.multiplexer = Upsertable.of("multiplexer", ContentType.TEXT_PLAIN, multiplexer);
        else if (this.multiplexer.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set the runschedule of the transform
     * @param runSchedule the run schedule
     */
    public TransformUpsertBuilder setRunSchedule(String runSchedule) throws Exception {
        if (this.runSchedule == null)
            this.runSchedule = Upsertable.of("runSchedule", ContentType.TEXT_PLAIN, runSchedule);
        else if (this.runSchedule.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set the transform metadata
     * @param metadata json metadata
     */
    public TransformUpsertBuilder setMetadata(Map<String, Object> metadata) throws Exception {
        if (this.metadata == null)
            this.metadata = Upsertable.of("metadata", ContentType.APPLICATION_JSON, metadata);
        else if (this.metadata.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set all the devices linked to the transform
     * @param devices a list of transforms to link
     */
    public TransformUpsertBuilder setDevices(List<RegistryDeviceReference> devices) throws Exception {
        if (this.devices == null)
            this.devices = Upsertable.of("devices", ContentType.APPLICATION_JSON, devices);
        else if (this.devices.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set all the sources for the transform
     * @param sources a list of streams to add as sources
     */
    public TransformUpsertBuilder setSources(List<RegistryTransformSource> sources) throws Exception {
        if (this.sources == null)
            this.sources = Upsertable.of("sources", ContentType.APPLICATION_JSON, sources);
        else if (this.sources.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set all the additional outputStreams for the transform
     * @param streams a list of streams to add as additional transform outputs
     */
    public TransformUpsertBuilder setOutputStreams(List<RegistryTransformOutputStream> streams) throws Exception {
        if (this.outputStreams == null)
            this.outputStreams = Upsertable.of("outputStreams", ContentType.APPLICATION_JSON, streams);
        else if (this.outputStreams.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set all pushSubscribers for the transform
     * @param devices a list of devices
     */
    public TransformUpsertBuilder setPushSubscribers(List<RegistryPushSubscriber> devices) throws Exception {
        if (this.pushSubscribers == null)
            this.pushSubscribers = Upsertable.of("pushSubscribers", ContentType.APPLICATION_JSON, devices);
        else if (this.pushSubscribers.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set all the DNS records for the transform
     * @param dnsRecords a list of DNS records for the entity key
     */
    public TransformUpsertBuilder setDnsRecords(List<RegistryDNSRecord> dnsRecords) throws Exception {
        if (this.dnsRecords == null)
            this.dnsRecords = Upsertable.of("dnsRecords", ContentType.APPLICATION_JSON, dnsRecords);
        else if (this.dnsRecords.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set all the user permissions for the transform
     * @param userPermissions the map of username to permissions
     */
    public TransformUpsertBuilder setUserPermissions(Map<String, List<String>> userPermissions) throws Exception {
        if (this.userPermissions == null)
            this.userPermissions = Upsertable.of("userPermissions", ContentType.APPLICATION_JSON, userPermissions);
        else if (this.userPermissions.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set all the group permissions for the transform
     * @param groupPermissions the map of groupname to permissions
     */
    public TransformUpsertBuilder setGroupPermissions(Map<String, List<String>> groupPermissions) throws Exception {
        if (this.groupPermissions == null)
            this.groupPermissions = Upsertable.of("groupPermissions", ContentType.APPLICATION_JSON, groupPermissions);
        else if (this.groupPermissions.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Remove the name from the transform
     */
    public TransformUpsertBuilder removeName() throws Exception {
        if (this.name == null)
            this.name = Upsertable.remove("name", ContentType.TEXT_PLAIN, String::new);
        else if (!this.name.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    /**
     * Remove the type from the transform
     */
    public TransformUpsertBuilder removeType() throws Exception {
        if (this.type == null)
            this.type = Upsertable.remove("type", ContentType.TEXT_PLAIN, String::new);
        else if (!this.type.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    /**
     * Remove any parameters to the function
     */
    public TransformUpsertBuilder removeParameterValues() throws Exception {
        if (this.parameterValues == null)
            this.parameterValues = Upsertable.remove("parameterValues", ContentType.APPLICATION_JSON, HashMap::new);
        else if (!this.parameterValues.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    /**
     * Remove the run schedule
     */
    public TransformUpsertBuilder removeRunSchedule() throws Exception {
        if (this.runSchedule == null)
            this.runSchedule = Upsertable.remove("runSchedule", ContentType.TEXT_PLAIN, String::new);
        else if (!this.runSchedule.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    /**
     * Remove the metadata from the transform
     */
    public TransformUpsertBuilder removeMetadata() throws Exception {
        if (this.metadata == null)
            this.metadata = Upsertable.remove("objMap", ContentType.APPLICATION_JSON, HashMap::new);
        else if (!this.metadata.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    /**
     * Remove all the linked devices from the transform
     */
    public TransformUpsertBuilder removeDevices() throws Exception {
        if (this.devices == null)
            this.devices = Upsertable.remove("devices", ContentType.APPLICATION_JSON, ArrayList::new);
        else if (!this.devices.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    /**
     * Remove all the source streams from the transform
     */
    public TransformUpsertBuilder removeSources() throws Exception {
        if (this.sources == null)
            this.sources = Upsertable.remove("sources", ContentType.APPLICATION_JSON, ArrayList::new);
        else if (!this.sources.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    /**
     * Remove all the additional outputStreams from the transform
     */
    public TransformUpsertBuilder removeOutputStreams() throws Exception {
        if (this.outputStreams == null)
            this.outputStreams = Upsertable.remove("outputStreams", ContentType.APPLICATION_JSON, ArrayList::new);
        else if (!this.outputStreams.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    /**
     * Remove all the push subscribers from the transform
     */
    public TransformUpsertBuilder removePushSubscribers() throws Exception {
        if (this.pushSubscribers == null)
            this.pushSubscribers = Upsertable.remove("pushSubscribers", ContentType.APPLICATION_JSON, ArrayList::new);
        else if (!this.pushSubscribers.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    /**
     * Remove the DNS records from the transform
     */
    public TransformUpsertBuilder removeDnsRecords() throws Exception {
        if (this.dnsRecords == null)
            this.dnsRecords = Upsertable.remove("dnsRecords", ContentType.APPLICATION_JSON, ArrayList::new);
        else if (!this.dnsRecords.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    /**
     * Remove all the group permissions from the entity
     */
    public TransformUpsertBuilder removeGroupPermissions() throws Exception {
        if (this.groupPermissions == null)
            this.groupPermissions = Upsertable.remove("groupPermissions", ContentType.APPLICATION_JSON, HashMap::new);
        else if (!this.groupPermissions.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    public Upsertable<?>[] build() {
        List<Upsertable<?>> toUpsert = new ArrayList<>();

        if (name != null)
            toUpsert.add(name);
        if (type != null)
            toUpsert.add(type);
        if (functionName != null)
            toUpsert.add(functionName);
        if (parameterValues != null)
            toUpsert.add(parameterValues);
        if (multiplexer != null)
            toUpsert.add(multiplexer);
        if (runSchedule != null)
            toUpsert.add(runSchedule);
        if (metadata != null)
            toUpsert.add(metadata);
        if (devices != null)
            toUpsert.add(devices);
        if (sources != null)
            toUpsert.add(sources);
        if (outputStreams != null)
            toUpsert.add(outputStreams);
        if (pushSubscribers != null)
            toUpsert.add(pushSubscribers);
        if (dnsRecords != null)
            toUpsert.add(dnsRecords);
        if (userPermissions != null)
            toUpsert.add(userPermissions);
        if (groupPermissions != null)
            toUpsert.add(groupPermissions);

        return toUpsert.toArray(new Upsertable[0]);
    }

    public static TransformUpsertBuilder create(String key) {
        return new TransformUpsertBuilder(key);
    }

}
