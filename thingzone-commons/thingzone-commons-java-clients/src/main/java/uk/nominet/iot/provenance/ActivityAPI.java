/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.provenance;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import uk.nominet.iot.registry.JsonResponseHandler;


public class ActivityAPI {
    public static final String API_ACTIVITY = "/api/activity";
    private ProvsvrAPI api;

    public ActivityAPI(ProvsvrAPI provsvrAPI) {
     api = provsvrAPI;
    }

    /**
     * Create an Activity
     * @param prefix prefix
     * @param id id of the activity
     * @param activity PROV-JSON document describing the activity
     * @return the id of the activity
     */
    public String create(String prefix, String id, JsonObject activity) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPIUrl(API_ACTIVITY, prefix, id));

        MultipartEntityBuilder entityBuilder =
                MultipartEntityBuilder
                        .create()
                        .addTextBody("activity", activity.toString(), ContentType.APPLICATION_JSON);

        JsonObject response  = Request.Put(builder.build())
               .body(entityBuilder.build())
               .execute()
               .handleResponse(new JsonResponseHandler());

        return String.format("%s:%s", prefix, id);
    }

    /**
     * Create an Activity
     * @param id id of the activity
     * @param activity PROV-JSON document describing the activity
     * @return the id of the activity
     */
    public String create(String id, JsonObject activity) throws Exception {
        return create(api.defaultPrefixName, id, activity);
    }


    /**
     * Get a activity
     * @param prefix prefix
     * @param id id of the activity
     * @return PROV-JSON describing the activity
     */
    public JsonObject get(String prefix, String id) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPIUrl(API_ACTIVITY, prefix, id));

        JsonObject response =  Request.Get(builder.build())
                                      .execute()
                                      .handleResponse(new JsonResponseHandler());

        return response.get("activity").getAsJsonObject();

    }

    /**
     * Get a activity
     * @param id id of the activity
     * @return PROV-JSON describing the activity
     */
    public JsonObject get(String id) throws Exception {
        return get(api.defaultPrefixName, id);
    }



    /**
     * Get all activities
     * param prefix prefix
     * @return PROV-JSON describing the activity
     */
    public JsonArray getAll(String prefix) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPIUrl(API_ACTIVITY, prefix));

        JsonObject response =  Request.Get(builder.build())
                                      .execute()
                                      .handleResponse(new JsonResponseHandler());

        return response.get("activities").getAsJsonArray();

    }

    /**
     * Get all activities
     * @return PROV-JSON describing the activity
     */
    public JsonArray getAll() throws Exception {
        return getAll(api.defaultPrefixName);
    }

}
