/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.utils;

import com.google.gson.JsonObject;
import uk.nominet.iot.registry.RegistryAPISession;
import uk.nominet.iot.registry.exceptions.RegistryFailResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class APIOperation {
    private static final Logger LOG = LoggerFactory.getLogger(APIOperation.class);
    private RegistryAPISession api;

    public APIOperation(RegistryAPISession api) {
        this.api = api;
    }

    abstract public JsonObject invoke() throws Exception;

    public void handleException(Exception cause) {
        if (cause.getClass().getName().equals(RegistryFailResponseException.class.getName()) &&
                ((RegistryFailResponseException)cause).isSessionFailure() ) {
            LOG.warn("Updating session after API operation exception : " + cause.getMessage());
            api.invalidateSessionKey();
        }
    }
}
