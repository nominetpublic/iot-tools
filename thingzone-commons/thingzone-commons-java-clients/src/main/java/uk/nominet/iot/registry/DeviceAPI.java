/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import java.util.List;
import java.util.Optional;

import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import com.github.filosganga.geogson.model.Geometry;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import uk.nominet.iot.model.registry.RegistryDevice;
import uk.nominet.iot.registry.utils.Upsertable;
import uk.nominet.iot.utils.APIOperation;
import uk.nominet.iot.utils.APIOperationHelper;

/**
 * Created by edoardo on 09/02/2017.
 */
public class DeviceAPI {
    RegistryAPISession api;

    public DeviceAPI(RegistryAPISession registryAPISession) {
        this.api = registryAPISession;
    }

    /**
     * List all devices you have permissions to view
     * @return a list of device keys
     */
    public List<String> list() throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/device/list"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Get(builder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();
        return api.jsonStringSerialiser
                  .getDefaultSerialiser()
                  .fromJson(responseDocument.get("devices"), new TypeToken<List<String>>() {}.getType());
    }

    /**
     * Create a new device
     * @param device an external key to use
     * @param name a name for the device
     * @param type the type category of the device
     * @return the key of the new device
     */
    public String create(Optional<String> device, Optional<String> name, Optional<String> type) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/device/create"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                Request request = Request.Post(builder.build())
                                         .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey());

                Form form = Form.form();
                device.ifPresent(s -> form.add("device", s));
                name.ifPresent(s -> form.add("name", s));
                type.ifPresent(s -> form.add("type", s));
                request = request.bodyForm(form.build());

                return request.execute().handleResponse(new JsonResponseHandler());
            }
        });
        return apiOperationHelper.getResponseDocument().get("device").getAsString();
    }

    /**
     * Get information about a device
     * @param key a device key
     * @param fullDetails whether to show extra details
     * @param showPermissions whether to show the permissions
     * @return the device details
     */
    public RegistryDevice get(String key, boolean fullDetails, boolean showPermissions) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/device/get"));
        builder.setParameter("device", key);
        if (fullDetails)
            builder.setParameter("detail", "full");
        if (fullDetails && showPermissions)
            builder.setParameter("showPermissions", "true");

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Get(builder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();
        return api.jsonStringSerialiser
                  .readObject(responseDocument.get("device"), RegistryDevice.class);
    }

    /**
     * Update the device
     * @param key a device key
     * @param metadata json metadata
     * @param location json location
     * @param name a name for the device
     * @param type the type category of the device
     */
    public void update(String key,
                       Optional<JsonObject> metadata,
                       Optional<Geometry<?>> location,
                       Optional<String> name,
                       Optional<String> type) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/device/update"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create().addTextBody("device", key);
                metadata.ifPresent(jsonObject -> entityBuilder.addTextBody("metadata", jsonObject.toString()));
                location.ifPresent(geometry -> entityBuilder.addTextBody("location", api.jsonStringSerialiser.writeObject(geometry)));
                name.ifPresent(s -> entityBuilder.addTextBody("name", s));
                type.ifPresent(s -> entityBuilder.addTextBody("type", s));

                return Request.Put(builder.build())
                              .body(entityBuilder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });

    }

    /**
     * Link a datastream to the device
     * @param key a device
     * @param dataStream the datastream to link to the device
     * @param name the name of the link between the device and the stream
     */
    public void addStream(String key, String dataStream, Optional<String> name) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/device/add_stream"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                Form form = Form.form().add("device", key).add("dataStream", dataStream);
                name.ifPresent(s -> form.add("name", s));

                return Request.Put(builder.build())
                              .bodyForm(form.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
    }

    /**
     * Remove a datastream from a device
     * @param key a device key
     * @param dataStream a datastream already linked to that device
     */
    public void removeStream(String key, String dataStream) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/device/remove_stream"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                Form form = Form.form().add("device", key).add("dataStream", dataStream);

                return Request.Put(builder.build())
                              .bodyForm(form.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
    }

    /**
     * Delete the device
     * @param key a device key
     */
    public void delete(String key) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/device/delete"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                Form form = Form.form().add("device", key);

                return Request.Delete(builder.build())
                              .bodyForm(form.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
    }

    /**
     * Upsert data to the device - this means that those parts that a specified will overwrite existing values in the device
     * @param key a device key
     * @param parts the parts of the device to upsert
     * @return the full device details with permissions
     */
    public RegistryDevice upsert(String key, Upsertable<?>... parts) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/device/upsert"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
                entityBuilder.addTextBody("device", key);
                for (Upsertable<?> upsertable : parts) {
                   entityBuilder.addTextBody(upsertable.getName(), upsertable.getValue(), upsertable.getContentType());
                }

                return Request.Put(builder.build())
                              .body(entityBuilder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();
        return api.jsonStringSerialiser
                  .readObject(responseDocument.get("device"), RegistryDevice.class);
    }

}
