/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.provenance;

import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;
import uk.nominet.iot.model.provenance.Prefix;
import uk.nominet.iot.registry.JsonResponseHandler;

import java.util.List;

public class PrefixAPI {
    public static final String API_PREFIX = "/api/prefix";
    private ProvsvrAPI api;

    public PrefixAPI(ProvsvrAPI provsvrAPI) {
        api = provsvrAPI;
    }

    /**
     * Create a prefix
     * @param prefix prefix
     * @return Prerix object
     */
    public Prefix create(String prefix, String namespace) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPIUrl(API_PREFIX, prefix));
        
        Form form = Form.form()
                        .add("namespace", namespace);

        JsonObject response = Request.Put(builder.build())
                                     .bodyForm(form.build())
                                     .execute()
                                     .handleResponse(new JsonResponseHandler());

        return api.jsonStringSerialiser.readObject(response.get("prefix"), Prefix.class);
    }


    /**
     * Get a prefix
     * @param prefix prefix
     * @return Prefix
     */
    public Prefix get(String prefix) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPIUrl(API_PREFIX, prefix));

        JsonObject response = Request.Get(builder.build())
                                     .execute()
                                     .handleResponse(new JsonResponseHandler());

        return api.jsonStringSerialiser.readObject(response.get("prefix"), Prefix.class);

    }


    /**
     * Get a prefix
     * @return Prefix
     */
    public List<Prefix> getAll() throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPIUrl(API_PREFIX));

        JsonObject response = Request.Get(builder.build())
                                     .execute()
                                     .handleResponse(new JsonResponseHandler());

        return api.jsonStringSerialiser
                       .getDefaultSerialiser()
                       .fromJson(response.get("prefixes"), new TypeToken<List<Prefix>>() {
                       }.getType());

    }


}
