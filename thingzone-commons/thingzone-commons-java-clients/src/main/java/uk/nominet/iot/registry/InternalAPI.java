/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;

import com.google.common.collect.ImmutableList;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import uk.nominet.iot.model.registry.*;
import uk.nominet.iot.model.registry.RegistryDependentTransform;
import uk.nominet.iot.utils.APIOperation;
import uk.nominet.iot.utils.APIOperationHelper;

/**
 * Created by edoardo on 09/02/2017.
 */
public class InternalAPI {
    RegistryAPISession api;

    public InternalAPI(RegistryAPISession registryAPISession) {
        this.api = registryAPISession;
    }

    /**
     * Get the user from the session key
     * @param sessionKey the session key
     * @return The user of the session
     */
    public RegistryUser getUsername(String sessionKey) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/internal/get_username"));
        builder.setParameter("sessionKey", sessionKey);

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        String sessionKeyReg = api.getSession().getSessionKey();


        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Get(builder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();
        return api.jsonStringSerialiser.readObject(responseDocument, RegistryUser.class);
    }

    /**
     * @param keys        a list of entity keys
     * @param permissions a list of permissions
     * @param sessionKey  a session key, must have either this or the username set
     * @param username    a username
     * @return true if the permissions match
     */
    public boolean permissionCheck(String[] keys,
                                   String[] permissions,
                                   Optional<String> sessionKey,
                                   Optional<String> username) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/internal/permission_check"));
        builder.setParameter("keys", String.join(",", keys));
        builder.setParameter("permissions", String.join(",", permissions));
        sessionKey.ifPresent(s -> builder.setParameter("sessionKey", s));
        username.ifPresent(s -> builder.setParameter("username", s));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Get(builder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        // This will throw an error if the keys do not match the permissions
        apiOperationHelper.getResponseDocument();
        return true;
    }

    /**
     * Find all users with the specified permissions on the entity
     * @param key         an entity key
     * @param permissions list of permissions to match
     * @return a list of usernames
     */
    public List<String> permittedUsers(String key, String[] permissions) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/internal/permitted_users"));
        builder.setParameter("permissions", String.join(",", permissions));
        builder.setParameter("key", key);

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Get(builder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();
        return api.jsonStringSerialiser
                       .getDefaultSerialiser()
                       .fromJson(responseDocument.getAsJsonArray("users"), new TypeToken<List<String>>() {
                       }.getType());
    }

    /**
     * Show all user and group permissions on the entity
     * @param key an entity key
     * @return RegistryKey showing the permissions
     */
    public RegistryKey permissionsForKey(String key) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/internal/permissions_for_key"));
        builder.setParameter("key", key);

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Get(builder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();
        return api.jsonStringSerialiser.readObject(responseDocument.getAsJsonObject("permissions"), RegistryKey.class);
    }

    /**
     * Get a list of transforms that depend on the given stream as a source
     * @param key a datastream key
     * @return a list of transforms
     */
    public List<RegistryDependentTransform> transformDependentOnStream(String key) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/internal/transforms_dependent_on_stream"));
        builder.setParameter("key", key);

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Get(builder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();

        if (responseDocument.has("dependent_transforms")) {
            return api.jsonStringSerialiser
                           .getDefaultSerialiser()
                           .fromJson(responseDocument.getAsJsonArray("dependent_transforms"),
                                     new TypeToken<List<RegistryDependentTransform>>() {
                                     }.getType());
        }
        return ImmutableList.of();
    }


    /**
     * Returns transforms with runSchedule set
     * @return a list of transforms
     */
    public List<RegistryTransform> transformWithRunSchedule() throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/internal/transforms_with_runSchedule"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Get(builder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });

        JsonObject responseDocument = apiOperationHelper.getResponseDocument();

        if (responseDocument.has("transforms")) {
            return api.jsonStringSerialiser
                           .getDefaultSerialiser()
                           .fromJson(responseDocument.getAsJsonArray("transforms"),
                                     new TypeToken<List<RegistryTransform>>() {
                                     }.getType());
        }

        return ImmutableList.of();
    }


    /**
     * Returns information about the entity - identical to search/identify
     * @param key             an entity key
     * @param fullDetails     whether to show extra details
     * @param showPermissions whether to show the permissions
     * @return an object matching the type of the entity
     */
    public Object identify(String key, boolean fullDetails, boolean showPermissions) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/internal/identify"));
        builder.setParameter("key", key);
        if (fullDetails)
            builder.setParameter("detail", "full");
        if (fullDetails && showPermissions)
            builder.setParameter("showPermissions", "true");

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Get(builder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();

        if (responseDocument.has("device")) {
            return api.jsonStringSerialiser.readObject(responseDocument.get("device"), RegistryDevice.class);
        } else if (responseDocument.has("transform")) {
            return api.jsonStringSerialiser.readObject(responseDocument.get("transform"), RegistryTransform.class);
        } else if (responseDocument.has("dataStream")) {
            return api.jsonStringSerialiser.readObject(responseDocument.get("dataStream"), RegistryDataStream.class);
        } else if (responseDocument.has("dnsKey")) {
            return api.jsonStringSerialiser.readObject(responseDocument.get("dnsKey"), RegistryEntity.class);
        }
        return null;
    }


    public List<RegistryPushSubscriberInternal> listPushSubscribers() throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/internal/push_subscribers"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Get(builder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();

        List<RegistryPushSubscriberInternal> pushSubscriberInternalList = new ArrayList<>();

        JsonArray pushSubscribers = responseDocument.getAsJsonArray("pushSubscribers");

        for (JsonElement pushSubscriber : pushSubscribers) {
            RegistryPushSubscriberInternal pushSubscriberInternal =
                    api.jsonStringSerialiser.readObject(pushSubscriber,
                                                        RegistryPushSubscriberInternal.class);
            pushSubscriberInternalList.add(pushSubscriberInternal);
        }

        return pushSubscriberInternalList;
    }

}
