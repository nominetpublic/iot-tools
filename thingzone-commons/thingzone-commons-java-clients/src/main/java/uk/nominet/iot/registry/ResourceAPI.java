/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import com.google.gson.JsonObject;
import org.apache.http.HttpEntity;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import uk.nominet.iot.model.registry.RegistryResource;
import uk.nominet.iot.utils.APIOperation;
import uk.nominet.iot.utils.APIOperationHelper;

import java.util.Optional;

/**
 * Created by edoardo on 09/02/2017.
 */
public class ResourceAPI {
    RegistryAPISession api;

    public ResourceAPI(RegistryAPISession registryAPISession) {
        this.api = registryAPISession;
    }


    public void upsert(String resourceName,
                       String contentType,
                       byte[] content,
                       Optional<String> key,
                       Optional<String> entityName,
                       Optional<String> entityType) throws Exception {

        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/resource/upsert"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                Request request = Request.Put(builder.build())
                                         .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey());

                MultipartEntityBuilder multipartEntityBuilder =
                        MultipartEntityBuilder.create()
                                              .addTextBody("resource_name", resourceName)
                                              .addBinaryBody("content", content,
                                                             ContentType.parse(contentType), "");

                multipartEntityBuilder.addTextBody("content_type", contentType);

                key.ifPresent(s -> multipartEntityBuilder.addTextBody("key", s));
                entityName.ifPresent(s -> multipartEntityBuilder.addTextBody("name", s));
                entityType.ifPresent(s -> multipartEntityBuilder.addTextBody("resource_type", s));

                HttpEntity entity = multipartEntityBuilder.build();
                request = request.body(entity);
                return request.execute().handleResponse(new JsonResponseHandler());
            }
        });
    }

    public RegistryResource get(String entityKey, String resourceName) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall(String.format("/api/resource/%s/%s", entityKey, resourceName)));

        Request request = Request.Get(builder.build())
                .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey());

       return request.execute().handleResponse(new RegistryResourceHandler());

    }


}
