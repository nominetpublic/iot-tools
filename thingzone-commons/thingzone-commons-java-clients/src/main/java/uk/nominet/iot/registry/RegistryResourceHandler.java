/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ResponseHandler;
import org.apache.http.entity.ContentType;
import uk.nominet.iot.model.registry.RegistryResource;
import uk.nominet.iot.registry.exceptions.RegistryFailResponseException;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class RegistryResourceHandler implements ResponseHandler<RegistryResource> {

    @Override
    public RegistryResource handleResponse(HttpResponse response) throws IOException  {
        StatusLine statusLine = response.getStatusLine();
        HttpEntity entity = response.getEntity();

        if (entity == null) {
            throw new RegistryFailResponseException("Response contains no content", false, statusLine.getStatusCode());
        }


        if (statusLine.getStatusCode() > 300) {
            throw new RegistryFailResponseException("Error getting resource", false, statusLine.getStatusCode());
        }

        ContentType contentType = ContentType.getOrDefault(entity);


        Charset charset = contentType.getCharset();
        if (charset == null) {
            charset = StandardCharsets.UTF_8;
        }

        InputStreamReader contentReader;
        try {
            contentReader = new InputStreamReader(entity.getContent(), charset);
        } catch (IOException e) {
            throw new RegistryFailResponseException("Can not read response input stream", false, statusLine.getStatusCode());
        }

        RegistryResource registryResource = new RegistryResource();
        registryResource.setContentType(contentType);
        registryResource.setCharset(charset);
        registryResource.setData(IOUtils.toByteArray(contentReader, charset));

        return registryResource;
    }
}
