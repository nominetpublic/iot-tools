/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import java.net.URLEncoder;
import java.util.List;
import java.util.Optional;

import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;

import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import uk.nominet.iot.utils.APIOperation;
import uk.nominet.iot.utils.APIOperationHelper;

/**
 * Created by edoardo on 13/09/2018.
 */
public class GroupAPI {
    RegistryAPISession api;

    /**
     * @param registryAPISession api session
     */
    public GroupAPI(RegistryAPISession registryAPISession) {
        this.api = registryAPISession;
    }

    /**
     * List the groups you are in or that you own
     * @return List of groupnames
     */
    public List<String> list(Optional<Boolean> owned) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/groups"));
        owned.ifPresent(aBoolean -> builder.setParameter("owned", aBoolean.toString()));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Get(builder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();
        return api.jsonStringSerialiser
                  .getDefaultSerialiser()
                  .fromJson(responseDocument.get("groups"), new TypeToken<List<String>>() {}.getType());
    }

    /**
     * Create a new group
     * @param group the group name
     */
    public void create(String group) throws Exception {
        String format = String.format("/api/groups/%s", URLEncoder.encode(group, "UTF-8"));
        URIBuilder builder = new URIBuilder(api.generateAPICall(format));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Post(builder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
    }

    /**
     * List the users in the group
     * @param group the group name
     */
    public List<String> listUsers(String group) throws Exception {
        String url = String.format("/api/groups/%s/users", URLEncoder.encode(group, "UTF-8"));
        URIBuilder builder = new URIBuilder(api.generateAPICall(url));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Get(builder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();
        return api.jsonStringSerialiser
                  .getDefaultSerialiser()
                  .fromJson(responseDocument.get("users"), new TypeToken<List<String>>() {}.getType());
    }

    /**
     * Add users to the group
     * @param group the group name
     * @param usernames a list of usernames to add
     */
    public void addUsers(String group, String[] usernames) throws Exception {
        String url = String.format("/api/groups/%s/users/add", URLEncoder.encode(group, "UTF-8"));
        URIBuilder builder = new URIBuilder(api.generateAPICall(url));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Put(builder.build())
                              .bodyForm(Form.form().add("usernames", String.join(",", usernames)).build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
    }

    /**
     * Remove users from the group
     * @param group the group name
     * @param usernames a list of usernames to remove
     */
    public void removeUsers(String group, String[] usernames) throws Exception {
        String url = String.format("/api/groups/%s/users/remove", URLEncoder.encode(group, "UTF-8"));
        URIBuilder builder = new URIBuilder(api.generateAPICall(url));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Put(builder.build())
                              .bodyForm(Form.form().add("usernames", String.join(",", usernames)).build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
    }

    /**
     * Toggle the admin status of a user in a group
     * @param group the group name
     * @param username the user in the group to change the admin status of
     * @param admin the admin status to set
     */
    public void changeAdminStatus(String group, String username, boolean admin) throws Exception {
        String format = String.format("/api/groups/%s/admin", URLEncoder.encode(group, "UTF-8"));
        URIBuilder builder = new URIBuilder(api.generateAPICall(format));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Put(builder.build())
                              .bodyForm(Form.form()
                                        .add("username", username)
                                        .add("admin", (admin) ? "true" : "false")
                                        .build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
    }

}
