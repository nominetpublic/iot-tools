/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ResponseHandler;
import org.apache.http.entity.ContentType;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import uk.nominet.iot.registry.exceptions.RegistryFailResponseException;

public class JsonResponseHandler implements ResponseHandler<JsonObject> {

    @Override
    public JsonObject handleResponse(HttpResponse response) throws IOException  {
        StatusLine statusLine = response.getStatusLine();
        HttpEntity entity = response.getEntity();

        if (entity == null) {
            throw new RegistryFailResponseException("Response contains no content", false, statusLine.getStatusCode());
        }

        ContentType contentType = ContentType.getOrDefault(entity);
        if (!contentType.getMimeType().equals("application/json")) {

//            if (statusLine.getStatusCode() > 300)
//                System.out.println("Response:" + IOUtils.toString(entity.getContent(), StandardCharsets.UTF_8));

            throw new RegistryFailResponseException("Unexpected registry response type. actual: " +
                    contentType.getMimeType() + " expected: application/json. Status: " + statusLine.getStatusCode(), false, statusLine.getStatusCode());
        }

        Charset charset = contentType.getCharset();
        if (charset == null) {
            charset = StandardCharsets.UTF_8;
        }

        Reader contentReader;
        try {
            contentReader = new InputStreamReader(entity.getContent(), charset);
        } catch (IOException e) {
            throw new RegistryFailResponseException("Can not read response input stream", false, statusLine.getStatusCode());
        }

        JsonObject doc = new Gson().fromJson(contentReader, JsonObject.class);

        if (!doc.get("result").getAsString().equals("ok") || statusLine.getStatusCode() >= 300) {
            boolean sessionFailure = false;

            if (doc.has("reason") && doc.get("reason").getAsString().startsWith("session")) {
                sessionFailure = true;
            }
            throw new RegistryFailResponseException(doc.toString(), sessionFailure, statusLine.getStatusCode());
        }

        return doc;
    }
}
