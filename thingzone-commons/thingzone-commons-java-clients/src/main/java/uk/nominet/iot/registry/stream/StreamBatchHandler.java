/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry.stream;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.registry.RegistryDataStream;
import uk.nominet.iot.model.registry.RegistryDevice;
import uk.nominet.iot.model.registry.RegistryEntity;
import uk.nominet.iot.model.registry.RegistryTransform;

/**
 * Created by edoardo on 27/09/2016.
 * Stream handler that can deal with both dump and changes output
 */
public class StreamBatchHandler implements Serializable {
    private static final long serialVersionUID = -7334306378595722998L;
    private static final Logger LOG = LoggerFactory.getLogger(StreamBatchHandler.class);

    private BufferedReader rd;
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
    private int batchSize;

    public StreamBatchHandler(BufferedReader rd, int batchSize) {
        this.rd = rd;
        this.batchSize = batchSize;
    }

    public void forEachBatch(StreamBatchInterface worker) throws Exception {
        ArrayList<RegistryEntity> batch = new ArrayList<>();
        int batchSequence = 0;

        try (JsonReader reader = new JsonReader(rd)) {
            if (reader.peek().equals(JsonToken.BEGIN_OBJECT)) {
                reader.beginObject();
                while (reader.hasNext()) {
                    String nextName = reader.nextName();
                    if (nextName.equals("reason")) {
                        throw new Exception(reader.nextString());
                    } else {
                        reader.skipValue();
                    }
                }
            } else {
                reader.beginArray();
                while (reader.hasNext()) {
                    batch.add(readRegistryEntity(reader));
                }
                reader.endArray();

                if (batch.size() >= batchSize) {
                    worker.processBatch(batch, batchSequence++);
                    batch = new ArrayList<>();
                }
            }
        } catch (IOException e) {
            throw new Exception(e.getMessage());
        }

        if (!batch.isEmpty()) {
            worker.processBatch(batch, batchSequence++);
        }
    }

    private RegistryEntity readRegistryEntity(JsonReader reader) throws Exception {
        RegistryEntity entity = null;
        List<String> removedUserPermissions = new ArrayList<>();
        List<String> removedGroupPermissions = new ArrayList<>();
        reader.beginObject();
        while (reader.hasNext()) {
            switch (reader.nextName()) {
                case "device":
                    entity = jsonStringSerialiser.getDefaultSerialiser().fromJson(reader, RegistryDevice.class);
                    break;
                case "dataStream":
                    entity = jsonStringSerialiser.getDefaultSerialiser().fromJson(reader, RegistryDataStream.class);
                    break;
                case "transform":
                    entity = jsonStringSerialiser.getDefaultSerialiser().fromJson(reader, RegistryTransform.class);
                    break;
                case "removedUserPermissions":
                    reader.beginArray();
                    while (reader.hasNext()) {
                        removedUserPermissions.add(reader.nextString());
                    }
                    reader.endArray();
                    break;
                case "removedGroupPermissions":
                    reader.beginArray();
                    while (reader.hasNext()) {
                        removedGroupPermissions.add(reader.nextString());
                    }
                    reader.endArray();
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }
        reader.endObject();
        for (String username : removedUserPermissions) {
            assert entity != null;
            entity.getUserPermissions().put(username, ImmutableList.of());
        }
        for (String groupname : removedGroupPermissions) {
            assert entity != null;
            entity.getGroupPermissions().put(groupname, ImmutableList.of());
        }
        return entity;
    }
}
