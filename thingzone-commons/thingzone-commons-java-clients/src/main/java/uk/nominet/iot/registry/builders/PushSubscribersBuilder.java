/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry.builders;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import uk.nominet.iot.model.registry.RegistryPushSubscriber;

public class PushSubscribersBuilder {
    List<RegistryPushSubscriber> pushSubscribers;

    public PushSubscribersBuilder() {
        pushSubscribers = new ArrayList<>();
    }

    /**
     *
     * @param uri uri
     * @param uriParams uriParams
     * @param method methid
     * @param headers headhers
     * @param retrySequence retry sequence
     * @return push subscriber builder
     */
    public PushSubscribersBuilder addPushSubscriber(String uri, Map<String, Object> uriParams, String method, Map<String, Object> headers, String retrySequence) throws Exception {
        RegistryPushSubscriber registryPushSubscriber = new RegistryPushSubscriber(uri, uriParams, method, headers, retrySequence);

        if (pushSubscribers.indexOf(registryPushSubscriber) != -1)
            throw new Exception("The reference already exists");
        pushSubscribers.add(registryPushSubscriber);
        return this;
    }

    public List<RegistryPushSubscriber> build() {
        return pushSubscribers;
    }

    public static PushSubscribersBuilder create() {
        return new PushSubscribersBuilder();
    }
}
