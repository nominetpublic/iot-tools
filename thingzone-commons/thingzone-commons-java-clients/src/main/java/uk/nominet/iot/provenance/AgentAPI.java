/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.provenance;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import uk.nominet.iot.registry.JsonResponseHandler;

public class AgentAPI {
    public static final String API_AGENT = "/api/agent";
    private ProvsvrAPI api;

    public AgentAPI(ProvsvrAPI provsvrAPI) {
        api = provsvrAPI;
    }


    /**
     * Create an Activity
     * @param prefix prefix
     * @param id id of the activity
     * @param agent PROV-JSON document describing the activity
     * @return the id of the activity
     */
    public String create(String prefix, String id, JsonObject agent, String cookie) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPIUrl(API_AGENT, prefix, id));

        MultipartEntityBuilder entityBuilder =
                MultipartEntityBuilder
                        .create()
                        .addTextBody("cookie", cookie, ContentType.TEXT_PLAIN)
                        .addTextBody("agent", agent.toString(), ContentType.APPLICATION_JSON);


        JsonObject response  = Request.Put(builder.build())
                                      .body(entityBuilder.build())
                                      .execute()
                                      .handleResponse(new JsonResponseHandler());

        System.out.println("Response: " + response);


        return String.format("%s:%s", prefix, id);
    }

    /**
     * Get an entity
     * @param prefix prefix
     * @param id id of the entoty
     * @return PROV-JSON describing the entity
     */
    public JsonObject get(String prefix, String id) throws Exception{
        URIBuilder builder = new URIBuilder(api.generateAPIUrl(API_AGENT, prefix, id));

        JsonObject response =  Request.Get(builder.build())
                                      .execute()
                                      .handleResponse(new JsonResponseHandler());

        return response.get("agent").getAsJsonObject();
    }

    /**
     * Get an entity
     * @param id id of the entoty
     * @return PROV-JSON describing the entity
     */
    public JsonObject get(String id) throws Exception{
        return get(api.defaultPrefixName, id);
    }


    /**
     * Get all entities
     * @return PROV-JSON describing the entities
     */
    public JsonArray getAll(String prefix) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPIUrl(API_AGENT, prefix));

        JsonObject response =  Request.Get(builder.build())
                                      .execute()
                                      .handleResponse(new JsonResponseHandler());

        return response.get("entities").getAsJsonArray();

    }

    /**
     * Get all entities
     * @return PROV-JSON describing the entities
     */
    public JsonArray getAll() throws Exception {
        return getAll(api.defaultPrefixName);

    }


}
