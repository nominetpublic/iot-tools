/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.image;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.ByteArrayPartSource;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.http.client.utils.URIBuilder;

import java.io.IOException;

public class UploadImageAPI {
    public static final String API_UPLOAD = "/upload";

    private ImagesvrAPI api;

    public UploadImageAPI(ImagesvrAPI api) {
        this.api = api;
    }

    public String fromBytes(byte[] image)  throws Exception {

        URIBuilder builder = new URIBuilder(api.generateAPIUrl(API_UPLOAD));

        HttpConnectionManager httpConnectionManager = new MultiThreadedHttpConnectionManager();
        HttpClient httpClient = new HttpClient(httpConnectionManager);

        PostMethod postMethod = new PostMethod(builder.build().toString());

        ByteArrayPartSource byteArrayPartSource = new ByteArrayPartSource("file", image);
        FilePart filePart = new FilePart("file", byteArrayPartSource);
        Part[] parts = {filePart};
        try {
            MultipartRequestEntity multipartRequestEntity = new MultipartRequestEntity(parts, postMethod.getParams());
            postMethod.setRequestEntity(multipartRequestEntity);
            httpClient.executeMethod(postMethod);

            JsonObject responseJson = new Gson().fromJson(postMethod.getResponseBodyAsString(), JsonObject.class);
            return responseJson.get("name").getAsString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
