/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry.builders;

import uk.nominet.iot.model.registry.RegistryStreamReference;

import java.util.ArrayList;
import java.util.List;

public class StreamReferencesBuilder {
    List<RegistryStreamReference> streamReferences;

    public StreamReferencesBuilder() {
        streamReferences = new ArrayList<>();
    }

    /**
     * Add a datastream reference
     * @param key the entity key
     * @param name the linkName
     */
    public StreamReferencesBuilder addReference(String key, String name) throws Exception {
        RegistryStreamReference registryStreamReference = new RegistryStreamReference(key, name);

        if (streamReferences.indexOf(registryStreamReference) != -1)
            throw new Exception("The reference already exists");
        streamReferences.add(registryStreamReference);
        return this;
    }

    public List<RegistryStreamReference> build() {
        return streamReferences;
    }

    public static StreamReferencesBuilder create() {
        return new StreamReferencesBuilder();
    }
}
