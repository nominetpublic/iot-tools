/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.provenance;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;
import uk.nominet.iot.registry.JsonResponseHandler;

public class EntityAPI {
    public static final String API_ENTITY = "/api/entity";
    private ProvsvrAPI api;

    public EntityAPI(ProvsvrAPI provsvrAPI) {
        api = provsvrAPI;
    }

    /**
     * Get an entity
     * @param prefix prefix
     * @param id id of the entoty
     * @return PROV-JSON describing the entity
     */
    public JsonObject get(String prefix, String id) throws Exception{
        URIBuilder builder = new URIBuilder(api.generateAPIUrl(API_ENTITY, prefix, id));

        JsonObject response =  Request.Get(builder.build())
                                      .execute()
                                      .handleResponse(new JsonResponseHandler());

        return response.get("entity").getAsJsonObject();
    }

    /**
     * Get an entity
     * @param id id of the entoty
     * @return PROV-JSON describing the entity
     */
    public JsonObject get(String id) throws Exception{
        return get(api.defaultPrefixName, id);
    }


    /**
     * Get all entities
     * @return PROV-JSON describing the entities
     */
    public JsonArray getAll(String prefix) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPIUrl(API_ENTITY, prefix));

        JsonObject response =  Request.Get(builder.build())
                                      .execute()
                                      .handleResponse(new JsonResponseHandler());

        return response.get("entities").getAsJsonArray();

    }

    /**
     * Get all entities
     * @return PROV-JSON describing the entities
     */
    public JsonArray getAll() throws Exception {
        return getAll(api.defaultPrefixName);

    }


}
