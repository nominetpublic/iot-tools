/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.image;

import uk.nominet.iot.json.JsonStringSerialiser;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class ImagesvrAPI {

    public final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    private String imagesvrURL;

    public UploadImageAPI upload;

    public DownloadImageAPI download;

    public ImagesvrAPI(String imagesvrURL) {
        this.imagesvrURL = imagesvrURL;
        upload = new UploadImageAPI(this);
        download = new DownloadImageAPI(this);
    }

    /**
     * Generate api URL
     * @param basePath base path
     * @param pathParts parts
     * @return image reference
     */
    public String generateAPIUrl(String basePath, String... pathParts) throws UnsupportedEncodingException {
        StringBuilder url = new StringBuilder(imagesvrURL + basePath);

        for (String path : pathParts) {
            if (!url.toString().endsWith("/") && !path.startsWith("/")) url.append("/");
            url.append(URLEncoder.encode(path, "UTF-8"));
        }
        return url.toString();
    }
}
