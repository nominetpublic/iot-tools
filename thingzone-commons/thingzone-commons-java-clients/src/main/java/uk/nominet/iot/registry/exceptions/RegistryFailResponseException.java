/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry.exceptions;

import org.apache.http.client.ClientProtocolException;

/**
 Created by edoardo on 09/02/2017.
 */
public class RegistryFailResponseException extends ClientProtocolException {
    private static final long serialVersionUID = -8068867257117925472L;

    private boolean sessionFailure;
    private int statusCode;

    public RegistryFailResponseException(String message, boolean sessionFailure, int statusCode) {
        super(message);
        this.sessionFailure = sessionFailure;
        this.statusCode = statusCode;
    }

    public boolean isSessionFailure() {
        return sessionFailure;
    }

    public int getStatusCode() {
        return statusCode;
    }
}
