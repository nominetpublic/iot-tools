/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry.typeAdapter;

import com.google.gson.*;
import uk.nominet.iot.model.registry.RegistryDataStream;
import uk.nominet.iot.registry.RegistryAPISession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;

public class DataStreamResolver implements JsonSerializer<RegistryDataStream>, JsonDeserializer<RegistryDataStream> {
    private static final Logger LOG = LoggerFactory.getLogger(DataStreamResolver.class);

    RegistryAPISession api;

    public DataStreamResolver(RegistryAPISession api) {
        this.api = api;
    }

    @Override
    public RegistryDataStream deserialize(JsonElement jsonElement,
                                          Type type,
                                          JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        if (jsonElement.isJsonPrimitive()) {
            try {
                return api.datastream.get(jsonElement.getAsString(), true, true);
            } catch (Exception e) {
                LOG.error(e.getMessage());
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public JsonElement serialize(RegistryDataStream dataStream, Type type, JsonSerializationContext jsonSerializationContext) {
        return new Gson().toJsonTree(dataStream);
    }
}
