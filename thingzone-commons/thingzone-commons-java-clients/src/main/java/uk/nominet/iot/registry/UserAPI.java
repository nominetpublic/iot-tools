/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import java.util.Optional;

import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import com.google.gson.JsonObject;

import uk.nominet.iot.model.registry.RegistryUser;
import uk.nominet.iot.utils.APIOperation;
import uk.nominet.iot.utils.APIOperationHelper;

/**
 * Created by edoardo on 09/02/2017.
 */
public class UserAPI {
    RegistryAPISession api;

    /**
     * @param registryAPISession api session
     */
    public UserAPI(RegistryAPISession registryAPISession) {
        this.api = registryAPISession;
    }

    /**
     * Create a new user
     * @param username the username for the user
     * @param password the password for the user
     * @param privileges the privileges for the user
     */
    public void create(String username, String password, String[] privileges) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/user/create"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Post(builder.build())
                              .bodyForm(Form.form()
                                            .add("username", username)
                                            .add("password", password)
                                            .add("privileges", String.join(",", privileges))
                                            .build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });

    }

    /**
     * Change the password of the current user
     * @param existingPassword the current password
     * @param newPassword the password to change it to
     */
    public void changePassword(String existingPassword, String newPassword) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/user/change_password"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Put(builder.build())
                              .bodyForm(Form.form()
                                            .add("existingPassword", existingPassword)
                                            .add("newPassword", newPassword).build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
    }

    /**
     * Get information about a user
     * @param username the name of the user to look up - null for current user
     * @return the user details
     */
    public RegistryUser get(String username) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/user/get"));
        builder.setParameter("username", username);

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Get(builder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();
        return api.jsonStringSerialiser.readObject(responseDocument.get("user"), RegistryUser.class);
    }

    /**
     * Update the user information
     * @param preferences the user preferences
     * @param details the user details
     */
    public void update(Optional<JsonObject> preferences, Optional<JsonObject> details) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/user/update"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
                preferences.ifPresent(jsonObject -> entityBuilder.addTextBody("preferences", jsonObject.toString()));
                details.ifPresent(jsonObject -> entityBuilder.addTextBody("details", jsonObject.toString()));

                return Request.Put(builder.build())
                              .body(entityBuilder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
    }

    /**
     * Update the privileges for the user
     * @param username the username of the user to update
     * @param privileges the replacement privileges for the user
     */
    public void setPrivileges(String username, String[] privileges) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/user/set_privileges"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Put(builder.build())
                              .bodyForm(Form.form()
                                            .add("username", username)
                                            .add("privileges", String.join(",", privileges))
                                            .build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
    }

    /**
     * Request a password reset token
     * @param username the username to reset
     * @param expiry the time in seconds that the token will be valid for
     * @return a token code
     */
    public String requestToken(String username, Optional<Integer> expiry) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/user/request_token"));
        builder.setParameter("username", username);
        expiry.ifPresent(integer -> builder.setParameter("expiry", integer.toString()));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Get(builder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();
        return responseDocument.get("token").getAsString();
    }

    /**
     * Update the user password using a reset token
     * @param username the username to set the password for, must match the token
     * @param token a valid password reset token for that user
     * @param newPassword the new password to use for that user
     */
    public void setPassword(String username, String token, String newPassword) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/user/set_password"));
        builder.setParameter("username", username);
        builder.setParameter("token", token);
        builder.setParameter("newPassword", newPassword);

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Put(builder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        apiOperationHelper.getResponseDocument();
    }
}
