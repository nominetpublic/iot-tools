/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry.builders;

import java.util.ArrayList;
import java.util.List;

import uk.nominet.iot.model.registry.RegistryTransformOutputStream;

public class TransformOutputStreamsBuilder {
    List<RegistryTransformOutputStream> streams;

    public TransformOutputStreamsBuilder() {
        streams = new ArrayList<>();
    }

    /**
     * Add an output stream
     * @param key the key of the datastream to add
     * @param alias the alias for the stream
     */
    public TransformOutputStreamsBuilder addTransformOutputStream(String key, String alias) throws Exception {
        RegistryTransformOutputStream source = new RegistryTransformOutputStream(key, alias);

        if (streams.contains(source))
            throw new Exception("The reference already exists");
        streams.add(source);
        return this;
    }

    public List<RegistryTransformOutputStream> build() {
        return streams;
    }

    public static TransformOutputStreamsBuilder create() {
        return new TransformOutputStreamsBuilder();
    }
}
