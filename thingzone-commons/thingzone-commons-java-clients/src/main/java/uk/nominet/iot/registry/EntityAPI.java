/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import uk.nominet.iot.model.registry.RegistryEntity;
import uk.nominet.iot.utils.APIOperation;
import uk.nominet.iot.utils.APIOperationHelper;

import java.util.List;
import java.util.Optional;

/**
 Created by edoardo on 09/02/2017.
 */
public class EntityAPI {
    RegistryAPISession api;

    public EntityAPI(RegistryAPISession registryAPISession) {
        this.api = registryAPISession;
    }

    /**
     * Lists available keys by type
     * @param type The type description assigned to the key
     * @return list of datastream keys
     */
    public List<RegistryEntity> list(Optional<String> type, boolean fullDetails) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/entity/list"));

        builder.addParameter("type", type.orElse(""));

        if (fullDetails)
            builder.setParameter("detail", "full");

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Get(builder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();
        return api.jsonStringSerialiser
                  .getDefaultSerialiser()
                  .fromJson(responseDocument.get("entities"), new TypeToken<List<RegistryEntity>>() {}.getType());
    }

    /**
     * Create a new datastream
     * @param key an external key to use
     * @param name a name for the device
     * @param type the type category of the device
     * @return the key of the new datastream
     */
    public String create(Optional<String> key, Optional<String> name, Optional<String> type) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/entity/create_key"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                Request request = Request.Post(builder.build())
                                         .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey());

                Form form = Form.form();
                key.ifPresent(s -> form.add("key", s));
                type.ifPresent(s -> form.add("type", s));
                name.ifPresent(s -> form.add("name", s));


                request = request.bodyForm(form.build());
                return request.execute().handleResponse(new JsonResponseHandler());
            }
        });
        return apiOperationHelper.getResponseDocument().get("key").getAsString();
    }


    /**
     * Updates values on the key
     * @param key a entity key
     * @param name a name for the entity
     * @param type the type category of the entity
     */
    public void update(String key, Optional<String> name, Optional<String> type) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/entity/update"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create()
                        .addTextBody("dataStream", key);

                if (name.isPresent()) entityBuilder = entityBuilder.addTextBody("name", name.get());
                if (type.isPresent()) entityBuilder = entityBuilder.addTextBody("type", type.get());

                return Request.Put(builder.build())
                              .body(entityBuilder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
    }
}
