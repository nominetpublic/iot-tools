/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;

import com.google.gson.JsonObject;

import uk.nominet.iot.model.registry.RegistryDataStream;
import uk.nominet.iot.model.registry.RegistryDevice;
import uk.nominet.iot.model.registry.RegistryEntity;
import uk.nominet.iot.model.registry.RegistryTransform;
import uk.nominet.iot.utils.APIOperation;
import uk.nominet.iot.utils.APIOperationHelper;

/**
 * Created by edoardo on 09/02/2017.
 */
public class SearchAPI {
    RegistryAPISession api;

    public SearchAPI(RegistryAPISession registryAPISession) {
        this.api = registryAPISession;
    }

    /**
     * Returns information about the entity
     * @param key an entity key
     * @param fullDetails whether to show extra details
     * @param showPermissions whether to show the permissions
     * @return an object matching the type of the entity
     */
    public Object identify(String key, boolean fullDetails, boolean showPermissions) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/search/identify"));
        builder.setParameter("key", key);
        if (fullDetails)
            builder.setParameter("detail", "full");
        if (fullDetails && showPermissions)
            builder.setParameter("showPermissions", "true");

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Get(builder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();
        if (responseDocument.has("device")) {
            return api.jsonStringSerialiser.readObject(responseDocument.get("device"), RegistryDevice.class);
        } else if (responseDocument.has("transform")) {
            return api.jsonStringSerialiser.readObject(responseDocument.get("transform"), RegistryTransform.class);
        } else if (responseDocument.has("dataStream")) {
            return api.jsonStringSerialiser.readObject(responseDocument.get("dataStream"), RegistryDataStream.class);
        } else if (responseDocument.has("dnsKey")) {
            return api.jsonStringSerialiser.readObject(responseDocument.get("dnsKey"), RegistryEntity.class);
        }
        return null;
    }

}
