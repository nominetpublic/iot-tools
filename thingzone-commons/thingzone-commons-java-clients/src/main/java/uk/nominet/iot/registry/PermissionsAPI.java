/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import java.util.Optional;

import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;

import com.google.gson.JsonObject;

import uk.nominet.iot.model.registry.RegistryKey;
import uk.nominet.iot.utils.APIOperation;
import uk.nominet.iot.utils.APIOperationHelper;

/**
 * Created by edoardo on 09/02/2017.
 */
public class PermissionsAPI {
    RegistryAPISession api;

    /**
     * @param registryAPISession registry session
     */
    public PermissionsAPI(RegistryAPISession registryAPISession) {
        this.api = registryAPISession;
    }

    /**
     * List permissions on key - filtered by user or group
     * @param key an entity key
     * @param username a username to query
     * @param group a group to query
     * @return a list of
     */
    public RegistryKey list(String key, Optional<String> username, Optional<String> group) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/permission/list"));
        builder.setParameter("key", key);
        username.ifPresent(s -> builder.setParameter("username", s));
        group.ifPresent(s -> builder.setParameter("group", s));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Get(builder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();
        return api.jsonStringSerialiser.readObject(responseDocument, RegistryKey.class);
    }

    /**
     * List all permissions for a key
     * @param key an entity key
     * @return a registry key
     */
    public RegistryKey list(String key) throws Exception {
        return list(key, Optional.empty(), Optional.empty());
    }

    /**
     * Set permissions for a key
     * @param key and entity key
     * @param username the username to set permissions on
     * @param group the group to set permissions on (if username is also set then the permissions are applied to both)
     * @param permissions the list of permissions to set
     */
    public void set(String key, Optional<String> username, Optional<String> group, String[] permissions) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/permission/set"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                Form form = Form.form().add("key", key).add("permissions", String.join(",", permissions));
                username.ifPresent(s -> form.add("username", s));
                group.ifPresent(s -> form.add("group", s));

                return Request.Put(builder.build())
                              .bodyForm(form.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
    }
}
