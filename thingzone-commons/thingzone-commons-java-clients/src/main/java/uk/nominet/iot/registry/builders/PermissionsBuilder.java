/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry.builders;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PermissionsBuilder {
    private HashMap<String, List<String>> permissions;

    public PermissionsBuilder() {
        permissions = new HashMap<>();
    }

    /**
     * Add permissions
     * @param subject the user or group name
     * @param permission the permission to add
     */
    public PermissionsBuilder add(String subject, String permission) {
        if (!permissions.containsKey(subject))
            permissions.put(subject, new ArrayList<>());
        List<String> permissionList = permissions.get(subject);
        if (permissionList.indexOf(permission) == -1)
            permissionList.add(permission);
        return this;
    }

    /**
     * Add permissions
     * @param subject the user or group name
     * @param permissions an array of permissions
     */
    public PermissionsBuilder add(String subject, String... permissions) {
        for (String permission : permissions) {
            add(subject, permission);
        }
        return this;
    }

    public Map<String, List<String>> build() {
        return permissions;
    }

    public static PermissionsBuilder create() {
        return new PermissionsBuilder();
    }
}
