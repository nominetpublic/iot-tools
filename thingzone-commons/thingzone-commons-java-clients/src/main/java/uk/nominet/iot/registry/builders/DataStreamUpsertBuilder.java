/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry.builders;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.entity.ContentType;

import uk.nominet.iot.model.registry.RegistryDNSRecord;
import uk.nominet.iot.model.registry.RegistryDeviceReference;
import uk.nominet.iot.model.registry.RegistryPullSubscription;
import uk.nominet.iot.model.registry.RegistryPushSubscriber;
import uk.nominet.iot.registry.utils.Upsertable;

public class DataStreamUpsertBuilder {
    public static final String REMOVAL_MSG = "The field is already marked for removal";
    public static final String INITIALISED_MSG = "The field has already been initialised";

    private Upsertable<String> name;
    private Upsertable<String> type;
    private Upsertable<Map<String, Object>> metadata;
    private Upsertable<List<RegistryDeviceReference>> devices;
    private Upsertable<List<RegistryDNSRecord>> dnsRecords;
    private Upsertable<Map<String, List<String>>> userPermissions;
    private Upsertable<Map<String, List<String>>> groupPermissions;
    private Upsertable<List<RegistryPushSubscriber>> pushSubscribers;
    private Upsertable<RegistryPullSubscription> pullSubscription;

    public DataStreamUpsertBuilder(String key) {
    }

    /**
     * Set the name of the DataStream
     * @param name the entity name
     */
    public DataStreamUpsertBuilder setName(String name) throws Exception {
        if (this.name == null)
            this.name = Upsertable.of("name", ContentType.TEXT_PLAIN, name);
        else if (this.name.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set the type of the DataStream
     * @param type the entity type
     */
    public DataStreamUpsertBuilder setType(String type) throws Exception {
        if (this.type == null)
            this.type = Upsertable.of("type", ContentType.TEXT_PLAIN, type);
        else if (this.type.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set the DataStream metadata
     * @param metadata json metadata
     */
    public DataStreamUpsertBuilder setMetadata(Map<String, Object> metadata) throws Exception {
        if (this.metadata == null)
            this.metadata = Upsertable.of("metadata", ContentType.APPLICATION_JSON, metadata);
        else if (this.metadata.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set all the devices linked to the DataStream
     * @param devices a list of datastreams to link
     */
    public DataStreamUpsertBuilder setDevices(List<RegistryDeviceReference> devices) throws Exception {
        if (this.devices == null)
            this.devices = Upsertable.of("devices", ContentType.APPLICATION_JSON, devices);
        else if (this.devices.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set all pushSubscribers for the DataStream
     * @param pushSubscribers a list of push subscribers
     */
    public DataStreamUpsertBuilder setPushSubscribers(List<RegistryPushSubscriber> pushSubscribers) throws Exception {
        if (this.pushSubscribers == null)
            this.pushSubscribers = Upsertable.of("pushSubscribers", ContentType.APPLICATION_JSON, pushSubscribers);
        else if (this.pushSubscribers.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set the pull subscription for the DataStream
     * @param pullSubscription the pull subscription
     */
    public DataStreamUpsertBuilder setPullSubscription(RegistryPullSubscription pullSubscription) throws Exception {
        if (this.pullSubscription == null)
            this.pullSubscription = Upsertable.of("pullSubscription", ContentType.APPLICATION_JSON, pullSubscription);
        else if (this.pullSubscription.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set all the DNS records for the DataStream
     * @param dnsRecords a list of DNS records for the entity key
     */
    public DataStreamUpsertBuilder setDnsRecords(List<RegistryDNSRecord> dnsRecords) throws Exception {
        if (this.dnsRecords == null)
            this.dnsRecords = Upsertable.of("dnsRecords", ContentType.APPLICATION_JSON, dnsRecords);
        else if (this.dnsRecords.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set all the user permissions for the DataStream
     * @param userPermissions the map of username to permissions
     */
    public DataStreamUpsertBuilder setUserPermissions(Map<String, List<String>> userPermissions) throws Exception {
        if (this.userPermissions == null)
            this.userPermissions = Upsertable.of("userPermissions", ContentType.APPLICATION_JSON, userPermissions);
        else if (this.userPermissions.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set all the group permissions for the DataStream
     * @param groupPermissions the map of groupname to permissions
     */
    public DataStreamUpsertBuilder setGroupPermissions(Map<String, List<String>> groupPermissions) throws Exception {
        if (this.groupPermissions == null)
            this.groupPermissions = Upsertable.of("groupPermissions", ContentType.APPLICATION_JSON, groupPermissions);
        else if (this.groupPermissions.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Remove the name from the DataStream
     */
    public DataStreamUpsertBuilder removeName() throws Exception {
        if (this.name == null)
            this.name = Upsertable.remove("name", ContentType.TEXT_PLAIN, String::new);
        else if (!this.name.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    /**
     * Remove the type from the DataStream
     */
    public DataStreamUpsertBuilder removeType() throws Exception {
        if (this.type == null)
            this.type = Upsertable.remove("type", ContentType.TEXT_PLAIN, String::new);
        else if (!this.type.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    /**
     * Remove the metadata from the DataStream
     */
    public DataStreamUpsertBuilder removeMetadata() throws Exception {
        if (this.metadata == null)
            this.metadata = Upsertable.remove("objMap", ContentType.APPLICATION_JSON, HashMap::new);
        else if (!this.metadata.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    /**
     * Remove all the linked devices from the DataStream
     */
    public DataStreamUpsertBuilder removeDevices() throws Exception {
        if (this.devices == null)
            this.devices = Upsertable.remove("devices", ContentType.APPLICATION_JSON, ArrayList::new);
        else if (!this.devices.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    /**
     * Remove all the push subscribers from the DataStream
     */
    public DataStreamUpsertBuilder removePushSubscribers() throws Exception {
        if (this.pushSubscribers == null)
            this.pushSubscribers = Upsertable.remove("pushSubscribers", ContentType.APPLICATION_JSON, ArrayList::new);
        else if (!this.pushSubscribers.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    /**
     * Remove the pull subscription from the DataStream
     */
    public DataStreamUpsertBuilder removePullSubscription() throws Exception {
        if (this.pullSubscription == null)
            this.pullSubscription = Upsertable.remove("pullSubscription", ContentType.APPLICATION_JSON, HashMap::new);
        else if (!this.pullSubscription.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    /**
     * Remove the DNS records from the DataStream
     */
    public DataStreamUpsertBuilder removeDnsRecords() throws Exception {
        if (this.dnsRecords == null)
            this.dnsRecords = Upsertable.remove("dnsRecords", ContentType.APPLICATION_JSON, ArrayList::new);
        else if (!this.dnsRecords.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    /**
     * Remove all the group permissions from the entity
     */
    public DataStreamUpsertBuilder removeGroupPermissions() throws Exception {
        if (this.groupPermissions == null)
            this.groupPermissions = Upsertable.remove("groupPermissions", ContentType.APPLICATION_JSON, HashMap::new);
        else if (!this.groupPermissions.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    public Upsertable<?>[] build() {
        List<Upsertable<?>> toUpsert = new ArrayList<>();

        if (name != null)
            toUpsert.add(name);
        if (type != null)
            toUpsert.add(type);
        if (metadata != null)
            toUpsert.add(metadata);
        if (devices != null)
            toUpsert.add(devices);
        if (pushSubscribers != null)
            toUpsert.add(pushSubscribers);
        if (pullSubscription != null)
            toUpsert.add(pullSubscription);
        if (dnsRecords != null)
            toUpsert.add(dnsRecords);
        if (userPermissions != null)
            toUpsert.add(userPermissions);
        if (groupPermissions != null)
            toUpsert.add(groupPermissions);

        return toUpsert.toArray(new Upsertable[0]);
    }

    public static DataStreamUpsertBuilder create(String key) {
        return new DataStreamUpsertBuilder(key);
    }

}
