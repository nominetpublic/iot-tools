/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.registry.RegistrySession;

/**
 * Created by edoardo on 09/02/2017.
 */
public class RegistryAPISession {
    private static final int MAX_ATTEMPTS_DEFAULT = 3;
    private static final Logger LOG = LoggerFactory.getLogger(RegistryAPISession.class);
    public final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    private String registryURL;

    private String username;
    private String password;
    private int maxAttempts;

    private static final LoadingCache<SessionUserCache, RegistrySession>
            sessionLoadingCache = CacheBuilder.newBuilder()
                                              //.maximumSize(1)
                                              .expireAfterWrite(10, TimeUnit.MINUTES)
                                              .build(new CacheLoader<SessionUserCache, RegistrySession>() {
                                                  public RegistrySession load(SessionUserCache userCache) throws Exception {
                                                      return userCache.session.login(userCache.username, userCache.password);
                                                  }
                                              });

    public SessionAPI session;
    public DumpAPI dump;
    public InternalAPI internal;
    public SearchAPI search;
    public DataStreamAPI datastream;
    public PermissionsAPI permission;
    public DeviceAPI device;
    public UserAPI user;
    public TransformAPI transform;
    public GroupAPI groups;
    public EntityAPI entity;
    public ResourceAPI resource;
    public PushAPI push;


    class SessionUserCache {
        public final String username;
        public final String password;
        public final SessionAPI session;

        public SessionUserCache(String username, String password, SessionAPI session) {
            this.username = username;
            this.password = password;
            this.session = session;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o)
                return true;
            if (o == null || getClass() != o.getClass())
                return false;
            SessionUserCache that = (SessionUserCache) o;
            return Objects.equals(username, that.username);
        }

        @Override
        public int hashCode() {
            return Objects.hash(username);
        }
    }


    public RegistryAPISession(String registryURL, String username, String password, int maxAttempts) {
        this.registryURL = registryURL;
        session = new SessionAPI(this);
        dump = new DumpAPI(this);
        internal = new InternalAPI(this);
        search = new SearchAPI(this);
        datastream = new DataStreamAPI(this);
        permission = new PermissionsAPI(this);
        device = new DeviceAPI(this);
        user = new UserAPI(this);
        transform = new TransformAPI(this);
        groups = new GroupAPI(this);
        entity = new EntityAPI(this);
        resource = new ResourceAPI(this);
        push = new PushAPI(this);

        this.username = username;
        this.password = password;
        this.maxAttempts = maxAttempts;
    }

    public RegistryAPISession(String registryURL, String username, String password) {
        this(registryURL, username, password, MAX_ATTEMPTS_DEFAULT);
    }

    public String getRegistryURL() {
        return registryURL;
    }

    public String generateAPICall(String apiMethod) {
        return registryURL + apiMethod;
    }

    public RegistrySession getSession() throws Exception {
        try {
            return sessionLoadingCache.get(new SessionUserCache(username, password, session));
        } catch (ExecutionException e) {
            throw (Exception) e.getCause();
        }
    }

    public void close() throws Exception {
        sessionLoadingCache.invalidateAll();
        session.logout();
    }

    public void invalidateSessionKey() {
        sessionLoadingCache.invalidate(username);
    }

    public void invalidateCache() {
        sessionLoadingCache.invalidateAll();
    }

    public int getMaxAttempts() {
        return maxAttempts;
    }
}
