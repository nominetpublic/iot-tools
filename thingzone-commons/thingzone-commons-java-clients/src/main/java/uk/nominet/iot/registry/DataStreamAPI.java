/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import java.util.List;
import java.util.Optional;

import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import uk.nominet.iot.model.registry.RegistryDataStream;
import uk.nominet.iot.registry.utils.Upsertable;
import uk.nominet.iot.utils.APIOperation;
import uk.nominet.iot.utils.APIOperationHelper;

/**
 Created by edoardo on 09/02/2017.
 */
public class DataStreamAPI {
    RegistryAPISession api;

    public DataStreamAPI(RegistryAPISession registryAPISession) {
        this.api = registryAPISession;
    }

    /**
     * List all datastreams you have permissions to view
     * @return list of datastream keys
     */
    public List<String> list() throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/datastream/list"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Get(builder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();
        return api.jsonStringSerialiser
                  .getDefaultSerialiser()
                  .fromJson(responseDocument.get("dataStreams"), new TypeToken<List<String>>() {}.getType());
    }

    /**
     * Create a new datastream
     * @param dataStream an external key to use
     * @param name a name for the device
     * @param type the type category of the device
     * @return the key of the new datastream
     */
    public String create(Optional<String> dataStream, Optional<String> name, Optional<String> type) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/datastream/create"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                Request request = Request.Post(builder.build())
                                         .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey());

                Form form = Form.form();
                dataStream.ifPresent(s -> form.add("dataStream", s));
                name.ifPresent(s -> form.add("name", s));
                type.ifPresent(s -> form.add("type", s));
                request = request.bodyForm(form.build());

                return request.execute().handleResponse(new JsonResponseHandler());
            }
        });
        return apiOperationHelper.getResponseDocument().get("dataStream").getAsString();
    }

    /**
     * Get information about a datastream
     * @param dataStream a datastream key
     * @param fullDetails whether to show extra details
     * @param showPermissions whether to show the permissions
     * @return the datastream details
     */
    public RegistryDataStream get(String dataStream, boolean fullDetails, boolean showPermissions) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/datastream/get"));
        builder.setParameter("dataStream", dataStream);
        if (fullDetails)
            builder.setParameter("detail", "full");
        if (fullDetails && showPermissions)
            builder.setParameter("showPermissions", "true");

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Get(builder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();
        return api.jsonStringSerialiser.readObject(responseDocument.get("dataStream"), RegistryDataStream.class);
    }

    /**
     * Update the datastream
     * @param key a datastream key
     * @param metadata json metadata
     * @param name a name for the device
     * @param type the type category of the device
     */
    public void update(String key, JsonObject metadata, Optional<String> name, Optional<String> type) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/datastream/update"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create()
                                                          .addTextBody("dataStream", key)
                                                          .addTextBody("metadata", metadata.toString());

                if (name.isPresent()) entityBuilder = entityBuilder.addTextBody("name", name.get());
                if (type.isPresent()) entityBuilder = entityBuilder.addTextBody("type", type.get());

                return Request.Put(builder.build())
                              .body(entityBuilder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
    }

    /**
     * Delete the datastream
     * @param key a datastream key
     */
    public void delete(String key) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/datastream/delete"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                Form form = Form.form().add("dataStream", key);

                Request request = Request.Delete(builder.build())
                                         .bodyForm(form.build())
                                         .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey());
                return request.execute().handleResponse(new JsonResponseHandler());
            }
        });
    }

    /**
     * Upsert data to the datastream - this means that those parts that a specified will overwrite existing values in the datastream
     * @param key a datastream key
     * @param parts the parts of the datastream to upsert
     * @return the full datastream details with permissions
     */
    public RegistryDataStream upsert(String key, Upsertable<?>... parts) throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/datastream/upsert"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
                entityBuilder.addTextBody("dataStream", key);
                for (Upsertable<?> upsertable : parts) {
                   entityBuilder.addTextBody(upsertable.getName(), upsertable.getValue(), upsertable.getContentType());
                }

                return Request.Put(builder.build())
                              .body(entityBuilder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();
        return api.jsonStringSerialiser
                  .readObject(responseDocument.get("dataStream"), RegistryDataStream.class);
    }
}
