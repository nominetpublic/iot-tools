/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry.builders;

import com.github.filosganga.geogson.model.Geometry;
import org.apache.http.entity.ContentType;
import uk.nominet.iot.model.registry.RegistryDNSRecord;
import uk.nominet.iot.model.registry.RegistryStreamReference;
import uk.nominet.iot.registry.utils.Upsertable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeviceUpsertBuilder {
    public static final String REMOVAL_MSG = "The field is already marked for removal";
    public static final String INITIALISED_MSG = "The field has already been initialised";

    private Upsertable<String> name;
    private Upsertable<String> type;
    private Upsertable<Map<String, Object>> metadata;
    private Upsertable<Geometry<?>> location;
    private Upsertable<List<RegistryStreamReference>> dataStreams;
    private Upsertable<List<RegistryDNSRecord>> dnsRecords;
    private Upsertable<Map<String, List<String>>> userPermissions;
    private Upsertable<Map<String, List<String>>> groupPermissions;

    public DeviceUpsertBuilder(String device) {
    }

    /**
     * Set the name of the device
     * @param name the entity name
     */
    public DeviceUpsertBuilder setName(String name) throws Exception {
        if (this.name == null)
            this.name = Upsertable.of("name", ContentType.TEXT_PLAIN, name);
        else if (this.name.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set the type of the device
     * @param type the entity type
     */
    public DeviceUpsertBuilder setType(String type) throws Exception {
        if (this.type == null)
            this.type = Upsertable.of("type", ContentType.TEXT_PLAIN, type);
        else if (this.type.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set the device metadata
     * @param metadata json metadata
     */
    public DeviceUpsertBuilder setMetadata(Map<String, Object> metadata) throws Exception {
        if (this.metadata == null)
            this.metadata = Upsertable.of("metadata", ContentType.APPLICATION_JSON, metadata);
        else if (this.metadata.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set the device location
     * @param location the location geometry
     */
    public DeviceUpsertBuilder setLocation(Geometry<?> location) throws Exception {
        if (this.location == null)
            this.location = Upsertable.of("location", ContentType.APPLICATION_JSON, location);
        else if (this.location.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set all the datastreams linked to the device
     * @param dataStreams a list of datastreams to link
     */
    public DeviceUpsertBuilder setDataStreams(List<RegistryStreamReference> dataStreams) throws Exception {
        if (this.dataStreams == null)
            this.dataStreams = Upsertable.of("dataStreams", ContentType.APPLICATION_JSON, dataStreams);
        else if (this.dataStreams.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set all the DNS records for the device
     * @param dnsRecords a list of DNS records for the entity key
     */
    public DeviceUpsertBuilder setDnsRecords(List<RegistryDNSRecord> dnsRecords) throws Exception {
        if (this.dnsRecords == null)
            this.dnsRecords = Upsertable.of("dnsRecords", ContentType.APPLICATION_JSON, dnsRecords);
        else if (this.dnsRecords.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set all the user permissions for the device
     * @param userPermissions the map of username to permissions
     */
    public DeviceUpsertBuilder setUserPermissions(Map<String, List<String>> userPermissions) throws Exception {
        if (this.userPermissions == null)
            this.userPermissions = Upsertable.of("userPermissions", ContentType.APPLICATION_JSON, userPermissions);
        else if (this.userPermissions.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Set all the group permissions for the device
     * @param groupPermissions the map of groupname to permissions
     */
    public DeviceUpsertBuilder setGroupPermissions(Map<String, List<String>> groupPermissions) throws Exception {
        if (this.groupPermissions == null)
            this.groupPermissions = Upsertable.of("groupPermissions", ContentType.APPLICATION_JSON, groupPermissions);
        else if (this.groupPermissions.toRemove())
            throw new Exception(REMOVAL_MSG);

        return this;
    }

    /**
     * Remove the name from the device
     */
    public DeviceUpsertBuilder removeName() throws Exception {
        if (this.name == null)
            this.name = Upsertable.remove("name", ContentType.TEXT_PLAIN, String::new);
        else if (!this.name.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    /**
     * Remove the type from the device
     */
    public DeviceUpsertBuilder removeType() throws Exception {
        if (this.type == null)
            this.type = Upsertable.remove("type", ContentType.TEXT_PLAIN, String::new);
        else if (!this.type.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    /**
     * Remove the metadata from the device
     */
    public DeviceUpsertBuilder removeMetadata() throws Exception {
        if (this.metadata == null)
            this.metadata = Upsertable.remove("objMap", ContentType.APPLICATION_JSON, HashMap::new);
        else if (!this.metadata.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    /**
     * Remove the location from the device
     */
    public DeviceUpsertBuilder removeLocation() throws Exception {
        if (this.location == null)
            this.location = Upsertable.remove("location", ContentType.APPLICATION_JSON, HashMap::new);
        else if (!this.location.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    /**
     * Remove all the linked datastreams from the device
     */
    public DeviceUpsertBuilder removeDataStreams() throws Exception {
        if (this.dataStreams == null)
            this.dataStreams = Upsertable.remove("dataStreams", ContentType.APPLICATION_JSON, ArrayList::new);
        else if (!this.dataStreams.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    /**
     * Remove the DNS records from the device
     */
    public DeviceUpsertBuilder removeDnsRecords() throws Exception {
        if (this.dnsRecords == null)
            this.dnsRecords = Upsertable.remove("dnsRecords", ContentType.APPLICATION_JSON, ArrayList::new);
        else if (!this.dnsRecords.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    /**
     * Remove all the group permissions from the entity
     */
    public DeviceUpsertBuilder removeGroupPermissions() throws Exception {
        if (this.groupPermissions == null)
            this.groupPermissions = Upsertable.remove("groupPermissions", ContentType.APPLICATION_JSON, HashMap::new);
        else if (!this.groupPermissions.toRemove())
            throw new Exception(INITIALISED_MSG);

        return this;
    }

    public Upsertable<?>[] build() {
        List<Upsertable<?>> toUpsert = new ArrayList<>();
        if (name != null)
            toUpsert.add(name);
        if (type != null)
            toUpsert.add(type);
        if (metadata != null)
            toUpsert.add(metadata);
        if (location != null)
            toUpsert.add(location);
        if (dataStreams != null)
            toUpsert.add(dataStreams);
        if (dnsRecords != null)
            toUpsert.add(dnsRecords);
        if (userPermissions != null)
            toUpsert.add(userPermissions);
        if (groupPermissions != null)
            toUpsert.add(groupPermissions);
        return toUpsert.toArray(new Upsertable[0]);
    }

    public static DeviceUpsertBuilder create(String key) {
        return new DeviceUpsertBuilder(key);
    }

}
