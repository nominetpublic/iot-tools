/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry.builders;

import java.util.ArrayList;
import java.util.List;
import uk.nominet.iot.model.registry.RegistryTransformSource;

@SuppressWarnings("unused")
public class TransformSourcesBuilder {
    List<RegistryTransformSource> sources;

    public TransformSourcesBuilder() {
        sources = new ArrayList<>();
    }

    /**
     * Add a transform source
     * @param stream the key of the datastream
     * @param alias the alias for this datastream
     * @param trigger the trigger value
     * @param aggregationType the content type of the aggregation function
     * @param aggregationSpec the aggregation function
     */
    public TransformSourcesBuilder addTransformSource(String stream,
                                                      String alias,
                                                      boolean trigger,
                                                      String aggregationType,
                                                      String aggregationSpec) throws Exception {
        RegistryTransformSource source = new RegistryTransformSource(stream, alias, trigger, aggregationType, aggregationSpec);

        if (sources.contains(source))
            throw new Exception("The reference already exists");
        sources.add(source);
        return this;
    }

    public List<RegistryTransformSource> build() {
        return sources;
    }

    public static TransformSourcesBuilder create() {
        return new TransformSourcesBuilder();
    }
}
