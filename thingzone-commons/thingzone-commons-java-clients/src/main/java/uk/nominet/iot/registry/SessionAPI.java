/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import java.io.IOException;

import org.apache.http.client.HttpResponseException;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;

import com.google.gson.JsonObject;

import uk.nominet.iot.model.registry.RegistrySession;
import uk.nominet.iot.utils.APIOperation;
import uk.nominet.iot.utils.APIOperationHelper;

/**
 Created by edoardo on 09/02/2017.
 */
public class SessionAPI {
    RegistryAPISession api;

    /**
     * @param registryAPISession api session
     */
    public SessionAPI(RegistryAPISession registryAPISession) {
        this.api = registryAPISession;
    }

    /**
     * Call the login api
     * @param username the username to login
     * @param password the password for the username
     * @return the json response with the  new session details
     */
    private JsonObject loginAPICall(String username, String password) throws IOException {
        return Request.Post(api.generateAPICall("/api/session/login"))
                      .bodyForm(Form.form().add("username", username).add("password", password).build())
                      .execute()
                      .handleResponse(new JsonResponseHandler());
    }

    /**
     * Login a user
     * @param username the username to login
     * @param password the password for the username
     * @return a session
     */
    public RegistrySession login(String username, String password) throws Exception {
        JsonObject responseDocument = null;
        try {
            responseDocument = loginAPICall(username, password);
        } catch (HttpResponseException ex) {
            // ?
        }

        if (responseDocument == null || !responseDocument.has("sessionKey")) {
            throw new Exception("Invalid response from server, expecting sessionKey");
        }

        return api.jsonStringSerialiser.readObject(responseDocument.toString(), RegistrySession.class);
    }

    /**
     *
     * @return the time to live
     */
    public String keepalive() throws Exception {
        URIBuilder builder = new URIBuilder(api.generateAPICall("/api/session/keepalive"));

        APIOperationHelper apiOperationHelper = new APIOperationHelper();
        apiOperationHelper.doWithRetry(api.getMaxAttempts(), new APIOperation(api) {
            @Override
            public JsonObject invoke() throws Exception {
                return Request.Post(builder.build())
                              .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
                              .execute()
                              .handleResponse(new JsonResponseHandler());
            }
        });
        JsonObject responseDocument = apiOperationHelper.getResponseDocument();
        return responseDocument.get("ttl").getAsString();
    }

    /**
     * Logout the current session
     */
    public void logout() throws Exception {
        Request.Post(api.generateAPICall("/api/session/logout"))
               .addHeader("X-FollyBridge-SessionKey", api.getSession().getSessionKey())
               .execute()
               .handleResponse(new JsonResponseHandler());

        api.getSession().setSessionKey(null);
        api.getSession().setUser(null);
    }
}
