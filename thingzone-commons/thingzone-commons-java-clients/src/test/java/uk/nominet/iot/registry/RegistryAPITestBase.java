/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.notMatching;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Rule;

import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.extension.responsetemplating.ResponseTemplateTransformer;
import com.github.tomakehurst.wiremock.junit.WireMockRule;

import uk.nominet.iot.registry.exceptions.RegistryFailResponseException;
import uk.nominet.iot.utils.APIFunction;

public class RegistryAPITestBase {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(wireMockConfig().dynamicPort()
                                                                        .extensions(new ResponseTemplateTransformer(false)));

    private ArrayList<String> requestStack = new ArrayList<>();

    public ResponseDefinitionBuilder getFailResponse(String method, String path, String reason) {
        return aResponse().withHeader("Content-Type", "application/json")
                          .withStatus(400)
                          .withBody(String.format("{\n" + "  \"result\": \"fail\",\n" + "  \"reason\": \"%s\"\n" + "}",
                                                  reason, method, path))
                          .withTransformers("response-template");

    }

    public ResponseDefinitionBuilder getSessionFailResponse(String method, String path) {
        return getUnauthorisedResponse(method, path);
    }

    public ResponseDefinitionBuilder getUnauthorisedResponse(String method, String path) {
        return aResponse()
                .withHeader("Content-Type", "application/json")
                .withStatus(401)
                .withBody(String.format("{\n" +
                        "  \"result\": \"fail\",\n" +
                        "  \"reason\": \"session {{request.headers.X-FollyBridge-SessionKey}} not found\"\n" +
                        "}", method, path))
                .withTransformers("response-template");
    }

    public ResponseDefinitionBuilder getForbiddenResponse(String method, String path) {
        return aResponse()
                .withHeader("Content-Type", "application/json")
                .withStatus(403)
                .withBody(String.format("{\n" +
                        "  \"result\": \"fail\",\n" +
                        "  \"reason\": \"authentication error\"\n" +
                        "}", method, path))
                .withTransformers("response-template");
    }

    @Before
    public void setUp() throws Exception {

        requestStack = new ArrayList<>();

        wireMockRule.addMockServiceRequestListener((request, response) -> {
            System.out.println("Request: " + request.getAbsoluteUrl());
            System.out.println("Method: " + request.getMethod());
            System.out.println("Body: " + request.getBodyAsString());
            System.out.println("Headers: " + request.getHeaders());

            requestStack.add(request.getAbsoluteUrl());
        });

        stubFor(post(urlEqualTo("/api/session/login"))
                .withRequestBody(matching(".*username=test.*"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "    \"result\": \"ok\",\n" +
                                "    \"sessionKey\": \"XU2OBHoFm-Uc0uk0g3ttCA\",\n" +
                                "    \"ttl\": 899998,\n" +
                                "    \"user\": {\n" +
                                "        \"username\": \"test\",\n" +
                                "        \"details\": {\"foo\":\"bar\"},\n" +
                                "        \"preferences\": {\"bar\":\"foo\"}\n" +
                                "    }\n" +
                                "}")
                )
        );

        stubFor(post(urlEqualTo("/api/session/login"))
                .withRequestBody(matching(".*username=test1.*"))
                .willReturn(getFailResponse("POST", "/session/login", "authentication error"))
        );

        stubFor(post(urlEqualTo("/api/session/keepalive"))
                .withHeader("X-FollyBridge-SessionKey", matching("XU2OBHoFm-Uc0uk0g3ttCA"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "  \"ttl\": 900000,\n" +
                                "  \"result\": \"ok\"\n" +
                                "}")
                )
        );

        stubFor(post(urlEqualTo("/api/session/keepalive"))
                .withHeader("X-FollyBridge-SessionKey", notMatching("XU2OBHoFm-Uc0uk0g3ttCA"))
                .willReturn(getSessionFailResponse("POST", "/session/keepalive"))
        );

        stubFor(post(urlEqualTo("/api/session/logout"))
                .withHeader("X-FollyBridge-SessionKey", matching("XU2OBHoFm-Uc0uk0g3ttCA"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "  \"result\": \"ok\"\n" +
                                "}")
                )
        );

        stubFor(post(urlEqualTo("/api/session/logout"))
                .withHeader("X-FollyBridge-SessionKey", notMatching("XU2OBHoFm-Uc0uk0g3ttCA"))
                .willReturn(getSessionFailResponse("POST", "/session/keepalive"))
        );

    }

    public static void assertInvalidSession(APIFunction function) {
        try {
            function.apply();
            fail();
        } catch (RegistryFailResponseException ex) {
            assertTrue(ex.isSessionFailure());
            assertEquals(401, ex.getStatusCode());
        } catch (Exception e) {
            fail();
        }
    }

    public static void assertAuthError(APIFunction function) {
        try {
            function.apply();
            fail();
        } catch (RegistryFailResponseException ex) {
            assertEquals(401, ex.getStatusCode());
        } catch (Exception e) {
            fail();
        }
    }

    public static void assertPermissionError(APIFunction function) {
        try {
            function.apply();
            fail();
        } catch (RegistryFailResponseException ex) {
            assertEquals(403, ex.getStatusCode());
        } catch (Exception e) {
            fail();
        }
    }

    public static void assertBadRequest(APIFunction function) {
        try {
            function.apply();
            fail();
        } catch (RegistryFailResponseException ex) {
            assertTrue(!ex.isSessionFailure());
            assertEquals(400, ex.getStatusCode());
        } catch (Exception e) {
            fail();
        }
    }

}
