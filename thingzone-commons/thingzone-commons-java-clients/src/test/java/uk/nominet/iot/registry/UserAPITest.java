/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import uk.nominet.iot.common.Taxonomy;
import uk.nominet.iot.model.registry.RegistryUser;

import java.util.Optional;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.*;

public class UserAPITest extends RegistryAPITestBase {
    RegistryAPISession api;

    String[] privileges;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        api = new RegistryAPISession(wireMockRule.url("/"), "test", "test", 1);
        api.invalidateCache();
        privileges = new String[] { Taxonomy.PRIVILEGE_CREATE_USER, Taxonomy.PRIVILEGE_CREATE_DEVICE };
    }

    @Test
    public void createUser() throws Exception {
        stubFor(post(urlEqualTo("/api/user/create"))
                .withHeader("X-FollyBridge-SessionKey", matching("XU2OBHoFm-Uc0uk0g3ttCA"))
                .withRequestBody(matching(".*username=user1.*"))
                .withRequestBody(matching(".*password=password.*"))
                .withRequestBody(matching(".*privileges=CREATE_USER\\%2CCREATE_DEVICE.*"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "  \"result\": \"ok\"\n" +
                                "}")
                )
        );
        api.user.create("user1", "password", privileges);
    }

    @Test
    public void createUserInvalidSession() throws Exception {
        stubFor(post(urlEqualTo("/api/user/create"))
                .withHeader("X-FollyBridge-SessionKey", matching("invalidsessionkey"))
                .withRequestBody(matching(".*username=user1.*"))
                .withRequestBody(matching(".*password=password.*"))
                .withRequestBody(matching(".*privileges=CREATE_USER\\%2CCREATE_DEVICE.*"))
                .willReturn(getSessionFailResponse("POST", "/api/user/create"))
        );
        api.getSession().setSessionKey("invalidsessionkey");

        assertInvalidSession(() -> api.user.create("user1", "password", privileges));
    }

    @Test
    public void createUserNoPrivileges() throws Exception {
        stubFor(post(urlEqualTo("/api/user/create"))
                .withHeader("X-FollyBridge-SessionKey", matching("sessionNoPrivileges"))
                .withRequestBody(matching(".*username=user1.*"))
                .withRequestBody(matching(".*password=password.*"))
                .withRequestBody(matching(".*privileges=CREATE_USER\\%2CCREATE_DEVICE.*"))
                .willReturn(getUnauthorisedResponse("POST","/user/create"))
        );
        api.getSession().setSessionKey("sessionNoPrivileges");

        assertAuthError(() -> api.user.create("user1", "password", privileges));
    }

    @Test
    public void changePassword() throws Exception {
        stubFor(put(urlEqualTo("/api/user/change_password"))
                .withHeader("X-FollyBridge-SessionKey", matching("XU2OBHoFm-Uc0uk0g3ttCA"))
                .withRequestBody(matching(".*existingPassword=password1.*"))
                .withRequestBody(matching(".*newPassword=password2.*"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "  \"result\": \"ok\"\n" +
                                "}")
                )
        );
        api.user.changePassword("password1", "password2");
    }

    @Test
    public void changePasswordInvalidSession() throws Exception {
        stubFor(put(urlEqualTo("/api/user/change_password"))
                .withHeader("X-FollyBridge-SessionKey", matching("invalidsessionkey"))
                .withRequestBody(matching(".*existingPassword=password1.*"))
                .withRequestBody(matching(".*newPassword=password2.*"))
                .willReturn(getSessionFailResponse("PUT", "/api/user/change_password"))
        );
        api.getSession().setSessionKey("invalidsessionkey");

        assertInvalidSession( () -> api.user.changePassword("password1", "password2"));
    }

    @Test
    public void userGet() throws Exception {
        stubFor(get(urlEqualTo("/api/user/get?username=user1"))
                .withHeader("X-FollyBridge-SessionKey", matching("XU2OBHoFm-Uc0uk0g3ttCA"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "  \"result\": \"ok\",\n" +
                                "  \"user\": {\n" +
                                "    \"username\": \"user1\",\n" +
                                "    \"email\": \"email@example.com\",\n" +
                                "    \"details\": {\"foo\":\"bar\"},\n" +
                                "    \"preferences\": {\"bar\":\"foo\"},\n" +
                                "    \"privileges\": [\n" +
                                "      \"USE_SESSION\",\n" +
                                "      \"QUERY_STREAMS\"\n" +
                                "    ]\n" +
                                "  }\n" +
                                "}")
                )
        );
        RegistryUser user = api.user.get("user1");
        assertEquals("user1", user.getUsername());
        assertEquals("bar", api.getSession().getUser().getDetails().get("foo"));
        assertEquals("foo", api.getSession().getUser().getPreferences().get("bar"));
        assertThat(user.getPrivileges(), CoreMatchers.hasItems("USE_SESSION", "QUERY_STREAMS"));
    }

    @Test
    public void userGetInvalidSession() throws Exception {
        stubFor(get(urlEqualTo("/api/user/get?username=user1"))
                .withHeader("X-FollyBridge-SessionKey", matching("invalidsessionkey"))
                .willReturn(getSessionFailResponse("GET", "/api/user/get"))
        );
        api.getSession().setSessionKey("invalidsessionkey");

        assertInvalidSession( () -> api.user.get("user1"));
    }

    @Test
    public void userGetNoPrivileges() throws Exception {
        stubFor(get(urlEqualTo("/api/user/get?username=user1"))
                .withHeader("X-FollyBridge-SessionKey", matching("sessionNoPrivileges"))
                .willReturn(getForbiddenResponse("GET", "/api/user/get"))
        );
        api.getSession().setSessionKey("sessionNoPrivileges");

        assertPermissionError(() -> api.user.get("user1"));
    }

    @Test
    public void userUpdate() throws Exception {
        stubFor(put(urlEqualTo("/api/user/update"))
                .withHeader("X-FollyBridge-SessionKey", matching("XU2OBHoFm-Uc0uk0g3ttCA"))
                .withMultipartRequestBody(
                        aMultipart()
                                .withName("preferences")
                                .withHeader("Content-Type", containing("charset"))
                                .withBody(equalToJson("{\"foo\":\"bar\"}"))
                )
                .withMultipartRequestBody(
                        aMultipart()
                                .withName("details")
                                .withHeader("Content-Type", containing("charset"))
                                .withBody(equalToJson("{\"bar\":\"foo\"}"))
                )
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "  \"result\": \"ok\"\n" +
                                "}")
                )
        );
        JsonParser parser = new JsonParser();

        JsonObject prefrences = parser.parse("{\"foo\":\"bar\"}").getAsJsonObject();
        JsonObject details = parser.parse("{\"bar\":\"foo\"}").getAsJsonObject();

        api.user.update(Optional.of(prefrences), Optional.of(details));
    }

    @Test
    public void userUpdateInvalidSession() throws Exception {
        stubFor(put(urlEqualTo("/api/user/update"))
                .withHeader("X-FollyBridge-SessionKey", matching("invalidsessionkey"))
                .withMultipartRequestBody(
                        aMultipart()
                                .withName("preferences")
                                .withHeader("Content-Type", containing("charset"))
                                .withBody(equalToJson("{\"foo\":\"bar\"}"))
                )
                .withMultipartRequestBody(
                        aMultipart()
                                .withName("details")
                                .withHeader("Content-Type", containing("charset"))
                                .withBody(equalToJson("{\"bar\":\"foo\"}"))
                )
                .willReturn(getSessionFailResponse("PUT", "/user/update"))
        );
        api.getSession().setSessionKey("invalidsessionkey");

        JsonParser parser = new JsonParser();

        JsonObject prefrences = parser.parse("{\"foo\":\"bar\"}").getAsJsonObject();
        JsonObject details = parser.parse("{\"bar\":\"foo\"}").getAsJsonObject();

        assertInvalidSession(() -> api.user.update(Optional.of(prefrences), Optional.of(details)));
    }

    @Test
    public void setPrivileges() throws Exception {
        stubFor(put(urlEqualTo("/api/user/set_privileges"))
                .withHeader("X-FollyBridge-SessionKey", matching("XU2OBHoFm-Uc0uk0g3ttCA"))
                .withRequestBody(matching(".*username=user1.*"))
                .withRequestBody(matching(".*privileges=CREATE_USER\\%2CCREATE_DEVICE.*"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "  \"result\": \"ok\"\n" +
                                "}")
                )
        );
        api.user.setPrivileges("user1", privileges);
    }

    @Test
    public void setPrivilegesInvalidSession() throws Exception {
        stubFor(put(urlEqualTo("/api/user/set_privileges"))
                .withHeader("X-FollyBridge-SessionKey", matching("invalidsessionkey"))
                .withRequestBody(matching(".*username=user1.*"))
                .withRequestBody(matching(".*privileges=CREATE_USER\\%2CCREATE_DEVICE.*"))
                .willReturn(getSessionFailResponse("PUT", "/user/set_privileges"))
        );
        api.getSession().setSessionKey("invalidsessionkey");

        assertInvalidSession(() -> api.user.setPrivileges("user1", privileges));
    }

    @Test
    public void setPrivilegesNoPrivileges() throws Exception {
        stubFor(put(urlEqualTo("/api/user/set_privileges"))
                .withHeader("X-FollyBridge-SessionKey", matching("sessionNoPrivileges"))
                .withRequestBody(matching(".*username=user1.*"))
                .withRequestBody(matching(".*privileges=CREATE_USER\\%2CCREATE_DEVICE.*"))
                .willReturn(getUnauthorisedResponse("PUT", "/user/set_privileges"))
        );
        api.getSession().setSessionKey("sessionNoPrivileges");

        assertAuthError(() -> api.user.setPrivileges("user1", privileges));
    }

    @Test
    public void requestUserToken() throws Exception {
        stubFor(get(urlEqualTo("/api/user/request_token?username=user1"))
                .withHeader("X-FollyBridge-SessionKey", matching("XU2OBHoFm-Uc0uk0g3ttCA"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody(" {\n" +
                                "  \"result\": \"ok\",\n" +
                                "  \"token\": \"2DacXDEuJTIBbvYl2QUF2dIWxJgS9pcofwtH0bICHEK7I32KZWUuOtYSO2Cmvzuw\"\n" +
                                "}")
                )
        );
        String token = api.user.requestToken("user1", Optional.empty());
        assertEquals("2DacXDEuJTIBbvYl2QUF2dIWxJgS9pcofwtH0bICHEK7I32KZWUuOtYSO2Cmvzuw", token);
    }

    @Test
    public void requestUserTokenWithExpiry() throws Exception {
        stubFor(get(urlEqualTo("/api/user/request_token?username=user1&expiry=10"))
                .withHeader("X-FollyBridge-SessionKey", matching("XU2OBHoFm-Uc0uk0g3ttCA"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody(" {\n" +
                                "  \"result\": \"ok\",\n" +
                                "  \"token\": \"2DacXDEuJTIBbvYl2QUF2dIWxJgS9pcofwtH0bICHEK7I32KZWUuOtYSO2Cmvzuw\"\n" +
                                "}")
                )
        );
        String token = api.user.requestToken("user1", Optional.of(10));
        assertEquals("2DacXDEuJTIBbvYl2QUF2dIWxJgS9pcofwtH0bICHEK7I32KZWUuOtYSO2Cmvzuw", token);
    }

    @Test
    public void requestUserInvalidSession() throws Exception {
        stubFor(get(urlEqualTo("/api/user/request_token?username=user1"))
                .withHeader("X-FollyBridge-SessionKey", matching("invalidsessionkey"))
                .willReturn(getSessionFailResponse("PUT", "/user/request_token"))
        );
        api.getSession().setSessionKey("invalidsessionkey");

        assertInvalidSession(() ->api.user.requestToken("user1", Optional.empty()));
    }

    @Test
    public void setPassword() throws Exception {
        stubFor(put(urlEqualTo("/api/user/set_password?username=user1&token=2DacXDEu&newPassword=npw"))
                .withHeader("X-FollyBridge-SessionKey", matching("XU2OBHoFm-Uc0uk0g3ttCA"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody(" {\n" +
                                "  \"result\": \"ok\",\n" +
                                "  \"token\": \"2DacXDEuJTIBbvYl2QUF2dIWxJgS9pcofwtH0bICHEK7I32KZWUuOtYSO2Cmvzuw\"\n" +
                                "}")
                )
       );
       api.user.setPassword("user1", "2DacXDEu", "npw");
    }

    @Test
    public void setPasswordInvalidSession() throws Exception {
        stubFor(put(urlEqualTo("/api/user/set_password?username=user1&token=2DacXDEu&newPassword=npw"))
                .withHeader("X-FollyBridge-SessionKey", matching("invalidsessionkey"))
                .willReturn(getSessionFailResponse("PUT", "/user/request_token"))
        );
        api.getSession().setSessionKey("invalidsessionkey");

        assertInvalidSession(() -> api.user.setPassword("user1", "2DacXDEu", "npw"));
    }

    @Test
    public void setPasswordError() throws Exception {
        stubFor(put(urlEqualTo("/api/user/set_password?username=user1&token=2DacXDEu&newPassword=npw"))
                .withHeader("X-FollyBridge-SessionKey", matching("XU2OBHoFm-Uc0uk0g3ttCA"))
                .willReturn(getFailResponse("PUT", "/user/request_token", "some reason"))
        );
        api.getSession().setSessionKey("XU2OBHoFm-Uc0uk0g3ttCA");

        assertBadRequest(() -> api.user.setPassword("user1", "2DacXDEu", "npw"));
    }

}
