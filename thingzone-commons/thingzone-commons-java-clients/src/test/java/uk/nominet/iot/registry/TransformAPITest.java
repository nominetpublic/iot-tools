/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import uk.nominet.iot.model.registry.RegistryTransform;
import uk.nominet.iot.registry.exceptions.RegistryFailResponseException;

public class TransformAPITest extends RegistryAPITestBase {
    RegistryAPISession api;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        api = new RegistryAPISession(wireMockRule.url("/"), "test", "test", 1);
    }

    @Test
    public void getTransform() throws Exception {
        stubFor(get(urlEqualTo("/api/transform/get?transform=transform.id&detail=full&showPermissions=true"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "    \"result\": \"ok\",\n" +
                                "    \"transform\": {\n" +
                                "        \"key\": \"transform.id\",\n" +
                                "        \"metadata\": {\n" +
                                "            \"name\": \"name\",\n" +
                                "            \"unit\": \"unit\"\n" +
                                "        },\n" +
                                "        \"function\": {\n" +
                                "           \"name\": \"logFunction\",\n" +
                                "           \"isPublic\": \"false\"\n" +
                                "        },\n" +
                                "        \"sources\": [\n" +
                                "           {\n" +
                                "               \"alias\": \"transform source\",\n" +
                                "               \"trigger\": \"false\",\n" +
                                "               \"source\": \"cale-gvvn-2czt-adfa-g76z-2oja.z.g\",\n" +
                                "               \"aggregation\": {\n" +
                                "                   \"aggregationType\": \"type\",\n" +
                                "                   \"aggregationSpec\": \"spec\"\n" +
                                "               }\n" +
                                "           }\n" +
                                "       ],\n" +
                                "       \"outputStreams\": [\n" +
                                "           {\n" +
                                "               \"alias\": \"transform source\",\n" +
                                "               \"key\": \"cale-gvvn-2czt-adfa-g76z-2oja.z.g\"\n" +
                                "           }\n" +
                                "       ],\n" +
                                "       \"pushSubscribers\" : [\n" +
                                "           {\n" +
                                "               \"uri\" : \"https://en.wikipedia.org/\",\n" +
                                "               \"uriParams\" : {\n" +
                                "                 \"foo\" : \"bar\"\n" +
                                "               }\n" +
                                "           }\n" +
                                "        ],\n" +
                                "        \"userPermissions\": {\n" +
                                "            \"test\": [\n" +
                                "                \"ADMIN\",\n" +
                                "                \"UPLOAD\",\n" +
                                "                \"CONSUME\",\n" +
                                "                \"MODIFY\",\n" +
                                "                \"ANNOTATE\",\n" +
                                "                \"DISCOVER\"\n" +
                                "           ],\n" +
                                "           \"test1\": [\n" +
                                "                \"ADMIN\",\n" +
                                "                \"UPLOAD\",\n" +
                                "                \"CONSUME\",\n" +
                                "                \"MODIFY\",\n" +
                                "                \"ANNOTATE\",\n" +
                                "                \"DISCOVER\"\n" +
                                "           ]\n" +
                                "        },\n" +
                                "        \"groupPermissions\": {\n" +
                                "            \"group_1\": [\n" +
                                "                \"ADMIN\",\n" +
                                "                \"CONSUME\",\n" +
                                "                \"DISCOVER\",\n" +
                                "                \"MODIFY\"\n" +
                                "            ],\n" +
                                "            \"group_2\": [\n" +
                                "                \"DISCOVER\"\n" +
                                "            ]\n" +
                                "        }\n" +
                                "    }\n" +
                                "}")));
        RegistryTransform transform = api.transform.get("transform.id", true, true);
        assertEquals("transform.id", transform.getKey());
    }

    @Test (expected = RegistryFailResponseException.class)
    public void getUnknownTransform() throws Exception {
        stubFor(get(urlEqualTo("/api/transform/get?transform=transform.id.unknown"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(404)
                        .withBody("{\n" +
                                "    \"result\": \"fail\",\n" +
                                "    \"reason\": \"Transform not found: transform.id.unknown\"\n" +
                                "}")));

        api.transform.get("transform.id.unknown", false, false);
    }
}
