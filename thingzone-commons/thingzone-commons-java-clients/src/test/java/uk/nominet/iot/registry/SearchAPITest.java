/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import uk.nominet.iot.model.registry.RegistryDataStream;
import uk.nominet.iot.model.registry.RegistryDevice;
import uk.nominet.iot.model.registry.RegistryTransform;

public class SearchAPITest extends RegistryAPITestBase {
    RegistryAPISession api;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        api = new RegistryAPISession(wireMockRule.url("/"), "test", "test", 1);
    }

    @Test
    public void identifyDevice() throws Exception {
        stubFor(get(urlEqualTo("/api/search/identify?key=123456789"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "    \"result\": \"ok\",\n" +
                                "    \"device\": {\n" +
                                "        \"key\": \"123456789\",\n" +
                                "        \"metadata\": {\n" +
                                "            \"name\": \"123456789\",\n" +
                                "            \"DevEUI\": \"123456789\"\n" +
                                "        },\n" +
                                "        \"dataStreams\": [\n" +
                                "            { \"key\": \"123456789.stream1\" },\n" +
                                "            { \"key\": \"123456789.stream2\" }\n" +
                                "        ]\n" +
                                "    }\n" +
                                "}")
                )
        );

        stubFor(get(urlEqualTo("/api/datastream/get?dataStream=123456789.stream1"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "    \"result\": \"ok\",\n" +
                                "    \"dataStream\": {\n" +
                                "        \"key\": \"123456789.stream1\",\n" +
                                "        \"metadata\": {\n" +
                                "            \"name\": \"Stream 1\",\n" +
                                "            \"unit\": \"U\",\n" +
                                "            \"content_type\": \"application/json\"\n" +
                                "        },\n" +
                                "        \"isTransform\": false\n" +
                                "    }\n" +
                                "}")
                )
        );

        stubFor(get(urlEqualTo("/api/datastream/get?dataStream=123456789.stream2"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "    \"result\": \"ok\",\n" +
                                "    \"dataStream\": {\n" +
                                "        \"key\": \"123456789.stream2\",\n" +
                                "        \"metadata\": {\n" +
                                "            \"name\": \"Stream 2\",\n" +
                                "            \"unit\": \"U\",\n" +
                                "            \"content_type\": \"application/json\"\n" +
                                "        },\n" +
                                "        \"isTransform\": false\n" +
                                "    }\n" +
                                "}")
                )
        );
        Object thing = api.search.identify("123456789", false, false);
        System.out.println("Thing: " + thing);
        assertEquals(RegistryDevice.class.getName(), thing.getClass().getName());

        RegistryDevice device = (RegistryDevice)thing;

        System.out.println("Device: " + device);

        assertEquals("123456789", device.getKey());
        assertEquals(2, device.getDataStreams().size());
    }

    @Test
    public void identifyStream() throws Exception {
        stubFor(get(urlEqualTo("/api/search/identify?key=123456789.stream1"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "    \"result\": \"ok\",\n" +
                                "    \"dataStream\": {\n" +
                                "        \"key\": \"123456789.stream1\",\n" +
                                "        \"metadata\": {\n" +
                                "            \"name\": \"Stream 1\",\n" +
                                "            \"unit\": \"U\", \n" +
                                "            \"content_type\": \"application/json\" \n" +
                                "        },\n" +
                                "        \"isTransform\": false,\n" +
                                "        \"dnsRecords\": []\n" +
                                "    }\n" +
                                "}")));
        Object thing = api.search.identify("123456789.stream1", false, false);
        System.out.println("Thing: " + thing);
        assertEquals(RegistryDataStream.class.getName(), thing.getClass().getName());

        RegistryDataStream stream = (RegistryDataStream)thing;
        assertEquals("123456789.stream1", stream.getKey());
    }

    @Test
    public void identifyTransform() throws Exception {
        stubFor(get(urlEqualTo("/api/search/identify?key=123456789.transform1&detail=full&showPermissions=true"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "    \"result\": \"ok\",\n" +
                                "    \"transform\": {\n" +
                                "        \"key\": \"123456789.transform1\",\n" +
                                "        \"metadata\": {\n" +
                                "            \"name\": \"Transform 1\",\n" +
                                "            \"unit\": \"U\", \n" +
                                "            \"content_type\": \"application/json\" \n" +
                                "        },\n" +
                                "        \"dnsRecords\": [],\n" +
                                "        \"function\": {\n" +
                                "          \"name\": \"logFunction\",\n" +
                                "          \"isPublic\": \"false\"\n" +
                                "        },\n" +
                                "        \"userPermissions\": {\n" +
                                "          \"user_1\": [\n" +
                                "            \"ADMIN\",\n" +
                                "            \"CONSUME\",\n" +
                                "            \"DISCOVER\",\n" +
                                "            \"MODIFY\"\n" +
                                "          ],\n" +
                                "          \"user_2\": [\n" +
                                "            \"DISCOVER\",\n" +
                                "            \"CONSUME\"\n" +
                                "          ]\n" +
                                "        },\n" +
                                "        \"groupPermissions\": {\n" +
                                "          \"group_1\": [\n" +
                                "            \"ADMIN\",\n" +
                                "            \"CONSUME\",\n" +
                                "            \"DISCOVER\",\n" +
                                "            \"MODIFY\"\n" +
                                "          ],\n" +
                                "          \"group_2\": [\n" +
                                "            \"DISCOVER\"\n" +
                                "          ]\n" +
                                "        }," +
                                "        \"devices\": [\n" +
                                "          {\n" +
                                "            \"linkName\": \"camera_trap\",\n" +
                                "            \"key\": \"cale-gvvn-2czt-adfa-g76z-2oja.z.g\"\n" +
                                "          }\n" +
                                "        ],\n" +
                                "        \"sources\": [\n" +
                                "          {\n" +
                                "            \"alias\": \"transform source\",\n" +
                                "            \"trigger\": \"false\",\n" +
                                "            \"source\": \"cale-gvvn-2czt-adfa-g76z-2oja.z.g\",\n" +
                                "            \"aggregation\": {\n" +
                                "              \"aggregationType\": \"application/json\",\n" +
                                "              \"aggregationSpec\": \"string\"\n" +
                                "            }\n" +
                                "          }\n" +
                                "        ]\n" +
                                "    }\n" +
                                "}")));
        Object thing = api.search.identify("123456789.transform1", true, true);
        System.out.println("Thing: " + thing);
        assertEquals(RegistryTransform.class.getName(), thing.getClass().getName());

        RegistryTransform transform = (RegistryTransform)thing;
        assertEquals("123456789.transform1", transform.getKey());
        assertEquals("logFunction", transform.getFunction().getName());
        assertEquals(1, transform.getSources().size());
    }
}
