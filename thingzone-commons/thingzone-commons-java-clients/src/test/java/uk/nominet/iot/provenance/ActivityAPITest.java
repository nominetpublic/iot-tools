/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.provenance;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.junit.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static org.junit.Assert.*;

public class ActivityAPITest extends ProvsvrAPITestBase {

    @Test
    public void createActivity() throws Exception {
        stubFor(put(urlEqualTo("/api/activity/testPrefix/123456"))
                        .withMultipartRequestBody(aMultipart().withName("activity")
                                                              .withHeader("Content-Type", containing("application/json"))
                                                              //.withBody(equalTo("device1"))
                                                              )
                        .willReturn(aResponse()
                                            .withHeader("Content-Type", "application/json")
                                            .withStatus(201)
                                            .withBody("{\n" +
                                                      "  \"result\" : \"ok\",\n" +
                                                      "  \"id\": \"123456\"\n" +
                                                      "}")));


        String id = api.activity.create("123456", new JsonObject());
        assertEquals("testPrefix:123456", id);
    }


    @Test
    public void getActivity() throws Exception {
        stubFor(get(urlEqualTo("/api/activity/testPrefix/123456"))
                        .willReturn(aResponse()
                                            .withHeader("Content-Type", "application/json")
                                            .withStatus(200)
                                            .withBody("{\n" +
                                                      "  \"result\" : \"ok\",\n" +
                                                      "  \"activity\" : { \"foo\": \"bar\" }\n" +
                                                      "}")));

        JsonObject activity = api.activity.get("123456");

        assertNotNull(activity);
        assertEquals("bar", activity.get("foo").getAsString());
    }

    @Test
    public void getAllActivities() throws Exception {
        stubFor(get(urlEqualTo("/api/activity/testPrefix"))
                        .willReturn(aResponse()
                                            .withHeader("Content-Type", "application/json")
                                            .withStatus(200)
                                            .withBody("{\n" +
                                                      "  \"result\" : \"ok\",\n" +
                                                      "  \"activities\" : [\n" +
                                                      "    {  \"foo\": \"bar\" },\n" +
                                                      "    { \"foo\": \"baz\" }\n" +
                                                      "  ]\n" +
                                                      "}")));

        JsonArray activities = api.activity.getAll();

        assertNotNull(activities);
        assertEquals("bar", activities.get(0).getAsJsonObject().get("foo").getAsString());
        assertEquals("baz", activities.get(1).getAsJsonObject().get("foo").getAsString());
    }

}
