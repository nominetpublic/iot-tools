/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import static com.github.tomakehurst.wiremock.client.WireMock.aMultipart;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.containing;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.put;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;

import uk.nominet.iot.common.Taxonomy;
import uk.nominet.iot.model.registry.RegistryDevice;
import uk.nominet.iot.registry.builders.DeviceUpsertBuilder;
import uk.nominet.iot.registry.builders.DnsRecordsBuilder;
import uk.nominet.iot.registry.builders.ObjectBuilder;
import uk.nominet.iot.registry.builders.PermissionsBuilder;
import uk.nominet.iot.registry.builders.StreamReferencesBuilder;
import uk.nominet.iot.registry.utils.Upsertable;

public class DeviceAPITest extends RegistryAPITestBase {

    public static final String DEVICE_GET_BODY = "{\n" +
            "  \"device\": {\n" +
            "    \"key\": \"device1\",\n" +
            "    \"name\": \"c1\",\n" +
            "    \"type\": \"camera\",\n" +
            "    \"location\": {\"type\":\"Point\",\"coordinates\":[-1.25,51.766,0.0]}," +
            "    \"metadata\": {\"foo\":\"bar\"},\n" +
            "    \"dataStreams\": [\n" +
            "      {\n" +
            "        \"key\": \"stream1\",\n" +
            "        \"metadata\": {\"foo\":\"bar\"},\n" +
            "        \"isTransform\": true,\n" +
            "        \"name\": \"device_1_stream\",\n" +
            "        \"dnsRecords\": [\n" +
            "          {\n" +
            "            \"subdomain\": \"string\",\n" +
            "            \"rrtype\": \"A\",\n" +
            "            \"rdata\": \"1.2.3.4\",\n" +
            "            \"ttl\": 600,\n" +
            "            \"description\": \"dns record\"\n" +
            "          }\n" +
            "        ],\n" +
            "        \"userPermissions\": {\n" +
            "          \"user1\": [\n" +
            "            \"ADMIN\",\n" +
            "            \"CONSUME\",\n" +
            "            \"DISCOVER\"\n" +
            "          ],\n" +
            "          \"user2\": [\n" +
            "            \"DISCOVER\"\n" +
            "          ]\n" +
            "        },\n" +
            "        \"groupPermissions\": {\n" +
            "          \"group1\": [\n" +
            "            \"DISCOVER\"\n" +
            "          ]\n" +
            "        }\n" +
            "      }\n" +
            "    ],\n" +
            "    \"dnsRecords\": [\n" +
            "      {\n" +
            "        \"subdomain\": \"string\",\n" +
            "        \"rrtype\": \"A\",\n" +
            "        \"rdata\": \"1.2.3.4\",\n" +
            "        \"ttl\": 600,\n" +
            "        \"description\": \"dns record\"\n" +
            "      }\n" +
            "    ],\n" +
            "    \"userPermissions\": {\n" +
            "      \"user1\": [\n" +
            "        \"ADMIN\",\n" +
            "        \"CONSUME\",\n" +
            "        \"DISCOVER\",\n" +
            "        \"MODIFY\"\n" +
            "      ],\n" +
            "      \"user2\": [\n" +
            "        \"DISCOVER\"\n" +
            "      ]\n" +
            "    },\n" +
            "    \"groupPermissions\": {\n" +
            "      \"group1\": [\n" +
            "        \"DISCOVER\"\n" +
            "      ]\n" +
            "    }\n" +
            "  },\n" +
            "  \"result\": \"ok\"\n" +
            "}";
    RegistryAPISession api;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        api = new RegistryAPISession(wireMockRule.url("/"), "test", "test", 1);
    }

    @Test
    public void getList() throws Exception {
        stubFor(get(urlEqualTo("/api/device/list"))
                .withHeader("X-FollyBridge-SessionKey", matching("XU2OBHoFm-Uc0uk0g3ttCA"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "  \"devices\": [\n" +
                                "    \"ncjm-eo7z-foh3-ho2q-swfz-jy4x.q.k\",\n" +
                                "    \"v2q5-orwi-zsj6-zmrv-g773-pkno.h.w\"\n" +
                                "  ],\n" +
                                "  \"result\": \"ok\"\n" +
                                "}")
                )
        );
        List<String> devices = api.device.list();

        assertEquals(2, devices.size());
        assertThat(devices,
                   CoreMatchers.hasItems("ncjm-eo7z-foh3-ho2q-swfz-jy4x.q.k", "v2q5-orwi-zsj6-zmrv-g773-pkno.h.w"));
    }

    @Test
    public void getFullDevice() throws Exception {
        stubFor(get(urlEqualTo("/api/device/get?device=device1&detail=full&showPermissions=true"))
                .withHeader("X-FollyBridge-SessionKey", matching("XU2OBHoFm-Uc0uk0g3ttCA"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody(DEVICE_GET_BODY)
                )
        );
        RegistryDevice device = api.device.get("device1", true, true);

        assertEquals("device1", device.getKey());
        assertEquals("c1", device.getName());
        assertEquals("camera", device.getType());
        assertEquals("POINT", device.getLocation().type().name());
        assertEquals("bar", device.getMetadata().get("foo"));
        assertEquals("string", device.getDnsRecords().get(0).getSubdomain());
        assertEquals("A", device.getDnsRecords().get(0).getRrtype());
        assertEquals("1.2.3.4", device.getDnsRecords().get(0).getRdata());
        assertEquals(600, device.getDnsRecords().get(0).getTtl());
        assertEquals("dns record", device.getDnsRecords().get(0).getDescription());
        assertThat(device.getUserPermissions().get("user1"),
                   CoreMatchers.hasItems(Taxonomy.PERMISSION_ADMIN,
                                         Taxonomy.PERMISSION_CONSUME,
                                         Taxonomy.PERMISSION_DISCOVER,
                                         Taxonomy.PERMISSION_MODIFY)
        );
        assertThat(device.getUserPermissions().get("user2"),
                   CoreMatchers.hasItems(Taxonomy.PERMISSION_DISCOVER)
        );
        assertThat(device.getGroupPermissions().get("group1"),
                   CoreMatchers.hasItems(Taxonomy.PERMISSION_DISCOVER)
        );

        //Data Stream
        assertEquals(1, device.getDataStreams().size());
        assertEquals("stream1", device.getDataStreams().get(0).getKey());
        assertEquals("device_1_stream", device.getDataStreams().get(0).getName());
        assertEquals("bar", device.getDataStreams().get(0).getMetadata().get("foo"));
        assertTrue(device.getDataStreams().get(0).isTransform());
        assertEquals("string", device.getDataStreams().get(0).getDnsRecords().get(0).getSubdomain());
        assertEquals("A", device.getDataStreams().get(0).getDnsRecords().get(0).getRrtype());
        assertEquals("1.2.3.4", device.getDataStreams().get(0).getDnsRecords().get(0).getRdata());
        assertEquals(600, device.getDataStreams().get(0).getDnsRecords().get(0).getTtl());
        assertEquals("dns record", device.getDataStreams().get(0).getDnsRecords().get(0).getDescription());
        assertThat(device.getDataStreams().get(0).getUserPermissions().get("user1"),
                   CoreMatchers.hasItems(Taxonomy.PERMISSION_ADMIN,
                                         Taxonomy.PERMISSION_CONSUME,
                                         Taxonomy.PERMISSION_DISCOVER)
        );
        assertThat(device.getDataStreams().get(0).getUserPermissions().get("user1"),
                   CoreMatchers.hasItems("DISCOVER")
        );
        assertThat(device.getDataStreams().get(0).getGroupPermissions().get("group1"),
                   CoreMatchers.hasItems("DISCOVER")
        );
    }

    @Test
    public void upsertFullDevice() throws Exception {
        stubFor(put(urlEqualTo("/api/device/upsert"))
                .withHeader("X-FollyBridge-SessionKey", matching("XU2OBHoFm-Uc0uk0g3ttCA"))
                .withMultipartRequestBody(aMultipart().withName("device")
                                                      .withHeader("Content-Type", containing("charset"))
                                                      .withBody(equalTo("device1")))
                .willReturn(aResponse().withHeader("Content-Type",
                                                   "application/json")
                                       .withStatus(200)
                                       .withBody(DEVICE_GET_BODY)));

        Upsertable<?>[] upserts = DeviceUpsertBuilder.create("device1")
                .setName("My new device")
                .setType("device type 1")
                .setDataStreams(StreamReferencesBuilder.create()
                                                       .addReference("streamkey1234", "stream1Link")
                                                       .addReference("streamkey1235", "stream2Link")
                                                       .build())
                .setDnsRecords(DnsRecordsBuilder.create()
                                                .addDnsRecord("dns1", "sub1", 6000, "A", "1.2.3.4")
                                                .addDnsRecord("dns2", "sub2", 6000, "A", "1.2.3.4")
                                                .build())
                .setUserPermissions(PermissionsBuilder.create()
                                                      .add("user1", Taxonomy.PERMISSION_DISCOVER)
                                                      .add("user2",
                                                           Taxonomy.PERMISSION_DISCOVER,
                                                           Taxonomy.PERMISSION_ADMIN,
                                                           Taxonomy.PERMISSION_CONSUME)
                                                      .build())
                .setGroupPermissions(PermissionsBuilder.create()
                                                       .add("group1", Taxonomy.PERMISSION_DISCOVER)
                                                       .add("group2",
                                                            Taxonomy.PERMISSION_DISCOVER,
                                                            Taxonomy.PERMISSION_ADMIN,
                                                            Taxonomy.PERMISSION_CONSUME)
                                                       .build())
                .setMetadata(ObjectBuilder.create()
                                          .add("prop1", 123)
                                          .add("foo", "bar")
                                          .add("number", 10.56)
                                          .add("array", "1", "2", "3", "4", "5", "6")
                                          .add("test123",
                                               ObjectBuilder.create().add("foo", "bar").build(),
                                               ObjectBuilder.create().add("foo", "bar").build(),
                                               ObjectBuilder.create().add("foo", "bar").build(),
                                               ObjectBuilder.create().add("foo", "bar").build())
                                          .add("test")
                                          .add("abc", 123)
                                          .end()
                                          .build())
                .build();
        api.device.upsert("device1", upserts);
    }

}
