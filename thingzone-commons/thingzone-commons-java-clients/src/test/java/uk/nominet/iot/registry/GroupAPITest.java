/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import java.util.List;
import java.util.Optional;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static org.junit.Assert.*;

public class GroupAPITest extends RegistryAPITestBase {
    private RegistryAPISession api;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        api = new RegistryAPISession(wireMockRule.url("/"), "test", "test", 1);
        api.invalidateCache();
    }

    @Test
    public void listGroups() throws Exception {
        stubFor(get(urlEqualTo("/api/groups"))
                .withHeader("X-FollyBridge-SessionKey", matching("XU2OBHoFm-Uc0uk0g3ttCA"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "  \"groups\": [\n" +
                                "    \"group1\",\n" +
                                "    \"group2\"\n" +
                                "  ],\n" +
                                "  \"result\": \"ok\"\n" +
                                "}")
                )
        );
        List<String> groups = api.groups.list(Optional.empty());

        assertThat(groups, CoreMatchers.hasItems("group1", "group2"));

    }

    @Test
    public void listGroupsInvalidSession() throws Exception {
        stubFor(get(urlEqualTo("/api/groups"))
                .withHeader("X-FollyBridge-SessionKey", matching("invalidsessionkey"))
                .willReturn(getSessionFailResponse("GET", "groups"))
        );
        api.getSession().setSessionKey("invalidsessionkey");

        assertInvalidSession(() -> api.groups.list(Optional.empty()));
    }

    @Test
    public void listOwnedGroups() throws Exception {
        stubFor(get(urlEqualTo("/api/groups?owned=true"))
                .withHeader("X-FollyBridge-SessionKey", matching("XU2OBHoFm-Uc0uk0g3ttCA"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "  \"groups\": [\n" +
                                "    \"group1\",\n" +
                                "  ],\n" +
                                "  \"result\": \"ok\"\n" +
                                "}")
                )
        );
        List<String> groups = api.groups.list(Optional.of(true));

        assertThat(groups, CoreMatchers.hasItems("group1"));
    }

    @Test
    public void createNewGroup() throws Exception {
        stubFor(post(urlEqualTo("/api/groups/group1"))
                .withHeader("X-FollyBridge-SessionKey", matching("XU2OBHoFm-Uc0uk0g3ttCA"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "  \"result\": \"ok\"\n" +
                                "}")
                )
        );
        api.groups.create("group1");
    }

    @Test
    public void createNewGroupInvalidSession() throws Exception {
        stubFor(post(urlEqualTo("/api/groups/group1"))
                .withHeader("X-FollyBridge-SessionKey", matching("invalidsessionkey"))
                .willReturn(getSessionFailResponse("POST", "groups/group1"))
        );
        api.getSession().setSessionKey("invalidsessionkey");

        assertInvalidSession(() -> api.groups.create("group1"));
    }

    @Test
    public void createNewGroupNoPrivileges() throws Exception {
        stubFor(post(urlEqualTo("/api/groups/group1"))
                .withHeader("X-FollyBridge-SessionKey", matching("sessionNoPrivileges"))
                .willReturn(getUnauthorisedResponse("POST", "groups/group1"))
        );
        api.getSession().setSessionKey("sessionNoPrivileges");

        assertAuthError(() -> api.groups.create("group1"));
    }

    @Test
    public void listUsers() throws Exception {
        stubFor(get(urlEqualTo("/api/groups/group1/users"))
                .withHeader("X-FollyBridge-SessionKey", matching("XU2OBHoFm-Uc0uk0g3ttCA"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "  \"users\": [\n" +
                                "    \"user1\",\n" +
                                "    \"user2\"\n" +
                                "  ],\n" +
                                "  \"result\": \"ok\"\n" +
                                "}")
                )
        );
        List<String> users = api.groups.listUsers("group1");

        assertThat(users, CoreMatchers.hasItems("user1", "user2"));
    }

    @Test
    public void addUser() throws Exception {
        stubFor(put(urlEqualTo("/api/groups/group1/users/add"))
                .withRequestBody(matching(".*usernames=user1.*"))
                .withHeader("X-FollyBridge-SessionKey", matching("XU2OBHoFm-Uc0uk0g3ttCA"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "  \"result\": \"ok\"\n" +
                                "}")
                )
        );
        api.groups.addUsers("group1", new String[] {"user1"});
    }

    @Test
    public void addUsers() throws Exception {
        stubFor(put(urlEqualTo("/api/groups/group1/users/add"))
                .withRequestBody(matching(".*usernames=user1\\%2Cuser2.*"))
                .withHeader("X-FollyBridge-SessionKey", matching("XU2OBHoFm-Uc0uk0g3ttCA"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "  \"result\": \"ok\"\n" +
                                "}")
                )
        );
        api.groups.addUsers("group1", new String[] {"user1","user2"});
    }

    @Test
    public void addUserInvalidSession() throws Exception {
        stubFor(put(urlEqualTo("/api/groups/group1/users/add"))
                .withRequestBody(matching(".*usernames=user1.*"))
                .withHeader("X-FollyBridge-SessionKey", matching("invalidsessionkey"))
                .willReturn(getSessionFailResponse("PUT", "/groups/group1/users/add"))
        );
        api.getSession().setSessionKey("invalidsessionkey");

        assertInvalidSession(() -> api.groups.addUsers("group1", new String[] {"user1"}));
    }

    @Test
    public void addUserNoPrivileges() throws Exception {
        stubFor(put(urlEqualTo("/api/groups/group1/users/add"))
                .withRequestBody(matching(".*usernames=user1.*"))
                .withHeader("X-FollyBridge-SessionKey", matching("sessionNoPrivileges"))
                .willReturn(getUnauthorisedResponse("PUT", "/groups/group1/users/add"))
        );
        api.getSession().setSessionKey("sessionNoPrivileges");

        assertAuthError(() -> api.groups.addUsers("group1", new String[] {"user1"}));
    }

    @Test
    public void removeUser() throws Exception {
        stubFor(put(urlEqualTo("/api/groups/group1/users/remove"))
                .withRequestBody(matching(".*usernames=user1.*"))
                .withHeader("X-FollyBridge-SessionKey", matching("XU2OBHoFm-Uc0uk0g3ttCA"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "  \"result\": \"ok\"\n" +
                                "}")
                )
        );
        api.groups.removeUsers("group1", new String[] {"user1"});
    }

    @Test
    public void removeUsers() throws Exception {
        stubFor(put(urlEqualTo("/api/groups/group1/users/remove"))
                .withRequestBody(matching(".*usernames=user1\\%2Cuser2.*"))
                .withHeader("X-FollyBridge-SessionKey", matching("XU2OBHoFm-Uc0uk0g3ttCA"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "  \"result\": \"ok\"\n" +
                                "}")
                )
        );
        api.groups.removeUsers("group1", new String[] {"user1","user2"});
    }

    @Test
    public void removeUserInvalidSession() throws Exception {
        stubFor(put(urlEqualTo("/api/groups/group1/users/remove"))
                .withRequestBody(matching(".*usernames=user1.*"))
                .withHeader("X-FollyBridge-SessionKey", matching("invalidsessionkey"))
                .willReturn(getSessionFailResponse("PUT", "/groups/group1/users/remove"))
        );
        api.getSession().setSessionKey("invalidsessionkey");

        assertInvalidSession(() -> api.groups.removeUsers("group1", new String[] {"user1"}));
    }

    @Test
    public void removeUserNoPrivileges() throws Exception {
        stubFor(put(urlEqualTo("/api/groups/group1/users/remove"))
                .withRequestBody(matching(".*usernames=user1.*"))
                .withHeader("X-FollyBridge-SessionKey", matching("sessionNoPrivileges"))
                .willReturn(getUnauthorisedResponse("PUT", "/groups/group1/users/remove"))
        );
        api.getSession().setSessionKey("sessionNoPrivileges");

        assertAuthError(() -> api.groups.removeUsers("group1", new String[] {"user1"}));
    }

    @Test
    public void changeAdminStatus() throws Exception {
        stubFor(put(urlEqualTo("/api/groups/group1/admin"))
                .withRequestBody(matching(".*username=user1.*"))
                .withRequestBody(matching(".*admin=true.*"))
                .withHeader("X-FollyBridge-SessionKey", matching("XU2OBHoFm-Uc0uk0g3ttCA"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "  \"result\": \"ok\"\n" +
                                "}")
                )
        );
        api.groups.changeAdminStatus("group1", "user1", true);
    }

    @Test
    public void changeAdminStatusInvalidSession() throws Exception {
        stubFor(put(urlEqualTo("/api/groups/group1/admin"))
                .withRequestBody(matching(".*username=user1.*"))
                .withRequestBody(matching(".*admin=true.*"))
                .withHeader("X-FollyBridge-SessionKey", matching("invalidsessionkey"))
                .willReturn(getSessionFailResponse("PUT","groups/group1/admin"))
        );
        api.getSession().setSessionKey("invalidsessionkey");

        assertInvalidSession(() -> api.groups.changeAdminStatus("group1", "user1", true));
    }

    @Test
    public void changeAdminStatusNoPrivileges() throws Exception {
        stubFor(put(urlEqualTo("/api/groups/group1/admin"))
                .withRequestBody(matching(".*username=user1.*"))
                .withRequestBody(matching(".*admin=true.*"))
                .withHeader("X-FollyBridge-SessionKey", matching("sessionNoPrivileges"))
                .willReturn(getUnauthorisedResponse("PUT","groups/group1/admin"))
        );
        api.getSession().setSessionKey("sessionNoPrivileges");

        assertAuthError(() -> api.groups.changeAdminStatus("group1", "user1", true));
    }

}
