/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableList;

import uk.nominet.iot.model.registry.RegistryKey;
import uk.nominet.iot.registry.exceptions.RegistryFailResponseException;

public class PermissionsAPITest extends RegistryAPITestBase {
    RegistryAPISession api;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        api = new RegistryAPISession(wireMockRule.url("/"), "test", "test", 1);
    }

    @Test
    public void listSingle() throws Exception {
        stubFor(get(urlEqualTo("/api/permission/list?key=123456789&username=test"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "    \"result\": \"ok\",\n" +
                                "    \"userPermissions\": {\n" +
                                "        \"test\": [\n" +
                                "            \"ADMIN\",\n" +
                                "            \"UPLOAD\",\n" +
                                "            \"CONSUME\",\n" +
                                "            \"MODIFY\",\n" +
                                "            \"ANNOTATE\",\n" +
                                "            \"DISCOVER\"\n" +
                                "        ]\n" +
                                "    },\n" +
                                "    \"key\": \"fusion\"\n" +
                                "}")));

        stubFor(get(urlEqualTo("/api/permission/list?key=123456789&username=test"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "    \"result\": \"ok\",\n" +
                                "    \"userPermissions\": {\n" +
                                "        \"test\": [\n" +
                                "            \"ADMIN\",\n" +
                                "            \"UPLOAD\",\n" +
                                "            \"CONSUME\",\n" +
                                "            \"MODIFY\",\n" +
                                "            \"ANNOTATE\",\n" +
                                "            \"DISCOVER\"\n" +
                                "        ]\n" +
                                "    },\n" +
                                "    \"key\": \"fusion\"\n" +
                                "}")));
        RegistryKey permissions = api.permission.list("123456789", Optional.of("test"), Optional.empty());

        assertEquals(1, permissions.getUserPermissions().size());
        assertTrue(permissions.getUserPermissions().containsKey("test"));
        assertTrue(permissions.getUserPermissions().get("test")
                              .equals(ImmutableList.of("ADMIN", "UPLOAD", "CONSUME", "MODIFY", "ANNOTATE", "DISCOVER")));
    }

    @Test
    public void listMultiple() throws Exception {
        stubFor(get(urlEqualTo("/api/permission/list?key=123456789"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "    \"result\": \"ok\",\n" +
                                "    \"userPermissions\": {\n" +
                                "        \"test\": [\n" +
                                "            \"ADMIN\",\n" +
                                "            \"UPLOAD\",\n" +
                                "            \"CONSUME\",\n" +
                                "            \"MODIFY\",\n" +
                                "            \"ANNOTATE\",\n" +
                                "            \"DISCOVER\"\n" +
                                "        ],\n" +
                                "        \"test1\": [\n" +
                                "            \"ADMIN\",\n" +
                                "            \"UPLOAD\",\n" +
                                "            \"CONSUME\",\n" +
                                "            \"MODIFY\",\n" +
                                "            \"ANNOTATE\",\n" +
                                "            \"DISCOVER\"\n" +
                                "        ]\n" +
                                "    },\n" +
                                "    \"groupPermissions\": {\n" +
                                "      \"group_1\": [\n" +
                                "        \"ADMIN\",\n" +
                                "        \"CONSUME\",\n" +
                                "        \"DISCOVER\",\n" +
                                "        \"MODIFY\"\n" +
                                "      ],\n" +
                                "      \"group_2\": [\n" +
                                "        \"DISCOVER\"\n" +
                                "      ]\n" +
                                "    },\n" +
                                "    \"key\": \"fusion\"\n" +
                                "}")));
        RegistryKey permissions = api.permission.list("123456789");

        assertEquals(2, permissions.getUserPermissions().size());
        assertTrue(permissions.getUserPermissions().containsKey("test"));
        assertTrue(permissions.getUserPermissions().containsKey("test1"));
        assertTrue(permissions.getUserPermissions().get("test")
                              .equals(ImmutableList.of("ADMIN", "UPLOAD", "CONSUME", "MODIFY", "ANNOTATE", "DISCOVER")));
        assertEquals(2, permissions.getGroupPermissions().size());
        assertTrue(permissions.getGroupPermissions().containsKey("group_1"));
    }

    @Test (expected = RegistryFailResponseException.class)
    public void permittedUsersNoKey() throws Exception {
        stubFor(get(urlEqualTo("/api/permission/list?key=123456789unknown"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(404)
                        .withBody("{\n" +
                                "    \"result\": \"fail\",\n" +
                                "    \"reason\": \"DNSkey not found: 123456789unknown\"\n" +
                                "}")));
        api.permission.list("123456789unknown");
    }

}
