/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Optional;

import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;

import uk.nominet.iot.common.Taxonomy;
import uk.nominet.iot.model.registry.RegistryKey;
import uk.nominet.iot.model.registry.RegistryUser;
import uk.nominet.iot.registry.exceptions.RegistryFailResponseException;

public class InternalAPITest extends RegistryAPITestBase {

    RegistryAPISession api;
    private String[] consumePermissions;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        api = new RegistryAPISession(wireMockRule.url("/"), "test", "test", 1);
        consumePermissions = new String[] {"CONSUME"};
    }

    @Test
    public void getUsername() throws Exception {
        stubFor(get(urlEqualTo("/api/internal/get_username?sessionKey=123456789"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "    \"result\": \"ok\",\n" +
                                "    \"username\": \"test\",\n" +
                                "    \"ttl\": 821662\n" +
                                "}")));
        RegistryUser user = api.internal.getUsername("123456789");
        assertEquals("test", user.getUsername());
    }

    @Test (expected = RegistryFailResponseException.class)
    public void getUsernameUnknownSession() throws Exception {
        stubFor(get(urlEqualTo("/api/internal/get_username?sessionKey=123456789unknown"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(401)
                        .withBody("{\n" +
                                "    \"result\": \"fail\",\n" +
                                "    \"reason\": \"reference session 123456789unknown not found\"\n" +
                                "}")));
        api.internal.getUsername("123456789unknown");
    }

    @Test
    public void permissionCheck() throws Exception {
        stubFor(get(urlEqualTo("/api/internal/permission_check?keys=testStream&permissions=CONSUME&sessionKey=123456789"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "    \"result\": \"ok\"\n" +
                                "}")));
        String[] keys = {"testStream"};
        assertTrue(api.internal.permissionCheck(keys, consumePermissions, Optional.of("123456789"), Optional.empty()));
    }

    @Test (expected = RegistryFailResponseException.class)
    public void permissionCheckUnknownSession() throws Exception {
        stubFor(get(urlEqualTo("/api/internal/permission_check?keys=testStream&permissions=CONSUME&sessionKey=123456789unknown"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(401)
                        .withBody("{\n" +
                                "    \"result\": \"fail\",\n" +
                                "    \"reason\": \"reference session 123456789unknown not found\"\n" +
                                "}")));
        String[] keys = {"testStream"};
        api.internal.permissionCheck(keys, consumePermissions, Optional.of("123456789unknown"), Optional.empty());
    }

    @Test
    public void permittedUsers() throws Exception {
        stubFor(get(urlEqualTo("/api/internal/permitted_users?permissions=CONSUME&key=123456789"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "    \"result\": \"ok\",\n" +
                                "    \"users\": [\n" +
                                "        \"user1\",\n" +
                                "        \"user2\",\n" +
                                "        \"user3\"\n" +
                                "    ]\n" +
                                "}")));
        List<String> users = api.internal.permittedUsers("123456789", consumePermissions);
        assertThat(users,  CoreMatchers.hasItems("user1", "user2", "user3"));
    }

    @Test (expected = RegistryFailResponseException.class)
    public void permittedUsersNoKey() throws Exception {
        stubFor(get(urlEqualTo("/api/internal/permitted_users?permissions=CONSUME&key=123456789unknown"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "    \"result\": \"fail\",\n" +
                                "    \"reason\": \"DNSkey '123456789unknown' not found\"\n" +
                                "}")));
        api.internal.permittedUsers("123456789unknown", consumePermissions);
    }

    @Test
    public void permissionsForKey() throws Exception {
        stubFor(get(urlEqualTo("/api/internal/permissions_for_key?key=123456789"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "  \"permissions\": {\n" +
                                "    \"key\": \"123456789\",\n" +
                                "    \"userPermissions\": {\n" +
                                "      \"user_1\": [\n" +
                                "        \"ADMIN\",\n" +
                                "        \"CONSUME\",\n" +
                                "        \"DISCOVER\",\n" +
                                "        \"MODIFY\"\n" +
                                "      ],\n" +
                                "      \"user_2\": [\n" +
                                "        \"DISCOVER\",\n" +
                                "        \"CONSUME\"\n" +
                                "      ]\n" +
                                "    },\n" +
                                "    \"groupPermissions\": {\n" +
                                "      \"group_1\": [\n" +
                                "        \"ADMIN\",\n" +
                                "        \"CONSUME\",\n" +
                                "        \"DISCOVER\",\n" +
                                "        \"MODIFY\"\n" +
                                "      ],\n" +
                                "      \"group_2\": [\n" +
                                "        \"DISCOVER\"\n" +
                                "      ]\n" +
                                "    }\n" +
                                "  },\n" +
                                "  \"result\": \"ok\"\n" +
                                "}")));
        RegistryKey key = api.internal.permissionsForKey("123456789");

        assertEquals("123456789", key.getKey());
        assertThat(key.getUserPermissions().get("user_1"), CoreMatchers.hasItems(
                Taxonomy.PERMISSION_ADMIN,
                Taxonomy.PERMISSION_CONSUME,
                Taxonomy.PERMISSION_DISCOVER,
                Taxonomy.PERMISSION_MODIFY
        ));
        assertThat(key.getUserPermissions().get("user_2"), CoreMatchers.hasItems(
                Taxonomy.PERMISSION_CONSUME,
                Taxonomy.PERMISSION_DISCOVER
        ));
        assertThat(key.getGroupPermissions().get("group_1"), CoreMatchers.hasItems(
                Taxonomy.PERMISSION_ADMIN,
                Taxonomy.PERMISSION_CONSUME,
                Taxonomy.PERMISSION_DISCOVER,
                Taxonomy.PERMISSION_MODIFY
        ));
        assertThat(key.getGroupPermissions().get("group_2"), CoreMatchers.hasItems(
                Taxonomy.PERMISSION_DISCOVER
        ));
    }

}
