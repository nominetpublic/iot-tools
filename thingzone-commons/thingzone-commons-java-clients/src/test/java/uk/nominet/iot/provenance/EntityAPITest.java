/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.provenance;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.junit.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static org.junit.Assert.*;

public class EntityAPITest extends ProvsvrAPITestBase {

    @Test
    public void getAnEntity() throws Exception {
        stubFor(get(urlEqualTo("/api/entity/testPrefix/123456"))
                        .willReturn(aResponse()
                                            .withHeader("Content-Type", "application/json")
                                            .withStatus(200)
                                            .withBody("{\n" +
                                                      "  \"result\" : \"ok\",\n" +
                                                      "  \"entity\" : { \"foo\": \"bar\" }\n" +
                                                      "}")));

        JsonObject entity = api.entity.get("123456");

        assertNotNull(entity);
        assertEquals("bar", entity.get("foo").getAsString());

    }

    @Test
    public void getAllActivities() throws Exception {
        stubFor(get(urlEqualTo("/api/entity/testPrefix"))
                        .willReturn(aResponse()
                                            .withHeader("Content-Type", "application/json")
                                            .withStatus(200)
                                            .withBody("{\n" +
                                                      "  \"result\" : \"ok\",\n" +
                                                      "  \"entities\" : [\n" +
                                                      "    {  \"foo\": \"bar\" },\n" +
                                                      "    { \"foo\": \"baz\" }\n" +
                                                      "  ]\n" +
                                                      "}")));

        JsonArray entities = api.entity.getAll();

        assertNotNull(entities);
        assertEquals("bar", entities.get(0).getAsJsonObject().get("foo").getAsString());
        assertEquals("baz", entities.get(1).getAsJsonObject().get("foo").getAsString());
    }

}
