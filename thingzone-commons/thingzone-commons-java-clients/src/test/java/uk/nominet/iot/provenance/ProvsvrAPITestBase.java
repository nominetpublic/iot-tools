/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.provenance;

import com.github.tomakehurst.wiremock.extension.responsetemplating.ResponseTemplateTransformer;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Before;
import org.junit.Rule;

import java.util.ArrayList;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

public class ProvsvrAPITestBase {

    @Rule
    public WireMockRule wireMockRule =
            new WireMockRule(wireMockConfig().dynamicPort()
                                             .extensions(new ResponseTemplateTransformer(false)));

    protected ProvsvrAPI api;

    private ArrayList<String> requestStack = new ArrayList<>();

    @Before
    public void setUp() throws Exception {

        requestStack = new ArrayList<>();

        wireMockRule.addMockServiceRequestListener((request, response) -> {
            System.out.println("Request: " + request.getAbsoluteUrl());
            System.out.println("Method: " + request.getMethod());
            System.out.println("Body: " + request.getBodyAsString());
            System.out.println("Headers: " + request.getHeaders());

            requestStack.add(request.getAbsoluteUrl());
        });

        api = new ProvsvrAPI(wireMockRule.url("/"), "testPrefix");
    }

}
