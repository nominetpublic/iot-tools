/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.provenance;

import org.junit.Test;
import uk.nominet.iot.model.provenance.Prefix;


import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.*;

public class PrefixAPITest extends ProvsvrAPITestBase {

    @Test
    public void createPrefix() throws Exception {
        stubFor(put(urlEqualTo("/api/prefix/foo"))
                        .willReturn(aResponse()
                                            .withHeader("Content-Type", "application/json")
                                            .withStatus(201)
                                            .withBody("{\n" +
                                                      "   \"result\" : \"ok\",\n" +
                                                      "   \"prefix\" : {\n" +
                                                      "      \"namespace\" : \"http://example.com\",\n" +
                                                      "      \"name\" : \"foo\"\n" +
                                                      "   }\n" +
                                                      "}")
                                   )
               );


        Prefix prefix = api.prefix.create("foo", "https://www.example.com");

        assertNotNull(prefix);
        assertEquals("foo", prefix.getName());
        assertEquals("http://example.com", prefix.getNamespace());
    }

    @Test
    public void getPrefix() throws Exception {
        stubFor(get(urlEqualTo("/api/prefix/foo"))
                        .willReturn(aResponse()
                                            .withHeader("Content-Type", "application/json")
                                            .withStatus(200)
                                            .withBody("{\n" +
                                                      "   \"result\" : \"ok\",\n" +
                                                      "   \"prefix\" : {\n" +
                                                      "      \"namespace\" : \"http://example.com\",\n" +
                                                      "      \"name\" : \"foo\"\n" +
                                                      "   }\n" +
                                                      "}")
                                   )
               );


        Prefix prefix = api.prefix.get("foo");

        assertNotNull(prefix);
        assertEquals("foo", prefix.getName());
        assertEquals("http://example.com", prefix.getNamespace());
    }


    @Test
    public void getAllPrefixes() throws Exception {
        stubFor(get(urlEqualTo("/api/prefix"))
                        .willReturn(aResponse()
                                            .withHeader("Content-Type", "application/json")
                                            .withStatus(200)
                                            .withBody("{\n" +
                                                      "   \"prefixes\" : [\n" +
                                                      "      {\n" +
                                                      "         \"name\" : \"foo\",\n" +
                                                      "         \"namespace\" : \"http://example.com\"\n" +
                                                      "      },\n" +
                                                      "      {\n" +
                                                      "         \"name\" : \"bar\",\n" +
                                                      "         \"namespace\" : \"http://example.com\"\n" +
                                                      "      },\n" +
                                                      "      {\n" +
                                                      "         \"name\" : \"baz\",\n" +
                                                      "         \"namespace\" : \"http://example.org\"\n" +
                                                      "      }\n" +
                                                      "   ],\n" +
                                                      "   \"result\" : \"ok\"\n" +
                                                      "}")
                                   )
               );


        List<Prefix> prefixes = api.prefix.getAll();

        assertNotNull(prefixes);

        System.out.println("Prefixes: " +prefixes);
        assertEquals("foo", prefixes.get(0).getName());
        assertEquals("http://example.com", prefixes.get(0).getNamespace());
        assertEquals("bar", prefixes.get(1).getName());
        assertEquals("http://example.com", prefixes.get(0).getNamespace());
        assertEquals("baz", prefixes.get(2).getName());
        assertEquals("http://example.com", prefixes.get(0).getNamespace());

    }

}
