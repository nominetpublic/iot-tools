/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import uk.nominet.iot.model.registry.RegistryDataStream;
import uk.nominet.iot.registry.exceptions.RegistryFailResponseException;

public class DataStreamAPITest extends RegistryAPITestBase {
    RegistryAPISession api;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        api = new RegistryAPISession(wireMockRule.url("/"), "test", "test", 1);
    }

    @Test
    public void getDatastream() throws Exception {
        stubFor(get(urlEqualTo("/api/datastream/get?dataStream=data.stream.id&detail=full&showPermissions=true"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "    \"result\": \"ok\",\n" +
                                "    \"dataStream\": {\n" +
                                "        \"key\": \"data.stream.id\",\n" +
                                "        \"metadata\": {\n" +
                                "            \"name\": \"name\",\n" +
                                "            \"unit\": \"unit\"\n" +
                                "        },\n" +
                                "        \"isTransform\": false,\n" +
                                "        \"pushSubscribers\" : [\n" +
                                "           {\n" +
                                "               \"uri\" : \"https://en.wikipedia.org/\",\n" +
                                "               \"uriParams\" : {}\n" +
                                "           }\n" +
                                "        ],\n" +
                                "        \"userPermissions\": {\n" +
                                "            \"test\": [\n" +
                                "                \"ADMIN\",\n" +
                                "                \"UPLOAD\",\n" +
                                "                \"CONSUME\",\n" +
                                "                \"MODIFY\",\n" +
                                "                \"ANNOTATE\",\n" +
                                "                \"DISCOVER\"\n" +
                                "           ],\n" +
                                "           \"test1\": [\n" +
                                "                \"ADMIN\",\n" +
                                "                \"UPLOAD\",\n" +
                                "                \"CONSUME\",\n" +
                                "                \"MODIFY\",\n" +
                                "                \"ANNOTATE\",\n" +
                                "                \"DISCOVER\"\n" +
                                "           ]\n" +
                                "        },\n" +
                                "        \"groupPermissions\": {\n" +
                                "            \"group_1\": [\n" +
                                "                \"ADMIN\",\n" +
                                "                \"CONSUME\",\n" +
                                "                \"DISCOVER\",\n" +
                                "                \"MODIFY\"\n" +
                                "            ],\n" +
                                "            \"group_2\": [\n" +
                                "                \"DISCOVER\"\n" +
                                "            ]\n" +
                                "        }\n" +
                                "    }\n" +
                                "}")));
        RegistryDataStream stream = api.datastream.get("data.stream.id", true, true);
        assertEquals("data.stream.id", stream.getKey());
    }

    @Test (expected = RegistryFailResponseException.class)
    public void getUnknownDatastream() throws Exception {
        stubFor(get(urlEqualTo("/api/datastream/get?dataStream=data.stream.id.unknown"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(404)
                        .withBody("{\n" +
                                "    \"result\": \"fail\",\n" +
                                "    \"reason\": \"DataStream not found: fusion.autonomy123\"\n" +
                                "}")));
        api.datastream.get("data.stream.id.unknown", false, false);
    }

}
