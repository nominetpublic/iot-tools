/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.builders;

import org.junit.Test;
import uk.nominet.iot.common.Taxonomy;
import uk.nominet.iot.registry.builders.*;
import uk.nominet.iot.registry.utils.Upsertable;

import static org.junit.Assert.*;

public class DeviceUpsertBuilderTest {

    @Test
    public void fullBuild() throws Exception {
        Upsertable<?>[] upserts = DeviceUpsertBuilder
                .create("device1")
                .setName("My new device")
                .setType("device type 1")
                .setDataStreams(StreamReferencesBuilder.create()
                                                       .addReference("streamkey1234", "stream1Link")
                                                       .addReference("streamkey1235", "stream2Link")
                                                       .build())
                .setDnsRecords(DnsRecordsBuilder.create()
                                                .addDnsRecord("dns1", "sub1", 6000, "A", "1.2.3.4")
                                                .addDnsRecord("dns2", "sub2", 6000, "A", "1.2.3.4")
                                                .build())
                .setUserPermissions(PermissionsBuilder.create()
                                                      .add("user1", Taxonomy.PERMISSION_DISCOVER)
                                                      .add("user2",
                                                           Taxonomy.PERMISSION_DISCOVER,
                                                           Taxonomy.PERMISSION_ADMIN,
                                                           Taxonomy.PERMISSION_CONSUME)
                                                      .build())
                .setGroupPermissions(PermissionsBuilder.create()
                                                       .add("group1", Taxonomy.PERMISSION_DISCOVER)
                                                       .add("group2",
                                                            Taxonomy.PERMISSION_DISCOVER,
                                                            Taxonomy.PERMISSION_ADMIN,
                                                            Taxonomy.PERMISSION_CONSUME)
                                                       .build())
                .setMetadata(ObjectBuilder.create()
                                          .add("prop1", 123)
                                          .add("foo", "bar")
                                          .add("number", 10.56)
                                          .add("array", "1", "2", "3", "4", "5", "6")
                                          .add("test123",
                                               ObjectBuilder.create().add("foo", "bar").build(),
                                               ObjectBuilder.create().add("foo", "bar").build(),
                                               ObjectBuilder.create().add("foo", "bar").build(),
                                               ObjectBuilder.create().add("foo", "bar").build())
                                          .add("test")
                                          .add("abc", 123)
                                          .end()
                                          .build())
                .build();

        assertEquals("text/plain", upserts[0].getContentType().getMimeType());
        assertEquals("My new device", upserts[0].getValue());

        assertEquals("text/plain", upserts[1].getContentType().getMimeType());
        assertEquals("device type 1", upserts[1].getValue());

        assertEquals("application/json", upserts[2].getContentType().getMimeType());
        assertEquals("{\"prop1\":123,\"number\":10.56,\"test\":{\"abc\":123},"
                     + "\"array\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\"],\"test123\":"
                     + "[{\"foo\":\"bar\"},{\"foo\":\"bar\"},{\"foo\":\"bar\"}"
                     + ",{\"foo\":\"bar\"}],\"foo\":\"bar\"}", upserts[2].getValue());

        assertEquals("application/json", upserts[3].getContentType().getMimeType());
        assertEquals("[{\"linkName\":\"stream1Link\",\"key\":\"streamkey1234\"},"
                     + "{\"linkName\":\"stream2Link\",\"key\":\"streamkey1235\"}]", upserts[3].getValue());

        assertEquals("application/json", upserts[4].getContentType().getMimeType());
        assertEquals("[{\"description\":\"dns1\",\"subdomain\":\"sub1\",\"ttl\":6000,"
                     + "\"rrtype\":\"A\",\"rdata\":\"1.2.3.4\"},{\"description\":\"dns2\","
                     + "\"subdomain\":\"sub2\",\"ttl\":6000," + "\"rrtype\":\"A\",\"rdata\":\"1.2.3.4\"}]",
                     upserts[4].getValue());

        assertEquals("application/json", upserts[5].getContentType().getMimeType());
        assertEquals("{\"user1\":[\"DISCOVER\"],\"user2\":[\"DISCOVER\",\"ADMIN\",\"CONSUME\"]}",
                     upserts[5].getValue());

        assertEquals("application/json", upserts[6].getContentType().getMimeType());
        assertEquals("{\"group2\":[\"DISCOVER\",\"ADMIN\",\"CONSUME\"],\"group1\":[\"DISCOVER\"]}",
                     upserts[6].getValue());
    }

    @Test
    public void partialUpsert() throws Exception {
        Upsertable<?>[] upserts = DeviceUpsertBuilder
                .create("device1")
                .setName("My new device")
                .setType("device type 1")
                .setUserPermissions(PermissionsBuilder.create()
                                                      .add("user1", Taxonomy.PERMISSION_DISCOVER)
                                                      .add("user2",
                                                           Taxonomy.PERMISSION_DISCOVER,
                                                           Taxonomy.PERMISSION_ADMIN,
                                                           Taxonomy.PERMISSION_CONSUME)
                                                      .build())
                .build();

        assertEquals(3, upserts.length);

        assertEquals("text/plain", upserts[0].getContentType().getMimeType());
        assertEquals("My new device", upserts[0].getValue());

        assertEquals("text/plain", upserts[1].getContentType().getMimeType());
        assertEquals("device type 1", upserts[1].getValue());

        assertEquals("application/json", upserts[2].getContentType().getMimeType());
        assertEquals("{\"user1\":[\"DISCOVER\"],\"user2\":[\"DISCOVER\",\"ADMIN\",\"CONSUME\"]}",
                     upserts[2].getValue());
    }

    @Test
    public void upsertWithRemove() throws Exception {
        Upsertable<?>[] upserts = DeviceUpsertBuilder.create("device1")
                .setName("My new device")
                .setType("device type 1")
                .setUserPermissions(PermissionsBuilder.create()
                                                      .add("user1", Taxonomy.PERMISSION_DISCOVER)
                                                      .add("user2",
                                                           Taxonomy.PERMISSION_DISCOVER,
                                                           Taxonomy.PERMISSION_ADMIN,
                                                           Taxonomy.PERMISSION_CONSUME)
                                                      .build())
                .removeDataStreams()
                .removeDnsRecords()
                .removeMetadata()
                .build();

        assertEquals(6, upserts.length);
        assertEquals("text/plain", upserts[0].getContentType().getMimeType());
        assertEquals("My new device", upserts[0].getValue());

        assertEquals("text/plain", upserts[1].getContentType().getMimeType());
        assertEquals("device type 1", upserts[1].getValue());

        assertEquals("application/json", upserts[2].getContentType().getMimeType());
        assertEquals("{}", upserts[2].getValue());

        assertEquals("application/json", upserts[3].getContentType().getMimeType());
        assertEquals("[]", upserts[3].getValue());

        assertEquals("application/json", upserts[4].getContentType().getMimeType());
        assertEquals("[]", upserts[4].getValue());

        assertEquals("application/json", upserts[5].getContentType().getMimeType());
        assertEquals("{\"user1\":[\"DISCOVER\"],\"user2\":[\"DISCOVER\",\"ADMIN\",\"CONSUME\"]}",
                     upserts[5].getValue());
    }

}
