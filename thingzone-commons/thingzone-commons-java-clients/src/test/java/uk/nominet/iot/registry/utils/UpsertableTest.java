/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry.utils;

import com.google.gson.JsonObject;
import org.apache.http.entity.ContentType;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.*;

public class UpsertableTest {

    @Test
    public void upsertableString() throws Exception {
        Upsertable<String> upsertable = Upsertable.of("name",ContentType.TEXT_PLAIN, "Hello");

        assertEquals("name", upsertable.getName());
        assertEquals("Hello", upsertable.getValue());
        assertFalse(upsertable.toRemove());
    }

    @Test
    public void upsertableJsonObject() throws Exception {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("foo", "bar");

        Upsertable<String> upsertable = Upsertable.of("name",ContentType.TEXT_PLAIN, jsonObject.toString());

        assertEquals("name", upsertable.getName());
        assertEquals("{\"foo\":\"bar\"}", upsertable.getValue());
        assertFalse(upsertable.toRemove());
    }

    @Test
    public void upsertableRemoveString() throws Exception {
        Upsertable<String> upsertable = Upsertable.remove("name",ContentType.TEXT_PLAIN, String::new);

        assertEquals("name", upsertable.getName());
        assertTrue(upsertable.toRemove());
        assertEquals("", upsertable.getValue());
    }

    @Test
    public void upsertableRemoveJsonObject() throws Exception {
        Upsertable<String> upsertable = Upsertable.remove("name", ContentType.TEXT_PLAIN, JsonObject::new);

        assertEquals("name", upsertable.getName());
        assertTrue(upsertable.toRemove());
        assertEquals("{}", upsertable.getValue());
    }

    @Test
    public void upsertableRemoveMapObject() throws Exception {
        Upsertable<String> upsertable = Upsertable.remove("name",ContentType.TEXT_PLAIN, HashMap::new);

        assertEquals("name", upsertable.getName());
        assertTrue(upsertable.toRemove());
        assertEquals("{}", upsertable.getValue());
    }

    @Test
    public void upsertableRemoveJsonString() throws Exception {

        Upsertable<String> upsertable = Upsertable.remove("name",ContentType.TEXT_PLAIN, () -> "{}");

        assertEquals("name", upsertable.getName());
        assertTrue(upsertable.toRemove());
        assertEquals("{}", upsertable.getValue());
    }

    @Test
    public void upsertableRemoveArrayString() throws Exception {

        Upsertable<String> upsertable = Upsertable.remove("name",ContentType.TEXT_PLAIN, ArrayList::new);

        assertEquals("name", upsertable.getName());
        assertTrue(upsertable.toRemove());
        assertEquals("[]", upsertable.getValue());
    }

    @Test
    public void upsertableRemoveArrayJsonString() throws Exception {

        Upsertable<String> upsertable = Upsertable.remove("name",ContentType.TEXT_PLAIN, () -> "[]");

        assertEquals("name", upsertable.getName());
        assertTrue(upsertable.toRemove());
        assertEquals("[]", upsertable.getValue());
    }

}
