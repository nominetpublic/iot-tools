/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.URLEncoder;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import uk.nominet.iot.model.registry.RegistryDataStream;
import uk.nominet.iot.model.registry.RegistryDevice;
import uk.nominet.iot.model.registry.RegistryEntity;
import uk.nominet.iot.model.registry.RegistryTransform;
import uk.nominet.iot.registry.stream.StreamBatchHandler;

public class DumpApiTest extends RegistryAPITestBase {

    RegistryAPISession api;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        api = new RegistryAPISession(wireMockRule.url("/"), "test", "test", 1);
    }

    @Test
    public void dumpAll() throws Exception {
        stubFor(get(urlEqualTo("//api/dump/all")).willReturn(aResponse()
           .withHeader("Content-Type", "application/json")
           .withStatus(200)
           .withBody("[\n" + "   {\n"
                     + "      \"device\" : {\n"
                     + "         \"userPermissions\" : {\n"
                     + "            \"user_device_dump_2_1539340994\" : [\n"
                     + "               \"ADMIN\",\n"
                     + "               \"UPLOAD\",\n"
                     + "               \"CONSUME\",\n"
                     + "               \"MODIFY\",\n"
                     + "               \"ANNOTATE\",\n"
                     + "               \"DISCOVER\"\n"
                     + "            ]\n" + "         },\n"
                     + "         \"groupPermissions\" : {},\n"
                     + "         \"metadata\" : {},\n"
                     + "         \"key\" : \"rem5-ox7o-6kpu-mdi6-t6le-qtek.b.b\"\n"
                     + "     }\n" + "   },\n" + "   {\n"
                     + "      \"dataStream\" : {\n"
                     + "         \"metadata\" : {},\n"
                     + "         \"groupPermissions\" : {},\n"
                     + "         \"key\" : \"i5kj-umxr-xiqr-tovv-4f5x-jgez.m.w\",\n"
                     + "         \"userPermissions\" : {\n"
                     + "            \"user_device_dump_2_1539340994\" : [\n"
                     + "               \"ADMIN\",\n"
                     + "               \"UPLOAD\",\n"
                     + "               \"CONSUME\",\n"
                     + "               \"MODIFY\",\n"
                     + "               \"ANNOTATE\",\n"
                     + "               \"DISCOVER\"\n"
                     + "            ]\n" + "         },\n"
                     + "         \"isTransform\" : false\n"
                     + "      }\n" + "   },\n" + "   {\n"
                     + "      \"transform\" : {\n"
                     + "         \"pushSubscribers\" : [\n"
                     + "            {\n"
                     + "               \"uriParams\" : {\n"
                     + "                  \"httpHeaders\" : {\n"
                     + "                     \"SessionHeaderId\" : \"asda\"\n"
                     + "                  }\n"
                     + "               },\n"
                     + "               \"uri\" : \"http://transform-push.example.com\"\n"
                     + "            }\n" + "         ],\n"
                     + "         \"groupPermissions\" : {},\n"
                     + "         \"function\" : {\n"
                     + "            \"name\" : \"testfn\",\n"
                     + "            \"isPublic\" : false\n"
                     + "         },\n"
                     + "         \"key\" : \"ygie-fod7-o3u2-j4hu-na6s-5hqi.u.i\",\n"
                     + "         \"multiplexer\" : false,\n"
                     + "         \"userPermissions\" : {\n"
                     + "            \"user_device_dump_1_1539340994\" : [\n"
                     + "               \"ADMIN\",\n"
                     + "               \"UPLOAD\",\n"
                     + "               \"CONSUME\",\n"
                     + "               \"MODIFY\",\n"
                     + "               \"ANNOTATE\",\n"
                     + "               \"DISCOVER\"\n"
                     + "            ]\n" + "         },\n"
                     + "         \"metadata\" : {},\n"
                     + "         \"sources\" : [\n"
                     + "            {\n"
                     + "               \"aggregationSpec\" : null,\n"
                     + "               \"trigger\" : true,\n"
                     + "               \"source\" : \"mzdu-gdds-unfb-cdet-q2uz-flg5.u.v\",\n"
                     + "               \"aggregationType\" : null,\n"
                     + "               \"alias\" : \"my favourite stream\"\n"
                     + "            }\n" + "         ],\n"
                     + "         \"devices\" : [\n"
                     + "            {\n"
                     + "               \"key\" : \"c5g5-qzu6-f5ud-ktzk-lvkn-qyaz.s.i\",\n"
                     + "               \"linkName\" : \"t1\"\n"
                     + "            }\n" + "         ],\n"
                     + "         \"parameterValues\" : {\n"
                     + "            \"value\" : \"foo\"\n"
                     + "         },\n"
                     + "         \"outputStreams\" : [\n"
                     + "            {\n"
                     + "               \"key\" : \"44jm-5fnu-itdi-a7gz-dfwl-qexv.h.w\",\n"
                     + "               \"alias\" : \"output2\"\n"
                     + "            }\n" + "         ],\n"
                     + "         \"runSchedule\" : \"\"\n"
                     + "      }\n" + "   },\n" + "   {\n"
                     + "      \"result\" : \"ok\"\n"
                     + "   }\n" + "]")));
        StreamBatchHandler all = api.dump.all(5);
        List<RegistryEntity> records = new ArrayList<>();
        try {
            all.forEachBatch((batches, sequence) -> {
                for (RegistryEntity batch : batches) {
                    records.add(batch);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        RegistryDevice device = (RegistryDevice) records.get(0);
        assertEquals("rem5-ox7o-6kpu-mdi6-t6le-qtek.b.b", device.getKey());

        RegistryDataStream datastream = (RegistryDataStream) records.get(1);
        assertEquals("i5kj-umxr-xiqr-tovv-4f5x-jgez.m.w", datastream.getKey());

        RegistryTransform transform = (RegistryTransform) records.get(2);
        assertEquals("ygie-fod7-o3u2-j4hu-na6s-5hqi.u.i", transform.getKey());
        assertEquals("mzdu-gdds-unfb-cdet-q2uz-flg5.u.v", transform.getSources().get(0).getSource());
        assertEquals("foo", transform.getParameterValues().get("value"));
    }

    @Test
    public void dumpAllInvalidPrivilege() throws Exception {
        stubFor(get(urlEqualTo("//api/dump/all")).willReturn(aResponse()
               .withHeader("Content-Type", "application/json")
               .withStatus(200)
               .withBody("{\n" + "   \"result\" : \"fail\",\n"
                         + "   \"reason\" : \"authentication error\"\n"
                         + "}\n")));

        StreamBatchHandler all = api.dump.all(5);
        List<RegistryEntity> records = new ArrayList<>();
        try {
            all.forEachBatch((batches, sequence) -> {
                for (RegistryEntity batch : batches) {
                    records.add(batch);
                }
            });
            fail();
        } catch (Exception e) {
            assertEquals("authentication error", e.getMessage());
        }
    }

    @Test
    public void dumpDatastream() throws Exception {
        stubFor(get(urlEqualTo("/api/dump/datastream?dataStream=pojb-ugsj-nz4i-6qr3-lzuw-pz3g.g.2"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n"
                        + "   \"result\" : \"ok\",\n"
                        + "   \"dataStream\" : {\n"
                        + "      \"userPermissions\" : {\n"
                        + "         \"user_device_dump_1_1539357234\" : [\n"
                        + "            \"ADMIN\",\n"
                        + "            \"UPLOAD\",\n"
                        + "            \"CONSUME\",\n"
                        + "            \"MODIFY\",\n"
                        + "            \"ANNOTATE\",\n"
                        + "            \"DISCOVER\"\n"
                        + "         ]\n" + "      },\n"
                        + "      \"isTransform\" : false,\n"
                        + "      \"key\" : \"pojb-ugsj-nz4i-6qr3-lzuw-pz3g.g.2\",\n"
                        + "      \"groupPermissions\" : {},\n"
                        + "      \"metadata\" : {}\n"
                        + "   }\n"
                        + "}\n")));

        RegistryDataStream datastream = api.dump.datastream("pojb-ugsj-nz4i-6qr3-lzuw-pz3g.g.2");
        assertEquals("pojb-ugsj-nz4i-6qr3-lzuw-pz3g.g.2", datastream.getKey());
        assertTrue(datastream.getUserPermissions().containsKey("user_device_dump_1_1539357234"));
    }

    @Test
    public void dumpChanges() throws Exception {
        Instant now = Instant.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ").withZone(ZoneId.of("UTC"));
        String formattedDate = URLEncoder.encode(formatter.format(now), "UTF-8");
        stubFor(get(urlEqualTo("/api/changes/" + formattedDate)).willReturn(aResponse()
               .withHeader("Content-Type", "application/json")
               .withStatus(200)
               .withBody("[\n"
                     + "{\n"
                     + "      \"dataStream\" : {\n"
                     + "         \"userPermissions\" : {\n"
                     + "            \"user_device_dump_1_1539357234\" : [\n"
                     + "               \"ADMIN\",\n"
                     + "               \"UPLOAD\",\n"
                     + "               \"CONSUME\",\n"
                     + "               \"MODIFY\",\n"
                     + "               \"ANNOTATE\",\n"
                     + "               \"DISCOVER\"\n"
                     + "            ],\n"
                     + "            \"user_device_dump_2_1539357234\" : [\n"
                     + "               \"ADMIN\",\n"
                     + "               \"UPLOAD\",\n"
                     + "               \"CONSUME\",\n"
                     + "               \"MODIFY\",\n"
                     + "               \"ANNOTATE\",\n"
                     + "               \"DISCOVER\"\n"
                     + "            ],\n"
                     + "            \"public\" : [\n"
                     + "               \"DISCOVER\"\n"
                     + "            ]\n"
                     + "         },\n"
                     + "         \"key\" : \"aos7-v3ug-kxzb-sj44-2im5-rmz3.c.a\",\n"
                     + "         \"isTransform\" : false,\n"
                     + "         \"metadata\" : {},\n"
                     + "         \"groupPermissions\" : {},\n"
                     + "         \"devices\" : [\n"
                     + "            {\n"
                     + "               \"linkName\" : \"s2\",\n"
                     + "               \"key\" : \"xw5b-tbw2-r3pc-iqmn-zbss-emtu.l.d\"\n"
                     + "            }\n"
                     + "         ]\n"
                     + "      },\n"
                     + "      \"key\" : \"aos7-v3ug-kxzb-sj44-2im5-rmz3.c.a\",\n"
                     + "      \"entityType\" : \"DATASTREAM\"\n"
                     + "   },\n"
                     + "   {\n"
                     + "      \"key\" : \"n7h6-btyy-zbhk-chlr-hbnc-35ne.d.r\",\n"
                     + "      \"entityType\" : \"DEVICE\",\n"
                     + "      \"device\" : {\n"
                     + "         \"userPermissions\" : {\n"
                     + "            \"user_device_dump_1_1539357234\" : [\n"
                     + "               \"ADMIN\",\n"
                     + "               \"UPLOAD\",\n"
                     + "               \"CONSUME\",\n"
                     + "               \"MODIFY\",\n"
                     + "               \"ANNOTATE\",\n"
                     + "               \"DISCOVER\"\n"
                     + "            ]\n"
                     + "         },\n"
                     + "         \"key\" : \"n7h6-btyy-zbhk-chlr-hbnc-35ne.d.r\",\n"
                     + "         \"type\" : \"new_type\",\n"
                     + "         \"metadata\" : {},\n"
                     + "         \"groupPermissions\" : {}\n"
                     + "      }\n"
                     + "   },\n"
                     + "   {\n"
                     + "      \"transform\" : {\n"
                     + "         \"key\" : \"qcfx-ohqj-xisn-hqur-xd7p-zy2b.p.z\",\n"
                     + "         \"multiplexer\" : false,\n"
                     + "         \"outputStreams\" : [\n"
                     + "            {\n"
                     + "               \"alias\" : \"output2\",\n"
                     + "               \"key\" : \"aos7-v3ug-kxzb-sj44-2im5-rmz3.c.a\"\n"
                     + "            }\n"
                     + "         ],\n"
                     + "         \"devices\" : [\n"
                     + "            {\n"
                     + "               \"linkName\" : \"t1\",\n"
                     + "               \"key\" : \"xw5b-tbw2-r3pc-iqmn-zbss-emtu.l.d\"\n"
                     + "            }\n"
                     + "         ],\n"
                     + "         \"sources\" : [\n"
                     + "            {\n"
                     + "               \"alias\" : \"my favourite stream\",\n"
                     + "               \"aggregationSpec\" : null,\n"
                     + "               \"trigger\" : true,\n"
                     + "               \"source\" : \"wot2-onvp-dml6-bwfz-q2cr-wlki.k.i\",\n"
                     + "               \"aggregationType\" : null\n"
                     + "            }\n"
                     + "         ],\n"
                     + "         \"groupPermissions\" : {\n"
                     + "            \"group_dump_1_1539357234\" : [\n"
                     + "               \"CONSUME\",\n"
                     + "               \"MODIFY\",\n"
                     + "               \"DISCOVER\"\n"
                     + "            ]\n"
                     + "         },\n"
                     + "         \"runSchedule\" : \"\",\n"
                     + "         \"pushSubscribers\" : [\n"
                     + "            {\n"
                     + "               \"uriParams\" : {\n"
                     + "                  \"httpHeaders\" : {\n"
                     + "                     \"SessionHeaderId\" : \"asda\"\n"
                     + "                  }\n"
                     + "               },\n"
                     + "               \"uri\" : \"http://transform-push.example.com\"\n"
                     + "            }\n"
                     + "         ],\n"
                     + "         \"parameterValues\" : {\n"
                     + "            \"parameter1\" : \"foo\",\n"
                     + "            \"parameter2\" : 42\n"
                     + "         },\n"
                     + "         \"userPermissions\" : {\n"
                     + "            \"user_device_dump_1_1539357234\" : [\n"
                     + "               \"ADMIN\",\n"
                     + "               \"UPLOAD\",\n"
                     + "               \"CONSUME\",\n"
                     + "               \"MODIFY\",\n"
                     + "               \"ANNOTATE\",\n"
                     + "               \"DISCOVER\"\n"
                     + "            ]\n"
                     + "         },\n"
                     + "         \"metadata\" : {},\n"
                     + "         \"function\" : {\n"
                     + "            \"name\" : \"testfn\",\n"
                     + "            \"isPublic\" : true\n"
                     + "         }\n"
                     + "      },\n"
                     + "      \"key\" : \"qcfx-ohqj-xisn-hqur-xd7p-zy2b.p.z\",\n"
                     + "      \"entityType\" : \"DATASTREAM\"\n"
                     + "   },\n"
                     + "   {\n"
                     + "      \"removedUserPermissions\" : [\n"
                     + "         \"user_device_dump_2_1539357234\"\n"
                     + "      ],\n"
                     + "      \"key\" : \"xw5b-tbw2-r3pc-iqmn-zbss-emtu.l.d\",\n"
                     + "      \"removedGroupPermissions\" : [\n"
                     + "         \"group_dump_1_1539357234\"\n"
                     + "      ],\n"
                     + "      \"device\" : {\n"
                     + "         \"userPermissions\" : {\n"
                     + "            \"user_device_dump_1_1539357234\" : [\n"
                     + "               \"ADMIN\",\n"
                     + "               \"UPLOAD\",\n"
                     + "               \"CONSUME\",\n"
                     + "               \"MODIFY\",\n"
                     + "               \"ANNOTATE\",\n"
                     + "               \"DISCOVER\"\n"
                     + "            ]\n"
                     + "         },\n"
                     + "         \"key\" : \"xw5b-tbw2-r3pc-iqmn-zbss-emtu.l.d\",\n"
                     + "         \"dnsRecords\" : [\n"
                     + "            {\n"
                     + "               \"rrtype\" : \"A\",\n"
                     + "               \"rdata\" : \"1.2.3.4\",\n"
                     + "               \"description\" : \"dns on device\",\n"
                     + "               \"subdomain\" : \"arse\",\n"
                     + "               \"ttl\" : 600\n"
                     + "            }\n"
                     + "         ],\n"
                     + "         \"metadata\" : {},\n"
                     + "         \"groupPermissions\" : {},\n"
                     + "         \"dataStreams\" : [\n"
                     + "            {\n"
                     + "               \"key\" : \"aos7-v3ug-kxzb-sj44-2im5-rmz3.c.a\",\n"
                     + "               \"linkName\" : \"s2\"\n"
                     + "            },\n"
                     + "            {\n"
                     + "               \"linkName\" : \"t1\",\n"
                     + "               \"key\" : \"qcfx-ohqj-xisn-hqur-xd7p-zy2b.p.z\"\n"
                     + "            },\n"
                     + "            {\n"
                     + "               \"key\" : \"wot2-onvp-dml6-bwfz-q2cr-wlki.k.i\",\n"
                     + "               \"linkName\" : \"s1\"\n"
                     + "            }\n"
                     + "         ]\n"
                     + "      },\n"
                     + "      \"entityType\" : \"DEVICE\"\n"
                     + "   },\n"
                     + "   {\n"
                     + "      \"result\" : \"ok\"\n"
                     + "   }\n"
                     + "]\n")));

        StreamBatchHandler all = api.dump.changes(now, 2);
        List<RegistryEntity> records = new ArrayList<>();
        try {
            all.forEachBatch((batches, sequence) -> {
                for (RegistryEntity batch : batches) {
                    records.add(batch);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        RegistryDataStream datastream = (RegistryDataStream) records.get(0);
        assertEquals("aos7-v3ug-kxzb-sj44-2im5-rmz3.c.a", datastream.getKey());
        assertEquals("xw5b-tbw2-r3pc-iqmn-zbss-emtu.l.d", datastream.getDevices().get(0).getKey());

        RegistryDevice device = (RegistryDevice) records.get(1);
        assertEquals("n7h6-btyy-zbhk-chlr-hbnc-35ne.d.r", device.getKey());
        assertEquals("new_type", device.getType());

        RegistryTransform transform = (RegistryTransform) records.get(2);
        assertEquals("qcfx-ohqj-xisn-hqur-xd7p-zy2b.p.z", transform.getKey());
        assertEquals("aos7-v3ug-kxzb-sj44-2im5-rmz3.c.a", transform.getOutputStreams().get(0).getKey());

        RegistryDevice device2 = (RegistryDevice) records.get(3);
        assertEquals("xw5b-tbw2-r3pc-iqmn-zbss-emtu.l.d", device2.getKey());
        assertTrue(device2.getGroupPermissions().get("group_dump_1_1539357234").isEmpty());
        assertTrue(device2.getUserPermissions().get("user_device_dump_2_1539357234").isEmpty());
        assertEquals(6, device2.getUserPermissions().get("user_device_dump_1_1539357234").size());
    }


    @Test
    public void dumpChangesStreamError() throws Exception {
        Instant now = Instant.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ").withZone(ZoneId.of("UTC"));
        String formattedDate = URLEncoder.encode(formatter.format(now), "UTF-8");
        stubFor(get(urlEqualTo("/api/changes/" + formattedDate)).willReturn(aResponse()
               .withHeader("Content-Type", "application/json")
               .withStatus(200)
               .withBody("[\n"
                     + "  {\n"
                     + "      \"dataStream\" : {\n"
                     + "         \"userPermissions\" : {\n"
                     + "            \"user_device_dump_1_1539357234\" : [\n"
                     + "               \"ADMIN\",\n"
                     + "               \"UPLOAD\",\n"
                     + "               \"CONSUME\",\n"
                     + "               \"MODIFY\",\n"
                     + "               \"ANNOTATE\",\n"
                     + "               \"DISCOVER\"\n"
                     + "            ],\n"
                     + "            \"user_device_dump_2_1539357234\" : [\n"
                     + "               \"ADMIN\",\n"
                     + "               \"UPLOAD\",\n"
                     + "               \"CONSUME\",\n"
                     + "               \"MODIFY\",\n"
                     + "               \"ANNOTATE\",\n"
                     + "               \"DISCOVER\"\n"
                     + "            ],\n"
                     + "            \"public\" : [\n"
                     + "               \"DISCOVER\"\n"
                     + "            ]\n"
                     + "         },\n"
                     + "         \"key\" : \"aos7-v3ug-kxzb-sj44-2im5-rmz3.c.a\",\n"
                     + "         \"isTransform\" : false,\n"
                     + "         \"metadata\" : {},\n"
                     + "         \"groupPermissions\" : {},\n"
                     + "         \"devices\" : [\n"
                     + "            {\n"
                     + "               \"linkName\" : \"s2\",\n"
                     + "               \"key\" : \"xw5b-tbw2-r3pc-iqmn-zbss-emtu.l.d\"\n"
                     + "            }\n"
                     + "         ]\n"
                     + "      },\n"
                     + "      \"key\" : \"aos7-v3ug-kxzb-sj44-2im5-rmz3.c.a\",\n"
                     + "      \"entityType\" : \"DATASTREAM\"\n"
                     + "   }\n")));

        StreamBatchHandler all = api.dump.changes(now, 5);
        List<RegistryEntity> records = new ArrayList<>();
        try {
            all.forEachBatch((batches, sequence) -> {
                for (RegistryEntity batch : batches) {
                    records.add(batch);
                }
            });
            fail();
        } catch (Exception e) {
            // error because stream was missing the ending ] so was invalid json
            e.printStackTrace();
        }
    }
}
