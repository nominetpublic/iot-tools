/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.registry;

import org.junit.Test;
import uk.nominet.iot.model.registry.RegistrySession;
import uk.nominet.iot.registry.exceptions.RegistryFailResponseException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.*;

public class SessionAPITest extends RegistryAPITestBase {

    @Test
    public void login() throws Exception {
        uk.nominet.iot.registry.RegistryAPISession api =
                new RegistryAPISession(wireMockRule.url("/"), "test", "test");
        api.invalidateCache();

        assertNotNull(api.getSession());
        assertEquals("XU2OBHoFm-Uc0uk0g3ttCA", api.getSession().getSessionKey());
        assertEquals(899998, api.getSession().getTtl());
        assertEquals("test", api.getSession().getUser().getUsername());
        assertEquals("bar", api.getSession().getUser().getDetails().get("foo"));
        assertEquals("foo", api.getSession().getUser().getPreferences().get("bar"));
    }

    @Test
    public void loginFailWrongUsername() {
        uk.nominet.iot.registry.RegistryAPISession api =
                new RegistryAPISession(wireMockRule.url("/"), "test1", "test1");

        try {
            api.getSession();
            fail();
        } catch (RegistryFailResponseException ex) {
            assertFalse(ex.isSessionFailure());
            assertEquals(400, ex.getStatusCode());
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }

    }

    @Test
    public void keepalive() throws Exception {
        uk.nominet.iot.registry.RegistryAPISession api =
                new RegistryAPISession(wireMockRule.url("/"), "test", "test");
        api.invalidateCache();

        api.session.keepalive();
    }

    @Test
    public void keepaliveInvalidSession() throws Exception {
        uk.nominet.iot.registry.RegistryAPISession api =
                new RegistryAPISession(wireMockRule.url("/"), "test", "test", 1);

        stubFor(post(urlEqualTo("/api/session/keepalive"))
                .withHeader("X-FollyBridge-SessionKey", matching("invalidsessionkey"))
                .willReturn(getSessionFailResponse("POST", "/api/session/keepalive"))
        );

        api.getSession().setSessionKey("invalidsessionkey");
        assertInvalidSession(() -> api.session.keepalive());
    }

    @Test
    public void logout() throws Exception {
        uk.nominet.iot.registry.RegistryAPISession api =
                new RegistryAPISession(wireMockRule.url("/"), "test", "test");
        api.invalidateCache();
        api.session.logout();
        assertNull(api.getSession().getSessionKey());
    }

    @Test
    public void logoutInvalidSession() throws Exception {
        uk.nominet.iot.registry.RegistryAPISession api =
                new RegistryAPISession(wireMockRule.url("/"), "test", "test");
        api.getSession().setSessionKey("invalidsessionkey");

        try {
            api.session.logout();
            fail();
        } catch (RegistryFailResponseException ex) {
            assertTrue(ex.isSessionFailure());
            assertEquals(401, ex.getStatusCode());
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void sessionCaches() throws Exception {
        stubFor(post(urlEqualTo("/api/session/login"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "    \"result\": \"ok\",\n" +
                                "    \"sessionKey\": \"XU2OBHoFm-Uc0uk0g3ttCA\",\n" +
                                "    \"ttl\": 899998,\n" +
                                "    \"user\": {\n" +
                                "        \"username\": \"test\",\n" +
                                "        \"details\": {},\n" +
                                "        \"preferences\": {}\n" +
                                "    }\n" +
                                "}")));
        RegistryAPISession api = new RegistryAPISession(wireMockRule.url("/"), "test", "test");

        RegistrySession session1 = api.getSession();

        stubFor(post(urlEqualTo("/api/session/login"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "    \"result\": \"ok\",\n" +
                                "    \"sessionKey\": \"XU23456oFm-Uc345645ttCA\",\n" +
                                "    \"ttl\": 899998,\n" +
                                "    \"user\": {\n" +
                                "        \"username\": \"test\",\n" +
                                "        \"details\": {},\n" +
                                "        \"preferences\": {}\n" +
                                "    }\n" +
                                "}")));
        RegistrySession session2 = api.getSession();
        assertEquals(session1, session2);
    }


    @Test
    public void sessionInvalidates() throws Exception {
        stubFor(post(urlEqualTo("/api/session/login"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "    \"result\": \"ok\",\n" +
                                "    \"sessionKey\": \"XU2OBHoFm-Uc0uk0g3ttCA\",\n" +
                                "    \"ttl\": 899998,\n" +
                                "    \"user\": {\n" +
                                "        \"username\": \"test\",\n" +
                                "        \"details\": {},\n" +
                                "        \"preferences\": {}\n" +
                                "    }\n" +
                                "}")));
        RegistryAPISession api = new RegistryAPISession(wireMockRule.url("/"), "test", "test");
        api.invalidateSessionKey();

        RegistrySession session1 = api.getSession();

        stubFor(post(urlEqualTo("/api/session/login"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody("{\n" +
                                "    \"result\": \"ok\",\n" +
                                "    \"sessionKey\": \"XU23456oFm-Uc345645ttCA\",\n" +
                                "    \"ttl\": 899998,\n" +
                                "    \"user\": {\n" +
                                "        \"username\": \"test\",\n" +
                                "        \"details\": {},\n" +
                                "        \"preferences\": {}\n" +
                                "    }\n" +
                                "}")));
        RegistrySession session2 = api.getSession();
        assertEquals(session1, session2);
    }
}
