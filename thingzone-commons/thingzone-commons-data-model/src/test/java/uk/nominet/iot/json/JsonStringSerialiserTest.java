/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.json;

import org.junit.Test;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;
import uk.nominet.iot.model.timeseries.Image;
import uk.nominet.iot.model.timeseries.MetricDouble;
import uk.nominet.iot.model.timeseries.MetricInt;

import java.time.Instant;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class JsonStringSerialiserTest {
    private JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    private class DummyBean extends AbstractTimeseriesPoint {
        String testValue;
        
        public String getTestValue() {
            return testValue;
        }
        public void setTestValue(String testValue) {
            this.testValue = testValue;
        }
    }

    private class Message {
        AbstractTimeseriesPoint imagePoint;
        AbstractTimeseriesPoint intPoint;
        AbstractTimeseriesPoint doublePoint;

        public AbstractTimeseriesPoint getImagePoint() {
            return imagePoint;
        }

        public void setImagePoint(AbstractTimeseriesPoint imagePoint) {
            this.imagePoint = imagePoint;
        }

        public AbstractTimeseriesPoint getIntPoint() {
            return intPoint;
        }

        public void setIntPoint(AbstractTimeseriesPoint intPoint) {
            this.intPoint = intPoint;
        }

        public AbstractTimeseriesPoint getDoublePoint() {
            return doublePoint;
        }

        public void setDoublePoint(AbstractTimeseriesPoint doublePoint) {
            this.doublePoint = doublePoint;
        }

        @Override
        public String toString() {
            return "Message{" +
                   "imagePoint=" + imagePoint +
                   ", intPoint=" + intPoint +
                   ", doublePoint=" + doublePoint +
                   '}';
        }
    }

    @Test
    public void canSerialiseAndDeserialise() throws Exception {
       jsonStringSerialiser.register(DummyBean.class);

        UUID id = UUID.randomUUID();
        DummyBean dummyBean = new DummyBean();
        dummyBean.setId(id);
        dummyBean.setStreamKey("stream123");
        dummyBean.setTimestamp(Instant.parse("2017-07-18T16:04:54.000Z"));
        dummyBean.setTestValue("test value");
        String serialisedObejct = jsonStringSerialiser.writeObject(dummyBean);
        DummyBean deserialisedObject = jsonStringSerialiser.readObject(serialisedObejct, DummyBean.class);
    }

    @Test
    public void schemaValidationFails() throws Exception {
        jsonStringSerialiser.register(DummyBean.class);

        DummyBean dummyBean = new DummyBean();
        //dummyBean.setId("test123");
        dummyBean.setStreamKey("stream123");
        dummyBean.setTimestamp(Instant.parse("2017-07-18T16:04:54.000Z"));
        dummyBean.setTestValue("test value");
        try {
            jsonStringSerialiser.writeValidObject(dummyBean);
            fail();
        } catch (Exception e) {
            assertEquals("Schema validation exception: {\"pointerToViolation\":\"#\"," +
                    "\"causingExceptions\":[],\"keyword\":\"required\",\"message\":" +
                    "\"required key [id] not found\"}", e.getMessage());
        }
    }

    @Test
    public void canSerialiseAndDeserialiseWitoutRegistering() throws Exception {
        DummyBean dummyBean = new DummyBean();
        UUID id = UUID.randomUUID();
        dummyBean.setId(id);
        dummyBean.setStreamKey("stream123");
        dummyBean.setTimestamp(Instant.parse("2017-07-18T16:04:54.000Z"));
        dummyBean.setTestValue("test value");
        String serialisedObejct = jsonStringSerialiser.writeObject(dummyBean);
        System.out.println(serialisedObejct);
        DummyBean deserialisedObject = jsonStringSerialiser.readObject(serialisedObejct, DummyBean.class);
        System.out.println(deserialisedObject);
    }

    @Test
    public void nullObject() throws Exception {
        DummyBean dummyBean = new DummyBean();
        UUID id = UUID.randomUUID();
        dummyBean.setId(id);
        dummyBean.setStreamKey("stream123");
        dummyBean.setTimestamp(Instant.parse("2017-07-18T16:04:54.000Z"));
        dummyBean.setTestValue("test value");
        try {
            jsonStringSerialiser.writeObject(null);
            fail();
        } catch (Exception e) {
            assertEquals("Object cannot be null", e.getMessage());
        }
    }

    @Test
    public void ownValidator() throws Exception {
        JsonValidator validator = new JsonValidator();
        validator.register(DummyBean.class);

        jsonStringSerialiser.register(DummyBean.class);

        DummyBean dummyBean = new DummyBean();
        dummyBean.setStreamKey("stream123");
        dummyBean.setTimestamp(Instant.parse("2017-07-18T16:04:54.000Z"));
        dummyBean.setTestValue("test value");
        try {
            jsonStringSerialiser.writeValidObject(dummyBean);
            fail();
        } catch (Exception e) {
            assertEquals("Schema validation exception: {\"pointerToViolation\":\"#\"," +
                    "\"causingExceptions\":[],\"keyword\":\"required\",\"message\":" +
                    "\"required key [id] not found\"}", e.getMessage());
        }
    }

    @Test
    public void canSerialiseTimeseriesPoint() throws Exception {
        Image image = new Image();
        image.setImageRef("abcd");
        image.setStreamKey("stream1");
        image.setId(UUID.randomUUID());
        image.setTimestamp(Instant.now());

        MetricInt metricInt = new MetricInt();
        metricInt.setValue(123);
        metricInt.setStreamKey("stream2");
        metricInt.setId(UUID.randomUUID());
        metricInt.setTimestamp(Instant.now());

        MetricDouble metricDouble = new MetricDouble();
        metricDouble.setValue(1.23);
        metricDouble.setStreamKey("stream3");
        metricDouble.setId(UUID.randomUUID());
        metricDouble.setTimestamp(Instant.now());

        Message message = new Message();
        message.setImagePoint(image);
        message.setIntPoint(metricInt);
        message.setDoublePoint(metricDouble);

        String json = jsonStringSerialiser.writeObject(message);


        Message decodedMessage = jsonStringSerialiser.readObject(json, Message.class);


        assertEquals(Image.class, decodedMessage.imagePoint.getClass());
        assertEquals(MetricDouble.class, decodedMessage.doublePoint.getClass());
        assertEquals(MetricInt.class, decodedMessage.intPoint.getClass());

    }
}
