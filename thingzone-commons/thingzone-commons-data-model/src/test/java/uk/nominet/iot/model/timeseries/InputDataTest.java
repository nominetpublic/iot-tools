/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.timeseries;

import com.google.common.collect.ImmutableList;
import org.junit.Test;
import uk.nominet.iot.json.JsonStringSerialiser;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class InputDataTest {

    @Test
    public void generateData() {
        InputData data = new InputData();
        data.setId(UUID.randomUUID());
        data.setStreamKey("0b35144b3435364b.event");
        data.setTimestamp(Instant.now());
        data.setTags(ImmutableList.of("tag1", "tag2"));

        Map<String, Object> values = new HashMap<>();
        values.put("timestamp", 1584726227);
        values.put("event_code", 1);
        values.put("temperature_degc", 26.3125);
        values.put("event_number", 1);
        values.put("image_number", 0);
        values.put("streamKey", "0b35144b3435364b.event");
        data.setValue(values);

        JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
        System.out.println(jsonStringSerialiser.writeObject(data));
     }

    @Test
    public void generateData1() {
        InputData data = new InputData();
        data.setId(UUID.randomUUID());
        data.setStreamKey("0b35144b3435364b.images");
        data.setTimestamp(Instant.now());
        data.setTags(ImmutableList.of("tag1", "tag2"));

        Map<String, Object> values = new HashMap<>();
        values.put("image_ref", "W45BLTISO34");
        values.put("image_number", 0);
        values.put("event_number", 1);
        values.put("streamKey", "0b35144b3435364b.images");
        data.setValue(values);


        JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
        System.out.println(jsonStringSerialiser.writeObject(data));
    }
}
