/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.timeseries;

import com.github.filosganga.geogson.model.Point;
import com.github.filosganga.geogson.model.positions.SinglePosition;
import com.google.common.collect.ImmutableList;
import org.junit.Ignore;
import org.junit.Test;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.DerivedRelation;
import uk.nominet.iot.model.Feedback;
import uk.nominet.iot.model.QualityIndicator;
import uk.nominet.iot.model.RelatedRelation;
import uk.nominet.iot.model.registry.RegistryDevice;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Ignore
public class EventTest {
    private static JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    @Test
    public void generateMetric() throws Exception {
        MetricInt metricInt = new MetricInt();
        metricInt.setTimestamp(Instant.now());
        metricInt.setStreamKey("oo09oxb.sim1-ss");
        metricInt.setValue(87);
        metricInt.setId(UUID.randomUUID());
        metricInt.setMetadata(jsonStringSerialiser.getDefaultSerialiser().fromJson("{\"foo\":\"bar\"}", Map.class));

        System.out.println(jsonStringSerialiser.writeObject(metricInt));

    }

    @Test
    public void generateEventJson() throws Exception {
        Event event = new Event();
        event.setStreamKey("device1.stream1");
        event.setId(UUID.randomUUID());
        event.setEventType(ThingzoneEventType.GENERIC);
        event.setSeverity(0.7);
        event.setLocation(new Point(new SinglePosition(-1.227938, 51.658642, 0)));

        event.setName("Sample Event");
        event.setDescription("Sample event");
        event.setCategory("Event Category");
        event.setReceivers(ImmutableList.of("user1", "user2"));
        event.setType("Event type");

        event.setStatus("event status");

        event.setValue(0.8);


        event.setMetadata(jsonStringSerialiser.getDefaultSerialiser().fromJson("{\"foo\":\"bar\"}", Map.class));


        event.setTags(ImmutableList.of("tag1", "tag2"));

        //Time
        event.setStartTimestamp(Instant.now().minus(10, ChronoUnit.MINUTES));
        event.setEndTimestamp(Instant.now());
        event.setTimestamp(Instant.now());


        //Quality
        QualityIndicator indicator = new QualityIndicator();
        indicator.setTimestamp(Instant.now());
        indicator.setIndicator("timeliness");
        indicator.setValue(0.8);
        QualityIndicator indicator1 = new QualityIndicator();
        indicator1.setTimestamp(Instant.now());
        indicator1.setIndicator("correctness");
        indicator1.setValue(0.6);
        List<QualityIndicator> quality = new ArrayList<>();
        quality.add(indicator);
        quality.add(indicator1);
        event.setQuality(quality);







        //Feedback
        Feedback feedback = new Feedback();
        feedback.setUser("user1");
        feedback.setCategory("feedback category");
        feedback.setDescription("feedback description");
        feedback.setId(UUID.randomUUID());
        feedback.setTimestamp(Instant.now().plus(5, ChronoUnit.MINUTES));
        feedback.setType("feedback Type");
        List<Feedback> feedbacks = new ArrayList<>();
        feedbacks.add(feedback);
        event.setFeedback(feedbacks);


        //Configuration
        event.setConfiguration(jsonStringSerialiser.getDefaultSerialiser().fromJson("{\"foo\":\"bar\"}", Map.class));



        //Related to
        RelatedRelation relatedRelation1 = new RelatedRelation();
        relatedRelation1.setKlass(RegistryDevice.class.getName());
        relatedRelation1.setKey("device1");
        relatedRelation1.setType("relatedToVehicle");
        relatedRelation1.setDescription("Event about vehicle ");
        List<RelatedRelation> relatedRelations = new ArrayList<>();
        relatedRelations.add(relatedRelation1);
        event.setRelatedTo(relatedRelations);



        //Derived From
        DerivedRelation d1 = new DerivedRelation();
        d1.setStreamKey("device1.stream2");
        d1.setId(UUID.randomUUID());
        d1.setDescription("Event was triggered by another event");
        d1.setType("wasTriggeredBy");
        d1.setType(Event.class.getName());
        d1.setWeight(0.9);

        DerivedRelation d2 = new DerivedRelation();
        d2.setStreamKey("device1.stream3");
        d2.setId(UUID.randomUUID());
        d2.setDescription("Event was triggered by another event");
        d2.setType("wasTriggeredBy");
        d2.setType(Event.class.getName());
        d2.setWeight(0.4);

        DerivedRelation d3 = new DerivedRelation();
        d3.setStreamKey("device2.stream2");
        d3.setId(UUID.randomUUID());
        d3.setDescription("Event was triggered by another event");
        d3.setType("wasTriggeredBy");
        d3.setType(Event.class.getName());
        d3.setWeight(0.6);

        List<DerivedRelation> derivedRelations = ImmutableList.of(d1, d2, d3);
        event.setDerivedFrom(derivedRelations);

        System.out.println(jsonStringSerialiser.writeObject(event));



    }
}
