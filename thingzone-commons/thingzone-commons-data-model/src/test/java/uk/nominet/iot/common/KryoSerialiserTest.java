/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.common;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;


import static org.junit.Assert.*;

public class KryoSerialiserTest {

    @Test
    public void serialise() throws Exception {
        Map<String, Object> objectToSerialise = new HashMap<>();
        objectToSerialise.put("foo", "bar");
        objectToSerialise.put("bar", 123);

        byte[] data = KryoSerialiser.serialise(objectToSerialise);

        Object deserialisedObject = KryoSerialiser.deserialise(data, HashMap.class);
        assertTrue(deserialisedObject instanceof HashMap);
        assertEquals(2, ((HashMap)deserialisedObject).size());
        assertEquals("bar", ((HashMap)deserialisedObject).get("foo"));
        assertEquals(123, ((HashMap)deserialisedObject).get("bar"));
    }

    @Test
    public void serialiseEmpty() throws Exception {
        Map<String, Object> objectToSerialise = new HashMap<>();
        byte[] data = KryoSerialiser.serialise(objectToSerialise);

        Object deserialisedObject = KryoSerialiser.deserialise(data, HashMap.class);
        assertTrue(deserialisedObject instanceof HashMap);
        assertEquals(0, ((HashMap)deserialisedObject).size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void serialiseNull() throws Exception {
        byte[] data = KryoSerialiser.serialise(null);
    }
}
