/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.timeseries;

import static org.junit.Assert.assertEquals;

import com.github.filosganga.geogson.model.Point;
import com.github.filosganga.geogson.model.positions.SinglePosition;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import jnr.ffi.annotations.In;
import org.apache.commons.collections.map.HashedMap;
import org.junit.Test;

import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.DerivedRelation;
import uk.nominet.iot.model.Feedback;
import uk.nominet.iot.model.QualityIndicator;
import uk.nominet.iot.model.RelatedRelation;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;
import uk.nominet.iot.model.core.Classification;

import java.time.Instant;
import java.util.Map;
import java.util.UUID;

public class TimeseriesTest {

    @Test
    public void canIdentifyPointFromJSON() throws Exception {
        String json = "{\n" +
                "\t\"value_int\": 2,\n" +
                "\t\"klass\": \"uk.nominet.iot.model.timeseries.MetricInt\",\n" +
                "\t\"streamKey\": \"stream1\",\n" +
                "\t\"timestamp\": \"1970-01-18T15:41:02.831Z\",\n" +
                "\t\"id\": \"b433a959-57a4-4d98-9299-c464af72c983\",\n" +
                "\t\"metadata\": {\n" +
                "\t\t\"foo\": \"bar\"\n" +
                "\t}\n" +
                "}";

        JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
        AbstractTimeseriesPoint point = jsonStringSerialiser.readObject(json, AbstractTimeseriesPoint.class);

        Object point1 = jsonStringSerialiser.readObject(json, point.klassObject());

        assertEquals(MetricInt.class, point1.getClass());
    }

    @Test
    public void throwaway() {
        JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

        Map<String, Object> metadata = new HashedMap();
        metadata.put("foo", "bar");

        Classification classification1 = new Classification();
        classification1.setLabel("class1");
        classification1.setConfidence(0.9);
        classification1.setCurated(false);
        classification1.setSelection("selection");
        classification1.setOrigin("user1");
        classification1.setTimestamp(Instant.now());

        Classification classification2 = new Classification();
        classification2.setLabel("class2");
        classification2.setConfidence(0.8);
        classification2.setCurated(false);
        classification2.setSelection("selection");
        classification2.setOrigin("user1");
        classification2.setTimestamp(Instant.now());

        QualityIndicator q1 = new QualityIndicator();
        q1.setIndicator("timeliness");
        q1.setTimestamp(Instant.now());
        q1.setValue(0.8);

        QualityIndicator q2 = new QualityIndicator();
        q2.setIndicator("validity");
        q2.setTimestamp(Instant.now());
        q2.setValue(0.9);


        MetricInt metricInt = new MetricInt();
        metricInt.setStreamKey("streamkey123");
        metricInt.setTimestamp(Instant.now());
        metricInt.setId(UUID.randomUUID());
        metricInt.setMetadata(metadata);
        metricInt.setTags(ImmutableList.of("tag1", "tag2"));
        metricInt.setClassifications(ImmutableList.of(classification1, classification2));
        metricInt.setQuality(ImmutableList.of(q1, q2));

        metricInt.setValue(10);


        Image image = new Image();
        image.setImageRef("EFDYYVR124423");


        Json json = new Json();
        json.setValue(metadata);

        Config config = new Config();
        config.setConfig(metadata);
        config.setDerivedFromConfig(UUID.randomUUID());
        config.setOrigin(ThingzoneConfigOrigin.DEVICE);
        config.setRevision("0.1");


        RelatedRelation rr = new RelatedRelation();
        rr.setType("associated image");
        rr.setKlass(Image.class.getName());
        rr.setDescription("relation description");
        rr.setKey(UUID.randomUUID().toString());

        DerivedRelation dr = new DerivedRelation();
        dr.setType("origin alert");
        dr.setDescription("relation description");
        dr.setStreamKey("streamkey123");
        dr.setId(UUID.randomUUID());
        dr.setKlass(Event.class.getName());
        dr.setWeight(0.4);

        Feedback feedback = new Feedback();
        feedback.setUser("user1");
        feedback.setCategory("category1");
        feedback.setDescription("description");
        feedback.setId(UUID.randomUUID());
        feedback.setTimestamp(Instant.now());
        feedback.setType("feedback type");


        Event event = new Event();
        event.setLocation(new Point(new SinglePosition(0, 0, 0)));
        event.setName("event123");
        event.setEventType(ThingzoneEventType.GENERIC); // GENERIC, ALERT, OBSERVATION
        event.setCategory("category1");
        event.setSeverity(0.5);
        event.setDescription("event description");
        event.setReceivers(ImmutableList.of("user1", "user2"));
        event.setType("event type");
        event.setRelatedTo(ImmutableList.of(rr));
        event.setDerivedFrom(ImmutableList.of(dr));
        event.setConfiguration(metadata);
        event.setEndTimestamp(Instant.now());
        event.setStartTimestamp(Instant.now());
        event.setFeedback(ImmutableList.of(feedback));
        event.setValue(0.5);
        event.setStatus("OPEN");

        System.out.println(jsonStringSerialiser.writeObject(event));
    }
}
