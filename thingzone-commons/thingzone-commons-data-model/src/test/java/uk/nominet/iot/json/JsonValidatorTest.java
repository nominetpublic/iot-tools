/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.json;

import org.junit.Before;
import org.junit.Test;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;

import java.time.Instant;
import java.util.UUID;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class JsonValidatorTest {
    private JsonValidator jsonValidator;
    private JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    private class DummyBean extends AbstractTimeseriesPoint {
        String testValue;
        public String getTestValue() {
            return testValue;
        }
        public void setTestValue(String testValue) {
            this.testValue = testValue;
        }
    }

    private class DummyBeanNoSchema extends AbstractTimeseriesPoint {
        String testValue;
        public String getTestValue() {
            return testValue;
        }
        public void setTestValue(String testValue) {
            this.testValue = testValue;
        }
    }

    @Before
    public void setUp() throws Exception {
        jsonValidator = new JsonValidator();
    }

    @Test
    public void validateWellFormedBean() throws Exception {
        DummyBean dummyBean = new DummyBean();
        UUID id = UUID.randomUUID();
        dummyBean.setId(id);
        dummyBean.setStreamKey("stream123");
        dummyBean.setTimestamp(Instant.parse("2017-07-18T16:04:54.000Z"));
        dummyBean.setTestValue("test value");
        String dummyBeanJson = jsonStringSerialiser.writeObject(dummyBean);
        jsonValidator.validate(dummyBeanJson, DummyBean.class);
    }

    @Test
    public void validateMaformedBean() throws Exception {
        DummyBean dummyBean = new DummyBean();
        dummyBean.setStreamKey("stream123");
        dummyBean.setTimestamp(Instant.parse("2017-07-18T16:04:54.000Z"));
        dummyBean.setTestValue("test value");
        String dummyBeanJson = jsonStringSerialiser.writeObject(dummyBean);
        try {
            jsonValidator.validate(dummyBeanJson, DummyBean.class);
            fail();
        } catch (Exception e) {
            assertTrue(e.getMessage().startsWith("Schema validation exception:"));
        }
    }

    @Test
    public void validateNullBean() throws Exception {
        try {
            jsonValidator.validate(null, DummyBean.class);
            fail();
        } catch (Exception e) {
            assertTrue(e.getMessage().startsWith("Input can not be null"));
        }
    }

    @Test
    public void validateObjectWithNoSchemaBean() throws Exception {
        DummyBeanNoSchema dummyBean = new DummyBeanNoSchema();
        dummyBean.setStreamKey("stream123");
        dummyBean.setTimestamp(Instant.parse("2017-07-18T16:04:54.000Z"));
        dummyBean.setTestValue("test value");
        String dummyBeanJson = jsonStringSerialiser.writeObject(dummyBean);

        try {
            jsonValidator.validate(dummyBeanJson, DummyBeanNoSchema.class);
            fail();
        } catch (Exception e) {
            assertTrue(e.getMessage().startsWith("Schema resource DummyBeanNoSchema.json not found"));
        }

    }

}
