/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model;
import com.datastax.driver.mapping.annotations.UDT;

import java.io.Serializable;
import java.util.UUID;


/**
 * Created by edoardo on 01/06/2017.
 */
@UDT(keyspace = "thingzone", name = "derivedRelation")
public class DerivedRelation implements Serializable {
    private static final long serialVersionUID = -4123119665575769796L;

    public String type;
    public String description;
    public Double weight;
    public String klass;
    public String streamKey;
    public UUID id;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKlass() {
        return klass;
    }

    public void setKlass(String klass) {
        this.klass = klass;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getStreamKey() {
        return streamKey;
    }

    public void setStreamKey(String streamKey) {
        this.streamKey = streamKey;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "DerivedRelation{" +
               "type='" + type + '\'' +
               ", description='" + description + '\'' +
               ", weight=" + weight +
               ", klass='" + klass + '\'' +
               ", streamKey='" + streamKey + '\'' +
               ", id=" + id +
               '}';
    }
}
