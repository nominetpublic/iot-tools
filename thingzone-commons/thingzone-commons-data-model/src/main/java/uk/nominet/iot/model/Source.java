/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model;

import com.datastax.driver.mapping.annotations.Field;
import com.datastax.driver.mapping.annotations.UDT;
import uk.nominet.iot.codec.MapToBytesCodec;

import java.util.Map;

@UDT(keyspace = "thingzone", name = "source")
public class Source {
    private String id;
    private String endpoint;

    @Field(codec = MapToBytesCodec.class)
    private Map<String, Object> metadata;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public Map<String, Object> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map <String, Object>metadata) {
        this.metadata = metadata;
    }

    @Override
    public String toString() {
        return "Source{" +
                "id='" + id + '\'' +
                ", endpoint='" + endpoint + '\'' +
                ", metadata=" + metadata +
                '}';
    }
}
