/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.registry;

import uk.nominet.iot.annotations.TzCollection;

import java.util.List;
import java.util.Map;

@TzCollection(indexType = "stream")
public class RegistryDataStream extends RegistryEntity {
    private static final long serialVersionUID = -6338540465695096820L;

    protected List<RegistryDeviceReference> devices;
    protected Map<String, Object> metadata;
    protected boolean isTransform;

    protected List<RegistryPushSubscriber> pushSubscribers;
    private RegistryPullSubscription pullSubscription;

    public List<RegistryDeviceReference> getDevices() {
        return devices;
    }

    public void setDevices(List<RegistryDeviceReference> devices) {
        this.devices = devices;
    }

    public Map<String, Object> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, Object> metadata) {
        this.metadata = metadata;
    }

    public boolean isTransform() {
        return isTransform;
    }

    public void setTransform(boolean transform) {
        isTransform = transform;
    }

    public List<RegistryPushSubscriber> getPushSubscribers() {
        return pushSubscribers;
    }

    public void setPushSubscribers(List<RegistryPushSubscriber> pushSubscribers) {
        this.pushSubscribers = pushSubscribers;
    }

    public RegistryPullSubscription getPullSubscription() {
        return pullSubscription;
    }

    public void setPullSubscription(RegistryPullSubscription pullSubscription) {
        this.pullSubscription = pullSubscription;
    }

    @Override
    public String toString() {
        return "RegistryDataStream{" +
                "devices=" + devices +
                ", metadata=" + metadata +
                ", isTransform=" + isTransform +
                ", pushSubscribers=" + pushSubscribers +
                ", pullSubscription=" + pullSubscription +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", dnsRecords=" + dnsRecords +
                ", key='" + key + '\'' +
                ", userPermissions=" + userPermissions +
                ", groupPermissions=" + groupPermissions +
                '}';
    }
}
