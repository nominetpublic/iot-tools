/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.registry;

import uk.nominet.iot.annotations.ProvId;

import java.util.List;
import java.util.Map;

/**
 * Contains the key and the user and group permissions to access that key
 */
public class RegistryKey {
    @ProvId
    protected String key;
    protected Map<String, List<String>> userPermissions;
    protected Map<String, List<String>> groupPermissions;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Map<String, List<String>> getUserPermissions() {
        return userPermissions;
    }

    public void setUserPermissions(Map<String, List<String>> userPermissions) {
        this.userPermissions = userPermissions;
    }

    public Map<String, List<String>> getGroupPermissions() {
        return groupPermissions;
    }

    public void setGroupPermissions(Map<String, List<String>> groupPermissions) {
        this.groupPermissions = groupPermissions;
    }
}
