/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.json;

import com.google.gson.*;
import com.google.gson.internal.Streams;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * Adapts abstract timeseries points to the correct class depending on the klass property
 */
public class TimeseriesTypeAdapterFactory implements TypeAdapterFactory {

    public Gson gson;


    private TypeAdapter<?> getDelegateAdapter(Class<?> srcType) {
        return gson.getDelegateAdapter(this, TypeToken.get(srcType));
    }

    @Override
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {

        if (type.getRawType() != AbstractTimeseriesPoint.class) {
            return null;
        }

        this.gson = gson;

        Map<Class<?>, TypeAdapter<?>> subtypeToDelegate = new LinkedHashMap<>();

        return new TypeAdapter<T>() {
            @Override
            public void write(JsonWriter jsonWriter, T value) throws IOException {
                Class<?> srcType = value.getClass();

                if (!subtypeToDelegate.containsKey(srcType)) {
                    TypeAdapter<?> delegate = getDelegateAdapter(srcType);
                    subtypeToDelegate.put(srcType, delegate);
                }

                @SuppressWarnings("unchecked")
                TypeAdapter<T> delegate = (TypeAdapter<T>) subtypeToDelegate.get(srcType);
                if (delegate == null) {
                    throw new JsonParseException("cannot serialize " + srcType.getName()
                                                 + "; did you forget to register a subtype?");
                }

                JsonObject jsonObject = delegate.toJsonTree(value).getAsJsonObject();

                Streams.write(jsonObject, jsonWriter);
            }

            @Override
            public T read(JsonReader jsonReader) {
                JsonElement jsonElement = Streams.parse(jsonReader);

                if (jsonElement == null) {
                    throw new JsonParseException("cannot deserialize AbstractTimeseriesPoint" +
                                                 " because it does not define a valid JsonElement");
                }

                if (jsonElement.isJsonNull()) return null;

                JsonElement klasslJsonElement = jsonElement.getAsJsonObject().get("klass");

                if (klasslJsonElement == null) {
                    throw new JsonParseException("cannot deserialize AbstractTimeseriesPoint" +
                                                 " because it does not define a field named klass");
                }

                Class<?> destType = null;
                try {
                    destType = Class.forName(klasslJsonElement.getAsString());
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

                if (!subtypeToDelegate.containsKey(destType)) {
                    TypeAdapter<?> delegate = getDelegateAdapter(destType);
                    subtypeToDelegate.put(destType, delegate);
                }

                @SuppressWarnings("unchecked")
                TypeAdapter<T> delegate = (TypeAdapter<T>) subtypeToDelegate.get(destType);


                if (delegate == null) {
                    throw new JsonParseException("cannot deserialize AbstractTimeseriesPoint subtype " +
                                                 destType + ";");
                }
                return delegate.fromJsonTree(jsonElement);
            }
        };
    }
}
