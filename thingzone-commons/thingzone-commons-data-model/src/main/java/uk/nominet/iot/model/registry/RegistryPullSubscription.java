/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.registry;

import java.io.Serializable;
import java.util.Map;

/**
 * Pull subscriptions can be added to a datastream, and mean that data is pulled from the URI at the specified interval
 * to populate the datastream.
 */
public class RegistryPullSubscription implements Serializable {
    private static final long serialVersionUID = -9140465671286253753L;

    private String uri;
    private Map<String, Object> uriParams;

    int interval;
    int aligned;

    public RegistryPullSubscription() {
        // default constructor
    }

    public RegistryPullSubscription(String uri, Map<String, Object> uriParams, int interval, int aligned) {
        this.uri = uri;
        this.uriParams = uriParams;
        this.interval = interval;
        this.aligned = aligned;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Map<String, Object> getUriParams() {
        return uriParams;
    }

    public void setUriParams(Map<String, Object> uriParams) {
        this.uriParams = uriParams;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public int getAligned() {
        return aligned;
    }

    public void setAligned(int aligned) {
        this.aligned = aligned;
    }

    @Override
    public String toString() {
        return "RegistryPullSubscription{" +
               "uri='" + uri + '\'' +
               ", uriParams=" + uriParams +
               ", interval=" + interval +
               ", aligned=" + aligned +
               '}';
    }
}
