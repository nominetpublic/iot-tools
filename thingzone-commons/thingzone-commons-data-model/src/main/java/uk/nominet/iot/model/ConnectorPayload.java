/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model;

import com.datastax.driver.extras.codecs.jdk8.InstantCodec;
import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import com.google.gson.annotations.SerializedName;
import org.openprovenance.prov.model.Attribute;
import uk.nominet.iot.annotations.ProvAttribute;
import uk.nominet.iot.annotations.ProvEntity;
import uk.nominet.iot.annotations.ProvId;
import uk.nominet.iot.codec.BytesCodec;
import uk.nominet.iot.codec.MapToJsonCodec;
import uk.nominet.iot.common.ThingzoneType;

import java.time.Instant;
import java.util.Arrays;
import java.util.Map;
import java.util.UUID;

@Table( keyspace = "thingzone",
        name = "connectorPayloads")
@ProvEntity(type = ThingzoneType.connectorPayload)
public class ConnectorPayload {
    @PartitionKey
    @ProvId
    private UUID id;

    @Column(codec = InstantCodec.class)
    private Instant receivedAt;

    private Source source;
    private ThingzoneConnectorContext connectorContext;

    private String registryUser;
    private String contentType;

    @ProvAttribute(attribute = Attribute.AttributeKind.PROV_LABEL)
    private String key;

    @Column(codec = MapToJsonCodec.class)
    private Map<String, Object> metadata;

    @Column(codec = BytesCodec.class)
    @SerializedName("base64Data")
    private byte[] data;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Instant getReceivedAt() {
        return receivedAt;
    }

    public void setReceivedAt(Instant receivedAt) {
        this.receivedAt = receivedAt;
    }

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    public ThingzoneConnectorContext getConnectorContext() {
        return connectorContext;
    }

    public void setConnectorContext(ThingzoneConnectorContext connectorContext) {
        this.connectorContext = connectorContext;
    }

    public String getRegistryUser() {
        return registryUser;
    }

    public void setRegistryUser(String registryUser) {
        this.registryUser = registryUser;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Map<String, Object> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, Object> metadata) {
        this.metadata = metadata;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ConnectorPayload{" +
                "id=" + id +
                ", receivedAt=" + receivedAt +
                ", source=" + source +
                ", connectorContext=" + connectorContext +
                ", registryUser='" + registryUser + '\'' +
                ", contentType='" + contentType + '\'' +
                ", key='" + key + '\'' +
                ", metadata=" + metadata +
                ", data=" + Arrays.toString(data) +
                '}';
    }
}
