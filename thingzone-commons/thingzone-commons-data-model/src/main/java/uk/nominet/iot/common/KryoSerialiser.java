/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.common;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import uk.nominet.iot.model.ConnectorPayload;
import uk.nominet.iot.model.Source;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Base64;
import java.util.Map;

public class KryoSerialiser {
    public static Kryo kryo = new Kryo();

    static {
        kryo.register(ConnectorPayload.class, 10);
        kryo.register(Source.class, 11);
        kryo.register(Map.class, 12);
    }

    public static byte[] serialise(Object obj) {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        Output output = new Output(buffer);
        kryo.writeObject(output, obj);
        output.flush();
        return buffer.toByteArray();
    }

    public static <T> Object deserialise(byte[] data, Class<T> obClass) {
        ByteArrayInputStream inputBuffer = new ByteArrayInputStream(data);
        Input input = new Input(inputBuffer);
        return kryo.readObject(input, obClass);
    }

    public static byte[] serialiseBase64(Object obj) {
        return Base64.getEncoder().encode(serialise(obj));
    }

    public static <T> Object deserialiseBase64(byte[] data, Class<T> obClass) {
        return deserialise(Base64.getDecoder().decode(data), obClass);
    }

}
