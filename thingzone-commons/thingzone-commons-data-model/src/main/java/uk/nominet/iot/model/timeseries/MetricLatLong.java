/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.timeseries;

import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.Table;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import uk.nominet.iot.annotations.TzTimeseries;
import uk.nominet.iot.model.LatLong;

@Table( keyspace = "thingzone",
        name = "metrics")
@TzTimeseries(indexType = "metric")
public class MetricLatLong extends Metric {
    private static final long serialVersionUID = 6012417959383864464L;

    @Column(name = "t5_value_lat_long")
    @SerializedName("value_lat_long")
    @JsonProperty("value_lat_long")
    private LatLong value;

    public LatLong getValue() {
        return value;
    }

    public void setValue(LatLong value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "MetricLatLong{" +
                "value=" + value +
                ", streamKey='" + streamKey + '\'' +
                ", timestamp=" + timestamp +
                ", id=" + id +
                ", metadata=" + metadata +
                '}';
    }
}
