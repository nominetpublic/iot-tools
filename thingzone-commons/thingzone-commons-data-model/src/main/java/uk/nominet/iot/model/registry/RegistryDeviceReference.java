/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.registry;

/**
 * A reference to a linked device from a datastream
 */
public class RegistryDeviceReference {
    private String key;
    private String linkName;

    public RegistryDeviceReference() {
    }

    /**
     * Create a reference between the datastream and a device
     * @param key a device key
     * @param linkName the name of the reference
     */
    public RegistryDeviceReference(String key, String linkName) {
        this.linkName = linkName;
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLinkName() {
        return linkName;
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }

    @Override
    public String toString() {
        return "RegistryDeviceReference{" +
                "linkName='" + linkName + '\'' +
                ", key='" + key + '\'' +
                '}';
    }
}
