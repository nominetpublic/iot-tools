/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model;

import com.datastax.driver.mapping.annotations.UDT;

import java.io.Serializable;

/**
 * Created by edoardo on 01/06/2017.
 */
@UDT(keyspace = "thingzone", name = "relatedRelation")
public class RelatedRelation implements Serializable {
    private static final long serialVersionUID = -7392745548065048179L;

    public String type;
    public String description;
    public String klass;
    public String key;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKlass() {
        return klass;
    }

    public void setKlass(String klass) {
        this.klass = klass;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "RelatedRelation{" +
               "type='" + type + '\'' +
               ", description='" + description + '\'' +
               ", klass='" + klass + '\'' +
               ", key='" + key + '\'' +
               '}';
    }
}
