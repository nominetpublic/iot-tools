/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Response;



public class ExceptionHandler {
    private static final Logger LOG = LoggerFactory.getLogger(ExceptionHandler.class);

    public static String handle(Response res, Exception e) {
        return handle(res, new APIException(400, e), e.getMessage());
    }

    public static String handle(Response res, Exception e, String description) {
        return handle(res, new APIException(400, e), description);
    }

    public static String handle(Response res, APIException e) {
        return handle(res, e, e.getMessage());
    }

    public static String handle(Response res, APIException e, String description) {
        res.status(e.getStatus());
        LOG.error(description);
        return description;
    }
}
