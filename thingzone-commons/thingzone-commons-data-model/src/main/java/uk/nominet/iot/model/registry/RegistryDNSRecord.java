/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.registry;

import java.io.Serializable;

/**
 * A DNS record for a registry entity
 */
public class RegistryDNSRecord implements Serializable {
    private static final long serialVersionUID = -4074448566814143757L;

    private String description;
    private String subdomain;
    private int ttl;
    private String rrtype;
    private String rdata;

    public RegistryDNSRecord(String description, String subdomain, int ttl, String rrtype, String rdata) {
        this.description = description;
        this.subdomain = subdomain;
        this.ttl = ttl;
        this.rrtype = rrtype;
        this.rdata = rdata;
    }

    public RegistryDNSRecord() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubdomain() {
        return subdomain;
    }

    public void setSubdomain(String subdomain) {
        this.subdomain = subdomain;
    }

    public int getTtl() {
        return ttl;
    }

    public void setTtl(int ttl) {
        this.ttl = ttl;
    }

    public String getRrtype() {
        return rrtype;
    }

    public void setRrtype(String rrtype) {
        this.rrtype = rrtype;
    }

    public String getRdata() {
        return rdata;
    }

    public void setRdata(String rdata) {
        this.rdata = rdata;
    }

    @Override
    public String toString() {
        return "RegistryDNSRecord{" +
                "description='" + description + '\'' +
                ", subdomain='" + subdomain + '\'' +
                ", ttl=" + ttl +
                ", rrtype='" + rrtype + '\'' +
                ", rdata='" + rdata + '\'' +
                '}';
    }
}
