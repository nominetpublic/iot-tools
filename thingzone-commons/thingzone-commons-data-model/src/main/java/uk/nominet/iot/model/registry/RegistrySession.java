/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.registry;

public class RegistrySession {
    private String sessionKey;
    private int ttl;
    private RegistryUser user;

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public int getTtl() {
        return ttl;
    }

    public void setTtl(int ttl) {
        this.ttl = ttl;
    }

    public RegistryUser getUser() {
        return user;
    }

    public void setUser(RegistryUser user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "RegistrySession{" +
                "sessionKey='" + sessionKey + '\'' +
                ", ttl=" + ttl +
                ", user=" + user +
                '}';
    }
}
