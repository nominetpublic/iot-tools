/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.codec;

import com.datastax.driver.core.DataType;
import com.datastax.driver.core.ProtocolVersion;
import com.datastax.driver.core.TypeCodec;
import com.datastax.driver.core.exceptions.InvalidTypeException;
import com.github.filosganga.geogson.model.Geometry;
import com.google.common.reflect.TypeToken;
import uk.nominet.iot.json.JsonStringSerialiser;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class GeometryToJsonCodec extends TypeCodec<Geometry<?>> {

    private final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    protected GeometryToJsonCodec() {
        super(DataType.varchar(), new TypeToken<Geometry<?>>() {});
    }

    protected GeometryToJsonCodec(DataType cqlType, TypeToken<Geometry<?>> javaType) {
        super(cqlType, javaType);
    }

    @Override
    public ByteBuffer serialize(Geometry value, ProtocolVersion protocolVersion) throws InvalidTypeException {
        if (value == null)
            return null;
        try {
            return ByteBuffer.wrap(jsonStringSerialiser.writeObject(value).getBytes());
        } catch (Exception e) {
            throw new InvalidTypeException(e.getMessage(), e);
        }
    }

    @Override
    public Geometry<?> deserialize(ByteBuffer bytes, ProtocolVersion protocolVersion) throws InvalidTypeException {
        if (bytes == null)
            return null;
        try {
            byte[] bytesCopy = new byte[bytes.remaining()];
            // always duplicate the ByteBuffer instance before consuming it!
            bytes.duplicate().get(bytesCopy);
            return jsonStringSerialiser.readObject(new String(bytesCopy, StandardCharsets.UTF_8), new TypeToken<Geometry<?>>() {}.getType());
        } catch (Exception e) {
            throw new InvalidTypeException(e.getMessage(), e);
        }
    }

    @Override
    public Geometry<?>  parse(String value) throws InvalidTypeException {
        if (value == null || value.isEmpty() || value.equalsIgnoreCase("NULL"))
            return null;

        try {
            return jsonStringSerialiser.readObject(value, new TypeToken<Map<String, Object>>() {}.getType());
        } catch (Exception e) {
            throw new InvalidTypeException(e.getMessage(), e);
        }
    }

    @Override
    public String format(Geometry<?>  value) throws InvalidTypeException {
        if (value == null)
            return "NULL";
        String json;

        try {
            json = jsonStringSerialiser.writeObject(value);
        } catch (Exception e) {
            throw new InvalidTypeException(e.getMessage(), e);
        }

        return json;
    }
}
