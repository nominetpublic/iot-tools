/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.registry;

import java.io.Serializable;

/**
 * A transform function
 */
public class RegistryTransformFunction  implements Serializable {
    private static final long serialVersionUID = -9198313018900864416L;

    private String name;
    private boolean isPublic;
    private String functionType;
    private String functionContent;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public String getFunctionType() {
        return functionType;
    }

    public void setFunctionType(String functionType) {
        this.functionType = functionType;
    }

    public String getFunctionContent() {
        return functionContent;
    }

    public void setFunctionContent(String functionContent) {
        this.functionContent = functionContent;
    }

    @Override
    public String toString() {
        return "RegistryTransformFunction{" +
                "name='" + name + '\'' +
                ", isPublic=" + isPublic +
                ", functionType='" + functionType + '\'' +
                ", functionContent='" + functionContent + '\'' +
                '}';
    }
}
