/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.geo;

import com.google.gson.JsonElement;
import uk.nominet.iot.model.registry.cache.DeviceDocument;
import uk.nominet.iot.model.registry.cache.RegistryDataStreamWithLatestPayload;

import java.util.List;
import java.util.Map;

public class DeviceGeoFeature {
    private Map<String, JsonElement> additionalProperties;
    private DeviceDocument deviceDocument;
    private List<RegistryDataStreamWithLatestPayload> streamsWithLatest;

    public Map<String, JsonElement> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, JsonElement> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public DeviceDocument getDeviceDocument() {
        return deviceDocument;
    }

    public void setDeviceDocument(DeviceDocument deviceDocument) {
        this.deviceDocument = deviceDocument;
    }

    public List<RegistryDataStreamWithLatestPayload> getStreamsWithLatest() {
        return streamsWithLatest;
    }

    public void setStreamsWithLatest(List<RegistryDataStreamWithLatestPayload> streamsWithLatest) {
        this.streamsWithLatest = streamsWithLatest;
    }
}
