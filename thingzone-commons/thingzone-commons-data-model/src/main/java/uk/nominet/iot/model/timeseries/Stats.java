/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.timeseries;

import uk.nominet.iot.model.core.AbstractTimeseriesPoint;

public class Stats extends AbstractTimeseriesPoint {
    private static final long serialVersionUID = -1982884345066759045L;

    private Long count;
    private Double min;
    private Double max;
    private Double avg;
    private Double sum;
    private Double sum_of_squares;
    private Double variance;
    private Double std_deviation;

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Double getMin() {
        return min;
    }

    public void setMin(Double min) {
        this.min = min;
    }

    public Double getMax() {
        return max;
    }

    public void setMax(Double max) {
        this.max = max;
    }

    public Double getAvg() {
        return avg;
    }

    public void setAvg(Double avg) {
        this.avg = avg;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }

    public Double getSum_of_squares() {
        return sum_of_squares;
    }

    public void setSum_of_squares(Double sum_of_squares) {
        this.sum_of_squares = sum_of_squares;
    }

    public Double getVariance() {
        return variance;
    }

    public void setVariance(Double variance) {
        this.variance = variance;
    }

    public Double getStd_deviation() {
        return std_deviation;
    }

    public void setStd_deviation(Double std_deviation) {
        this.std_deviation = std_deviation;
    }
}
