/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.api;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ResponseBuilder {
    private JsonObject response;
    private JsonElement responseBody;
    private boolean isOk;

    public ResponseBuilder(boolean isOk) {
        this.isOk = isOk;
        response = new JsonObject();
        response.addProperty("result", (isOk) ? "ok" : "fail");
        responseBody = new JsonObject();
    }

    public ResponseBuilder setReason(String reason) {
        response.addProperty("reason", reason);
        return this;
    }

    public ResponseBuilder setResponse(JsonElement response) {
        this.responseBody = response;
        return this;
    }

    public ResponseBuilder addElement(String property, JsonElement value) {
        responseBody.getAsJsonObject().add(property, value);
        return this;
    }

    public ResponseBuilder addProperty(String property, String value) {
        responseBody.getAsJsonObject().addProperty(property, value);
        return this;
    }

    public ResponseBuilder addProperty(String property, Number value) {
        responseBody.getAsJsonObject().addProperty(property, value);
        return this;
    }

    public ResponseBuilder addProperty(String property, Boolean value) {
        responseBody.getAsJsonObject().addProperty(property, value);
        return this;
    }

    public ResponseBuilder addProperty(String property, Character value) {
        responseBody.getAsJsonObject().addProperty(property, value);
        return this;
    }

    public JsonObject build() {
        if (isOk) {
            response.add("response", responseBody);
        }

        return response;
    }

    @Override
    public String toString() {
        return response.toString();
    }
}
