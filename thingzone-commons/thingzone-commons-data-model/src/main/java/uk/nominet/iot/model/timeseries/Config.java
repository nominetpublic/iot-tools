/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.timeseries;

import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.Table;
import uk.nominet.iot.annotations.TzTimeseries;
import uk.nominet.iot.codec.MapToJsonCodec;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;

import java.util.Map;
import java.util.UUID;


@Table(keyspace = "thingzone",
       name = "config")
@TzTimeseries(indexType = "config")
public class Config extends AbstractTimeseriesPoint {


    @Column(codec = MapToJsonCodec.class)
    protected Map<String, Object> config;
    private ThingzoneConfigOrigin origin;
    private String revision;
    private UUID derivedFromConfig;


    public Map<String, Object> getConfig() {
        return config;
    }

    public void setConfig(Map<String, Object> config) {
        this.config = config;
    }

    public ThingzoneConfigOrigin getOrigin() {
        return origin;
    }

    public void setOrigin(ThingzoneConfigOrigin origin) {
        this.origin = origin;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    public UUID getDerivedFromConfig() {
        return derivedFromConfig;
    }

    public void setDerivedFromConfig(UUID derivedFromConfig) {
        this.derivedFromConfig = derivedFromConfig;
    }

    @Override
    public String toString() {
        return "Config{" +
               "config=" + config +
               ", origin=" + origin +
               ", revision='" + revision + '\'' +
               ", derivedFromConfig=" + derivedFromConfig +
               ", streamKey='" + streamKey + '\'' +
               ", month=" + month +
               ", klass='" + klass + '\'' +
               ", timestamp=" + timestamp +
               ", id=" + id +
               ", metadata=" + metadata +
               ", quality=" + quality +
               ", tags=" + tags +
               ", combinedPoints=" + combinedPoints +
               '}';
    }
}
