/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.timeseries;

import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.Table;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import uk.nominet.iot.annotations.TzTimeseries;

import java.util.List;

@Table( keyspace = "thingzone",
        name = "metrics")
@TzTimeseries(indexType = "metric")
public class MetricStrings extends Metric {
    private static final long serialVersionUID = 8785829240740289432L;

    @Column(name = "t3_value_list_text")
    @SerializedName("value_list_text")
    @JsonProperty("value_list_text")
    private List<String> value;

    public List<String> getValue() {
        return value;
    }

    public void setValue(List<String> value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "MetricStrings{" +
                "value=" + value +
                ", streamKey='" + streamKey + '\'' +
                ", timestamp=" + timestamp +
                ", id=" + id +
                ", metadata=" + metadata +
                '}';
    }
}
