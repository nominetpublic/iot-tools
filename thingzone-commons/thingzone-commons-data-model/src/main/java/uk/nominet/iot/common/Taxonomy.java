/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.common;

public final class Taxonomy {
    public static final String prefix = "thingzone:";

    // Entity Types
    public static final String ENTITY_PAYLOAD = prefix + "payload";
    public static final String ENTITY_METRIC = prefix + "metric";
    public static final String ENTITY_EVENT = prefix + "event";
    public static final String ENTITY_IMAGE = prefix + "image";
    public static final String ENTITY_DEVICE = prefix + "device";
    public static final String ENTITY_DATASTREAM = prefix + "dataStream";



    // Agent Types
    public static final String AGENT_THINGZONE_USER = prefix + "thingzoneUser";
    public static final String AGENT_PERSON = prefix + "person";
    public static final String AGENT_INTERNAL_SERVICE = prefix + "internalService";
    public static final String AGENT_EXTERNAL_SERVICE = prefix + "externalService";
    public static final String AGENT_MQTT_CLIENT = prefix + "mqttClient";


    // Permissions
    public static final String PERMISSION_ADMIN = "ADMIN";
    public static final String PERMISSION_UPLOAD = "UPLOAD";
    public static final String PERMISSION_CONSUME = "CONSUME";
    public static final String PERMISSION_MODIFY = "MODIFY";
    public static final String PERMISSION_ANNOTATE = "ANNOTATE";
    public static final String PERMISSION_DISCOVER = "DISCOVER";
    public static final String PERMISSION_UPLOAD_EVENTS = "UPLOAD_EVENTS";
    public static final String PERMISSION_UPLOAD_ALERTS = "UPLOAD_ALERTS";
    public static final String PERMISSION_CONSUME_EVENTS = "CONSUME_EVENTS";
    public static final String PERMISSION_CONSUME_ALERTS = "CONSUME_ALERTS";
    public static final String PERMISSION_VIEW_LOCATION = "VIEW_LOCATION";
    public static final String PERMISSION_VIEW_METADATA = "VIEW_METADATA";

    // Privileges
    public static final String PRIVILEGE_CREATE_USER  = "CREATE_USER";
    public static final String PRIVILEGE_CREATE_DEVICE  = "CREATE_DEVICE";
    public static final String PRIVILEGE_CREATE_DATASTREAM  = "CREATE_DATASTREAM";
    public static final String PRIVILEGE_CREATE_TRANSFORM  = "CREATE_TRANSFORM";
    public static final String PRIVILEGE_CREATE_TRANSFORM_FUNCTION  = "CREATE_TRANSFORM_FUNCTION";
    public static final String PRIVILEGE_GRANT_CREATE_USER = "GRANT_CREATE_USER";
    public static final String PRIVILEGE_GRANT_CREATE_DEVICE  = "GRANT_CREATE_DEVICE";
    public static final String PRIVILEGE_GRANT_CREATE_DATASTREAM  = "GRANT_CREATE_DATASTREAM";
    public static final String PRIVILEGE_GRANT_CREATE_TRANSFORM = "GRANT_CREATE_TRANSFORM";
    public static final String PRIVILEGE_GRANT_CREATE_TRANSFORM_FUNCTION = "GRANT_CREATE_TRANSFORM_FUNCTION";
    public static final String PRIVILEGE_GRANT_GRANTS = "GRANT_GRANTS";
    public static final String PRIVILEGE_CREATE_DETECTOR  = "CREATE_DETECTOR ";
    public static final String PRIVILEGE_RUN_DETECTOR  = "RUN_DETECTOR";
    public static final String PRIVILEGE_GRANT_CREATE_DETECTOR = "GRANT_CREATE_DETECTOR";
    public static final String PRIVILEGE_GRANT_RUN_DETECTOR = "GRANT_RUN_DETECTOR";
    public static final String PRIVILEGE_DUMP_ALL = "DUMP_ALL";
    public static final String PRIVILEGE_USE_SESSION = "USE_SESSION";
    public static final String PRIVILEGE_USER_ADMIN = "USER_ADMIN";




}
