/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.json;

import com.github.filosganga.geogson.model.Feature;
import com.github.filosganga.geogson.model.FeatureCollection;
import com.google.gson.*;
import uk.nominet.iot.model.geo.TimeseriesFeatureCollection;
import uk.nominet.iot.model.timeseries.TimeseriesGeoFeature;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TimeseriesFeatureCollectionTypeAdapter implements JsonSerializer<TimeseriesFeatureCollection>, JsonDeserializer<TimeseriesFeatureCollection> {
    @Override
    public JsonElement serialize(TimeseriesFeatureCollection timeseriesFeatureCollection, Type type, JsonSerializationContext jsonSerializationContext) {

        List<Feature> features = new ArrayList<>();

        for (Object timeseriesGeoFeatureOb : timeseriesFeatureCollection) {
            TimeseriesGeoFeature timeseriesGeoFeature = (TimeseriesGeoFeature)timeseriesGeoFeatureOb;
            Feature featureObj = timeseriesGeoFeature.getFeature();


            Map<String,JsonElement> props = new HashMap<>();
            props.put("point", jsonSerializationContext.serialize(timeseriesGeoFeature.getPoint()));
            props.put("combinedPoints", jsonSerializationContext.serialize(timeseriesGeoFeature.getAssociatedPoints()));

            if (timeseriesGeoFeature.getAdditionalProperties() != null) {
                props.putAll(timeseriesGeoFeature.getAdditionalProperties());
            }

            Feature feature = Feature.builder().withGeometry(featureObj.geometry())
                    .withProperties(props)
                    .withId(featureObj.id())
                    .build();

            features.add(feature);
        }

        FeatureCollection featureCollection = new FeatureCollection(features);

        return jsonSerializationContext.serialize(featureCollection);
    }

    @Override
    public TimeseriesFeatureCollection deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        return null;
    }
}
