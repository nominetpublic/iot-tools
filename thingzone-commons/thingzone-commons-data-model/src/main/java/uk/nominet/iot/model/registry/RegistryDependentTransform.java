/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.registry;

/**
 * Output from the internal/transforms_dependent_on_stream registry api method.
 */
public class RegistryDependentTransform {
    private String alias;
    private boolean trigger;
    private RegistryTransform transform;

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public boolean isTrigger() {
        return trigger;
    }

    public void setTrigger(boolean trigger) {
        this.trigger = trigger;
    }

    public RegistryTransform getTransform() {
        return transform;
    }

    public void setTransform(RegistryTransform transform) {
        this.transform = transform;
    }

    @Override
    public String toString() {
        return "RegistryDependentTransform{" +
                "alias='" + alias + '\'' +
                ", trigger=" + trigger +
                ", transform=" + transform +
                '}';
    }
}
