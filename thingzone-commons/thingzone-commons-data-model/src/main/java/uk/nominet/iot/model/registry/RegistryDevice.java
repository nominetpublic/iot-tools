/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.registry;

import com.github.filosganga.geogson.model.Geometry;
import org.openprovenance.prov.model.Attribute;
import uk.nominet.iot.annotations.ProvAttribute;
import uk.nominet.iot.annotations.ProvEntity;
import uk.nominet.iot.annotations.TzCollection;
import uk.nominet.iot.common.ThingzoneType;

import java.util.List;
import java.util.Map;

@TzCollection(indexType = "device")
@ProvEntity(type = ThingzoneType.device)
public class RegistryDevice extends RegistryEntity {
    private static final long serialVersionUID = 1783821996283780506L;

    protected List<RegistryDataStream> dataStreams;
    protected Map<String, Object> metadata;

    @ProvAttribute(attribute = Attribute.AttributeKind.PROV_LOCATION, prefix="thingzone", n = "geojson")
    private Geometry<?> location;

    public List<RegistryDataStream> getDataStreams() {
        return dataStreams;
    }

    public void setDataStreams(List<RegistryDataStream> dataStreams) {
        this.dataStreams = dataStreams;
    }

    public Map<String, Object> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, Object> metadata) {
        this.metadata = metadata;
    }

    public Geometry<?> getLocation() {
        return location;
    }

    public void setLocation(Geometry<?> location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "RegistryDevice{" +
                "dataStreams=" + dataStreams +
                ", metadata=" + metadata +
                ", location=" + location +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", dnsRecords=" + dnsRecords +
                ", key='" + key + '\'' +
                ", userPermissions=" + userPermissions +
                ", groupPermissions=" + groupPermissions +
                '}';
    }
}
