/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.json;

import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

public class JsonValidator {

    protected final Map<Class<?>, Schema> schemas = new HashMap<>();

    public void register(Class<?> type) throws Exception {
        schemas.put(type, loadSchemaFromResource(type.getSimpleName()));
    }

    public void register(Class<?> type, Schema schema) {
        schemas.put(type, schema);
    }

    public void validate(String input, Class<?> type) throws Exception {
        if (input == null)
            throw new Exception("Input can not be null");
        JSONObject doc = new JSONObject(new JSONTokener(input));

        Schema schema;
        if (schemas.containsKey(type)) {
            schema = schemas.get(type);
        } else {
            schema = loadSchemaFromResource(type.getSimpleName());
        }

        try {
            schema.validate(doc);
        } catch (ValidationException e) {
            throw new Exception("Schema validation exception: " + e.toJSON());
        }
    }

    public Schema loadSchemaFromResource(String resourceName) throws Exception {
        if (!resourceName.endsWith(".json")) {
            resourceName = resourceName + ".json";
        }

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream("schemas/" + resourceName);
        if (is != null) {
            Reader reader = new InputStreamReader(is);
            JSONObject schemaJSONObject = new JSONObject(new JSONTokener(reader));
            return SchemaLoader.load(schemaJSONObject);
        }
        throw new Exception("Schema resource " + resourceName + " not found");
    }

}
