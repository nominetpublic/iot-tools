/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.script;

import uk.nominet.iot.model.core.AbstractTimeseriesPoint;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Used by transfrom sandbox
 */
public class ExecutionTaskOutput {
    private String transformKey;
    private List<UUID> inputs;
    private AbstractTimeseriesPoint transformOutput;
    private Map<String, AbstractTimeseriesPoint> streamOutputs;
    private List<LogEntry> logs;


    public AbstractTimeseriesPoint getTransformOutput() {
        return transformOutput;
    }

    public void setTransformOutput(AbstractTimeseriesPoint transformOutput) {
        this.transformOutput = transformOutput;
    }

    public Map<String, AbstractTimeseriesPoint> getStreamOutputs() {
        return streamOutputs;
    }

    public void setStreamOutputs(Map<String, AbstractTimeseriesPoint> streamOutputs) {
        this.streamOutputs = streamOutputs;
    }

    public List<LogEntry> getLogs() {
        return logs;
    }

    public void setLogs(List<LogEntry> logs) {
        this.logs = logs;
    }

    public String getTransformKey() {
        return transformKey;
    }

    public void setTransformKey(String transformKey) {
        this.transformKey = transformKey;
    }


    public List<UUID> getInputs() {
        return inputs;
    }

    public void setInputs(List<UUID> inputs) {
        this.inputs = inputs;
    }

    @Override
    public String toString() {
        return "ExecutionTaskOutput{" +
               "transformKey='" + transformKey + '\'' +
               ", inputs=" + inputs +
               ", transformOutput=" + transformOutput +
               ", streamOutputs=" + streamOutputs +
               ", logs=" + logs +
               '}';
    }
}
