/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.script;

import java.util.List;
import java.util.Map;

/**
 * Used by transfrom sandbox
 */
public class ExecutionTask {
    private String transformKey;
    private Map<String, Object> parameters;
    private Map<String, Object> sources;
    private List<String> outputAliases;
    private Map<String, Object> metadata;
    private ExecutableScript script;

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }

    public Map<String, Object> getSources() {
        return sources;
    }

    public void setSources(Map<String, Object> sources) {
        this.sources = sources;
    }

    public List<String> getOutputAliases() {
        return outputAliases;
    }

    public void setOutputAliases(List<String> outputAliases) {
        this.outputAliases = outputAliases;
    }

    public Map<String, Object> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, Object> metadata) {
        this.metadata = metadata;
    }

    public ExecutableScript getScript() {
        return script;
    }

    public void setScript(ExecutableScript script) {
        this.script = script;
    }

    public String getTransformKey() {
        return transformKey;
    }

    public void setTransformKey(String transformKey) {
        this.transformKey = transformKey;
    }

    @Override
    public String toString() {
        return "ExecutionTask{" +
               "transformKey='" + transformKey + '\'' +
               ", parameters=" + parameters +
               ", sources=" + sources +
               ", outputAliases=" + outputAliases +
               ", metadata=" + metadata +
               ", script=" + script +
               '}';
    }
}
