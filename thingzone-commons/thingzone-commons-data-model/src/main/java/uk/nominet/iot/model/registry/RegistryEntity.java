/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.registry;
import org.openprovenance.prov.model.Attribute;
import uk.nominet.iot.annotations.ProvAttribute;

import java.io.Serializable;
import java.util.List;

/**
 * The entity record contains the things common to all registry records with a key
 */
public class RegistryEntity extends RegistryKey implements Serializable {
    private static final long serialVersionUID = -4790977423635291766L;

    @ProvAttribute(attribute = Attribute.AttributeKind.PROV_LABEL)
    protected String name;

    protected String type;
    protected List<RegistryDNSRecord> dnsRecords;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<RegistryDNSRecord> getDnsRecords() {
        return dnsRecords;
    }

    public void setDnsRecords(List<RegistryDNSRecord> dnsRecords) {
        this.dnsRecords = dnsRecords;
    }

    @Override
    public String toString() {
        return "RegistryEntity{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", dnsRecords=" + dnsRecords +
                ", key='" + key + '\'' +
                ", userPermissions=" + userPermissions +
                ", groupPermissions=" + groupPermissions +
                '}';
    }
}
