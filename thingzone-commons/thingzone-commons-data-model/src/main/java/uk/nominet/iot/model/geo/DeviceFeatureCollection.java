/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.geo;

import uk.nominet.iot.model.registry.cache.DeviceDocument;
import uk.nominet.iot.model.registry.cache.RegistryDataStreamWithLatestPayload;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DeviceFeatureCollection extends ArrayList<DeviceGeoFeature> {
    private static final long serialVersionUID = 7771891280155925480L;
    private Long total;


    public static DeviceFeatureCollection build(List<DeviceDocument> devices, List<RegistryDataStreamWithLatestPayload> streamsWithPayload, Long total) {
        DeviceFeatureCollection deviceFeatureCollection = new DeviceFeatureCollection();

        deviceFeatureCollection.total = total;
    for (DeviceDocument device : devices) {
            DeviceGeoFeature deviceGeoFeature = new DeviceGeoFeature();
            deviceGeoFeature.setDeviceDocument(device);

            if (streamsWithPayload != null) {
                List<RegistryDataStreamWithLatestPayload> streamWithLatestPayloads =
                        streamsWithPayload
                                .stream()
                                .filter(stream -> stream.containsDeviceData(device.getKey()))
                                .collect(Collectors.toList());

                deviceGeoFeature.setStreamsWithLatest(streamWithLatestPayloads);
            }

            deviceFeatureCollection.add(deviceGeoFeature);
        }

        return deviceFeatureCollection;
    }

    public List<DeviceDocument> toDeviceList() {
        return this.stream().map(feature -> feature.getDeviceDocument()).collect(Collectors.toList());
    }

    public Long getTotal() {
        return total;
    }
}
