/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.timeseries;

import com.datastax.driver.mapping.annotations.Table;
import uk.nominet.iot.annotations.TzTimeseries;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;

@Table( keyspace = "thingzone",
        name = "metrics")
@TzTimeseries(indexType = "metric")
public abstract class Metric extends AbstractTimeseriesPoint {
    private static final long serialVersionUID = 5133130874535238630L;
}
