/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.timeseries;

import java.time.Instant;
import java.util.List;
import java.util.Map;

import com.datastax.driver.extras.codecs.jdk8.InstantCodec;
import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.Table;
import com.github.filosganga.geogson.model.Geometry;

import org.openprovenance.prov.model.Attribute;
import uk.nominet.iot.annotations.ProvAttribute;
import uk.nominet.iot.annotations.ProvEntity;
import uk.nominet.iot.annotations.TzTimeseries;
import uk.nominet.iot.codec.GeometryToJsonCodec;
import uk.nominet.iot.codec.MapToJsonCodec;
import uk.nominet.iot.common.ThingzoneType;
import uk.nominet.iot.model.DerivedRelation;
import uk.nominet.iot.model.Feedback;
import uk.nominet.iot.model.RelatedRelation;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;

/**
 Created by edoardo on 18/05/2017.
 */
@Table( keyspace = "thingzone",
        name = "events")
@TzTimeseries(indexType = "event")
@ProvEntity(type = ThingzoneType.event)

public class Event extends AbstractTimeseriesPoint {
    private static final long serialVersionUID = 8295798159835139013L;

    @ProvAttribute(attribute = Attribute.AttributeKind.PROV_LABEL)
    protected String name;

    protected String description;
    protected String type;
    protected ThingzoneEventType eventType;
    protected String category;

    @Column(codec = InstantCodec.class) protected Instant startTimestamp;
    @Column(codec = InstantCodec.class) protected Instant endTimestamp;

    @Column(codec = GeometryToJsonCodec.class)
    @ProvAttribute(attribute = Attribute.AttributeKind.PROV_LOCATION, prefix="thingzone", n = "geojson")
    protected Geometry<?> location; //Represented as a geojson geometry

    protected List<String> receivers = null;
    @Column(codec = MapToJsonCodec.class)  protected Map<String, Object> configuration;

    protected List<Feedback> feedback;

    protected Double severity;

    protected String status;

    protected Double value;

    protected List<RelatedRelation> relatedTo;

    protected List<DerivedRelation> derivedFrom;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public ThingzoneEventType getEventType() {
        return eventType;
    }

    public void setEventType(ThingzoneEventType eventType) {
        this.eventType = eventType;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Instant getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(Instant startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public Instant getEndTimestamp() {
        return endTimestamp;
    }

    public void setEndTimestamp(Instant endTimestamp) {
        this.endTimestamp = endTimestamp;
    }

    public Geometry<?> getLocation() {
        return location;
    }

    public void setLocation(Geometry<?> location) {
        this.location = location;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }


    public List<String> getReceivers() {
        return receivers;
    }

    public void setReceivers(List<String> receivers) {
        this.receivers = receivers;
    }

    public Map<String, Object> getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Map<String, Object> configuration) {
        this.configuration = configuration;
    }

    public List<Feedback> getFeedback() {
        return feedback;
    }

    public void setFeedback(List<Feedback> feedback) {
        this.feedback = feedback;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<RelatedRelation> getRelatedTo() {
        return relatedTo;
    }

    public void setRelatedTo(List<RelatedRelation> relatedTo) {
        this.relatedTo = relatedTo;
    }

    public List<DerivedRelation> getDerivedFrom() {
        return derivedFrom;
    }

    public void setDerivedFrom(List<DerivedRelation> derivedFrom) {
        this.derivedFrom = derivedFrom;
    }

    public Double getSeverity() {
        return severity;
    }

    public void setSeverity(Double severity) {
        this.severity = severity;
    }


    @Override
    public String toString() {
        return "Event{" +
               "name='" + name + '\'' +
               ", description='" + description + '\'' +
               ", type='" + type + '\'' +
               ", eventType=" + eventType +
               ", category='" + category + '\'' +
               ", startTimestamp=" + startTimestamp +
               ", endTimestamp=" + endTimestamp +
               ", location=" + location +
               ", receivers=" + receivers +
               ", configuration=" + configuration +
               ", feedback=" + feedback +
               ", severity=" + severity +
               ", status='" + status + '\'' +
               ", value=" + value +
               ", relatedTo=" + relatedTo +
               ", derivedFrom=" + derivedFrom +
               ", streamKey='" + streamKey + '\'' +
               ", month=" + month +
               ", klass='" + klass + '\'' +
               ", timestamp=" + timestamp +
               ", id=" + id +
               ", metadata=" + metadata +
               ", quality=" + quality +
               ", tags=" + tags +
               ", combinedPoints=" + combinedPoints +
               '}';
    }
}
