/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model;

import com.datastax.driver.extras.codecs.jdk8.InstantCodec;
import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.Field;
import com.datastax.driver.mapping.annotations.UDT;

import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;

/**
 * Created by edoardo on 01/06/2017.
 */
@UDT(keyspace = "thingzone", name = "feedback")
public class Feedback implements Serializable {
    private static final long serialVersionUID = 5258082920394376578L;

    public UUID id;
    @Field(codec = InstantCodec.class) public Instant timestamp;
    public String user;
    public String type;
    public String category;
    public String description;

    public UUID getId() { return id; }

    public void setId(UUID id) { this.id = id; }

    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Feedback{" +
               "timestamp='" + timestamp + '\'' +
               ", user='" + user + '\'' +
               ", type='" + type + '\'' +
               ", category='" + category + '\'' +
               ", description='" + description + '\'' +
               '}';
    }
}
