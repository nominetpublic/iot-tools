/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.registry;

import uk.nominet.iot.annotations.TzCollection;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@TzCollection(indexType = "stream")
public class RegistryTransform extends RegistryDataStream implements Serializable {
    private static final long serialVersionUID = -1640512348664976209L;

    private Map<String, Object> parameterValues;
    private String runSchedule;
    private boolean multiplexer;
    private List<RegistryTransformSource> sources;
    private List<RegistryTransformOutputStream> outputStreams;
    private RegistryTransformFunction function;

    public Map<String, Object> getParameterValues() {
        return parameterValues;
    }

    public void setParameterValues(Map<String, Object> parameterValues) {
        this.parameterValues = parameterValues;
    }

    public String getRunSchedule() {
        return runSchedule;
    }

    public void setRunSchedule(String runSchedule) {
        this.runSchedule = runSchedule;
    }

    public List<RegistryTransformSource> getSources() {
        return sources;
    }

    public void setSources(List<RegistryTransformSource> sources) {
        this.sources = sources;
    }

    public List<RegistryTransformOutputStream> getOutputStreams() {
        return outputStreams;
    }

    public void setOutputStreams(List<RegistryTransformOutputStream> outputStreams) {
        this.outputStreams = outputStreams;
    }

    public RegistryTransformFunction getFunction() {
        return function;
    }

    public void setFunction(RegistryTransformFunction function) {
        this.function = function;
    }

    public boolean isMultiplexer() {
        return multiplexer;
    }

    public void setMultiplexer(boolean multiplexer) {
        this.multiplexer = multiplexer;
    }

    @Override
    public String toString() {
        return "RegistryTransform{" +
                "parameterValues=" + parameterValues +
                ", runSchedule='" + runSchedule + '\'' +
                ", multiplexer=" + multiplexer +
                ", sources=" + sources +
                ", outputStreams=" + outputStreams +
                ", function=" + function +
                ", devices=" + devices +
                ", metadata=" + metadata +
                ", isTransform=" + isTransform +
                ", pushSubscribers=" + pushSubscribers +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", dnsRecords=" + dnsRecords +
                ", key='" + key + '\'' +
                ", userPermissions=" + userPermissions +
                ", groupPermissions=" + groupPermissions +
                '}';
    }
}
