/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.registry;

import java.io.Serializable;

/**
 * A source for a transform
 */
public class RegistryTransformSource  implements Serializable {
    private static final long serialVersionUID = 7380151558285600196L;

    private String source;
    private String alias;
    private boolean trigger;
    private String aggregationType;
    private String aggregationSpec;

    public RegistryTransformSource() {}

    public RegistryTransformSource(String source,
                                   String alias,
                                   boolean trigger,
                                   String aggregationType,
                                   String aggregationSpec) {
        this.source = source;
        this.alias = alias;
        this.trigger = trigger;
        this.aggregationType = aggregationType;
        this.aggregationSpec = aggregationSpec;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public boolean isTrigger() {
        return trigger;
    }

    public void setTrigger(boolean trigger) {
        this.trigger = trigger;
    }

    public String getAggregationType() {
        return aggregationType;
    }

    public void setAggregationType(String aggregationType) {
        this.aggregationType = aggregationType;
    }

    public String getAggregationSpec() {
        return aggregationSpec;
    }

    public void setAggregationSpec(String aggregationSpec) {
        this.aggregationSpec = aggregationSpec;
    }

    @Override
    public String toString() {
        return "RegistryTransformSource{" +
                "source='" + source + '\'' +
                ", alias='" + alias + '\'' +
                ", trigger=" + trigger +
                ", aggregationType='" + aggregationType + '\'' +
                ", aggregationSpec='" + aggregationSpec + '\'' +
                '}';
    }
}
