/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.annotations;

import uk.nominet.iot.common.Taxonomy;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface TzTimeseries {
    boolean index() default true;
    boolean store() default true;
    String indexType() default "[unassigned]";
    String addPermission() default Taxonomy.PERMISSION_MODIFY;
    String removePermission() default Taxonomy.PERMISSION_MODIFY;
    String findPermission() default Taxonomy.PERMISSION_CONSUME;
}

