/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.registry;

import java.io.Serializable;
import java.util.Map;

/**
 * Push subscribers can be added to a datastream. When the datastream gets new data it is
 * pushed to the push subscribers
 */
public class RegistryPushSubscriber implements Serializable {
    private static final long serialVersionUID = 3756434897844996508L;

    private String uri;
    private Map<String, Object> uriParams;
    String method;
    Map<String,Object> headers;
    String retrySequence;

    public RegistryPushSubscriber() {
    }

    public RegistryPushSubscriber(String uri, Map<String, Object> uriParams, String method, Map<String, Object> headers, String retrySequence) {
        this.uri = uri;
        this.uriParams = uriParams;
        this.method = method;
        this.headers = headers;
        this.retrySequence = retrySequence;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Map<String, Object> getUriParams() {
        return uriParams;
    }

    public void setUriParams(Map<String, Object> uriParams) {
        this.uriParams = uriParams;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Map<String, Object> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, Object> headers) {
        this.headers = headers;
    }

    public String getRetrySequence() {
        return retrySequence;
    }

    public void setRetrySequence(String retrySequence) {
        this.retrySequence = retrySequence;
    }

    @Override
    public String toString() {
        return "RegistryPushSubscriber{" +
               "uri='" + uri + '\'' +
               ", uriParams=" + uriParams +
               ", method='" + method + '\'' +
               ", headers=" + headers +
               ", retrySequence='" + retrySequence + '\'' +
               '}';
    }
}
