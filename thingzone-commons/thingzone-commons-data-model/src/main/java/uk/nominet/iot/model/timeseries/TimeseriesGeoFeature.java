/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.timeseries;

import com.github.filosganga.geogson.model.Feature;
import com.google.gson.JsonElement;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;

import java.util.List;
import java.util.Map;

public class TimeseriesGeoFeature<T extends AbstractTimeseriesPoint> {
    private T point;
    private List<AbstractTimeseriesPoint> associatedPoints;
    private Feature feature;
    private Map<String, JsonElement> additionalProperties;


    public T getPoint() {
        return point;
    }

    public void setPoint(T point) {
        this.point = point;
    }

    public Feature getFeature() {
        return feature;
    }

    public void setFeature(Feature feature) {
        this.feature = feature;
    }

    public List<AbstractTimeseriesPoint> getAssociatedPoints() {
        return associatedPoints;
    }

    public Map<String, JsonElement> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, JsonElement> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public void setAssociatedPoints(List<AbstractTimeseriesPoint> associatedPoints) {
        this.associatedPoints = associatedPoints;
    }
}
