/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.json;

import com.github.filosganga.geogson.model.Feature;
import com.github.filosganga.geogson.model.FeatureCollection;
import com.google.gson.*;
import uk.nominet.iot.model.geo.DeviceFeatureCollection;
import uk.nominet.iot.model.geo.DeviceGeoFeature;
import uk.nominet.iot.model.geo.TimeseriesFeatureCollection;
import uk.nominet.iot.model.timeseries.TimeseriesGeoFeature;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeviceFeatureCollectionTypeAdapter implements JsonSerializer<DeviceFeatureCollection>, JsonDeserializer<DeviceFeatureCollection> {
    @Override
    public JsonElement serialize(DeviceFeatureCollection deviceFeatureCollection, Type type, JsonSerializationContext jsonSerializationContext) {

        List<Feature> features = new ArrayList<>();

        for (Object timeseriesGeoFeatureOb : deviceFeatureCollection) {
            DeviceGeoFeature deviceGeoFeature = (DeviceGeoFeature)timeseriesGeoFeatureOb;

            Map<String,JsonElement> props = new HashMap<>();
            props.put("device", jsonSerializationContext.serialize(deviceGeoFeature.getDeviceDocument()));
            props.put("latest", jsonSerializationContext.serialize(deviceGeoFeature.getStreamsWithLatest()));

            if (deviceGeoFeature.getAdditionalProperties() != null) {
                props.putAll(deviceGeoFeature.getAdditionalProperties());
            }

            Feature feature = Feature.builder().withGeometry(deviceGeoFeature.getDeviceDocument().getLocation())
                    .withProperties(props)
                    .withId(deviceGeoFeature.getDeviceDocument().getKey())
                    .build();

            features.add(feature);
        }

        FeatureCollection featureCollection = new FeatureCollection(features);

        return jsonSerializationContext.serialize(featureCollection);
    }

    @Override
    public DeviceFeatureCollection deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        return null;
    }
}
