/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.core;

import com.datastax.driver.extras.codecs.jdk8.InstantCodec;
import com.datastax.driver.mapping.annotations.ClusteringColumn;
import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Transient;
import uk.nominet.iot.annotations.ProvId;
import uk.nominet.iot.codec.MapToJsonCodec;
import uk.nominet.iot.model.QualityIndicator;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 Created by edoardo on 24/05/2017.
 */
public class AbstractTimeseriesPoint implements Serializable {
    private static final long serialVersionUID = 4167837208008474500L;

    @PartitionKey()
    protected String streamKey;

    @PartitionKey(1)
    protected int month;

    protected String klass;

    @ClusteringColumn
    @Column(codec = InstantCodec.class)
    protected Instant timestamp;

    @Column(name = "id")
    @ProvId
    protected UUID id;

    @Column(codec = MapToJsonCodec.class)
    protected Map<String, Object> metadata;

    protected List<QualityIndicator> quality;

    protected List<String> tags;

    protected List<Classification> classifications;

    @Transient
    protected Map<String, AbstractTimeseriesPoint> combinedPoints;


    public AbstractTimeseriesPoint() {
        this.klass = this.getClass().getName();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Map<String, Object> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, Object> metadata) {
        this.metadata = metadata;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
        LocalDateTime ldt = LocalDateTime.ofInstant(timestamp, ZoneId.systemDefault());
        month = ldt.getMonth().getValue();
    }

    public String getStreamKey() {
        return streamKey;
    }

    public void setStreamKey(String streamKey) {
        this.streamKey = streamKey;
    }

    public List<QualityIndicator> getQuality() {
        return quality;
    }

    public void setQuality(List<QualityIndicator> quality) {
        this.quality = quality;
    }

    public String getKlass() {
        return klass;
    }

    public Class<?> klassObject() throws ClassNotFoundException {
        return Class.forName(klass);
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public Map<String, AbstractTimeseriesPoint> getCombinedPoints() {
        return combinedPoints;
    }

    public void setCombinedPoints(Map<String, AbstractTimeseriesPoint> combinedPoints) {
        this.combinedPoints = combinedPoints;
    }

    public List<Classification> getClassifications() {
        return classifications;
    }

    public void setClassifications(List<Classification> classifications) {
        this.classifications = classifications;
    }

    public int getMonth() {
        return month;
    }

    @Override
    public String toString() {
        return "AbstractTimeseriesPoint{" +
               "streamKey='" + streamKey + '\'' +
               ", month=" + month +
               ", klass='" + klass + '\'' +
               ", timestamp=" + timestamp +
               ", id=" + id +
               ", metadata=" + metadata +
               ", quality=" + quality +
               ", tags=" + tags +
               ", classifications=" + classifications +
               ", combinedPoints=" + combinedPoints +
               '}';
    }
}
