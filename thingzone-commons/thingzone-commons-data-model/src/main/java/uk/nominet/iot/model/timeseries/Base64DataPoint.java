/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.timeseries;

import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.Table;
import com.datastax.driver.mapping.annotations.Transient;
import com.google.gson.annotations.SerializedName;
import org.openprovenance.prov.model.Attribute;
import uk.nominet.iot.annotations.ProvAttribute;
import uk.nominet.iot.annotations.ProvEntity;
import uk.nominet.iot.annotations.TzTimeseries;
import uk.nominet.iot.codec.BytesCodec;
import uk.nominet.iot.common.ThingzoneType;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;

import java.util.Arrays;

@Table( keyspace = "thingzone",
        name = "binary")
@TzTimeseries(index=false)
@ProvEntity(type = ThingzoneType.raw)
public class Base64DataPoint extends AbstractTimeseriesPoint {
    private static final long serialVersionUID = -1691226221070340893L;

    private String contentType;

    @ProvAttribute(attribute = Attribute.AttributeKind.PROV_LABEL)
    @Transient
    private String name = "raw";

    @Column(codec = BytesCodec.class)
    @SerializedName("base64Data")
    private byte[] base64Data;

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }


    public byte[] getBase64Data() {
        return base64Data;
    }

    public void setBase64Data(byte[] base64Data) {
        this.base64Data = base64Data;
    }

    @Override
    public String toString() {
        return "Base64DataPoint{" +
               "contentType='" + contentType + '\'' +
               ", base64Data=" + Arrays.toString(base64Data) +
               ", streamKey='" + streamKey + '\'' +
               ", timestamp=" + timestamp +
               ", id=" + id +
               ", metadata=" + metadata +
               ", quality=" + quality +
               ", tags=" + tags +
               '}';
    }
}
