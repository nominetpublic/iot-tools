/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.registry;

import java.io.Serializable;

/**
 * Additional output streams for a transform function
 */
public class RegistryTransformOutputStream implements Serializable {
    private static final long serialVersionUID = 6154668068059849071L;

    private String key;
    private String alias;

    public RegistryTransformOutputStream() {
    }

    /**
     * Create a new transform output stream
     * @param key the datastream key
     * @param alias the name for the outputStream
     */
    public RegistryTransformOutputStream(String key, String alias) {
        this.key = key;
        this.alias = alias;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @Override
    public String toString() {
        return "RegistryTransformOutputStream{" +
                "key='" + key + '\'' +
                ", alias='" + alias + '\'' +
                '}';
    }
}
