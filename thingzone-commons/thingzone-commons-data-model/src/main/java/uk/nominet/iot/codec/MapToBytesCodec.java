/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.codec;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import com.datastax.driver.core.DataType;
import com.datastax.driver.core.ProtocolVersion;
import com.datastax.driver.core.TypeCodec;
import com.datastax.driver.core.exceptions.InvalidTypeException;
import com.google.common.reflect.TypeToken;

import uk.nominet.iot.common.KryoSerialiser;

public class MapToBytesCodec extends TypeCodec<Map<String, Object>> {

    protected MapToBytesCodec(DataType cqlType, TypeToken<Map<String, Object>> javaType) {
        super(cqlType, javaType);
    }

    @Override
    public ByteBuffer serialize(Map<String, Object> value, ProtocolVersion protocolVersion) throws InvalidTypeException {
        if (value == null)
            return null;
        try {
            return ByteBuffer.wrap(KryoSerialiser.serialiseBase64(value));
        } catch (Exception e) {
            throw new InvalidTypeException(e.getMessage(), e);
        }
    }

    @Override
    public Map<String, Object> deserialize(ByteBuffer bytes, ProtocolVersion protocolVersion) throws InvalidTypeException {
        if (bytes == null)
            return null;
        try {
            byte[] bytesCopy = new byte[bytes.remaining()];
            // always duplicate the ByteBuffer instance before consuming it!
            bytes.duplicate().get(bytesCopy);
            Map<String, Object> map = (HashMap)KryoSerialiser.deserialiseBase64(bytesCopy, HashMap.class);
            return map;
        } catch (Exception e) {
            throw new InvalidTypeException(e.getMessage(), e);
        }
    }

    @Override
    public Map<String, Object> parse(String value) throws InvalidTypeException {
        return null;
    }

    @Override
    public String format(Map<String, Object> value) throws InvalidTypeException {
        return null;
    }
}
