/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.registry.cache;

import uk.nominet.iot.annotations.TzCollection;
import uk.nominet.iot.model.registry.RegistryDataStream;

import java.util.List;

@TzCollection(store = false, indexType = "stream", ignoreFields = "permissions,groupPermissions")
public class DataStreamDocument extends RegistryDataStream {
    private static final long serialVersionUID = -4245789670646300140L;

    private List<String> permissions;

    public List<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions;
    }

    @Override
    public String toString() {
        return "DataStreamDocument{" +
                "permissions=" + permissions +
                ", devices=" + devices +
                ", metadata=" + metadata +
                ", isTransform=" + isTransform +
                ", pushSubscribers=" + pushSubscribers +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", dnsRecords=" + dnsRecords +
                ", key='" + key + '\'' +
                ", userPermissions=" + userPermissions +
                ", groupPermissions=" + groupPermissions +
                '}';
    }

}
