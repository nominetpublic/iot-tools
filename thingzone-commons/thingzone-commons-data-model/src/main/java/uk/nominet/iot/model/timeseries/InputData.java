/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.timeseries;

import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.Table;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import uk.nominet.iot.annotations.TzTimeseries;
import uk.nominet.iot.codec.MapToJsonCodec;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;

import java.util.Map;


@Table(keyspace = "thingzone",
       name = "json")
@TzTimeseries(indexType = "json")
public class InputData extends AbstractTimeseriesPoint {
    private static final long serialVersionUID = 8909874078089756934L;

    @Column(codec = MapToJsonCodec.class)
    @SerializedName("InputData")
    @JsonProperty("InputData")
    protected Map<String, Object> value;

    public Map<String, Object> getValue() {
        return value;
    }

    public void setValue(Map<String, Object> value) {
        this.value = value;
    }


    @Override
    public String toString() {
        return "VdfInputData{" +
               "value=" + value +
               ", streamKey='" + streamKey + '\'' +
               ", month=" + month +
               ", klass='" + klass + '\'' +
               ", timestamp=" + timestamp +
               ", id=" + id +
               ", metadata=" + metadata +
               ", quality=" + quality +
               ", tags=" + tags +
               '}';
    }
}
