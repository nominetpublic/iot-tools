/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.json;

import com.github.filosganga.geogson.gson.GeometryAdapterFactory;
import com.google.gson.*;
import uk.nominet.iot.model.geo.DeviceFeatureCollection;
import uk.nominet.iot.model.geo.TimeseriesFeatureCollection;

import java.lang.reflect.Type;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

public class JsonStringSerialiser {

    private JsonValidator validator;

    protected final Map<Class<?>, Gson> serialisers = new HashMap<>();

    public JsonStringSerialiser() {
        validator = new JsonValidator();
    }

    public JsonStringSerialiser(JsonValidator validator) {
        this.validator = validator;
    }

    public void register(Class<?> type) {
        serialisers.put(type, getDefaultSerialiser());
    }

    public void register(Class<?> type, Gson serialiser) {
        serialisers.put(type, serialiser);
    }

    public String writeObject(Object object) {
        if (object == null)
            throw new IllegalArgumentException("Object cannot be null");
        Gson serialiser;
        if (serialisers.containsKey(object.getClass())) {
            serialiser = serialisers.get(object.getClass());
        } else {
            serialiser = getDefaultSerialiser();
        }

        return serialiser.toJson(object);
    }

    public String writeValidObject(Object object) throws Exception {
        if (object == null)
            throw new IllegalArgumentException("Object cannot be null");
        Gson serialiser;
        if (serialisers.containsKey(object.getClass())) {
            serialiser = serialisers.get(object.getClass());
        } else {
            serialiser = getDefaultSerialiser();
        }

        String serialisedObject = serialiser.toJson(object);
        validator.validate(serialisedObject, object.getClass());

        return serialiser.toJson(object);
    }

    public <T> T readObject(JsonElement input, Class<T> type) {
        if (input == null)
            throw new IllegalArgumentException("Input cannot be null");
        if (type == null)
            throw new IllegalArgumentException("Type cannot be null");
        Gson serialiser;
        if (serialisers.containsKey(type)) {
            serialiser = serialisers.get(type);
        } else {
            serialiser = getDefaultSerialiser();
        }
        return serialiser.fromJson(input, type);
    }

    public <T> T readObject(String input, Class<T> type) {
        if (input == null)
            throw new IllegalArgumentException("Input cannot be null");
        if (type == null)
            throw new IllegalArgumentException("Type cannot be null");
        Gson serialiser;
        if (serialisers.containsKey(type)) {
            serialiser = serialisers.get(type);
        } else {
            serialiser = getDefaultSerialiser();
        }
        return serialiser.fromJson(input, type);
    }

    public <T> T readObject(String input, Type type) {
        if (input == null)
            throw new IllegalArgumentException("Input cannot be null");
        if (type == null)
            throw new IllegalArgumentException("Type cannot be null");
        Gson serialiser = getDefaultSerialiser();
        return serialiser.fromJson(input, type);
    }

    public <T> T readValidObject(String input, Class<T> type) throws Exception {
        validator.validate(input, type);
        if (input == null)
            throw new IllegalArgumentException("Input cannot be null");
        if (type == null)
            throw new IllegalArgumentException("Type cannot be null");
        Gson serialiser;
        if (serialisers.containsKey(type)) {
            serialiser = serialisers.get(type);
        } else {
            serialiser = getDefaultSerialiser();
        }
        return serialiser.fromJson(input, type);
    }

    public Gson getDefaultSerialiser() {
        GsonBuilder gsonBuilder = new GsonBuilder()
                                          .disableHtmlEscaping()
                                          .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                                          .registerTypeHierarchyAdapter(Instant.class,
                                                                        new InstantTypeAdapter())
                                          .registerTypeAdapterFactory(new GeometryAdapterFactory())
                                          .registerTypeAdapterFactory(new TimeseriesTypeAdapterFactory())
                                          .registerTypeAdapterFactory(new TimeseriesTypeAdapterFactory())
                                          .registerTypeHierarchyAdapter(TimeseriesFeatureCollection.class,
                                                                        new TimeseriesFeatureCollectionTypeAdapter())
                                          .registerTypeHierarchyAdapter(DeviceFeatureCollection.class,
                                                                        new DeviceFeatureCollectionTypeAdapter())
                                          .registerTypeHierarchyAdapter(byte[].class,
                                                                        new ByteArrayToBase64TypeAdapter());

        return gsonBuilder.create();
    }

}
