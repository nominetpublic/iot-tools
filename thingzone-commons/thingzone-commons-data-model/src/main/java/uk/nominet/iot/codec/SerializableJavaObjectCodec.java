/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.codec;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.util.Map;

import com.datastax.driver.core.DataType;
import com.datastax.driver.core.ProtocolVersion;
import com.datastax.driver.core.TypeCodec;
import com.datastax.driver.core.exceptions.InvalidTypeException;
import com.google.common.reflect.TypeToken;

public class SerializableJavaObjectCodec extends TypeCodec<Object> {

    protected SerializableJavaObjectCodec() {
        super(DataType.blob(), Object.class);
    }

    protected SerializableJavaObjectCodec(DataType cqlType, TypeToken<Object> javaType) {
        super(cqlType, javaType);
    }

    @Override
    public ByteBuffer serialize(Object value, ProtocolVersion protocolVersion) throws InvalidTypeException {
        if (value == null)
            return null;
        try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            ObjectOutputStream so = new ObjectOutputStream(bo);
            so.writeObject(value);
            so.flush();
            //return ByteBuffer.wrap(Base64.encodeBase64(bo.toByteArray()));
            return ByteBuffer.wrap(bo.toByteArray());

        } catch (Exception e) {
            throw new InvalidTypeException(e.getMessage(), e);
        }
    }

    @Override
    public Object deserialize(ByteBuffer bytes, ProtocolVersion protocolVersion) throws InvalidTypeException {
        if (bytes == null)
            return null;
        try {
            //byte[] b = Base64.decodeBase64(bytes.array());
            byte[] b = bytes.array();
            ByteArrayInputStream bi = new ByteArrayInputStream(b);
            ObjectInputStream si = new ObjectInputStream(bi);
            return si.readObject();
        } catch (Exception e) {
            throw new InvalidTypeException(e.getMessage(), e);
        }
    }

    @Override
    public Map<String, Object> parse(String s) throws InvalidTypeException {
        return null;
    }

    @Override
    public String format(Object t) throws InvalidTypeException {
        return null;
    }
}
