/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.model.timeseries;

import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.Table;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import uk.nominet.iot.annotations.TzTimeseries;

@Table( keyspace = "thingzone",
        name = "metrics")
@TzTimeseries(indexType = "metric")
public class MetricInt extends Metric {
    private static final long serialVersionUID = 6050829844481307379L;

    @Column(name = "t4_value_int")
    @SerializedName("value_int")
    @JsonProperty("value_int")
    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "MetricInt{" +
               "value=" + value +
               ", streamKey='" + streamKey + '\'' +
               ", klass='" + klass + '\'' +
               ", timestamp=" + timestamp +
               ", id=" + id +
               ", metadata=" + metadata +
               ", quality=" + quality +
               ", tags=" + tags +
               '}';
    }
}
