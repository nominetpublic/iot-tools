/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.decoder;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import uk.nominet.iot.decoder.JsonDecoder;

import static org.junit.Assert.*;

public class JsonDecoderTest {
    JsonDecoder jsonDecoder;


    @Before
    public void setUp() throws Exception {
        jsonDecoder = new JsonDecoder();
    }

    @Test
    public void fromBytes() throws Exception {
        byte[] data = "{\"test\":\"json\"}".getBytes();
        JSONObject jsonObject = jsonDecoder.fromBytes(data).toJsonObject();
        assertEquals("json" , jsonObject.getString("test"));
    }

    @Test(expected = JSONException.class)
    public void fromBytesEmpty() throws Exception {
        byte[] data = "".getBytes();
        JSONObject jsonObject = jsonDecoder.fromBytes(data).toJsonObject();
    }

    @Test
    public void fromBytesCharset() throws Exception {
        byte[] data = new String("{\"test\":\"json\"}").getBytes();
        JSONObject jsonObject = jsonDecoder.fromBytes(data, "UTF-8").toJsonObject();
        assertEquals("json" , jsonObject.getString("test"));
    }

    @Test(expected = JSONException.class)
    public void fromBytesDifferentCharset() throws Exception {
        byte[] data = new String("{\"test\":\"json\"}").getBytes();
        JSONObject jsonObject = jsonDecoder.fromBytes(data, "UTF-16LE").toJsonObject();
    }

    @Test
    public void fromBytesNull() throws Exception {
        try {
            jsonDecoder.fromBytes(null);
            fail();
        } catch (Exception e) {
            assertEquals("Data can not be null", e.getMessage());
        }
    }

    @Test
    public void fromString() throws Exception {
        JSONObject jsonObject = jsonDecoder.fromString("{\"test\":\"json\"}").toJsonObject();
        assertEquals("json" , jsonObject.getString("test"));
    }

    @Test(expected = JSONException.class)
    public void fromStringEmpty() throws Exception {
        JSONObject jsonObject = jsonDecoder.fromString("").toJsonObject();
    }

    @Test
    public void fromStringNull() throws Exception {
        try {
            jsonDecoder.fromString(null);
            fail();
        } catch (Exception e) {
            assertEquals("Data can not be null", e.getMessage());
        }


    }


}
