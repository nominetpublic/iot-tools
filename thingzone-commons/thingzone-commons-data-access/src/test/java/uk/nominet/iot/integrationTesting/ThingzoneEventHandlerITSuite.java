/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.integrationTesting;

import com.github.filosganga.geogson.model.Point;
import com.github.filosganga.geogson.model.positions.SinglePosition;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.testcontainers.shaded.com.google.common.collect.ImmutableList;
import uk.nominet.iot.common.Taxonomy;
import uk.nominet.iot.config.RegistryConstant;
import uk.nominet.iot.config.RegistryDriverConfig;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.manager.*;
import uk.nominet.iot.manager.registry.RegistryDataStreamHandler;
import uk.nominet.iot.manager.registry.RegistryDataStreamManager;
import uk.nominet.iot.manager.registry.RegistryDeviceHandler;
import uk.nominet.iot.manager.registry.RegistryDeviceManager;
import uk.nominet.iot.model.Feedback;
import uk.nominet.iot.model.registry.RegistryDevice;
import uk.nominet.iot.model.timeseries.Event;
import uk.nominet.iot.model.timeseries.ThingzoneEventType;

import java.time.Instant;
import java.util.*;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertThat;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ThingzoneEventHandlerITSuite {
    private static JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    private ThingzoneTimeseriesManager manager;
    private static List<UUID> ids = new ArrayList<>();

    private ThingzoneEventManager eventManager;
    private RegistryDataStreamManager streamManager;

    private static Instant timestamp = Instant.now();
    private static String deviceKey = "device.b";
    private static String deviceName = "Device B";
    private static String streamName = String.format("%s.stream.1", deviceKey);
    private static UUID eventId = UUID.randomUUID();
    private static UUID feedbackId1 = UUID.randomUUID();
    private static UUID feedbackId2 = UUID.randomUUID();
    private static Event event;

    @Before
    public void setUp() throws Exception {
        if (RegistryAPIDriver.isInitialized())
            RegistryAPIDriver.globalDispose();
        if (!RegistryAPIDriver.isInitialized()) {
            RegistryDriverConfig registryConfig = new RegistryDriverConfig();
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_SCHEME, "http");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST,
                                       DataAccessIntegrationIT.environment.getServiceHost("registry", 8081));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT,
                                       DataAccessIntegrationIT.environment.getServicePort("registry", 8081));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_USERNAME, "datastore_agent");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD, "datastore_agent");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS, 3);
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }

        //UserContext userContext = new UserContext("test");//.setPassword("test");
        //timeseriesManager = ThingzoneTimeseriesManager.createManager(userContext);
        eventManager = ThingzoneEventManager.createManager();


        event = new Event();
        event.setStreamKey(streamName);
        event.setEventType(ThingzoneEventType.GENERIC);
        event.setSeverity(1.0);
        event.setReceivers(ImmutableList.of("user1","user2"));
        event.setDescription("event description");
        event.setName("event name");
        event.setCategory("event category");
        event.setTimestamp(timestamp);
        event.setLocation(new Point(new SinglePosition(-1.25596, 51.75222, 0)));
        event.setConfiguration(new HashMap<>());
        event.setId(eventId);
        event.setMetadata(new HashMap<>());

        System.out.println(jsonStringSerialiser.writeObject(event));

    }


    @Test
    public void step00_createNewDeviceWithStreams() throws Exception {
        UserContext testUser = new UserContext("test");
        testUser.setPassword("test");
        RegistryDeviceHandler deviceItem = RegistryDeviceManager.createManager(testUser).create(Optional.of(deviceKey),
                                                                                                Optional.of(deviceName),
                                                                                                Optional.of("deviceTypeA"));

        RegistryDataStreamManager streamManager = RegistryDataStreamManager.createManager(testUser);



        RegistryDataStreamHandler streamItem = streamManager.create(Optional.of(streamName),
                                                                    Optional.of(streamName),
                                                                    Optional.of("streamTypeA"));
        streamItem.updateMetadata(new Gson().fromJson("{\"foo\": \"bar\", \"bar\" : {\"foo\" : 1}}", JsonObject.class));
        streamItem.setUserPermission("test1", new String[]{Taxonomy.PERMISSION_UPLOAD,
                                                           Taxonomy.PERMISSION_DISCOVER,
                                                           Taxonomy.PERMISSION_ANNOTATE});

        deviceItem.addStream(streamName, Optional.of(streamName));


        deviceItem.index();
        RegistryDevice device = deviceItem.get();
        Assert.assertNotNull(device);
    }

    @Test
    public void step01_dataUpload() throws Exception {

        Event event = new Event();
        event.setStreamKey(streamName);
        event.setEventType(ThingzoneEventType.GENERIC);
        event.setSeverity(1.0);
        event.setReceivers(ImmutableList.of("user1","user2"));
        event.setDescription("event description");
        event.setName("event name");
        event.setCategory("event category");
        event.setTimestamp(timestamp);
        event.setLocation(new Point(new SinglePosition(-1.25596, 51.75222, 0)));
        event.setConfiguration(new HashMap<>());
        event.setId(eventId);
        event.setMetadata(new HashMap<>());

        ThingzoneEventHandler eventHandler = eventManager.timeseries(streamName);
        eventHandler.addPoint(event);


        JsonObject document = DataAccessIntegrationIT.getElasticDocumentById(
                "thingzone_74657374",
                "event",
                streamName,
                event.getId().toString());

        JSONAssert.assertEquals(jsonStringSerialiser.writeObject(event), document.toString(), JSONCompareMode.STRICT);
    }


    @Test
    public void step02_getEventById() throws Exception {
        ThingzoneEventHandler eventHandler = eventManager.timeseries(streamName);
        Event eventPoint = eventHandler.getPoint(eventId);

        assertEquals(streamName, eventPoint.getStreamKey());
        assertEquals(event.getEventType(), eventPoint.getEventType());
        assertEquals(event.getSeverity(), eventPoint.getSeverity(), 0.01);
        assertThat(eventPoint.getReceivers(), CoreMatchers.hasItems("user1", "user2"));
        assertEquals(event.getDescription(), eventPoint.getDescription());
        assertEquals(event.getName(), eventPoint.getName());
        assertEquals(event.getCategory(), eventPoint.getCategory());
    }

    @Test
    public void step03_addFeedback() throws Exception {
        ThingzoneEventHandler eventHandler = eventManager.timeseries(streamName);

        Feedback feedback = new Feedback();
        feedback.setId(feedbackId1);
        feedback.setCategory("feedback category");
        feedback.setType("feedback type");
        feedback.setTimestamp(timestamp);
        feedback.setDescription("feedback description");
        feedback.setUser("user1");

        eventHandler.addFeedback(eventId, feedback);


        Event eventPoint = eventHandler.getPoint(eventId);
        JsonObject document = DataAccessIntegrationIT.getElasticDocumentById(
                "thingzone_74657374",
                "event",
                streamName,
                eventId.toString());

        assertEquals(1, eventPoint.getFeedback().size());
        JSONAssert.assertEquals(jsonStringSerialiser.writeObject(eventPoint), document.toString(), JSONCompareMode.STRICT);
    }

    @Test
    public void step04_addAdditionalFeedback() throws Exception {
        ThingzoneEventHandler eventHandler = eventManager.timeseries(streamName);

        Feedback feedback = new Feedback();
        feedback.setId(feedbackId2);
        feedback.setCategory("feedback category1");
        feedback.setType("feedback type1");
        feedback.setTimestamp(timestamp);
        feedback.setDescription("feedback description1");
        feedback.setUser("user2");

        eventHandler.addFeedback(eventId, feedback);

        Event eventPoint = eventHandler.getPoint(eventId);

        JsonObject document = DataAccessIntegrationIT.getElasticDocumentById(
                "thingzone_74657374",
                "event",
                streamName,
                eventId.toString());

        assertEquals(2, eventPoint.getFeedback().size());
        JSONAssert.assertEquals(jsonStringSerialiser.writeObject(eventPoint), document.toString(), JSONCompareMode.STRICT);
    }

    @Test
    public void step05_removeFeedback() throws Exception {
        ThingzoneEventHandler eventHandler = eventManager.timeseries(streamName);
        eventHandler.removeFeedback(eventId, feedbackId1, "user1");

        Event eventPoint = eventHandler.getPoint(eventId);
        JsonObject document = DataAccessIntegrationIT.getElasticDocumentById(
                "thingzone_74657374",
                "event",
                streamName,
                eventId.toString());

        assertEquals(1, eventPoint.getFeedback().size());
        JSONAssert.assertEquals(jsonStringSerialiser.writeObject(eventPoint), document.toString(), JSONCompareMode.STRICT);
    }

    @Test
    public void step06_addTags() throws Exception {
        ThingzoneEventHandler eventHandler = eventManager.timeseries(streamName);
        eventHandler.addTag(eventId, "tag1");
        eventHandler.addTag(eventId, "tag2");

        Event eventPoint = eventHandler.getPoint(eventId);
        JsonObject document = DataAccessIntegrationIT.getElasticDocumentById(
                "thingzone_74657374",
                "event",
                streamName,
                eventId.toString());

        assertThat(eventPoint.getTags(),CoreMatchers.hasItems("tag1", "tag2"));
        JSONAssert.assertEquals(jsonStringSerialiser.writeObject(eventPoint), document.toString(), JSONCompareMode.STRICT);
    }

    @Test
    public void step06_removeTag() throws Exception {
        ThingzoneEventHandler eventHandler = eventManager.timeseries(streamName);
        eventHandler.removeTag(eventId, "tag1");

        Event eventPoint = eventHandler.getPoint(eventId);
        JsonObject document = DataAccessIntegrationIT.getElasticDocumentById(
                "thingzone_74657374",
                "event",
                streamName,
                eventId.toString());

        assertThat(eventPoint.getTags(),CoreMatchers.hasItem("tag2"));
        assertThat(eventPoint.getTags(),CoreMatchers.not(CoreMatchers.hasItems("tag1")));
        JSONAssert.assertEquals(jsonStringSerialiser.writeObject(eventPoint), document.toString(), JSONCompareMode.STRICT);
    }

}
