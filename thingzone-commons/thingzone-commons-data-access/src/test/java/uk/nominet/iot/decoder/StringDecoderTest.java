/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.decoder;

import org.junit.Before;
import org.junit.Test;
import uk.nominet.iot.decoder.StringDecoder;

import static org.junit.Assert.*;

public class StringDecoderTest {
    StringDecoder stringDecoder;

    @Before
    public void setUp() throws Exception {
        stringDecoder = new StringDecoder();
    }

    @Test
    public void fromBytes() throws Exception {
        byte[] data = new String("some data").getBytes();
        String string = stringDecoder.fromBytes(data).toString();
        assertEquals("some data", string);
    }

    @Test
    public void fromBytesEmpty() throws Exception {
        byte[] data = new String("").getBytes();
        String string = stringDecoder.fromBytes(data).toString();
        assertEquals("", string);
    }


    @Test
    public void fromBytesCharset() throws Exception {
        byte[] data = new String("some data").getBytes();
        String string = stringDecoder.fromBytes(data, "UTF-8").toString();
        assertEquals("some data", string);
    }

    @Test
    public void fromBytesDifferentCharset() throws Exception {
        byte[] data = new String("some data").getBytes();
        String string = stringDecoder.fromBytes(data, "UTF-16LE").toString();
        assertNotEquals("some data", string);
    }

    @Test
    public void fromBytesNull() throws Exception {
        try {
            stringDecoder.fromBytes(null);
            fail();
        } catch (Exception e) {
            assertEquals("Data can not be null", e.getMessage());
        }
    }

    @Test
    public void fromString() throws Exception {
        String string = stringDecoder.fromString("some data").toString();
        assertEquals("some data", string);
    }

    @Test
    public void fromStringEmpty() throws Exception {
        String string = stringDecoder.fromString("").toString();
        assertEquals("", string);
    }

    @Test
    public void fromStringNull() throws Exception {
        try {
            stringDecoder.fromString(null);
            fail();
        } catch (Exception e) {
            assertEquals("Data can not be null", e.getMessage());
        }
    }


}
