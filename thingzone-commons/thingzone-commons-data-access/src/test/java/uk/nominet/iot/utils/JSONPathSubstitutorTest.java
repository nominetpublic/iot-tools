/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.utils;

import uk.nominet.iot.test.utils.TestConfiguration;
import org.junit.Before;
import org.junit.Test;
import uk.nominet.iot.utils.JSONPathSubstitutor;

import static org.junit.Assert.*;

/**
 Created by edoardo on 08/06/2017.
 */
public class JSONPathSubstitutorTest {

    JSONPathSubstitutor sub;

    @Before
    public void setUp() throws Exception {
        sub = new JSONPathSubstitutor(
                        TestConfiguration
                        .loadJSONFromResource("testObjectsAPI/detectorTemplate1Configuration").toString()
        );
    }

    @Test
    public void canReplaceTextProperty() throws Exception {
        String templateString = "var myString = ${test};";
        String result = sub.replace(templateString);
        assertEquals("var myString = \"test\";", result);
    }

    @Test
    public void canReplaceNumberIntProperty() throws Exception {
        String templateString = "var myInt = ${numberInt};";
        String result = sub.replace(templateString);
        assertEquals("var myInt = 123;", result);
    }

    @Test
    public void canReplaceNumberDoubleProperty() throws Exception {
        String templateString = "var myDouble = ${numberDouble};";
        String result = sub.replace(templateString);
        assertEquals("var myDouble = 12.34;", result);
    }

    @Test
    public void canReplaceStringArrayProperty() throws Exception {
        String templateString = "var myArray = ${arrayOfStrings};";
        String result = sub.replace(templateString);
        assertEquals("var myArray = [\"one\",\"two\",\"three\"];", result);
    }

    @Test
    public void canReplaceFromArrayElement() throws Exception {
        String templateString = "var myArray = ${arrayOfStrings[0]};";
        String result = sub.replace(templateString);
        assertEquals("var myArray = \"one\";", result);
    }

    @Test
    public void canReplaceObject() throws Exception {
        String templateString = "var myObject = ${someObject};";
        String result = sub.replace(templateString);
        assertEquals("var myObject = {\"four\":12.34,\"one\":\"foo\",\"two\":\"bar\",\"three\":123};", result);
    }


    @Test
    public void canReplaceSubpropertyObject() throws Exception {
        String templateString = "var myObject = ${someObject.two};";
        String result = sub.replace(templateString);
        assertEquals("var myObject = \"bar\";", result);
    }

    @Test(expected = com.jayway.jsonpath.PathNotFoundException.class)
    public void arrayIndexOutOfBound() throws Exception {
        String templateString = "var myArray = ${arrayOfStrings[5]};";
        String result = sub.replace(templateString);
    }

    @Test(expected = com.jayway.jsonpath.InvalidPathException.class)
    public void arrayIndexNegative() throws Exception {
        String templateString = "var myArray = ${arrayOfStrings[-5]};";
        String result = sub.replace(templateString);
    }

    @Test(expected = com.jayway.jsonpath.InvalidPathException.class)
    public void invalidPath() throws Exception {
        String templateString = "var myArray = ${___***(()))***};";
        String result = sub.replace(templateString);
    }


}
