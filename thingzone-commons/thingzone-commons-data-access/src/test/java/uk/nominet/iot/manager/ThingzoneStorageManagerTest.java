/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.manager;

import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.testcontainers.shaded.com.google.common.collect.ImmutableList;
import uk.nominet.iot.common.Taxonomy;
import uk.nominet.iot.model.registry.RegistryKey;

import java.util.*;

import static org.junit.Assert.*;

public class ThingzoneStorageManagerTest {

    class MockStorageManager extends ThingzoneStorageManager {

        public MockStorageManager(Optional<UserContext> userContext) {
            super(userContext);
        }

    }

    private MockStorageManager mockStorageManager;

    private RegistryKey registryKey;

    @Before
    public void setUp() throws Exception {
        UserContext userContext = new UserContext("testUser");
        mockStorageManager = new MockStorageManager(Optional.of(userContext));

        registryKey = new RegistryKey();
        registryKey.setKey("entity123");
        Map<String, List<String>> userPermissions = new HashMap<>();
        userPermissions.put("user1", ImmutableList.of(Taxonomy.PERMISSION_ADMIN,
                                                      Taxonomy.PERMISSION_MODIFY,
                                                      Taxonomy.PERMISSION_DISCOVER,
                                                      Taxonomy.PERMISSION_UPLOAD,
                                                      Taxonomy.PERMISSION_VIEW_LOCATION,
                                                      Taxonomy.PERMISSION_VIEW_METADATA,
                                                      Taxonomy.PERMISSION_ANNOTATE));
        userPermissions.put("user2", ImmutableList.of(Taxonomy.PERMISSION_DISCOVER,
                                                      Taxonomy.PERMISSION_UPLOAD,
                                                      Taxonomy.PERMISSION_VIEW_LOCATION,
                                                      Taxonomy.PERMISSION_ANNOTATE));
        registryKey.setUserPermissions(userPermissions);

        Map<String, List<String>> groupPermissions = new HashMap<>();
        userPermissions.put("group1", ImmutableList.of(Taxonomy.PERMISSION_MODIFY,
                                                      Taxonomy.PERMISSION_DISCOVER,
                                                      Taxonomy.PERMISSION_UPLOAD,
                                                      Taxonomy.PERMISSION_VIEW_LOCATION,
                                                      Taxonomy.PERMISSION_VIEW_METADATA,
                                                      Taxonomy.PERMISSION_ANNOTATE));
        userPermissions.put("group2", ImmutableList.of(Taxonomy.PERMISSION_DISCOVER,
                                                      Taxonomy.PERMISSION_UPLOAD,
                                                      Taxonomy.PERMISSION_VIEW_LOCATION,
                                                      Taxonomy.PERMISSION_ANNOTATE));
        registryKey.setGroupPermissions(groupPermissions);


    }

    @Test
    public void entityContainsPermissions() {
        assertTrue(mockStorageManager.hasAllPermissions("user1", registryKey,
                                                                   new String[]{Taxonomy.PERMISSION_ADMIN}));

        assertTrue(mockStorageManager.hasAllPermissions("user1", registryKey,
                                                                   new String[]{Taxonomy.PERMISSION_ADMIN,
                                                                             Taxonomy.PERMISSION_UPLOAD,
                                                                             Taxonomy.PERMISSION_VIEW_METADATA}));

        assertTrue(mockStorageManager.hasAllPermissions("user2", registryKey,
                                                                   new String[]{Taxonomy.PERMISSION_UPLOAD,
                                                                             Taxonomy.PERMISSION_VIEW_LOCATION}));

        assertFalse(mockStorageManager.hasAllPermissions("user2", registryKey,
                                                                    new String[]{Taxonomy.PERMISSION_UPLOAD,
                                                                             Taxonomy.PERMISSION_VIEW_METADATA}));

        assertFalse(mockStorageManager.hasAllPermissions("group1", registryKey,
                                                                    new String[]{Taxonomy.PERMISSION_ADMIN}));

        assertTrue(mockStorageManager.hasAllPermissions("group1", registryKey,
                                                                   new String[]{Taxonomy.PERMISSION_MODIFY}));

        assertTrue(mockStorageManager.hasAllPermissions("group1", registryKey,
                                                                   new String[]{Taxonomy.PERMISSION_UPLOAD,
                                                                             Taxonomy.PERMISSION_VIEW_METADATA}));

        assertTrue(mockStorageManager.hasAllPermissions("group2", registryKey,
                                                                   new String[]{Taxonomy.PERMISSION_UPLOAD,
                                                                             Taxonomy.PERMISSION_VIEW_LOCATION}));
    }

    @Test
    public void getSubjectsForEntityAny() {
        String[] permissions = new String[]{Taxonomy.PERMISSION_DISCOVER};

        assertThat(mockStorageManager.getSubjectsWithAllPermissions(registryKey, permissions),
                   CoreMatchers.hasItems("user1", "user2","group1", "group2"));


    }


    @Test
    public void entityContainsPermissionsAny() {
        assertTrue(mockStorageManager.hasAnyOfPermissions("user1", registryKey,
                                                        new String[]{Taxonomy.PERMISSION_ADMIN}));

        assertTrue(mockStorageManager.hasAnyOfPermissions("user1", registryKey,
                                                        new String[]{Taxonomy.PERMISSION_ADMIN,
                                                                     Taxonomy.PERMISSION_UPLOAD,
                                                                     Taxonomy.PERMISSION_VIEW_METADATA}));

        assertTrue(mockStorageManager.hasAnyOfPermissions("user2", registryKey,
                                                        new String[]{Taxonomy.PERMISSION_UPLOAD,
                                                                     Taxonomy.PERMISSION_VIEW_LOCATION}));

        assertTrue(mockStorageManager.hasAnyOfPermissions("user2", registryKey,
                                                         new String[]{Taxonomy.PERMISSION_UPLOAD,
                                                                      Taxonomy.PERMISSION_VIEW_METADATA}));

        assertFalse(mockStorageManager.hasAnyOfPermissions("group1", registryKey,
                                                         new String[]{Taxonomy.PERMISSION_ADMIN}));

        assertTrue(mockStorageManager.hasAnyOfPermissions("group1", registryKey,
                                                        new String[]{Taxonomy.PERMISSION_MODIFY}));

        assertTrue(mockStorageManager.hasAnyOfPermissions("group1", registryKey,
                                                        new String[]{Taxonomy.PERMISSION_UPLOAD,
                                                                     Taxonomy.PERMISSION_VIEW_METADATA}));

        assertTrue(mockStorageManager.hasAnyOfPermissions("group2", registryKey,
                                                        new String[]{Taxonomy.PERMISSION_UPLOAD,
                                                                     Taxonomy.PERMISSION_VIEW_LOCATION}));
    }

    @Test
    public void getSubjectsForEntity() {
        String[] permissions = new String[]{Taxonomy.PERMISSION_DISCOVER};

        assertThat(mockStorageManager.getSubjectsWithAnyOfPermissions(registryKey, permissions),
                   CoreMatchers.hasItems("user1", "user2","group1", "group2"));


    }

}
