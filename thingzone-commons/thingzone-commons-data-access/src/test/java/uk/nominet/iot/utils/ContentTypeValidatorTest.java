/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.utils;

import uk.nominet.iot.error.InvalidContentTypeException;
import uk.nominet.iot.test.utils.TestConfiguration;
import org.junit.Test;
import uk.nominet.iot.utils.ContentTypeValidator;


import static org.junit.Assert.*;

public class ContentTypeValidatorTest {

    @Test
    public void validJson() throws Exception {
        byte[] data = "{\"test\":\"some value\"}".getBytes();
        ContentTypeValidator.validate("application/json", data);
    }

    @Test
    public void invalidJson() throws Exception {
        byte[] data = "{\"test\":\"some value\"".getBytes();
        try {
            ContentTypeValidator.validate("application/json", data);
            fail("Exception not thrown");
        } catch (InvalidContentTypeException e) {
            assertEquals(e.getMessage(), "Data is not application/json");
        }
    }

    @Test
    public void noData() throws Exception {
        byte[] data = "".getBytes();
        try {
            ContentTypeValidator.validate("application/json", data);
            fail("Exception not thrown");
        } catch (InvalidContentTypeException e) {
            assertEquals(e.getMessage(), "Data is not application/json");
        }
    }

    @Test
    public void unknownType() throws Exception {
        byte[] data = "{\"test\":\"some value\"}".getBytes();
        try {
            ContentTypeValidator.validate("application/mytype", data);
            fail("Exception not thrown");
        } catch (InvalidContentTypeException e) {
            assertEquals(e.getMessage(), "We don't have a validator for application/mytype");
        }
    }

    @Test
    public void image() throws Exception {
        byte[] data = TestConfiguration.loadStringFromResource("sampleImage.jpeg").getBytes();
        ContentTypeValidator.validate("image/jpeg", data);
    }


}
