/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.integrationTesting;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import uk.nominet.iot.config.RegistryConstant;
import uk.nominet.iot.config.RegistryDriverConfig;
import uk.nominet.iot.driver.ProvsvrDriver;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.timeseries.Event;

import uk.nominet.iot.model.timeseries.ThingzoneEventType;
import uk.nominet.iot.provenance.SimpleTestActivityEvents;

import java.util.*;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertThat;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ThingzoneProvenanceITSuite {
    private static JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    @Before
    public void setUp() throws Exception {
        if (RegistryAPIDriver.isInitialized())
            RegistryAPIDriver.globalDispose();

        if (!RegistryAPIDriver.isInitialized()) {
            RegistryDriverConfig registryConfig = new RegistryDriverConfig();
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_SCHEME, "http");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST,
                                       DataAccessIntegrationIT.environment.getServiceHost("registry", 8081));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT,
                                       DataAccessIntegrationIT.environment.getServicePort("registry", 8081));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_USERNAME, "datastore_agent");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD, "datastore_agent");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS, 3);
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }


    }


    @Test
    public void step01_createAPrefix() throws Exception {
        ProvsvrDriver.getAPIClient().prefix.create("xsd", "http://www.w3.org/2001/XMLSchema#");
        ProvsvrDriver.getAPIClient().prefix.create("prov", "http://www.w3.org/ns/prov#");
        ProvsvrDriver.getAPIClient().prefix.create("thingzone", "https://www.yourdomain.uk/thingzone#");

        ProvsvrDriver.getAPIClient().prefix.getAll().forEach(prefix -> {
            System.out.println("Prefix:" + prefix);

        });
    }


    @Test
    public void step02_canCreateActivity() throws Exception {
        SimpleTestActivityEvents simpleActivity = new SimpleTestActivityEvents();
        Event event = new Event();

        event.setId(UUID.randomUUID());
        event.setEventType(ThingzoneEventType.GENERIC);
        event.setName("Event 1");

        simpleActivity.run();
    }
}
