/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.integrationTesting;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import io.searchbox.client.JestResult;
import io.searchbox.core.Get;
import uk.nominet.iot.config.*;
import uk.nominet.iot.driver.CassandraDriver;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.driver.RegistryAPIDriver;

import uk.nominet.iot.init.CassandraInitService;
import uk.nominet.iot.init.ElasticInitService;
import uk.nominet.iot.test.docker.compose.ThingzoneComposeRuntimeContainer;
import uk.nominet.iot.test.docker.compose.ThingzoneEnvironment;
import uk.nominet.iot.tools.BootstrapRegistryData;
import org.apache.commons.io.IOUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

@RunWith(Suite.class)
@Suite.SuiteClasses({
                            RegistryDeviceManagerITSuite.class,
                            RegistryUserManagerTestITSuite.class,
                            ThingzoneTimeseriesHandlerITSuite.class,
                            ThingzoneEventHandlerITSuite.class,
                            ElasticsearchInitServiceITSuite.class,
                            RegistryFunctionITSuite.class,
                            RegistryTransformITSuite.class,
                            //ThingzoneProvenanceITSuite.class
                    })
public class DataAccessIntegrationIT {
    public static final String DOCKER_COMPOSE_TEST_TEST_DATA_CONFIG_JSON = "docker_compose_test/test_data_config.json";
    public static final String DOCKER_COMPOSE_TEST_TEST_DATA_CSV = "docker_compose_test/test_data.csv";
    public static CassandraDriverConfig cassandraConfig;
    public static RegistryDriverConfig registryConfig;
    public static ElasticsearchDriverConfig elasticsearchConfig;
    public static ProvsvrDriverConfig provsvrConfig;


    @ClassRule
    public static ThingzoneComposeRuntimeContainer environment =
            new ThingzoneEnvironment()
                    .withServices(
                            ThingzoneEnvironment.SERVICE_ELASTICSEARCH,
                            ThingzoneEnvironment.SERVICE_CASSANDRA,
                            ThingzoneEnvironment.SERVICE_POSTGRES,
                            ThingzoneEnvironment.SERVICE_REGISTRY,
                            ThingzoneEnvironment.SERVICE_NEO4J
                            //ThingzoneEnvironment.SERVICE_PROVSVR
                                 )
                    .getContainer();

    @BeforeClass
    public static void setUpClass() throws Exception {

        if (!CassandraDriver.isInitialized()) {
            cassandraConfig = new CassandraDriverConfig();
            cassandraConfig.addProperty(CassandraConstant.CASSANDRA_SERVER_HOST,
                                        environment.getServiceHost(ThingzoneEnvironment.SERVICE_CASSANDRA));
            cassandraConfig.addProperty(CassandraConstant.CASSANDRA_SERVER_PORT,
                                        environment.getServicePort(ThingzoneEnvironment.SERVICE_CASSANDRA));
            CassandraDriver.setConnectionProperties(cassandraConfig);
            CassandraDriver.globalInitialise();
        }

        if (!ElasticsearchDriver.isInitialized()) {
            elasticsearchConfig = new ElasticsearchDriverConfig();
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_SCHEME, "http");
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_HOST,
                                            environment.getServiceHost(ThingzoneEnvironment.SERVICE_ELASTICSEARCH));
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_PORT,
                                            environment.getServicePort(ThingzoneEnvironment.SERVICE_ELASTICSEARCH));
            ElasticsearchDriver.setConnectionProperties(elasticsearchConfig);
            ElasticsearchDriver.globalInitialise();
        }

        CassandraInitService cassandraInitService = new CassandraInitService();
        cassandraInitService.clean();
        cassandraInitService.init();

        ElasticInitService elasticInitService = new ElasticInitService();
        elasticInitService.clean();
        elasticInitService.init();

        if (!RegistryAPIDriver.isInitialized()) {
            registryConfig = new RegistryDriverConfig();
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_SCHEME, "http");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST,
                                       environment.getServiceHost(ThingzoneEnvironment.SERVICE_REGISTRY));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT,
                                       environment.getServicePort(ThingzoneEnvironment.SERVICE_REGISTRY));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_USERNAME, "user_admin");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD, "user_admin");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS, 3);
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }


//        if (!ProvsvrDriver.isInitialized()) {
//            provsvrConfig = new ProvsvrDriverConfig();
//            provsvrConfig.addProperty(ProvsvrConstant.PROVSVR_SERVER_SCHEME, "http");
//            provsvrConfig.addProperty(ProvsvrConstant.PROVSVR_SERVER_HOST,
//                                       environment.getServiceHost(ThingzoneEnvironment.SERVICE_PROVSVR));
//            provsvrConfig.addProperty(ProvsvrConstant.PROVSVR_SERVER_PORT,
//                                       environment.getServicePort(ThingzoneEnvironment.SERVICE_PROVSVR));
//            ProvsvrDriver.setConnectionProperties(provsvrConfig);
//            ProvsvrDriver.globalInitialise();
//
//        }

        InputStream jobInputStream = new FileInputStream(DOCKER_COMPOSE_TEST_TEST_DATA_CONFIG_JSON);
        File deviceListCSVFile = new File(DOCKER_COMPOSE_TEST_TEST_DATA_CSV);
        String jsonTxt = IOUtils.toString(jobInputStream, "UTF-8");
        JsonObject job = new Gson().fromJson(jsonTxt, JsonObject.class);

        BootstrapRegistryData bootstrapRegistryData = new BootstrapRegistryData(
                job,
                deviceListCSVFile,
                environment.getServicePort(ThingzoneEnvironment.SERVICE_REGISTRY)
        );
        bootstrapRegistryData.createRegistryUsers();
        bootstrapRegistryData.createFunctions();
        bootstrapRegistryData.createDevices();
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        CassandraDriver.globalDispose();
    }

    public static JsonObject getElasticDocumentById(String index, String type, String parent, String id) throws IOException {

        Get.Builder getBuilder = new Get.Builder(index, id).type(type);
        if (parent != null) getBuilder = getBuilder.setParameter("parent", parent);

        JsonObject document = null;

        JestResult result = ElasticsearchDriver.getClient().execute(getBuilder.build());
        if (result.isSucceeded()) {
            System.out.println("Result OK");
            document =  result.getJsonObject().getAsJsonObject("_source");
        } else {
            System.out.println("Error:" + result.getErrorMessage());

        }

        return document;
    }

}
