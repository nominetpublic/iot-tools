/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.integrationTesting;

import com.google.gson.JsonParser;
import uk.nominet.iot.config.RegistryConstant;
import uk.nominet.iot.config.RegistryDriverConfig;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.manager.UserContext;
import uk.nominet.iot.manager.registry.RegistryFunctionHandler;
import uk.nominet.iot.manager.registry.RegistryFunctionManager;
import uk.nominet.iot.model.registry.RegistryTransformFunction;
import uk.nominet.iot.registry.exceptions.RegistryFailResponseException;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegistryFunctionITSuite {
    public static final String JAVASCRIPT = "javascript";
    RegistryFunctionManager functionManager;
    private static final String functionName = RandomStringUtils.randomAlphanumeric(20);
    private static final String alternativeFunctionName = RandomStringUtils.randomAlphanumeric(20);
    private static final JsonParser jsonParser = new JsonParser();
    private static final String functionContent = "console.log(\"HelloWorld!\");";

    @Before
    public void setUp() throws Exception {
        if (RegistryAPIDriver.isInitialized()) RegistryAPIDriver.globalDispose();
        if (!RegistryAPIDriver.isInitialized()){
            RegistryDriverConfig registryConfig = new RegistryDriverConfig();
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_SCHEME, "http");
            //registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST, "localhost");
            //registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT, 8081);
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST,
                    DataAccessIntegrationIT.environment.getServiceHost("registry", 8081));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT,
                    DataAccessIntegrationIT.environment.getServicePort("registry", 8081));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_USERNAME, "datastore_agent");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD, "datastore_agent");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS, 3);
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }

        UserContext testUser = new UserContext("test");
        testUser.setPassword("test");

        functionManager = RegistryFunctionManager.createManager(testUser);
    }

    @Test
    public void step01_createNewFunction() throws Exception {
        functionManager.create(functionName,
                JAVASCRIPT,
                functionContent,
                false);
    }

    @Test
    public void step02_canGetFunctionFunction() throws Exception {
        RegistryTransformFunction function = functionManager.function(functionName, false).get();
        assertEquals(JAVASCRIPT, function.getFunctionType());
        assertEquals(functionContent, function.getFunctionContent());
    }

    @Test
    public void step03_modifyFunction() throws Exception {
        RegistryFunctionHandler function = functionManager.function(functionName, false);
        function.updateFunction(JAVASCRIPT, "console.log(\"HelloMe!\");");

        RegistryTransformFunction transformFunction = function.get();

        assertEquals(JAVASCRIPT, transformFunction.getFunctionType());
        assertEquals("console.log(\"HelloMe!\");", transformFunction.getFunctionContent());
    }

    @Test
    public void step04_modifyFunctionName() throws Exception {
        RegistryFunctionHandler function = functionManager.function(functionName, false);
        function.updateName(alternativeFunctionName);

        RegistryTransformFunction transformFunction = function.get();
        assertEquals(alternativeFunctionName, transformFunction.getName());
        assertEquals("console.log(\"HelloMe!\");", transformFunction.getFunctionContent());
    }

    @Test
    public void step05_deleteFunction() throws Exception {
        RegistryFunctionHandler function = functionManager.function(alternativeFunctionName, false);
        function.delete();

        try {
            RegistryTransformFunction transformFunction = function.get();
            fail();
        } catch (RegistryFailResponseException e) {
            assertEquals(404, e.getStatusCode());
            assertTrue(e.getMessage().contains("Transform Function not found"));
        }

    }

}
