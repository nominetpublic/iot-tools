/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.provenance;

import com.github.filosganga.geogson.model.Point;
import com.github.filosganga.geogson.model.positions.SinglePosition;
import org.openprovenance.prov.model.Attribute;
import uk.nominet.iot.annotations.ProvActivity;
import uk.nominet.iot.annotations.ProvAttribute;
import uk.nominet.iot.annotations.ProvId;
import uk.nominet.iot.common.ThingzoneType;
import uk.nominet.iot.error.ProvenanceRecordViolationException;
import uk.nominet.iot.model.timeseries.Event;
import uk.nominet.iot.model.timeseries.ThingzoneEventType;

import java.time.Instant;
import java.util.UUID;

@ProvActivity(type = ThingzoneType.process)
public class SimpleTestActivityEvents {
    @ProvId
    private String processId = "CameraMovementSensing1";

    @ProvAttribute(attribute = Attribute.AttributeKind.PROV_LABEL)
    protected String name = "Camera Movement Sensing";


    public Event run() throws Exception {
        Event event = new Event();
        try {
            ProvenanceManager pm = new ProvenanceManager("app");
            ThingzoneActivity ac = new ThingzoneActivity(pm, this);

            ac.start();

            Thread.sleep(2000);

            // Generating some output

            event.setId(UUID.randomUUID());
            event.setName("TestEvent");
            event.setLocation(new Point(new SinglePosition(51.76, -1.23, 0.0)));
            event.setTimestamp(Instant.now());
            event.setEventType(ThingzoneEventType.GENERIC);
            ac.generated(event, "event", Instant.now());

            ac.associatedWith(pm.qn("app",UUID.randomUUID().toString()), pm.qn("app","Joe"));
            ac.associatedWith(pm.qn("app",UUID.randomUUID().toString()), pm.qn("app","GCS"));


            ac.stop();

            ac.commitToProvenanceRecord();
        } catch (ProvenanceRecordViolationException e) {
            // The process is doing something invalid
            // rollback?
            e.printStackTrace();
        }

        return event;
    }

}
