/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.provenance;

import com.github.filosganga.geogson.model.Point;
import com.github.filosganga.geogson.model.positions.SinglePosition;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import org.junit.Ignore;
import org.junit.Test;
import org.openprovenance.prov.model.Document;
import org.openprovenance.prov.model.StatementOrBundle;
import uk.nominet.iot.model.registry.RegistryDevice;
import uk.nominet.iot.model.timeseries.Event;

import java.util.Arrays;
import java.util.UUID;

@Ignore
public class ProvenanceManagerTest {



    @Test
    public void canBuildProvForDevice() throws Exception {
        RegistryDevice device = new RegistryDevice();
        device.setKey("device1");
        device.setLocation(new Point(new SinglePosition(51.76, -1.23, 0.0)));
        device.setName("Device 1");

        ProvenanceManager provenancemanager = new ProvenanceManager("app");

        StatementOrBundle prov = provenancemanager.toProvStatement(device);
        Document provDocument = provenancemanager.newDocument();

        provDocument.getStatementOrBundle()
                    .addAll(Arrays.asList(new StatementOrBundle[]{prov}));

        JsonObject document = ProvenanceManager.documentToJson(provDocument);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        System.out.println("Prov: \n " + gson.toJson(document));

    }

}
