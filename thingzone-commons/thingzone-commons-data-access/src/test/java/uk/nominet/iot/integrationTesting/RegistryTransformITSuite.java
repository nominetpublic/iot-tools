/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.integrationTesting;

import com.google.gson.JsonObject;
import uk.nominet.iot.config.RegistryConstant;
import uk.nominet.iot.config.RegistryDriverConfig;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.manager.UserContext;
import uk.nominet.iot.manager.registry.RegistryFunctionManager;
import uk.nominet.iot.manager.registry.RegistryTransformHandler;
import uk.nominet.iot.manager.registry.RegistryTransformManager;
import uk.nominet.iot.model.registry.RegistryTransform;
import uk.nominet.iot.registry.exceptions.RegistryFailResponseException;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.Optional;

import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegistryTransformITSuite {
    static RegistryTransformManager transformManager;
    private static final String functionName = RandomStringUtils.randomAlphabetic(20);
    private static final String functionNameAlternative = RandomStringUtils.randomAlphanumeric(20);
    private static final String transformName = RandomStringUtils.randomAlphabetic(20).toLowerCase();
    public static final String JAVASCRIPT = "javascript";
    private static final String functionContent = "console.log(\"HelloWorld!\");";


    private RegistryTransformHandler registryItem;

    @BeforeClass
    public static void setUpClass() throws Exception {
        if (RegistryAPIDriver.isInitialized()) RegistryAPIDriver.globalDispose();
        if (!RegistryAPIDriver.isInitialized()){
            RegistryDriverConfig registryConfig = new RegistryDriverConfig();
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_SCHEME, "http");
            //registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST, "localhost");
            //registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT, 8081);
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST,
                    DataAccessIntegrationIT.environment.getServiceHost("registry", 8081));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT,
                    DataAccessIntegrationIT.environment.getServicePort("registry", 8081));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_USERNAME, "datastore_agent");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD, "datastore_agent");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS, 3);
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }

        UserContext testUser = new UserContext("test");
        testUser.setPassword("test");

        RegistryFunctionManager functionManager = RegistryFunctionManager.createManager(testUser);

        functionManager.create(functionName,
                JAVASCRIPT,
                functionContent,
                false);

        functionManager.create(functionNameAlternative,
                JAVASCRIPT,
                functionContent,
                false);


        transformManager = RegistryTransformManager.createManager(testUser);
    }

    @Before
    public void setUp() throws Exception {
        registryItem = transformManager.transform(transformName);
    }

    @Test
    public void step01_createNewTransform() throws Exception {
        JsonObject parameters = new JsonObject();
        parameters.addProperty("foo", 123);
        parameters.addProperty("bar", 1.25);


        registryItem = transformManager.create(Optional.of(transformName),
                Optional.of(transformName),
                Optional.of("transformTypeA"),
                Optional.of("scheduleString"),
                functionName,
                false,
                parameters);

        JsonObject metadata = new JsonObject();
        metadata.addProperty("foo", "bar");
        metadata.addProperty("boo", "hello");

        registryItem.updateMetadata(metadata);
        registryItem.index();
        Thread.sleep(5000);
    }


    @Test
    public void step02_canGetTransform() throws Exception {
        RegistryTransform transform = registryItem.get();
        assertEquals(transformName, transform.getKey());
        assertNotNull(transform.getMetadata());
    }

    @Test
    public void step03_updateTransformFunction() throws Exception {
        registryItem.updateFunction(functionNameAlternative, false);

        RegistryTransform transform = transformManager.transform(transformName).get();
        assertEquals(functionNameAlternative,transform.getFunction().getName());
    }


    @Test
    public void step04_updateSources() throws Exception {
        registryItem.updateSource("device1.stream1",
                                  "stream1-alias",
                                  true,
                                  Optional.empty(),
                                  Optional.empty());
        registryItem.updateSource("device1.stream2",
                                  "stream2-alias",
                                  false,
                                  Optional.empty(),
                                  Optional.empty());
        registryItem.updateSource("device1.stream3",
                                  "stream3-alias",
                                  false,
                                  Optional.empty(),
                                  Optional.empty());

        RegistryTransform transform = transformManager.transform(transformName).get();
        assertEquals(transformName,transform.getKey());
        assertEquals(functionNameAlternative,transform.getFunction().getName());
        assertEquals(3,transform.getSources().size());

    }

    @Test
    public void step05_updateParameters() throws Exception {
        JsonObject parameters = new JsonObject();
        parameters.addProperty("foo", 345.45);
        parameters.addProperty("bar", 12);

        registryItem.updateParameters(parameters);

        RegistryTransform transform = transformManager.transform(transformName).get();

        assertEquals(transformName,transform.getKey());
        assertEquals(functionNameAlternative,transform.getFunction().getName());
        assertEquals(345.45, transform.getParameterValues().get("foo"));
        assertEquals(12.0, transform.getParameterValues().get("bar"));

    }


    @Test
    public void step06_deleteTransform() throws Exception {
        registryItem.delete();

        try {
            RegistryTransform transform = registryItem.get();
            fail();
        } catch (RegistryFailResponseException e) {
            assertTrue(e.getMessage().contains("Transform not found:"));
        }
    }




}
