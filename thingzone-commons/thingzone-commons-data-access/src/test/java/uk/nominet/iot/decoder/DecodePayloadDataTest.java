/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.decoder;

import org.json.JSONObject;
import org.junit.Test;
import uk.nominet.iot.decoder.Decode;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class DecodePayloadDataTest {
    @Test
    public void stringFromBytes() throws Exception {
        byte[] data = "some data".getBytes();
        String string = Decode.forContentType("text/plain").fromBytes(data).toString();
        assertEquals("some data", string);
    }

    @Test
    public void stringFromBytesToJson() throws Exception {
        byte[] data = "some data".getBytes();
        try {
            Decode.forContentType("text/plain").fromBytes(data).toJsonObject();
            fail();
        } catch (Exception e) {
            assertEquals("Not a Json Object", e.getMessage());
        }

    }

    @Test
    public void jsonFromBytes() throws Exception {
        byte[] data = new String("{\"test\":\"json\"}").getBytes();
        JSONObject jsonObject = Decode.forContentType("application/json").fromBytes(data).toJsonObject();
        assertEquals("json" , jsonObject.getString("test"));
    }

    @Test
    public void jsonFromBytesToString() throws Exception {
        byte[] data = new String("{\"test\":\"json\"}").getBytes();
        String string = Decode.forContentType("application/json").fromBytes(data).toString();
        assertEquals("{\"test\":\"json\"}", string);
    }

    @Test
    public void unknownFromBytes() throws Exception {
        byte[] data = "some data".getBytes();

        try {
            Decode.forContentType("unknown/type").fromBytes(data).toJsonObject();
            fail();
        } catch (Exception e) {
            assertEquals("Content type not recognised", e.getMessage());
        }

    }
}
