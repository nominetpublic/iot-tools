/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.integrationTesting;

import com.google.gson.JsonObject;
import io.searchbox.action.Action;
import io.searchbox.client.JestResult;
import io.searchbox.indices.aliases.GetAliases;
import io.searchbox.indices.mapping.GetMapping;
import io.searchbox.indices.template.GetTemplate;
import uk.nominet.iot.driver.ElasticsearchDriver;
import org.json.JSONObject;
import org.junit.*;
import org.junit.runners.MethodSorters;
import uk.nominet.iot.init.ElasticInitService;

import java.io.IOException;

import static uk.nominet.iot.test.data.TestDataGenerator.uploadDeviceElasticsearch;
import static uk.nominet.iot.test.data.TestDataGenerator.uploadStreamElasticsearch;
import static uk.nominet.iot.utils.ElasticMappingsUtils.getTemplate;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ElasticsearchInitServiceITSuite {

    @Test
    public void step01_initElasticEmptyDatabase() throws Exception {
        JSONObject mappingTemplate =  getTemplate();

        GetTemplate getTemplateAction = new GetTemplate.Builder("thingzone_template").build();
        JestResult templateResult = ElasticsearchDriver.getClient().execute(getTemplateAction);

        if (templateResult.isSucceeded()) {
            JsonObject templateResultJsonObject = templateResult.getJsonObject();
            if (!templateResultJsonObject.has("thingzone_template")) fail();
            JsonObject thingzoneTemplateObject = templateResultJsonObject.get("thingzone_template").getAsJsonObject();
            assertEquals("thingzone_*",  thingzoneTemplateObject.get("template").getAsString());
            assertEquals(mappingTemplate.getInt("version"),  thingzoneTemplateObject.get("version").getAsInt());
            if (! thingzoneTemplateObject.has("mappings")) fail();
            JsonObject mappings = thingzoneTemplateObject.get("mappings").getAsJsonObject();
            assertJsonEquals(mappings.toString(), mappingTemplate.get("mappings").toString());
        } else {
            fail();
        }
    }

    @Test
    public void step02_initElasticDatabaseChangeModel() throws Exception {
        JSONObject mappingTemplate = getTemplate();
        for (int i = 0; i < 50; i++) {
            uploadDeviceElasticsearch(ElasticsearchDriver.getClient(), "thingzone_7465737455736572",
                    "device"+i);
            uploadDeviceElasticsearch(ElasticsearchDriver.getClient(), "thingzone_7465737455736573",
                    "device"+i+"a");

            for (int j=0; j < 10; j++ ) {
                uploadStreamElasticsearch(ElasticsearchDriver.getClient(), "thingzone_7465737455736572",
                        "stream"+i+"_"+j,
                        "device"+i);
                uploadStreamElasticsearch(ElasticsearchDriver.getClient(), "thingzone_7465737455736573",
                        "stream"+i+"a_"+j,
                        "device"+i+"a");
            }

        }

        mappingTemplate.put("version", 100);

        JSONObject newTestMapping = new JSONObject("{\n" +
                "    \"_parent\": {\n" +
                "      \"type\": \"stream\"\n" +
                "    },\n" +
                "    \"properties\": {\n" +
                "      \"@timestamp\": {\n" +
                "        \"doc_values\": true,\n" +
                "        \"type\": \"date\"\n" +
                "      }\n" +
                "    }\n" +
                "  }");

        mappingTemplate.getJSONObject("mappings").put("testtype", newTestMapping);

        new ElasticInitService().initElasticsearch(mappingTemplate);

        GetTemplate getTemplateAction = new GetTemplate.Builder("thingzone_template").build();
        JestResult templateResult = ElasticsearchDriver.getClient().execute(getTemplateAction);

        //Thread.sleep(3000);

        if (templateResult.isSucceeded()) {
            JsonObject templateResultJsonObject = templateResult.getJsonObject();
            if (!templateResultJsonObject.has("thingzone_template")) fail();
            JsonObject thingzoneTemplateObject = templateResultJsonObject.get("thingzone_template").getAsJsonObject();
            assertEquals("thingzone_*",  thingzoneTemplateObject.get("template").getAsString());
            assertEquals(mappingTemplate.getInt("version"),  thingzoneTemplateObject.get("version").getAsInt());
            if (! thingzoneTemplateObject.has("mappings")) fail();
            JsonObject mappings = thingzoneTemplateObject.get("mappings").getAsJsonObject();
            assertJsonEquals(mappings.toString(), mappingTemplate.get("mappings").toString());
        } else {
            fail();
        }

        GetAliases aliasesAction = new GetAliases.Builder().build();
        JestResult aliasesRes = ElasticsearchDriver.getClient().execute(aliasesAction);
        JsonObject aliases = aliasesRes.getJsonObject();
        aliases.entrySet().forEach(index -> {
            String indexName = index.getKey();

            Action<?> getMapping = new GetMapping.Builder().addIndex(indexName).build();
            JestResult mappingResults = null;
            try {
                mappingResults = ElasticsearchDriver.getClient().execute(getMapping);
            } catch (IOException e) {
                fail();
                e.printStackTrace();
            }
            if (mappingResults.isSucceeded()) {
                JsonObject newTestMappingObject = mappingResults
                        .getJsonObject()
                        .getAsJsonObject(indexName)
                        .getAsJsonObject("mappings")
                        .getAsJsonObject("testtype");

                assertNotNull(newTestMappingObject);

            } else {
                fail();
            }
        });
    }
}
