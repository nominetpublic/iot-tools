/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.testUtils;

import uk.nominet.iot.registry.*;

import static org.mockito.Mockito.*;

public class MockRegistryAPI {
    public static RegistryAPISession getMockedRegistryAPI(String sessionKey) {
        RegistryAPISession api = mock(RegistryAPISession.class);
        //api.setSessionKey(sessionKey);

        InternalAPI internal = mock(InternalAPI.class);
        api.internal = internal;

        SessionAPI session = mock(SessionAPI.class);
        api.session = session;

        DataStreamAPI dataStream = mock(DataStreamAPI.class);
        api.datastream = dataStream;

        SearchAPI search = mock(SearchAPI.class);
        api.search = search;

        DumpAPI dump = mock(DumpAPI.class);
        api.dump = dump;

        return api;
    }
}
