/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.integrationTesting;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import uk.nominet.iot.common.Taxonomy;
import uk.nominet.iot.config.RegistryConstant;
import uk.nominet.iot.config.RegistryDriverConfig;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.manager.ThingzoneTimeseriesHandler;
import uk.nominet.iot.manager.ThingzoneTimeseriesManager;
import uk.nominet.iot.manager.UserContext;
import uk.nominet.iot.manager.registry.RegistryDataStreamHandler;
import uk.nominet.iot.manager.registry.RegistryDataStreamManager;
import uk.nominet.iot.manager.registry.RegistryDeviceHandler;
import uk.nominet.iot.manager.registry.RegistryDeviceManager;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;
import uk.nominet.iot.model.registry.RegistryDevice;
import uk.nominet.iot.model.timeseries.*;
import uk.nominet.iot.test.data.MetricGenerator;
import org.junit.*;
import org.junit.runners.MethodSorters;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;


import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertThat;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ThingzoneTimeseriesHandlerITSuite {

    private static final int TOTAL_STREAMS = 5;

    private ThingzoneTimeseriesManager manager;
    private static List<UUID> ids = new ArrayList<>();

    private ThingzoneTimeseriesManager timeseriesManager;
    private RegistryDataStreamManager streamManager;

    private static Instant initialTime = Instant.now();
    private static String deviceKey = "device.a";
    private static String deviceName = "Device A";

    @Before
    public void setUp() throws Exception {
        if (RegistryAPIDriver.isInitialized())
            RegistryAPIDriver.globalDispose();
        if (!RegistryAPIDriver.isInitialized()) {
            RegistryDriverConfig registryConfig = new RegistryDriverConfig();
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_SCHEME, "http");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST,
                                       DataAccessIntegrationIT.environment.getServiceHost("registry", 8081));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT,
                                       DataAccessIntegrationIT.environment.getServicePort("registry", 8081));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_USERNAME, "datastore_agent");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD, "datastore_agent");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS, 3);
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }

        //UserContext userContext = new UserContext("test");//.setPassword("test");
        //timeseriesManager = ThingzoneTimeseriesManager.createManager(userContext);
        timeseriesManager = ThingzoneTimeseriesManager.createManager();
    }


    @Test
    public void step00_createNewDeviceWithStreams() throws Exception {
        UserContext testUser = new UserContext("test");
        testUser.setPassword("test");
        RegistryDeviceHandler deviceItem = RegistryDeviceManager.createManager(testUser).create(Optional.of(deviceKey),
                                                                                                Optional.of(deviceName),
                                                                                                Optional.of("deviceTypeA"));

        RegistryDataStreamManager streamManager = RegistryDataStreamManager.createManager(testUser);

        for (int i = 1; i <= TOTAL_STREAMS; i++) {
            String streamName = String.format("%s.stream.%d", deviceKey, i);
            RegistryDataStreamHandler streamItem = streamManager.create(Optional.of(streamName),
                                                                        Optional.of(streamName),
                                                                        Optional.of("streamTypeA"));
            streamItem.updateMetadata(new Gson().fromJson("{\"foo\": \"bar\", \"bar\" : {\"foo\" : 1}}", JsonObject.class));
            streamItem.setUserPermission("test1", new String[]{Taxonomy.PERMISSION_UPLOAD, Taxonomy.PERMISSION_DISCOVER});

            deviceItem.addStream(streamName, Optional.of(streamName));
        }

        deviceItem.index();
        RegistryDevice device = deviceItem.get();
        Assert.assertNotNull(device);
    }

    @Test
    public void step01_dataUpload() throws Exception {


        Instant latestTime = null;

        for (int i = 1; i <= TOTAL_STREAMS; i++) {
            String streamName = String.format("%s.stream.%d", deviceKey, i);

            ThingzoneTimeseriesHandler<MetricDouble> timeseriesMetricDouble =
                    timeseriesManager.timeseries(streamName, MetricDouble.class);

            for (Instant time = initialTime.minus(10, ChronoUnit.HOURS);
                 time.isBefore(initialTime) || time.equals(initialTime);
                 time = time.plus(15, ChronoUnit.MINUTES)) {

                UUID id = UUID.randomUUID();
                ids.add(id);

                MetricDouble metric = MetricGenerator.generateMetricDouble(
                        id,
                        streamName,
                        (i + 0.5)
                                                                          );

                metric.setTimestamp(time);

                timeseriesMetricDouble.addPoint(metric);
                latestTime = time;
            }
        }

        assertEquals(TOTAL_STREAMS * 41, ids.size());

    }


    @Test
    public void step02_getPointById() throws Exception {
        ThingzoneTimeseriesHandler<MetricDouble> timeseriesMetricDouble =
                timeseriesManager.timeseries(deviceKey + ".stream.1", MetricDouble.class);
        MetricDouble metricDouble = timeseriesMetricDouble.getPoint(ids.get(0));

        assertNotNull(metricDouble);
        assertEquals(1.5, metricDouble.getValue(), 0.01);
    }

    @Test
    public void step03_findLatest() throws Exception {
        ThingzoneTimeseriesHandler<MetricDouble> timeseriesMetricDouble =
                timeseriesManager.timeseries(deviceKey + ".stream.1", MetricDouble.class);
        MetricDouble metricDouble = timeseriesMetricDouble.findLatest();

        assertEquals(metricDouble.getTimestamp(), initialTime);

    }

    @Test
    public void step04_findFromTo() throws Exception {
        ThingzoneTimeseriesHandler<MetricDouble> timeseriesMetricDouble =
                timeseriesManager.timeseries(deviceKey + ".stream.1", MetricDouble.class);

        Instant from = Instant.now().minus(2, ChronoUnit.HOURS);
        Instant to = Instant.now();

        List<MetricDouble> metrics = timeseriesMetricDouble.find(
                from,
                to
                                                                );

        assertEquals(8, metrics.size());

        metrics.forEach(metric -> {
            assertNotNull(metric);
            assertTrue(metric.getTimestamp().isAfter(from));
            assertTrue(metric.getTimestamp().isBefore(to));
        });
    }


    @Test
    public void step05_anStoreAndIndexAIntMetric() throws Exception {
        String json = "{\n" +
                      "\t\"value_int\": 2,\n" +
                      "\t\"klass\": \"uk.nominet.iot.model.timeseries.MetricInt\",\n" +
                      "\t\"deviceKey\": \"device1.stream1\",\n" +
                      "\t\"timestamp\": \"" + Instant.now() + "\",\n" +
                      "\t\"id\": \"b433a959-57a4-4d98-9299-c464af72c983\",\n" +
                      "\t\"metadata\": {\n" +
                      "\t\t\"foo\": \"bar\"\n" +
                      "\t}\n" +
                      "}";


        JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
        AbstractTimeseriesPoint abstractTimeseriesPoint = jsonStringSerialiser.readObject(json, AbstractTimeseriesPoint.class);
        Class<?> pointClass = abstractTimeseriesPoint.klassObject();

        abstractTimeseriesPoint.setTimestamp(abstractTimeseriesPoint.getTimestamp());

        AbstractTimeseriesPoint point = (AbstractTimeseriesPoint) jsonStringSerialiser.readObject(json, pointClass);
        point.setTimestamp(point.getTimestamp());

        UUID id = timeseriesManager.timeseries("device1.stream1", pointClass).addPoint(point);

        ThingzoneTimeseriesHandler<MetricInt> metricLatLongThingzoneTimeseriesHandler =
                timeseriesManager.timeseries("device1.stream1", MetricInt.class);
        MetricInt metrincInt = metricLatLongThingzoneTimeseriesHandler.findLatest();

        System.out.println("Metric: " + metrincInt);


        assertTrue(metrincInt.getId().equals(id));

    }

    /*
    {"value_text":"ROUTE1234567","deviceKey":"fusion.route","timestamp":"1970-01-18T15:42:18.410Z","klass":"iot.nominet.model.timeseries.MetricString","id":"af8d8d5a-e6e2-4e6e-8e18-055b60a13b3f"}
    {"value_lat_long":{"lat":3051.054259058843,"lon":-77.84270943147845},"deviceKey":"fusion.location","timestamp":"1970-01-18T15:42:18.410Z","klass":"iot.nominet.model.timeseries.MetricLatLong","id":"e3e6c2eb-c3cb-4332-acc7-3811eec60d83","metadata":{"unit":"latlong"}}

     */
    @Test
    public void step06_canStoreAndIndexALatLongMetric() throws Exception {
        String json = "{\n" +
                      "\t\"value_lat_long\": {\n" +
                      "\t\t\"lat\": 51.054259058843,\n" +
                      "\t\t\"lon\": -1.84270943147845\n" +
                      "\t},\n" +
                      "\t\"deviceKey\": \"device1.stream1\",\n" +
                      "\t\"timestamp\": \"" + Instant.now() + "\",\n" +
                      "\t\"klass\": \"uk.nominet.iot.model.timeseries.MetricLatLong\",\n" +
                      "\t\"id\": \"e3e6c2eb-c3cb-4332-acc7-3811eec60d83\",\n" +
                      "\t\"metadata\": {\n" +
                      "\t\t\"unit\": \"latlong\"\n" +
                      "\t}\n" +
                      "}";

        JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
        AbstractTimeseriesPoint abstractTimeseriesPoint = jsonStringSerialiser.readObject(json, AbstractTimeseriesPoint.class);
        abstractTimeseriesPoint.setTimestamp(abstractTimeseriesPoint.getTimestamp());

        Class<?> pointClass = abstractTimeseriesPoint.klassObject();

        AbstractTimeseriesPoint point = (AbstractTimeseriesPoint) jsonStringSerialiser.readObject(json, pointClass);
        point.setTimestamp(point.getTimestamp());
        UUID id = timeseriesManager.timeseries("device1.stream1", pointClass).addPoint(point);

        ThingzoneTimeseriesHandler<MetricLatLong> metricLatLongThingzoneTimeseriesHandler =
                timeseriesManager.timeseries("device1.stream1", MetricLatLong.class);
        MetricLatLong metricLatLong = metricLatLongThingzoneTimeseriesHandler.findLatest();


        assertTrue(metricLatLong.getId().equals(id));
    }

    @Test
    public void step07_canStoreAndIndexAStringMetric() throws Exception {
        String json = "{\n" +
                      "\t\"value_text\": \"ROUTE1234567\",\n" +
                      "\t\"deviceKey\": \"device1.stream1\",\n" +
                      "\t\"timestamp\": \"" + Instant.now() + "\",\n" +
                      "\t\"klass\": \"uk.nominet.iot.model.timeseries.MetricString\",\n" +
                      "\t\"id\": \"af8d8d5a-e6e2-4e6e-8e18-055b60a13b3f\"\n" +
                      "}";

        JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
        AbstractTimeseriesPoint abstractTimeseriesPoint = jsonStringSerialiser.readObject(json, AbstractTimeseriesPoint.class);
        abstractTimeseriesPoint.setTimestamp(abstractTimeseriesPoint.getTimestamp());

        Class<?> pointClass = abstractTimeseriesPoint.klassObject();

        AbstractTimeseriesPoint point = (AbstractTimeseriesPoint) jsonStringSerialiser.readObject(json, pointClass);
        point.setTimestamp(point.getTimestamp());
        UUID id = timeseriesManager.timeseries("device1.stream1", pointClass).addPoint(point);

        ThingzoneTimeseriesHandler<MetricString> metricStringThingzoneTimeseriesHandler =
                timeseriesManager.timeseries("device1.stream1", MetricString.class);
        MetricString metricLatLong = metricStringThingzoneTimeseriesHandler.findLatest();

        assertTrue(metricLatLong.getId().equals(id));
    }

}
