/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.integrationTesting;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import uk.nominet.iot.common.Taxonomy;
import uk.nominet.iot.config.RegistryConstant;
import uk.nominet.iot.config.RegistryDriverConfig;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.error.DataStorageException;
import uk.nominet.iot.manager.UserContext;
import uk.nominet.iot.manager.registry.RegistryUserHandler;
import uk.nominet.iot.manager.registry.RegistryUserManager;
import uk.nominet.iot.model.registry.RegistryUser;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegistryUserManagerTestITSuite {
    private RegistryUserManager userManager;

    @Before
    public void setUp() throws Exception {
        if (RegistryAPIDriver.isInitialized()) RegistryAPIDriver.globalDispose();
        if (!RegistryAPIDriver.isInitialized()){
            RegistryDriverConfig registryConfig = new RegistryDriverConfig();
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_SCHEME, "http");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST,
                    DataAccessIntegrationIT.environment.getServiceHost("registry", 8081));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT,
                    DataAccessIntegrationIT.environment.getServicePort("registry", 8081));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_USERNAME, "user_admin");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD, "user_admin");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS, 3);
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }

        userManager = RegistryUserManager.createManager();
    }

    @Test
    public void step01_createNewUser() throws Exception {
        userManager.create(
                "testUser",
                "testPassword",
                new String[]{Taxonomy.PRIVILEGE_CREATE_DEVICE, Taxonomy.PRIVILEGE_CREATE_DATASTREAM}
        );
    }

    @Test(expected = DataStorageException.class)
    public void step02_updateUserAsAdminNotAllowed() throws Exception {
        RegistryUserHandler userItem = userManager.user("testUser");
        userItem.updateDetails(new Gson().fromJson("{\"foo\":\"bar\"}", JsonObject.class));
        userItem.updatePreferences(new Gson().fromJson("{\"bar\":\"foo\"}", JsonObject.class));
    }

    @Test
    public void step03_updateUser() throws Exception {
        UserContext userContext = new UserContext("testUser");
        userContext.setPassword("testPassword");
        RegistryUserManager testUserManager = RegistryUserManager.createManager(userContext);

        RegistryUserHandler userItem = testUserManager.user("testUser");
        userItem.updateDetails(new Gson().fromJson("{\"foo\":\"bar\"}", JsonObject.class));
        userItem.updatePreferences(new Gson().fromJson("{\"bar\":\"foo\"}", JsonObject.class));
        RegistryUser user = userItem.get();

        assertEquals("bar", user.getDetails().get("foo"));
        assertEquals("foo", user.getPreferences().get("bar"));
    }

    @Test
    public void step04_getUserInfo() throws Exception {
        RegistryUserHandler userItem = userManager.user("testUser");
        RegistryUser user = userItem.get();

        assertEquals("testUser", user.getUsername());
        assertEquals("bar", user.getDetails().get("foo"));
        assertEquals("foo", user.getPreferences().get("bar"));
    }

}

