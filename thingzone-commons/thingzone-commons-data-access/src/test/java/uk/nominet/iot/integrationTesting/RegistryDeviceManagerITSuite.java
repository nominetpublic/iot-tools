/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.integrationTesting;

import com.github.filosganga.geogson.model.Point;
import com.github.filosganga.geogson.model.positions.SinglePosition;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import uk.nominet.iot.common.Taxonomy;
import uk.nominet.iot.config.RegistryConstant;
import uk.nominet.iot.config.RegistryDriverConfig;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.manager.UserContext;
import uk.nominet.iot.manager.registry.RegistryDataStreamHandler;
import uk.nominet.iot.manager.registry.RegistryDataStreamManager;
import uk.nominet.iot.manager.registry.RegistryDeviceHandler;
import uk.nominet.iot.manager.registry.RegistryDeviceManager;
import uk.nominet.iot.model.registry.RegistryDevice;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.Optional;
import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegistryDeviceManagerITSuite  {

    private RegistryDeviceManager deviceManager;
    private RegistryDataStreamManager streamManager;

    private static String deviceKey = RandomStringUtils.randomAlphabetic(20).toLowerCase();
    private static String deviceName = deviceKey;

    private static JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    private RegistryDeviceHandler registryItem;

    @Before
    public void setUp() throws Exception {
        if (RegistryAPIDriver.isInitialized()) RegistryAPIDriver.globalDispose();
        if (!RegistryAPIDriver.isInitialized()){
            RegistryDriverConfig registryConfig = new RegistryDriverConfig();
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_SCHEME, "http");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST,
                    DataAccessIntegrationIT.environment.getServiceHost("registry", 8081));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT,
                    DataAccessIntegrationIT.environment.getServicePort("registry", 8081));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_USERNAME, "datastore_agent");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD, "datastore_agent");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS, 3);
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }

        UserContext testUser = new UserContext("test");
        testUser.setPassword("test");
        deviceManager = RegistryDeviceManager.createManager(testUser);
        streamManager = RegistryDataStreamManager.createManager(testUser);

        registryItem = deviceManager.device(deviceKey);
    }

    @Test
    public void step01_createNewDevice() throws Exception {
        RegistryDeviceHandler deviceItem = deviceManager.create(Optional.of(deviceKey), Optional.of(deviceName), Optional.of("deviceYpe"));
        deviceItem.updateMetadata(new Gson().fromJson("{\"foo\": \"bar\", \"bar\" : {\"foo\" : 1}}", JsonObject.class));
        deviceItem.setUserPermission("test1", new String[]{Taxonomy.PERMISSION_UPLOAD, Taxonomy.PERMISSION_DISCOVER});
        deviceItem.setLocation(new Point(new SinglePosition( -1.2555313110351562, 51.74807630995209, 0.0)));

        RegistryDataStreamHandler streamItem = streamManager.create(Optional.of("test.device.stream1"), Optional.of("test.device.stream1"), Optional.of("streamTYpe"));
        streamItem.updateMetadata(new Gson().fromJson("{\"foo\": \"bar\", \"bar\" : {\"foo\" : 1}}", JsonObject.class));
        streamItem.setUserPermission("test1", new String[]{Taxonomy.PERMISSION_UPLOAD, Taxonomy.PERMISSION_DISCOVER});

        deviceItem.addStream("test.device.stream1", Optional.of("MyStream1"));

        deviceItem.index();
        RegistryDevice device = deviceItem.get();

        JsonObject deviceUser1 = DataAccessIntegrationIT.getElasticDocumentById(
                "thingzone_74657374",
                "device",
          null,
                deviceKey);

        JsonObject deviceUser2 = DataAccessIntegrationIT.getElasticDocumentById(
                "thingzone_7465737431",
                "device",
          null,
                deviceKey);

    }




}

