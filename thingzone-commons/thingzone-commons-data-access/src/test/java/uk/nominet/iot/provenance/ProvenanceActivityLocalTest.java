/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.provenance;

import com.google.common.collect.Lists;
import com.google.gson.JsonObject;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openprovenance.prov.interop.InteropFramework;
import org.openprovenance.prov.model.*;
import uk.nominet.iot.config.ProvsvrConstant;
import uk.nominet.iot.config.ProvsvrDriverConfig;
import uk.nominet.iot.driver.ProvsvrDriver;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.timeseries.Event;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

@Ignore
public class ProvenanceActivityLocalTest {
    public static ProvsvrDriverConfig provsvrConfig;
    public final ProvFactory pFactory = InteropFramework.newXMLProvFactory();
    public final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
    public Namespace ns = new Namespace();

    @Before
    public void setUp() throws Exception {

        if (!ProvsvrDriver.isInitialized()) {
            provsvrConfig = new ProvsvrDriverConfig();
            provsvrConfig.addProperty(ProvsvrConstant.PROVSVR_SERVER_SCHEME, "http");
            provsvrConfig.addProperty(ProvsvrConstant.PROVSVR_SERVER_HOST, "localhost");
            provsvrConfig.addProperty(ProvsvrConstant.PROVSVR_SERVER_PORT, 8083);
            ProvsvrDriver.setConnectionProperties(provsvrConfig);
            ProvsvrDriver.globalInitialise();

        }

        ns.register("app", "https://app.yourdomain.uk/");
        ns.register("prov", "http://www.w3.org/ns/prov#");
        ns.register("xsd", "http://www.w3.org/2001/XMLSchema#");
        ns.register("thingzone", "https://www.yourdomain.uk/thingzone#");

    }

    @Test
    public void setNamespaces() throws Exception {
        ProvsvrDriver.getAPIClient().prefix.create("app", "https://app.yourdomain.uk/");
        ProvsvrDriver.getAPIClient().prefix.create("app1", "https://app1.yourdomain.uk/");
        ProvsvrDriver.getAPIClient().prefix.create("xsd", "http://www.w3.org/2001/XMLSchema#");
        ProvsvrDriver.getAPIClient().prefix.create("prov", "http://www.w3.org/ns/prov#");
        ProvsvrDriver.getAPIClient().prefix.create("thingzone", "https://www.yourdomain.uk/thingzone#");

    }


    public QualifiedName qn(String prefix, String n) {
        return ns.qualifiedName(prefix, n, pFactory);
    }

    @Test
    public void createAgents() throws Exception {

        ProvsvrDriver.getAPIClient().prefix.create("app", "https://app.yourdomain.uk/");
        ProvsvrDriver.getAPIClient().prefix.create("xsd", "http://www.w3.org/2001/XMLSchema#");
        ProvsvrDriver.getAPIClient().prefix.create("prov", "http://www.w3.org/ns/prov#");
        ProvsvrDriver.getAPIClient().prefix.create("thingzone", "https://www.yourdomain.uk/thingzone#");

        Agent agentJoe = pFactory.newAgent(qn("app", "Joe"),"Joe");
        Agent agentTom = pFactory.newAgent(qn("app", "Tom"),"Tom");
        Agent agentGCS = pFactory.newAgent(qn("app", "GCS"),"GCS");
        Agent agentJavascriptExecutor = pFactory.newAgent(qn("app", "JavascriptExecutor"),"JavascriptExecutor");

        for (Agent agent : Lists.newArrayList(agentJoe, agentTom, agentGCS, agentJavascriptExecutor)) {
            Document provDocument = pFactory.newDocument();
            provDocument.setNamespace(ns);

            provDocument.getStatementOrBundle()
                        .addAll(Arrays.asList(new StatementOrBundle[]{agent}));
            provDocument.setNamespace(ns);

            InteropFramework interopFramework = new InteropFramework();
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            interopFramework.writeDocument(os, InteropFramework.ProvFormat.JSON, provDocument);

            JsonObject doc = jsonStringSerialiser
                                     .getDefaultSerialiser().fromJson(os.toString(), JsonObject.class);

            System.out.println("Doc:" + doc);

            ProvsvrDriver.getAPIClient().agent.create(agent.getId().getPrefix(), agent.getId().getLocalPart(), doc, "nomnom");
        }


    }
}
