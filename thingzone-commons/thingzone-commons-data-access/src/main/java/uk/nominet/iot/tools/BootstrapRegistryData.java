/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import com.github.filosganga.geogson.model.Geometry;
import com.github.filosganga.geogson.model.Point;
import com.github.filosganga.geogson.model.positions.SinglePosition;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import uk.nominet.iot.config.ElasticsearchConstant;
import uk.nominet.iot.config.ElasticsearchDriverConfig;
import uk.nominet.iot.config.RegistryConstant;
import uk.nominet.iot.config.RegistryDriverConfig;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.error.DataStorageException;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.manager.UserContext;
import uk.nominet.iot.manager.registry.*;
import uk.nominet.iot.registry.RegistryAPISession;
import uk.nominet.iot.utils.AnsiChar;

public class BootstrapRegistryData {
    private Integer port;
    private JsonObject job;
    private Map<String, String> userCredentials = new HashMap<>();
    private JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
    private File csvData;
    private int devCount = 0;

    /**
     * Accept consumer for each key matching a regex
     * @param keyset   set of keys
     * @param regex    regex over the keys
     * @param consumer consumer
     */
    private void forEachKeyMatch(Set<String> keyset, String regex, Consumer<String> consumer) {
        for (String key : keyset) {
            if (!key.matches(regex))
                continue;
            consumer.accept(key);
        }
    }

    /**
     * Create registry users
     */
    public void createRegistryUsers() throws DataStorageException {
        if (!job.has("registerUsers"))
            return;

        // Init the registry
        if (RegistryAPIDriver.isInitialized())
            RegistryAPIDriver.globalDispose();
        initRegistryDriver(job.getAsJsonObject("registry").get("adminUsername").getAsString());

        System.out.println("--------------------------------------------------------------------------------");
        System.out.println("                         USER CREATION");
        System.out.println("--------------------------------------------------------------------------------");

        RegistryUserManager userManager = RegistryUserManager.createManager();

        JsonObject registerUsers = job.getAsJsonObject("registerUsers");
        for (Map.Entry<String, JsonElement> entry : registerUsers.entrySet()) {

            String user = entry.getKey();
            System.out.print("User: " + AnsiChar.ANSI_BLUE + user + AnsiChar.ANSI_RESET);

            JsonObject userObj = registerUsers.getAsJsonObject(user);
            userCredentials.put(user, userObj.get("password").getAsString());
            RegistryUserHandler userItem = null;

            try {
                userItem = userManager.create(user, userObj.get("password").getAsString(),
                                              userObj.get("privileges").getAsString().split(","));
            } catch (Exception e) {
                System.out.print(AnsiChar.ANSI_PURPLE + " (user exists) " + AnsiChar.ANSI_RESET);
            } finally {
                try {
                    if (userItem == null)
                        userItem = userManager.user(user);
                    userItem.setPrivileges(userObj.get("privileges").getAsString().split(","));
                    System.out.print(" updated preferences");
                    System.out.println(AnsiChar.ANSI_GREEN + " OK" + AnsiChar.ANSI_RESET);
                } catch (Exception e1) {
                    System.out.println(AnsiChar.ANSI_RED + " FAILED " + e1.getMessage() + AnsiChar.ANSI_RESET);
                }
            }
        }
    }


    /**
     * Create registry groups
     */
    public void createRegistryGroups() throws DataStorageException {
        if (!job.has("registerGroups"))
            return;

        // Init the registry
        if (RegistryAPIDriver.isInitialized())
            RegistryAPIDriver.globalDispose();
        initRegistryDriver("app_admin");

        System.out.println("--------------------------------------------------------------------------------");
        System.out.println("                         GROUP CREATION");
        System.out.println("--------------------------------------------------------------------------------");

        RegistryGroupManager groupManager = RegistryGroupManager.createManager();

        JsonObject registerGroups = job.getAsJsonObject("registerGroups");
        for (Map.Entry<String, JsonElement> entry : registerGroups.entrySet()) {
            String group = entry.getKey();

            System.out.print("Group: " + AnsiChar.ANSI_BLUE + group + AnsiChar.ANSI_RESET);

            Gson gson = new Gson();
            String[] users = gson.fromJson(entry.getValue(), String[].class);

            RegistryGroupHandler groupHandler = null;
            try {
                groupHandler = groupManager.create(group);
            } catch (Exception e) {
                System.out.print(AnsiChar.ANSI_PURPLE + " (" + e.getMessage() + ") " + AnsiChar.ANSI_RESET);
            } finally {
                if (groupHandler == null) {
                    try {
                        groupHandler = groupManager.group(group);
                    } catch (Exception e) {
                        System.out.println(AnsiChar.ANSI_RED + " FAILED " + e.getMessage() + AnsiChar.ANSI_RESET);
                    }
                }

                System.out.print(" adding users " + Arrays.toString(users));

                try {
                    groupHandler.addUsers(users);
                    System.out.println(AnsiChar.ANSI_GREEN + " OK" + AnsiChar.ANSI_RESET);
                } catch (Exception e) {
                    System.out.println(AnsiChar.ANSI_RED + " FAILED " + e.getMessage() + AnsiChar.ANSI_RESET);
                }


            }
        }

    }

    /**
     * Create functions
     */
    public void createFunctions() throws DataStorageException {
        if (!job.has("functions"))
            return;

        // Init the registry
        if (RegistryAPIDriver.isInitialized())
            RegistryAPIDriver.globalDispose();
        initRegistryDriver(job.getAsJsonObject("registry").get("serviceUsername").getAsString());

        System.out.println("--------------------------------------------------------------------------------");
        System.out.println("                          CREATE FUNCTIONS");
        System.out.println("--------------------------------------------------------------------------------");

        JsonObject functions = job.getAsJsonObject("functions");
        for (Map.Entry<String, JsonElement> functionEntry : functions.entrySet()) {
            System.out.print("Function: " + AnsiChar.ANSI_BLUE + functionEntry.getKey() + AnsiChar.ANSI_RESET);
            JsonObject function = functionEntry.getValue().getAsJsonObject();
            String owner = function.get("owner").getAsString();
            UserContext userContext = new UserContext(owner);
            userContext.setPassword(userCredentials.get(owner));

            RegistryFunctionManager functionManager = RegistryFunctionManager.createManager(userContext);

            String functionContent = function.get("functionContent").getAsString();
            if (functionContent.startsWith("file://")) {
                File functionFile = new File(functionContent.replace("file://", ""));
                try {
                    functionContent = FileUtils.readFileToString(functionFile, Charset.defaultCharset());
                } catch (IOException e) {
                    System.out.println(AnsiChar.ANSI_RED + " FAILED " + e.getMessage() + AnsiChar.ANSI_RESET);
                }
            }

            RegistryFunctionHandler registryFunctionHandler = null;
            try {
                registryFunctionHandler = functionManager.create(functionEntry.getKey(),
                                                                 function.get("functionType").getAsString(),
                                                                 functionContent,
                                                                 function.get("isPublic").getAsBoolean());
                System.out.println(AnsiChar.ANSI_GREEN + " OK" + AnsiChar.ANSI_RESET);
            } catch (Exception e) {
                System.out.print(AnsiChar.ANSI_PURPLE + " (function exists) " + AnsiChar.ANSI_RESET);
                System.out.println(e.getMessage());
            } finally {
                if (registryFunctionHandler == null) {
                    try {
                        registryFunctionHandler = functionManager.function(functionEntry.getKey(),
                                                                           function.get("isPublic").getAsBoolean());
                        registryFunctionHandler.updateFunction(function.get("functionType").getAsString(),
                                                               functionContent);
                        System.out.println(AnsiChar.ANSI_GREEN + " UPDATED" + AnsiChar.ANSI_RESET);
                    } catch (Exception e1) {
                        System.out.println(AnsiChar.ANSI_RED + " FAILED " + e1.getMessage() + AnsiChar.ANSI_RESET);
                    }
                }
            }
        }
    }


    /**
     * Create entities
     */
    public void createEntities() throws DataStorageException, IOException {
        if (!job.has("registerEntities"))
            return;


        // Init the registry
        if (RegistryAPIDriver.isInitialized())
            RegistryAPIDriver.globalDispose();
        initRegistryDriver(job.getAsJsonObject("registry").get("serviceUsername").getAsString());

        System.out.println("--------------------------------------------------------------------------------");
        System.out.println("                    ENTITY CREATION");
        System.out.println("--------------------------------------------------------------------------------");


        JsonArray entities = job.getAsJsonArray("registerEntities");

        for (JsonElement entity : entities) {
            String name = entity.getAsJsonObject().get("name").getAsString();
            String type = entity.getAsJsonObject().get("type").getAsString();
            String key = entity.getAsJsonObject().get("key").getAsString();
            String owner = entity.getAsJsonObject().get("owner").getAsString();
            String group = entity.getAsJsonObject().has("group") ? entity.getAsJsonObject().get("group").getAsString() : null;

            System.out.print("Entity: " + AnsiChar.ANSI_BLUE + String.format("%s %s %s", key, name, type) + AnsiChar.ANSI_RESET);


            UserContext userContext = new UserContext(owner);
            userContext.setPassword(userCredentials.get(owner));

            RegistryEntityManager entityManager = RegistryEntityManager.createManager(userContext);

            try {
                entityManager.create(Optional.of(key), Optional.of(name), Optional.of(type));
                System.out.print(AnsiChar.ANSI_GREEN + " OK" + AnsiChar.ANSI_RESET);
            } catch (Exception e) {
                System.out.print(AnsiChar.ANSI_RED + " FAILED" + AnsiChar.ANSI_RESET + e);
            } finally {
                if (group != null) {
                    RegistryAPISession session = RegistryAPIDriver.getUserSession(owner, userCredentials.get(owner));
                    try {
                        session.permission.set(key, Optional.empty(), Optional.of(group), new String[]{"DISCOVER","MODIFY"});
                        System.out.print(AnsiChar.ANSI_GREEN + " OK PERMISSION" + AnsiChar.ANSI_RESET);
                    } catch (Exception e) {
                        System.out.println(AnsiChar.ANSI_RED + " FAILED PERMISSIONS " + key + "  " + group + AnsiChar.ANSI_RESET + e);
                    }
                }
            }
            System.out.println("");
        }
    }

    /**
     * Create entities
     */
    public void uploadResources() throws DataStorageException, IOException {
        if (!job.has("uploadResources"))
            return;


        // Init the registry
        if (RegistryAPIDriver.isInitialized())
            RegistryAPIDriver.globalDispose();
        initRegistryDriver(job.getAsJsonObject("registry").get("serviceUsername").getAsString());

        System.out.println("--------------------------------------------------------------------------------");
        System.out.println("                   RESOURCES UPLOAD");
        System.out.println("--------------------------------------------------------------------------------");


        JsonArray resources = job.getAsJsonArray("uploadResources");

        for (JsonElement resource : resources) {
            String name = resource.getAsJsonObject().get("name").getAsString();
            String contentType = resource.getAsJsonObject().get("content_type").getAsString();
            String key = resource.getAsJsonObject().get("key").getAsString();
            String user = resource.getAsJsonObject().get("user").getAsString();
            String type = resource.getAsJsonObject().get("type").getAsString();

            JsonElement contentElement = resource.getAsJsonObject().get("content");

            System.out.print("Resource: " + AnsiChar.ANSI_BLUE + String.format("%s %s %s %s ", key, name, contentType, contentElement) + AnsiChar.ANSI_RESET);


            RegistryAPISession session = RegistryAPIDriver.getUserSession(user, userCredentials.get(user));

            try {
                byte[] content = null;

                if (contentElement.isJsonPrimitive() && contentElement.getAsString().startsWith("file://")) {
                    File resourceFile = new File(contentElement.getAsString().replace("file://", ""));
                    try {
                        content = FileUtils.readFileToByteArray(resourceFile);
                    } catch (IOException e) {
                        System.out.println(AnsiChar.ANSI_RED + " FAILED " + e.getMessage() + AnsiChar.ANSI_RESET);
                    }
                } else if (contentElement.isJsonObject()){
                    content = contentElement.getAsJsonObject().toString().getBytes();
                }

                session.resource.upsert(name, contentType, content, Optional.of(key), Optional.empty(), Optional.of(type));
                System.out.println(AnsiChar.ANSI_GREEN + " OK" + AnsiChar.ANSI_RESET);
            } catch (Exception e) {
                System.out.println(AnsiChar.ANSI_RED + " FAILED" + AnsiChar.ANSI_RESET + e);
            }
        }
    }


    /**
     * Create devices
     */
    public void createDevices() throws DataStorageException, IOException {
        if (!job.has("registerDevices"))
            return;

        // Init the registry
        if (RegistryAPIDriver.isInitialized())
            RegistryAPIDriver.globalDispose();
        initRegistryDriver(job.getAsJsonObject("registry").get("serviceUsername").getAsString());

        System.out.println("--------------------------------------------------------------------------------");
        System.out.println("                    DEVICE AND STREAM CREATION");
        System.out.println("--------------------------------------------------------------------------------");

        Reader deviceDataReader = new FileReader(csvData);
        Iterable<CSVRecord> deviceDataCSV = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(deviceDataReader);

        for (CSVRecord record : deviceDataCSV) {
            Map<String, String> device = record.toMap();

            // Get List of users from device
            HashMap<String, String> permittedUsers = new HashMap<>();

            forEachKeyMatch(device.keySet(), "users\\[[0-9]*\\].name", key -> {
                Pattern pattern = Pattern.compile("\\[[0-9]*]");
                Matcher matcher = pattern.matcher(key);
                if (matcher.find()) {
                    String permissionKey = key.replaceFirst("name", "permissions");
                    permittedUsers.put(device.get(key), device.get(permissionKey));
                }
            });

            // Get lisy of groups for a device
            HashMap<String, String> permittedGroups = new HashMap<>();

            forEachKeyMatch(device.keySet(), "groups\\[[0-9]*\\].name", key -> {
                Pattern pattern = Pattern.compile("\\[[0-9]*]");
                Matcher matcher = pattern.matcher(key);
                if (matcher.find()) {
                    String permissionKey = key.replaceFirst("name", "permissions");
                    permittedGroups.put(device.get(key), device.get(permissionKey));
                }
            });

            UserContext userContext = new UserContext(device.get("owner"));
            userContext.setPassword(userCredentials.get(device.get("owner")));

            System.out.print((devCount++) + " Device: " + AnsiChar.ANSI_BLUE + device.get("key") + AnsiChar.ANSI_RESET);
            RegistryDeviceManager deviceManager = RegistryDeviceManager.createManager(userContext);

            Optional<String> deviceKey = Optional.of(device.get("key"));
            Optional<String> name = (device.get("name") != null) ? Optional.of(device.get("name")) : Optional.empty();
            Optional<String> type = (device.get("type") != null) ? Optional.of(device.get("type")) : Optional.empty();

            RegistryDeviceHandler deviceItem = null;
            try {
                deviceItem = deviceManager.create(deviceKey, name, type);
            } catch (Exception e) {
                System.out.print(AnsiChar.ANSI_PURPLE + " (could not create device) " + AnsiChar.ANSI_RESET);
                System.out.print(e.getMessage());
            } finally {
                // Update Metadata
                if (deviceItem == null && deviceKey.isPresent()) {
                    try {
                        deviceItem = deviceManager.device(deviceKey.get());
                    } catch (Exception e) {
                        System.out.println(AnsiChar.ANSI_RED + " FAILED " + e.getMessage() + AnsiChar.ANSI_RESET);
                        e.printStackTrace();
                    }
                }
                JsonObject metadata = new JsonObject();
                metadata.addProperty("name", device.get("name"));

                forEachKeyMatch(device.keySet(), "metadata\\..*", key -> {
                    String property = key.split("\\.")[1];
                    metadata.addProperty(property, device.get(key));
                });

                if (device.containsKey("lat") && device.containsKey("long")) {
                    Geometry<?> geometry = new Point(new SinglePosition(Double.parseDouble(device.get("long")),
                                                                        Double.parseDouble(device.get("lat")), 0.0));
                    try {
                        assert deviceItem != null;
                        deviceItem.setLocation(geometry);
                    } catch (Exception e) {
                        System.out.print(AnsiChar.ANSI_RED + " FAILED" + AnsiChar.ANSI_RESET + e);
                        e.printStackTrace();
                    }
                }

                try {
                    if (deviceItem == null)
                        deviceItem = deviceManager.device(device.get("key"));
                    deviceItem.updateMetadata(metadata);
                    deviceItem.updateName(name.get());
                    deviceItem.updateType(type.get());

                    for (String key : permittedUsers.keySet()) {
                        deviceItem.setUserPermission(key, permittedUsers.get(key).split(","));
                    }

                    for (String key : permittedGroups.keySet()) {
                        deviceItem.setGroupPermission(key, permittedGroups.get(key).split(","));
                    }

                    System.out.print(" updated metadata and permissions ");
                    System.out.print(AnsiChar.ANSI_GREEN + " OK" + AnsiChar.ANSI_RESET);
                } catch (Exception e) {
                    System.out.print(AnsiChar.ANSI_RED + " FAILED" + AnsiChar.ANSI_RESET + e);
                    e.printStackTrace();
                }

                try {
                    assert deviceItem != null;
                    deviceItem.index();
                    System.out.println(AnsiChar.ANSI_GREEN + " INDEX" + AnsiChar.ANSI_RESET);
                } catch (Exception e) {
                    System.out.println(AnsiChar.ANSI_RED + " INDEX FAILED" + AnsiChar.ANSI_RESET + e);
                }

                createStreams(userContext, deviceItem.getKey(), permittedUsers, permittedGroups, device);
                createTransforms(userContext, deviceItem.getKey(), permittedUsers, permittedGroups, device);
            }
        }
    }

    /**
     * Create transfrorms
     * @param userContext    user context for the transform e.g. owner
     * @param deviceKey      key ot the device associated with the transform
     * @param permittedUsers other user permissions on the transform
     * @param device         device json
     */
    public void createTransforms(UserContext userContext,
                                 String deviceKey,
                                 HashMap<String, String> permittedUsers,
                                 HashMap<String, String> permittedGroups,
                                 Map<String, String> device) throws DataStorageException {
        JsonObject registerDevicesObj = job.getAsJsonObject("registerDevices");
        if (!registerDevicesObj.has("transformDefs"))
            return;

        RegistryTransformManager transformManager = RegistryTransformManager.createManager(userContext);

        for (JsonElement transformDef : registerDevicesObj.getAsJsonArray("transformDefs")) {
            JsonObject transformDefObj = transformDef.getAsJsonObject();
            String transformKey = String.format("%s.%s", deviceKey, transformDefObj.get("key_suffix").getAsString());
            System.out.print(" - Transform: " + AnsiChar.ANSI_BLUE + transformKey + AnsiChar.ANSI_RESET);

            Optional<String> runSchedule = transformDefObj.has("runSchedule") ?
                                   Optional.of(transformDefObj.get("runSchedule").getAsString()) :
                                   Optional.empty();

            RegistryTransformHandler transformItem = null;
            try {



                transformItem = transformManager.create(Optional.of(transformKey),
                                                        Optional.of(transformDefObj.get("name").getAsString()),
                                                        Optional.of(transformDefObj.get("type").getAsString()),
                                                        runSchedule,
                                                        transformDefObj.get("function").getAsString(),
                                                        transformDefObj.get("function").getAsBoolean(),
                                                        transformDefObj.getAsJsonObject("parameterValues"));
            } catch (Exception e) {
                System.out.print(AnsiChar.ANSI_PURPLE + " (could not create transform) " + AnsiChar.ANSI_RESET);
            } finally {
                try {
                    if (transformItem == null)
                        transformItem = transformManager.transform(transformKey);
                    transformItem.updateFunction(transformDefObj.get("function").getAsString(),
                                                 transformDefObj.get("function").getAsBoolean());

                    if (runSchedule.isPresent()) {
                        System.out.print(AnsiChar.ANSI_CYAN + " NEW RUN SCHEDULE: " + runSchedule.get()+ " SS" + AnsiChar.ANSI_RESET);
                        transformItem.updateRunSchedule(runSchedule.get());
                    }

                    transformItem.updateParameters(transformDefObj.getAsJsonObject("parameterValues"));
                    transformItem.updateMetadata(transformDefObj.getAsJsonObject("metadata"));
                    transformItem.addToDevice(deviceKey, Optional.empty());


                    if (transformDefObj.has("outputStreams")) {
                        for (JsonElement sourceElement : transformDefObj.getAsJsonArray("outputStreams")) {
                            JsonObject output = sourceElement.getAsJsonObject();
                            String streamKey = String.format("%s.%s", deviceKey, output.get("stream").getAsString());
                            transformItem.updateOutputStream(streamKey, output.get("alias").getAsString());
                        }
                    }

                    // Add sources
                    for (JsonElement sourceElement : transformDefObj.getAsJsonArray("sources")) {

                        JsonObject source = sourceElement.getAsJsonObject();

                        Optional aggType = source.has("aggregationType") ?
                                           Optional.of(source.get("aggregationType").getAsString()) :
                                           Optional.empty();
                        Optional aggSpec = source.has("aggregationSpec") ?
                                           Optional.of(source.get("aggregationSpec").getAsString()) :
                                           Optional.empty();

                        String streamKey = String.format("%s.%s", deviceKey, source.get("source").getAsString());
                        transformItem.updateSource(streamKey,
                                                   source.get("alias").getAsString(),
                                                   source.get("trigger").getAsBoolean(),
                                                   aggType,
                                                   aggSpec);
                    }

                    for (String key : permittedUsers.keySet()) {
                        transformItem.setUserPermission(key, permittedUsers.get(key).split(","));
                    }

                    for (String key : permittedGroups.keySet()) {
                        transformItem.setGroupPermission(key, permittedGroups.get(key).split(","));
                    }

                    System.out.print(AnsiChar.ANSI_CYAN + " ADDED" + AnsiChar.ANSI_RESET);
                    System.out.print(AnsiChar.ANSI_GREEN + " OK" + AnsiChar.ANSI_RESET);
                } catch (Exception e) {
                    System.out.print(AnsiChar.ANSI_RED + " FAILED" + e.getMessage() + AnsiChar.ANSI_RESET);
                    // System.out.println(e.getMessage());
                }

                try {
                    assert transformItem != null;
                    transformItem.index();
                    System.out.println(AnsiChar.ANSI_GREEN + " INDEX" + AnsiChar.ANSI_RESET);
                } catch (Exception e) {
                    System.out.println(AnsiChar.ANSI_RED + " INDEX FAILED" + AnsiChar.ANSI_RESET + e);
                }
            }
        }
    }

    /**
     * Create data streams
     * @param userContext    user context for the stream e.g. owner
     * @param deviceKey      key ot the device associated with the stream
     * @param permittedUsers other user permissions on the stream
     * @param device         device json
     */
    private void createStreams(UserContext userContext,
                               String deviceKey,
                               HashMap<String, String> permittedUsers,
                               HashMap<String, String> permittedGroups,
                               Map<String, String> device) throws DataStorageException {
        JsonObject registerDevicesObj = job.getAsJsonObject("registerDevices");

        if (!registerDevicesObj.has("streamDefs"))
            return;

        RegistryDataStreamManager streamManager = RegistryDataStreamManager.createManager(userContext);

        for (JsonElement streamDef : registerDevicesObj.getAsJsonArray("streamDefs")) {
            JsonObject streamDefObj = streamDef.getAsJsonObject();
            Optional<String> streamKey = Optional.of(String.format("%s.%s", deviceKey,
                                                                   streamDefObj.get("key_suffix").getAsString()));
            Optional<String> name = (streamDefObj.has("name")) ? Optional.of(streamDefObj.get("name").getAsString())
                                                               : Optional.empty();
            Optional<String> type = (streamDefObj.has("type")) ? Optional.of(streamDefObj.get("type").getAsString())
                                                               : Optional.empty();
            System.out.print(" - Stream: " + AnsiChar.ANSI_BLUE + streamKey.get() + AnsiChar.ANSI_RESET);
            RegistryDataStreamHandler dataStreamItem = null;

            try {
                dataStreamItem = streamManager.create(streamKey, name, type);
            } catch (Exception e) {
                System.out.print(AnsiChar.ANSI_PURPLE + " (could not create stream) " + AnsiChar.ANSI_RESET);
                // System.out.print(e.getMessage());
            } finally {
                try {
                    if (dataStreamItem == null)
                        dataStreamItem = streamManager.stream(streamKey.get());

                    JsonObject metadata = streamDefObj.getAsJsonObject("metadata");
                    String streamSuffix = streamDefObj.get("key_suffix").getAsString();

                    forEachKeyMatch(device.keySet(), "\\$" + streamSuffix + "\\.metadata\\..*", key -> {
                        String property = key.split("\\.")[2];
                        metadata.addProperty(property, device.get(key));
                    });

                    dataStreamItem.updateMetadataAndType(metadata, type.get());

                    System.out.print(" updated METADATA ");


                    for (String key : permittedUsers.keySet()) {
                        dataStreamItem.setUserPermission(key, permittedUsers.get(key).split(","));
                    }

                    for (String key : permittedGroups.keySet()) {
                        dataStreamItem.setGroupPermission(key, permittedGroups.get(key).split(","));
                    }

                    System.out.print("PERMISSIONS ");

                    // Create push subscriptions
                    if (streamDefObj.has("push")) {
                        JsonArray pushSubs = streamDefObj.getAsJsonArray("push");
                        for (JsonElement push : pushSubs) {
                            JsonObject pushObj = push.getAsJsonObject();
                            String method =  pushObj.get("method").getAsString();
                            String uri = pushObj.get("uri").getAsString();
                            Map<String,String> uriParams = jsonStringSerialiser
                                                                   .getDefaultSerialiser()
                                                                   .fromJson(pushObj.get("uriParams"), new TypeToken<Map<String, String>>() {}.getType());
                            Map<String,String> headers = jsonStringSerialiser
                                                                 .getDefaultSerialiser()
                                                                 .fromJson(pushObj.get("headers"), new TypeToken<Map<String, String>>() {}.getType());
                            String retrySequence = pushObj.get("retrySequence").getAsString();

                            dataStreamItem.addPushSubscriber(method, uri, uriParams, headers, retrySequence);
                        }
                        System.out.print("PUSH ");

                    }


                } catch (Exception e) {
                    System.out.println(AnsiChar.ANSI_RED + " FAILED" + e.getMessage() + AnsiChar.ANSI_RESET);
                }

                try {
                    assert dataStreamItem != null;
                    dataStreamItem.addToDevice(deviceKey, Optional.empty());
                    System.out.print(AnsiChar.ANSI_CYAN + " ADDED" + AnsiChar.ANSI_RESET);
                    System.out.print(AnsiChar.ANSI_GREEN + " OK" + AnsiChar.ANSI_RESET);
                } catch (Exception ignored) {
                }

                try {
                    dataStreamItem.index();
                    System.out.println(AnsiChar.ANSI_GREEN + " INDEX" + AnsiChar.ANSI_RESET);
                } catch (Exception e) {
                    System.out.println(AnsiChar.ANSI_RED + " INDEX FAILED" + AnsiChar.ANSI_RESET + e);
                }
            }
        }
    }

    /**
     * Initialise the registry driver for a specific user
     * @param username username
     */
    private void initRegistryDriver(String username) {
        JsonObject registryConfigJson = job.getAsJsonObject("registry");
        if (!RegistryAPIDriver.isInitialized()) {
            RegistryDriverConfig registryConfig = new RegistryDriverConfig();
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_SCHEME,
                                       registryConfigJson.get("scheme").getAsString());
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST,
                                       registryConfigJson.get("host").getAsString());
            if (port == null) {
                registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT,
                                           registryConfigJson.get("port").getAsInt());
            } else {
                registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT, port);
            }
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_USERNAME, username);
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD, userCredentials.get(username));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS, 3);
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }
    }

    /**
     * Initialise bootstrap
     * @param job     job description Json
     * @param csvData file containing csv data
     * @param port    registry service port number
     */
    public BootstrapRegistryData(JsonObject job, File csvData, Integer port) {
        this.job = job;
        this.csvData = csvData;
        this.port = port;
        // JsonObject registryConfigJson = job.getAsJsonObject("registry");
        JsonObject elasticConfigJson = job.getAsJsonObject("elasticsearch");

        job.getAsJsonObject("userCredentials").entrySet().forEach(entry -> {
            userCredentials.put(entry.getKey(), entry.getValue().getAsString());
        });

        if (!ElasticsearchDriver.isInitialized()) {
            ElasticsearchDriverConfig elasticsearchDriverConfig = new ElasticsearchDriverConfig();
            elasticsearchDriverConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_SCHEME,
                                                  elasticConfigJson.get("scheme").getAsString());
            elasticsearchDriverConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_HOST,
                                                  elasticConfigJson.get("host").getAsString());
            elasticsearchDriverConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_PORT,
                                                  elasticConfigJson.get("port").getAsInt());
            ElasticsearchDriver.setConnectionProperties(elasticsearchDriverConfig);
            ElasticsearchDriver.globalInitialise();
        }
    }

    public static void main(String args[]) throws Exception {
        if (args.length < 1) {
            System.out.println("Usage: BootstrapRegistryData config.json data.csv [registry_port]");
            System.exit(1);
        }

        InputStream jobInputStream = new FileInputStream(args[0]);
        String jsonTxt = IOUtils.toString(jobInputStream, "UTF-8");
        JsonObject job = new Gson().fromJson(jsonTxt, JsonObject.class);
        // new JSONObject(jsonTxt);



        File csvData = (args.length == 2) ? new File(args[1]) : null;

        Integer port = (args.length == 3) ? Integer.parseInt(args[2]) : null;

        BootstrapRegistryData bootstrapRegistryData = new BootstrapRegistryData(job, csvData, port);
        bootstrapRegistryData.createRegistryUsers();
        bootstrapRegistryData.createRegistryGroups();
        bootstrapRegistryData.createFunctions();
        bootstrapRegistryData.createDevices();
        bootstrapRegistryData.createEntities();
        bootstrapRegistryData.uploadResources();
    }
}
