/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.manager;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import uk.nominet.iot.driver.RegistryAPIDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.nominet.iot.model.registry.RegistryKey;

import java.lang.invoke.MethodHandles;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


public abstract class ThingzoneStorageManager {
    private final static Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    protected Optional<UserContext> userContext;


    public ThingzoneStorageManager(Optional<UserContext> userContext) {
        this.userContext = userContext;
    }


    /**
     * Cache for entity permissions requests
     */
    private final static LoadingCache<String, RegistryKey> entityPermissionCache =
            CacheBuilder.newBuilder()
                        .maximumSize(100)
                        .expireAfterWrite(2, TimeUnit.MINUTES)
                        .build(new CacheLoader<String, RegistryKey>() {
                            @Override
                            public RegistryKey load(String key) throws Exception {
                                return RegistryAPIDriver
                                               .getSession()
                                               .internal
                                               .permissionsForKey(key);
                            }
                        });

    /**
     * Determine if the user context (user or group) has a specific set of permissions on a registry entity
     * @param key key of the registry entity
     * @param permissions key of the registry entity
     * @return true/false
     */
    public boolean hasAllPermissions(String key, String[] permissions) throws Exception {
        if (!userContext.isPresent())
            return true;
        return hasAllPermissions(userContext.get().getUsername(), key, permissions);
    }


    /**
     * Determine if a subject (user or group) has a specific set of permissions on a registry key
     * @param subject permission subject user or group name
     * @param key key of the registry entity
     * @param permissions key of the registry entity
     * @return true/false
     */
    public boolean hasAllPermissions(String subject, String key, String[] permissions) throws Exception {

        if (userContext.isPresent()) {
            RegistryKey registryKey = entityPermissionCache.get(key);
            return hasAllPermissions(subject, registryKey, permissions);
        }

        // This is a privileged user operation
        return true;
    }


    /**
     * Determine if a subject (user or group) contains a specific set of permissions on a registry entity
     * @param subject     user or group
     * @param entity      the registry entity to check
     * @param permissions set of permissions
     * @return true/false
     */
    public boolean hasAllPermissions(String subject, RegistryKey entity, String[] permissions) {
        Map<String, List<String>> allPermissions = new HashMap<>();
        allPermissions.putAll(entity.getUserPermissions());
        allPermissions.putAll(entity.getGroupPermissions());

        if (allPermissions.containsKey(subject)) {
            return Arrays.stream(permissions).parallel().allMatch(allPermissions.get(subject)::contains);
        }

        return false;
    }


    /**
     * Determine if the user context (user or group) has a specific set of permissions on a registry entity
     * @param key key of the registry entity
     * @param permissions set of permissions
     * @return true/false
     */
    public boolean hasAnyOfPermissions(String key, String[] permissions) throws Exception {
        if (!userContext.isPresent())
            return true;
        return hasAnyOfPermissions(userContext.get().getUsername(), key, permissions);
    }


    /**
     * Determine if a subject (user or group) has a specific set of permissions on a registry key
     * @param subject     user or group
     * @param key      key of the registry entity
     * @param permissions set of permissions
     * @return true/false
     */
    public boolean hasAnyOfPermissions(String subject, String key, String[] permissions) throws Exception {

        if (userContext.isPresent()) {
            RegistryKey registryKey = entityPermissionCache.get(key);
            return hasAnyOfPermissions(subject, registryKey, permissions);
        }

        // This is a privileged user operation
        return true;
    }


    /**
     * Determine if a subject (user or group) contains a specific set of permissions on a registry entity
     * @param subject     user or group
     * @param entity      the registry entity to check
     * @param permissions set of permissions
     * @return true/false
     */
    protected boolean hasAnyOfPermissions(String subject, RegistryKey entity, String[] permissions) {
        Map<String, List<String>> allPermissions = new HashMap<>();
        allPermissions.putAll(entity.getUserPermissions());
        allPermissions.putAll(entity.getGroupPermissions());

        if (allPermissions.containsKey(subject)) {
            return Arrays.stream(permissions).parallel().anyMatch(allPermissions.get(subject)::contains);
        }

        return false;
    }


    /**
     * Returns the list of Permitted subjects (users and groups) for a registry entity that matches a set of permissions
     * @param key         the key of the entity
     * @param permissions a set of permissions
     * @return list of subjects with permissions
     */
    public List<String> getSubjectsWithAllPermissions(String key, String[] permissions) throws Exception {
        RegistryKey registryKey = entityPermissionCache.get(key);
        return getSubjectsWithAllPermissions(registryKey, permissions);
    }

    /**
     * Get subjects from a registry entity that matches a set of permissions
     * @param entity      registry entity
     * @param permissions set of permissions
     * @return list of subjects with permissions
     */
    protected List<String> getSubjectsWithAllPermissions(RegistryKey entity, String[] permissions) {
        Map<String, List<String>> allPermissions = new HashMap<>();
        allPermissions.putAll(entity.getUserPermissions());
        allPermissions.putAll(entity.getGroupPermissions());

        return allPermissions.entrySet()
                             .stream()
                             .filter(entry -> Arrays.stream(permissions).parallel().allMatch(entry.getValue()::contains))
                             .map(Map.Entry::getKey)
                             .collect(Collectors.toList());
    }

    /**
     * Returns the list of Permitted subjects (users and groups) for a registry entity that matches a set of permissions
     * @param key         the key of the entity
     * @param permissions a set of permissions
     * @return list of subjects with permissions
     */
    public List<String> getSubjectsWithAnyOfPermissions(String key, String[] permissions) throws Exception {
        RegistryKey registryKey = entityPermissionCache.get(key);
        return getSubjectsWithAnyOfPermissions(registryKey, permissions);
    }

    /**
     * Get subjects from a registry entity that matches a set of permissions
     * @param entity      registry entity
     * @param permissions set of permissions
     * @return list of subjects with permissions
     */
    protected List<String> getSubjectsWithAnyOfPermissions(RegistryKey entity, String[] permissions) {
        Map<String, List<String>> allPermissions = new HashMap<>();
        allPermissions.putAll(entity.getUserPermissions());
        allPermissions.putAll(entity.getGroupPermissions());

        return allPermissions.entrySet()
                             .stream()
                             .filter(entry -> Arrays.stream(permissions).parallel().anyMatch(entry.getValue()::contains))
                             .map(Map.Entry::getKey)
                             .collect(Collectors.toList());
    }

    /**
     * Get the user context
     * @return user context
     */
    public Optional<UserContext> getUserContext() {
        return userContext;
    }
}
