/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.manager;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.Result;
import com.datastax.driver.mapping.annotations.Table;
import com.google.common.primitives.Ints;
import com.google.gson.JsonObject;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import uk.nominet.iot.annotations.TzTimeseries;
import uk.nominet.iot.driver.CassandraDriver;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.elastic.ThingzoneTimeseriesIndex;
import uk.nominet.iot.elastic.ThingzoneTimeseriesIndexDelete;
import uk.nominet.iot.error.DataCachingException;
import uk.nominet.iot.error.DataStorageException;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.RelatedRelation;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;
import uk.nominet.iot.model.core.Classification;
import uk.nominet.iot.model.registry.RegistryDataStream;
import uk.nominet.iot.model.registry.RegistryDevice;
import uk.nominet.iot.model.registry.RegistryDeviceReference;
import uk.nominet.iot.model.timeseries.Event;
import uk.nominet.iot.model.timeseries.Metric;

import java.io.IOException;
import java.sql.Date;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


import static com.datastax.driver.mapping.Mapper.Option.saveNullFields;

public class ThingzoneTimeseriesHandler<T extends AbstractTimeseriesPoint> {
    private final Class<T> klass;
    private final TzTimeseries tingzoneTimeseriesAnnotation;
    private final Table cassandraTableAnnotations;
    private final String streamKey;
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    private JestClient elasticsearchClient;
    private Mapper<T> cassandraMapper;
    private List<String> subjects;
    private ThingzoneStorageManager manager;


    /**
     * Initialise a timeseries handler
     * @param streamKey key of the data stream
     * @param klass class of the timeseries data points
     * @param manager reference to storage manager
     */
    public ThingzoneTimeseriesHandler(String streamKey, Class<T> klass, ThingzoneStorageManager manager)
            throws Exception {
        Class<T> timeseriesTypeClass;


        if (klass == null) {
            RegistryDataStream stream = (RegistryDataStream) RegistryAPIDriver.getSession()
                                                                     .internal
                                                                     .identify(streamKey, true, true);

            timeseriesTypeClass = (Class<T>) Class.forName(stream.getType());
        } else {
            timeseriesTypeClass = klass;
        }



        if (!timeseriesTypeClass.isAnnotationPresent(TzTimeseries.class))
            throw new DataStorageException("@TzTimeseries annotation not present in class: " + timeseriesTypeClass);

        this.tingzoneTimeseriesAnnotation = timeseriesTypeClass.getAnnotation(TzTimeseries.class);

        if (this.tingzoneTimeseriesAnnotation.store() && !timeseriesTypeClass.isAnnotationPresent(Table.class))
            throw new DataStorageException("@Table annotation not present in class: " + timeseriesTypeClass);

        if (!(this.tingzoneTimeseriesAnnotation.store() || this.tingzoneTimeseriesAnnotation.index()))
            throw new DataStorageException("Nothing to do with class: " + timeseriesTypeClass);


        if (timeseriesTypeClass.isAnnotationPresent(Table.class)) {
            this.cassandraTableAnnotations = timeseriesTypeClass.getAnnotation(Table.class);
        } else {
            this.cassandraTableAnnotations = null;
        }

        this.klass = timeseriesTypeClass;
        this.cassandraMapper = CassandraDriver.getMappingManager().mapper(timeseriesTypeClass);
        this.elasticsearchClient = ElasticsearchDriver.getClient();
        this.streamKey = streamKey;
        this.manager = manager;

        // Get List of users that can consume the data stream
        try {
            subjects = manager.getSubjectsWithAllPermissions(
                    streamKey,
                    new String[]{tingzoneTimeseriesAnnotation.findPermission()}
                                                            );
        } catch (Exception e) {
            throw new DataStorageException("Could not get permissions", e);
        }
    }


    /**
     * Initialise a timeseries handler and infer the klass type
     * @param streamKey key of the data stream
     * @param manager reference to storage manager
     */
    public ThingzoneTimeseriesHandler(String streamKey, ThingzoneStorageManager manager)  throws Exception {
        this(streamKey, null, manager);
    }



    /**
     * Add a point to the timeseries
     * @param point timeseries data point
     * @return uuid of the point
     */
    public UUID addPoint(T point) throws DataStorageException, DataCachingException {
        if (point == null)
            throw new DataStorageException("Point object can not be null");

        if (point.getStreamKey() == null)
            point.setStreamKey(streamKey);

        try {
            if (!manager.hasAllPermissions(point.getStreamKey(), new String[]{tingzoneTimeseriesAnnotation.addPermission()}))
                throw new DataStorageException("Permission denied");
        } catch (Exception e) {
            throw new DataStorageException("Permission denied", e);
        }

        if (!point.getStreamKey().equals(streamKey))
            throw new DataStorageException("The stream Key does not correspond to the key of the timeseries");



        if (point instanceof Event) {
            String streamKey = point.getStreamKey();

            try {
                RegistryDataStream stream = (RegistryDataStream)RegistryAPIDriver.getSession()
                                                                        .internal
                                                                        .identify(streamKey, true, true);

                List<String> devices = stream.getDevices()
                                             .stream()
                                             .map(RegistryDeviceReference::getKey)
                                             .collect(Collectors.toList());

                for (String device : devices) {
                    RelatedRelation relatedRelation = new RelatedRelation();
                    relatedRelation.setType("dataStreamOf");
                    relatedRelation.setDescription("data stream of");
                    relatedRelation.setKlass(RegistryDevice.class.getName());
                    relatedRelation.setKey(device);
                    if (((Event) point).getRelatedTo() == null)
                        ((Event) point).setRelatedTo(new ArrayList<>());
                    if (!((Event) point).getRelatedTo().contains(relatedRelation) )
                        ((Event) point).getRelatedTo().add(relatedRelation);
                }


            } catch (Exception e) {
                throw new DataStorageException(e);
            }

        }


        //Store point to cassandra
        if (this.tingzoneTimeseriesAnnotation.store())
            cassandraMapper.save(point, saveNullFields(false));

        //Index point to elasticsearch
        if (this.tingzoneTimeseriesAnnotation.index() ) {

            for (String subject : subjects) {
                try {
                    ThingzoneTimeseriesIndex indexDocument =
                            new ThingzoneTimeseriesIndex.IndexBuilder(point, subject).build();
                    JestResult result = elasticsearchClient.execute(indexDocument);
                    if (!result.isSucceeded()) {
                        System.out.println(result.getErrorMessage());
                        throw new DataStorageException(result.getErrorMessage());
                    }

                } catch (DataCachingException | IOException e) {
                    System.out.println(e.getMessage());
                    throw new DataCachingException("Could not index document", e);
                }
            }
        }

        return point.getId();
    }


    /**
     * Add a point to the timeseries
     * @param point timeseries data point
     * @return uuid of the point
     */
    public List<ThingzoneTimeseriesIndex> addPointForBuldIndexing(T point) throws DataStorageException, DataCachingException {
        List<ThingzoneTimeseriesIndex> indexList = new ArrayList<>();

        if (point == null)
            throw new DataStorageException("Point object can not be null");

        if (point.getStreamKey() == null)
            point.setStreamKey(streamKey);

        try {
            if (!manager.hasAllPermissions(point.getStreamKey(), new String[]{tingzoneTimeseriesAnnotation.addPermission()}))
                throw new DataStorageException("Permission denied");
        } catch (Exception e) {
            throw new DataStorageException("Permission denied", e);
        }

        if (!point.getStreamKey().equals(streamKey))
            throw new DataStorageException("The stream Key does not correspond to the key of the timeseries");



        if (point instanceof Event) {
            String streamKey = point.getStreamKey();

            try {
                RegistryDataStream stream = (RegistryDataStream)RegistryAPIDriver.getSession()
                                                                        .internal
                                                                        .identify(streamKey, true, true);

                List<String> devices = stream.getDevices()
                                             .stream()
                                             .map(RegistryDeviceReference::getKey)
                                             .collect(Collectors.toList());

                for (String device : devices) {
                    RelatedRelation relatedRelation = new RelatedRelation();
                    relatedRelation.setType("dataStreamOf");
                    relatedRelation.setDescription("data stream of");
                    relatedRelation.setKlass(RegistryDevice.class.getName());
                    relatedRelation.setKey(device);
                    if (((Event) point).getRelatedTo() == null)
                        ((Event) point).setRelatedTo(new ArrayList<>());
                    if (!((Event) point).getRelatedTo().contains(relatedRelation) )
                        ((Event) point).getRelatedTo().add(relatedRelation);
                }


            } catch (Exception e) {
                throw new DataStorageException(e);
            }

        }


        //Store point to cassandra
        if (this.tingzoneTimeseriesAnnotation.store())
            cassandraMapper.save(point, saveNullFields(false));

        //Index point to elasticsearch
        if (this.tingzoneTimeseriesAnnotation.index() ) {

            for (String subject : subjects) {

                try {
                    indexList.add(new ThingzoneTimeseriesIndex.IndexBuilder(point, subject).build());
                } catch (DataCachingException e) {
                    System.out.println(e.getMessage());
                    throw new DataCachingException("Could not create index document", e);
                }
            }
        }

        return indexList;
    }







    /**
     * Remove a point from the timeseries
     * @param id uuid of the point to remove
     */
    public void removePoint(UUID id) throws DataStorageException, DataCachingException {
        if (id == null)
            throw new DataStorageException("ID can not be null");

        String query = "SELECT * FROM " +
                       cassandraTableAnnotations.keyspace() +
                       "." +
                       cassandraTableAnnotations.name() +
                       " " +
                       "WHERE id = :id ";
        PreparedStatement prepared = CassandraDriver.getSession().prepare(query);

        BoundStatement bound =
                prepared.bind(id);
        ResultSet results = CassandraDriver.getSession().execute(bound);
        Result<T> result = cassandraMapper.map(results);

        //Get information about the point to delete
        AbstractTimeseriesPoint point = result.one();

        if (point == null)
            throw new DataStorageException("Timeseries point id does not exists" + id.toString());

        removePoint(point);

    }

    /**
     * Remove a point from the timeseries
     * @param point instance of the point to remove
     */
    public void removePoint(AbstractTimeseriesPoint point) throws DataStorageException, DataCachingException {
        try {
            if (!manager.hasAllPermissions(point.getStreamKey(), new String[]{tingzoneTimeseriesAnnotation.removePermission()}))
                throw new DataStorageException("Permission denied");
        } catch (Exception e) {
            throw new DataStorageException("Permission denied", e);
        }

        if (this.tingzoneTimeseriesAnnotation.store())
            cassandraMapper.delete(point.getStreamKey(), point.getMonth(), point.getTimestamp());

        if (this.tingzoneTimeseriesAnnotation.index()) {
            for (String subject : subjects) {
                try {
                    ThingzoneTimeseriesIndexDelete deleteDocument =
                            new ThingzoneTimeseriesIndexDelete.IndexBuilder(point, subject).build();
                    JestResult result = elasticsearchClient.execute(deleteDocument);
                    if (!result.isSucceeded())
                        throw new DataStorageException(result.getErrorMessage());
                } catch (DataCachingException | IOException e) {
                    throw new DataCachingException("Could not delete document", e);
                }
            }
        }
    }

    /**
     * Find timeseries point by UUID
     * @param id uuid
     * @return the timeseries point matching the UUID
     */
    public T getPoint(UUID id) throws DataStorageException {
        if (!this.tingzoneTimeseriesAnnotation.store())
            throw new DataStorageException("Timeseries " + klass + " must be stored in Cassandra");

        try {
            if (!manager.hasAllPermissions(streamKey, new String[]{tingzoneTimeseriesAnnotation.findPermission()}))
                throw new DataStorageException("Permission denied");
        } catch (Exception e) {
            throw new DataStorageException("Permission denied", e);
        }

        String query = "SELECT * FROM " +
                       cassandraTableAnnotations.keyspace() +
                       "." +
                       cassandraTableAnnotations.name() +
                       " " +
                       "WHERE id = :id " +
                       "LIMIT 1 ";
        PreparedStatement prepared = CassandraDriver.getSession().prepare(query);
        //BoundStatement bound = prepared.bind(streamKey, id);
        BoundStatement bound = prepared.bind(id);

        ResultSet results = CassandraDriver.getSession().execute(bound);
        if (!results.isExhausted()) {
            Result<T> mappedResults = cassandraMapper.map(results);
            return mappedResults.one();
        }

        if (this.tingzoneTimeseriesAnnotation.index()) {

            SearchSourceBuilder searchBuilder = new SearchSourceBuilder()
                    .query(new TermQueryBuilder("id.keyword", id))
                    .size(1);

            Search search = new Search.Builder(searchBuilder.toString())
                    .addType(tingzoneTimeseriesAnnotation.indexType())
                    .build();

            SearchResult result;
            try {
                result = ElasticsearchDriver.getClient().execute(search);
            } catch (IOException e) {
                throw new DataStorageException("Could not execute elasticsearch query", e);
            }

            if (result.isSucceeded()) {
                SearchResult.Hit<JsonObject, Void> hit = result.getFirstHit(JsonObject.class);
                if (hit != null) {
                    return jsonStringSerialiser.readObject(hit.source, klass);
                } else {
                    throw new DataStorageException("Could not find point in elasticsearch id: " + id);
                }
            } else {
                throw new DataStorageException(result.getErrorMessage());
            }
        } else {
            throw new DataStorageException("Data point not available in elasticsearch");
        }
    }


    /**
     * @return latest timeseries point
     */
    public T findLatest() throws DataStorageException {
        List<T> latest = findLatestN(1);
        return (latest.size() == 1) ? latest.get(0) : null;
    }


    /**
     * Find the latest timeseries point to a specific date
     * @return timeseries point
     */
    public List<T> findLatestN(int number) throws DataStorageException {
        if (!this.tingzoneTimeseriesAnnotation.store())
            throw new DataStorageException("Timeseries " + klass + " in not stored in Cassandra");

        try {
            if (!manager.hasAllPermissions(streamKey, new String[]{tingzoneTimeseriesAnnotation.findPermission()}))
                throw new DataStorageException("Permission denied");
        } catch (Exception e) {
            throw new DataStorageException("Permission denied", e);
        }


        Instant now = Instant.now();
        LocalDateTime ldt = LocalDateTime.ofInstant(now, ZoneId.systemDefault());
        int month = ldt.getMonth().getValue();


        String query = "SELECT * FROM " +
                       cassandraTableAnnotations.keyspace() +
                       "." +
                       cassandraTableAnnotations.name() +
                       " " +
                       "WHERE streamkey = :key " +
                       " AND month = :month " +
                       "LIMIT :number ";
        PreparedStatement prepared = CassandraDriver.getSession().prepare(query);
        BoundStatement bound = prepared.bind(streamKey, month, number);

        ResultSet results = CassandraDriver.getSession().execute(bound);
        Result<T> mappedResults = cassandraMapper.map(results);

        return mappedResults.all();
    }



    /**
     * Find the latest timeseries point to a specific date
     * @param to date
     * @return timeseries point
     */
    public T findLatest(Instant to) throws DataStorageException {

        if (!this.tingzoneTimeseriesAnnotation.store())
            throw new DataStorageException("Timeseries " + klass + " in not stored in Cassandra");

        try {
            if (!manager.hasAllPermissions(streamKey, new String[]{tingzoneTimeseriesAnnotation.findPermission()}))
                throw new DataStorageException("Permission denied");
        } catch (Exception e) {
            throw new DataStorageException("Permission denied", e);
        }

        LocalDateTime ldt = LocalDateTime.ofInstant(to, ZoneId.systemDefault());
        int month = ldt.getMonth().getValue();

        String query = "SELECT * FROM " +
                       cassandraTableAnnotations.keyspace() +
                       "." +
                       cassandraTableAnnotations.name() +
                       " " +
                       "WHERE streamkey = :key " +
                       " AND month = :month " +
                       " AND timestamp <= :dateTo " +
                       "LIMIT 1 ";
        PreparedStatement prepared = CassandraDriver.getSession().prepare(query);
        BoundStatement bound = prepared.bind(streamKey, month, Date.from(to));

        ResultSet results = CassandraDriver.getSession().execute(bound);
        Result<T> mappedResults = cassandraMapper.map(results);

        return mappedResults.one();
    }

    /**
     * Return data within a time period
     * @param from timne from
     * @param to time to
     * @return list of timeseries points
     */
    public List<T> find(Instant from, Instant to) throws DataStorageException {

        if (!this.tingzoneTimeseriesAnnotation.store())
            throw new DataStorageException("Timeseries " + klass + " must be stored in Cassandra");

        try {
            if (!manager.hasAllPermissions(streamKey, new String[]{tingzoneTimeseriesAnnotation.findPermission()}))
                throw new DataStorageException("Permission denied");
        } catch (Exception e) {
            throw new DataStorageException("Permission denied", e);
        }


        LocalDateTime ldtFrom = LocalDateTime.ofInstant(from, ZoneId.systemDefault());
        int monthFrom = ldtFrom.getMonth().getValue();

        LocalDateTime ldtTo = LocalDateTime.ofInstant(to, ZoneId.systemDefault());
        int monthTo = ldtTo.getMonth().getValue();

        String query = "SELECT * FROM " +
                       cassandraTableAnnotations.keyspace() +
                       "." +
                       cassandraTableAnnotations.name() +
                       " " +
                       "WHERE streamkey=:key " +
                       "AND month IN :months " +
                       "AND timestamp >= :dateFrom AND timestamp < :dateTo ";
        PreparedStatement prepared = CassandraDriver.getSession().prepare(query);

        List<Integer> months = Ints.asList(IntStream.range(monthFrom, monthTo+1).toArray());

        BoundStatement bound = prepared.bind(streamKey, null, Date.from(from), Date.from(to));
        bound.setList("months", months);

        ResultSet results = CassandraDriver.getSession().execute(bound);
        Result<T> mappedResults = cassandraMapper.map(results);


        return mappedResults.all();
    }



    /**
     * Add a tag to a point
     * @param pointId UUID of the event
     * @param tag new tag
     */
    public T addTag(UUID pointId, String tag) throws DataStorageException, DataCachingException {
        T point = getPoint(pointId);

        if (point == null) {
            throw new DataStorageException("Could not find point with id " + pointId);
        }

        if (point.getTags() == null) point.setTags(new ArrayList<>());

        List<String> tags = point.getTags();

        if (!tags.contains(tag)) {
            tags.add(tag);
            addPoint(point);
        } else {
            throw new DataStorageException("The tag " + tag + " already exists");
        }

        return point;
    }

    /**
     * Replace classification for a point
     * @param pointId
     * @param classification
     */
    public T replaceClassification(UUID pointId, List<Classification> classification) throws DataStorageException, DataCachingException {
        T point = getPoint(pointId);

        if (point == null) {
            throw new DataStorageException("Could not find point with id " + pointId);
        }

        point.setClassifications(classification);
        addPoint(point);

        return point;
    }

        /**
         * Remove tag from point
         * @param pointId UUID of the event
         * @param tag tag
         */
    public T removeTag(UUID pointId, String tag) throws DataStorageException, DataCachingException {
        T point = getPoint(pointId);

        if (point == null) throw new DataStorageException("Could not find point with id " + pointId.toString());

        if (point.getTags() != null) {
            boolean removed = point.getTags().removeIf(item -> item.equals(tag));
            if (!removed) throw new DataStorageException("Could not remove tag " + tag);
        }

        addPoint(point);

        return point;

    }

    /**
     * Get the class representing this timeseries points
     * @return timeseries data point class
     */
    public Class<T> getTimeseriesClass() {
        return klass;
    }


    /**
     * Get the stream key for this timeseries
     * @return stream key
     */
    public String getStreamKey() {
        return streamKey;
    }
}
