/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.init;

import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonObject;
import io.searchbox.client.JestResult;
import io.searchbox.indices.DeleteIndex;
import io.searchbox.indices.Refresh;
import io.searchbox.indices.aliases.GetAliases;
import io.searchbox.indices.reindex.Reindex;
import io.searchbox.indices.template.GetTemplate;
import uk.nominet.iot.config.ElasticsearchDriverConfig;
import uk.nominet.iot.config.ElasticsearchConstant;
import uk.nominet.iot.driver.ElasticsearchDriver;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import static uk.nominet.iot.utils.ElasticMappingsUtils.getIndexTemplateAction;
import static uk.nominet.iot.utils.ElasticMappingsUtils.getTemplate;

/**
 * Init elasticsearch database
 */
public class ElasticInitService extends DatabaseInitialiser {
    private static final Logger LOG = LoggerFactory.getLogger(ElasticInitService.class);

    /**
     * Move the content of a index to another with a different name
     * @param sourceIndex source index name
     * @param destIndex   destination index name
     * @return results of the task
     */
    private JestResult moveIndex(String sourceIndex, String destIndex) throws IOException {
        ImmutableMap<String, Object> source = ImmutableMap.of("index", sourceIndex);
        ImmutableMap<String, Object> dest = ImmutableMap.of("index", destIndex);
        Reindex reindex = new Reindex.Builder(source, dest)
                                  .waitForCompletion(true)
                                  .build();

        JestResult reindexRes = null;
        try {
            reindexRes = ElasticsearchDriver.getClient().execute(reindex);
        } catch (IOException e) {
            e.printStackTrace();
        }

        assert reindexRes != null;
        if (!reindexRes.isSucceeded()) {
            LOG.error(reindexRes.getErrorMessage());
            System.out.println(reindexRes.getErrorMessage());
            return reindexRes;
        } else {

            Refresh refresh = new Refresh.Builder().addIndex(destIndex).build();
            JestResult refreshResult = ElasticsearchDriver.getClient().execute(refresh);

            if (!refreshResult.isSucceeded()) {
                LOG.error(refreshResult.getErrorMessage());
                System.out.println(refreshResult.getErrorMessage());
                return refreshResult;
            } else {

                DeleteIndex deleteIndexAction = new DeleteIndex.Builder(sourceIndex).build();
                JestResult deleteIndexRes = null;
                try {
                    deleteIndexRes = ElasticsearchDriver.getClient().execute(deleteIndexAction);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                assert deleteIndexRes != null;
                if (!deleteIndexRes.isSucceeded()) {
                    LOG.error(deleteIndexRes.getErrorMessage());
                    System.out.println(deleteIndexRes.getErrorMessage());
                    return deleteIndexRes;
                } else {
                    return deleteIndexRes;
                }
            }
        }
    }


    /**
     * Apply new mappings to existing elasticsearch user indexes
     */
    private void remapUserIndexes() throws IOException {
        GetAliases aliasesAction = new GetAliases.Builder().build();
        JestResult aliasesRes = ElasticsearchDriver.getClient().execute(aliasesAction);
        if (!aliasesRes.isSucceeded()) {
            LOG.error(aliasesRes.getErrorMessage());
            System.out.println(aliasesRes.getErrorMessage());
            return;
        }

        LOG.info(aliasesRes.getJsonString());

        JsonObject aliases = aliasesRes.getJsonObject();
        aliases.entrySet().forEach(index -> {
            String indexName = index.getKey();
            JestResult moveIndexRes = null;
            try {
                moveIndexRes = moveIndex(indexName, indexName + "_temp");
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (moveIndexRes != null && moveIndexRes.isSucceeded()) {
                JestResult moveIndexBackRes = null;
                try {
                    moveIndexBackRes = moveIndex(indexName + "_temp", indexName);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                assert moveIndexBackRes != null;
                if (!moveIndexBackRes.isSucceeded()) {
                    System.out.println(moveIndexBackRes.getErrorMessage());
                    LOG.error(moveIndexBackRes.getErrorMessage());
                }
            }
        });

    }

    /**
     * Upload a new mapping template
     * @param mappingTemplate Json object representing the mapping template
     */
    private void uploadMappingTemplate(JSONObject mappingTemplate) throws Exception {
        LOG.info("Template Version: " + mappingTemplate.getInt("version"));
        JestResult templateUploadResult = ElasticsearchDriver.getClient().execute(
                getIndexTemplateAction("thingzone_template",
                                       mappingTemplate.toString())
                                                                                 );

        if (templateUploadResult.isSucceeded()) {
            LOG.info(" New template successfully uploaded");
            remapUserIndexes();
        } else {
            LOG.error("Eror uploading template: " + templateUploadResult.getErrorMessage());
            System.out.println(templateUploadResult.getErrorMessage());

        }
    }

    /**
     * Initialise elasticsearch database
     * @param mappingTemplate JsonObject representing the mapping template to use
     */
    public void initElasticsearch(JSONObject mappingTemplate) throws Exception {
        if (!mappingTemplate.has("version"))
            throw new Exception("The mapping template is missing" +
                                " a version number");

        LOG.info("Initialising Elasticsearch...");


        int newVersion = mappingTemplate.getInt("version");

        GetTemplate getTemplateAction = new GetTemplate.Builder("thingzone_template").build();
        JestResult templateResult = ElasticsearchDriver.getClient().execute(getTemplateAction);

        if (templateResult.isSucceeded()) {
            JsonObject oldTemplateObject = templateResult.getJsonObject().get("thingzone_template").getAsJsonObject();
            if (!oldTemplateObject.has("version")) {
                LOG.info("The existing template does not have a version number." +
                         " Suspecting an older version of the system. Uploading a new template");
                uploadMappingTemplate(mappingTemplate);
            }
            int oldVersion = oldTemplateObject.get("version").getAsInt();
            if (oldVersion != newVersion) {
                LOG.info("The thigzone data model is using a different version " +
                         "of the template. Uploading a new template");
                uploadMappingTemplate(mappingTemplate);
            } else {
                LOG.info("Template version match. Nothing to do.");

            }
        } else {
            LOG.info("The template does not exist. Uploading a new template");
            uploadMappingTemplate(mappingTemplate);
        }

        LOG.info("Initialisation completed.");
    }


    /**
     * Initialise elasticsearch database
     */
    public void init() throws Exception {
        JSONObject mappingTemplate = getTemplate();
        assert mappingTemplate != null;
        initElasticsearch(mappingTemplate);
    }

    /**
     * Clean elasticsearch database
     */
    @Override
    public void clean() {
    }

    /**
     * Print usage information for init service
     * @param errorMessage error message
     */
    private static void printUsage(String errorMessage) {
        if (errorMessage != null)
            System.out.println("Error: " + errorMessage);
        System.out.println("Usage: ElasticInitService clean|init url");
    }

    public static void main(String[] args) throws URISyntaxException {
        if (args.length != 2) {
            printUsage("wrong number of arguments");
            System.exit(1);
        }

        String task = args[0];

        URI uri = new URI(args[1]);

        if (!ElasticsearchDriver.isInitialized()) {
            ElasticsearchDriverConfig config = new ElasticsearchDriverConfig();
            config.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_SCHEME, uri.getScheme());
            config.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_HOST, uri.getHost());
            config.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_PORT, uri.getPort());
            ElasticsearchDriver.setConnectionProperties(config);
            ElasticsearchDriver.globalInitialise();
        }

        ElasticInitService elasticInitService = new ElasticInitService();

        try {
            elasticInitService.executeTask(task);
        } catch (Exception e) {
            printUsage(e.getMessage());
            System.exit(1);
        }
    }

}
