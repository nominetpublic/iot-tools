/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.manager;

import com.datastax.driver.mapping.Mapper;
import uk.nominet.iot.driver.CassandraDriver;
import uk.nominet.iot.model.admin.DatabaseMinc;

import java.util.Optional;


public class ThingzoneDatabaseMincManager extends ThingzoneStorageManager {
    private Mapper<DatabaseMinc> cassandraMapper;

    public ThingzoneDatabaseMincManager() {
        super(Optional.empty());
        this.cassandraMapper = CassandraDriver.getMappingManager().mapper(DatabaseMinc.class);
    }

    /**
     * Add new database minc
     * @param minc database minc
     */
    public void addMinc(DatabaseMinc minc) {
        cassandraMapper.save(minc);
    }

    /**
     * Get database min by id
     * @param id id of the database minc
     * @return database minc
     */
    public DatabaseMinc getMincById(String id) {
        return cassandraMapper.get(id);
    }

    /**
     * Remove database minc
     * @param id id of the minc to remove
     */
    public void removeMincById(String id) {
        cassandraMapper.delete(id);
    }

    /**
     * Create a new database minc manager
     * @return Database minc manager
     */
    public static ThingzoneDatabaseMincManager createManager() {
        return new ThingzoneDatabaseMincManager();
    }
}
