/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.driver;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.CodecRegistry;
import com.datastax.driver.core.Session;
import com.datastax.driver.extras.codecs.enums.EnumNameCodec;
import com.datastax.driver.mapping.MappingManager;
import uk.nominet.iot.config.CassandraDriverConfig;
import uk.nominet.iot.config.DriverConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.nominet.iot.model.timeseries.ThingzoneConfigOrigin;
import uk.nominet.iot.model.timeseries.ThingzoneEventType;

import java.lang.invoke.MethodHandles;

public class CassandraDriver {
    private final static Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private static MappingManager mappingManager;
    private static Cluster cluster;
    private static Session session;
    private static CassandraDriverConfig configuration;
    private static boolean initialized = false;

    /**
     * Set connection properties for the cassandra driver
     * @param configuration driver configuration
     */
    public static void setConnectionProperties(DriverConfig configuration) {
        CassandraDriver.configuration = (CassandraDriverConfig) configuration;
    }

    /**
     * Initialise the driver globally
     */
    public static void globalInitialise() {
        if (initialized) { // we should initialise exactly once!
            throw new RuntimeException("Cassandra Database already initialised");
        }

        if (mappingManager != null) {
            throw new RuntimeException("Cassandra Database Client already initialised");
        }

        LOG.info("initialising cassandra database");

        cluster = Cluster.builder()
                         .addContactPoint(configuration.getHost())
                         .withPort(configuration.getPort())
                         .build();

        CodecRegistry codecRegistry = cluster.getConfiguration().getCodecRegistry();
        codecRegistry.register(new EnumNameCodec<>(ThingzoneEventType.class));
        codecRegistry.register(new EnumNameCodec<>(ThingzoneConfigOrigin.class));

        session = cluster.connect();

        mappingManager = new MappingManager(session);

        initialized = true;

    }

    /**
     * Check if the driver is already initialised
     * @return true if initialised otherwise false
     */
    synchronized public static boolean isInitialized() {
        return initialized;
    }

    /**
     * Return an instance of the mapping manager from the current cassandra session
     * @return mapping manager
     */
    public static MappingManager getMappingManager() {
        if (!initialized)
            throw new RuntimeException("Cassandra database not yet initialised");

        if (mappingManager == null)
            throw new RuntimeException("Cassandra driver not yet initialised");

        return mappingManager;
    }

    /**
     * Get the current cassandra session
     * @return cassandra session
     */
    public static Session getSession() {
        if (!initialized)
            throw new RuntimeException("Cassandra database not yet initialised");

        if (session == null)
            throw new RuntimeException("Cassandra driver  not yet initialised");

        return session;
    }

    /**
     * Globally dispose of the driver
     */
    public static void globalDispose() {
        if (isInitialized()) {
            cluster.close();
            initialized = false;
            mappingManager = null;
        }
    }

}
