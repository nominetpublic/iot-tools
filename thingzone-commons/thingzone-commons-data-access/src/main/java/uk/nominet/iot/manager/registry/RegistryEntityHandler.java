/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.manager.registry;


import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.error.DataStorageException;
import uk.nominet.iot.manager.ThingzoneStorageManager;
import uk.nominet.iot.model.registry.RegistryEntity;
import uk.nominet.iot.registry.RegistryAPISession;

import java.util.Optional;


public class RegistryEntityHandler {
    private String key;
    private final RegistryAPISession registrySession;
    private ThingzoneStorageManager manager;

    /**
     * Get the registry transform function
     * @return transform function
     */
    public RegistryEntity get() throws Exception {
        return null;
    }


    /**
     * Updates values on the key
     * @param name a name for the entity
     * @param type
     * @throws Exception
     */
    public void update(Optional<String> name, Optional<String> type) throws Exception {
        registrySession.entity.update(key, name, type);
    }

    /**
     * Initialise a new registry entity handler
     * @param key key
     * @param manager reference to storage manager
     */
    public RegistryEntityHandler(String key,  ThingzoneStorageManager manager) throws DataStorageException {
        this.key = key;
        if (manager.getUserContext().isPresent()) {

            registrySession = RegistryAPIDriver.getUserSession(
                    manager.getUserContext().get().getUsername(),
                    manager.getUserContext().get().getPassword());

        } else {
                       registrySession = RegistryAPIDriver.getSession();
        }
        this.manager = manager;

    }
}
