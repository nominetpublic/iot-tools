/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.manager;

import uk.nominet.iot.error.DataStorageException;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.Optional;

public class ThingzoneTimeseriesManager extends ThingzoneStorageManager {
    private final static Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public ThingzoneTimeseriesManager(Optional<UserContext> userContext) {
        super(userContext);
    }

    /**
     * Get a handler for an existing timeseries
     * @param key datastream key
     * @param klass class of the timeseries data points
     * @param <T> class of the timeseries data points
     * @return timeseries holder
     */
   public <T extends AbstractTimeseriesPoint> ThingzoneTimeseriesHandler<T> timeseries(String key, Class klass)
           throws Exception {
        return new ThingzoneTimeseriesHandler<T>(key, klass, this);
   }


    /**
     * Get a handler for an existing timeseries and infer the klass tyoe
     * @param key datastream key
     * @param <T> class of the timeseries data points
     * @return timeseries holder
     */
    public <T extends AbstractTimeseriesPoint> ThingzoneTimeseriesHandler<T> timeseries(String key)
            throws Exception {
        return new ThingzoneTimeseriesHandler<T>(key,this);
    }




    /**
     * Create a new timeseries manager
     * @return timeseries manager
     */
    public static ThingzoneTimeseriesManager createManager() {
        return new ThingzoneTimeseriesManager(Optional.empty());
    }

    /**
     * Create a new timeseries manager with user context
     * @param userContext user context
     * @return timeseries manager
     */
    public static ThingzoneTimeseriesManager createManager(UserContext userContext) {
        return new ThingzoneTimeseriesManager(Optional.of(userContext));
    }

}
