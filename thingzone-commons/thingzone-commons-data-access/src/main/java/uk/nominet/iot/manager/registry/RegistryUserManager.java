/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.manager.registry;

import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.error.DataStorageException;
import uk.nominet.iot.manager.ThingzoneStorageManager;
import uk.nominet.iot.manager.UserContext;

import java.util.Optional;

public class RegistryUserManager extends ThingzoneStorageManager {

    public RegistryUserManager(Optional<UserContext> userContext) {
        super(userContext);
    }

    /**
     * Create a new user and attach a handler to it
     * @param username   name of the user
     * @param password   password
     * @param privileges set of privileges
     * @return regustry user handler
     */
    public RegistryUserHandler create(String username, String password, String[] privileges) throws Exception {
        RegistryAPIDriver.getSession().user.create(username, password, privileges);
        return user(username);
    }


    /**
     * Get a handler for an existing user
     * @param username name of the user
     * @return user handler
     */
    public RegistryUserHandler user(String username) {
        return new RegistryUserHandler(username, this);
    }


    /**
     * Create new manager
     * @return user manager
     */
    public static RegistryUserManager createManager() {
        return new RegistryUserManager(Optional.empty());
    }

    /**
     * Create new manager with user context
     * @return user manager
     */
    public static RegistryUserManager createManager(UserContext userContext) {
        return new RegistryUserManager(Optional.of(userContext));
    }
}
