/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.tools;

import org.apache.commons.csv.CSVParser;
import uk.nominet.iot.config.*;
import uk.nominet.iot.driver.CassandraDriver;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.manager.ThingzoneTimeseriesHandler;
import uk.nominet.iot.manager.ThingzoneTimeseriesManager;
import uk.nominet.iot.manager.UserContext;
import uk.nominet.iot.model.LatLong;
import uk.nominet.iot.model.timeseries.MetricDouble;
import uk.nominet.iot.model.timeseries.MetricInt;
import uk.nominet.iot.model.timeseries.MetricLatLong;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class UploadMobileMetricData {
    JSONObject job;
    File csvData;

    public UploadMobileMetricData(JSONObject job, File csvData) {
        this.job = job;
        this.csvData = csvData;

        JSONObject registryConfigJson = job.getJSONObject("registry");
        JSONObject cassandraConfigJson = job.getJSONObject("cassandra");
        JSONObject elasticsearchConfigJson = job.getJSONObject("elasticsearch");

        if (!CassandraDriver.isInitialized()) {
            CassandraDriverConfig cassandraConfig = new CassandraDriverConfig();
            cassandraConfig.addProperty(CassandraConstant.CASSANDRA_SERVER_HOST, cassandraConfigJson.getString("host"));
            cassandraConfig.addProperty(CassandraConstant.CASSANDRA_SERVER_PORT, cassandraConfigJson.getInt("port"));
            CassandraDriver.setConnectionProperties(cassandraConfig);
            CassandraDriver.globalInitialise();
        }

        if (!RegistryAPIDriver.isInitialized()) {
            RegistryDriverConfig registryConfig = new RegistryDriverConfig();
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_SCHEME, registryConfigJson.getString("scheme"));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST, registryConfigJson.getString("host"));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT, registryConfigJson.getInt("port"));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_USERNAME,
                                       registryConfigJson.getString("admin_username"));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD,
                                       registryConfigJson.getString("admin_password"));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS, 3);
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }
        if (!ElasticsearchDriver.isInitialized()) {
            ElasticsearchDriverConfig elasticsearchConfig = new ElasticsearchDriverConfig();
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_SCHEME,
                                            elasticsearchConfigJson.getString("scheme"));
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_HOST,
                                            elasticsearchConfigJson.getString("host"));
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_PATH,
                                            elasticsearchConfigJson.getString("path"));
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_PORT,
                                            elasticsearchConfigJson.getInt("port"));
            ElasticsearchDriver.setConnectionProperties(elasticsearchConfig);
            ElasticsearchDriver.globalInitialise();
        }
    }

    public void uploadMetrics() throws Exception {
        JSONObject registryConfigJson = job.getJSONObject("registry");

        UserContext userContext = new UserContext(registryConfigJson.getString("username"));
        userContext.setPassword(registryConfigJson.getString("password"));

        ThingzoneTimeseriesManager timeseriesManager = ThingzoneTimeseriesManager.createManager(userContext);

        Reader mobileDataReader = new FileReader(csvData);
        CSVParser mobileDataCSV = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(mobileDataReader);

        for (CSVRecord record : mobileDataCSV) {
            System.out.println("Processing line: " + mobileDataCSV.getCurrentLineNumber());

            Map<String, String> datum = record.toMap();
            String deviceKey = datum.get("vehicle_id").toLowerCase();

            //Location
            ThingzoneTimeseriesHandler<MetricLatLong> timeseriesLocation =
                    timeseriesManager.timeseries(deviceKey + ".location", MetricLatLong.class);
            MetricLatLong locationMetric = new MetricLatLong();
            LatLong loc = new LatLong();
            loc.setLat(Double.parseDouble(datum.get("mobile.latitude")));
            loc.setLon(Double.parseDouble(datum.get("mobile.longitude")));
            locationMetric.setValue(loc);
            locationMetric.setId(UUID.randomUUID());
            locationMetric.setTimestamp(Instant.parse(datum.get("time")));
            HashMap<String, Object> locationMetadata = new HashMap<>();
            locationMetadata.put("accuracy", datum.get("mobile.accuracy"));
            locationMetadata.put("bearing", datum.get("mobile.bearing"));
            locationMetadata.put("speed", datum.get("mobile.speed"));
            locationMetric.setMetadata(locationMetadata);
            timeseriesLocation.addPoint(locationMetric);

            //Velocity
            ThingzoneTimeseriesHandler<MetricDouble> timeseriesVelocity =
                    timeseriesManager.timeseries(deviceKey + ".velocity", MetricDouble.class);
            MetricDouble velocityMetric = new MetricDouble();
            velocityMetric.setValue(Double.parseDouble(datum.get("mobile.speed")));
            velocityMetric.setId(UUID.randomUUID());
            velocityMetric.setTimestamp(Instant.parse(datum.get("time")));
            timeseriesVelocity.addPoint(velocityMetric);

            //DBM
            ThingzoneTimeseriesHandler<MetricInt> timeseriesDBM =
                    timeseriesManager.timeseries(deviceKey + ".dbm", MetricInt.class);
            MetricInt dbmMetric = new MetricInt();
            dbmMetric.setValue(Integer.parseInt(datum.get("DBM")));
            dbmMetric.setId(UUID.randomUUID());
            dbmMetric.setTimestamp(Instant.parse(datum.get("time")));
            HashMap<String, Object> dbmMetadata = new HashMap<>();
            dbmMetadata.put("cellKey", datum.get("CELL_KEY"));
            dbmMetric.setMetadata(dbmMetadata);
            timeseriesDBM.addPoint(dbmMetric);

            // RSRP
            ThingzoneTimeseriesHandler<MetricInt> timeseriesRSRP=
                    timeseriesManager.timeseries(deviceKey + ".rsrp", MetricInt.class);
            MetricInt rsrpMetric = new MetricInt();
            rsrpMetric.setValue(Integer.parseInt(datum.get("RSRP")));
            rsrpMetric.setId(UUID.randomUUID());
            rsrpMetric.setTimestamp(Instant.parse(datum.get("time")));
            HashMap<String, Object> rsrpMetadata = new HashMap<>();
            rsrpMetadata.put("cellKey", datum.get("CELL_KEY"));
            rsrpMetric.setMetadata(rsrpMetadata);
            timeseriesRSRP.addPoint(rsrpMetric);

            //RSRQ
            ThingzoneTimeseriesHandler<MetricInt> timeseriesRSRQ=
                    timeseriesManager.timeseries(deviceKey + ".rsrq", MetricInt.class);
            MetricInt rsrqMetric = new MetricInt();
            rsrqMetric.setValue(Integer.parseInt(datum.get("RSRQ")));
            rsrqMetric.setId(UUID.randomUUID());
            rsrqMetric.setTimestamp(Instant.parse(datum.get("time")));
            HashMap<String, Object> rsrqMetadata = new HashMap<>();
            rsrqMetadata.put("cellKey", datum.get("CELL_KEY"));
            rsrqMetric.setMetadata(rsrqMetadata);
            timeseriesRSRQ.addPoint(rsrqMetric);

            //SINR
            ThingzoneTimeseriesHandler<MetricDouble> timeseriesSINR=
                    timeseriesManager.timeseries(deviceKey + ".sinr", MetricDouble.class);
            MetricDouble sinrMetric = new MetricDouble();
            sinrMetric.setValue(Double.parseDouble(datum.get("SINR")));
            sinrMetric.setId(UUID.randomUUID());
            sinrMetric.setTimestamp(Instant.parse(datum.get("time")));
            HashMap<String, Object> sinrMetadata = new HashMap<>();
            sinrMetadata.put("cellKey", datum.get("CELL_KEY"));
            sinrMetric.setMetadata(sinrMetadata);
            timeseriesSINR.addPoint(sinrMetric);

            //SS
            ThingzoneTimeseriesHandler<MetricInt> timeseriesSS=
                    timeseriesManager.timeseries(deviceKey + ".ss", MetricInt.class);
            MetricInt ssMetric = new MetricInt();
            ssMetric.setValue(Integer.parseInt(datum.get("SS")));
            ssMetric.setId(UUID.randomUUID());
            ssMetric.setTimestamp(Instant.parse(datum.get("time")));
            HashMap<String, Object> ssMetadata = new HashMap<>();
            ssMetadata.put("cellKey", datum.get("CELL_KEY"));
            ssMetric.setMetadata(ssMetadata);
            timeseriesSS.addPoint(ssMetric);

            //SS
            ThingzoneTimeseriesHandler<MetricInt> timeseriesCONNECTED =
                    timeseriesManager.timeseries(deviceKey + ".connected", MetricInt.class);
            MetricInt connectedMetric = new MetricInt();
            connectedMetric.setValue(Integer.parseInt(datum.get("connection_stat")));
            connectedMetric.setId(UUID.randomUUID());
            connectedMetric.setTimestamp(Instant.parse(datum.get("time")));
            HashMap<String, Object> connectedMetadata = new HashMap<>();
            connectedMetadata.put("cellKey", datum.get("CELL_KEY"));
            connectedMetric.setMetadata(connectedMetadata);
            timeseriesCONNECTED.addPoint(connectedMetric);


            //MODEMSYSMODE
            ThingzoneTimeseriesHandler<MetricInt> timeseriesMODEMSYSMODE =
                    timeseriesManager.timeseries(deviceKey + ".modemsysmode", MetricInt.class);
            MetricInt modemsysmodeMetric = new MetricInt();
            int value = -1;
            switch (datum.get("MODEMSYSMODE")) {
                case "LTE":
                    value = 2;
                    break;
                case "WCDMA":
                    value = 1;
                    break;
                case "GSM":
                    value = 0;
                    break;
            }

            modemsysmodeMetric.setValue(value);
            modemsysmodeMetric.setId(UUID.randomUUID());
            modemsysmodeMetric.setTimestamp(Instant.parse(datum.get("time")));
            HashMap<String, Object> modemsysmodeMetadata = new HashMap<>();
            modemsysmodeMetadata.put("cellKey", datum.get("CELL_KEY"));
            modemsysmodeMetric.setMetadata(modemsysmodeMetadata);
            timeseriesMODEMSYSMODE.addPoint(modemsysmodeMetric);
        }
    }

    public static void main(String args[]) throws Exception {
        if (args.length != 2) {
            System.out.println("Usage: UploadMobileMetricData config.json data.csv");
            System.exit(1);
        }

        InputStream jobInputStream = new FileInputStream(args[0]);
        String jsonTxt = IOUtils.toString(jobInputStream, StandardCharsets.UTF_8);
        JSONObject job = new JSONObject(jsonTxt);

        File csvData = new File(args[1]);

        UploadMobileMetricData uploadMobileMetricData = new UploadMobileMetricData(job, csvData);
        uploadMobileMetricData.uploadMetrics();
        System.out.println("Done! ");
    }
}
