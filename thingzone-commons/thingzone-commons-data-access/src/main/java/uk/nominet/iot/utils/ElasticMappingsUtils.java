/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.utils;

import io.searchbox.indices.template.PutTemplate;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.net.URL;
import java.nio.file.*;
import java.util.Collections;
import java.util.Iterator;
import java.util.stream.Stream;

public class ElasticMappingsUtils {

    public static JSONObject getTemplate() throws Exception {

        JSONObject mappings = new JSONObject();
        JSONObject template = new JSONObject();

        FileSystem fileSystem = null;
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        URL mappingsUrl = classloader.getResource("mappings/");
        if (mappingsUrl == null) return null;
        URI uri = mappingsUrl.toURI();
        Path myPath;

        if (uri.getScheme().equals("jar")) {
            fileSystem = FileSystems.newFileSystem(uri, Collections.emptyMap());
            myPath = fileSystem.getPath("mappings/");
        } else {
            myPath = Paths.get(uri);
        }

        try(Stream<Path> walk = Files.walk(myPath, 1)) {
            for (Iterator<Path> it = walk.iterator(); it.hasNext(); ) {
                Path mappingFile = it.next();
                if (mappingFile.toString().endsWith(".json")) {
                    String fileToLoad = mappingFile.getFileName().toString();
                    String mappingEntity = fileToLoad.replaceFirst("[.][^.]+$", "");
                    JSONObject mappingJSONObject = loadMappings(fileToLoad);
                    mappings.put(mappingEntity, mappingJSONObject.get(mappingEntity));
                }
            }
        }

        template.put("mappings", mappings);
        template.put("template", "thingzone_*");
        int versionHash = template.toString().hashCode();
        template.put("version", versionHash);

        if (fileSystem != null) fileSystem.close();

        return template;
    }

    public static PutTemplate getIndexTemplateAction(String name, String template) {
        return new PutTemplate.Builder(name, template).build();
    }

    private static JSONObject loadMappings(String filename) throws Exception {
        if (!filename.endsWith(".json")) {
            filename = filename + ".json";
        }

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream("mappings/" + filename);
        if (is != null) {
            Reader reader = new InputStreamReader(is);
            return new JSONObject(new JSONTokener(reader));
        }
        throw new Exception("Mapping file resource" + filename + " not found");
    }
}
