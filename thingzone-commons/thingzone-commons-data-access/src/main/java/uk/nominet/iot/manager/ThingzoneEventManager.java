/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.manager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.Optional;

public class ThingzoneEventManager extends ThingzoneStorageManager {
    private final static Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public ThingzoneEventManager(Optional<UserContext> userContext) {
        super(userContext);
    }

    /**
     * Get a handler for an existing alert timeseries
     * @param key datastream key
     * @return timeseries handler
     */
   public ThingzoneEventHandler timeseries(String key)
           throws Exception {
        return new ThingzoneEventHandler(key, this);
   }

    /**
     * Create a new timeseries manager
     * @return timeseries manager
     */
    public static ThingzoneEventManager createManager() {
        return new ThingzoneEventManager(Optional.empty());
    }

    /**
     * Create a new timeseries manager with user context
     * @param userContext user context
     * @return timeseries manager
     */
    public static ThingzoneEventManager createManager(UserContext userContext) {
        return new ThingzoneEventManager(Optional.of(userContext));
    }

}
