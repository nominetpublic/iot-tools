/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.manager.registry;

import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.manager.ThingzoneStorageManager;
import uk.nominet.iot.model.registry.RegistryTransformFunction;
import uk.nominet.iot.registry.RegistryAPISession;

import java.util.Optional;


public class RegistryFunctionHandler {
    private final boolean isPublic;
    private String functionName;
    private final RegistryAPISession registrySession;

    /**
     * Get the registry transform function
     * @return transform function
     */
    public RegistryTransformFunction get() throws Exception {
        return registrySession.transform.getFunction(functionName, isPublic);
    }

    /**
     * Update the function
     * @param functionType    type of function e.g. javascript
     * @param functionContent content of the function
     */
    public void updateFunction(String functionType, String functionContent) throws Exception {
        registrySession.transform.modifyFunction(functionName,
                                                 Optional.of(functionType),
                                                 Optional.of(functionContent),
                                                 Optional.empty(),
                                                 isPublic);
    }

    /**
     * Update the name of the function
     * @param updatedFunctionName new function name
     */
    public void updateName(String updatedFunctionName) throws Exception {
        registrySession.transform.modifyFunction(functionName,
                                                 Optional.empty(),
                                                 Optional.empty(),
                                                 Optional.of(updatedFunctionName),
                                                 isPublic);
        functionName = updatedFunctionName;
    }

    /**
     * Delete function
     */
    public void delete() throws Exception {
        registrySession.transform.deleteFunction(functionName, isPublic);
    }


    /**
     * Initialise a new registry function handler
     * @param functionName name of the function
     * @param isPublic public function
     * @param manager reference to storage manager
     */
    public RegistryFunctionHandler(String functionName, boolean isPublic, ThingzoneStorageManager manager) {
        this.functionName = functionName;
        this.isPublic = isPublic;
        if (manager.getUserContext().isPresent()) {
            this.registrySession = RegistryAPIDriver.getUserSession(
                    manager.getUserContext().get().getUsername(),
                    manager.getUserContext().get().getPassword()
                                                                   );
        } else {
            this.registrySession = RegistryAPIDriver.getSession();
        }

    }
}
