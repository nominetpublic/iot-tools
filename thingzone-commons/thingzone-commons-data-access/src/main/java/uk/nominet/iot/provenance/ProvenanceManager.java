/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.provenance;

import com.google.gson.JsonObject;
import org.openprovenance.prov.interop.InteropFramework;
import org.openprovenance.prov.model.*;
import org.openprovenance.prov.model.Attribute.AttributeKind;
import uk.nominet.iot.annotations.ProvActivity;
import uk.nominet.iot.annotations.ProvAttribute;
import uk.nominet.iot.annotations.ProvEntity;
import uk.nominet.iot.annotations.ProvId;
import uk.nominet.iot.json.JsonStringSerialiser;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Objects.requireNonNull;

public class ProvenanceManager {
    public final ProvFactory pFactory = InteropFramework.newXMLProvFactory();
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
    public final Namespace ns;
    private final String prefix;


    public ProvenanceManager(String prefix) {
        this.prefix = prefix;
        ns = new Namespace();
        ns.register("prov", "http://www.w3.org/ns/prov#");
        ns.register("xsd", "http://www.w3.org/2001/XMLSchema#");
        ns.register("thingzone", "https://www.yourdomain.uk/thingzone#");
    }


    public Document newDocument() {
        Document provDocument = pFactory.newDocument();
        provDocument.setNamespace(ns);
        return provDocument;
    }

    public String getDefaultPrefix() {
        return prefix;
    }

    /**
     * Generate prov json from annotated java object
     * @param object java object
     * @return prov json
     */
    public StatementOrBundle toProvStatement(Object object) throws Exception {
        return toProvStatement(object, null);
    }

    /**
     * Generate prov json from annotated java object
     * @param object java object
     * @param id the id of the object
     * @return prov json
     */
    public StatementOrBundle toProvStatement(Object object, QualifiedName id) throws Exception {

        Class<?> objectClass = requireNonNull(object).getClass();
        StatementOrBundle statement;
        if (objectClass.isAnnotationPresent(ProvEntity.class)) {
            statement = pFactory.newEntity((id == null) ? qn(prefix,"") : id);


            ((Entity) statement)
                    .getType()
                    .add(pFactory.newType(
                            "thingzone:" + objectClass.getAnnotation(ProvEntity.class).type().name(),
                            qn("xsd", "QName")));


        } else if (objectClass.isAnnotationPresent(ProvActivity.class)) {
            statement = pFactory.newActivity((id == null) ? qn(prefix,"") : id);
            ((Activity) statement)
                    .getType()
                    .add(pFactory.newType(
                            "thingzone:" + objectClass.getAnnotation(ProvActivity.class).type().name(),
                            qn("xsd", "QName")));
        } else {
            throw new Exception("The class " + objectClass.getName() + " does not contain a @Prov... annotation");
        }


        for (Field field : getAllFields(new ArrayList<>(), objectClass)) {
            field.setAccessible(true);

            Object fieldValue = field.get(object);

            if (field.isAnnotationPresent(ProvId.class) && id == null) {
                ((Identifiable) statement).setId(qn(prefix, fieldValue.toString()));
            }

            if (field.isAnnotationPresent(ProvAttribute.class)) {
                ProvAttribute attribute = field.getAnnotation(ProvAttribute.class);

                AttributeKind kind = attribute.attribute();

                QualifiedName attributeType = null;

                if (!attribute.prefix().equals("[unassigned]") && !attribute.n().equals("[unassigned]")) {
                    attributeType = qn(attribute.prefix(), attribute.n());
                }

                if (fieldValue != null) {
                    switch (kind) {
                        case PROV_LABEL:
                            ((HasLabel) statement)
                                    .getLabel()
                                    .add(pFactory.newInternationalizedString(fieldValue.toString()));
                            break;
                        case PROV_LOCATION:
                            ((HasLocation) statement)
                                    .getLocation()
                                    .add(pFactory.newLocation(jsonStringSerialiser.writeObject(fieldValue), attributeType));
                            break;
                        case PROV_ROLE:
                            ((HasRole) statement)
                                    .getRole()
                                    .add(pFactory.newRole(jsonStringSerialiser.writeObject(fieldValue), attributeType));
                            break;
                    }
                }
            }
        }

//        Document provDocument = pFactory.newDocument();
//
//        provDocument.getStatementOrBundle()
//                    .addAll(Arrays.asList(new StatementOrBundle[]{statement}));
//        provDocument.setNamespace(ns);
//
//        document = documentToJson(provDocument);


        return statement;
    }

    /**
     * Get all of the object fields including the one declared in superclasses
     * @param fields fields
     * @param type type
     * @return List of fields
     */
    private List<Field> getAllFields(List<Field> fields, Class<?> type) {
        fields.addAll(Arrays.asList(type.getDeclaredFields()));

        if (type.getSuperclass() != null) {
            getAllFields(fields, type.getSuperclass());
        }

        return fields;
    }


    public QualifiedName qn(String prefix, String n) {
        return ns.qualifiedName(prefix, n, pFactory);
    }

    /**
     * Convert prov document to PROV-JSON
     * @param document Prov document
     * @return PROV-JSON
     */

    public static JsonObject documentToJson(Document document) {
        InteropFramework interopFramework = new InteropFramework();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        interopFramework.writeDocument(os, InteropFramework.ProvFormat.JSON, document);

        return jsonStringSerialiser
                       .getDefaultSerialiser().fromJson(os.toString(), JsonObject.class);

    }

    public void addToProvenanceRecord(Document document) {
    }


}
