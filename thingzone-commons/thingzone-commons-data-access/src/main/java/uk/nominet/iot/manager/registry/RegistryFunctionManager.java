/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.manager.registry;

import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.error.DataStorageException;
import uk.nominet.iot.manager.ThingzoneStorageManager;
import uk.nominet.iot.manager.UserContext;
import uk.nominet.iot.registry.RegistryAPISession;

import java.util.Optional;

public class RegistryFunctionManager extends ThingzoneStorageManager {

    public RegistryFunctionManager(Optional<UserContext> userContext) {
        super(userContext);
    }

    /**
     * Create a new registry function and attach an hadler to it
     * @param functionName    name of the function
     * @param functionType    type of function e.g. javascript
     * @param functionContent content of the function
     * @param isPublic        function is public
     * @return registry function handler
     */
    public RegistryFunctionHandler create(String functionName, String functionType, String functionContent, boolean isPublic) throws Exception {
        RegistryAPISession registrySession = RegistryAPIDriver.getSession();

        if (getUserContext().isPresent()) {
            registrySession = RegistryAPIDriver.getUserSession(
                    getUserContext().get().getUsername(),
                    getUserContext().get().getPassword()
                                                              );
        }

        registrySession.transform.createFunction(functionName, functionType, functionContent, isPublic);
        return new RegistryFunctionHandler(functionName, isPublic, this);
    }


    public RegistryFunctionHandler function(String functionName, boolean isPublic) {
        return new RegistryFunctionHandler(functionName, isPublic, this);
    }


    /**
     * Create new manager
     * @return function manager
     */

    public static RegistryFunctionManager createManager() {
        return new RegistryFunctionManager(Optional.empty());
    }

    /**
     * Create new manager
     * @return function manager
     */

    public static RegistryFunctionManager createManager(UserContext userContext) {
        return new RegistryFunctionManager(Optional.of(userContext));
    }


}
