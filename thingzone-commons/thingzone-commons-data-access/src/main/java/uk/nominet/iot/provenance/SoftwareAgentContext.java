/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.provenance;

import com.google.gson.JsonObject;
import org.openprovenance.prov.interop.InteropFramework;
import org.openprovenance.prov.model.*;
import uk.nominet.iot.driver.ProvsvrDriver;
import uk.nominet.iot.json.JsonStringSerialiser;

import java.io.ByteArrayOutputStream;
import java.time.Instant;
import java.util.Arrays;

public class SoftwareAgentContext {
    public final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
    public ProvenanceManager pm;
    private Agent agent;

    public SoftwareAgentContext(ProvenanceManager pm) {
        this.pm = pm;
    }

    public Agent getAgent() {
        return agent;
    }

    public void register(Class klass, String version, Instant startupTime) throws Exception {
        String agentId = String.format("%s-%d-%s", klass.getSimpleName(), startupTime.getEpochSecond(), version);
        QualifiedName agentName = pm.qn(pm.getDefaultPrefix(), agentId);
        Agent agent = pm.pFactory.newAgent(agentName, klass.getSimpleName());

        agent.getOther().add(pm.pFactory.newOther(pm.qn("thingzone", "version"), version ,pm.qn("xsd","string")));
        agent.getOther().add(pm.pFactory.newOther(pm.qn("thingzone", "startupTime"), startupTime ,pm.qn("xsd","dateTime")));
        agent.getType().add(pm.pFactory.newType("BridgeSoftwareAgent", pm.qn("thingzone","agentType")));

        Document provDocument = pm.pFactory.newDocument();
        provDocument.setNamespace(pm.ns);

        provDocument.getStatementOrBundle()
                    .addAll(Arrays.asList(new StatementOrBundle[]{agent}));
        provDocument.setNamespace(pm.ns);

        InteropFramework interopFramework = new InteropFramework();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        interopFramework.writeDocument(os, InteropFramework.ProvFormat.JSON, provDocument);

        JsonObject doc = jsonStringSerialiser
                                 .getDefaultSerialiser().fromJson(os.toString(), JsonObject.class);

        ProvsvrDriver.getAPIClient().agent.create(agent.getId().getPrefix(), agent.getId().getLocalPart(), doc, "nomnom");


        this.agent = agent;

    }
}
