/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.http;

import com.google.gson.JsonObject;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.model.api.ResponseBuilder;
import uk.nominet.iot.model.registry.RegistryUser;
import uk.nominet.iot.registry.RegistryAPISession;
import uk.nominet.iot.registry.exceptions.RegistryFailResponseException;
import org.json.JSONException;
import org.json.JSONObject;
import spark.Service;

import java.util.List;

/**
 * Manages thingzone registry session key introspection for spark APIs
 */
public class ThingzoneRegistrySessionKeyIntrospector {
    //static final Logger log = LogManager.getLogger(ThingzoneRegistrySessionKeyIntrospector.class.getName());

    private RegistryAPISession registryAPI;
    private boolean testMode;
    private String testUser;
    private List<String> excludePath;


    /**
     * Initialise session key introspector
     * @param testMode    Enable test mode
     * @param testUser    test username
     * @param excludePath exclude paths for key introspection
     */
    public ThingzoneRegistrySessionKeyIntrospector(boolean testMode,
                                                   String testUser,
                                                   List<String> excludePath) throws JSONException {
        this.testMode = testMode;
        this.testUser = testUser;
        this.excludePath = excludePath;

        if (testMode) {
            System.out.println("--------------------------------------------------------------------------------");
            System.out.println("      WARNING: API RUNNING IN TEST MODE WITHOUT REGISTRY SECURITY");
            System.out.println("      Username: " + testUser);
            System.out.println("--------------------------------------------------------------------------------");
        } else {
            this.registryAPI = RegistryAPIDriver.getSession();
        }
    }

    /**
     * Attach introspector to spark http service
     * @param http spark http service
     */
    public void attachTo(Service http) {
        http.before("/*", (request, response) -> {
            if (excludePath.contains(request.pathInfo()))
                return;

            if (!request.requestMethod().equals("OPTIONS")) {
                if (testMode) {
                    request.attribute("introspected_username", testUser);
                } else {
                    String introspectiveKey = request.headers("X-Introspective-Session-Key");
                    if (introspectiveKey == null) {

                        JsonObject responseObj = new ResponseBuilder(false)
                                                         .setReason("Require introspective session key")
                                                         .build();

                        response.type("application/json");
                        http.halt(401, responseObj.toString());
                    }

                    RegistryUser user = null;
                    try {
                        user = registryAPI.internal.getUsername(introspectiveKey);
                    } catch (RegistryFailResponseException e) {
                        //log.warn(e.getMessage());
                        System.out.println("Error 123: " + e.getMessage());
                        System.out.println(registryAPI.getSession().getUser());

                        JSONObject message = new JSONObject(e.getMessage());
                        message.remove("request");
                        response.type("application/json");
                        JsonObject responseObj = new ResponseBuilder(false)
                                                         .setReason(message.getString("reason"))
                                                         .build();
                        http.halt(401, responseObj.toString());
                    }

                    if (user == null || user.getUsername() == null) {
                        JsonObject responseObj = new ResponseBuilder(false)
                                                         .setReason("Session key not recognised")
                                                         .build();
                        response.type("application/json");
                        http.halt(401, responseObj.toString());
                    } else {
                        request.attribute("introspected_username", user.getUsername());
                    }
                }
            }
        });
    }
}
