/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.manager.registry;

import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.manager.ThingzoneStorageManager;
import uk.nominet.iot.registry.RegistryAPISession;

import java.util.List;

public class RegistryGroupHandler {
    private String groupName;
    private RegistryAPISession registrySession;

    /**
     * Get the user in the group
     * @return registry user
     */
    public List<String> users() throws Exception {
        return registrySession.groups.listUsers(groupName);
    }

    /**
     * Add a list of usernames to the group.
     * @param usernames List of usernames
     */
    public void addUsers(String[] usernames) throws Exception {
        registrySession.groups.addUsers(groupName, usernames);
    }

    /**
     * Remove a list of usernames to the group.
     * @param usernames List of usernames
     */
    public void removeUsers(String[] usernames) throws Exception {
        registrySession.groups.removeUsers(groupName, usernames);
    }


    /**
     * Adds or removes admin status from a user in the group.
     * @param username the username of a user in the group
     * @param admin whether they should be admin
     */
    public void changeAdmin(String username, boolean admin) throws Exception {
        registrySession.groups.changeAdminStatus(groupName, username, admin);
    }

    /**
     * Initialise a registry user handler
     * @param groupName name of the group
     * @param manager reference to the storage manager
     */
    public RegistryGroupHandler(String groupName, ThingzoneStorageManager manager) {
        this.groupName = groupName;
        if (manager.getUserContext().isPresent()) {
            this.registrySession = RegistryAPIDriver.getUserSession(
                    manager.getUserContext().get().getUsername(),
                    manager.getUserContext().get().getPassword()
            );
        } else {
            this.registrySession = RegistryAPIDriver.getSession();
        }
    }
}
