/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.manager.registry;

import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.manager.ThingzoneStorageManager;
import uk.nominet.iot.manager.UserContext;
import uk.nominet.iot.registry.RegistryAPISession;

import java.util.Optional;

public class RegistryEntityManager extends ThingzoneStorageManager {

    public RegistryEntityManager(Optional<UserContext> userContext) {
        super(userContext);
    }

    /**
     * Create a new registry entity and attach an hadler to it

     * @return registry function handler
     */
    public RegistryEntityHandler create(Optional<String> key, Optional<String> name, Optional<String> type) throws Exception {
        RegistryAPISession registrySession = RegistryAPIDriver.getSession();

        if (getUserContext().isPresent()) {
            registrySession = RegistryAPIDriver.getUserSession(getUserContext().get().getUsername(),
                                                               getUserContext().get().getPassword());
        }
        return entity(registrySession.entity.create(key, name, type));
    }


    public RegistryEntityHandler entity(String key) throws Exception {
        return new RegistryEntityHandler(key, this);
    }


    /**
     * Create new manager
     * @return function manager
     */

    public static RegistryEntityManager createManager() {
        return new RegistryEntityManager(Optional.empty());
    }

    /**
     * Create new manager
     * @return function manager
     */

    public static RegistryEntityManager createManager(UserContext userContext) {
        return new RegistryEntityManager(Optional.of(userContext));
    }


}
