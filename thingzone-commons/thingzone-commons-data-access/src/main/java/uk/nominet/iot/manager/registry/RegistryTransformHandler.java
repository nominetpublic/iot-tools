/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.manager.registry;

import com.google.gson.JsonObject;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import org.apache.commons.beanutils.BeanUtils;
import uk.nominet.iot.annotations.TzCollection;
import uk.nominet.iot.common.Taxonomy;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.elastic.ThingzoneCollectionIndex;
import uk.nominet.iot.error.DataCachingException;
import uk.nominet.iot.manager.ThingzoneStorageManager;
import uk.nominet.iot.model.registry.RegistryTransform;
import uk.nominet.iot.model.registry.cache.TransformDocument;
import uk.nominet.iot.registry.RegistryAPISession;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class RegistryTransformHandler {
    private String transformKey;
    private RegistryAPISession registrySession;
    private ThingzoneStorageManager manager;
    private final TzCollection thingzoneCollectionAnnotation = RegistryTransform.class.getAnnotation(TzCollection.class);
    private JestClient elasticsearchClient;

    /**
     * Get the registry transfrom
     * @return object representing a registry transfrom
     */
    public RegistryTransform get() throws Exception {
        return registrySession.transform.get(transformKey, true, true);
    }

    /**
     * Update transform metadata
     * @param metadata JsonObject representing the transform metadata
     */
    public void updateMetadata(JsonObject metadata) throws Exception {
        registrySession.datastream.update(transformKey, metadata, Optional.empty(), Optional.empty());
    }

    /**
     * Update the function associated with the transform
     * @param functionName name of the function
     * @param isPublic the function is public
     */
    public void updateFunction(String functionName, boolean isPublic) throws Exception {
        registrySession.transform.updateFunction(transformKey, functionName, isPublic);
    }

    /**
     * Update output streams associated with the transform
     * @param stream key of the data stream
     * @param alias alias for the output
     */
    public void updateOutputStream(String stream, String alias) throws Exception {
        registrySession.transform.updateOutputStream(transformKey, stream, alias);
    }

    /**
     * Update run schedule
     * @param runSchedule run schedule string
     */
    public void updateRunSchedule(String runSchedule) throws Exception {
        registrySession.transform.updateRunSchedule(transformKey, runSchedule);
    }
    /**
     * Update sources associated with transfrom function
     * @param stream the id of the source datastrem
     * @param alias the alias associated with the source
     * @param trigger weather the source is a trigger or not
     * @param aggregationType type of aggregation specification
     * @param aggregationSpec content of the aggregation specification
     */
    public void updateSource(String stream,
                             String alias, boolean trigger,
                             Optional<String> aggregationType,
                             Optional<String> aggregationSpec) throws Exception {
        registrySession.transform.updateSource(transformKey, stream, alias, trigger, aggregationType, aggregationSpec);
    }

    /**
     * Update transfrom function parameter values
     * @param parameterValues a JSONObject containing property names and values of function parameters
     */
    public void updateParameters(JsonObject parameterValues) throws Exception {
        registrySession.transform.updateParameters(transformKey, parameterValues);
    }

    /**
     * Set user permission for transform
     * @param user username
     * @param permissions set of permissions
     */
    public void setUserPermission(String user, String[] permissions) throws Exception {
        registrySession.permission.set(transformKey, Optional.of(user), Optional.empty(), permissions);
    }

    /**
     * Set group permission for transform
     * @param group group
     * @param permissions set of permissions
     */
    public void setGroupPermission(String group, String[] permissions) throws Exception {
        registrySession.permission.set(transformKey, Optional.empty(), Optional.of(group), permissions);
    }

    /**
     * Associate transform with device
     * @param deviceKey device key
     * @param name name of association
     */
    public void addToDevice(String deviceKey, Optional<String> name) throws Exception {
        registrySession.device.addStream(deviceKey, transformKey, name);
    }

    /**
     * Delete transfrom
     */
    public void delete() throws Exception {
        registrySession.transform.delete(transformKey);
    }

    /**
     * Index current transform to elasticsearch
     */
    public void index() throws Exception {
        RegistryTransform transform = get();
        index(transform);
    }

    /**
     * Index transform to elasticsearch
     * @param transform transform instance
     */
    public void index(RegistryTransform transform) throws Exception {
        List<String> indexes = manager.getSubjectsWithAllPermissions(transformKey, thingzoneCollectionAnnotation.findPermissions().split(","));

        for (String indexName : indexes) {
            TransformDocument transformDocument = new TransformDocument();
            BeanUtils.copyProperties(transformDocument, transform);

            // Assign group permission to device document
            if (transform.getGroupPermissions() != null && transform.getGroupPermissions().containsKey(indexName)) {
                transformDocument.setPermissions(transform.getGroupPermissions().get(indexName));
            }

            // Assign user permission to device document
            if (transform.getUserPermissions() != null && transform.getUserPermissions().containsKey(indexName)) {
                transformDocument.setPermissions(transform.getUserPermissions().get(indexName));
            }

            // Remove metadata property if user does not have permissions
            if (!manager.hasAllPermissions(transformKey, new String[]{Taxonomy.PERMISSION_VIEW_METADATA})) {
                transformDocument.setMetadata(null);
            }

            try {
                ThingzoneCollectionIndex indexDocument =
                        new ThingzoneCollectionIndex.IndexBuilder(transformDocument, indexName).build();
                JestResult result = elasticsearchClient.execute(indexDocument);
                if (!result.isSucceeded())
                    throw new DataCachingException("Could not index transform: " + result.getErrorMessage());
            } catch (DataCachingException | IOException e) {
                throw new DataCachingException("Could not index document", e);
            }
        }
    }

    /**
     * Initialise a nre registry transfrom handler
     * @param transformKey key of the transform to handle
     * @param manager reference to the storage manager
     */
    RegistryTransformHandler(String transformKey, ThingzoneStorageManager manager) {
        this.transformKey = transformKey;

        elasticsearchClient = ElasticsearchDriver.getClient();

        if (manager.getUserContext().isPresent()) {
            this.registrySession = RegistryAPIDriver.getUserSession(
                    manager.getUserContext().get().getUsername(),
                    manager.getUserContext().get().getPassword()
                                                                   );
        } else {
            this.registrySession = RegistryAPIDriver.getSession();
        }
        this.manager = manager;
    }
    
}
