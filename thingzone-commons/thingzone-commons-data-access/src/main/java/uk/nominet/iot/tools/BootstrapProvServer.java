/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.tools;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.commons.io.IOUtils;
import org.openprovenance.prov.interop.InteropFramework;
import org.openprovenance.prov.model.Agent;
import org.openprovenance.prov.model.Document;
import org.openprovenance.prov.model.QualifiedName;
import uk.nominet.iot.config.ProvsvrConstant;
import uk.nominet.iot.config.ProvsvrDriverConfig;
import uk.nominet.iot.driver.ProvsvrDriver;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.provenance.ProvenanceManager;
import uk.nominet.iot.utils.AnsiChar;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collections;

public class BootstrapProvServer {
    private static final ProvenanceManager pm = new ProvenanceManager("thingxone");
    public static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    public static void main(String args[]) throws Exception {
        if (args.length != 1) {
            System.out.println("Usage: BootstrapProvServer config.json ");
            System.exit(1);
        }

        InputStream jobInputStream = new FileInputStream(args[0]);
        String jsonTxt = IOUtils.toString(jobInputStream, StandardCharsets.UTF_8);
        JsonObject job = new Gson().fromJson(jsonTxt, JsonObject.class);


        JsonObject connection = job.getAsJsonObject("connection");

        // Init provsvr driver


        if (!ProvsvrDriver.isInitialized()) {
            ProvsvrDriverConfig provsvrConfig = new ProvsvrDriverConfig();
            provsvrConfig.addProperty(ProvsvrConstant.PROVSVR_SERVER_SCHEME, connection.get("scheme").getAsString());
            provsvrConfig.addProperty(ProvsvrConstant.PROVSVR_SERVER_HOST, connection.get("host").getAsString());
            provsvrConfig.addProperty(ProvsvrConstant.PROVSVR_SERVER_PORT, connection.get("port").getAsInt());
            ProvsvrDriver.setConnectionProperties(provsvrConfig);
            ProvsvrDriver.globalInitialise();
        }

        createPrefixes(job.getAsJsonObject("prefixes"));
        createAgents(job.getAsJsonArray("agents"));




    }

    private static void createAgents(JsonArray agents) {
        System.out.println("--------------------------------------------------------------------------------");
        System.out.println("                         CREATE AGENTS");
        System.out.println("--------------------------------------------------------------------------------");

        agents.forEach(element -> {
            JsonObject id = element.getAsJsonObject().getAsJsonObject("id");
            String type = element.getAsJsonObject().get("type").getAsString();
            String label = element.getAsJsonObject().get("label").getAsString();


            QualifiedName agentName = pm.qn(id.get("prefix").getAsString(), id.get("name").getAsString());

            System.out.print("Agent: " + AnsiChar.ANSI_BLUE + agentName + AnsiChar.ANSI_RESET);

            Agent agent = pm.pFactory.newAgent(agentName, label);

            agent.getType().add(pm.pFactory.newType(type, pm.qn("thingzone","agentType")));

            Document provDocument = pm.pFactory.newDocument();
            provDocument.setNamespace(pm.ns);

            provDocument.getStatementOrBundle()
                        .addAll(Collections.singletonList(agent));
            provDocument.setNamespace(pm.ns);

            InteropFramework interopFramework = new InteropFramework();
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            interopFramework.writeDocument(os, InteropFramework.ProvFormat.JSON, provDocument);

            JsonObject doc = jsonStringSerialiser
                                     .getDefaultSerialiser().fromJson(os.toString(), JsonObject.class);
            System.out.println(AnsiChar.ANSI_GREEN + " OK" + AnsiChar.ANSI_RESET);

            try {
                ProvsvrDriver.getAPIClient().agent.create(agent.getId().getPrefix(), agent.getId().getLocalPart(), doc, "nomnom");
            } catch (Exception e) {
                System.out.println(AnsiChar.ANSI_RED + " FAILED " + e.getMessage() + AnsiChar.ANSI_RESET);
            }


        });
    }

    private static void createPrefixes(JsonObject prefixes) {
        System.out.println("--------------------------------------------------------------------------------");
        System.out.println("                         CREATE PREFIXES");
        System.out.println("--------------------------------------------------------------------------------");
        prefixes.entrySet().forEach( entry -> {
            String prefix = entry.getKey();
            String namespace = entry.getValue().getAsString();
            System.out.print("Prefix: " + AnsiChar.ANSI_BLUE + prefix + AnsiChar.ANSI_RESET);
            System.out.print(" Namespace: " + AnsiChar.ANSI_BLUE + prefix + AnsiChar.ANSI_RESET);

            pm.ns.register(prefix, namespace);

            try {
                ProvsvrDriver.getAPIClient().prefix.create(prefix, namespace);
                System.out.println(AnsiChar.ANSI_GREEN + " OK" + AnsiChar.ANSI_RESET);
            } catch (Exception e) {
                System.out.println(AnsiChar.ANSI_RED + " FAILED " + e.getMessage() + AnsiChar.ANSI_RESET);
            }
        });
    }


}
