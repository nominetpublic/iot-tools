/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.config;

import org.apache.commons.configuration2.CombinedConfiguration;

public class RegistryDriverConfig extends CombinedConfiguration implements DriverConfig {
    private static final int DEFAULT_MAX_ATTEMPTS = 1;

    @Override
    public String getScheme() {
        return this.getString(RegistryConstant.REGISTRY_SERVER_SCHEME);
    }

    @Override
    public String getHost() {
        return this.getString(RegistryConstant.REGISTRY_SERVER_HOST);
    }

    @Override
    public String getPath() {
        return this.getString(RegistryConstant.REGISTRY_SERVER_PATH);
    }

    @Override
    public int getPort() {
        return this.getInt(RegistryConstant.REGISTRY_SERVER_PORT);
    }

    @Override
    public String getUsername() { return this.getString(RegistryConstant.REGISTRY_SERVER_USERNAME); }

    @Override
    public String getPassword() {
        return this.getString(RegistryConstant.REGISTRY_SERVER_PASSWORD);
    }


    public int getMaxAttempts() {
        return (this.containsKey(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS)) ?
                this.getInt(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS) :
                DEFAULT_MAX_ATTEMPTS;
    }
}
