/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.tools.task;

import uk.nominet.iot.model.timeseries.Metric;

public class TimeseriesUploadTask {
    private int id;
    private Class<? extends Metric> metricClass;
    private Metric metricObject;

    public TimeseriesUploadTask(int id, Class<? extends Metric> metricClass, Metric metricObject) {
        this.id = id;
        this.metricClass = metricClass;
        this.metricObject = metricObject;
    }

    public Class<? extends Metric> getMetricClass() {
        return metricClass;
    }

    public void setMetricClass(Class<? extends Metric> metricClass) {
        this.metricClass = metricClass;
    }

    public Metric getMetricObject() {
        return metricObject;
    }

    public void setMetricObject(Metric metricObject) {
        this.metricObject = metricObject;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
