/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.manager.registry;

import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.error.DataStorageException;
import uk.nominet.iot.manager.ThingzoneStorageManager;
import uk.nominet.iot.manager.UserContext;
import uk.nominet.iot.registry.RegistryAPISession;

import java.util.Optional;

public class RegistryDataStreamManager extends ThingzoneStorageManager {


    public RegistryDataStreamManager(Optional<UserContext> userContext) {
        super(userContext);
    }

    /**
     * Creare a new datastream entity at attach a handler to it
     * @param key key of the datastream
     * @param name name of the datastream
     * @param type type of the datastream
     * @return datastream handler
     */
    public RegistryDataStreamHandler create(Optional<String> key, Optional<String> name, Optional<String> type) throws Exception {
        RegistryAPISession registrySession = RegistryAPIDriver.getSession();

        if (getUserContext().isPresent()) {
            registrySession = RegistryAPIDriver.getUserSession(
                    getUserContext().get().getUsername(),
                    getUserContext().get().getPassword()
            );
        }

        String dataStreamKey = registrySession.datastream.create(key, name, type);
        return stream(dataStreamKey);
    }

    /**
     * Attach a data stream handler to an existing data stream
     * @param key data stream key
     * @return data stream handler
     */
    public RegistryDataStreamHandler stream(String key) throws Exception {
        return new RegistryDataStreamHandler(key, this);
    }


    /**
     * Create new manager
     * @return a new instance of the manager
     */

    public static RegistryDataStreamManager createManager() {
        return new RegistryDataStreamManager(Optional.empty());
    }

    /**
     * Create a new manager with user context
     * @param userContext credentials for the manager user context
     * @return a new instance of the manager
     */
    public static RegistryDataStreamManager createManager(UserContext userContext) {
        return new RegistryDataStreamManager(Optional.of(userContext));
    }


}
