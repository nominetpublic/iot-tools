/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.manager.registry;

import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.manager.ThingzoneStorageManager;
import uk.nominet.iot.manager.UserContext;
import uk.nominet.iot.registry.RegistryAPISession;

import java.util.Optional;

public class RegistryDeviceManager extends ThingzoneStorageManager {

    public RegistryDeviceManager(Optional<UserContext> userContext) {
        super(userContext);
    }

    /**
     * Create a new device and attach a handler to it
     * @param key device key
     * @param name device name
     * @param type device type
     * @return registry device handler
     */
    public RegistryDeviceHandler create(Optional<String> key, Optional<String> name, Optional<String> type) throws Exception {
        RegistryAPISession registrySession = RegistryAPIDriver.getSession();

        if (getUserContext().isPresent()) {
            registrySession = RegistryAPIDriver.getUserSession(getUserContext().get().getUsername(),
                                                               getUserContext().get().getPassword());
        }
        return device(registrySession.device.create(key, name, type));
    }

    /**
     * Attach a handler to an existing device
     * @param key device key
     * @return registry device handler
     */
    public RegistryDeviceHandler device(String key) {
        return new RegistryDeviceHandler(key, this);
    }

    /**
     * Create new manager
     * @return an instance of the manager
     */
    public static RegistryDeviceManager createManager() {
        return new RegistryDeviceManager(Optional.empty());
    }

    /**
     * Create a new manager with user context
     * @param userContext credentials for the manager user context
     * @return a new instance of the manager
     */
    public static RegistryDeviceManager createManager(UserContext userContext) {
        return new RegistryDeviceManager(Optional.of(userContext));
    }

}
