/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.elastic;

import io.searchbox.action.BulkableAction;
import io.searchbox.action.SingleResultAbstractDocumentTargetedAction;
import io.searchbox.core.DocumentResult;
import io.searchbox.params.Parameters;
import uk.nominet.iot.annotations.TzTimeseries;
import uk.nominet.iot.error.DataCachingException;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;

import java.util.Collection;

import static uk.nominet.iot.utils.ConversionUtils.asciiToHex;

/**
 * Implements elasticsearch bulkable action for indexing thingzone timeseries points e.g. metrics, images, events, alerts
 */
public class ThingzoneTimeseriesIndex extends SingleResultAbstractDocumentTargetedAction
        implements BulkableAction<DocumentResult> {


    private ThingzoneTimeseriesIndex(IndexBuilder builder) {
        super(builder);
        this.payload = builder.source;
        this.setURI(this.buildURI());
    }

    @Override
    public String getPathToResult() {
        return "ok";
    }

    @Override
    public String getRestMethodName() {
        return this.id != null ? "PUT" : "POST";
    }

    @Override
    public String getBulkMethodName() {
        Collection<Object> opType = this.getParameter("op_type");
        if (opType != null) {
            if (opType.size() > 1) {
                throw new IllegalArgumentException("Expecting a single value for OP_TYPE parameter, you provided: " + opType.size());
            } else {
                return opType.size() == 1 && opType.iterator().next().toString().equalsIgnoreCase("create") ? "create" : "index";
            }
        } else {
            return "index";
        }
    }


    /**
     * Index builder
     */
    public static class IndexBuilder extends Builder<ThingzoneTimeseriesIndex, ThingzoneTimeseriesIndex.Builder> {

        private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
        private final Object source;

        /**
         * Initialise the builder
         * @param point   registry entity to index
         * @param subject user or group
         */
        public IndexBuilder(AbstractTimeseriesPoint point, String subject) throws DataCachingException {
            String json;
            try {
                json = jsonStringSerialiser.writeObject(point);
            } catch (Exception e) {
                throw new DataCachingException("Can not convert entity to json", e);
            }
            this.source = json;
            this.id(point.getId().toString());

            TzTimeseries timeseriesTypeAnnotation = point.getClass().getAnnotation(TzTimeseries.class);

            if (timeseriesTypeAnnotation == null)
                throw new DataCachingException("Can not identify timeseries point type");
            this.type(timeseriesTypeAnnotation.indexType());

            if (point.getStreamKey() == null)
                throw new DataCachingException("Timeseries point should have a stream key");
            this.setParameter(Parameters.PARENT, point.getStreamKey());

            if (subject == null)
                throw new DataCachingException("Must have a valid user");

            String index;
            try {
                index = "thingzone_" + asciiToHex(subject);
            } catch (Exception e) {
                throw new DataCachingException("Can not identify user", e);
            }
            this.index(index);
        }

        /**
         * Build a timeseries index
         * @return timeseries index
         */
        public ThingzoneTimeseriesIndex build() {
            return new ThingzoneTimeseriesIndex(this);
        }

    }
}
