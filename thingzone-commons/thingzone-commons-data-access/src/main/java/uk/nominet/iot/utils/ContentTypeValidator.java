/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.utils;

import com.google.common.net.MediaType;
import uk.nominet.iot.error.InvalidContentTypeException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class ContentTypeValidator {

    @FunctionalInterface
    interface ValidatorFunction <D, R> {
         R apply (D data);
    }

    private static ValidatorFunction<byte[], Boolean> jsonValidator = data -> {
        String dataString = new String(data, StandardCharsets.UTF_8);
        try {
            new JSONObject(dataString);
        } catch (JSONException ex) {
            try {
                new JSONArray(dataString);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    };

    private static ValidatorFunction<byte[], Boolean> trueIfNotEmpty = data -> (data.length > 0);


    private static Map<String, ValidatorFunction<byte[], Boolean>> validators = new HashMap<>();
    static {
        //Add all validators
        validators.put("application/json", jsonValidator);
        validators.put("image/jpeg", trueIfNotEmpty);
    }

    public static void validate(String contentType, byte[] data) throws InvalidContentTypeException {
        MediaType mediaType;
        try {
            mediaType = MediaType.parse(contentType);
        } catch (IllegalArgumentException e) {
            throw new InvalidContentTypeException("Could not parse content type string");
        }

        String type = mediaType.type();
        String subtype = mediaType.subtype();

        Charset encoding = mediaType.charset().or(StandardCharsets.UTF_8);

        if (!encoding.equals(StandardCharsets.UTF_8))
            throw new InvalidContentTypeException("We can only handle UTF-8 encoding");

        String normalisedContentType = String.format("%s/%s", type, subtype);

        if (validators.containsKey(normalisedContentType)) {
          Boolean isValid = validators.get(normalisedContentType).apply(data);
          if (!isValid)
              throw new InvalidContentTypeException("Data is not "+ normalisedContentType);
        } else {
            throw new InvalidContentTypeException("We don't have a validator for "+ normalisedContentType);
        }

    }
}
