/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.manager;

/**
 * Defines the context in which a manager should be operating. When a manager specifies a user context, the manager
 * will perform operations on behalf of the specified user.
 * */
public class UserContext {
    private String username;
    private String password;

    /**
     * Create new user context
     * @param username name of the user
     */
    public UserContext(String username) {
        this.username = username;
    }

    /**
     * Get the name of the user
     * @return username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Get the user password
     * @return oasswird
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set the user password
     * @param password user password
     * @return this user context
     */
    public UserContext setPassword(String password) {
        this.password = password;
        return this;
    }

    /**
     * Set the user name
     * @param username user name
     * @return this user context
     */
    public UserContext setUsername(String username) {
        this.username = username;
        return this;
    }
}
