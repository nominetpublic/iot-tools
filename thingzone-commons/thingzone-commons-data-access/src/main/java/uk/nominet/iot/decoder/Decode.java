/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.decoder;

import java.util.HashMap;

public class Decode {
    public static HashMap<String, PayloadDataDecoder> decoders = new HashMap<>();

    static {
        try {
            attachDecoder("text/plain", StringDecoder.class.getName());
            attachDecoder("application/json", JsonDecoder.class.getName());
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
    }

    public static void attachDecoder(String contentType, String decoderClassName)
            throws ClassNotFoundException, IllegalAccessException, InstantiationException {

        Class<PayloadDataDecoder> decoderClass = (Class<PayloadDataDecoder>) Class.forName(decoderClassName);
        PayloadDataDecoder decoder = decoderClass.newInstance();
        decoders.put(contentType, decoder);
    }

    public static PayloadDataDecoder<?> forContentType(String contentType) throws Exception {
        if (!decoders.containsKey(contentType)) throw new Exception("Content type not recognised");
        return decoders.get(contentType);
    }
}
