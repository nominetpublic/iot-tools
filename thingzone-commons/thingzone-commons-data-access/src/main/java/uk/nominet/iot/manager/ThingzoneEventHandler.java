/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.manager;

import uk.nominet.iot.error.DataCachingException;
import uk.nominet.iot.error.DataStorageException;
import uk.nominet.iot.model.Feedback;
import uk.nominet.iot.model.timeseries.Event;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class ThingzoneEventHandler  extends ThingzoneTimeseriesHandler<Event> {

    /**
     * Initialise a timeseries handler
     * @param streamKey key of the data stream
     * @param manager   reference to storage manager
     */
    public ThingzoneEventHandler(String streamKey, ThingzoneStorageManager manager) throws Exception {
        super(streamKey, Event.class, manager);
    }


    /**
     * Add feedback to event
     * @param id UUID of the event
     * @param feedback feedback to add
     */
    public Event addFeedback(UUID id, Feedback feedback) throws DataStorageException, DataCachingException {
        Event event = getPoint(id);

        if (event == null) throw new DataStorageException("Could not find event with id " + id.toString());

        if (event.getFeedback() == null) event.setFeedback(new ArrayList<>());

        if (feedback.getId() == null) feedback.setId(UUID.randomUUID());

        List<Feedback> feedbackList = event.getFeedback();

        Feedback exists = feedbackList.stream()
                                      .filter(f -> f.getId() != null && f.getId().toString().equals(feedback.getId().toString()))
                                      .findFirst()
                                      .orElse(null);

        if (exists != null) throw new DataStorageException("A feedback with the same id already exists");

        feedbackList.add(feedback);

        //removePoint(id);
        addPoint(event);

        return event;
    }

    /**
     * Remove feedback from event
     * @param eventId UUID of the event
     * @param feedbackId UUID of the feedback
     */
    public Event removeFeedback(UUID eventId, UUID feedbackId, String user) throws DataStorageException, DataCachingException {
        Event event = getPoint(eventId);

        if (event == null) throw new DataStorageException("Could not find event with id " + eventId.toString());

        if (event.getFeedback() != null) {
            boolean removed = event.getFeedback().removeIf(feedbackItem -> feedbackItem.getId() != null &&
                                                                           feedbackItem.getId().toString().equals(feedbackId.toString()) &&
                                                                           feedbackItem.getUser() != null &&
                                                                           feedbackItem.getUser().equals(user));

            if (!removed) throw new DataStorageException("Could not remove feedback with id " + feedbackId.toString());
        }

        addPoint(event);

        return event;

    }



    /**
     * Change severity value
     * @param id UUID of the event
     * @param severity new severity
     */
    public void changeSeverity(UUID id, double severity) throws DataStorageException, DataCachingException {
        Event event = getPoint(id);

        if (event == null) throw new DataStorageException("Could not find event with id " + id.toString());

        event.setSeverity(severity);

        addPoint(event);

    }

    /**
     * Change event status
     * @param id UUID of the event
     * @param status new statusSWS
     */
    public void changeStatus(UUID id, String status) throws DataStorageException, DataCachingException {
        Event event = getPoint(id);

        if (event == null) throw new DataStorageException("Could not find event with id " + id.toString());

        event.setStatus(status);

        addPoint(event);

    }

}
