/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.elastic;

import io.searchbox.action.BulkableAction;
import io.searchbox.action.SingleResultAbstractDocumentTargetedAction;
import io.searchbox.core.DocumentResult;
import io.searchbox.params.Parameters;
import uk.nominet.iot.annotations.TzTimeseries;
import uk.nominet.iot.error.DataCachingException;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;


import static uk.nominet.iot.utils.ConversionUtils.asciiToHex;

public class ThingzoneTimeseriesIndexDelete extends SingleResultAbstractDocumentTargetedAction
        implements BulkableAction<DocumentResult> {


    private ThingzoneTimeseriesIndexDelete(IndexBuilder builder) {
        super(builder);
        this.setURI(this.buildURI());
    }

    @Override
    public String getPathToResult() {
        return "ok";
    }

    @Override
    public String getRestMethodName() {
        return "DELETE";
    }

    @Override
    public String getBulkMethodName() {
        return "delete";
    }


    public static class IndexBuilder
            extends Builder<ThingzoneTimeseriesIndexDelete, ThingzoneTimeseriesIndexDelete.Builder> {

        public IndexBuilder(AbstractTimeseriesPoint point, String user) throws DataCachingException {
            if (point == null || point.getId() == null)
                throw new DataCachingException("Must have a valid id");
            this.id(point.getId().toString());

            if (user == null)
                throw new DataCachingException("Must have a valid user");

            String index;
            try {
                index = "thingzone_" + asciiToHex(user);
            } catch (Exception e) {
                throw new DataCachingException("Can not identify user", e);
            }

            this.index(index);

            TzTimeseries timeseriesTypeAnnotation = point.getClass().getAnnotation(TzTimeseries.class);

            if (timeseriesTypeAnnotation == null)
                throw new DataCachingException("Can not identify timeseries point type");
            this.type(timeseriesTypeAnnotation.indexType());

            if (point.getStreamKey() == null)
                throw new DataCachingException("Timeseries point should have a stream key");
            this.setParameter(Parameters.PARENT, point.getStreamKey());
        }

        public ThingzoneTimeseriesIndexDelete build() {
            return new ThingzoneTimeseriesIndexDelete(this);
        }

    }
}
