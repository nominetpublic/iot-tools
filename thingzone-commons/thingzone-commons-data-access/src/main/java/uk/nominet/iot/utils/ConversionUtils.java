/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.utils;

import java.io.Serializable;

/**
 Created by edoardo on 10/05/2017.
 */
public class ConversionUtils implements Serializable {
    private static final long serialVersionUID = 7160133258216839112L;

    public static String asciiToHex(String asciiString) throws Exception {
        if (asciiString == null) throw new Exception("The string can not be null");
        char[] chars = asciiString.toCharArray();
        StringBuilder hex = new StringBuilder();
        for (char aChar : chars) {
            hex.append(Integer.toHexString(aChar));
        }
        return hex.toString();
    }
}
