/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.manager.registry;

import com.google.gson.JsonObject;
import io.searchbox.client.JestClient;

import io.searchbox.client.JestResult;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.elastic.ThingzoneCollectionIndex;
import uk.nominet.iot.error.DataCachingException;
import uk.nominet.iot.error.DataStorageException;
import uk.nominet.iot.manager.ThingzoneStorageManager;

import uk.nominet.iot.registry.RegistryAPISession;
import org.apache.commons.beanutils.BeanUtils;
import uk.nominet.iot.annotations.TzCollection;
import uk.nominet.iot.model.registry.RegistryDataStream;
import uk.nominet.iot.model.registry.RegistryDevice;
import uk.nominet.iot.model.registry.cache.DataStreamDocument;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class RegistryDataStreamHandler {
    private String key;
    private RegistryAPISession registrySession;
    private ThingzoneStorageManager manager;
    private JestClient elasticsearchClient;
    private final TzCollection thingzoneCollectionAnnotation = RegistryDevice.class.getAnnotation(TzCollection.class);

    /**
     * Get the data stream instance
     * @return data stream
     */
    public RegistryDataStream get() throws Exception {
        return (RegistryDataStream)registrySession.internal.identify(key, true, true);
    }

    /**
     * Update the indexes
     */
    public void index() throws Exception {
        index(get());
    }

    /**
     * Update the indexes
     * @param stream instance of the RegistryDataStream to index
     */
    public void index(RegistryDataStream stream) throws Exception {
        List<String> indexes = manager.getSubjectsWithAnyOfPermissions(key, thingzoneCollectionAnnotation.findPermissions().split(","));

        for (String indexName : indexes) {
            DataStreamDocument streamDocument = new DataStreamDocument();
            BeanUtils.copyProperties(streamDocument, stream);

            // Assign group permission to device document
            if (stream.getGroupPermissions() != null && stream.getGroupPermissions().containsKey(indexName)) {
                streamDocument.setPermissions(stream.getGroupPermissions().get(indexName));
            }

            // Assign user permission to device document
            if (stream.getUserPermissions() != null && stream.getUserPermissions().containsKey(indexName)) {
                streamDocument.setPermissions(stream.getUserPermissions().get(indexName));
            }

            streamDocument.setUserPermissions(null);
            streamDocument.setGroupPermissions(null);

            if (!manager.hasAnyOfPermissions(indexName,
                                             key,
                                             thingzoneCollectionAnnotation.viewMetadataPermissions().split(","))) {
                streamDocument.setMetadata(null);
            }

            try {
                ThingzoneCollectionIndex indexDocument =
                        new ThingzoneCollectionIndex.IndexBuilder(streamDocument, indexName).build();
                JestResult result = elasticsearchClient.execute(indexDocument);
                if (!result.isSucceeded())
                    throw new DataCachingException("Could not index data stream: "+ result.getErrorMessage());
            } catch (DataCachingException | IOException e) {
                throw new DataCachingException("Could not index document", e);
            }
        }
    }

    /**
     * Set the user permissions of the data stream
     * @param user user name
     * @param permissions set of permissions
     */
    public void setUserPermission(String user, String[] permissions) throws Exception {
        registrySession.permission.set(key, Optional.of(user), Optional.empty(), permissions);
    }

    /**
     * Set the group permissions of the data stream
     * @param group group name
     * @param permissions set of permissions
     */
    public void setGroupPermission(String group, String[] permissions) throws Exception {
        registrySession.permission.set(key, Optional.empty(), Optional.of(group),  permissions);
    }

    /**
     * Update the content of the data stream metadata
     * @param metadata new metadata object
     */
    public void updateMetadata(JsonObject metadata) throws Exception {
        registrySession.datastream.update(key, metadata, Optional.empty(), Optional.empty());
    }

    /**
     * Update the content of the data stream metadata
     * @param metadata new metadata object
     */
    public void updateMetadataAndType(JsonObject metadata, String type) throws Exception {
        registrySession.datastream.update(key, metadata, Optional.empty(), Optional.of(type));
    }

    /**
     * Add datastream to device
     * @param deviceKey key of the target device
     * @param name name of the link
     */
    public void addToDevice(String deviceKey, Optional<String> name) throws Exception {
        registrySession.device.addStream(deviceKey, key, name);
    }


    /**
     * Add push subscriber to stream
     * @param method method POST, PUSH
     * @param uri uri
     * @param uriParams uri parameters
     * @param headers headher parameters
     * @param retrySequence retay sequence
     * @throws Exception
     */
    public void addPushSubscriber(String method,
                                  String uri,
                                  Map<String,String> uriParams,
                                  Map<String,String> headers,
                                  String retrySequence) throws Exception {
        registrySession.push.setPushSubscriber(key,method, uri, uriParams, headers, retrySequence);
    }



    /**
     * Get the key of the datastream
     * @return datastream key
     */
    public String getKey() {
        return key;
    }

    /**
     * Initialise a new datastream handler
     * @param key key of the data stream to handle
     * @param manager reference to the storage manager
     */

    RegistryDataStreamHandler(String key, ThingzoneStorageManager manager) {
        this.key = key;
        this.elasticsearchClient = ElasticsearchDriver.getClient();

        if (manager.getUserContext().isPresent()) {
            this.registrySession = RegistryAPIDriver.getUserSession(
                    manager.getUserContext().get().getUsername(),
                    manager.getUserContext().get().getPassword()
            );
        } else {
            this.registrySession = RegistryAPIDriver.getSession();
        }
        this.manager = manager;
    }
}
