/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.config;

import org.apache.commons.configuration2.CombinedConfiguration;

public class CassandraDriverConfig extends CombinedConfiguration implements DriverConfig {

    @Override
    public String getScheme() {
        throw new RuntimeException("Cassandra configuration getScheme not implemented");
    }

    @Override
    public String getHost() {
        return this.getString(CassandraConstant.CASSANDRA_SERVER_HOST);
    }

    @Override
    public String getPath() {
        return this.getString(CassandraConstant.CASSANDRA_SERVER_PATH);
    }

    @Override
    public int getPort() {
        return this.getInt(CassandraConstant.CASSANDRA_SERVER_PORT);
    }

    @Override
    public String getUsername() {
        throw new RuntimeException("Cassandra configuration getUsername not implemented");
    }

    @Override
    public String getPassword() {
        throw new RuntimeException("Cassandra configuration getPassword not implemented");
    }
}
