/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.config;

public interface DriverConfig {
    /**
     * Get teh scheme for the connection to the service e.g. http, https
     * @return scheme
     */
    String getScheme();

    /**
     * Get the host name or ip address of the service
     * @return host
     */
    String getHost();

    /**
     * Get the path
     * @return path
     */
    String getPath();

    /**
     * Get the port
     * @return port
     */
    int getPort();

    /**
     * Get the username fo the connection
     * @return username
     */
    String getUsername();

    /**
     * Get the password for the connection
     * @return password
     */
    String getPassword();
}
