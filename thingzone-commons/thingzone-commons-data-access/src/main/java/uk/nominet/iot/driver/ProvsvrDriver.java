/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.driver;

import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.nominet.iot.config.DriverConfig;
import uk.nominet.iot.config.ProvsvrDriverConfig;
import uk.nominet.iot.provenance.ProvsvrAPI;

import java.lang.invoke.MethodHandles;
import java.net.URI;
import java.net.URISyntaxException;

public class ProvsvrDriver {
    private final static Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private static ProvsvrDriverConfig configuration;
    private static ProvsvrAPI apiClient;
    private static boolean initialized = false;

    /**
     * Set connection properties for the provsvr driver
     * @param configuration driver configuration
     */
    public static void setConnectionProperties(DriverConfig configuration) {
        ProvsvrDriver.configuration = (ProvsvrDriverConfig) configuration;
    }

    /**
     * Initialise the driver globally
     */
    public static void globalInitialise() {
        if (initialized) { // we should initialise exactly once!
            throw new RuntimeException("Provsvr API client already initialised");
        }

        if (apiClient != null) {
            throw new RuntimeException("Provsvr API client already initialised");
        }

        // Build url from configuration
        URIBuilder uriBuilder = new URIBuilder();
        uriBuilder.setScheme(configuration.getScheme());
        uriBuilder.setHost(configuration.getHost());
        uriBuilder.setPort(configuration.getPort());
        URI provsvrUrl;

        try {
            provsvrUrl = uriBuilder.build();
        } catch (URISyntaxException e) {
            LOG.error(e.getMessage());
            throw new RuntimeException("Invalid provsvr URL configuration");
        }

        apiClient = new ProvsvrAPI(provsvrUrl.toString());
        initialized = true;

    }

    /**
     * Check if the driver is initialised
     * @return true/false
     */
    synchronized public static boolean isInitialized() {
        return initialized;
    }

    /**
     * Get the current registry session
     * @return registry api session
     */
    public static ProvsvrAPI getAPIClient() {
        if (!initialized)
            throw new RuntimeException("Provsvr API client not yet initialised");

        if (apiClient == null)
            throw new RuntimeException("Provsvr API client not yet initialised");

        return apiClient;
    }

    /**
     * Globally dispose the driver
     */
    public static void globalDispose() {
        initialized = false;
        apiClient = null;
    }
}
