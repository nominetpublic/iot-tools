/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.manager.registry;

import com.google.gson.JsonObject;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.error.DataStorageException;
import uk.nominet.iot.manager.ThingzoneStorageManager;
import uk.nominet.iot.model.registry.RegistryUser;
import uk.nominet.iot.registry.RegistryAPISession;

import java.util.Optional;


public class RegistryUserHandler {
    private String username;
    private RegistryAPISession registrySession;


    /**
     * Get the registry user
     * @return registry user
     */
    public RegistryUser get() throws Exception {
        return registrySession.user.get(username);
    }

    /**
     * Update user preferences
     * @param preferences Json object with user preferences
     */
    public void updatePreferences(JsonObject preferences) throws Exception {
        if (!registrySession.getSession().getUser().getUsername().equals(username))
            throw new DataStorageException("The user in the registry session does not match the user in the request");

        registrySession.user.update(Optional.of(preferences), Optional.empty());
    }


    /**
     * Update user details
     * @param details Json object with user details
     */
    public void updateDetails(JsonObject details) throws Exception {
        if (!registrySession.getSession().getUser().getUsername().equals(username))
            throw new DataStorageException("The user in the registry session does not match the user in the request");
        registrySession.user.update(Optional.empty(), Optional.of(details));
    }

    /**
     * Set user privileges
     * @param privileges set of privileges
     */
    public void setPrivileges(String[] privileges) throws Exception {
        registrySession.user.setPrivileges(username, privileges);
    }


    /**
     * Initialise a registry user handler
     * @param username name of the user
     * @param manager reference to the storage manager
     */
    RegistryUserHandler(String username, ThingzoneStorageManager manager) {
        this.username = username;
        if (manager.getUserContext().isPresent()) {
            this.registrySession = RegistryAPIDriver.getUserSession(
                    manager.getUserContext().get().getUsername(),
                    manager.getUserContext().get().getPassword()
            );
        } else {
            this.registrySession = RegistryAPIDriver.getSession();
        }
    }
}
