/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.init;

import uk.nominet.iot.config.CassandraDriverConfig;
import uk.nominet.iot.config.CassandraConstant;
import uk.nominet.iot.driver.CassandraDriver;
import uk.nominet.iot.manager.ThingzoneDatabaseMincManager;
import uk.nominet.iot.model.admin.DatabaseMinc;
import uk.nominet.iot.utils.MincsEnumeration;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * Init cassandra database
 */
public class CassandraInitService extends DatabaseInitialiser {
    private static final Logger LOG = LoggerFactory.getLogger(CassandraInitService.class);

    /**
     * Execute cassandra CSQL statements
     * @param statements CSQL statements separated by semicolon
     */
    private void executeStatements(String statements) {
        String[] statementsArray = statements.split(";(?=([^']*'[^']*')*[^']*$)");

        for (String statement : statementsArray) {
//            System.out.println("--------------------------------------------------------------------------------");
//            System.out.println(statement);
//            System.out.println("--------------------------------------------------------------------------------");
            if (statement != null && !statement.equals("")) {
                try {
                    CassandraDriver.getSession().execute(statement);
                } catch (Exception e) {
                    System.err.println("Statement: \"" + statement + "\"");
                    e.printStackTrace();
                }

            }
        }
    }

    /**
     * Initialise cassandra database
     */
    public void init() throws Exception {

        // Define basic database structure
        applyStatementFromResource("cassandra/base.cql");

        // Initialise mincs dao
        ThingzoneDatabaseMincManager mincs = ThingzoneDatabaseMincManager.createManager();

        // Apply mincs
        MincsEnumeration mincsEnum = new MincsEnumeration();
        while (mincsEnum.hasMoreElements()) {
            DatabaseMinc minc = mincsEnum.nextElement();

            if (minc != null) {
                DatabaseMinc dbMinc = mincs.getMincById(minc.getId());

                if (dbMinc == null) {
                    executeStatements(minc.getCql());
                    mincs.addMinc(minc);
                } else {
                    LOG.info("Skipping mink " + minc.getId());
                    System.out.println("Skipping mink " + minc.getId());
                }
            }
        }
    }

    /**
     * Clean cassandra schema
     */
    @Override
    public void clean() throws Exception {
        applyStatementFromResource("cassandra/clean.cql");
    }

    /**
     * Load resource containing CSQL statements and apply it to cassandra database
     * @param resourceName name of the resource
     */
    private void applyStatementFromResource(String resourceName) throws Exception {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(resourceName);
        assert is != null;
        Reader reader = new InputStreamReader(is);
        executeStatements(IOUtils.toString(reader));
    }

    /**
     * Print usage information for init service
     * @param errorMessage error message
     */
    private static void printUsage(String errorMessage) {
        if (errorMessage != null)
            System.out.println("Error: " + errorMessage);
        System.out.println("Usage: CassandraInitService clean|init cassandra_ip cassandra_port");
    }


    public static void main(String[] args) {
        if (args.length != 3) {
            printUsage("wrong number of arguments");
            System.exit(1);
        }

        String task = args[0];

        if (!CassandraDriver.isInitialized()) {
            CassandraDriverConfig config = new CassandraDriverConfig();
            config.addProperty(CassandraConstant.CASSANDRA_SERVER_HOST, args[1]);
            config.addProperty(CassandraConstant.CASSANDRA_SERVER_PORT, Integer.parseInt(args[2]));
            CassandraDriver.setConnectionProperties(config);
            CassandraDriver.globalInitialise();
        }

        CassandraInitService cassandraInitService = new CassandraInitService();

        try {
            cassandraInitService.executeTask(task);
        } catch (Exception e) {
            e.printStackTrace();
            printUsage(e.getMessage());
            System.exit(1);
        } finally {
            System.exit(0);
        }

    }

}
