/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.manager.registry;

import com.github.filosganga.geogson.model.Geometry;
import com.github.filosganga.geogson.model.Point;
import com.google.gson.JsonObject;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import uk.nominet.iot.annotations.TzCollection;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.elastic.ThingzoneCollectionIndex;
import uk.nominet.iot.error.DataCachingException;
import uk.nominet.iot.error.DataStorageException;
import uk.nominet.iot.manager.ThingzoneStorageManager;
import uk.nominet.iot.model.registry.RegistryDevice;
import uk.nominet.iot.model.registry.cache.DeviceDocument;
import uk.nominet.iot.registry.RegistryAPISession;
import org.apache.commons.beanutils.BeanUtils;
import uk.nominet.iot.registry.utils.Upsertable;

import java.io.IOException;
import java.util.*;

public class RegistryDeviceHandler {
    private String key;
    private RegistryAPISession registrySession;
    private ThingzoneStorageManager manager;
    private JestClient elasticsearchClient;
    private final TzCollection tingzoneCollectionAnnotation = RegistryDevice.class.getAnnotation(TzCollection.class);

    /**
     * Get the registry device
     * @return registry device
     */
    public RegistryDevice get() throws Exception {
        return registrySession.device.get(key, true, true); //get full details
    }

    /**
     * Update the indexes
     */
    public void index() throws Exception {
        index(get());
    }

    /**
     * Update the indexes
     * @param device instance of RegistryDevice to index
     */
    public void index(RegistryDevice device) throws Exception {
        List<String> indexes = manager.getSubjectsWithAnyOfPermissions(key, tingzoneCollectionAnnotation.findPermissions().split(","));
        for (String indexName : indexes) {
            DeviceDocument deviceDocument = new DeviceDocument();
            BeanUtils.copyProperties(deviceDocument, device);


            // Assign group permission to device document
            if (device.getGroupPermissions() != null && device.getGroupPermissions().containsKey(indexName)) {
                deviceDocument.setPermissions(device.getGroupPermissions().get(indexName));
            }

            // Assign user permission to device document
            if (device.getUserPermissions() != null && device.getUserPermissions().containsKey(indexName)) {
                deviceDocument.setPermissions(device.getUserPermissions().get(indexName));
            }

            deviceDocument.setUserPermissions(null);
            deviceDocument.setGroupPermissions(null);
            deviceDocument.setDataStreams(null);


            // Remove metadata property if user does not have permissions
            if (!manager.hasAnyOfPermissions(indexName,
                                             key,
                                             tingzoneCollectionAnnotation.viewMetadataPermissions().split(","))) {
                deviceDocument.setMetadata(null);
            }

            // Remove location property is user does not have permissions
            if (!manager.hasAnyOfPermissions(indexName,
                                             key,
                                             tingzoneCollectionAnnotation.viewLocationPermissions().split(","))) {
                deviceDocument.setLocation(null);
            } else if (deviceDocument.getLocation() != null){
                if (deviceDocument.getLocation().type() == Geometry.Type.POINT) {
                    Point point = (Point)deviceDocument.getLocation();
                    deviceDocument.setSinglePosition(new double[]{point.lon(), point.lat()});
                }
            }

            try {
                ThingzoneCollectionIndex indexDocument = new ThingzoneCollectionIndex.IndexBuilder(deviceDocument, indexName).build();
                JestResult result = elasticsearchClient.execute(indexDocument);
                if (!result.isSucceeded())
                    throw new DataCachingException("Could not index device: " + result.getErrorMessage());
            } catch (DataCachingException | IOException e) {
                throw new DataCachingException("Could not index document", e);
            }
        }
    }

    /**
     * Update the content of the device metadata
     * @param metadata new metadata object
     */
    public void updateMetadata(JsonObject metadata) throws Exception {
        registrySession.device.update(key, Optional.of(metadata), Optional.empty(), Optional.empty(), Optional.empty());
    }


    /**
     * Update the device name
     * @param name new name for the
     */
    public void updateName(String name) throws Exception {
        registrySession.device.update(key, Optional.empty(), Optional.empty(), Optional.of(name), Optional.empty());
    }

    /**
     * Update the device type
     * @param type new name for the
     */
    public void updateType(String type) throws Exception {
        registrySession.device.update(key, Optional.empty(), Optional.empty(), Optional.empty(), Optional.of(type));
    }

    /**
     * Link a stream to the device
     * @param streamKey the key of the data stream
     * @param name optional name for the link
     */
    public void addStream(String streamKey, Optional<String> name) throws Exception {
        registrySession.device.addStream(key, streamKey, name);
    }

    /**
     * Remove the link between a data stream and the device
     * @param streamKey the key of the data stream
     */
    public void removeStream(String streamKey) throws Exception {
        registrySession.device.removeStream(key, streamKey);
    }

    /**
     * Set user permissions for device
     * @param user usename
     * @param permissions set of permissions
     */
    public void setUserPermission(String user, String[] permissions) throws Exception {
        registrySession.permission.set(key, Optional.of(user), Optional.empty(), permissions);
    }

    /**
     * Set group permissions for device
     * @param group name of the group
     * @param permissions set of permissions
     */
    public void setGroupPermission(String group, String[] permissions) throws Exception {
        registrySession.permission.set(key, Optional.empty(), Optional.of(group), permissions);
    }

    /**
     * Set the location of a device
     * @param location the location of the device in geojson format
     */
    public void setLocation(Geometry<?> location) throws Exception {
        registrySession.device.update(key, Optional.empty(), Optional.of(location), Optional.empty(), Optional.empty());
    }

    /**
     * Upsert the content of a device
     * @param parts usertable parts
     */

    public void upsert(Upsertable<?>... parts) throws Exception {
        registrySession.device.upsert(key, parts);
    }

    /**
     * Delete a device form the registry
     */
    public void delete() throws Exception {
        registrySession.device.delete(key);
    }

    /**
     * Get the device key
     */
    public String getKey() {
        return key;
    }

    /**
     * Initialise a new registry device handler
     * @param key key of the device to handle
     * @param manager reference to the storage manager
     */
    public RegistryDeviceHandler(String key, ThingzoneStorageManager manager) {
        this.key = key;

        this.elasticsearchClient = ElasticsearchDriver.getClient();

        if (manager.getUserContext().isPresent()) {
            this.registrySession = RegistryAPIDriver.getUserSession(
                    manager.getUserContext().get().getUsername(),
                    manager.getUserContext().get().getPassword()
                                                                   );
        } else {
            this.registrySession = RegistryAPIDriver.getSession();
        }
        this.manager = manager;
    }
}
