/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.driver;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import uk.nominet.iot.config.DriverConfig;
import uk.nominet.iot.config.ElasticsearchDriverConfig;
import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.net.URI;
import java.net.URISyntaxException;

public class ElasticsearchDriver {
    private final static Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private static JestClient esClient;
    private static ElasticsearchDriverConfig configuration;
    private static boolean initialized = false;

    /**
     * Set connection properties for the elasticsearch driver
     * @param configuration driver configuration
     */
    public static void setConnectionProperties(DriverConfig configuration) {
        ElasticsearchDriver.configuration = (ElasticsearchDriverConfig) configuration;
    }


    /**
     * Initialise the driver globally
     */
    public static void globalInitialise() {
        if (initialized) { // we should initialise exactly once!
            throw new RuntimeException("Elasticsearch Database already initialised");
        }

        if (esClient != null) {
            throw new RuntimeException("Elasticsearch Database Client already initialised");
        }

        LOG.info("initialising elasticsearch database");

        // Build url from configuration
        URIBuilder uriBuilder = new URIBuilder();
        uriBuilder.setScheme(configuration.getScheme());
        uriBuilder.setPath(configuration.getPath());
        uriBuilder.setHost(configuration.getHost());
        uriBuilder.setPort(configuration.getPort());
        URI url;

        try {
            url = uriBuilder.build();
        } catch (URISyntaxException e) {
            LOG.error(e.getMessage());
            throw new RuntimeException("Invalid elasticsearch URL configuration");
        }

        // Initialise a new client
        JestClientFactory factory = new JestClientFactory();
        factory.setHttpClientConfig(new HttpClientConfig.Builder(url.toString())
                                            .multiThreaded(true)
                                            .build());

        esClient = factory.getObject();

        initialized = true;

    }

    /**
     * Check if the driver is initialises
     * @return true/false
     */
    synchronized public static boolean isInitialized() {
        return initialized;
    }

    /**
     * Get the current instance of the elasticsearch client
     * @return jest client
     */
    public static JestClient getClient() {
        if (!initialized)
            throw new RuntimeException("Elasticsearch database not yet initialised");

        if (esClient == null)
            throw new RuntimeException("Client not yet initialised");

        return esClient;
    }

    /**
     * Globally dispose of the driver
     */
    public static void globalDispose() {
        esClient.shutdownClient();
        esClient = null;
        initialized = false;
    }

}
