/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.utils;

import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.*;

/**
 * Created by edoardo on 11/05/2017.
 */
public class SchemaUtils implements Serializable {
    private static final long serialVersionUID = 212221030962064273L;

    public static Schema loadSchemaFromResource(String resourceName) throws Exception {

        if (!resourceName.endsWith(".json")) {
            resourceName = resourceName + ".json";
        }

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream("schemas/" + resourceName);
        if (is != null) {
            Reader reader = new InputStreamReader(is);
            JSONObject schemaJSONObject = new JSONObject(new JSONTokener(reader));
            return SchemaLoader.load(schemaJSONObject);
        }
        throw new Exception("Schema resource " + resourceName + " not found");
    }

    public static void validate(JSONObject document, String schemaName) throws Exception {
        JSONObject doc = new JSONObject(new JSONTokener(document.toString()));
        Schema schema = SchemaUtils.loadSchemaFromResource(schemaName);
        schema.validate(doc);
    }
}
