/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.driver;

import uk.nominet.iot.config.DriverConfig;
import uk.nominet.iot.config.RegistryDriverConfig;
import uk.nominet.iot.registry.RegistryAPISession;
import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.net.URI;
import java.net.URISyntaxException;

public class RegistryAPIDriver {
    private final static Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private static RegistryDriverConfig configuration;
    private static RegistryAPISession session;
    private static URI registryUrl;
    private static boolean initialized = false;

    /**
     * Set connection properties for the registry driver
     * @param configuration driver configuration
     */
    public static void setConnectionProperties(DriverConfig configuration) {
        RegistryAPIDriver.configuration = (RegistryDriverConfig) configuration;
    }

    /**
     * Initialise the driver globally
     */
    public static void globalInitialise() {
        if (initialized) { // we should initialise exactly once!
            throw new RuntimeException("Registry API client already initialised");
        }

        if (session != null) {
            throw new RuntimeException("Registry API client already initialised");
        }

        LOG.info("Registry scheme: " + configuration.getScheme());
        LOG.info("Registry host: " + configuration.getHost());
        LOG.info("Registry port: " + configuration.getPort());


        // Build url from configuration
        URIBuilder uriBuilder = new URIBuilder();
        uriBuilder.setScheme(configuration.getScheme());
        uriBuilder.setHost(configuration.getHost());
        uriBuilder.setPort(configuration.getPort());
        registryUrl = null;

        try {
            registryUrl = uriBuilder.build();
        } catch (URISyntaxException e) {

            LOG.error(e.getMessage());
            throw new RuntimeException("Invalid registry URL configuration");
        }


        session = new RegistryAPISession(
                registryUrl.toString(),
                configuration.getUsername(),
                configuration.getPassword(),
                configuration.getMaxAttempts()
        );

        initialized = true;

    }

    /**
     * Check if the driver is initialised
     * @return true/false
     */
    synchronized public static boolean isInitialized() {
        return initialized;
    }

    /**
     * Get the current registry session
     * @return registry api session
     */
    public static RegistryAPISession getSession() {
        if (!initialized)
            throw new RuntimeException("Registry API client not yet initialised");

        if (session == null)
            throw new RuntimeException("Registry API client not yet initialised");

        return session;
    }

    /**
     * Get a new user session
     * @param username name of the user
     * @param password password
     * @return registry api session
     */
    public static RegistryAPISession getUserSession(String username, String password) {
        return new RegistryAPISession(
                registryUrl.toString(),
                username,
                password,
                configuration.getMaxAttempts()
        );
    }

    /**
     * Globally dispose the driver
     */
    public static void globalDispose() {
        initialized = false;
        session = null;
    }
}
