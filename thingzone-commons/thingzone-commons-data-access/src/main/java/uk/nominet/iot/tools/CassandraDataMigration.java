/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.tools;

import com.datastax.driver.core.*;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import uk.nominet.iot.config.*;
import uk.nominet.iot.driver.CassandraDriver;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.error.DataStorageException;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.manager.ThingzoneTimeseriesHandler;
import uk.nominet.iot.manager.ThingzoneTimeseriesManager;
import uk.nominet.iot.model.LatLong;
import uk.nominet.iot.model.timeseries.*;
import uk.nominet.iot.tools.queues.TimeseresUploadQueue;
import uk.nominet.iot.tools.task.TimeseriesUploadTask;

import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Map;
import java.util.UUID;

public class CassandraDataMigration {

    private JSONObject job;
    private Cluster sourceDataCluster;
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    public CassandraDataMigration(JSONObject job) {
        this.job = job;
        JSONObject registryConfigJson = job.getJSONObject("registry");
        JSONObject cassandraConfigJson = job.getJSONObject("cassandra");
        JSONObject elasticsearchConfigJson = job.getJSONObject("elasticsearch");
        JSONObject sourceCassandraConfigJson = job.getJSONObject("sourceCassandra");

        if (!CassandraDriver.isInitialized()) {
            CassandraDriverConfig cassandraConfig = new CassandraDriverConfig();
            cassandraConfig.addProperty(CassandraConstant.CASSANDRA_SERVER_HOST, cassandraConfigJson.getString("host"));
            cassandraConfig.addProperty(CassandraConstant.CASSANDRA_SERVER_PORT, cassandraConfigJson.getInt("port"));
            CassandraDriver.setConnectionProperties(cassandraConfig);
            CassandraDriver.globalInitialise();
        }

        if (!RegistryAPIDriver.isInitialized()) {
            RegistryDriverConfig registryConfig = new RegistryDriverConfig();
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_SCHEME, registryConfigJson.getString("scheme"));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST, registryConfigJson.getString("host"));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT, registryConfigJson.getInt("port"));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_USERNAME,
                    registryConfigJson.getString("admin_username"));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD,
                    registryConfigJson.getString("admin_password"));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS, 3);
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }

        if (!ElasticsearchDriver.isInitialized()) {
            ElasticsearchDriverConfig elasticsearchConfig = new ElasticsearchDriverConfig();
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_SCHEME,
                    elasticsearchConfigJson.getString("scheme"));
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_HOST,
                    elasticsearchConfigJson.getString("host"));
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_PATH,
                    elasticsearchConfigJson.getString("path"));
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_PORT,
                    elasticsearchConfigJson.getInt("port"));
            ElasticsearchDriver.setConnectionProperties(elasticsearchConfig);
            ElasticsearchDriver.globalInitialise();
        }

        sourceDataCluster = Cluster.builder()
                .addContactPoint(sourceCassandraConfigJson.getString("host"))
                .withPort(sourceCassandraConfigJson.getInt("port"))
                .build();
    }

    public void run() throws Exception {

        TimeseresUploadQueue queue = new TimeseresUploadQueue(20,1000000);

        new Thread(() -> {

            int payloadPerSecond;
            int lastValue = 0;

            while (true) {

                int processed = (queue.getTotal() - queue.queueSize());
                payloadPerSecond = (processed - lastValue) / 3;

                System.out.print("\r Q: " + queue.queueSize() + " P: " + (queue.getTotal() - queue.queueSize()) +  "  ("+ payloadPerSecond +" msg/s)                         ");

                lastValue = processed;

                try {
                    Thread.sleep(3000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();


        JSONObject sourceCassandraConfigJson = job.getJSONObject("sourceCassandra");
        int skip = sourceCassandraConfigJson.getInt("skip");

        Session session = sourceDataCluster.connect("thingzone");

        ResultSet results = session.execute("SELECT * FROM metrics WHERE timestamp > '2019-09-01 08:27:22+0000' ALLOW FILTERING;");

        int i = 0;

        ThingzoneTimeseriesManager manager = ThingzoneTimeseriesManager.createManager();

        for (Row row : results) {

            if (i >= skip) {
                //System.out.print("\r Processing: " + i);
                String klassString = "";

                if (row.getColumnDefinitions().contains("klass")) {
                    klassString = row.getString("klass");
                } else {
                    int typeId = row.getByte("metricType");
                    switch (typeId) {
                        case 0:
                            klassString = "uk.nominet.iot.model.timeseries.MetricDouble";
                            break;
                        case 1:
                            klassString = "uk.nominet.iot.model.timeseries.MetricString";
                            break;
                        case 4:
                            klassString = "uk.nominet.iot.model.timeseries.MetricInt";
                            break;
                        case 5:
                            klassString = "uk.nominet.iot.model.timeseries.MetricLatLong";
                            break;
                        default:
                            System.out.println("Could not process metric type " + typeId);
                            break;
                    }
                }

                if (klassString.startsWith("iot.nominet")) {
                    klassString = klassString.replace("iot.nominet", "uk.nominet.iot");
                }


                Class classDefinition = Class.forName(klassString);
                Metric metric = (Metric) classDefinition.newInstance();


                String streamkey = row.getString("streamKey");

                if (job.has("keymappings")) {
                    JSONArray keymappings = job.getJSONArray("keymappings");
                    for (Object mapping : keymappings) {
                        JSONObject mappingJson = (JSONObject)mapping;
                        String key = mappingJson.getString("key");
                        String mapto = mappingJson.getString("mapto");
                        streamkey = streamkey.replace(key, mapto);
                    }
                }

                metric.setStreamKey(streamkey);
                metric.setTimestamp(row.getTimestamp("timestamp").toInstant());
                metric.setId(row.getUUID("id"));

                Type type = new TypeToken<Map<String, Object>>() {}.getType();
                metric.setMetadata(jsonStringSerialiser.getDefaultSerialiser().fromJson(row.getString("metadata"), type));



                switch (klassString) {
                    case "uk.nominet.iot.model.timeseries.MetricInt":
                        ((MetricInt) metric).setValue(row.getInt("t4_value_int"));
                        queue.produce(new TimeseriesUploadTask(i, MetricInt.class, metric));

//                        ThingzoneTimeseriesHandler<MetricInt> thingzoneTimeseriesHandlerInt =
//                                manager.timeseries(metric.getStreamKey(), MetricInt.class);
//                        thingzoneTimeseriesHandlerInt.addPoint((MetricInt) metric);
                        break;
                    case "uk.nominet.iot.model.timeseries.MetricDouble":
                        ((MetricDouble) metric).setValue(row.getDouble("t0_value_double"));
                        queue.produce(new TimeseriesUploadTask(i, MetricDouble.class, metric));

//                        ThingzoneTimeseriesHandler<MetricDouble> thingzoneTimeseriesHandlerDouble =
//                                manager.timeseries(metric.getStreamKey(), MetricDouble.class);
//                        thingzoneTimeseriesHandlerDouble.addPoint((MetricDouble) metric);
                        break;
                    case "uk.nominet.iot.model.timeseries.MetricString":
                        ((MetricString) metric).setValue(row.getString("t1_value_text"));
                        queue.produce(new TimeseriesUploadTask(i, MetricString.class, metric));

//                        ThingzoneTimeseriesHandler<MetricString> thingzoneTimeseriesHandlerString =
//                                manager.timeseries(metric.getStreamKey(), MetricString.class);
//                        thingzoneTimeseriesHandlerString.addPoint((MetricString) metric);
                        break;
                    case "uk.nominet.iot.model.timeseries.MetricLatLong":
                        UDTValue latlogUdt = row.getUDTValue("t5_value_lat_long");
                        LatLong latlong = new LatLong();
                        latlong.setLat(latlogUdt.getDouble("lat"));
                        latlong.setLon(latlogUdt.getDouble("lon"));
                        ((MetricLatLong) metric).setValue(latlong);

                        queue.produce(new TimeseriesUploadTask(i, MetricLatLong.class, metric));
//                        ThingzoneTimeseriesHandler<MetricLatLong> thingzoneTimeseriesHandlerLatLong =
//                                manager.timeseries(metric.getStreamKey(), MetricLatLong.class);
//                        thingzoneTimeseriesHandlerLatLong.addPoint((MetricLatLong) metric);
                        break;
                    default:
                        System.out.println("Could not process " + klassString);
                        break;

                }

            } else {
                System.out.print("\r Skipping: " + i);
            }

            i++;
        }

        if (sourceDataCluster != null) sourceDataCluster.close();
    }

    public static void main(String args[]) throws Exception {
        if (args.length != 1) {
            System.out.println("Usage: CassandraDataMigration config.json");
            System.exit(1);
        }

        InputStream jobInputStream = new FileInputStream(args[0]);
        String jsonTxt = IOUtils.toString(jobInputStream, StandardCharsets.UTF_8);
        JSONObject job = new JSONObject(jsonTxt);

        CassandraDataMigration cassandraDataMigration = new CassandraDataMigration(job);
        cassandraDataMigration.run();
    }
}
