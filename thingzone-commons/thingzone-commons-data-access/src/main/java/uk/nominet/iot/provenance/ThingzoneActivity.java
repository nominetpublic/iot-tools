/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.provenance;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import org.openprovenance.prov.model.*;
import uk.nominet.iot.driver.ProvsvrDriver;
import uk.nominet.iot.error.ProvenanceRecordViolationException;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.Instant;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

public  class ThingzoneActivity {
    private Instant startTime;
    private Instant endTime;
    protected ProvenanceManager pm;

    private Activity activity;
    private boolean existingActivity;
    private List<WasGeneratedBy> wasGeneratedBy;
    private List<WasAssociatedWith> wasAssociatedWith;
    private List<Entity> generatedEntities;
    private List<Used> used;
    private Object processInstance;
    private QualifiedName activityId;


    public ThingzoneActivity(ProvenanceManager pm, Object processInstance) {
        this.pm = pm;
        this.processInstance = processInstance;
        generatedEntities = new ArrayList<>();
        wasGeneratedBy = new ArrayList<>();
        wasAssociatedWith = new ArrayList<>();
        used = new ArrayList<>();
        existingActivity = false;
    }

    public ThingzoneActivity(ProvenanceManager pm, Object processInstance, QualifiedName activityId) {
        this(pm, processInstance);
        this.activityId = activityId;
    }

    public void useExistingActivity(QualifiedName activityId) {
        activity = pm.pFactory.newActivity(activityId);
        startTime = Instant.now();
        existingActivity = true;
    }

    public void start() throws Exception {
        if (activity != null) throw new Exception("activity already initialised");
        if (endTime != null) throw new Exception("activity already completed");
        this.activity = (Activity) pm.toProvStatement(processInstance, activityId);
        startTime = Instant.now();
        setStartTime(startTime);
    }


    public void stop() throws Exception {
        if (endTime != null) throw new Exception("activity already completed");
        if (startTime == null) throw new Exception("activity not started");
        endTime = Instant.now();
        setEndTime(endTime);
    }

    public void stop(Object value, QualifiedName type) {
        activity.getLocation().add(pm.pFactory.newLocation(value, type));
    }


    public void setStartTime(Instant time) throws Exception {
        activity.setStartTime(instantToXMLGregorianCalendar(time));
    }


    public void setEndTime(Instant time) throws Exception {
        activity.setEndTime(instantToXMLGregorianCalendar(time));
    }

    public void addLabel(String label) {
        activity.getLabel().add(pm.pFactory.newInternationalizedString(label));
    }

    public void addOther(String namespace, String local, String prefix, Object value, QualifiedName type) {
        activity.getOther().add(pm.pFactory.newOther(namespace, local, prefix, value, type));
    }

    public void addOther(QualifiedName elementName, Object value, QualifiedName type) {
        activity.getOther().add(pm.pFactory.newOther(elementName, value, type));
    }


    public void addType(Object value, QualifiedName type) {
        activity.getType().add(pm.pFactory.newType(value, type));
    }


    public WasGeneratedBy generated(Object entityObject, String role, Instant time) throws Exception {
        Entity entity = (Entity) pm.toProvStatement(entityObject);
        generatedEntities.add(entity);
        WasGeneratedBy generated = pm.pFactory.newWasGeneratedBy(entity, role, activity);
        generated.setTime(instantToXMLGregorianCalendar(time));
        wasGeneratedBy.add(generated);
        return generated;
    }

    public WasAssociatedWith associatedWith(QualifiedName id, QualifiedName agentId) {
        WasAssociatedWith waw = pm.pFactory.newWasAssociatedWith(id);
        waw.setActivity(activity.getId());
        waw.setAgent(agentId);
        wasAssociatedWith.add(waw);
        return waw;
    }

    public Used used(QualifiedName id) {
        Used usedEntity = pm.pFactory.newUsed(id);
        usedEntity.setActivity(activity.getId());
        used.add(usedEntity);
        return usedEntity;
    }

    public Used used(QualifiedName id, QualifiedName entityId, Instant time) throws DatatypeConfigurationException {
        Used usedEntity = pm.pFactory.newUsed(id);
        usedEntity.setActivity(activity.getId());
        usedEntity.setEntity(entityId);
        usedEntity.setTime(instantToXMLGregorianCalendar(time));
        used.add(usedEntity);
        return usedEntity;
    }


    public Used used(QualifiedName id, Object entityObejct, Instant time) throws Exception {
        Entity entity = (Entity) pm.toProvStatement(entityObejct);
        Used usedEntity = pm.pFactory.newUsed(id);
        usedEntity.setTime(instantToXMLGregorianCalendar(time));
        usedEntity.setActivity(activity.getId());
        usedEntity.setEntity(entity.getId());
        used.add(usedEntity);
        return usedEntity;
    }


    protected XMLGregorianCalendar instantToXMLGregorianCalendar(Instant time) throws DatatypeConfigurationException {
        GregorianCalendar startTimeGreg = new GregorianCalendar();
        startTimeGreg.setTimeInMillis(time.toEpochMilli());
        return DatatypeFactory.newInstance().newXMLGregorianCalendar(startTimeGreg);

    }


    public Document getProvDocument() {
        Document provDocument = pm.newDocument();

        List<StatementOrBundle> statements = new ArrayList<>();
        if (activity != null && !existingActivity) statements.add(activity);

        statements.addAll(wasGeneratedBy);
        statements.addAll(wasAssociatedWith);
        statements.addAll(generatedEntities);
        statements.addAll(used);

        provDocument.getStatementOrBundle()
                    .addAll(statements);

        return provDocument;
    }

    public void commitToProvenanceRecord() throws ProvenanceRecordViolationException {
        JsonObject document = ProvenanceManager.documentToJson(getProvDocument());


        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        System.out.println("--------------------------------------------------------------------------------");
        System.out.println("      COMMITTING TO PROVENANCE RECORD");
        System.out.println("--------------------------------------------------------------------------------");
        System.out.println(gson.toJson(document));
        System.out.println("--------------------------------------------------------------------------------");


        try {
            ProvsvrDriver.getAPIClient().activity.create(activity.getId().getPrefix(), activity.getId().getLocalPart(), document);
        } catch (Exception e) {
            throw new ProvenanceRecordViolationException(e);
        }



    }

}
