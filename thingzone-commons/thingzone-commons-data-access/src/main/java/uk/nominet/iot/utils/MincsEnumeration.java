/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.utils;

import uk.nominet.iot.model.admin.DatabaseMinc;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.time.Instant;
import java.util.Enumeration;

public class MincsEnumeration implements Enumeration<DatabaseMinc> {
    private int index;
    private String mincSequenceId;
    private ClassLoader classloader;
    private String resourcePointer;


    public MincsEnumeration() {
        this.index = 0;
        this.classloader = Thread.currentThread().getContextClassLoader();
        nextResource();
    }

    @Override
    public boolean hasMoreElements() {
        return resourceExists();
    }

    @Override
    public DatabaseMinc nextElement() {
        InputStream is = classloader.getResourceAsStream(resourcePointer);
        DatabaseMinc element = null;

        if (is != null) {
            Reader reader = new InputStreamReader(is);
            try {
                element = new DatabaseMinc();
                element.setId(mincSequenceId);
                element.setTimestamp(Instant.now());
                element.setCql(IOUtils.toString(reader));
            } catch (IOException e) {
                element = null;
            }
        }

        nextResource();
        return element;
    }

    private void nextResource() {
        mincSequenceId = String.format("%03d", index++);
        resourcePointer = String.format("cassandra/minc.%s.cql", mincSequenceId);
    }

    private boolean resourceExists() {
        URL resourceURL = classloader.getResource(resourcePointer);
        return resourceURL != null;
    }
}
