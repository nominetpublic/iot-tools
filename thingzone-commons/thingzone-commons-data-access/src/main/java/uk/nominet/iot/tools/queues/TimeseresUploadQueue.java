/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.tools.queues;

import io.searchbox.core.Bulk;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.elastic.ThingzoneTimeseriesIndex;
import uk.nominet.iot.manager.ThingzoneTimeseriesHandler;
import uk.nominet.iot.manager.ThingzoneTimeseriesManager;
import uk.nominet.iot.model.timeseries.MetricDouble;
import uk.nominet.iot.model.timeseries.MetricInt;
import uk.nominet.iot.model.timeseries.MetricLatLong;
import uk.nominet.iot.model.timeseries.MetricString;
import uk.nominet.iot.tools.task.TimeseriesUploadTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;


public class TimeseresUploadQueue {
    private final BlockingQueue<TimeseriesUploadTask> workQueue;
    private int total;

    public TimeseresUploadQueue(int numWorkers, int workQueueSize) {
        workQueue = new LinkedBlockingQueue<>(workQueueSize);
        ExecutorService service = Executors.newFixedThreadPool(numWorkers);

        total = 0;

        for (int i = 0; i < numWorkers; i++) {
            service.submit(new Worker(workQueue));
        }
    }

    public void produce(TimeseriesUploadTask item) {
        total++;
        try {
            workQueue.put(item);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
            Thread.currentThread().interrupt();
        }
    }

    public int queueSize() {
        return workQueue.size();
    }

    public int getTotal() {
        return total;
    }


    private static class Worker implements Runnable {
        private final BlockingQueue<TimeseriesUploadTask> workQueue;

        public Worker(BlockingQueue<TimeseriesUploadTask> workQueue) {
            this.workQueue = workQueue;
        }

        @Override
        public void run() {
            ThingzoneTimeseriesManager manager;
            manager = ThingzoneTimeseriesManager.createManager();

            List<ThingzoneTimeseriesIndex> bulkIndexList = new ArrayList<>();

            while (!Thread.currentThread().isInterrupted()) {
                try {
                    TimeseriesUploadTask item = workQueue.take();

                    if (item.getMetricClass() == MetricInt.class) {
                        ThingzoneTimeseriesHandler<MetricInt> thingzoneTimeseriesHandlerInt =
                                manager.timeseries(item.getMetricObject().getStreamKey(), MetricInt.class);
                        bulkIndexList.addAll(thingzoneTimeseriesHandlerInt.addPointForBuldIndexing((MetricInt) item.getMetricObject()));
                    }  else if (item.getMetricClass() == MetricDouble.class) {
                        ThingzoneTimeseriesHandler<MetricDouble> thingzoneTimeseriesHandlerInt =
                                manager.timeseries(item.getMetricObject().getStreamKey(), MetricDouble.class);
                        bulkIndexList.addAll(thingzoneTimeseriesHandlerInt.addPointForBuldIndexing((MetricDouble) item.getMetricObject()));
                    } else if (item.getMetricClass() == MetricString.class) {
                        ThingzoneTimeseriesHandler<MetricString> thingzoneTimeseriesHandlerInt =
                                manager.timeseries(item.getMetricObject().getStreamKey(), MetricString.class);
                        bulkIndexList.addAll(thingzoneTimeseriesHandlerInt.addPointForBuldIndexing((MetricString) item.getMetricObject()));
                    } else if (item.getMetricClass() == MetricLatLong.class) {
                        ThingzoneTimeseriesHandler<MetricLatLong> thingzoneTimeseriesHandlerInt =
                                manager.timeseries(item.getMetricObject().getStreamKey(), MetricLatLong.class);
                        bulkIndexList.addAll(thingzoneTimeseriesHandlerInt.addPointForBuldIndexing((MetricLatLong) item.getMetricObject()));
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                    Thread.currentThread().interrupt();
                    break;
                }


                if (bulkIndexList.size() > 1500) {
                    Bulk bulk = new Bulk.Builder()
                            .addAction(bulkIndexList)
                            .build();

                    try {
                        ElasticsearchDriver.getClient().execute(bulk);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        bulkIndexList.clear();
                    }


                }

            }

            Bulk bulk = new Bulk.Builder()
                                .addAction(bulkIndexList)
                                .build();

            try {
                ElasticsearchDriver.getClient().execute(bulk);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
