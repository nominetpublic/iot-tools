/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.manager.registry;

import com.google.gson.JsonObject;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.manager.ThingzoneStorageManager;
import uk.nominet.iot.manager.UserContext;
import uk.nominet.iot.registry.RegistryAPISession;

import java.util.Optional;

public class RegistryTransformManager extends ThingzoneStorageManager {


    public RegistryTransformManager(Optional<UserContext> userContext) {
        super(userContext);
    }

    /**
     * Create a new registry transform and attach a handler to it
     * @param transformKey     transfrom key
     * @param name             transform name
     * @param type             transform type
     * @param functionName     name of the associated function
     * @param functionIsPublic function is public
     * @param parameterValues  parameter values
     * @return a registry transform handler
     */
    public RegistryTransformHandler create(Optional<String> transformKey,
                                           Optional<String> name,
                                           Optional<String> type,
                                           Optional<String> runSchedule,
                                           String functionName,
                                           boolean functionIsPublic,
                                           JsonObject parameterValues) throws Exception {
        RegistryAPISession registrySession = RegistryAPIDriver.getSession();

        if (getUserContext().isPresent()) {
            registrySession = RegistryAPIDriver.getUserSession(
                    getUserContext().get().getUsername(),
                    getUserContext().get().getPassword()
                                                              );
        }

        String key = registrySession.transform.create(functionName, functionIsPublic, parameterValues, transformKey, name, type, runSchedule);
        return new RegistryTransformHandler(key, this);
    }

    public RegistryTransformHandler transform(String transformKey) {
        return new RegistryTransformHandler(transformKey, this);
    }


    /**
     * Create new manager
     * @return transform manager
     */
    public static RegistryTransformManager createManager() {
        return new RegistryTransformManager(Optional.empty());
    }

    /**
     * Create new manager with user context
     * @return transform manager
     */
    public static RegistryTransformManager createManager(UserContext userContext) {
        return new RegistryTransformManager(Optional.of(userContext));
    }


}
