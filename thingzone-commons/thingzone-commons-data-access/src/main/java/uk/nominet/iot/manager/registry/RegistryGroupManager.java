/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.manager.registry;

import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.manager.ThingzoneStorageManager;
import uk.nominet.iot.manager.UserContext;

import java.util.Optional;

public class RegistryGroupManager extends ThingzoneStorageManager {

    public RegistryGroupManager(Optional<UserContext> userContext) {
        super(userContext);
    }

    /**
     * Create a new group and attach a handler to it
     * @param groupName   name of the group
     * @return regustry group handler
     */
    public RegistryGroupHandler create(String groupName) throws Exception {
        RegistryAPIDriver.getSession().groups.create(groupName);
        return group(groupName);
    }


    /**
     * Get a handler for an existing user
     * @param groupName name of the user
     * @return user handler
     */
    public RegistryGroupHandler group(String groupName) {
        return new RegistryGroupHandler(groupName, this);
    }


    /**
     * Create new manager
     * @return user manager
     */
    public static RegistryGroupManager createManager() {
        return new RegistryGroupManager(Optional.empty());
    }

    /**
     * Create new manager with user context
     * @return user manager
     */
    public static RegistryGroupManager createManager(UserContext userContext) {
        return new RegistryGroupManager(Optional.of(userContext));
    }
}
