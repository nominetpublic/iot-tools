/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.test.data;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.json.JSONObject;

import com.github.filosganga.geogson.model.Point;
import com.github.filosganga.geogson.model.positions.SinglePosition;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.core.Index;
import uk.nominet.iot.model.Feedback;
import uk.nominet.iot.model.QualityIndicator;
import uk.nominet.iot.model.timeseries.*;
import uk.nominet.iot.test.utils.DateUtils;

public class TestDataGenerator {

    public static void uploadDeviceElasticsearch(JestClient esClient, String index, String key) throws Exception {
        String deviceJson = "{\n" +
                "  \"key\": \""+key+"\",\n" +
                "  \"metadata\": {\n" +
                "    \"name\": \"TestDevice\",\n" +
                "    \"status\": \"ignore\",\n" +
                "    \"type\": \"device\"\n" +
                "  },\n" +
                "  \"dnsRecords\": [],\n" +
                "  \"userPermissions\": [\n" +
                "    \"ADMIN\",\n" +
                "    \"UPLOAD\",\n" +
                "    \"CONSUME\",\n" +
                "    \"MODIFY\",\n" +
                "    \"ANNOTATE\",\n" +
                "    \"DISCOVER\"\n" +
                "  ]\n" +
                "}";

        Index.Builder indexDeviceBuilder = new Index
                .Builder(deviceJson)
                .index(index)
                .type("device")
                .id(key);

        Index indexDevice = indexDeviceBuilder.build();
        JestResult result = esClient.execute(indexDevice);
        if (!result.isSucceeded()) throw new Exception("Could not upload device "+ result.getErrorMessage());
    }


    public static void uploadStreamElasticsearch(JestClient esClient, String index, String key, String parent) throws Exception {
        String streamJson = "{\n" +
                "  \"key\": \""+key+"\",\n" +
                "  \"metadata\": {\n" +
                "    \"name\": \"TestStream\"\n" +
                "  },\n" +
                "  \"dnsRecords\": [],\n" +
                "  \"userPermissions\": [\n" +
                "    \"ADMIN\",\n" +
                "    \"UPLOAD\",\n" +
                "    \"CONSUME\",\n" +
                "    \"MODIFY\",\n" +
                "    \"ANNOTATE\",\n" +
                "    \"DISCOVER\"\n" +
                "  ]\n" +
                "}";

        Index.Builder indexStreamBuilder = new Index
                .Builder(streamJson)
                .index(index)
                .type("stream")
                //.setParameter(Parameters.PARENT, parent)
                .id(key);

        Index indexStream = indexStreamBuilder.build();
        JestResult result = esClient.execute(indexStream);
        if (!result.isSucceeded()) throw new Exception("Could not upload device "+ result.getErrorMessage());
    }

    public static Event createAlert(String deviceKey, String streamKey, UUID id) {
        Instant date = Instant.now();
        Event alert = new Event();

        alert.setId(id);
        alert.setStreamKey(streamKey);
        alert.setEventType(ThingzoneEventType.ALERT);
        alert.setCategory("maintenance");
        alert.setConfiguration(new JSONObject("{\"config\":\"here\"}").toMap());
        alert.setName("Test alert " + id);

        Feedback feedbackItem = new Feedback();
        feedbackItem.setUser("testUser");
        feedbackItem.setCategory("maintenance");
        feedbackItem.setDescription("testFeedback");
        feedbackItem.setTimestamp(Instant.now());
        feedbackItem.setType("testType");
        List<Feedback> feedback = new ArrayList<>();
        feedback.add(feedbackItem);
        alert.setFeedback(feedback);

        List<String> receivers =  new ArrayList<>();
        receivers.add("testUser");
        alert.setReceivers(receivers);

        alert.setTimestamp(date);
        alert.setLocation(new Point(new SinglePosition(125.6, 10.1, 0.0)));

        QualityIndicator qualityIndicator = new QualityIndicator();
        qualityIndicator.setIndicator("timeliness");
        qualityIndicator.setTimestamp(date);
        qualityIndicator.setValue(0.8);
        List<QualityIndicator> quality = new ArrayList<>();
        quality.add(qualityIndicator);
        alert.setQuality(quality);

        alert.setSeverity(0.5);

        alert.setMetadata(new JSONObject("{\"some metadata\":\"here\"}").toMap());
        return alert;
    }

    public static Event createEvent(String deviceKey, String streamKey, UUID id) {
        Instant date = Instant.now();
        Event event = new Event();
        event.setId(id);
        //event.setDeviceKey(deviceKey);
        event.setStreamKey(streamKey);

        event.setName("Test event " + id);
        event.setDescription("Test event");

        event.setTimestamp(date);
        event.setLocation(new Point(new SinglePosition(125.6, 10.1, 0.0)));

        QualityIndicator qualityIndicator = new QualityIndicator();
        qualityIndicator.setIndicator("timeliness");
        qualityIndicator.setTimestamp(date);
        qualityIndicator.setValue(0.8);
        List<QualityIndicator> quality = new ArrayList<>();
        quality.add(qualityIndicator);
        event.setQuality(quality);

        event.setMetadata(new JSONObject("{\"some metadata\":\"here\"}").toMap());

        event.setTimestamp(date);
        event.setEndTimestamp(date);
        event.setStartTimestamp(date);

        return event;
    }

    public static Image createImage(String deviceKey, String streamKey, UUID id) {
        Image image = new Image();
        Date date = new Date();
        image.setId(id);
        //image.setDeviceKey(deviceKey);
        image.setStreamKey(streamKey);
        image.setImageRef("SOME_IMAGE_REF");

        QualityIndicator qualityIndicator = new QualityIndicator();
        qualityIndicator.setIndicator("timeliness");
        qualityIndicator.setTimestamp(Instant.parse(DateUtils.dateToISO(date)));
        qualityIndicator.setValue(0.8);
        List<QualityIndicator> quality = new ArrayList<>();
        quality.add(qualityIndicator);
        image.setQuality(quality);

        image.setMetadata(new JSONObject("{\"some metadata\":\"here\"}").toMap());

        return image;
    }

    public static Metric createMetric(String deviceKey, String streamKey, UUID id) {
        MetricDouble metric = new MetricDouble();
        metric.setId(id);
        //metric.setDeviceKey(deviceKey);
        metric.setStreamKey(streamKey);
        metric.setValue(50.2);
        metric.setTimestamp(Instant.now());
        QualityIndicator qualityIndicator = new QualityIndicator();
        qualityIndicator.setIndicator("timeliness");
        qualityIndicator.setTimestamp(Instant.now());
        qualityIndicator.setValue(0.8);
        List<QualityIndicator> quality = new ArrayList<>();
        quality.add(qualityIndicator);
        metric.setQuality(quality);

        metric.setMetadata(new JSONObject("{\"some metadata\":\"here\"}").toMap());

        return metric;
    }

}
