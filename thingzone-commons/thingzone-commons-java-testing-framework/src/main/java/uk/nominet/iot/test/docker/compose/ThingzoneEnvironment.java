/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.test.docker.compose;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.output.Slf4jLogConsumer;
import org.testcontainers.containers.wait.strategy.Wait;

import java.io.File;
import java.time.Duration;

public class ThingzoneEnvironment  {
    private static final Logger logger = LoggerFactory.getLogger(ThingzoneEnvironment.class);
    public static final ThingzoneService SERVICE_KAFKA = new ThingzoneService("kafka", 9092);
    public static final ThingzoneService SERVICE_CASSANDRA = new ThingzoneService("cassandra", 9042);
    public static final ThingzoneService SERVICE_ELASTICSEARCH = new ThingzoneService("elasticsearch", 9200);
    public static final ThingzoneService SERVICE_POSTGRES = new ThingzoneService("postgres", 5432);
    public static final ThingzoneService SERVICE_REGISTRY = new ThingzoneService("registry", 8081);
    public static final ThingzoneService SERVICE_NEO4J = new ThingzoneService("neo4j", 7687);
    public static final ThingzoneService SERVICE_PROVSVR = new ThingzoneService("provsvr", 8083);

    public static final String DOCKER_COMPOSE_FILE = "docker_compose_test/docker-compose.yaml";

    private ThingzoneComposeRuntimeContainer container;

    public ThingzoneEnvironment() {
        container = (ThingzoneComposeRuntimeContainer) new ThingzoneComposeRuntimeContainer(new File(DOCKER_COMPOSE_FILE))
                .withLocalCompose(true)
                .withPull(false);
    }

    public ThingzoneEnvironment(String docker_compose_file) {
        container = (ThingzoneComposeRuntimeContainer) new ThingzoneComposeRuntimeContainer(new File(docker_compose_file))
                .withLocalCompose(true)
                .withPull(false);
    }

    public ThingzoneEnvironment withServices(ThingzoneService... services) {
        for (ThingzoneService service: services) {
            container.withExposedService(service.getName(), service.getPort(), Wait.forListeningPort().withStartupTimeout(Duration.ofMinutes(5)));
            container.withLogConsumer(service.getName(), new Slf4jLogConsumer(logger));
        }
        return this;
    }

    public ThingzoneComposeRuntimeContainer getContainer(){
        return container;
    }

}
