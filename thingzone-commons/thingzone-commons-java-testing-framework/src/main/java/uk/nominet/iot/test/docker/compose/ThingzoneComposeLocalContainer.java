/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.test.docker.compose;

import scala.collection.mutable.HashMap;
import uk.nominet.iot.test.errors.TestEnvironmentException;

public class ThingzoneComposeLocalContainer {
    private HashMap<String, Integer> services;

    public ThingzoneComposeLocalContainer(){
        services = new HashMap<>();
    }

    public String getServiceHost(ThingzoneService service) throws TestEnvironmentException {
        if (!services.contains(service.getName()))
            throw new TestEnvironmentException(String.format("Service %s not exposed", service.getName()));

        return "localhost";
    }

    public String getServiceHost(String serviceName, int servicePort) throws TestEnvironmentException {
        if (!services.contains(serviceName))
            throw new TestEnvironmentException(String.format("Service %s not exposed", serviceName));

        return "localhost";
    }

    public Integer getServicePort(ThingzoneService service) throws TestEnvironmentException  {

        if (!services.contains(service.getName()))
            throw new TestEnvironmentException(String.format("Service %s not exposed", service.getName()));

        return service.getPort();
    }

    public Integer getServicePort(String serviceName, int servicePort) throws TestEnvironmentException {
        if (!services.contains(serviceName))
            throw new TestEnvironmentException(String.format("Service %s not exposed", serviceName));

        return servicePort;
    }

    public void withExposedService(String name, int port) {
        services.put(name, port);
    }
}
