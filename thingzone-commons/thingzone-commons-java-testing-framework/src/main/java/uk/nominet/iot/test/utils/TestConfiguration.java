/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.test.utils;

import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

/**
 Created by edoardo on 04/10/2016.
 */
public class TestConfiguration {
    public static JSONObject loadJSONFromResource(String resourceName) throws Exception {

        if (!resourceName.endsWith(".json")) {
            resourceName = resourceName + ".json";
        }

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(resourceName);
        if (is != null) {
            Reader reader = new InputStreamReader(is);
            return new JSONObject(new JSONTokener(reader));
        }
        throw new Exception("Configuration resource " + resourceName + " not found");

    }

    public static String loadStringFromResource(String resourceName) throws Exception {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(resourceName);
        if (is != null) {
            return org.apache.commons.io.IOUtils.toString(is, StandardCharsets.UTF_8);
        }
        throw new Exception("Resource " + resourceName + " not found");

    }
}

