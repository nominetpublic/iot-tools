/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.test.utils;

import pl.allegro.tech.embeddedelasticsearch.EmbeddedElastic;
import pl.allegro.tech.embeddedelasticsearch.PopularProperties;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class EmbeddedElasticUtils {
    public static Map<String,EmbeddedElastic> esNodes = new HashMap<>();

    public static EmbeddedElastic getEmbeddedElastic(String version, int port) {
        try {

            if (esNodes.containsKey(version+port)) {
               return esNodes.get(version+port);
            } else {

                EmbeddedElastic embeddedElastic = EmbeddedElastic.builder()
                        .withElasticVersion(version)
                        .withCleanInstallationDirectoryOnStop(true)
                        .withSetting(PopularProperties.TRANSPORT_TCP_PORT, 9350)
                        .withSetting(PopularProperties.HTTP_PORT, port)
                        .withSetting(PopularProperties.CLUSTER_NAME, "TestClusetr_" + version)
                        .build()
                        .start();

                esNodes.put(version + port, embeddedElastic);
                return embeddedElastic;
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

}
