/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.test.utils;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class CheckPostgres {
    private static final int MAX_ATTEMPTS = 10;
    private static final int DELAY_BETWEEN_ATTEMPTS = 2000;

    public static void main(String[] args) {
        if (args.length != 4) {
            System.out.println("Usage: CheckPostgres host database username password");
            System.exit(1);
        }

        String host = args[0];
        String database = args[1];
        String username = args[2];
        String password = args[3];

        String url = String.format("jdbc:postgresql://%s/%s", host, database);
        Properties props = new Properties();
        props.setProperty("user", username);
        props.setProperty("password", password);

        boolean success = false;
        int attempts = 0;

        while (!success) {
            System.out.println("Connecting to db " + attempts);

            try {
                DriverManager.getConnection(url, props);
                success = true;
                break;
            } catch (Exception e) {
              System.out.println("Error: " + e.getMessage());

            } finally {
                attempts++;
                if (attempts > MAX_ATTEMPTS) break;
                try {
                    Thread.sleep(DELAY_BETWEEN_ATTEMPTS);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        if (!success) {
            System.out.println("Failed to connect to database");
            System.exit(1);
        }
    }
}
