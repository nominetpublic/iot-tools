/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.test.data;

import uk.nominet.iot.model.LatLong;
import uk.nominet.iot.model.QualityIndicator;
import uk.nominet.iot.model.timeseries.MetricDouble;
import uk.nominet.iot.model.timeseries.MetricInt;
import uk.nominet.iot.model.timeseries.MetricLatLong;
import uk.nominet.iot.model.timeseries.MetricString;

import java.time.Instant;
import java.util.*;

public class MetricGenerator {

    public static MetricDouble generateMetricDouble(UUID id, String streamKey, double value) {
        MetricDouble metric = new MetricDouble();
        metric.setId(id);
        metric.setStreamKey(streamKey);
        metric.setTimestamp(Instant.now());
        metric.setValue(value);

        HashMap<String, Object> metadata = new HashMap<>();
        metadata.put("test", "abc");
        metadata.put("test1", 123);
        metadata.put("test2", 20.34);
        metric.setMetadata(metadata);

        QualityIndicator indicator = new QualityIndicator();
        indicator.setTimestamp(Instant.now());
        indicator.setIndicator("timeliness");
        indicator.setValue(0.8);

        QualityIndicator indicator1 = new QualityIndicator();
        indicator1.setTimestamp(Instant.now());
        indicator1.setIndicator("correctness");
        indicator1.setValue(0.6);
        List<QualityIndicator> quality = new ArrayList<>();
        quality.add(indicator);
        quality.add(indicator1);

        metric.setQuality(quality);

        return metric;
    }

    public static MetricInt generateMetricInt(UUID id, String streamKey, int value) {
        MetricInt metric = new MetricInt();
        metric.setId(id);
        metric.setStreamKey(streamKey);
        metric.setTimestamp(Instant.now());
        metric.setValue(value);

        HashMap<String, Object> metadata = new HashMap<>();
        metadata.put("test", "abc");
        metadata.put("test1", 123);
        metadata.put("test2", 20.34);
        metric.setMetadata(metadata);

        QualityIndicator indicator = new QualityIndicator();
        indicator.setTimestamp(Instant.now());
        indicator.setIndicator("timeliness");
        indicator.setValue(0.8);

        QualityIndicator indicator1 = new QualityIndicator();
        indicator1.setTimestamp(Instant.now());
        indicator1.setIndicator("correctness");
        indicator1.setValue(0.6);
        List<QualityIndicator> quality = new ArrayList<>();
        quality.add(indicator);
        quality.add(indicator1);

        metric.setQuality(quality);

        return metric;
    }

    public static MetricString generateMetricString(UUID id, String streamKey, String value) {
        MetricString metric = new MetricString();
        metric.setId(id);
        metric.setStreamKey(streamKey);
        metric.setTimestamp(Instant.now());
        metric.setValue(value);

        HashMap<String, Object> metadata = new HashMap<>();
        metadata.put("test", "abc");
        metadata.put("test1", 123);
        metadata.put("test2", 20.34);
        metric.setMetadata(metadata);

        QualityIndicator indicator = new QualityIndicator();
        indicator.setTimestamp(Instant.now());
        indicator.setIndicator("timeliness");
        indicator.setValue(0.8);

        QualityIndicator indicator1 = new QualityIndicator();
        indicator1.setTimestamp(Instant.now());
        indicator1.setIndicator("correctness");
        indicator1.setValue(0.6);
        List<QualityIndicator> quality = new ArrayList<>();
        quality.add(indicator);
        quality.add(indicator1);

        metric.setQuality(quality);

        return metric;
    }

    public static MetricLatLong generateMetricLatLong(UUID id, String streamKey, double lat, double lon) {
        MetricLatLong metric = new MetricLatLong();
        metric.setId(id);
        metric.setStreamKey(streamKey);
        metric.setTimestamp(Instant.now());
        LatLong latLong = new LatLong();
        latLong.setLat(lat);
        latLong.setLon(lon);
        metric.setValue(latLong);

        HashMap<String, Object> metadata = new HashMap<>();
        metadata.put("test", "abc");
        metadata.put("test1", 123);
        metadata.put("test2", 20.34);
        metric.setMetadata(metadata);

        QualityIndicator indicator = new QualityIndicator();
        indicator.setTimestamp(Instant.now());
        indicator.setIndicator("timeliness");
        indicator.setValue(0.8);

        QualityIndicator indicator1 = new QualityIndicator();
        indicator1.setTimestamp(Instant.now());
        indicator1.setIndicator("correctness");
        indicator1.setValue(0.6);
        List<QualityIndicator> quality = new ArrayList<>();
        quality.add(indicator);
        quality.add(indicator1);

        metric.setQuality(quality);

        return metric;
    }

}
