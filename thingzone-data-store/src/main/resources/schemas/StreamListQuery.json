{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "StreamSearchQuery",
  "description": "The query format accepted by the stream search API endpoint",
  "type": "object",
  "additionalProperties": false,
  "properties": {
    "keyword": {
      "description": "Search terms that ElasticSearch will use to filter results, searching over all fields. Will AND whitespace separated terms together",
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "values": {
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "combination": {
          "$ref": "#/definitions/booleanOperator",
          "default": "or"
        }
      },
      "required": ["values"]
    },
    "outputType": {
      "description": "The output format that should be produced by the API result. Using 'devices' will match the schema used by elasticsearch and uses pagination, whilst 'geojson' will use the GeoJSON output format and returns all results for the current bounding polygon(s), and 'deviceKey' consists only of an array of keys for the matched devices",
      "enum": [
        "streams",
        "streamkeys"
      ],
      "default": "streams"
    },
    "name": {
      "type" : "string"
    },
    "secondaryNames": {
      "type" : "array"
    },
    "devices": {
      "description": "An array of device IDs, where each stream must be associated with at least one of them",
      "type": "array",
      "items": {
        "type": ["number", "string"]
      }
    }
  },
  "definitions": {
    "booleanOperator": {
      "enum": [
        "and", "or"
      ]
    },
    "facetPath": {
      "type": "array",
      "items": {
        "type": "string"
      },
      "minItems": 1
    },
    "facetValue": {
      "type": ["number", "string"]
    },
    "facetRestriction": {
      "description": "A restriction on the values of a facet. Can either be a list of values or a numeric range that the facet specified by the path is tested against. If exclude is true, then this restriction is negated",
      "type": "object",
      "oneOf": [
        {
          "properties": {
            "path": {
              "$ref": "#/definitions/facetPath"
            },
            "exclude": {
              "type": "boolean",
              "default": false
            },
            "values": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/facetValue"
              },
              "minItems": 1
            },
            "combination": {
              "$ref": "#/definitions/booleanOperator",
              "default": "or"
            }
          },
          "additionalProperties": false
        },
        {
          "properties": {
            "path": {
              "$ref": "#/definitions/facetPath"
            },
            "exclude": {
              "type": "boolean",
              "default": false
            },
            "range": {
              "type": "array",
              "items": {
                "type": "number"
              },
              "minItems": 2,
              "maxItems": 2
            }
          },
          "additionalProperties": false
        }
      ]
    },
    "facetRestrictionList": {
      "type": "array",
      "items": {
        "$ref": "#/definitions/facetRestriction"
      }
    },
    "position": {
      "description": "A single position",
      "type": "array",
      "minItems": 2,
      "items": [ { "type": "number" }, { "type": "number" } ],
      "additionalItems": false
    },
    "positionArray": {
      "description": "An array of positions",
      "type": "array",
      "items": { "$ref": "#/definitions/position" }
    },
    "linearRing": {
      "description": "An array of at least four positions where the first equals the last",
      "allOf": [
        { "$ref": "#/definitions/positionArray" },
        { "minItems": 4 }
      ]
    }
  }
}
