/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.core.search.aggregation.TermsAggregation;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.ExistsQueryBuilder;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.search.aggregations.bucket.filter.FilterAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.support.ValueType;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.everit.json.schema.ValidationException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import spark.Request;
import spark.Response;
import spark.Service;
import uk.nominet.iot.api.datastore.model.AnnotatedEvent;
import uk.nominet.iot.api.datastore.model.DataStreamGeo;
import uk.nominet.iot.api.datastore.model.DataStreamValues;
import uk.nominet.iot.api.datastore.query.EventGeoQuery;
import uk.nominet.iot.api.datastore.query.EventListQuery;
import uk.nominet.iot.http.APIController;
import uk.nominet.iot.model.api.ExceptionHandler;
import uk.nominet.iot.api.datastore.errors.IllegalQueryException;
import uk.nominet.iot.api.datastore.utils.ElasticSearchUtils;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.error.DataCachingException;
import uk.nominet.iot.error.DataStorageException;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.manager.ThingzoneEventHandler;
import uk.nominet.iot.manager.ThingzoneEventManager;
import uk.nominet.iot.model.Feedback;
import uk.nominet.iot.model.api.ResponseBuilder;
import uk.nominet.iot.model.timeseries.Event;

import java.io.IOException;
import java.time.Instant;
import java.util.*;

import static uk.nominet.iot.utils.ConversionUtils.asciiToHex;

public class EventController implements APIController {
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
    private static ThingzoneEventManager eventManager = ThingzoneEventManager.createManager();



    @Override
    public void publishAPI(Service http, String path) {
        http.path(path, () -> {
            http.post("/list", this::list);
            http.post("/geo", this::geo);
            http.put("/:streamKey", this::addEvent);
            http.get("/:streamKey/:eventId", this::getEvent);
            http.put("/:streamKey/:eventId/feedback", this::addFeedback);
            http.delete("/:streamKey/:eventId/feedback/:feedbackId", this::removeFeedback);
            http.put("/:streamKey/:eventId/tag/:tag", this::addTag);
            http.delete("/:streamKey/:eventId/tag/:tag", this::removeTag);
            http.put("/:streamKey/:eventId/severity/:severity", this::changeSeverity);
            http.put("/:streamKey/:eventId/status/:status", this::changeStatus);
            http.get("/tags/suggest/:prefix", this::suggestTags);
            http.get("/facets", this::getFacets);
        });
    }

    private String addEvent(Request req, Response res) {
        JSONObject eventJson;
        String streamKey = req.params(":streamKey");
        String user = req.attribute("introspected_username");

        try {
            eventJson = new JSONObject(new JSONTokener(req.body()));
            Event event = jsonStringSerialiser.readObject(eventJson.toString(), Event.class);
            if (event.getId() == null) event.setId(UUID.randomUUID());
            if (event.getTimestamp() == null) event.setTimestamp(Instant.now());

            ThingzoneEventHandler eventHandler = eventManager.timeseries(streamKey);

           eventHandler.addPoint(event);

            return new ResponseBuilder(true)
                           .setResponse(buildEventMessage(event, user))
                           .build().toString();

        } catch (Exception e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }
    }


    /**
     * Handle get facets request
     * @param req http request
     * @param res http response
     * @return response body
     */
    private String getFacets(Request req, Response res) {
        String user = req.attribute("introspected_username");

        JsonObject facets = new JsonObject();
        try {
            facets.add("tags", listFacetsProperty(user, "tags"));
            facets.add("types", listFacetsProperty(user, "type"));
            facets.add("status", listFacetsProperty(user, "status"));
            facets.add("devices", listFacetsProperty(user, "relatedTo.key"));
            facets.add("new", listFacetsForRead(user));
        } catch (Exception e) {
            return ExceptionHandler.handle(res, e);
        }

        return new ResponseBuilder(true)
                       .addElement("facets", facets)
                       .build().toString();
    }

    /**
     * Handle event change status requests
     * @param req http request
     * @param res http response
     * @return response body
     */
    private String changeStatus(Request req, Response res) {
        String eventId = req.params(":eventId");
        String streamKey = req.params(":streamKey");
        String status = req.params(":status");

        ThingzoneEventHandler eventHandler;

        try {
            eventHandler = eventManager.timeseries(streamKey);
            eventHandler.changeStatus(UUID.fromString(eventId), status);

            return handleInternalFeedback(eventHandler,
                                          eventId,
                                          req.attribute("introspected_username"),
                                          "status",
                                          "changed status to " + status);
        } catch (Exception e) {
            return ExceptionHandler.handle(res, e);
        }

    }


    private JsonArray listFacetsForRead(String user) throws Exception {

        String index = ElasticSearchUtils.INDEX_PREFIX + asciiToHex(user);

        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.must(new TermsQueryBuilder("feedback.type", "system"));
        boolQueryBuilder.must(new TermsQueryBuilder("feedback.category", "read"));
        boolQueryBuilder.must(new TermsQueryBuilder("feedback.user", user));

        FilterAggregationBuilder filterAggregationBuilder = new FilterAggregationBuilder("read", boolQueryBuilder);

        ExistsQueryBuilder existsQueryBuilder = new ExistsQueryBuilder("severity");

        SearchSourceBuilder searchBuilder = new SearchSourceBuilder()
                                                    .query(existsQueryBuilder)
                                                    .aggregation(filterAggregationBuilder)
                                                    .size(0);

        Search search = new Search.Builder(searchBuilder.toString())
                                .addIndex(index)
                                .addType(ElasticSearchUtils.EVENT_TYPE)
                                .build();

        SearchResult result = ElasticsearchDriver.getClient().execute(search);
        JsonArray items = new JsonArray();

        if (!result.isSucceeded()) {
            throw new Exception(result.getErrorMessage());
        }


        Long total = result.getTotal();
        Long read = result.getAggregations().getFilterAggregation("read").getCount();

        JsonObject unreadFacet = new JsonObject();
        unreadFacet.addProperty("id", "new");
        unreadFacet.addProperty("value", "new");
        unreadFacet.addProperty("count", total - read);
        items.add(unreadFacet);

        return items;
    }

    /**
     * Build a facet list for a specific property
     * @param user     Username
     * @param property property
     * @return list of facets for properties
     */
    private JsonArray listFacetsProperty(String user, String property) throws Exception {

        String index = ElasticSearchUtils.INDEX_PREFIX + asciiToHex(user);

        TermsAggregationBuilder termsAggregationBuilder = new TermsAggregationBuilder("prop", ValueType.STRING);
        termsAggregationBuilder.field(property + ".keyword");
        termsAggregationBuilder.size(100);

        ExistsQueryBuilder existsQueryBuilder = new ExistsQueryBuilder("severity");

        SearchSourceBuilder searchBuilder = new SearchSourceBuilder()
                                                    .query(existsQueryBuilder)
                                                    .aggregation(termsAggregationBuilder)
                                                    .size(0);

        Search search = new Search.Builder(searchBuilder.toString())
                                .addIndex(index)
                                .addType(ElasticSearchUtils.EVENT_TYPE)
                                .build();

        SearchResult result = ElasticsearchDriver.getClient().execute(search);
        JsonArray items = new JsonArray();

        if (!result.isSucceeded()) {
            throw new Exception(result.getErrorMessage());
        }

        List<TermsAggregation.Entry> entries = result.getAggregations().getTermsAggregation("prop").getBuckets();

        for (TermsAggregation.Entry entry : entries) {
            String value = entry.getKeyAsString();
            JsonObject item = new JsonObject();
            item.addProperty("id", value);
            item.addProperty("value", value);
            item.addProperty("count", entry.getCount());
            items.add(item);
        }

        return items;
    }

    /**
     * Handle request for tags suggestion
     * @param req http request
     * @param res http response
     * @return response body
     */
    private String suggestTags(Request req, Response res) {

        String user = req.attribute("introspected_username");

        String prefix = (req.params(":prefix") != null) ? req.params(":prefix") : "";

        String index;
        try {
            index = ElasticSearchUtils.INDEX_PREFIX + asciiToHex(user);
        } catch (Exception e) {
            return ExceptionHandler.handle(res, e, "Could not identify elasticsearch index");
        }


        Search search = new Search.Builder("{\n" +
                                           " \"_source\": \"suggest\", \n" +
                                           " \"suggest\": {\n" +
                                           "        \"tags\" : {\n" +
                                           "            \"prefix\" : \"" + prefix + "\", \n" +
                                           "            \"completion\" : { \n" +
                                           "                \"field\" : \"tags.suggest\" \n" +
                                           "            }\n" +
                                           "        }\n" +
                                           "    }\n" +
                                           "}")
                                .addIndex(index)
                                .addType(ElasticSearchUtils.EVENT_TYPE)
                                .build();


        SearchResult result;
        try {
            result = ElasticsearchDriver.getClient().execute(search);

            if (!result.isSucceeded()) {
                return new ResponseBuilder(false)
                               .setReason(result.getErrorMessage())
                               .build().toString();
            }


            JsonObject document = result.getJsonObject();


            JsonArray options = document.getAsJsonObject("suggest")
                                        .get("tags")
                                        .getAsJsonArray()
                                        .get(0)
                                        .getAsJsonObject()
                                        .getAsJsonArray("options");

            ArrayList<String> suggestedTags = new ArrayList<>();

            options.forEach(option -> {
                String tag = option.getAsJsonObject().get("text").getAsString();
                String type = option.getAsJsonObject().get("_type").getAsString();
                if (tag != null && !suggestedTags.contains(tag) && type.equals("event"))
                    suggestedTags.add(tag);
            });

            return new ResponseBuilder(true)
                           .addElement("suggestions", new Gson().toJsonTree(suggestedTags))
                           .build().toString();

        } catch (IOException e) {
            return ExceptionHandler.handle(res, e, "Could not send request to elasticsearch");
        }


    }


    /**
     * Append feedback item to event after internal operation
     * @param eventHandler event handler
     * @param eventId      alert id
     * @param user         username for feedback
     * @param category     feedback category
     * @param description  feedback description
     * @return json string with full event
     */
    private String handleInternalFeedback(ThingzoneEventHandler eventHandler,
                                          String eventId,
                                          String user,
                                          String category,
                                          String description)
            throws DataStorageException, DataCachingException {

        Feedback feedback = new Feedback();

        feedback.setTimestamp(Instant.now());
        feedback.setUser(user);
        feedback.setId(UUID.randomUUID());
        feedback.setType("system");
        feedback.setCategory(category);
        feedback.setDescription(description);

        Event event = eventHandler.addFeedback(UUID.fromString(eventId), feedback);

        if (event == null) {
            return new ResponseBuilder(false)
                           .setReason("Could not return event object")
                           .build()
                           .toString();
        }

        return new ResponseBuilder(true)
                       .setResponse(buildEventMessage(event, user))
                       .build()
                       .toString();
    }


    /**
     * Change severity of event message
     * @param req http request
     * @param res http response
     * @return response body
     */
    private String changeSeverity(Request req, Response res) {
        String eventId = req.params(":eventId");
        String streamKey = req.params(":streamKey");
        double severity = Double.parseDouble(req.params(":severity"));

        ThingzoneEventHandler eventHandler;

        try {
            eventHandler = eventManager.timeseries(streamKey);
            eventHandler.changeSeverity(UUID.fromString(eventId), severity);
            return handleInternalFeedback(eventHandler,
                                          eventId,
                                          req.attribute("introspected_username"),
                                          "severity",
                                          "changed severity to " + severity);

        } catch (Exception e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

    }

    /**
     * Method for handling remove tag from event requests
     * @param req http request
     * @param res http response
     * @return response body
     */
    private String removeTag(Request req, Response res) {
        String eventId = req.params(":eventId");
        String streamKey = req.params(":streamKey");
        String tag = req.params(":tag");

        ThingzoneEventHandler eventHandler;

        try {
            eventHandler = eventManager.timeseries(streamKey);
            eventHandler.removeTag(UUID.fromString(eventId), tag);

            return handleInternalFeedback(eventHandler,
                                          eventId,
                                          req.attribute("introspected_username"),
                                          "tags",
                                          "removed tag " + tag);
        } catch (Exception e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }
    }


    /**
     * Method for handling add tag to event requests
     * @param req http request
     * @param res http response
     * @return response body
     */
    private String addTag(Request req, Response res) {
        String eventId = req.params(":eventId");
        String streamKey = req.params(":streamKey");
        String tag = req.params(":tag");

        ThingzoneEventHandler eventHandler;

        try {
            eventHandler = eventManager.timeseries(streamKey);
            eventHandler.addTag(UUID.fromString(eventId), tag);

            return handleInternalFeedback(eventHandler,
                                          eventId,
                                          req.attribute("introspected_username"),
                                          "tags",
                                          "added tag " + tag);
        } catch (Exception e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }
    }


    /**
     * Method for handling remove feedback from event requests
     * @param req http request
     * @param res http response
     * @return response body
     */
    private String removeFeedback(Request req, Response res) {
        String eventId = req.params(":eventId");
        String streamKey = req.params(":streamKey");
        String feedbackId = req.params(":feedbackId");
        String user = req.attribute("introspected_username");

        ThingzoneEventHandler eventHandler;

        try {
            eventHandler = eventManager.timeseries(streamKey);
            Event event = eventHandler.removeFeedback(UUID.fromString(eventId), UUID.fromString(feedbackId), user);
            return new ResponseBuilder(true)
                           .setResponse(buildEventMessage(event, user))
                           .build().toString();
        } catch (Exception e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

    }

    /**
     * Method for handling add feedback to event requests
     * @param req http request
     * @param res http response
     * @return response body
     */
    private String addFeedback(Request req, Response res) {
        JSONObject feedbackJson;
        String eventId = req.params(":eventId");
        String streamKey = req.params(":streamKey");
        String user = req.attribute("introspected_username");

        try {
            feedbackJson = new JSONObject(new JSONTokener(req.body()));
            Feedback feedback = jsonStringSerialiser.readObject(feedbackJson.toString(), Feedback.class);
            feedback.setUser(user);

            ThingzoneEventHandler eventHandler = eventManager.timeseries(streamKey);

            Event event = eventHandler.addFeedback(UUID.fromString(eventId), feedback);

            return new ResponseBuilder(true)
                           .setResponse(buildEventMessage(event, user))
                           .build().toString();

        } catch (Exception e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }


    }


    /**
     * Method for handling get event requests
     * @param req http request
     * @param res http response
     * @return response body
     */
    private String getEvent(Request req, Response res) {
        String eventId = req.params(":eventId");
        String streamKey = req.params(":streamKey");
        String user = req.attribute("introspected_username");

        ThingzoneEventHandler eventHandler;

        try {
            eventHandler = eventManager.timeseries(streamKey);
            Event event = eventHandler.getPoint(UUID.fromString(eventId));
            AnnotatedEvent annotatedEvent = EventListQuery.annotateEvent(event, user);

            if (event == null) {
                return new ResponseBuilder(false)
                               .setReason("Could not fid event with id " + eventId)
                               .build().toString();
            }

            return new ResponseBuilder(true)
                           .setResponse(buildEventMessage(annotatedEvent, user))
                           .build().toString();

        } catch (Exception e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }


    }

    /**
     * Handle list events request
     * @param req http request
     * @param res http response
     * @return response body
     */
    private String list(Request req, Response res) {
        JSONObject queryObject;
        try {
            queryObject = new JSONObject(new JSONTokener(req.body()));
        } catch (JSONException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

        try {
            EventListQuery query = new EventListQuery(queryObject, req.attribute("introspected_username"));
            List<DataStreamValues<AnnotatedEvent>> eventsTimeseries = query.execute();
            ResponseBuilder response = new ResponseBuilder(true)
                                               .setResponse(jsonStringSerialiser.getDefaultSerialiser()
                                                                                .toJsonTree(eventsTimeseries)
                                                                                .getAsJsonArray());
            return response.build().toString();
        } catch (ValidationException e) {
            return ExceptionHandler.handle(res, new IllegalQueryException(e.getKeyword(), queryObject.toString(),
                                                                          e.getErrorMessage()), e.getAllMessages().toString());
        } catch (IllegalQueryException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return ExceptionHandler.handle(res, e, "Unknown server error");
        }
    }

    /**
     * List events given a query
     * @param query query
     * @return List of events json string
     */
    public JsonObject listEvents(EventListQuery query) throws Exception {
        List<DataStreamValues<AnnotatedEvent>> eventsTimeseries = query.execute();
        ResponseBuilder response = new ResponseBuilder(true)
                                           .setResponse(jsonStringSerialiser.getDefaultSerialiser()
                                                                            .toJsonTree(eventsTimeseries)
                                                                            .getAsJsonArray());
        return response.build();
    }

    /**
     * Handle geo events request
     * @param req http request
     * @param res http response
     * @return response body
     */
    private String geo(Request req, Response res) {
        JSONObject queryObject = null;

        try {
            queryObject = new JSONObject(new JSONTokener(req.body()));
            EventGeoQuery query = new EventGeoQuery(queryObject, req.attribute("introspected_username"));
            List<DataStreamGeo<Event>> events = query.execute();

            ResponseBuilder response = new ResponseBuilder(true)
                                               .setResponse(jsonStringSerialiser
                                                       .getDefaultSerialiser()
                                                       .toJsonTree(events.get(0)));

            return response.build().toString();
        } catch (ValidationException e) {
            assert queryObject != null;
            return ExceptionHandler.handle(res,
                    new IllegalQueryException(e.getKeyword(),
                    queryObject.toString(),
                    e.getErrorMessage()),
                    e.getAllMessages().toString());
        } catch (JSONException | IllegalQueryException e) {
                return ExceptionHandler.handle(res, e, e.getMessage());
            } catch (Exception e) {
            e.printStackTrace();
            return ExceptionHandler.handle(res, e, "Unknown server error");
        }
    }

    public JsonObject geoQueryGroups(EventGeoQuery query) throws Exception {
        List<DataStreamGeo<Event>> events = query.execute();


        ResponseBuilder response = new ResponseBuilder(true)
                                           .setResponse(jsonStringSerialiser.getDefaultSerialiser()
                                                                            .toJsonTree(events)
                                                                            .getAsJsonArray().get(0).getAsJsonObject());

        return response.build();
    }


    /**
     * Build the event return message including inferred property read
     * @param event event object
     * @param user  requesting username
     * @return a return json object with the event message
     */
    private JsonObject buildEventMessage(Event event, String user) {
        return jsonStringSerialiser.getDefaultSerialiser().toJsonTree(event).getAsJsonObject();
    }

}
