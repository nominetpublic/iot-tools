/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Facet {
    private final String title;
    private final String property;
    private final String format;
    private final List<String> path = new ArrayList<>();
    private final List<FacetOption> options = new ArrayList<>();

    public Facet(String title, String property, String format, String[] path) {
        this.title = title;
        this.property = property;
        this.format = format;
        Collections.addAll(this.path, path);
    }

    public List<FacetOption> getOptions() {
        return options;
    }

    public List<String> getPath() {
        return path;
    }

    public String getFormat() {
        return format;
    }

    public String getProperty() {
        return property;
    }

    public String getTitle() {
        return title;
    }

    public JsonObject toJsonObject() {
        JsonObject deviceFacet = new JsonObject();
        deviceFacet.addProperty("title", this.title);
        deviceFacet.addProperty("property", this.property);
        JsonArray path = new JsonArray();
        for(String part : this.path) {
            path.add(part);
        }
        deviceFacet.add("path", path);
        deviceFacet.addProperty("format", this.format);

        JsonArray options = new JsonArray();
        for(FacetOption option : this.options) {
            options.add(option.toJsonObject());
        }

        deviceFacet.add("options", options);
        return deviceFacet;
    }
}
