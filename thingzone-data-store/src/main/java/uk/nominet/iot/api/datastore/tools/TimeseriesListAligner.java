/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.tools;

import uk.nominet.iot.api.datastore.model.DataStreamValues;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;
import java.util.*;

public class TimeseriesListAligner {
    private final List<DataStreamValues<AbstractTimeseriesPoint>> primaryTimeseries;
    private final Map<String, List<DataStreamValues<AbstractTimeseriesPoint>>> secondaryTimeserieses;

    public TimeseriesListAligner(List<DataStreamValues<AbstractTimeseriesPoint>> primaryTimeseries,
                                 Map<String, List<DataStreamValues<AbstractTimeseriesPoint>>> secondaryTimeserieses) {
        this.primaryTimeseries = primaryTimeseries;
        this.secondaryTimeserieses = secondaryTimeserieses;
    }

    /**
     * Align the primary timeseries with the secondary timeserieses
     * @return aligned timeseries
     */
    public List<DataStreamValues<AbstractTimeseriesPoint>> align() throws Exception {
        List<DataStreamValues<AbstractTimeseriesPoint>> result = new ArrayList<>();

        for (int i = 0; i < primaryTimeseries.size(); i++) {
            // define local set of streams to combine
            DataStreamValues<AbstractTimeseriesPoint> primaryStream = primaryTimeseries.get(i);
            Map<String, List<AbstractTimeseriesPoint>> secondaryStreams = new HashMap<>();
            for (Map.Entry<String, List<DataStreamValues<AbstractTimeseriesPoint>>>
                         secondaryTimeseriesEntrySet : secondaryTimeserieses.entrySet()) {

                if (secondaryTimeseriesEntrySet.getValue().size() != primaryTimeseries.size())
                    throw new Exception(String.format("secondary timeserieses must have the same number of streams as " +
                                                      "the primary. Found  %d expected %d",secondaryTimeseriesEntrySet.getValue().size(),
                                                      primaryTimeseries.size()));
                List<AbstractTimeseriesPoint> secondaryValues = secondaryTimeseriesEntrySet.getValue().get(i).getValues();
                secondaryValues.sort(new TimeseriesTimeSorter());

                secondaryStreams.put(secondaryTimeseriesEntrySet.getKey(),
                                     secondaryValues);

            }
            result.add(combine(primaryStream, secondaryStreams));
        }

        return result;
    }

    /**
     *
     * @param primaryStream primary stream data
     * @param secondaryValues secondary streams data
     * @return combined stream
     */
    private DataStreamValues<AbstractTimeseriesPoint> combine(DataStreamValues<AbstractTimeseriesPoint> primaryStream,
                                                              Map<String, List<AbstractTimeseriesPoint>> secondaryValues) {

        for (AbstractTimeseriesPoint primaryPoint : primaryStream.getValues()) {
            primaryPoint.setCombinedPoints(new HashMap<>());

            for (Map.Entry<String, List<AbstractTimeseriesPoint>> secondaryValuesEntrySet : secondaryValues.entrySet()) {

                int index = Collections.binarySearch(secondaryValuesEntrySet.getValue(),
                        primaryPoint,
                        new TimeseriesTimeSorter());
                if (index < -1) index = -index - 2;
                if (index >= 0) primaryPoint.getCombinedPoints().put(secondaryValuesEntrySet.getKey(),
                                                                     secondaryValuesEntrySet.getValue().get(index));
            }
        }

        return primaryStream;
    }

}
