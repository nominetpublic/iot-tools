/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.query;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.core.search.aggregation.ChildrenAggregation;
import io.searchbox.core.search.aggregation.FilterAggregation;
import io.searchbox.core.search.aggregation.TermsAggregation;
import io.searchbox.core.search.aggregation.TopHitsAggregation;
import org.elasticsearch.index.query.*;
import org.elasticsearch.join.aggregations.ChildrenAggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filter.FilterAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.tophits.TopHitsAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.json.JSONObject;
import uk.nominet.iot.api.datastore.model.DataStreamValues;
import uk.nominet.iot.api.datastore.utils.ArrayUtils;
import uk.nominet.iot.api.datastore.utils.ElasticSearchUtils;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.model.api.APIException;
import uk.nominet.iot.model.timeseries.Config;
import uk.nominet.iot.model.timeseries.Json;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static uk.nominet.iot.utils.ConversionUtils.asciiToHex;

public class ConfigListQuery extends DatastoreListQuery<Config, List<DataStreamValues<Config>>> implements ElasticsearchBasicQuery {

    public ConfigListQuery(JSONObject requestBody, String user) throws Exception {
        super(requestBody, user);
        getSchema().validate(requestBody);
        setUser(user);
        if (requestBody.has("streams"))
            setStreams(ArrayUtils.toStringArray(requestBody.getJSONArray("streams")));
        if (requestBody.has("time"))
            setTime(requestBody.getString("time"));

        if (requestBody.has("timeLimit"))
            setTimeLimit(requestBody.getString("timeLimit"));

        if (requestBody.has("forward"))
            setForward(requestBody.getBoolean("forward"));
        else
            setForward(false);

        if (requestBody.has("size"))
            setSize(requestBody.getInt("size"));
    }



    @Override
    public QueryBuilder getQueryBuilder() {
        System.out.println("Streams: " + streams);

        return QueryBuilders.termsQuery("key.keyword", streams);
        //return QueryBuilders.matchAllQuery();
    }

    @Override
    public AggregationBuilder getAggregationBuilder() {
        TermsAggregationBuilder termsAggregationBuilder = AggregationBuilders.terms("stream").field("key.keyword").size(100);

        ChildrenAggregationBuilder childrenAggregation = new ChildrenAggregationBuilder("configts", "config");

        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.must(new MatchAllQueryBuilder());


        RangeQueryBuilder rangeQueryBuilder = ElasticSearchUtils.getRangeBuilder(time, timeLimit, forward);
        if (rangeQueryBuilder != null)  boolQueryBuilder.filter(rangeQueryBuilder);


        FilterAggregationBuilder filterAggregation = AggregationBuilders.filter("filtered", boolQueryBuilder);

        TopHitsAggregationBuilder topHitsAggregationBuilder = AggregationBuilders.topHits("config")
                                                                                 .size((size != null) ? size : 10000)
                                                                                 .sort("timestamp", SortOrder.DESC);


        return termsAggregationBuilder
                       .subAggregation(
                               childrenAggregation
                                       .subAggregation(
                                               filterAggregation
                                                       .subAggregation(topHitsAggregationBuilder)));
    }

    @Override
    public String getSortName() {
        return "timestamp";
    }

    @Override
    public SortOrder getSortOrder() {
        if (forward)
            return SortOrder.ASC;
        else
            return SortOrder.DESC;
    }



    @Override
    public List<DataStreamValues<Config>> execute() throws Exception {

        String index;
        try {
            index = ElasticSearchUtils.INDEX_PREFIX + asciiToHex(getUser());
        } catch (Exception e) {
            throw new APIException(400, "Could not identify elasticsearch index", e);
        }

        List<DataStreamValues<Config>> results = new ArrayList<>();

        SearchSourceBuilder searchBuilder = new SearchSourceBuilder()
                                                    .query(getQueryBuilder())
                                                    .aggregation(getAggregationBuilder())
                                                    .size(100);

        Search search = new Search.Builder(searchBuilder.toString())
                                .addIndex(index)
                                .addType(ElasticSearchUtils.STREAM_TYPE)
                                .build();


        SearchResult result;

        try {
            result = ElasticsearchDriver.getClient().execute(search);
            if (result.isSucceeded()) {

                JsonArray responseValue = new JsonArray();

                //Build a map of streams
                HashMap<String, JsonObject> streamsData = new HashMap<>();

                for (SearchResult.Hit<JsonObject, Void> streamHit : result.getHits(JsonObject.class)) {
                    streamsData.put(streamHit.id, streamHit.source);
                }

                // Go through aggregations
                for (TermsAggregation.Entry stream : result.getAggregations().getTermsAggregation("stream").getBuckets()) {
                    String streamKey = stream.getKey();

                    JsonObject streamData = streamsData.get(streamKey);

                    DataStreamValues<Config> jsonTimeseries = new DataStreamValues<>();
                    jsonTimeseries.setStreamKey(streamKey);
                    jsonTimeseries.setMetadata(jsonStringSerialiser.getDefaultSerialiser().fromJson(streamData.get("metadata").getAsJsonObject(), new TypeToken<Map<String, Object>>() {}.getType()));
                    jsonTimeseries.setMerged(false);

                    List<Config> values = new ArrayList<>();

                    ChildrenAggregation childrenAggregation = stream.getChildrenAggregation("configts");
                    FilterAggregation filterAggregation = childrenAggregation.getFilterAggregation("filtered");
                    TopHitsAggregation topHitsAggregation = filterAggregation.getTopHitsAggregation("config");

                    for (SearchResult.Hit<JsonObject, Void> jsonPointHit : topHitsAggregation.getHits(JsonObject.class)) {
                        values.add(jsonStringSerialiser.readObject(jsonPointHit.source, Config.class));
                    }

                    jsonTimeseries.setValues(values);

                    results.add(jsonTimeseries);
                }

            } else {
                throw new APIException(400, result.getErrorMessage());
            }
        } catch (IOException e) {
            throw new APIException(400, "Could not send request to elasticsearch", e);
        }


        return results;
    }
}
