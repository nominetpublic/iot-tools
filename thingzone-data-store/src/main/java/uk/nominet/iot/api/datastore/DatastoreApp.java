/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore;

import uk.nominet.iot.api.datastore.config.DatastoreConstant;
import uk.nominet.iot.api.datastore.controller.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Service;
import uk.nominet.iot.config.*;
import uk.nominet.iot.driver.CassandraDriver;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.driver.ImagesvrDriver;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.http.CORSEnabler;
import uk.nominet.iot.http.ThingzoneRegistrySessionKeyIntrospector;

import java.util.Collections;
import java.util.Properties;

import static spark.Service.ignite;

public class DatastoreApp {
    private static final Logger LOG = LoggerFactory.getLogger(DatastoreApp.class);
    public static final int VERSION = 1;
    private final Properties configProp;

    private DataStreamsController dataStreamsController;
    public MetricsAggController aggregationsController;
    public MetricsGeoController metricsGeoController;
    private FacetsSearchController facetsSearchController;
    private StatusController statusController;
    public StreamListController streamListController;
    public EventController eventController;
    public ImagesController imagesController;
    public MetricListController metricListController;
    private DatesController datesController;
    private StatisticalController statisticalController;
    private PipelineController pipelineController;
    public CommonStreamsController commonStreamsController;
    private JsonListController jsonListController;
    private ConfigListController configListController;
    private CombinationController combinationController;
    private LatestController latestController;

    public DatastoreApp(Properties configProp) {
        this.configProp = configProp;
    }

    public void initDrivers() {
        if (RegistryAPIDriver.isInitialized())
            RegistryAPIDriver.globalDispose();
        if (!RegistryAPIDriver.isInitialized()) {
            RegistryDriverConfig registryConfig = new RegistryDriverConfig();
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_SCHEME,
                                       configProp.getProperty(RegistryConstant.REGISTRY_SERVER_SCHEME));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST,
                                       configProp.getProperty(RegistryConstant.REGISTRY_SERVER_HOST));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT,
                                       configProp.getProperty(RegistryConstant.REGISTRY_SERVER_PORT));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_USERNAME,
                                       configProp.getProperty(RegistryConstant.REGISTRY_SERVER_USERNAME));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD,
                                       configProp.getProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS,
                                       configProp.getProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS));
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }

        if (CassandraDriver.isInitialized())
            CassandraDriver.globalDispose();
        if (!CassandraDriver.isInitialized()) {
            CassandraDriverConfig cassandraConfig = new CassandraDriverConfig();
            cassandraConfig.addProperty(CassandraConstant.CASSANDRA_SERVER_HOST,
                                        configProp.getProperty(CassandraConstant.CASSANDRA_SERVER_HOST));
            cassandraConfig.addProperty(CassandraConstant.CASSANDRA_SERVER_PORT,
                                        configProp.getProperty(CassandraConstant.CASSANDRA_SERVER_PORT));
            CassandraDriver.setConnectionProperties(cassandraConfig);
            CassandraDriver.globalInitialise();
        }

        if (ElasticsearchDriver.isInitialized())
            ElasticsearchDriver.globalDispose();
        if (!ElasticsearchDriver.isInitialized()) {
            ElasticsearchDriverConfig elasticsearchConfig = new ElasticsearchDriverConfig();
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_SCHEME,
                                            configProp.getProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_SCHEME));
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_HOST,
                                            configProp.getProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_HOST));
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_PORT,
                                            configProp.getProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_PORT));
            ElasticsearchDriver.setConnectionProperties(elasticsearchConfig);
            ElasticsearchDriver.globalInitialise();
        }

        if (ImagesvrDriver.isInitialized()) ImagesvrDriver.globalDispose();
        if (!ImagesvrDriver.isInitialized()) {
            ImagesvrDriverConfig imagesvrDriverConfig = new ImagesvrDriverConfig();
            imagesvrDriverConfig.addProperty(ImageServerConstant.IMAGESERVER_SERVER_HOST,
                                             configProp.getProperty(ImageServerConstant.IMAGESERVER_SERVER_HOST));
            imagesvrDriverConfig.addProperty(ImageServerConstant.IMAGESERVER_SERVER_SCHEME,
                                             configProp.getProperty(ImageServerConstant.IMAGESERVER_SERVER_SCHEME));
            imagesvrDriverConfig.addProperty(ImageServerConstant.IMAGESERVER_SERVER_PORT,
                                             configProp.getProperty(ImageServerConstant.IMAGESERVER_SERVER_PORT));
            ImagesvrDriver.setConnectionProperties(imagesvrDriverConfig);
            ImagesvrDriver.globalInitialise();
        }


    }

    public void run() throws Exception {
        // final String elasticsearchUrl = configProp.getProperty(DatastoreConstant.ELASTICSEARCH_URL);

        //Initialise http server
        Service http = ignite()
                               .port(Integer.parseInt(configProp.getProperty(DatastoreConstant.DATASTORE_PORT)))
                               .ipAddress(configProp.getProperty(DatastoreConstant.DATASTORE_HOST))
                               .threadPool(20);

        // Attach security components
        new CORSEnabler("*",
                        "GET,PUT,POST,DELETE,OPTIONS",
                        "Content-Type,Authorization,X-Requested-With," +
                        "Content-Length,Accept,Origin,X-Introspective-Session-Key")
                .attachTo(http);


        new ThingzoneRegistrySessionKeyIntrospector(
                false,
                null,
                Collections.singletonList(String.format("/api/v%d/status/health", VERSION)))
                .attachTo(http);

        // Set response type to application/json
        http.before("/*", (request, response) -> response.type("application/json"));

        // Gzip content of responses
        http.after((request, response) -> response.header("Content-Encoding", "gzip"));

        http.exception(Exception.class, (e, req, res) -> {
            LOG.error(e.getMessage(), e);
            LOG.debug("Request: " + req.toString());
            LOG.debug("Response: " + res.toString());
            e.printStackTrace();
        });


        /*
          GET /api/v1/search/stream/list
        */
        dataStreamsController = new DataStreamsController();
        dataStreamsController.publishAPI(http, String.format("/api/v%d/search", VERSION));

        /*
          POST /api/v1/metrics/agg
          POST /api/v1/metrics/geo
        */
        aggregationsController = new MetricsAggController();
        aggregationsController.publishAPI(http, String.format("/api/v%d/metrics", VERSION));
        metricsGeoController = new MetricsGeoController();
        metricsGeoController.publishAPI(http, String.format("/api/v%d/metrics", VERSION));


        /*
         POST /api/v1/metrics/list/:stream
         */
        metricListController = new MetricListController();
        metricListController.publishAPI(http, String.format("/api/v%d/metrics", VERSION));

        /*
         POST /api/v1/metrics/dates/:stream
         */
        datesController = new DatesController();
        datesController.publishAPI(http, String.format("/api/v%d/metrics", VERSION));

        /*
          GET /api/v1/device/facets
        */
        facetsSearchController = new FacetsSearchController();
        facetsSearchController.publishAPI(http, String.format("/api/v%d/device", VERSION));

         /*
          POST /api/v1/device/search/geo
        */
        latestController = new LatestController();
        latestController.publishAPI(http, String.format("/api/v%d/device/search", VERSION));


        /*
           GET /api/v1/status/health
         */
        statusController = new StatusController();
        statusController.publishAPI(http, String.format("/api/v%d/status", VERSION));

        /*
           POST /api/v1/stream/list
           GET  /api/v1/stream/facets
           POST /api/v1/stream/facets
         */
        streamListController = new StreamListController();
        streamListController.publishAPI(http, String.format("/api/v%d/stream", VERSION));


        /*
          POST /api/v1/stream/common
         */
        commonStreamsController = new CommonStreamsController();
        commonStreamsController.publishAPI(http, String.format("/api/v%d/stream", VERSION));


        /*
          POST   /api/v1/event/list
          POST   /api/v1/event/geo
          GET    /api/v1/event/:streamKey/:alertId
          PUT    /api/v1/event/:streamKey/:alertId/feedback
          DELETE /api/v1/event/:streamKey/:alertId/feedback/:feedbackId
          PUT    /api/v1/event/:streamKey/:alertId/tag/:tag
          DELETE /api/v1/event/:streamKey/:alertId/tag/:tag
          PUT    /api/v1/event/:streamKey/:alertId/severity/:severity
          PUT    /api/v1/event/:streamKey/:alertId/status/:status
          GET    /api/v1/event/tags/suggest/:prefix
          GET    /api/v1/event/facets"
         */
        eventController = new EventController();
        eventController.publishAPI(http, String.format("/api/v%d/event", VERSION));


        /*
          POST /api/v1/images/list
          PUT /api/v1/images/:streamKey/:imageId/tag/:tag
          DELETE /api/v1/images/:streamKey/:imageId/tag/:tag
         */
        imagesController = new ImagesController();
        imagesController.publishAPI(http, String.format("/api/v%d/images", VERSION));



        /*
          GET /api/v1/stat/vehicle
          POST /api/v1/stat/vehicle
         */
        statisticalController = new StatisticalController();
        statisticalController.publishAPI(http, String.format("/api/v%d/stat", VERSION));


        pipelineController = new PipelineController(this);
        pipelineController.publishAPI(http, String.format("/api/v%d", VERSION));


        /*
          POST /api/v1/json/list
         */
        jsonListController = new JsonListController();
        jsonListController.publishAPI(http, String.format("/api/v%d/json", VERSION));


         /*
          POST /api/v1/config/list
         */
        configListController = new ConfigListController();
        configListController.publishAPI(http, String.format("/api/v%d/config", VERSION));

        /*
          POST /api/v1/combination/list
          POST /api/v1/combination/geo
         */
        combinationController = new CombinationController();
        combinationController.publishAPI(http, String.format("/api/v%d/combination", VERSION));


        LOG.info("Service started in port " + http.port());
    }


    public static void main(String[] args) throws Exception {

        if (args.length != 1) {
            System.out.println("Usage: DatastoreApp configFile");
            System.exit(1);
        }

        Properties configProp = ConfigurationLoader.fromFileName(args[0]);

        DatastoreApp app = new DatastoreApp(configProp);
        app.initDrivers();
        app.run();
    }
}
