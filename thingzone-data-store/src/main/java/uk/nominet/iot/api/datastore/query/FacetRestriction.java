/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.query;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import uk.nominet.iot.api.datastore.errors.IllegalQueryException;

import java.util.ArrayList;
import java.util.List;

public class FacetRestriction {
    private final List<String> path;
    private List<Object> values;
    private Object min;
    private Object max;
    private Boolean exclude = false;
    private CombinationType combination = CombinationType.OR;

    private FacetRestriction(List<String> path, List<Object> values) {
        this.path = path;
        this.values = values;
    }

    private FacetRestriction(List<String> path, Object min, Object max) {
        this.path = path;
        this.max = max;
        this.min = min;
    }

    public static FacetRestriction parseJSON(JSONObject facetObject) throws IllegalQueryException {
        FacetRestriction restriction = null;
        JSONArray pathArray = facetObject.getJSONArray("path");
        ArrayList<String> path = new ArrayList<>();
        for (int j = 0; j < pathArray.length(); j++) {
            path.add(pathArray.getString(j));
        }
        if (facetObject.has("values")) {
            List<Object> values = new ArrayList<>();
            JSONArray valuesArray = facetObject.getJSONArray("values");
            for (int j = 0; j < valuesArray.length(); j++) {
                values.add(valuesArray.get(j));
            }
            restriction = new FacetRestriction(path, values);
            restriction.combination = CombinationType.valueOf(
                    facetObject.optString("combination", "OR").toUpperCase());
        }
        if (facetObject.has("range")) {
            JSONArray rangeArray = facetObject.getJSONArray("range");
            if (rangeArray.length() != 2) {
                throw new IllegalQueryException("facet.restrictions.range",
                        rangeArray.toString(), "a range restriction must contain exactly two values");
            }
            restriction = new FacetRestriction(path, rangeArray.get(0), rangeArray.get(1));
        }
        if (restriction == null) {
            throw new IllegalQueryException("facet.restrictions", facetObject.toString(),
                    "a restriction must contain either values or a range");
        }
        if (facetObject.has("exclude")) {
            restriction.exclude = facetObject.optBoolean("exclude", false);
        }
        return restriction;
    }

    public static BoolQueryBuilder buildBooleanQuery(List<FacetRestriction> restrictions, CombinationType combination,
                                                     BoolQueryBuilder booleanQuery) {
        List<QueryBuilder> includes = new ArrayList<>();
        List<QueryBuilder> excludes = new ArrayList<>();
        for(FacetRestriction restriction : restrictions) {
            String path = String.join(".", restriction.path);
            QueryBuilder facetQuery = null;
            if(restriction.values != null) {
                switch (restriction.combination) {
                    case AND:
                        facetQuery = QueryBuilders.boolQuery();
                        for (Object value : restriction.values) {
                            // Use .keyword for exact match
                            QueryBuilder termQuery = QueryBuilders.termQuery(path  + ".keyword" , value);
                            ((BoolQueryBuilder) facetQuery).must(termQuery);
                        }
                        break;
                    case OR:
                        facetQuery = QueryBuilders.boolQuery();
                        for (Object value : restriction.values) {
                            // Use .keyword for exact match
                            QueryBuilder termQuery = QueryBuilders.termQuery(path  + ".keyword" , value);
                            ((BoolQueryBuilder) facetQuery).should(termQuery);
                        }
                    default:
                        facetQuery = QueryBuilders.termsQuery(path  + ".keyword", restriction.values.toArray());
                        break;
                }
            } else if(restriction.min != null || restriction.max != null) {
                // either can be null to perform an unbounded min/max
                RangeQueryBuilder rangeQuery = QueryBuilders.rangeQuery(path);
                rangeQuery.from(restriction.min, true);
                rangeQuery.to(restriction.max, true);
                facetQuery = rangeQuery;
            }
            if(facetQuery == null) {
                throw new IllegalQueryException("facet.restrictions", null, "either values must be specified or " +
                        "a range with either a min, or a max, or both");
            }
            if (restriction.exclude) {
                excludes.add(facetQuery);
            } else {
                includes.add(facetQuery);
            }
        }
        if(restrictions.size() > 0) {
            switch(combination) {
                case AND:
                    for(QueryBuilder inclusion: includes) {
                        booleanQuery.filter(inclusion);
                    }
                    for(QueryBuilder exclusion: excludes) {
                        booleanQuery.mustNot(exclusion);
                    }
                    break;
                case OR:
                default:
                    BoolQueryBuilder shouldQuery = QueryBuilders.boolQuery();
                    for(QueryBuilder inclusion: includes) {
                        shouldQuery.should(inclusion);
                    }
                    for(QueryBuilder exclusion: excludes) {
                        BoolQueryBuilder mustNotQuery = QueryBuilders.boolQuery();
                        mustNotQuery.mustNot(exclusion);
                        shouldQuery.should(mustNotQuery);
                    }
                    shouldQuery.minimumShouldMatch(1);
                    booleanQuery.filter(shouldQuery);
                    break;
            }
        }
        return booleanQuery;
    }

    @Override
    public String toString() {
        return String.format("%s: path %s; values %s; min %s; max %s; exclude %s, combination %s",
                getClass().getName(), path, values, min, max, exclude, combination);
    }
}
