/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.query;

import org.joda.time.Instant;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class BulkQuery extends DatastoreQuery<Object> {
    private Instant start = null;
    private Instant end = null;
    private String interval;
    private final List<String> deviceIds = new ArrayList<>();

    public BulkQuery(JSONObject requestBody, String user) throws Exception {
        super(requestBody, user);
        getSchema().validate(requestBody);
        setUser(user);
        setStart(Instant.parse(requestBody.getString("start")));
        setEnd(Instant.parse(requestBody.getString("end")));
        if(requestBody.has("interval") && !requestBody.isNull("interval")) {
            setInterval(requestBody.getString("interval"));
        }
        if(requestBody.has("devices") && !requestBody.isNull("devices")) {
            JSONArray devices = requestBody.getJSONArray("devices");
            for(int i = 0; i < devices.length(); i++) {
                getDeviceIds().add(devices.getString(i));
            }
        }
    }


    public Instant getEnd() {
        return end;
    }

    public Instant getStart() {
        return start;
    }

    public String getInterval() { return interval; }

    public List<String> getDeviceIds() {
        return deviceIds;
    }

    private void setEnd(Instant end) {
        this.end = end;
    }

    private void setStart(Instant start) {
        this.start = start;
    }

    private void setInterval(String interval) { this.interval = interval; }

    @Override
    public Object execute() {
        return null;
    }
}
