/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.query;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.tophits.TopHitsAggregationBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.json.JSONObject;
import uk.nominet.iot.api.datastore.utils.ArrayUtils;

import java.util.Arrays;


public class CommonStreamsQuery extends DatastoreQuery<Object> implements ElasticsearchBasicQuery {


    private String[] devices;

    private String[] types;

    public CommonStreamsQuery(JSONObject requestBody, String user) throws Exception {
        super(requestBody, user);
        getSchema().validate(requestBody);
        setUser(user);

        if(requestBody.has("devices") && !requestBody.isNull("devices")) {
            setDevices(ArrayUtils.toStringArray(requestBody.getJSONArray("devices")));
        }

        if(requestBody.has("types") && !requestBody.isNull("types")) {
            setTypes(ArrayUtils.toStringArray(requestBody.getJSONArray("types")));
        }
    }

    public String[] getDevices() {
        return devices;
    }

    public void setDevices(String[] devices) {
        this.devices = devices;
    }

    public String[] getTypes() {
        return types;
    }

    private void setTypes(String[] types) {
        this.types = types;
    }

    @Override
    public String toString() {
        return "CommonStreamsQuery{" +
               "devices=" + Arrays.toString(devices) +
               ", types=" + Arrays.toString(types) +
               '}';
    }

    @Override
    public QueryBuilder getQueryBuilder() {
        return new TermsQueryBuilder("devices.key.keyword", devices);
    }

    @Override
    public AggregationBuilder getAggregationBuilder() {
        TermsAggregationBuilder termsAggregationBuilder = AggregationBuilders.terms("streams")
                                                                             .field("name.keyword")
                                                                             .size(10000)
                                                                             .minDocCount(devices.length);

        TopHitsAggregationBuilder topHitsAggregationBuilder = AggregationBuilders.topHits("top_hits").size(1);



        return termsAggregationBuilder.subAggregation(topHitsAggregationBuilder);
    }

    @Override
    public String getSortName() {
        return null;
    }

    @Override
    public SortOrder getSortOrder() {
        return null;
    }

    @Override
    public Object execute() {
        return null;
    }
}
