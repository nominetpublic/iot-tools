/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.controller;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.core.search.aggregation.ChildrenAggregation;
import io.searchbox.core.search.aggregation.FilterAggregation;
import io.searchbox.core.search.aggregation.TermsAggregation;
import io.searchbox.core.search.aggregation.TopHitsAggregation;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.everit.json.schema.ValidationException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import spark.Request;
import spark.Response;
import spark.Service;
import uk.nominet.iot.api.datastore.errors.IllegalQueryException;
import uk.nominet.iot.api.datastore.model.DataStreamValues;
import uk.nominet.iot.api.datastore.query.ConfigListQuery;
import uk.nominet.iot.api.datastore.query.JsonListQuery;
import uk.nominet.iot.api.datastore.utils.ElasticSearchUtils;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.http.APIController;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.api.APIException;
import uk.nominet.iot.model.api.ExceptionHandler;
import uk.nominet.iot.model.api.ResponseBuilder;
import uk.nominet.iot.model.timeseries.Config;
import uk.nominet.iot.model.timeseries.Json;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import static uk.nominet.iot.utils.ConversionUtils.asciiToHex;

public class ConfigListController implements APIController {
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();


    @Override
    public void publishAPI(Service http, String path) {
        http.path(path, () -> http.post("/list", this::list));
    }

    private String list(Request req, Response res) {
        JSONObject queryObject;
        try {
            queryObject = new JSONObject(new JSONTokener(req.body()));
        } catch (JSONException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

        try {
            ConfigListQuery query = new ConfigListQuery(queryObject, req.attribute("introspected_username"));

            List<DataStreamValues<Config>> configTimeseries = query.execute();
            ResponseBuilder response = new ResponseBuilder(true)
                                               .setResponse(jsonStringSerialiser.getDefaultSerialiser()
                                                                                .toJsonTree(configTimeseries)
                                                                                .getAsJsonArray());

            return response.build().toString();
        } catch (ValidationException e) {
            return ExceptionHandler.handle(res,
                    new IllegalQueryException(e.getKeyword(), queryObject.toString(),
                            e.getErrorMessage()), e.getAllMessages().toString());
        } catch (Exception e) {
            e.printStackTrace();
            return ExceptionHandler.handle(res, e);
        }

    }

    private JsonObject listJson(JsonListQuery query) throws APIException {
        ResponseBuilder response;
        String index;
        try {
            index = ElasticSearchUtils.INDEX_PREFIX + asciiToHex(query.getUser());
        } catch (Exception e) {
            throw new APIException(400, "Could not identify elasticsearch index", e);
        }


        SearchSourceBuilder searchBuilder = new SearchSourceBuilder()
                                                    .query(query.getQueryBuilder())
                                                    .aggregation(query.getAggregationBuilder())
                                                    .size(100);

        Search search = new Search.Builder(searchBuilder.toString())
                                .addIndex(index)
                                .addType(ElasticSearchUtils.STREAM_TYPE)
                                .build();
        
        SearchResult result;

        try {
            result = ElasticsearchDriver.getClient().execute(search);
            if (result.isSucceeded()) {

                JsonArray responseValue = new JsonArray();

                //Build a map of streams
                HashMap<String, JsonObject> streamsData = new HashMap<>();

                for (SearchResult.Hit<JsonObject, Void> streamHit : result.getHits(JsonObject.class)) {
                    streamsData.put(streamHit.id, streamHit.source);
                }


                // Go through aggregations

                for (TermsAggregation.Entry stream : result.getAggregations()
                        .getTermsAggregation("stream").getBuckets()) {
                    String streamKey = stream.getKey();

                    JsonObject responseItem = new JsonObject();

                    JsonObject streamData = streamsData.get(streamKey);

                    responseItem.addProperty("streamKey", streamKey);
                    responseItem.addProperty("name", streamData.get("name").getAsString());
                    responseItem.addProperty("type", streamData.get("type").getAsString());
                    responseItem.add("metadata", streamData.get("metadata"));


                    JsonArray jsonTimeseries = new JsonArray();

                    ChildrenAggregation childrenAggregation = stream.getChildrenAggregation("jsonts");
                    FilterAggregation filterAggregation = childrenAggregation.getFilterAggregation("filtered");
                    TopHitsAggregation topHitsAggregation = filterAggregation.getTopHitsAggregation("json");

                    for (SearchResult.Hit<JsonObject, Void> jsonPointHit : topHitsAggregation.getHits(JsonObject.class)) {
                        jsonTimeseries.add(jsonPointHit.source);

                    }

                    responseItem.add("values", jsonTimeseries);
                    responseValue.add(responseItem);

                }
                response = new ResponseBuilder(true).setResponse(responseValue);

            } else {
                response = new ResponseBuilder(false).setReason(result.getErrorMessage());
            }
        } catch (IOException e) {
            if (e.getCause() != null) {
                throw new APIException(400, "Could not send request to elasticsearch: " + e.getMessage(), e);
            } else {
                throw new APIException(400, "Could not send request to elasticsearch: " + e.getCause().getMessage(), e);
            }
        }

        return response.build();

    }
}
