/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class FacetOption {
    private final String id;
    private String value;
    private final Number[] range = {null, null};
    private final long count;

    public FacetOption(String id, String value, long count) {
        this.id = id;
        this.value = value;
        this.count = count;
    }

    public FacetOption(String id, Number min, Number max, long count) {
        this.id = id;
        this.range[0] = min;
        this.range[1] = max;
        this.count = count;
    }

    public String getId() {
        return id;
    }

    public long getCount() {
        return count;
    }

    public String getValue() {
        return value;
    }

    public Number[] getRange() {
        return range;
    }

    public JsonObject toJsonObject() {
        JsonObject option = new JsonObject();
        option.addProperty("id", id);
        if(value != null) {
            option.addProperty("value", value);
        } else if(range[0] != null && range[1] != null) {
            JsonArray range = new JsonArray();
            range.add(this.range[0]);
            range.add(this.range[1]);
            option.add("range", range);
        } else {
            throw new IllegalArgumentException("A facet option must have either a value or range specified");
        }
        option.addProperty("count", count);
        return option;
    }
}
