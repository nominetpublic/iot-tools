/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.model;

import java.util.*;

/**
 * A HashSet that is also ordered by when an element was inserted
 * 
 * @param <E>
 */
public final class IndexedSetList<E> implements List<E>, Set<E> {
    private final HashSet<E> members;
    private final ArrayList<E> ordering;

    public IndexedSetList() {
        members = new HashSet<>();
        ordering = new ArrayList<>();
    }

    @Override
    public int size() {
        return members.size();
    }

    @Override
    public boolean isEmpty() {
        return members.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return members.contains(o);
    }

    @Override
    public Iterator<E> iterator() {
        return ordering.iterator();
    }

    @Override
    public E get(int index) {
        return ordering.get(index);
    }

    @Override
    public E set(int index, E element) {
        throw new UnsupportedOperationException("Cannot alter IndexedSetLists by index");
    }

    @Override
    public void add(int index, E element) {
        throw new UnsupportedOperationException("Cannot alter IndexedSetLists by index");
    }

    @Override
    public E remove(int index) {
        E object = ordering.remove(index);
        members.remove(object);
        return object;
    }

    @Override
    public int indexOf(Object o) {
        if(!contains(o)) {
            return -1;
        }
        return ordering.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        if(!contains(o)) {
            return -1;
        }
        return ordering.lastIndexOf(o);
    }

    @Override
    public ListIterator<E> listIterator() {
        return ordering.listIterator();
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return ordering.listIterator(index);
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return ordering.subList(fromIndex, toIndex);
    }

    @Override
    public boolean remove(Object o) {
        if(contains(o)) {
            ordering.remove(o);
            members.remove(o);
            return true;
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return members.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        boolean modified = false;
        for(E object : c) {
            modified |= add(object);
        }
        return modified;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        throw new UnsupportedOperationException("Cannot alter IndexedSetLists by index");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean modified = false;
        for(Object object : c) {
            modified |= remove(object);
        }
        return modified;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean modified = ordering.retainAll(c);
        if(modified) {
            members.retainAll(c);
        }
        return modified;
    }

    @Override
    public void clear() {
        ordering.clear();
        members.clear();
    }

    @Override
    public Object[] toArray() {
        return ordering.toArray();
    }

    @SuppressWarnings("SuspiciousToArrayCall")
    @Override
    public <T> T[] toArray(T[] a) {
        return ordering.toArray(a);
    }

    @Override
    public boolean add(E e) {
        boolean modified = false;
        if(!members.contains(e)) {
            modified = true;
            members.add(e);
            ordering.add(e);
        }
        return modified;
    }

    @Override
    public Spliterator<E> spliterator() {
        return ordering.spliterator();
    }
}
