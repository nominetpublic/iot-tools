/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.utils;

import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.nominet.iot.api.datastore.controller.StatisticalController;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

/* example set of utils for simple potential fleet data */
public class VehicleStatisticalUtils {
    private static final Logger LOG = LoggerFactory.getLogger(StatisticalController.class);
    private static final int AUTONOMY = 2;
    private static final int MANUAL = 1;
    private static final int DEFAULT_LON_LAT = 0;
    private static final double DEFAULT_VELOCITY= -100;
    private static final int DEFAULT_AUTONOMY = 0;
    private static final int FILTERING_THRESHOLD_MILLISECOND = 120000;


    private final ArrayList<VehicleData> dataPoints = new ArrayList<>();
    private final DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    private final DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    private final String vehicleId;
    private int parkingStartedIndex = -1;

    public VehicleStatisticalUtils(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    /**
     *
     * @param dataPoint consists of autonomy status, location, velocity and timestamp
     */
    public void setDataPoints(JsonObject dataPoint) {
        double lat = DEFAULT_LON_LAT;
        double lon = DEFAULT_LON_LAT;
        double velocity = DEFAULT_VELOCITY;

        int autonomy = MANUAL;
        if(dataPoint.has("autonomy"))  {
            autonomy = dataPoint.get("autonomy").getAsInt();
        }

        if(dataPoint.has("location-latitude") && dataPoint.has("location-longitude")) {
            lat = dataPoint.get("location-latitude").getAsDouble();
            lon = dataPoint.get("location-longitude").getAsDouble();
        }
        if(dataPoint.has("velocity"))  {
            velocity = dataPoint.get("velocity").getAsDouble();
        }
        try
        {
            dataPoints.add(new VehicleData(df1.parse(dataPoint.get("timestamp").getAsString()),
                    autonomy, lat, lon, velocity));
        }
        catch (ParseException e) {
            //e.printStackTrace();
            try{
                dataPoints.add(new VehicleData(df2.parse(dataPoint.get("timestamp").getAsString()),
                        autonomy, lat, lon, velocity));
            }
            catch (ParseException e1) {
                e1.printStackTrace();
            }
        }
        setParkingStatus();
    }

    /**
     * Calculate the parking status of the newly added data points relative to
     * previous data points. If the vehicle was not moving for more than 2
     * minutes, then set parking status of all data points for the last 2
     * minutes to "parked"
     *
     */
    private void setParkingStatus() {
       // start calculation, if there are at least two data points in the list
        int numDataPoints = dataPoints.size();
        int thisIndex = numDataPoints - 1;
        if(numDataPoints > 1) {
            VehicleData thisPoint = dataPoints.get(thisIndex);
            VehicleData lastPoint = dataPoints.get(thisIndex - 1);
            VehicleData parkingIndexPoint = dataPoints.get(parkingStartedIndex);
            // if there is a data gap > 2 minutes between the last two points,
            // that means the vehicle was off so reset the parking calculation.
            if(getTimeDifference(thisPoint, lastPoint)
                    > FILTERING_THRESHOLD_MILLISECOND) {
                parkingStartedIndex = thisIndex;
            } else {
                // check if the vehicle is moving
                if(wasMoving(thisPoint, parkingIndexPoint)) {
                    // if time threshold met => set all data points from parkingStartIndex to the current
                    // point to "parked" status
                    if(getTimeDifference(thisPoint, parkingIndexPoint) > FILTERING_THRESHOLD_MILLISECOND) {
                        setParkingInterval(parkingStartedIndex, thisIndex - 1);
                    }
                    // reset parking index
                    parkingStartedIndex = thisIndex;
                } else {
                    // the time threshold passed, all data points from parkingStartIndex to the current
                    // point are set to "parked" status
                    if(getTimeDifference(thisPoint, parkingIndexPoint) > FILTERING_THRESHOLD_MILLISECOND) {
                        if(lastPoint.getParkingStat()) {
                            thisPoint.setParkingStat(true);
                        } else {
                            setParkingInterval(parkingStartedIndex, thisIndex);
                        }
                    }
                }

            }
        } else {
            parkingStartedIndex = 0;
        }
    }

    private void setParkingInterval(int startIndex, int endIndex) {
        for(int i = startIndex; i <= endIndex; ++i) {
            dataPoints.get(i).setParkingStat(true);
        }
    }

    private long getTimeDifference(VehicleData p1, VehicleData p2) {
        return Math.abs(p1.getTimestamp().getTime() -
                p2.getTimestamp().getTime());
    }

    private boolean wasMoving(VehicleData p1, VehicleData p2) {
        return p1.getVelocity() > 0
                && p1.getLon() != DEFAULT_LON_LAT
                && p1.getLat() != DEFAULT_LON_LAT
                && (p1.getLon() != p2.getLon() || p1.getLat() != p2.getLat());
    }

    private String getVehicleId() {
        return vehicleId;
    }

    private Date getStartTimestamp() {
        return dataPoints.size() > 0  ? dataPoints.get(0).getTimestamp() : null;
    }

    private Date getEndTimestamp() {
        int numDataPoints = dataPoints.size();
        return numDataPoints > 0  ? dataPoints.get(numDataPoints - 1).getTimestamp() : null;
    }

    /**
     * removes all data points when the vehicle was stationary or no data was present for lon and lat
     * @return a new list of data points excluding points that the vehicle was not moving or lon and
     * lat data were not present
     */
    private ArrayList<VehicleData> getCleanDataPoints() {
        // remove all data points with velocity = 0, lon and lat = DEFAULT_VALUE (0)
        // and consecutive points with equal lon and lat
        ArrayList<VehicleData> cleanDataPoints = new ArrayList<>();
        int numDataPoints = dataPoints.size();
        if(numDataPoints > 0) {
            // check the filtering condition on the first point in the data set
            VehicleData firstPoint = dataPoints.get(0);
            if(firstPoint.getVelocity() > 0
                    && firstPoint.getLon() != DEFAULT_LON_LAT
                    && firstPoint.getLat() != DEFAULT_LON_LAT) {
                cleanDataPoints.add(firstPoint);
            }
            double previousLon = firstPoint.getLon();
            double previousLat = firstPoint.getLat();
            for(int i = 1; i < numDataPoints; ++i) {
                VehicleData thisPoint = dataPoints.get(i);
                double thisLon = thisPoint.getLon();
                double thisLat = thisPoint.getLat();
                if(thisPoint.getVelocity() > 0
                        && thisLon != DEFAULT_LON_LAT  && thisLat != DEFAULT_LON_LAT
                        && (thisLon != previousLon || thisLat != previousLat)) {
                    cleanDataPoints.add(thisPoint);
                }
                previousLon = thisLon;
                previousLat = thisLat;
            }
        }
        return cleanDataPoints;
    }

    /**
     * Sum up time driven autonomy and manual
     * @return time driven autonomy, time the vehicle was parked, and total time driven autonomy and manual
     */
    private DrivingTimeData getDrivingTimes() {
        long timeAutonomy = 0;
        long timeParking = 0;
        long timeTotal = 0;
        VehicleData previousPoint = dataPoints.get(0);
        for(int i = 1; i < dataPoints.size(); i++) {
            VehicleData thisPoint = dataPoints.get(i);
            long timeDifference = getTimeDifference(thisPoint, previousPoint);
           // filter out any two consecutive data points which are parting away for more than 2 min values
            if (timeDifference  < FILTERING_THRESHOLD_MILLISECOND) {
                if (previousPoint.getAutonomy() == AUTONOMY) {
                    timeAutonomy += timeDifference;
                }
                else if (previousPoint.getParkingStat()) {
                    timeParking += timeDifference;
                }
                timeTotal += timeDifference;
            }
            previousPoint = thisPoint;
        }
         return new DrivingTimeData(timeAutonomy, timeParking, timeTotal);
    }

    private double AverageTimeGap() {
        long timeGap = 0;
        int timeGapCount = 0;
        VehicleData previousPoint = dataPoints.get(0);
        for(int i = 1; i < dataPoints.size(); i++) {
            VehicleData thisPoint = dataPoints.get(i);
            long timeDifference = getTimeDifference(thisPoint, previousPoint);
            timeGap += timeDifference;
            timeGapCount++;
            previousPoint = thisPoint;
        }
        if(timeGap > 0) {
            return (double)(timeGap / timeGapCount);
        } else {
            return 0;
        }
    }


    /**
     * Calculate distance and velocity driven by the vehicle in autonomy and manual mode
     * @return distance driven autonomy and manual, average velocity driven autonomy, manual and total
     */
    private DistanceAndVelocityData distanceVelocityStat() {
        double distanceAutonomy = 0;
        double distanceTotal = 0;
        double velocityAutonomy = 0;
        double velocityManual = 0;
        double velocityTotal = 0;
        double velocityAutonomyCount = 0;
        double velocityManualCount = 0;
        double velocityTotalCount = 0;
        ArrayList<VehicleData> cleanDataPoints = getCleanDataPoints();
        VehicleData previousPoint = cleanDataPoints.get(0);
        for(int i = 1; i < cleanDataPoints.size(); i++) {
            VehicleData thisPoint = cleanDataPoints.get(i);
            if(!previousPoint.getParkingStat()) {
                double gpsDistance = distance(previousPoint, thisPoint);
                if(previousPoint.getAutonomy() == AUTONOMY) {
                    distanceAutonomy += gpsDistance;
                    velocityAutonomy += previousPoint.getVelocity();
                    velocityAutonomyCount++;
                } else {
                    velocityManual += previousPoint.getVelocity();
                    velocityManualCount++;
                }
                distanceTotal += gpsDistance;
                velocityTotal += previousPoint.getVelocity();
                velocityTotalCount++;
            }
            previousPoint = thisPoint;
        }
        velocityAutonomy = (velocityAutonomy == 0) ?
                DEFAULT_VELOCITY : velocityAutonomy / velocityAutonomyCount;
        velocityManual = (velocityManual == 0) ?
                DEFAULT_VELOCITY : velocityManual / velocityManualCount;
        velocityTotal = (velocityTotal == 0) ?
                DEFAULT_VELOCITY : velocityTotal / velocityTotalCount;
        return new DistanceAndVelocityData(distanceAutonomy, distanceTotal,
                velocityAutonomy, velocityManual, velocityTotal);
    }

    public StatParameters getVehicleStat() {
        DrivingTimeData drivenTime = new DrivingTimeData(0, 0, 0);
        DistanceAndVelocityData drivenDistanceVelocity = new DistanceAndVelocityData(0,
                0, DEFAULT_VELOCITY, DEFAULT_VELOCITY, DEFAULT_VELOCITY);
        if (dataPoints.size() != 0) {
            drivenTime = getDrivingTimes();
            drivenDistanceVelocity = distanceVelocityStat();
        }
        return  new StatParameters(drivenTime, drivenDistanceVelocity);
    }


    /**
     *
     * @return Json object containing all the statistics calculated for a vehicle
     */

    public JsonObject JsonVehicleStat() {
        //printParkingStatus();
        JsonObject stats = new JsonObject();

        stats.addProperty("vehicle", getVehicleId());
        stats.addProperty("points", dataPoints.size());
        if (dataPoints.size() == 0) {
            stats.addProperty("pointsAfterCleaning", 0);
            stats.addProperty("startTimestamp", "null");
            stats.addProperty("endTimestamp", "null");
            stats.addProperty("timeGapMillisecond", 0);
            stats.addProperty("autonomyMillisecond", 0);
            stats.addProperty("parkingMillisecond", 0);
            stats.addProperty("totalMillisecond", 0);
            stats.addProperty("autonomyDistanceMeter", 0);
            stats.addProperty("totalDistanceMeter", 0);
            stats.addProperty("autonomyAverageVelocity", DEFAULT_VELOCITY);
            stats.addProperty("manualAverageVelocity", DEFAULT_VELOCITY);
            stats.addProperty("totalAverageVelocity", DEFAULT_VELOCITY);
        } else {
            stats.addProperty("pointsAfterCleaning", getCleanDataPoints().size());
            stats.addProperty("startTimestamp", Objects.requireNonNull(getStartTimestamp()).toString());
            stats.addProperty("endTimestamp", Objects.requireNonNull(getEndTimestamp()).toString());
            stats.addProperty("timeGapMillisecond", BigDecimal.valueOf(
                    AverageTimeGap()).setScale(2, RoundingMode.HALF_UP));

            DrivingTimeData drivenTime = getDrivingTimes();
            stats.addProperty("autonomyMillisecond", drivenTime.timeAutonomy);
            stats.addProperty("parkingMillisecond", drivenTime.timeParking);
            stats.addProperty("totalMillisecond", drivenTime.timeTotal);
            DistanceAndVelocityData drivenDistanceVelocity = distanceVelocityStat();
            stats.addProperty("autonomyDistanceMeter", BigDecimal.valueOf(
                    drivenDistanceVelocity.distanceAutonomy).setScale(2, RoundingMode.HALF_UP));
            stats.addProperty("totalDistanceMeter", BigDecimal.valueOf(
                    drivenDistanceVelocity.distanceTotal).setScale(2, RoundingMode.HALF_UP));
            stats.addProperty("autonomyAverageVelocity", BigDecimal.valueOf(
                    drivenDistanceVelocity.velocityAutonomy).setScale(2, RoundingMode.HALF_UP));
            stats.addProperty("manualAverageVelocity", BigDecimal.valueOf(
                    drivenDistanceVelocity.velocityManual).setScale(2, RoundingMode.HALF_UP));
            stats.addProperty("totalAverageVelocity", BigDecimal.valueOf(
                    drivenDistanceVelocity.velocityTotal).setScale(2, RoundingMode.HALF_UP));
        }
        return stats;
    }

    /**
     * Calculate distance between two points in latitude and longitude
     *
     * @param p1 first data point to get lat1, lon1 start point
     * @param p2 second data point to get lat2, lon2 end point
     * @return distance in meters
     */
    private static double distance(VehicleData p1, VehicleData p2) {
        double lat1 = p1.getLat();
        double lat2 = p2.getLat();
        double lon1 = p1.getLat();
        double lon2 = p2.getLat();

        double a = (lat1 - lat2) * distPerLat(lat1);
        double b = (lon1 - lon2) * distPerLng(lat1);
        return Math.sqrt(a * a + b * b);
    }

    private static double distPerLng(double lat){
        return 0.0003121092*Math.pow(lat, 4)
                +0.0101182384*Math.pow(lat, 3)
                -17.2385140059*lat*lat
                +5.5485277537*lat+111301.967182595;
    }

    private static double distPerLat(double lat){
        return -0.000000487305676*Math.pow(lat, 4)
                -0.0033668574*Math.pow(lat, 3)
                +0.4601181791*lat*lat
                -1.4558127346*lat+110579.25662316;
    }


    public static class DistanceAndVelocityData
    {
        private final double distanceAutonomy;
        private final double distanceTotal;
        private final double velocityAutonomy;
        private final double velocityManual;
        private final double velocityTotal;

        DistanceAndVelocityData(double dA, double dT, double vA, double vM, double vT)
        {
            distanceAutonomy = dA;
            distanceTotal = dT;
            velocityAutonomy = vA;
            velocityManual = vM;
            velocityTotal = vT;
        }

        public double getDistanceAutonomy() {
            return distanceAutonomy;
        }

        public double getDistanceTotal() {
            return distanceTotal;
        }

        public double getVelocityAutonomy() {
            return velocityAutonomy;
        }

        public double getVelocityManual() {
            return velocityManual;
        }

        public double getVelocityTotal() {
            return velocityTotal;
        }
    }

    public static class DrivingTimeData
    {
        private final long timeAutonomy;
        private final long timeParking;
        private final long timeTotal;

        DrivingTimeData(long tA, long tP, long tT)
        {
            timeAutonomy = tA;
            timeParking = tP;
            timeTotal = tT;
        }
        public long getTimeAutonomy() {
            return timeAutonomy;
        }

        public long getTimeParking() {
            return timeParking;
        }

        public long getTimeTotal() {
            return timeTotal;
        }
    }

    public class StatParameters {
        private final VehicleStatisticalUtils.DrivingTimeData timeData;
        private final VehicleStatisticalUtils.DistanceAndVelocityData distanceAndVelocityData;

        private StatParameters(VehicleStatisticalUtils.DrivingTimeData timeData,
                           VehicleStatisticalUtils.DistanceAndVelocityData distanceAndVelocityData) {
            this.timeData = timeData;
            this.distanceAndVelocityData = distanceAndVelocityData;

        }

        public DrivingTimeData getTimeData() {
            return timeData;
        }

        public DistanceAndVelocityData getDistanceAndVelocityData() {
            return distanceAndVelocityData;
        }
    }

}
