/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.geo.model;

import org.apache.commons.math3.util.FastMath;

public class Latitude implements Comparable<Latitude> {
    private double radians;

    public Latitude(double radians) {
        if(radians < -FastMath.PI / 2 || radians > FastMath.PI / 2) {
            throw new IllegalArgumentException(String.format("Latitude must be between -pi/2 and pi/2 radians, got: %f",
                    radians));
        }
        this.radians = radians;
    }

    public static Latitude getLatitudeFromDegrees(double degrees) {
        if(degrees < -90 || degrees > 90) {
            throw new IllegalArgumentException(
                    String.format("Latitude must be between -90 and 90 degrees, got: %f", degrees));
        }
        return new Latitude(FastMath.toRadians(degrees));
    }

    public double getRadians() {
        return radians;
    }

    public double getDegrees() {
        return FastMath.toDegrees(radians);
    }

    @Override
    public int compareTo(Latitude latitude) {
        return Double.compare(radians, latitude.radians);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Latitude) {
            return Double.compare(radians, ((Latitude) obj).radians) == 0;
        }
        return false;
    }

    @Override
    public String toString() {
        return String.format("%fr (%fd)", radians, getDegrees());
    }
}
