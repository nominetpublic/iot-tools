/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;

import com.google.gson.JsonObject;
import com.google.gson.stream.JsonWriter;
import io.searchbox.client.JestResult;
import io.searchbox.core.Search;

import org.elasticsearch.index.query.*;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.everit.json.schema.ValidationException;
import org.joda.time.Instant;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import spark.Service;
import uk.nominet.iot.api.datastore.errors.IllegalQueryException;
import uk.nominet.iot.api.datastore.errors.QueryFailureException;

import uk.nominet.iot.api.datastore.query.BulkQuery;
import uk.nominet.iot.api.datastore.utils.ElasticSearchUtils;
import uk.nominet.iot.api.datastore.utils.FleetStatisticalUtils;
import uk.nominet.iot.api.datastore.utils.VehicleStatisticalUtils;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.http.APIController;
import uk.nominet.iot.model.api.APIException;
import uk.nominet.iot.model.api.ExceptionHandler;


import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import java.nio.charset.StandardCharsets;
import java.util.*;

import static uk.nominet.iot.utils.ConversionUtils.asciiToHex;

public class StatisticalController implements APIController {
    public enum OutputFormat {
        CSV, JSON
    }

    private static final Map<String, String> VEHICLE_FIELDS = new LinkedHashMap<>();
    static {
        VEHICLE_FIELDS.put("autonomy", "value_int");
        VEHICLE_FIELDS.put("location", "value_lat_long");
        VEHICLE_FIELDS.put("velocity", "value_double");
        VEHICLE_FIELDS.put("lateral-acceleration", "value_double");
        VEHICLE_FIELDS.put("heading", "value_double");
        VEHICLE_FIELDS.put("steering-angle", "value_double");
        VEHICLE_FIELDS.put("speed-limit", "value_double");
        VEHICLE_FIELDS.put("brake-magnitude", "value_double");
        VEHICLE_FIELDS.put("battery-charge", "value_double");
        VEHICLE_FIELDS.put("route", "value_text");

    }

    private static String[] getFieldNames(String fieldName, String valueType) {
        if ("value_lat_long".equals(valueType)) {
            return new String[]{fieldName + "-latitude", fieldName + "-longitude"};
        }
        return new String[]{fieldName};
    }

    private static void parseValues(JsonObject input, String fieldName, String valueType, JsonObject output) {
        if ("value_lat_long".equals(valueType)) {
            output.add(fieldName + "-latitude", input.getAsJsonObject(valueType).
                                                                                        getAsJsonPrimitive("lat"));
            output.add(fieldName + "-longitude", input.getAsJsonObject(valueType).
                                                                                         getAsJsonPrimitive("lon"));
        } else {
            output.add(fieldName, input.getAsJsonPrimitive(valueType));
        }
    }

    private static final Logger LOG = LoggerFactory.getLogger(StatisticalController.class);


    private Instant checkRequestDateParameter(String parameter, Request request) {
        if(request.queryParams(parameter) == null || request.queryParams(parameter).isEmpty()) {
            throw new IllegalQueryException(parameter, request.queryParams(parameter), "it is a required parameter");
        }
        try {
            return Instant.parse(request.queryParams(parameter));
        } catch(IllegalArgumentException ex) {
            throw new IllegalQueryException(parameter, request.queryParams(parameter), "is not an ISO8601 format date");
        }
    }

    private void outputResult(JsonObject result, Gson gson, JsonWriter jsonPrinter) {
        gson.toJson(result, jsonPrinter);
    }

    /**
     * Calculate interval statistics (for individual vehicles and fleet) about
     * autonomy such as how much distance, time and average
     * speed is driven autonomously vs total distance and time
     * Output in JSON format, writing them to an OutputStream.
     * Output list [vehicleId, #data points, #data point after cleaning,
     * first time stamp, last time stamp, autonomy time, parking time,
     * total time, autonomy distance, total distance, autonomy avg velocity,
     * manual avg velocity and total avg velocity]
     * @param query the query parameter to select metrics on
     * @param output an output stream that the results will be written to
     * @return an empty string (required to prevent Spark from displaying a not found error
     */
    private String bulkVehicleTimeseriesQuery(BulkQuery query, OutputStream output)
            throws APIException {

        JsonObject response = null;
        String index;
        try {
            index = ElasticSearchUtils.INDEX_PREFIX + asciiToHex(query.getUser());
        } catch (Exception e) {
            throw new APIException(400, "Could not identify elasticsearch index", e);
        }

        //Parent Query
        BoolQueryBuilder booleanQuery = QueryBuilders.boolQuery();
        RangeQueryBuilder rangeQuery = QueryBuilders
                .rangeQuery(ElasticSearchUtils.TIMESTAMP_FIELD)
                .gte(query.getStart().toString())
                .lte(query.getEnd().toString());
        TermsQueryBuilder streamKeyQuery = QueryBuilders.termsQuery("streamKey", VEHICLE_FIELDS.keySet());
        booleanQuery = booleanQuery.filter(rangeQuery).must(streamKeyQuery);

        //Aggregation
        DateHistogramAggregationBuilder agg = AggregationBuilders
                .dateHistogram("timeseries")
                .field("timestamp")
                .dateHistogramInterval(new DateHistogramInterval(query.getInterval()))
                .subAggregation(
                        AggregationBuilders
                                .topHits("time_intervals")
                                .size(ElasticSearchUtils.MAX_RESULT_COUNT)
                                .sort("timestamp")
                );

        SearchSourceBuilder searchBuilder = new SearchSourceBuilder()
                .query(booleanQuery)
                .aggregation(agg)
                .size(0);
        searchBuilder.sort(SortBuilders.fieldSort(ElasticSearchUtils.TIMESTAMP_FIELD).order(SortOrder.ASC));

        return "";
    }

    private void addDeviceStream(String deviceIds) {
        VEHICLE_FIELDS.clear();
        VEHICLE_FIELDS.put(deviceIds+".autonomy", "value_int");
        VEHICLE_FIELDS.put(deviceIds+".location", "value_lat_long");
        VEHICLE_FIELDS.put(deviceIds+".velocity", "value_double");
        VEHICLE_FIELDS.put(deviceIds+".lateral-acceleration", "value_double");
        VEHICLE_FIELDS.put(deviceIds+".heading", "value_double");
        VEHICLE_FIELDS.put(deviceIds+".steering-angle", "value_double");
        VEHICLE_FIELDS.put(deviceIds+".speed-limit", "value_double");
        VEHICLE_FIELDS.put(deviceIds+".brake-magnitude", "value_double");
        VEHICLE_FIELDS.put(deviceIds+".battery-charge", "value_double");
        VEHICLE_FIELDS.put(deviceIds+".route", "value_text");
    }


    /**
     * Calculate statistics (for individual vehicles and fleet) about
     * autonomy such as how much distance, time and average
     * speed is driven autonomously vs total distance and time
     * Output in JSON format, writing them to an OutputStream.
     * Output list [vehicleId, #data points, #data point after cleaning,
     * first time stamp, last time stamp, autonomy time, parking time,
     * total time, autonomy distance, total distance, autonomy avg velocity,
     * manual avg velocity and total avg velocity]
     * @param query the query parameter to select metrics on
     * @param output an output stream that the results will be written to
     * @return an empty string (required to prevent Spark from displaying a not found error
     * @throws IOException probably caused by either fetching or writing the results
     * @throws QueryFailureException thrown if the generated query(s) fail on the ElasticSearch servers
     */
    private String bulkVehicleQuery(BulkQuery query, OutputStream output)
            throws IOException, QueryFailureException, APIException {
        String index;
        try {
            index = ElasticSearchUtils.INDEX_PREFIX + asciiToHex(query.getUser());
        } catch (Exception e) {
            throw new APIException(400, "Could not identify elasticsearch index", e);
        }

        Writer outputWriter = new OutputStreamWriter(output, StandardCharsets.UTF_8);
        JsonWriter jsonPrinter = new JsonWriter(outputWriter);
        jsonPrinter.beginObject().name("result").value("ok")
                .name("response").beginObject().name("vehicles").beginArray();

        List<String> deviceIds;
        if(query.getDeviceIds() != null && query.getDeviceIds().size() >= 1) {
            deviceIds = query.getDeviceIds();
        } else {
            deviceIds = findDevices(index);
        }

        if(deviceIds != null) {
            if (query.getInterval() == null) {
                FleetStatisticalUtils fleetStat = new FleetStatisticalUtils();
                for (String vehicleId : deviceIds) {
                    addDeviceStream(vehicleId);
                    VehicleStatisticalUtils.StatParameters vS = bulkSingleVehicleQuery(query, vehicleId,
                                                                                       index, output, jsonPrinter);
                    fleetStat.setVehicleStats(vehicleId, vS);
                }
                jsonPrinter.endArray();
                jsonPrinter.name("fleet");
                Gson gson = new Gson();
                outputResult(fleetStat.JsonFleetStat(), gson, jsonPrinter);
            } else {
                bulkVehicleTimeseriesQuery(query, output);
            }
        }

        jsonPrinter.endObject().endObject().close();
        return "";
    }

    private ArrayList<String> findDevices(String index) throws IOException, QueryFailureException{
        ArrayList<String> deviceIds = new ArrayList<>();
        TermsQueryBuilder queryBuilder =
                new TermsQueryBuilder("type", "vehicle");
        SearchSourceBuilder query = new SearchSourceBuilder().query(queryBuilder);

        Search search = new Search.Builder(query.toString())
                .addIndex(index)
                .addType(ElasticSearchUtils.DEVICE_TYPE)
                .build();

        JestResult result = ElasticsearchDriver.getClient().execute(search);
        if(!result.isSucceeded()) {
            System.out.println(result.getErrorMessage());
            throw new QueryFailureException(result);
        }

        JsonObject hitsObject = result.getJsonObject().getAsJsonObject("hits");
        JsonArray hits = new JsonArray();
        if(hitsObject != null && hitsObject.has("hits")) {
            hits = hitsObject.getAsJsonArray("hits");
        }

        for(int i = 0; i < hits.size(); ++i){
            String vehicleId = hits.get(i).getAsJsonObject().get("_id").getAsString();
            deviceIds.add(vehicleId);
        }
        return deviceIds;
    }


    private VehicleStatisticalUtils.StatParameters bulkSingleVehicleQuery(BulkQuery query, String vehicleId,
                                                                          String index, OutputStream output,
                                                                          JsonWriter jsonPrinter)
            throws IOException, QueryFailureException {
        JsonObject response = null;

        BoolQueryBuilder booleanQuery = QueryBuilders.boolQuery();
        RangeQueryBuilder rangeQuery = QueryBuilders
                .rangeQuery(ElasticSearchUtils.TIMESTAMP_FIELD)
                .gte(query.getStart().toString())
                .lte(query.getEnd().toString());
        TermsQueryBuilder streamKeyQuery = QueryBuilders.termsQuery("streamKey", VEHICLE_FIELDS.keySet());
        booleanQuery = booleanQuery.must(rangeQuery).must(streamKeyQuery);

        VehicleStatisticalUtils vehicleStat = new VehicleStatisticalUtils(vehicleId);
        Gson gson = new Gson();

        SearchSourceBuilder searchBuilder = new SearchSourceBuilder()
                .query(booleanQuery)
                .size(ElasticSearchUtils.MAX_RESULT_COUNT);
        searchBuilder.sort(SortBuilders.fieldSort(ElasticSearchUtils.TIMESTAMP_FIELD).order(SortOrder.ASC));

        ElasticSearchUtils.queryElasticSearchHitsAllCallback(ElasticsearchDriver.getClient(), searchBuilder, index,
                ElasticSearchUtils.METRIC_TYPE, null, (lastValues, hits) ->
        {
            boolean isLast = hits.size() < ElasticSearchUtils.MAX_RESULT_COUNT;
            String lastTimestamp = null;
            JsonObject values = lastValues;
            if(lastValues != null && lastValues.has("timestamp")) {
                lastTimestamp = lastValues.get("timestamp").getAsString();
            }
            // The way the query currently works, this code assumes that if any results come back, they must be
            // relevant (i.e., within the specified time window and for one of the queried vehicles). If that assumption
            // is no longer true, this code will need to be rewritten!
            if(hits.size() > 0) {
                if(lastTimestamp == null) {
                    lastTimestamp = hits.get(0).getAsJsonObject().get("_source").getAsJsonObject()
                            .get(ElasticSearchUtils.TIMESTAMP_FIELD).getAsString()
                            .replaceAll("(:[0-9]{2})Z", "$1.000Z");
                }
                if(values == null) {
                    values = new JsonObject();
                }
                if(!values.has("timestamp")) {
                    values.addProperty("timestamp", lastTimestamp);
                }

                for(int i = 0; i < hits.size(); i++) {
                    JsonObject source = hits.get(i).getAsJsonObject().get("_source").getAsJsonObject();
                    String currentTimestamp = source.get(ElasticSearchUtils.TIMESTAMP_FIELD).getAsString();
                    currentTimestamp = currentTimestamp.replaceAll("(:[0-9]{2})Z", "$1.000Z");
                    String streamKey = source.getAsJsonPrimitive("streamKey").getAsString();

                    // This works because the results are sorted by timestamp
                    if(!currentTimestamp.equalsIgnoreCase(lastTimestamp)) {
                        vehicleStat.setDataPoints(values);
                        values = new JsonObject();
                        values.addProperty("timestamp", currentTimestamp);
                        lastTimestamp = currentTimestamp;
                    }
                    if(!streamKey.contains(vehicleId)) {
                        vehicleStat.setDataPoints(values);
                        values = new JsonObject();
                        values.addProperty("timestamp", lastTimestamp);
                    }
                    parseValues(source, streamKey.replace(vehicleId + ".", ""),
                            VEHICLE_FIELDS.get(streamKey), values);
                }
            }
            if(isLast) {
                if(values != null) {
                    // write the final result if it has at least a timestamp
                    if (values.has("timestamp")) {
                        vehicleStat.setDataPoints(values);
                        values = null; // This shouldn't affect anything, but just in case...
                    }
                }
                // finish JSON
                outputResult(vehicleStat.JsonVehicleStat(), gson, jsonPrinter);
            }
            // pass the remaining un-used values (if there are any) on to the next iteration
            return values;
        });

        return vehicleStat.getVehicleStat();
    }

    private String handleBulkVehicleRequest(Request req, Response res) {
        res.type("application/json");


        JSONObject queryObject;
        try {
            queryObject = new JSONObject(new JSONTokener(req.body()));
        } catch (JSONException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }
        BulkQuery query;

        try {
            OutputFormat format;
            String user = req.attribute("introspected_username");
            query = new BulkQuery(queryObject, user);
            return bulkVehicleQuery(query, res.raw().getOutputStream());
        } catch (ValidationException e) {
            return ExceptionHandler.handle(res, new IllegalQueryException(e.getKeyword(), queryObject.toString(),
                    e.getErrorMessage()), e.getAllMessages().toString());
        } catch (Exception e) {
            e.printStackTrace();
            return ExceptionHandler.handle(res, e);
        }
    }

    public void publishAPI(Service http, String path) {
        http.path(path, () -> {
            http.get("/vehicle", this::handleBulkVehicleRequest);
            http.post("/vehicle", this::handleBulkVehicleRequest);
        });
    }

}
