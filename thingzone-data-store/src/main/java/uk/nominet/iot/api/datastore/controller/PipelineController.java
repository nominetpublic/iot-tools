/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.controller;

import org.everit.json.schema.ValidationException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import spark.Request;
import spark.Response;
import spark.Service;
import uk.nominet.iot.api.datastore.DatastoreApp;
import uk.nominet.iot.api.datastore.controller.pipeline.*;
import uk.nominet.iot.api.datastore.errors.IllegalQueryException;
import uk.nominet.iot.api.datastore.query.PipelineQuery;
import uk.nominet.iot.http.APIController;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.api.ExceptionHandler;


public class PipelineController implements APIController {
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
    private final DatastoreApp api;

    @Override
    public void publishAPI(Service http, String path) {
        http.path(path, () -> {
            http.post("/metrics/query/agg", this::metrics);
            http.post("/metrics/query/list", this::listMetrics);
            http.post("/metrics/query/geo", this::geoMetrics);
            http.post("/images/query/list", this::images);
            http.post("/event/query/geo", this::eventGeo);
            http.post("/event/query/list", this::eventList);
            http.post("/stream/query/common", this::commonStreams);
            http.post("/combination/query/list", this::combinationList);
            http.post("/combination/query/geo", this::combinationGeo);
        });
    }

    private String combinationGeo(Request req, Response res) {
        JSONObject queryObject;
        try {
            queryObject = new JSONObject(new JSONTokener(req.body()));
        } catch (JSONException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

        try {
            PipelineQuery query = new PipelineQuery(queryObject, req.attribute("introspected_username"));

            PipelineExecutor pipelineExecutor = new PipelineExecutor(api, query.isIntermediate());

            if (query.getDeviceList() != null) pipelineExecutor.add(new GeoSearchPipe(query.getDeviceList()));
            else if (query.getDeviceQuery() != null) pipelineExecutor.add(new GeoSearchPipe(query.getDeviceQuery()));

            if (query.getStreamList() != null) pipelineExecutor.add(new StreamSearchPipe(query.getStreamList()));
            else pipelineExecutor.add(new StreamSearchPipe(query.getStreamListQuery()));

            pipelineExecutor.add(new CombinationGeoPipe(query.getCombinationGeoQuery()));

            // return searchLatest(query);
            return pipelineExecutor.execute().toString();
        } catch (ValidationException e) {
            return ExceptionHandler.handle(res, new IllegalQueryException(e.getKeyword(), queryObject.toString(),
                    e.getErrorMessage()), e.getAllMessages().toString());
        } catch (IllegalQueryException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return ExceptionHandler.handle(res, e, e.getMessage());
        }
    }

    /**
     * Combination list query
     *
     * @param req request
     * @param res response
     * @return response
     */
    private String combinationList(Request req, Response res) {
        JSONObject queryObject;
        try {
            queryObject = new JSONObject(new JSONTokener(req.body()));
        } catch (JSONException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

        try {
            PipelineQuery query = new PipelineQuery(queryObject, req.attribute("introspected_username"));

            PipelineExecutor pipelineExecutor = new PipelineExecutor(api, query.isIntermediate());

            if (query.getDeviceList() != null) pipelineExecutor.add(new GeoSearchPipe(query.getDeviceList()));
            else if (query.getDeviceQuery() != null) pipelineExecutor.add(new GeoSearchPipe(query.getDeviceQuery()));

            if (query.getStreamList() != null) pipelineExecutor.add(new StreamSearchPipe(query.getStreamList()));
            else pipelineExecutor.add(new StreamSearchPipe(query.getStreamListQuery()));

            pipelineExecutor.add(new CombinationListPipe(query.getCombinationListQuery()));

            // return searchLatest(query);
            return pipelineExecutor.execute().toString();
        } catch (ValidationException e) {
            return ExceptionHandler.handle(res, new IllegalQueryException(e.getKeyword(), queryObject.toString(),
                    e.getErrorMessage()), e.getAllMessages().toString());
        } catch (IllegalQueryException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return ExceptionHandler.handle(res, e, e.getMessage());
        }
    }

    /**
     * Event list pipeline query
     *
     * @param req request
     * @param res response
     * @return response
     */
    private String eventList(Request req, Response res) {
        JSONObject queryObject;
        try {
            queryObject = new JSONObject(new JSONTokener(req.body()));
        } catch (JSONException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

        try {
            PipelineQuery query = new PipelineQuery(queryObject, req.attribute("introspected_username"));

            PipelineExecutor pipelineExecutor = new PipelineExecutor(api, query.isIntermediate());

            if (query.getDeviceList() != null) pipelineExecutor.add(new GeoSearchPipe(query.getDeviceList()));
            else if (query.getDeviceQuery() != null) pipelineExecutor.add(new GeoSearchPipe(query.getDeviceQuery()));

            if (query.getStreamList() != null) pipelineExecutor.add(new StreamSearchPipe(query.getStreamList()));
            else pipelineExecutor.add(new StreamSearchPipe(query.getStreamListQuery()));

            pipelineExecutor.add(new EventListPipe(query.getEventListQuery()));

            // return searchLatest(query);
            return pipelineExecutor.execute().toString();
        } catch (ValidationException e) {
            return ExceptionHandler.handle(res, new IllegalQueryException(e.getKeyword(), queryObject.toString(),
                    e.getErrorMessage()), e.getAllMessages().toString());
        } catch (IllegalQueryException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return ExceptionHandler.handle(res, e, e.getMessage());
        }
    }

    /**
     * Event geo pipeline query
     *
     * @param req request
     * @param res response
     * @return response
     */
    private String eventGeo(Request req, Response res) {
        JSONObject queryObject;
        try {
            queryObject = new JSONObject(new JSONTokener(req.body()));
        } catch (JSONException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

        try {
            PipelineQuery query = new PipelineQuery(queryObject, req.attribute("introspected_username"));

            PipelineExecutor pipelineExecutor = new PipelineExecutor(api, query.isIntermediate());

            if (query.getDeviceList() != null) pipelineExecutor.add(new GeoSearchPipe(query.getDeviceList()));
            else if (query.getDeviceQuery() != null) pipelineExecutor.add(new GeoSearchPipe(query.getDeviceQuery()));

            if (query.getStreamList() != null) pipelineExecutor.add(new StreamSearchPipe(query.getStreamList()));
            else pipelineExecutor.add(new StreamSearchPipe(query.getStreamListQuery()));

            pipelineExecutor.add(new EventGeoPipe(query.getEventGeoQuery()));

            // return searchLatest(query);
            return pipelineExecutor.execute().toString();
        } catch (ValidationException e) {
            return ExceptionHandler.handle(res, new IllegalQueryException(e.getKeyword(), queryObject.toString(),
                    e.getErrorMessage()), e.getAllMessages().toString());
        } catch (IllegalQueryException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return ExceptionHandler.handle(res, e, e.getMessage());
        }
    }

    public PipelineController(DatastoreApp api) {
        this.api = api;
    }

    /**
     * DataStreamValues query
     *
     * @param req request
     * @param res response
     * @return response
     */
    private String listMetrics(Request req, Response res) {
        JSONObject queryObject;
        try {
            queryObject = new JSONObject(new JSONTokener(req.body()));
        } catch (JSONException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

        try {
            PipelineQuery query = new PipelineQuery(queryObject, req.attribute("introspected_username"));

            PipelineExecutor pipelineExecutor = new PipelineExecutor(api, query.isIntermediate());

            if (query.getDeviceList() != null) pipelineExecutor.add(new GeoSearchPipe(query.getDeviceList()));
            else if (query.getDeviceQuery() != null) pipelineExecutor.add(new GeoSearchPipe(query.getDeviceQuery()));

            if (query.getStreamList() != null) pipelineExecutor.add(new StreamSearchPipe(query.getStreamList()));
            else pipelineExecutor.add(new StreamSearchPipe(query.getStreamListQuery()));

            pipelineExecutor.add(new MetricsListPipe(query.getMetricsListQuery()));

            // return searchLatest(query);
            return pipelineExecutor.execute().toString();
        } catch (ValidationException e) {
            return ExceptionHandler.handle(res, new IllegalQueryException(e.getKeyword(), queryObject.toString(),
                    e.getErrorMessage()), e.getAllMessages().toString());
        } catch (IllegalQueryException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return ExceptionHandler.handle(res, e, e.getMessage());
        }


    }

    /**
     * DataStreamValues query
     *
     * @param req request
     * @param res response
     * @return response
     */
    private String geoMetrics(Request req, Response res) {
        JSONObject queryObject;
        try {
            queryObject = new JSONObject(new JSONTokener(req.body()));
        } catch (JSONException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

        try {
            PipelineQuery query = new PipelineQuery(queryObject, req.attribute("introspected_username"));

            PipelineExecutor pipelineExecutor = new PipelineExecutor(api, query.isIntermediate());

            if (query.getDeviceList() != null) pipelineExecutor.add(new GeoSearchPipe(query.getDeviceList()));
            else if (query.getDeviceQuery() != null) pipelineExecutor.add(new GeoSearchPipe(query.getDeviceQuery()));

            if (query.getStreamList() != null) pipelineExecutor.add(new StreamSearchPipe(query.getStreamList()));
            else pipelineExecutor.add(new StreamSearchPipe(query.getStreamListQuery()));

            pipelineExecutor.add(new GeoMetricsPipe(query.getMetricsGeoQuery()));

            return pipelineExecutor.execute().toString();
        } catch (ValidationException e) {
            return ExceptionHandler.handle(res, new IllegalQueryException(e.getKeyword(), queryObject.toString(),
                    e.getErrorMessage()), e.getAllMessages().toString());
        } catch (IllegalQueryException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return ExceptionHandler.handle(res, e, e.getMessage());
        }


    }


    /**
     * DataStreamValues query
     *
     * @param req request
     * @param res response
     * @return response
     */
    private String metrics(Request req, Response res) {
        JSONObject queryObject;
        try {
            queryObject = new JSONObject(new JSONTokener(req.body()));
        } catch (JSONException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

        try {
            PipelineQuery query = new PipelineQuery(queryObject, req.attribute("introspected_username"));

            PipelineExecutor pipelineExecutor = new PipelineExecutor(api, query.isIntermediate());

            if (query.getDeviceList() != null) pipelineExecutor.add(new GeoSearchPipe(query.getDeviceList()));
            else if (query.getDeviceQuery() != null) pipelineExecutor.add(new GeoSearchPipe(query.getDeviceQuery()));

            if (query.getStreamList() != null) pipelineExecutor.add(new StreamSearchPipe(query.getStreamList()));
            else pipelineExecutor.add(new StreamSearchPipe(query.getStreamListQuery()));

            pipelineExecutor.add(new TimeseriesMetricsAggPipe(query.getMetricsAggQuery()));

            // return searchLatest(query);
            return pipelineExecutor.execute().toString();
        } catch (ValidationException e) {
            return ExceptionHandler.handle(res, new IllegalQueryException(e.getKeyword(), queryObject.toString(),
                    e.getErrorMessage()), e.getAllMessages().toString());
        } catch (IllegalQueryException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return ExceptionHandler.handle(res, e, e.getMessage());
        }


    }


    /**
     * DataStreamValues query
     *
     * @param req request
     * @param res response
     * @return response
     */
    private String images(Request req, Response res) {
        JSONObject queryObject;
        try {
            queryObject = new JSONObject(new JSONTokener(req.body()));
        } catch (JSONException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

        try {
            PipelineQuery query = new PipelineQuery(queryObject, req.attribute("introspected_username"));

            PipelineExecutor pipelineExecutor = new PipelineExecutor(api, query.isIntermediate());

            if (query.getDeviceList() != null) pipelineExecutor.add(new GeoSearchPipe(query.getDeviceList()));
            else if (query.getDeviceQuery() != null) pipelineExecutor.add(new GeoSearchPipe(query.getDeviceQuery()));

            if (query.getStreamList() != null) pipelineExecutor.add(new StreamSearchPipe(query.getStreamList()));
            else pipelineExecutor.add(new StreamSearchPipe(query.getStreamListQuery()));

            pipelineExecutor.add(new TimeseriesImageListPipe(query.getImagesListQuery()));

            // return searchLatest(query);
            return pipelineExecutor.execute().toString();
        } catch (ValidationException e) {
            return ExceptionHandler.handle(res, new IllegalQueryException(e.getKeyword(), queryObject.toString(),
                    e.getErrorMessage()), e.getAllMessages().toString());
        } catch (IllegalQueryException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return ExceptionHandler.handle(res, e, e.getMessage());
        }


    }

    /**
     * DataStreamValues query
     *
     * @param req request
     * @param res response
     * @return response
     */
    private String alertsList(Request req, Response res) {
        JSONObject queryObject;
        try {
            queryObject = new JSONObject(new JSONTokener(req.body()));
        } catch (JSONException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

        try {
            PipelineQuery query = new PipelineQuery(queryObject, req.attribute("introspected_username"));

            PipelineExecutor pipelineExecutor = new PipelineExecutor(api, query.isIntermediate());

            if (query.getDeviceList() != null) pipelineExecutor.add(new GeoSearchPipe(query.getDeviceList()));
            else if (query.getDeviceQuery() != null) pipelineExecutor.add(new GeoSearchPipe(query.getDeviceQuery()));

            pipelineExecutor.add(new TimeseriesAlertListPipe(query.getEventListQuery()));

            // return searchLatest(query);
            return pipelineExecutor.execute().toString();
        } catch (ValidationException e) {
            return ExceptionHandler.handle(res, new IllegalQueryException(e.getKeyword(), queryObject.toString(),
                    e.getErrorMessage()), e.getAllMessages().toString());
        } catch (IllegalQueryException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return ExceptionHandler.handle(res, e, e.getMessage());
        }


    }


    /**
     * DataStreamValues query
     *
     * @param req request
     * @param res response
     * @return response
     */
    private String alertsGeo(Request req, Response res) {
        JSONObject queryObject;
        try {
            queryObject = new JSONObject(new JSONTokener(req.body()));
        } catch (JSONException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

        try {
            PipelineQuery query = new PipelineQuery(queryObject, req.attribute("introspected_username"));

            PipelineExecutor pipelineExecutor = new PipelineExecutor(api, query.isIntermediate());

            if (query.getDeviceList() != null) pipelineExecutor.add(new GeoSearchPipe(query.getDeviceList()));
            else if (query.getDeviceQuery() != null) pipelineExecutor.add(new GeoSearchPipe(query.getDeviceQuery()));

            pipelineExecutor.add(new TimeseriesAlertGeoPipe(query.getEventGeoQuery()));

            // return searchLatest(query);
            return pipelineExecutor.execute().toString();
        } catch (ValidationException e) {
            return ExceptionHandler.handle(res, new IllegalQueryException(e.getKeyword(), queryObject.toString(),
                    e.getErrorMessage()), e.getAllMessages().toString());
        } catch (IllegalQueryException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return ExceptionHandler.handle(res, e, e.getMessage());
        }


    }


    /**
     * Common streams query
     *
     * @param req request
     * @param res response
     * @return response
     */
    private String commonStreams(Request req, Response res) {
        JSONObject queryObject;
        try {
            queryObject = new JSONObject(new JSONTokener(req.body()));
        } catch (JSONException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

        try {
            PipelineQuery query = new PipelineQuery(queryObject, req.attribute("introspected_username"));

            PipelineExecutor pipelineExecutor = new PipelineExecutor(api, query.isIntermediate());

            if (query.getDeviceList() != null) pipelineExecutor.add(new GeoSearchPipe(query.getDeviceList()));
            else if (query.getDeviceQuery() != null) pipelineExecutor.add(new GeoSearchPipe(query.getDeviceQuery()));

            pipelineExecutor.add(new CommonStreamsPipe(query.getCommonStreamsQuery()));

            // return searchLatest(query);
            return pipelineExecutor.execute().toString();
        } catch (ValidationException e) {
            return ExceptionHandler.handle(res, new IllegalQueryException(e.getKeyword(), queryObject.toString(),
                    e.getErrorMessage()), e.getAllMessages().toString());
        } catch (IllegalQueryException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return ExceptionHandler.handle(res, e, e.getMessage());
        }
    }


}
