/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.geo.model;

import org.apache.commons.math3.util.FastMath;

public class Longitude implements Comparable<Longitude> {
    private final double radians;

    public Longitude(double radians) {
        radians = ((radians + FastMath.PI) % (2 * FastMath.PI)) - FastMath.PI; // normalise to 0--360 range
        this.radians = radians;
    }

    public static Longitude getLongitudeFromDegrees(double degrees) {
        return new Longitude(FastMath.toRadians(degrees));
    }

    public double getRadians() {
        return radians;
    }

    public double getDegrees() {
        return FastMath.toDegrees(radians);
    }

    @Override
    public int compareTo(Longitude longitude) {
        return Double.compare(radians, longitude.radians);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Longitude) {
            return Double.compare(radians, ((Longitude) obj).radians) == 0;
        }
        return false;
    }

    @Override
    public String toString() {
        return String.format("%fr (%fd)", radians, getDegrees());
    }
}
