/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.query;

import org.elasticsearch.index.query.*;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval;
import org.elasticsearch.search.sort.SortOrder;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DatesQuery extends DatastoreQuery<Object> implements ElasticsearchBasicQuery {
    private final List<String> streams = new ArrayList<>();
    private String from;
    private String to;

    public DatesQuery(JSONObject requestBody, String user) throws Exception {
        super(requestBody, user);
        getSchema().validate(requestBody);
        setUser(user);
        if (requestBody.has("streams")) {
            requestBody.getJSONArray("streams").forEach(stream -> addStream(stream.toString()));
        }
        if (requestBody.has("from"))
            setFrom(requestBody.getString("from"));
        if (requestBody.has("to"))
            setTo(requestBody.getString("to"));
    }


    @Override
    public QueryBuilder getQueryBuilder() {
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        TermsQueryBuilder termsQueryBuilder = new TermsQueryBuilder("streamKey.keyword", streams);
        boolQueryBuilder.must(termsQueryBuilder);

        if (from != null && to != null) {
            RangeQueryBuilder rangeQueryBuilder = new RangeQueryBuilder("timestamp");
            rangeQueryBuilder = rangeQueryBuilder.gte(from);
            rangeQueryBuilder = rangeQueryBuilder.lte(to);
            boolQueryBuilder.must(rangeQueryBuilder);
        }
        return boolQueryBuilder;
    }

    @Override
    public AggregationBuilder getAggregationBuilder() {
        return AggregationBuilders.dateHistogram("dates")
                .field("timestamp")
                .dateHistogramInterval(DateHistogramInterval.DAY)
                .minDocCount(1);
    }

    @Override
    public String getSortName() {
        return "timestamp";
    }

    @Override
    public SortOrder getSortOrder() {
        return SortOrder.ASC;
    }

    public List<String> getStream() {
        return streams;
    }

    private void addStream(String stream) {
        this.streams.add(stream);
    }

    public String getFrom() {
        return from;
    }

    private void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    private void setTo(String to) {
        this.to = to;
    }

    @Override
    public Object execute() {
        return null;
    }
}
