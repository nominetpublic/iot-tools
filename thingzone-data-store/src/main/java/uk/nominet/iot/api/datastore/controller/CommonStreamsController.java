/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.controller;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.core.search.aggregation.TermsAggregation;
import io.searchbox.core.search.aggregation.TopHitsAggregation;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.everit.json.schema.ValidationException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import spark.Service;
import uk.nominet.iot.api.datastore.errors.IllegalQueryException;
import uk.nominet.iot.api.datastore.query.CommonStreamsQuery;
import uk.nominet.iot.api.datastore.utils.ElasticSearchUtils;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.http.APIController;
import uk.nominet.iot.model.api.APIException;
import uk.nominet.iot.model.api.ExceptionHandler;
import uk.nominet.iot.model.api.ResponseBuilder;
import uk.nominet.iot.model.registry.cache.DataStreamDocument;

import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

import static uk.nominet.iot.utils.ConversionUtils.asciiToHex;

public class CommonStreamsController implements APIController {

    private static final Logger LOG = LoggerFactory.getLogger(CommonStreamsController.class);


    @Override
    public void publishAPI(Service http, String path) {
        http.path(path, () -> http.post("/common", this::handleCommonStreamsRequest));
    }

    /**
     * Handle common streams request
     * @param req request
     * @param res response
     * @return response
     */
    private String handleCommonStreamsRequest(Request req, Response res) {
        JSONObject queryObject;
        try {
            queryObject = new JSONObject(new JSONTokener(req.body()));
        } catch (JSONException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }


        try {
            CommonStreamsQuery query = new CommonStreamsQuery(queryObject, req.attribute("introspected_username"));
            return listCommonStreams(query).toString();
        } catch (ValidationException e) {
            return ExceptionHandler.handle(res,
                    new IllegalQueryException(e.getKeyword(), queryObject.toString(),
                            e.getErrorMessage()), e.getAllMessages().toString());
        } catch (IllegalQueryException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return ExceptionHandler.handle(res, e, "Unknown server error");
        }
    }

    public JsonObject listCommonStreams(CommonStreamsQuery query) throws APIException {

        ResponseBuilder response;
        String index;
        try {
            index = ElasticSearchUtils.INDEX_PREFIX + asciiToHex(query.getUser());
        } catch (Exception e) {
            throw new APIException(400, "Could not identify elasticsearch index", e);
        }


        SearchSourceBuilder searchBuilder = new SearchSourceBuilder()
                                                    .query(query.getQueryBuilder())
                                                    .aggregation(query.getAggregationBuilder())
                                                    .size(0);


        Search search = new Search.Builder(searchBuilder.toString())
                                .addIndex(index)
                                .addType(ElasticSearchUtils.STREAM_TYPE)
                                .build();


        SearchResult result;
        try {
            result = ElasticsearchDriver.getClient().execute(search);
            if (result.isSucceeded()) {

                TermsAggregation streamsAggregation = result.getAggregations().getTermsAggregation("streams");

                JsonArray streams = new JsonArray();

                for (TermsAggregation.Entry bucket : streamsAggregation.getBuckets()) {
                    TopHitsAggregation topHitsAggregation = bucket.getTopHitsAggregation("top_hits");
                    SearchResult.Hit<DataStreamDocument, Void> hit =
                            topHitsAggregation.getFirstHit(DataStreamDocument.class);
                    DataStreamDocument stream = hit.source;
                    String type = stream.getType();

                    System.out.println(stream);

                    if (query.getTypes() == null || Arrays.asList(query.getTypes()).contains(type)) {
                        JsonObject streamRef = new JsonObject();
                        streamRef.addProperty("name", bucket.getKey());
                        streamRef.addProperty("type", type);
                        streams.add(streamRef);
                    }

                }

                response = new ResponseBuilder(true).setResponse(streams);

            } else {
                response = new ResponseBuilder(false).setReason(result.getErrorMessage());
            }

        } catch (IOException e) {
            if (e.getCause() != null) {
                throw new APIException(400, "Could not send request to elasticsearch: " + e.getMessage(), e);
            } else {
                throw new APIException(400, "Could not send request to elasticsearch: " +
                        Objects.requireNonNull(e.getCause()).getMessage(), e);
            }
        }

        return response.build();
    }


}
