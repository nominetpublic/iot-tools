/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.controller;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import spark.Service;
import uk.nominet.iot.model.api.APIException;
import uk.nominet.iot.api.datastore.utils.ElasticSearchUtils;
import uk.nominet.iot.api.datastore.errors.IllegalQueryException;
import uk.nominet.iot.model.api.ExceptionHandler;
import uk.nominet.iot.api.datastore.model.Facet;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.http.APIController;
import uk.nominet.iot.model.api.ResponseBuilder;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static uk.nominet.iot.utils.ConversionUtils.asciiToHex;

public class FacetsSearchController implements APIController {
    public enum FacetProperties {
        TYPE
    }

    private static final Map<FacetProperties, AggregationFacetConverter> AGGREGATIONS = new HashMap<>();
    private static final Logger LOG = LoggerFactory.getLogger(FacetsSearchController.class);
    private static final Pattern COORD_FORMAT = Pattern.compile(
            "^([+-]?(?:[0-9]*[.])?[0-9]+),([+-]?(?:[0-9]*[.])?[0-9]+)$");

    public FacetsSearchController() {
        AGGREGATIONS.put(FacetProperties.TYPE, AggregationFacetConverter.createTermsConverter("Device",
                FacetProperties.TYPE.name(), ElasticSearchUtils.TYPE_FIELD, "string"));
    }

    @Override
    public void publishAPI(Service http, String path) {
        http.path(path, () -> http.get("/facets", this::handleDeviceFacetsRequest));
    }

    public List<GeoPoint> parseGeoPoints(String[] coords) {
        ArrayList<GeoPoint> points = new ArrayList<>(coords.length);
        for(String coord: coords) {
            Matcher matcher = COORD_FORMAT.matcher(coord);
            if(!matcher.matches()) {
                throw new IllegalQueryException("geo[]", String.join(";", coords), "each value must be in " +
                        "the form 'latitude,longitude', with both coordinates in decimal format");
            }
            // Important: does not use conversion utils, to avoid normalising the longitude to the range [-180,180) (as
            // this is necessary information when the points represent a bounding box that crosses the anti-meridian)
            points.add(new GeoPoint(Double.parseDouble(matcher.group(1)), Double.parseDouble(matcher.group(2))));
        }
        if(!points.get(0).equals(points.get(coords.length - 1))) {
            throw new IllegalQueryException("geo[]", String.join(";", coords), "the points must form a " +
                    "closed polygon");
        }
        return points;
    }


    private String handleDeviceFacetsRequest(Request req, Response res) {
        res.type("application/json");
        String user = req.attribute("introspected_username");
        List<GeoPoint> points = Collections.emptyList();
        String[] coords = req.queryParamsValues("geo");
        if(coords != null && coords.length > 0) {
            if (coords.length < 4) {
                return  ExceptionHandler.handle(res, new IllegalQueryException("geo[]", String.join(";", coords),
                        "it must be an array that contains at least four lat,lon pairs"));
            }
            points = parseGeoPoints(coords);
        }
        FacetProperties facetProperties = null;
        try {
            String property = req.queryParamOrDefault("property", "").toUpperCase();
            facetProperties = FacetProperties.valueOf(property);
        } catch (IllegalArgumentException ex) {
            // do nothing, null facetProperties means no filter
        }
        try {
            return extractFacets(user, points, facetProperties);
        } catch (Exception e) {
            return ExceptionHandler.handle(res, e);
        }
    }

    public String extractFacets(String user, List<GeoPoint> points, FacetProperties property) throws APIException {
        String index;
        try {
            index = ElasticSearchUtils.INDEX_PREFIX + asciiToHex(user);
        } catch (Exception e) {
            throw new APIException(400, "Could not identify elasticsearch index", e);
        }

        SearchSourceBuilder searchBuilder = new SearchSourceBuilder().size(0);
        if(property != null) {
            searchBuilder.aggregation(AGGREGATIONS.get(property).getAggregationBuilder());
        } else {
            for(AggregationFacetConverter handler : AGGREGATIONS.values()) {
                searchBuilder.aggregation(handler.getAggregationBuilder());
            }
        }

        if(points.size() > 0) {
            searchBuilder.query(QueryBuilders.boolQuery().must(ElasticSearchUtils.buildGeoQuery(points)));
        }

        Search search = new Search.Builder(searchBuilder.toString())
                .addIndex(index)
                .addType(ElasticSearchUtils.DEVICE_TYPE)
                .build();

        SearchResult result;
        try {
            result = ElasticsearchDriver.getClient().execute(search);
        } catch(IOException e) {
            if (e.getCause() != null) {
                throw new APIException(400, "Could not send request to elasticsearch: " + e.getMessage(), e);
            } else {
                throw new APIException(400, "Could not send request to elasticsearch: " + e.getCause().getMessage(), e);
            }
        }

        JsonArray facets = new JsonArray();
        long total = 0;

        if(property != null) {
            AggregationFacetConverter handler = AGGREGATIONS.get(property);
            Facet facet = handler.getFacetFromSearchResult(result);
            facets.add(facet.toJsonObject());
            total += facet.getOptions().size();
        } else {
            for(AggregationFacetConverter handler : AGGREGATIONS.values()) {
                Facet facet = handler.getFacetFromSearchResult(result);
                facets.add(facet.toJsonObject());
                total += facet.getOptions().size();
            }
        }

        JsonObject responseObj = new ResponseBuilder(true)
                .addProperty("total", total)
                .addElement("facets", facets)
                .build();

        return responseObj.toString();
    }


}
