/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import uk.nominet.iot.api.datastore.utils.ElasticSearchUtils;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.http.APIController;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import spark.Service;
import uk.nominet.iot.model.api.ResponseBuilder;
import uk.nominet.iot.model.registry.cache.DataStreamDocument;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static uk.nominet.iot.utils.ConversionUtils.asciiToHex;


public class DataStreamsController implements APIController {
    private static final Logger LOG = LoggerFactory.getLogger(DataStreamsController.class);


    private String handleException(Exception e, String description) {
        LOG.error(description + ": " + e.getMessage());
        JsonObject response = new ResponseBuilder(false)
                .setReason(description)
                .build();
        return response.toString();
    }

    private String search(Request req, Response res) {
        JsonObject response;
        res.type("application/json");

        boolean keyOnly = req.queryParams("keysOnly") != null && req.queryParams("keysOnly").equals("true");


        String index;
        try {
            index = ElasticSearchUtils.INDEX_PREFIX + asciiToHex(req.attribute("introspected_username"));
        } catch (Exception e) {
            return handleException(e, "Could not identify elasticsearch index");
        }


        TermsQueryBuilder queryBuilder =
                new TermsQueryBuilder("devices.key.keyword", req.queryParamsValues("device"));

        SearchSourceBuilder searchBuilder = new SearchSourceBuilder().query(queryBuilder);
        searchBuilder.size(100);

        Search search = new Search.Builder(searchBuilder.toString())
                .addIndex(index)
                .addType(ElasticSearchUtils.STREAM_TYPE)
                .build();

        SearchResult result;
        try {
            result = ElasticsearchDriver.getClient().execute(search);

            if (result.isSucceeded()) {
                List<DataStreamDocument> streams = new ArrayList<>();


                List<SearchResult.Hit<DataStreamDocument, Void>> hits = result.getHits(DataStreamDocument.class);
                hits.forEach(hit -> streams.add(hit.source));

                if (keyOnly) {

                    JsonArray keysElement = new JsonArray();
                    for (DataStreamDocument stream : streams) {
                        keysElement.add(stream.getKey());
                    }

                    response = new ResponseBuilder(true)
                            .addElement("streams", keysElement)
                            .build();
                } else {
                    JsonElement list = new Gson().toJsonTree(streams);
                    response = new ResponseBuilder(true)
                            .addElement("streams", list)
                            .build();
                }

            } else {
                response = new ResponseBuilder(false)
                        .setReason(result.getErrorMessage())
                        .build();
            }


        } catch (IOException e) {
            return handleException(e, "Could not send request to elasticsearch");
        }


        return response.toString();
    }

    @Override
    public void publishAPI(Service http, String path) {
        http.path(path, () -> http.get("/stream/list", this::search));
    }


}
