/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.controller;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.core.search.aggregation.TermsAggregation;
import io.searchbox.core.search.aggregation.TopHitsAggregation;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.everit.json.schema.ValidationException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import spark.Service;
import uk.nominet.iot.api.datastore.query.StreamListQuery;
import uk.nominet.iot.http.APIController;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.api.APIException;
import uk.nominet.iot.api.datastore.utils.ElasticSearchUtils;
import uk.nominet.iot.api.datastore.errors.IllegalQueryException;
import uk.nominet.iot.model.api.ExceptionHandler;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.model.api.ResponseBuilder;

import java.util.*;
import java.util.stream.Collectors;

import static uk.nominet.iot.api.datastore.query.StreamListQuery.*;
import static uk.nominet.iot.utils.ConversionUtils.asciiToHex;

public class StreamListController implements APIController {
    private static final Logger LOG = LoggerFactory.getLogger(StreamListController.class);
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    @Override
    public void publishAPI(Service http, String path) {
        http.path(path, () -> http.post("/list", this::handleStreamSearchRequest));
    }


    private String handleStreamSearchRequest(Request req, Response res) {
        res.type("application/json");

        JSONObject queryObject;
        try {
            queryObject = new JSONObject(new JSONTokener(req.body()));
        } catch (JSONException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

        try {
            StreamListQuery query = new StreamListQuery(queryObject, req.attribute("introspected_username"));

            return getStreams(query).toString();

        } catch (ValidationException e) {
            return ExceptionHandler.handle(res, new IllegalQueryException(e.getKeyword(), queryObject.toString(),
                    e.getErrorMessage()), e.getAllMessages().toString());
        } catch (Exception e) {
            e.printStackTrace();
            return ExceptionHandler.handle(res, e);
        }
    }

    public JsonObject getStreams(StreamListQuery query) throws Exception {
        ResponseBuilder response;
        String index;
        try {
            index = ElasticSearchUtils.INDEX_PREFIX + asciiToHex(query.getUser());
        } catch (Exception e) {
            throw new APIException(400, "Could not identify elasticsearch index", e);
        }

        JsonArray responseData = new JsonArray();
        int total = 0;

        SearchSourceBuilder searchBuilder = new SearchSourceBuilder()
                .query(query.getQueryBuilder())
                .aggregation(query.getAggregationBuilder())
                .size(0);

        Search search = new Search.Builder(searchBuilder.toString())
                .addIndex(index)
                .addType(ElasticSearchUtils.STREAM_TYPE)
                .build();

        SearchResult result;
        result = ElasticsearchDriver.getClient().execute(search);
        if (result.isSucceeded()) {

            Map<String, List<JsonObject>> devicesWithStreams = extractMapFromResults(result, query.getName());


            for (Map.Entry<String, List<JsonObject>> devices : devicesWithStreams.entrySet()) {
                total += devices.getValue().size();

                if (query.getSecondaryNames() == null) {
                    for (JsonObject stream : devices.getValue()) {
                        responseData.add(stream);
                    }
                } else {
                    JsonObject mainStream = devices.getValue().get(0);

                    if (devices.getValue().size() > 1) {
                        JsonArray secondaryStreams = new JsonArray();
                        for (int i = 1; i < devices.getValue().size(); i++) {
                            secondaryStreams.add(devices.getValue().get(i));
                        }
                        mainStream.add("secondaryStreams", secondaryStreams);
                    }

                    if (query.getOutputType() == OutputFormat.STREAMS) {
                        responseData.add(mainStream);
                    } else if (query.getOutputType() == OutputFormat.STREAMKEYS) {
                        if (devices.getValue().size() > 1) {
                            List<String> streamKeys = devices.getValue().stream().map(st -> st.get("key").getAsString()).collect(Collectors.toList());
                            responseData.add(jsonStringSerialiser.getDefaultSerialiser().toJsonTree(streamKeys));
                        } else
                            responseData.add(mainStream.get("key").getAsString());
                    }
                }
            }


        } else {
            throw new Exception(result.getErrorMessage());
        }

        response = new ResponseBuilder(true)
                .setResponse(responseData);

        return response.build();
    }

    private Map<String, List<JsonObject>> extractMapFromResults(SearchResult result, String name) {
        Map<String, List<JsonObject>> devicesWithStreams = new HashMap<>();

        TermsAggregation devicesAggregation = result.getAggregations().getTermsAggregation("devices");

        for (TermsAggregation.Entry deviceBucket : devicesAggregation.getBuckets()) {
            String deviceKey = deviceBucket.getKey();
            JsonObject mainStream = null;
            List<JsonObject> streamsList = new ArrayList<>();

            TopHitsAggregation streamsAggregation = deviceBucket.getTopHitsAggregation("streams");

            for (SearchResult.Hit<JsonObject, Void> streamHit : streamsAggregation.getHits(JsonObject.class)) {
                JsonObject stream = streamHit.source;
                if (name != null && name.equals(stream.get("name").getAsString())) {
                    mainStream = stream;
                } else {
                    streamsList.add(stream);
                }
            }

            if (mainStream != null) streamsList.add(0, mainStream);

            devicesWithStreams.put(deviceKey, streamsList);

        }


        return devicesWithStreams;
    }


}
