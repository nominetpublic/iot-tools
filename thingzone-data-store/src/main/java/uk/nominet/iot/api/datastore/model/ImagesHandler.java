/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.model;

import com.google.gson.JsonElement;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.timeseries.Image;
import uk.nominet.iot.model.timeseries.ImageBurst;

import java.time.*;

import java.time.temporal.TemporalAdjusters;
import java.util.*;

public class ImagesHandler {


    private final List<ImageBurst> items;
    private final Integer burstInterval;

    public ImagesHandler(Integer burstInterval) {
        this.burstInterval = burstInterval;
        items = new ArrayList<>();
    }


    /**
     * Add an image to the handler
     * @param image a json object
     */
    public void put(Image image) {
        Instant timestamp = image.getTimestamp();

        if (items.size() == 0) {
            ImageBurst block = new ImageBurst();
            block.setTimestamp(image.getTimestamp());
            block.getImages().add(image);
            items.add(block);
            return;
        }

        ImageBurst latestImagesBlock = items.get(items.size() - 1);
        Image latestImage = latestImagesBlock.getImages().get(latestImagesBlock.getImages().size() - 1);
        Instant latestTimestamp = latestImage.getTimestamp();

        if (burstInterval != null && Math.abs(Duration.between(timestamp, latestTimestamp).toMillis()) <= burstInterval) {
            latestImagesBlock.getImages().add(image);
        } else {
            ImageBurst block = new ImageBurst();
            block.setTimestamp(image.getTimestamp());
            block.getImages().add(image);
            items.add(block);
        }

    }


    /**
     * Convert items into a json element, array or object depending on grouping
     * @return json element
     */
    public JsonElement toJsonElement() {
        return new JsonStringSerialiser().getDefaultSerialiser().toJsonTree(items).getAsJsonArray();
    }

    public List<ImageBurst> getItems() {
        return items;
    }

}
