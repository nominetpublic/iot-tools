/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.vividsolutions.jts.geom.Coordinate;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.core.Get;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.core.search.aggregation.FilterAggregation;
import io.searchbox.core.search.aggregation.GeoHashGridAggregation;
import org.elasticsearch.common.geo.ShapeRelation;
import org.elasticsearch.common.geo.builders.ShapeBuilder;
import org.elasticsearch.common.geo.builders.ShapeBuilders;
import uk.nominet.iot.api.datastore.errors.QueryFailureException;
import uk.nominet.iot.api.datastore.errors.TooManyResultsReturned;
import uk.nominet.iot.api.datastore.geo.GeoUtils;
import uk.nominet.iot.api.datastore.geo.model.DeviceLocation;
import uk.nominet.iot.api.datastore.geo.model.Latitude;
import uk.nominet.iot.api.datastore.geo.model.Longitude;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.*;
import org.locationtech.spatial4j.context.SpatialContext;
import org.locationtech.spatial4j.io.GeohashUtils;
import org.locationtech.spatial4j.shape.Rectangle;
import uk.nominet.iot.driver.ElasticsearchDriver;

import java.io.IOException;
import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiFunction;

public class ElasticSearchUtils {
    public static final String GEO_FIELD = "singlePosition";
    public static final String TYPE_FIELD = "type.keyword";
    public static final String NAME_FIELD = "name.keyword";
    public static final String TIMESTAMP_FIELD = "timestamp";
    public static final String INDEX_PREFIX = "thingzone_";
    public static final String DEVICE_TYPE = "device";
    public static final String STREAM_TYPE = "stream";
    public static final String METRIC_TYPE = "metric";
    public static final String EVENT_TYPE = "event";
    public static final String IMAGE_TYPE = "image";
    public static final int MAX_RESULT_COUNT = 10000;

    private ElasticSearchUtils() {}

    private static JsonObject gridToDeviceJson(GeoHashGridAggregation.GeoHashGrid grid) {
        JsonObject deviceJson = new JsonObject();
        deviceJson.addProperty("id", grid.getKey());
        JsonObject sourceJson = new JsonObject();
        JsonObject metadata = new JsonObject();
        metadata.addProperty("name", grid.getKey());
        metadata.addProperty("type", "grid cluster");
        metadata.addProperty("count", grid.getCount());
        JsonObject geometry = new JsonObject();
        geometry.addProperty("type", "Polygon");
        JsonArray coordinates = new JsonArray();
        JsonArray polygon = new JsonArray();
        Rectangle gridBox = GeohashUtils.decodeBoundary(grid.getKey(), SpatialContext.GEO);
        JsonArray tl = new JsonArray();
        tl.add(gridBox.getMinX());
        tl.add(gridBox.getMinY());
        JsonArray tr = new JsonArray();
        tr.add(gridBox.getMaxX());
        tr.add(gridBox.getMinY());
        JsonArray br = new JsonArray();
        br.add(gridBox.getMaxX());
        br.add(gridBox.getMaxY());
        JsonArray bl = new JsonArray();
        bl.add(gridBox.getMinX());
        bl.add(gridBox.getMaxY());
        polygon.add(tl);
        polygon.add(tr);
        polygon.add(br);
        polygon.add(bl);
        polygon.add(tl);
        coordinates.add(polygon);
        geometry.add("coordinates", coordinates);
        metadata.add("geometry", geometry);
        sourceJson.add("metadata", metadata);
        deviceJson.add("_source", sourceJson);
        return deviceJson;
    }

    private static Object[] getObjectsFromSort(JsonArray sortItems) {
        Object[] results = new Object[sortItems.size()];
        for(int i = 0; i < results.length; i++) {
            JsonPrimitive value = sortItems.get(i).getAsJsonPrimitive();
            if(value.isString()) {
                results[i] = value.getAsString();
            }
            if(value.isNumber()) {
                results[i] = value.getAsDouble();
            }
            if(value.isBoolean()) {
                results[i] = value.getAsBoolean();
            }
        }
        return results;
    }



    public static DeviceLocation toDeviceLocation(JsonObject jsonObject, Integer index) {
        JsonArray coordArray = jsonObject
                .getAsJsonObject("_source")
                .getAsJsonObject("metadata")
                .getAsJsonObject("geometry")
                .getAsJsonArray("coordinates");
        // Note: lat, lon arrays are backwards!
        return new DeviceLocation(jsonObject.get("_id").getAsString(),
                jsonObject.getAsJsonObject("_source").getAsJsonObject("metadata").get("type").getAsString(),
                Latitude.getLatitudeFromDegrees(coordArray.get(1).getAsDouble()),
                Longitude.getLongitudeFromDegrees(coordArray.get(0).getAsDouble()), index);
    }

    public static QueryBuilder buildGeoQuery(List<GeoPoint> points) throws IllegalArgumentException {
        return buildGeoQuery(points, GEO_FIELD);
    }

    public static QueryBuilder buildGeoQuery(List<GeoPoint> points,  String geoField) throws IllegalArgumentException {
        if(points.size() < 2) {
            throw new IllegalArgumentException("Must provide at least two geo-points for a query");
        }
        GeoPoint[] topLeftBottomRight = GeoUtils.getTopLeftBottomRightRectangle(points);
        if(topLeftBottomRight != null) {
            return QueryBuilders.geoBoundingBoxQuery(geoField)
                                .setCorners(topLeftBottomRight[0], topLeftBottomRight[1])
                                .type(GeoExecType.INDEXED);
        } else {
            return QueryBuilders.geoPolygonQuery(geoField, points);
        }
    }

    public static QueryBuilder buildGeoShapeEnvelopeQuery(List<GeoPoint> points, String geoField) throws IllegalArgumentException {
        if(points.size() < 2) {
            throw new IllegalArgumentException("Must provide at least two geo-points for a query");
        }
        GeoPoint[] topLeftBottomRight = GeoUtils.getTopLeftBottomRightRectangle(points);

        Coordinate topLeft = new Coordinate(topLeftBottomRight[1].getLon(), topLeftBottomRight[1].getLat());
        Coordinate bottomRight = new Coordinate(topLeftBottomRight[0].getLon(), topLeftBottomRight[0].getLat());

        ShapeBuilder shapeBuilder = ShapeBuilders.newEnvelope(topLeft, bottomRight);

        GeoShapeQueryBuilder geoShapeQueryBuilder = new GeoShapeQueryBuilder(geoField, shapeBuilder);
        geoShapeQueryBuilder.relation(ShapeRelation.WITHIN);

        return geoShapeQueryBuilder;

    }


    public static long queryElasticSearchHitsWindow(JestClient esClient, SearchSourceBuilder baseQuery, String index,
                                                    JsonArray hits, String type)
            throws IOException, QueryFailureException {
        // ensure results are sorted by ID
        List<SortBuilder<?>> sorts = baseQuery.sorts();
        // Removed, not necessary to sort results, broke elasticsearch request
//        SortBuilder idSort = SortBuilders.fieldSort("_uid").order(SortOrder.ASC);
//        if(sorts == null || sorts.size() == 0 || sorts.stream().noneMatch(idSort::equals)) {
//            actualQuery = actualQuery.sort(idSort);
//        }
        Search search = new Search.Builder(baseQuery.toString()).addIndex(index).addType(type).build();

        JestResult result = esClient.execute(search);
        if(!result.isSucceeded()) {
            throw new QueryFailureException(result);
        }


        JsonObject hitsObject = result.getJsonObject().getAsJsonObject("hits");
        if(hitsObject != null && hitsObject.has("hits")) {
            hits.addAll(hitsObject.getAsJsonArray("hits"));
            return hitsObject.get("total").getAsInt();
        }
        return 0;
    }

    /**
     * Search for all results of the specified query in ElasticSearch, handling windowing using the "search after"
     * technique, and using a callback to handle the results in a streaming manner.
     * @param esClient the ElasticSearch JestClient to use for the queries
     * @param baseQuery the base query from which the queries made are derived
     * @param index the ES index to query
     * @param type the ES type to query
     * @param maxTotal the maximum number of results that should be returned by this method. If null, the limit is only
     *                 bounded by memory limitations
     * @param callback a callback function that takes in the last JsonObject produced by a call to this function (will
     *                 be null during the first iteration) and an array of hits, and produces a new JsonObject. These
     *                 JsonObjects can be used to pass information between result set windows - allowing for
     *                 aggregations that aren't broken by the windowing
     * @throws IOException if there is an exception caused by querying ElasticSearch
     * @throws QueryFailureException if the query against ElasticSearch fails
     */
    public static void queryElasticSearchHitsAllCallback(JestClient esClient, SearchSourceBuilder baseQuery,
                                                         String index, String type, Integer maxTotal,
                                                         BiFunction<JsonObject, JsonArray, JsonObject> callback)
            throws IOException, QueryFailureException {
        queryElasticSearchHitsAllCallback(esClient, baseQuery, index, type, maxTotal, null, null,
                callback);
    }

    private static void queryElasticSearchHitsAllCallback(JestClient esClient, SearchSourceBuilder baseQuery,
                                                          String index, String type, Integer maxTotal,
                                                          Object[] searchAfter, JsonObject lastValues,
                                                          BiFunction<JsonObject, JsonArray, JsonObject> callback)
            throws IOException, QueryFailureException {
        // ensure results are sorted by ID
        SearchSourceBuilder actualQuery = baseQuery;
        List<SortBuilder<?>> sorts = actualQuery.sorts();
        SortBuilder idSort = SortBuilders.fieldSort("_uid").order(SortOrder.ASC);
        if(actualQuery.from() != 0) {
            actualQuery = actualQuery.from(0);
        }
        if(searchAfter != null) {
            actualQuery = actualQuery.searchAfter(searchAfter);
        }

        Search search = new Search.Builder(actualQuery.toString()).addIndex(index).addType(type).build();
        JestResult result = esClient.execute(search);
//        System.out.println(actualQuery);
        if(!result.isSucceeded()) {
            throw new QueryFailureException(result);
        }

        JsonObject hitsObject = result.getJsonObject().getAsJsonObject("hits");

        if (maxTotal != null && hitsObject.has("total")
                && hitsObject.getAsJsonPrimitive("total").getAsInt() > maxTotal) {
            throw new TooManyResultsReturned();
        }

        JsonArray hits = new JsonArray();
        if(hitsObject != null && hitsObject.has("hits")) {
            hits = hitsObject.getAsJsonArray("hits");
        }
        lastValues = callback.apply(lastValues, hits); // will be called with an empty JSON array if no results returned
        if (hits.size() == actualQuery.size()) {
            JsonArray sortValues = hits.get(hits.size() - 1).getAsJsonObject().getAsJsonArray("sort");
            searchAfter = getObjectsFromSort(sortValues);
            queryElasticSearchHitsAllCallback(esClient, baseQuery, index, type, maxTotal, searchAfter, lastValues, callback);
        }
    }

    /**
     * Search for all results of the specified query in ElasticSearch, handling windowing using the "search after"
     * technique, and storing the results in the provided JsonArray.
     * @param esClient the ElasticSearch JestClient to use for the queries
     * @param baseQuery the base query from which the queries made are derived
     * @param index the ES index to query
     * @param type the ES type to query
     * @param threshold the maximum number of results that should be returned by this method. If null, the limit is
     *                  only bounded by memory limitations
     * @param hits a JsonArray that will have all the hits returned by this query added to it
     * @throws IOException if there is an exception caused by querying ElasticSearch
     * @throws QueryFailureException if the query against ElasticSearch fails
     * @return a count of results found by this query
     */
    public static int queryElasticSearchHitsAll(JestClient esClient, SearchSourceBuilder baseQuery, String index,
                                                 String type, Integer threshold, JsonArray hits)
            throws IOException, QueryFailureException {
        AtomicReference<Integer> resultCount = new AtomicReference<>(0);
        queryElasticSearchHitsAllCallback(esClient, baseQuery, index, type, threshold,
                (last, newHits) -> {
                    hits.addAll(newHits);
                    if (newHits.size() < baseQuery.size()) {
                        resultCount.set(hits.size());
                    }
                    return null;
                });
        return resultCount.get();
    }

    public static long queryElasticSearchAggGeohash(JestClient esClient, BoolQueryBuilder boundingQuery, String index,
                                                    JsonArray hits, Integer geohashPrecision, String geoField)
            throws IOException, QueryFailureException {
        SearchSourceBuilder searchBuilder = new SearchSourceBuilder().size(0) // returns no results, just aggregation
                .aggregation(AggregationBuilders.filter("zoomed-in", boundingQuery)
                        .subAggregation(AggregationBuilders.geohashGrid("zoom").field(geoField)
                                .precision(geohashPrecision)));

        Search search = new Search.Builder(searchBuilder.toString())
                .addIndex(index)
                .addType(DEVICE_TYPE)
                .build();

        SearchResult result = esClient.execute(search);
        if(!result.isSucceeded()) {
            throw new QueryFailureException(result);
        }

        FilterAggregation filterAgg = result.getAggregations().getFilterAggregation("zoomed-in");
        GeoHashGridAggregation gridAgg = filterAgg.getGeohashGridAggregation("zoom");
        for(GeoHashGridAggregation.GeoHashGrid grid: gridAgg.getBuckets()) {
            hits.add(gridToDeviceJson(grid));
        }
        return filterAgg.getCount();
    }


    /**
     * Get stream information from elasticsearch
     * @param index     Elasticsearch index
     * @param streamKey stream key
     * @return stream document as a json object
     */
    public static JsonObject getStreamInfo(String index, String streamKey) throws Exception {
        Get get = new Get.Builder(index, streamKey).type(ElasticSearchUtils.STREAM_TYPE).build();

        JestResult result = ElasticsearchDriver.getClient().execute(get);

        if (result.isSucceeded()) {
            return result.getJsonObject().get("_source").getAsJsonObject();
        } else {
            throw new Exception(result.getErrorMessage());
        }

    }

    /**
     * Extract a summary from a stream object
     * @param streamInfo stream object
     * @return stream summary json object
     */
    public static JsonObject getStreamSummary(JsonObject streamInfo) {
        JsonObject streamSummary = new JsonObject();
        streamSummary.addProperty("streamKey", streamInfo.get("key").getAsString());
        if (streamInfo.has("name"))
            streamSummary.addProperty("name", streamInfo.get("name").getAsString());
        if (streamInfo.has("type"))
            streamSummary.addProperty("type", streamInfo.get("type").getAsString());
        if (streamInfo.has("metadata"))
            streamSummary.add("metadata", streamInfo.get("metadata").getAsJsonObject());
        return streamSummary;
    }


    public static String timeDelta(String initialTime, String timeLimit, boolean forward) {
        String sign = (forward) ? "+" : "-";
        if (isValidDate(initialTime)) {
            return String.format("%s||%s%s", initialTime, sign, timeLimit);
        } else {
            return String.format("%s%s%s", initialTime, sign, timeLimit);
        }
    }

    private static boolean isValidDate(String inDate) {
        try {
            Instant.parse(inDate);
        } catch (DateTimeParseException e) {
            return false;
        }
        return true;
    }


    public static RangeQueryBuilder getRangeBuilder(String time, String timeLimit, boolean forward) {

        if (time == null && forward) return null;

        RangeQueryBuilder rangeQueryBuilder = new RangeQueryBuilder("timestamp");
        String initialTime = (time != null) ? time : "now";

        if (initialTime != null) {
            if (forward) {
                rangeQueryBuilder = rangeQueryBuilder.gte(initialTime);
                if (timeLimit != null)
                    rangeQueryBuilder = rangeQueryBuilder.lte(timeDelta(initialTime, timeLimit, forward));
            } else {
                rangeQueryBuilder = rangeQueryBuilder.lte(initialTime);
                if (timeLimit != null) {
                    rangeQueryBuilder = rangeQueryBuilder.gte(timeDelta(initialTime, timeLimit, forward));
                }
            }

            return rangeQueryBuilder;
        }

        return null;
    }



}
