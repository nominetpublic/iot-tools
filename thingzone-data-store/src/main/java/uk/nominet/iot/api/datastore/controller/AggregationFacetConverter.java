/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.controller;

import io.searchbox.core.SearchResult;
import io.searchbox.core.search.aggregation.Aggregation;
import io.searchbox.core.search.aggregation.StatsAggregation;
import io.searchbox.core.search.aggregation.TermsAggregation;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import uk.nominet.iot.api.datastore.model.Facet;
import uk.nominet.iot.api.datastore.model.FacetOption;

import java.util.function.Function;

/**
 * A abstract class for converting ElasticSearch Aggregations into Facets and FacetOptions.
 *
 * @param <T> the aggregation type that the class converts from
 */
class AggregationFacetConverter<T extends Aggregation> {
    private final String property;
    private final AggregationBuilder aggregationBuilder;
    private final Function<T, Facet> aggregationToFacet;
    private final Class<T> aggregationType;

    /**
     * @param property the name of the property in the produced facet. Should be unique on a per query basis, as it
     *                 will be used to name the aggregation in the search request/response
     * @param aggregationBuilder a builder object that will be used to produce aggregations of type T
     * @param aggregationType the type T of aggregations that will be produced. Needed to extract the aggregation from
     *                        the search results
     * @param aggregationToFacet a function to convert an aggregation of type T into a Facet
     */
    private AggregationFacetConverter(String property, AggregationBuilder aggregationBuilder,
                                      Class<T> aggregationType, Function<T, Facet> aggregationToFacet) {
        this.property = property;
        this.aggregationBuilder = aggregationBuilder;
        this.aggregationType = aggregationType;
        this.aggregationToFacet = aggregationToFacet;
    }

    public String getProperty() {
        return property;
    }

    public AggregationBuilder getAggregationBuilder() {
        return aggregationBuilder;
    }

    /**
     * Given a FilterAggregation that performs the initial result filtering, that contains a sub-aggregation with the
     * name {@link AggregationFacetConverter#getProperty()} and of type T, obtain a Facet object
     * @param searchResult A Jest SearchResult object that contains the aggregations over the results of interest
     * @return a Facet for property, built from the aggregation of type T
     */
    public Facet getFacetFromSearchResult(SearchResult searchResult) {
        return aggregationToFacet.apply(searchResult.getAggregations().getAggregation(property, aggregationType));
    }

    /**
     * An aggregation to facet converter for Terms aggregations. Produces Facets with FacetOptions that contain string
     * values.
     * @param prefix a prefix string that is appended to the property for the title and individual facet option IDs
     * @param property the name of the property for the facet
     * @param field the path to the field in JSON, represented as a full-stop separated string
     * @param format the format of the facet
     * @return an AggregationFacetConverter for TermsAggregations to Facets
     */
    public static AggregationFacetConverter<TermsAggregation> createTermsConverter(
            String prefix, String property, String field, String format) {
        return new AggregationFacetConverter<>(property,
                AggregationBuilders.terms(property).field(field),
                TermsAggregation.class,
                (termAgg) -> {
                    Facet facet = new Facet(String.format("%s %s", prefix, property.toLowerCase()),
                            property.toLowerCase(), format, field.split("\\."));
                    for (TermsAggregation.Entry entry : termAgg.getBuckets()) {
                        facet.getOptions().add(
                                new FacetOption(String.format("%s_%s", prefix.toLowerCase(),
                                        entry.getKey().toLowerCase().replaceAll(" ", "_")),
                                    entry.getKey(),
                                    entry.getCount()));
                    }
                    return facet;
                });
    }

    /**
     * An aggregation to facet converter for Stats aggregations. Produces Facets with FacetOptions that contain ranges
     * of values.
     * @param prefix a prefix string that is appended to the property for the title and individual facet option IDs
     * @param property the name of the property for the facet
     * @param field the path to the field in JSON, represented as a full-stop separated string
     * @param format the format of the facet
     * @return an AggregationFacetConverter for StatsAggregations to Facets
     */
    public static AggregationFacetConverter<StatsAggregation> createStatsConverter(
            String prefix, String property, String field, String format) {
        return new AggregationFacetConverter<>(property,
                AggregationBuilders.stats(property).field(field),
                StatsAggregation.class,
                (statsAgg) -> {
                    String[] parts = field.split("\\.");
                    Facet facet = new Facet(String.format("%s %s", prefix, property.toLowerCase()),
                            property.toLowerCase(), format, parts);
                    facet.getOptions().add(new FacetOption(String.format("%s_%s", prefix.toLowerCase(),
                            parts[parts.length-1]), statsAgg.getMin(), statsAgg.getMax(), statsAgg.getCount()));
                    return facet;
                });
    }
}
