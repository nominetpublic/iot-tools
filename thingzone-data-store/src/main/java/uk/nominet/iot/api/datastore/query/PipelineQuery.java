/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.query;

import com.google.gson.reflect.TypeToken;
import org.json.JSONObject;
import uk.nominet.iot.api.datastore.model.PipelineRegistryEntity;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.registry.RegistryEntity;

import java.util.ArrayList;
import java.util.List;

public class PipelineQuery extends DatastoreQuery<Object> {
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    private DeviceLatestQuery deviceQuery;
    private List<PipelineRegistryEntity> deviceList;
    private StreamListQuery streamListQuery;
    private List<PipelineRegistryEntity> streamList;
    private MetricsAggQuery metricsAggQuery;
    private ImagesListQuery imagesListQuery;
    private MetricsListQuery metricsListQuery;
    private MetricsGeoQuery metricsGeoQuery;
    private CommonStreamsQuery commonStreamsQuery;
    private CombinationListQuery combinationListQuery;
    private CombinationGeoQuery combinationGeoQuery;
    private EventListQuery eventListQuery;
    private EventGeoQuery eventGeoQuery;

    private boolean intermediate;

    public PipelineQuery(JSONObject requestBody, String user) throws Exception {
        super(requestBody, user);
        getSchema().validate(requestBody);

        setUser(user);

        if (requestBody.has("deviceList")) {
            deviceList = jsonStringSerialiser
                                        .getDefaultSerialiser()
                                        .fromJson(requestBody.getJSONArray("deviceList").toString(),
                                                  new TypeToken<List<RegistryEntity>>() {}.getType());

        } else if (requestBody.has("deviceQuery")) {
            deviceQuery = new DeviceLatestQuery(requestBody.getJSONObject("deviceQuery"), user);
            deviceQuery.setFrom(0);
            deviceQuery.setSize(5000);
        }


        if (requestBody.has("streamList")) {
            streamList = jsonStringSerialiser
                                        .getDefaultSerialiser()
                                        .fromJson(requestBody.getJSONArray("streamList").toString(),
                                                  new TypeToken<List<RegistryEntity>>() {
                                                  }.getType());
        } else if (requestBody.has("streamListQuery")) {
            streamListQuery = new StreamListQuery(requestBody.getJSONObject("streamListQuery"), user);
            streamListQuery.setOutputType(StreamListQuery.OutputFormat.STREAMS); //Fix results output as stream
        }

        if (requestBody.has("metricsAggQuery")) {
            metricsAggQuery = new MetricsAggQuery(requestBody.getJSONObject("metricsAggQuery"), user);
        }

        if (requestBody.has("imagesListQuery")) {
            imagesListQuery = new ImagesListQuery(requestBody.getJSONObject("imagesListQuery"), user);
        }

        if (requestBody.has("eventListQuery")) {
            eventListQuery = new EventListQuery(requestBody.getJSONObject("eventListQuery"), user);
        }

        if (requestBody.has("eventGeoQuery")) {
            eventGeoQuery = new EventGeoQuery(requestBody.getJSONObject("eventGeoQuery"), user);
        }

        if (requestBody.has("metricsListQuery")) {
            metricsListQuery = new MetricsListQuery(requestBody.getJSONObject("metricsListQuery"), user);
        }

        if (requestBody.has("metricsGeoQuery")) {
            metricsGeoQuery = new MetricsGeoQuery(requestBody.getJSONObject("metricsGeoQuery"), user);
        }

        if (requestBody.has("commonStreamsQuery")) {
            commonStreamsQuery = new CommonStreamsQuery(requestBody.getJSONObject("commonStreamsQuery"), user);
        }

        if (requestBody.has("combinationListQuery")) {
            combinationListQuery  = new CombinationListQuery(requestBody.getJSONObject("combinationListQuery"), user);
            if (streamListQuery != null) {
                List<String> secondaryNames = new ArrayList<>(combinationListQuery.getSecondary().keySet());
                streamListQuery.setSecondaryNames(secondaryNames);
            }
        }

        if (requestBody.has("combinationGeoQuery")) {
            combinationGeoQuery = new CombinationGeoQuery(requestBody.getJSONObject("combinationGeoQuery"), user);
            if (streamListQuery != null) {
                List<String> secondaryNames = new ArrayList<>(combinationGeoQuery.getSecondary().keySet());
                streamListQuery.setSecondaryNames(secondaryNames);
            }
        }


        if (requestBody.has("intermediate")) {
            intermediate = requestBody.getBoolean("intermediate");
        }
    }

    public DeviceLatestQuery getDeviceQuery() {
        return deviceQuery;
    }

    public void setDeviceQuery(DeviceLatestQuery deviceQuery) {
        this.deviceQuery = deviceQuery;
    }

    public StreamListQuery getStreamListQuery() {
        return streamListQuery;
    }

    public void setStreamListQuery(StreamListQuery streamListQuery) {
        this.streamListQuery = streamListQuery;
    }

    public MetricsAggQuery getMetricsAggQuery() {
        return metricsAggQuery;
    }

    public void setMetricsAggQuery(MetricsAggQuery metricsAggQuery) {
        this.metricsAggQuery = metricsAggQuery;
    }

    public ImagesListQuery getImagesListQuery() {
        return imagesListQuery;
    }

    public void setImagesListQuery(ImagesListQuery imagesListQuery) {
        this.imagesListQuery = imagesListQuery;
    }

    public EventListQuery getEventListQuery() {
        return eventListQuery;
    }

    public void setEventListQuery(EventListQuery eventListQuery) {
        this.eventListQuery = eventListQuery;
    }

    public boolean isIntermediate() {
        return intermediate;
    }

    public void setIntermediate(boolean intermediate) {
        this.intermediate = intermediate;
    }

    public List<PipelineRegistryEntity> getDeviceList() {
        return deviceList;
    }

    public void setDeviceList(List<PipelineRegistryEntity> deviceList) {
        this.deviceList = deviceList;
    }

    public List<PipelineRegistryEntity> getStreamList() {
        return streamList;
    }

    public void setStreamList(List<PipelineRegistryEntity> streamList) {
        this.streamList = streamList;
    }

    public MetricsListQuery getMetricsListQuery() {
        return metricsListQuery;
    }

    public void setMetricsListQuery(MetricsListQuery metricsListQuery) {
        this.metricsListQuery = metricsListQuery;
    }

    public MetricsGeoQuery getMetricsGeoQuery() {
        return metricsGeoQuery;
    }

    public void setMetricsGeoQuery(MetricsGeoQuery metricsGeoQuery) {
        this.metricsGeoQuery = metricsGeoQuery;
    }

    public CommonStreamsQuery getCommonStreamsQuery() {
        return commonStreamsQuery;
    }

    public void setCommonStreamsQuery(CommonStreamsQuery commonStreamsQuery) {
        this.commonStreamsQuery = commonStreamsQuery;
    }


    public CombinationListQuery getCombinationListQuery() {
        return combinationListQuery;
    }

    public void setCombinationListQuery(CombinationListQuery combinationListQuery) {
        this.combinationListQuery = combinationListQuery;
    }

    public CombinationGeoQuery getCombinationGeoQuery() {
        return combinationGeoQuery;
    }

    public void setCombinationGeoQuery(CombinationGeoQuery combinationGeoQuery) {
        this.combinationGeoQuery = combinationGeoQuery;
    }

    public EventGeoQuery getEventGeoQuery() {
        return eventGeoQuery;
    }

    public void setEventGeoQuery(EventGeoQuery eventGeoQuery) {
        this.eventGeoQuery = eventGeoQuery;
    }

    @Override
    public Object execute() {
        return null;
    }
}
