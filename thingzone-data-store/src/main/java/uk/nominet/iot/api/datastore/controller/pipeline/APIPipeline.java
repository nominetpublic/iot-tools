/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.controller.pipeline;

import com.google.gson.JsonObject;
import uk.nominet.iot.api.datastore.DatastoreApp;
import uk.nominet.iot.api.datastore.model.PipelineRegistryEntity;

import java.util.List;

interface APIPipeline {

    List<PipelineRegistryEntity> pipe(List<PipelineRegistryEntity> input, DatastoreApp api) throws Exception;

    List<PipelineRegistryEntity> pipe(DatastoreApp api) throws Exception;

    JsonObject collect(List<PipelineRegistryEntity> input, DatastoreApp api) throws Exception;

    JsonObject collect(DatastoreApp api) throws Exception;
}
