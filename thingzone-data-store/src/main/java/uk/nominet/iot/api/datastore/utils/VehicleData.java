/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.utils;

import java.util.Date;

class VehicleData {
    private final Date timestamp;
    private final int autonomy;
    private final double lon, lat;
    private final double velocity;
    private boolean parking;
    /*private final double lateralAcceleration;
    private final double heading;
    private final double steeringAngle;
    private final double speedLimit;
    private final double brakeMagnitude;
    private final double batteryCharge;
    private final double route;
*/
//    public VehicleData(int autonomy, double lon, double lat, double velocity, double lateralAcceleration
//            , double heading, double steeringAngle, double speedLimit, double brakeMagnitude
 //           , double batteryCharge, double route){
    public VehicleData(Date timestamp, int autonomy, double lat, double lon, double velocity){
        this.timestamp = timestamp;
        this.autonomy = autonomy;
        this.lon = lon;
        this.lat = lat;
        this.velocity = velocity;
        /*this.lateralAcceleration = lateralAcceleration;
        this.heading = heading;
        this.steeringAngle = steeringAngle;
        this.speedLimit = speedLimit;
        this.brakeMagnitude = brakeMagnitude;
        this.batteryCharge = batteryCharge;
        this.route = route;*/
    }


    public Date getTimestamp() {
        return timestamp;
    }

    public int getAutonomy() {
        return autonomy;
    }

    public double getLon(){
        return lon;
    }

    public double getLat(){
        return lat;
    }

    public double getVelocity() {
        return velocity;
    }

    public boolean getParkingStat() { return parking; }

    public void setParkingStat(boolean parking) { this.parking = parking; }

/*    public double getLateralAcceleration() {
        return lateralAcceleration;
    }

    public double getHeading() {
        return heading;
    }

    public double getSteeringAngle() {
        return steeringAngle;
    }

    public double getSpeedLimit() {
        return speedLimit;
    }

    public double getBrakeMagnitude() {
        return brakeMagnitude;
    }

    public double getBatteryCharge() {
        return batteryCharge;
    }

    public double getRoute() {
        return route;
    }*/
}

