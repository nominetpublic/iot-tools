/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.query;

import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.core.search.aggregation.DateHistogramAggregation;
import io.searchbox.core.search.aggregation.ExtendedStatsAggregation;
import io.searchbox.core.search.aggregation.FilterAggregation;
import io.searchbox.core.search.aggregation.TermsAggregation;

import org.elasticsearch.common.joda.DateMathParser;
import org.elasticsearch.index.mapper.DateFieldMapper;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.join.query.HasParentQueryBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filter.FilterAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.histogram.Histogram;
import org.elasticsearch.search.aggregations.metrics.stats.extended.ExtendedStatsAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.json.JSONObject;
import uk.nominet.iot.api.datastore.model.DataStreamValues;
import uk.nominet.iot.api.datastore.utils.AggregationUtils;
import uk.nominet.iot.api.datastore.utils.ArrayUtils;
import uk.nominet.iot.api.datastore.utils.ElasticSearchUtils;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.model.api.APIException;
import uk.nominet.iot.model.timeseries.Stats;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.*;

import static uk.nominet.iot.api.datastore.utils.ElasticSearchUtils.timeDelta;
import static uk.nominet.iot.utils.ConversionUtils.asciiToHex;

public class MetricsAggQuery extends DatastoreListQuery<Stats, List<DataStreamValues<Stats>>> implements ElasticsearchBasicQuery {
    private String[] streams;
    private String metricType;
    private String[] fields;
    private Double interval;
    private boolean groupByStream;

    String[] getStreams() {
        return streams;
    }

    public void setStreams(String[] streams) {
        this.streams = streams;
    }

    public String getMetricType() {
        return metricType;
    }

    public void setMetricType(String metricType) {
        this.metricType = metricType;
    }

    private String[] getFields() {
        return fields;
    }

    private void setFields(String[] fields) {
        this.fields = fields;
    }

    private Double getInterval() {
        return interval;
    }

    private void setInterval(Double interval) {
        this.interval = interval;
    }

    private boolean isGroupByStream() {
        return groupByStream;
    }

    private void setGroupByStream(boolean groupByStream) {
        this.groupByStream = groupByStream;
    }

    public MetricsAggQuery(JSONObject requestBody, String user) throws Exception {
        super(requestBody, user);
        getSchema().validate(requestBody);

        setUser(user);

        if (requestBody.has("streams"))
            setStreams(ArrayUtils.toStringArray(requestBody.getJSONArray("streams")));

        if (requestBody.has("time"))
            setTime(requestBody.getString("time"));

        if (requestBody.has("timeLimit"))
            setTimeLimit(requestBody.getString("timeLimit"));

        if (requestBody.has("forward"))
            setForward(requestBody.getBoolean("forward"));
        else
            setForward(false);

        if (requestBody.has("size"))
            setSize(requestBody.getInt("size"));

        setMetricType(requestBody.has("metricType") ? requestBody.getString("metricType") : "value_int");

        if (requestBody.has("fields"))
            setFields(ArrayUtils.toStringArray(requestBody.getJSONArray("fields")));

        if (requestBody.has("interval"))
            setInterval(requestBody.getDouble("interval"));

        setGroupByStream(requestBody.optBoolean("groupByStream", false));

    }

    @Override
    public List<DataStreamValues<Stats>> execute() throws APIException {

        List<DataStreamValues<Stats>> results = new ArrayList<>();

        String index;
        try {
            index = ElasticSearchUtils.INDEX_PREFIX + asciiToHex(getUser());
        } catch (Exception e) {
            throw new APIException(400, "Could not identify elasticsearch index", e);
        }

        SearchSourceBuilder searchBuilder = new SearchSourceBuilder()
                                                    .query(getQueryBuilder())
                                                    .aggregation(getAggregationBuilder())
                                                    .size(0);

        Search search = new Search.Builder(searchBuilder.toString())
                                .addIndex(index)
                                .addType(ElasticSearchUtils.METRIC_TYPE)
                                .build();

        SearchResult result;
        try {
            result = ElasticsearchDriver.getClient().execute(search);

            if (result.isSucceeded()) {

                if (isGroupByStream()) {
                    TermsAggregation streams = result.getAggregations().getTermsAggregation("streams");

                    for (TermsAggregation.Entry stream : streams.getBuckets()) {
                        JsonObject streamInfo;
                        try {
                            streamInfo = ElasticSearchUtils.getStreamInfo(index, stream.getKeyAsString());
                        } catch (Exception e) {
                            throw new APIException(400, "Could not get stream info", e);
                        }

                        FilterAggregation metrics = stream.getFilterAggregation("metrics");

                        DataStreamValues<Stats> timeseries = new DataStreamValues<>();
                        timeseries.setStreamKey(streamInfo.get("key").getAsString());

                        if (streamInfo.has("name"))
                            timeseries.setName(streamInfo.get("name").getAsString());

                        if (streamInfo.has("type"))
                            timeseries.setType(streamInfo.get("type").getAsString());

                        if (streamInfo.has("metadata")) {
                            Map<String, Object> metadata =
                                    jsonStringSerialiser
                                            .getDefaultSerialiser()
                                            .fromJson(streamInfo.get("metadata").getAsJsonObject(),
                                                      new TypeToken<Map<String, Object>>() {
                                                      }.getType());
                            timeseries.setMetadata(metadata);
                        }

                        timeseries.setMerged(false);

                        timeseries.setValues(extractStats(metrics));

                        results.add(timeseries);
                    }

                } else {
                    FilterAggregation metrics = result.getAggregations()
                                                      .getFilterAggregation("metrics");


                    DataStreamValues<Stats> timeseries = new DataStreamValues<>();

                    timeseries.setStreamKey(String.join(",", getStreams()));
                    timeseries.setMerged(true);
                    timeseries.setValues(extractStats(metrics));

                    results.add(timeseries);
                }

            } else {
                throw new APIException(400, result.getErrorMessage());
            }

        } catch (IOException e) {
            throw new APIException(400, "Could not send request to elasticsearch", e);
        }

        return results;
    }


    private List<Stats> extractStats(FilterAggregation metrics) {


        List<Stats> stats = new ArrayList<>();

        if (getInterval() != null) {
            DateHistogramAggregation dateHistogramAggregation =
                    metrics.getDateHistogramAggregation("time_intervals");

            for (DateHistogramAggregation.DateHistogram timeInterval : dateHistogramAggregation.getBuckets()) {
                ExtendedStatsAggregation statsAggregation =
                        timeInterval.getExtendedStatsAggregation("stats");

                Stats statsObject = AggregationUtils.toStats(statsAggregation, getFields());
                statsObject.setTimestamp(Instant.parse(timeInterval.getTimeAsString()));
                stats.add(statsObject);
            }
        } else {
            ExtendedStatsAggregation statsAggregation = metrics.getExtendedStatsAggregation("stats");
            Stats statsObject = AggregationUtils.toStats(statsAggregation, getFields());
            stats.add(statsObject);
        }

        return stats;
    }


    @Override
    public QueryBuilder getQueryBuilder() {
        TermsQueryBuilder termsQueryBuilder = new TermsQueryBuilder("_id", streams);
        return new HasParentQueryBuilder("stream", termsQueryBuilder, false);
    }

    @Override
    public AggregationBuilder getAggregationBuilder() {

        String from = "now";
        String to = "now-1h";

        String initialTime = (time != null) ? time : "now";

        if (forward) {
            from = initialTime;
            if (timeLimit != null)
                to = timeDelta(initialTime, timeLimit, forward);
        } else {
            to = initialTime;
            if (timeLimit != null) {
                from = timeDelta(initialTime, timeLimit, forward);
            }
        }

        //Aggregation
        FilterAggregationBuilder metricsAgg = AggregationBuilders
                                                      .filter("metrics", QueryBuilders.rangeQuery("timestamp")
                                                                                      .from(from)
                                                                                      .to(to));

        ExtendedStatsAggregationBuilder extendedStats = AggregationBuilders.extendedStats("stats").field(metricType);

        if (interval != null) {

            double offset = 0;

            DateMathParser dmp = new DateMathParser(DateFieldMapper.DEFAULT_DATE_TIME_FORMATTER);
            Instant queryTime = (forward) ? Instant.ofEpochMilli(dmp.parse(from, () -> Instant.now().toEpochMilli()))
                                : Instant.ofEpochMilli(dmp.parse(to, () -> Instant.now().toEpochMilli())) ;

            if (interval % 31536000 == 0) {
                Instant before = ZonedDateTime.ofInstant(queryTime, ZoneId.systemDefault())
                                              .with(TemporalAdjusters.firstDayOfYear())
                                              .truncatedTo(ChronoUnit.DAYS).toInstant();
                offset = Duration.between(before,
                                          queryTime).toMillis();
            } else if (interval % 2592000 == 0) {
                Instant before = ZonedDateTime.ofInstant(queryTime, ZoneId.systemDefault())
                                              .with(TemporalAdjusters.firstDayOfMonth())
                                              .truncatedTo(ChronoUnit.DAYS).toInstant();
                offset = Duration.between(before, queryTime).toMillis();
            } else if (interval % 86400 == 0) {
                offset = Duration.between(queryTime.truncatedTo(ChronoUnit.DAYS), queryTime).toMillis();
            } else if (interval % 3600 == 0) {
                offset = Duration.between(queryTime.truncatedTo(ChronoUnit.HOURS),queryTime).toMillis();
            } else if (interval % 60 == 0) {
                offset = Duration.between(queryTime.truncatedTo(ChronoUnit.MINUTES), queryTime).toMillis();
            }


            metricsAgg.subAggregation(
                    AggregationBuilders
                            .histogram("time_intervals")
                            .field("timestamp")
                            .offset(offset)
                            .order(Histogram.Order.KEY_ASC)
                            .interval(interval * 1000)
                            .subAggregation(extendedStats));
        } else {
            metricsAgg.subAggregation(extendedStats);
        }

        if (!groupByStream)
            return metricsAgg;

        return AggregationBuilders.terms("streams")
                                  .field("streamKey.keyword")
                                  .subAggregation(metricsAgg);


    }

    @Override
    public String getSortName() {
        return null;
    }

    @Override
    public SortOrder getSortOrder() {
        return null;
    }


}
