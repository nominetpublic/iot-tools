/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.query;

import org.json.JSONArray;
import org.json.JSONObject;
import uk.nominet.iot.api.datastore.model.DataStreamGeo;
import uk.nominet.iot.api.datastore.model.DataStreamValues;
import uk.nominet.iot.api.datastore.tools.TimeseriesGeoAligner;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CombinationGeoQuery extends DatastoreGeoQuery<AbstractTimeseriesPoint, List<DataStreamGeo<AbstractTimeseriesPoint>>> {

    private DatastoreGeoQuery primary;
    private Map<String, DatastoreListQuery> secondary;

    public CombinationGeoQuery(JSONObject requestBody, String user) throws Exception {
        super(requestBody, user);
        getSchema().validate(requestBody);

        if (requestBody.has("primary")) primary = parseGeoQuery(requestBody.getJSONObject("primary"));
        else throw new Exception("Primary query missing");

        if (requestBody.has("secondary")) {
            secondary = new HashMap<>();
            JSONArray secondaryQueries = requestBody.getJSONArray("secondary");

            for (Object secondaryQuery : secondaryQueries) {
                DatastoreListQuery datastoreListQuery = parseListQuery((JSONObject)secondaryQuery);
                datastoreListQuery.setTime(primary.getTime());
                datastoreListQuery.setTimeLimit(primary.getTimeLimit());
                datastoreListQuery.setForward(false);
                secondary.put(((JSONObject)secondaryQuery).getString("streamName"),
                              datastoreListQuery);
            }

        }
        else throw new Exception("secondary query missing");

    }

    public DatastoreGeoQuery getPrimary() {
        return primary;
    }

    public void setPrimary(DatastoreGeoQuery primary) {
        this.primary = primary;
    }

    public Map<String, DatastoreListQuery> getSecondary() {
        return secondary;
    }

    public void setSecondary(Map<String, DatastoreListQuery> secondary) {
        this.secondary = secondary;
    }

    /**
     * Parse a list-type json query
     * @param query json object with the query
     * @return ListQuery object
     */
    private  DatastoreGeoQuery parseGeoQuery(JSONObject query) throws Exception {
        if (query.has("metricsGeoQuery"))
            return new MetricsGeoQuery(query.getJSONObject("metricsGeoQuery"), getUser());
        else if (query.has("alertGeoQuery"))
            return new EventGeoQuery(query.getJSONObject("alertGeoQuery"), getUser());
        else if (query.has("eventGeoQuery"))
            return new EventGeoQuery(query.getJSONObject("eventGeoQuery"), getUser());
        else throw new Exception("Could not find a valid query in the Json object");
    }

    /**
     * Parse a list-type json query
     * @param query json object with the query
     * @return ListQuery object
     */
    private  DatastoreListQuery parseListQuery(JSONObject query) throws Exception {
        if (query.has("metricsListQuery"))
            return new MetricsListQuery(query.getJSONObject("metricsListQuery"), getUser());
        else if (query.has("alertListQuery"))
            return new EventListQuery(query.getJSONObject("alertListQuery"), getUser());
        else if (query.has("eventListQuery"))
            return new EventListQuery(query.getJSONObject("eventListQuery"), getUser());
        else if (query.has("imagesListQuery"))
            return new ImagesListQuery(query.getJSONObject("imagesListQuery"), getUser());
        else throw new Exception("Could not find a valid query in the Json object");
    }

    @Override
    public List<DataStreamGeo<AbstractTimeseriesPoint>> execute() throws Exception {

        //noinspection unchecked
        List<DataStreamGeo<AbstractTimeseriesPoint>> primaryResults = (List)primary.execute();

        Map<String, List<DataStreamValues<AbstractTimeseriesPoint>>> secondaryResults = new HashMap<>();

        for (Map.Entry<String, DatastoreListQuery> secondaryQueryEntry : secondary.entrySet()) {
            String streamName = secondaryQueryEntry.getKey();

            //noinspection unchecked
            List<DataStreamValues<AbstractTimeseriesPoint>> secondaryResultsList = (List)secondaryQueryEntry.getValue().execute();
            secondaryResults.put(streamName, secondaryResultsList);
        }

        TimeseriesGeoAligner timeseriesGeoAligner = new TimeseriesGeoAligner(primaryResults, secondaryResults);

        return new ArrayList<>(timeseriesGeoAligner.align());
    }


}
