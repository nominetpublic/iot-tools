/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.query;

import com.github.filosganga.geogson.model.Feature;
import com.google.common.collect.ImmutableList;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.json.JSONArray;
import org.json.JSONObject;
import uk.nominet.iot.api.datastore.geo.GeoUtils;
import uk.nominet.iot.api.datastore.model.AnnotatedEvent;
import uk.nominet.iot.api.datastore.model.DataStreamGeo;
import uk.nominet.iot.api.datastore.utils.ElasticSearchUtils;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.model.RelatedRelation;
import uk.nominet.iot.model.api.APIException;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;
import uk.nominet.iot.model.geo.TimeseriesFeatureCollection;
import uk.nominet.iot.model.registry.RegistryDevice;

import uk.nominet.iot.model.timeseries.Event;
import uk.nominet.iot.model.timeseries.TimeseriesGeoFeature;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static uk.nominet.iot.api.datastore.utils.ElasticSearchUtils.timeDelta;
import static uk.nominet.iot.utils.ConversionUtils.asciiToHex;

public class EventGeoQuery extends DatastoreGeoQuery<Event, List<DataStreamGeo<Event>>> implements ElasticsearchBasicQuery {
    private List<String> types;
    private List<String> eventType;
    private List<List<GeoPoint>> polygons = new ArrayList<>();
    private List<Double> severity;
    private List<String> tags;
    private List<String> keywords;
    private List<String> status;
    private List<String> devices;
    private RelatedRelation relatedTo;
    private Boolean newEvent;

    public EventGeoQuery(JSONObject requestBody, String user) throws Exception {
        super(requestBody, user);
        getSchema().validate(requestBody);

        setUser(user);
        if (requestBody.has("time"))
            setTime(requestBody.getString("time"));

        if (requestBody.has("timeLimit"))
            setTimeLimit(requestBody.getString("timeLimit"));

        if (requestBody.has("forward"))
            setForward(requestBody.getBoolean("forward"));
        else
            setForward(false);

        if (requestBody.has("size"))
            setSize(requestBody.getInt("size"));

        if (requestBody.has("eventType")) {
            ArrayList<String> eventTypes = new ArrayList<>();
            requestBody.getJSONArray("eventType").toList().forEach(type -> eventTypes.add(type.toString()));
            if (eventTypes.size() > 0)
                setEventType(eventTypes);
        }

        if (requestBody.has("types")) {
            ArrayList<String> types = new ArrayList<>();
            requestBody.getJSONArray("types").toList().forEach(type -> types.add(type.toString()));

            if (types.size() > 0)
                setTypes(types);
        }

        if (requestBody.has("boundingPolygons")) {
            JSONArray polygons = requestBody.optJSONArray("boundingPolygons");
            List<List<GeoPoint>> parsedPolygons = GeoUtils.parsePolygons(polygons);
            if (parsedPolygons != null) {
                getPolygons().addAll(parsedPolygons);
            }
        }

        if (requestBody.has("severity"))
            setSeverity(ImmutableList.of(requestBody.getJSONArray("severity").getDouble(0),
                                         requestBody.getJSONArray("severity").getDouble(1)));


        if (requestBody.has("tags")) {
            ArrayList<String> tags = new ArrayList<>();
            requestBody.getJSONArray("tags").toList().forEach(tag -> tags.add(tag.toString()));

            if (tags.size() > 0)
                setTags(tags);
        }

        if (requestBody.has("keywords")) {
            ArrayList<String> keywords = new ArrayList<>();
            requestBody.getJSONArray("keywords").toList().forEach(item -> keywords.add(item.toString()));

            if (keywords.size() > 0)
                setKeywords(keywords);
        }

        if (requestBody.has("status")) {
            ArrayList<String> status = new ArrayList<>();
            requestBody.getJSONArray("status").toList().forEach(item -> status.add(item.toString()));

            if (status.size() > 0)
                setStatus(status);
        }

        if (requestBody.has("new"))
            setNewEvent(requestBody.getBoolean("new"));

        if (requestBody.has("devices")) {
            ArrayList<String> devices = new ArrayList<>();
            requestBody.getJSONArray("devices").toList().forEach(item -> devices.add(item.toString()));

            if (devices.size() > 0)
                setDevices(devices);
        }

        if (requestBody.has("relatedTo")) {
            relatedTo = jsonStringSerialiser.readObject(requestBody.get("relatedTo").toString(), RelatedRelation.class);
        }

    }


    public List<String> getEventType() {
        return eventType;
    }

    private void setEventType(List<String> eventType) {
        this.eventType = eventType;
    }


    public List<String> getTypes() {
        return types;
    }

    private void setTypes(List<String> types) {
        this.types = types;
    }

    private List<List<GeoPoint>> getPolygons() {
        return polygons;
    }

    public void setPolygons(List<List<GeoPoint>> polygons) {
        this.polygons = polygons;
    }


    public List<Double> getSeverity() {
        return severity;
    }


    private void setSeverity(List<Double> severity) {
        this.severity = severity;
    }

    public List<String> getTags() {
        return tags;
    }

    private void setTags(List<String> tags) {
        this.tags = tags;
    }


    public List<String> getKeywords() {
        return keywords;
    }

    private void setKeywords(List<String> keyword) {
        this.keywords = keyword;
    }

    public List<String> getStatus() {
        return status;
    }

    private void setStatus(List<String> status) {
        this.status = status;
    }

    public Boolean getNewEvent() {
        return newEvent;
    }

    private void setNewEvent(Boolean newEvent) {
        this.newEvent = newEvent;
    }

    public List<String> getDevices() {
        return devices;
    }

    public void setDevices(List<String> devices) {
        this.devices = devices;
    }


    @Override
    public QueryBuilder getQueryBuilder() {
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.must(new MatchAllQueryBuilder());


        ExistsQueryBuilder existsQueryBuilder = new ExistsQueryBuilder("severity");
        boolQueryBuilder.must(existsQueryBuilder);

        // Time range query

        RangeQueryBuilder timeRangeQueryBuilder = new RangeQueryBuilder("timestamp");

        String initialTime = (time != null) ? time : "now";


        if (forward) {
            timeRangeQueryBuilder = timeRangeQueryBuilder.gte(initialTime);
            if (timeLimit != null)
                timeRangeQueryBuilder = timeRangeQueryBuilder.lte(timeDelta(initialTime, timeLimit, forward));
        } else {
            timeRangeQueryBuilder = timeRangeQueryBuilder.lte(initialTime);

            if (timeLimit != null) {
                timeRangeQueryBuilder = timeRangeQueryBuilder.gte(timeDelta(initialTime, timeLimit, forward));
            }

        }

        boolQueryBuilder.filter(timeRangeQueryBuilder);


        // Severity range query
        if (severity != null) {
            RangeQueryBuilder rangeQueryBuilder = new RangeQueryBuilder("severity")
                                                          .gte(severity.get(0))
                                                          .lte(severity.get(1));
            boolQueryBuilder.filter(rangeQueryBuilder);
        }


        // Tags query
        if (tags != null) {
            BoolQueryBuilder tagsBoolQueryBuilder = new BoolQueryBuilder();

            for (String tag : tags) {
                TermQueryBuilder termQueryBuilder = new TermQueryBuilder("tags.keyword", tag);
                tagsBoolQueryBuilder.should(termQueryBuilder);
            }

            boolQueryBuilder.filter(tagsBoolQueryBuilder);
        }

        // Keyword query
        if (keywords != null) {
            BoolQueryBuilder keywordBoolQueryBuilder = new BoolQueryBuilder();

            for (String term : keywords) {
                MultiMatchQueryBuilder multiMatchQueryBuilder =
                        new MultiMatchQueryBuilder(term, "name", "type", "category");
                keywordBoolQueryBuilder.should(multiMatchQueryBuilder);
            }

            boolQueryBuilder.filter(keywordBoolQueryBuilder);

        }

        // Types query
        if (eventType != null) {
            BoolQueryBuilder eventTypeBoolQueryBuilder = new BoolQueryBuilder();

            for (String et : eventType) {
                MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("eventType.keyword", et);
                eventTypeBoolQueryBuilder.should(matchQueryBuilder);
            }

            boolQueryBuilder.filter(eventTypeBoolQueryBuilder);

        }

        // Types query
        if (types != null) {
            BoolQueryBuilder typeBoolQueryBuilder = new BoolQueryBuilder();

            for (String type : types) {
                MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("type.keyword", type);
                typeBoolQueryBuilder.should(matchQueryBuilder);
            }

            boolQueryBuilder.filter(typeBoolQueryBuilder);
        }

        //Status Query
        if (status != null) {
            BoolQueryBuilder statusBoolQueryBuilder = new BoolQueryBuilder();

            for (String statusItem : status) {
                MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("status.keyword", statusItem);
                statusBoolQueryBuilder.should(matchQueryBuilder);
            }

            boolQueryBuilder.filter(statusBoolQueryBuilder);
        }


        //Unread/read query
        if (newEvent != null) {
            BoolQueryBuilder readBoolQueryBuilder = new BoolQueryBuilder();
            readBoolQueryBuilder.must(new TermsQueryBuilder("feedback.type", "system"));
            readBoolQueryBuilder.must(new TermsQueryBuilder("feedback.category", "read"));
            readBoolQueryBuilder.must(new TermsQueryBuilder("feedback.user", getUser()));

            if (!newEvent) {
                boolQueryBuilder.filter(readBoolQueryBuilder);
            } else {
                BoolQueryBuilder unreadBoolQueryBuilder = new BoolQueryBuilder();
                unreadBoolQueryBuilder.mustNot(readBoolQueryBuilder);
                boolQueryBuilder.filter(unreadBoolQueryBuilder);
            }

        }

        if (devices != null) {
            BoolQueryBuilder devicesBoolQueryBuilder = new BoolQueryBuilder();

            for (String device : devices) {
                MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("relatedTo.key.keyword", device);
                devicesBoolQueryBuilder.should(matchQueryBuilder);
            }

            boolQueryBuilder.filter(devicesBoolQueryBuilder);
        }

        if (polygons != null && polygons.size() > 0) {
            if (polygons.size() > 1) {
                BoolQueryBuilder polygonsQuery = QueryBuilders.boolQuery();
                for (List<GeoPoint> polygon : polygons) {
                    polygonsQuery.should(ElasticSearchUtils.buildGeoShapeEnvelopeQuery(polygon, "location"));
                }
                polygonsQuery.minimumShouldMatch(1);
                boolQueryBuilder.filter(polygonsQuery);
            } else {
                boolQueryBuilder.filter(ElasticSearchUtils.buildGeoShapeEnvelopeQuery(polygons.get(0), "location"));
            }
        }

        if (relatedTo != null) {
            BoolQueryBuilder devicesBoolQueryBuilder = new BoolQueryBuilder();

            if (relatedTo.key != null) {
                MatchQueryBuilder matchQueryBuilderKey =
                        new MatchQueryBuilder("relatedTo.key.keyword", relatedTo.key);
                devicesBoolQueryBuilder.must(matchQueryBuilderKey);
            }
            if( relatedTo.klass != null) {
                MatchQueryBuilder matchQueryBuilderKlass =
                        new MatchQueryBuilder("relatedTo.klass.keyword", relatedTo.klass);
                devicesBoolQueryBuilder.must(matchQueryBuilderKlass);
            }

            boolQueryBuilder.filter(devicesBoolQueryBuilder);
        }

        return boolQueryBuilder;
    }


    @Override
    public AggregationBuilder getAggregationBuilder() {
        return null;
    }

    @Override
    public String getSortName() {
        return "timestamp";
    }

    @Override
    public SortOrder getSortOrder() {
        if (forward)
            return SortOrder.ASC;
        else
            return SortOrder.DESC;
    }


    @Override
    public String toString() {
        return "EventGeoQuery{" +
               "types=" + types +
               ", eventType='" + eventType + '\'' +
               ", polygons=" + polygons +
               ", severity=" + severity +
               ", tags=" + tags +
               ", keywords=" + keywords +
               ", status=" + status +
               ", devices=" + devices +
               ", newEvent=" + newEvent +
               ", streams=" + Arrays.toString(streams) +
               ", time='" + time + '\'' +
               ", timeLimit='" + timeLimit + '\'' +
               ", forward=" + forward +
               ", size=" + size +
               '}';
    }

    @Override
    public List<DataStreamGeo<Event>> execute() throws Exception {

        List<DataStreamGeo<Event>> results = new ArrayList<>();

        String index;
        try {
            index = ElasticSearchUtils.INDEX_PREFIX + asciiToHex(getUser());
        } catch (Exception e) {
            throw new APIException(400, "Could not identify elasticsearch index", e);
        }

        SearchSourceBuilder searchBuilder = new SearchSourceBuilder()
                                                    .query(getQueryBuilder())
                                                    .sort(getSortName(), getSortOrder())
                                                    .size((getSize() != null) ? getSize() : 10000);

        Search search = new Search.Builder(searchBuilder.toString())
                                .addIndex(index)
                                .addType(ElasticSearchUtils.EVENT_TYPE)
                                .build();

        SearchResult result;

        try {
            result = ElasticsearchDriver.getClient().execute(search);
            if (result.isSucceeded()) {
                DataStreamGeo<Event> dataStreamGeo = new DataStreamGeo<>();

                TimeseriesFeatureCollection<Event> timeseriesFeatureCollection = new TimeseriesFeatureCollection<>();

                Map<Integer, MutablePair<String, List<AnnotatedEvent>>> groups = new HashMap<>();

                // Build groups
                for (SearchResult.Hit<JsonObject, Void> hit : result.getHits(JsonObject.class)) {
                    AnnotatedEvent alert = EventListQuery.annotateEvent(
                            jsonStringSerialiser.readObject(hit.source, AnnotatedEvent.class),
                            getUser());
                    List<String> deviceKeys = alert.getRelatedTo().stream()
                                                   .filter(item -> item.getKlass().equals(RegistryDevice.class.getName()))
                                                   .map(RelatedRelation::getKey)
                                                   .collect(Collectors.toList());

                    if (deviceKeys != null) {
                        deviceKeys.forEach(deviceKey -> {
                            int hashCode = getMapGroupId(deviceKey, alert);
                            if (!groups.containsKey(hashCode))
                                groups.put(hashCode, new MutablePair<>(deviceKey, new ArrayList<>()));

                            if (!groups.get(hashCode).getValue().contains(alert)) groups.get(hashCode).getValue().add(alert);
                        });
                    }
                }


                List<String> streams = new ArrayList<>();
                groups.forEach((key, alertsGroup) -> {
                    TimeseriesGeoFeature timeseriesGeoFeature = new TimeseriesGeoFeature();
                    String deviceKey = alertsGroup.getKey();
                    Double severity = alertsGroup.getValue()
                                                 .stream()
                                                 .mapToDouble(Event::getSeverity)
                                                 .max()
                                                 .orElse(1.0);

                    Map<String, JsonElement> additionalProperties = new HashMap<>();

                    additionalProperties.put("id", new JsonPrimitive(String.format("%s_%d", alertsGroup.getKey(), key)));
                    additionalProperties.put("deviceKey", new JsonPrimitive(deviceKey));
                    additionalProperties.put("type", new JsonPrimitive("group"));
                    additionalProperties.put("size", new JsonPrimitive(alertsGroup.getValue().size()));
                    additionalProperties.put("severity", new JsonPrimitive(severity));


                    timeseriesGeoFeature.setAssociatedPoints(alertsGroup.getValue());
                    //noinspection unchecked
                    timeseriesGeoFeature.setPoint(alertsGroup.getValue().get(0));
                    timeseriesGeoFeature.setFeature(Feature.builder()
                                                           .withGeometry(alertsGroup.getValue().get(0).getLocation())
                                                           .build());

                    timeseriesGeoFeature.setAdditionalProperties(additionalProperties);

                    streams.addAll(alertsGroup.getValue()
                                              .stream()
                                              .map(AbstractTimeseriesPoint::getStreamKey)
                                              .collect(Collectors.toList()));
                    //noinspection unchecked
                    timeseriesFeatureCollection.add(timeseriesGeoFeature);
                });


                dataStreamGeo.setFeatures(timeseriesFeatureCollection);
                dataStreamGeo.setStreamKey(StringUtils.join(streams.stream().distinct().collect(Collectors.toList()), ","));
                dataStreamGeo.setMerged(true);
                results.add(dataStreamGeo);

            } else {
                throw new APIException(400, result.getErrorMessage());
            }

        } catch (IOException e) {
            throw new APIException(400, "Could not send request to elasticsearch", e);
        }

        return results;
    }

    private int getMapGroupId(String deviceKey, Event alert) {
        return Objects.hash(deviceKey, alert.getLocation());
    }
}
