/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.controller.pipeline;

import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import uk.nominet.iot.api.datastore.DatastoreApp;
import uk.nominet.iot.api.datastore.model.PipelineRegistryEntity;
import uk.nominet.iot.api.datastore.query.DeviceLatestQuery;
import uk.nominet.iot.json.JsonStringSerialiser;

import java.util.List;

public class GeoSearchPipe implements APIPipeline {
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
    private DeviceLatestQuery query;
    private List<PipelineRegistryEntity> deviceList;


    public GeoSearchPipe(DeviceLatestQuery query) throws Exception {
        if (query == null) throw new Exception("Missing geo aggregation query");
        this.query = query;
    }

    public GeoSearchPipe(List<PipelineRegistryEntity> deviceList) {
      this.deviceList = deviceList;
    }


    @Override
    public List<PipelineRegistryEntity> pipe(List<PipelineRegistryEntity> input, DatastoreApp api) throws Exception {
        if (deviceList != null) return deviceList;

        JsonObject geoSearchResponse = collect(input, api);

        return jsonStringSerialiser
                                          .getDefaultSerialiser()
                                          .fromJson(geoSearchResponse.getAsJsonArray("devices"),
                                                    new TypeToken<List<PipelineRegistryEntity>>() {
                                                    }.getType());
    }

    @Override
    public List<PipelineRegistryEntity> pipe(DatastoreApp api) throws Exception {
        return pipe(null,  api);
    }

    @Override
    public JsonObject collect(List<PipelineRegistryEntity> input, DatastoreApp api) throws Exception {
        if (api == null) throw new Exception("Datastore api not provided");
        JsonObject output = new JsonObject();
        output.add("devices", jsonStringSerialiser.getDefaultSerialiser().toJsonTree(query.execute().toDeviceList()));
        return output;
    }

    @Override
    public JsonObject collect(DatastoreApp api) throws Exception {
        return collect(null, api);
    }
}
