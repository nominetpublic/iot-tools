/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.query;

import org.json.JSONObject;
import uk.nominet.iot.api.datastore.model.DataStreamGeo;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;

import java.util.List;

public abstract class DatastoreGeoQuery<V extends AbstractTimeseriesPoint, T extends List<DataStreamGeo<V>>> extends DatastoreQuery<T> {

    String[] streams;
    String time;
    String timeLimit;
    boolean forward;
    Integer size;

    String[] getStreams() {
        return streams;
    }

    public void setStreams(String[] streams) {
        this.streams = streams;
    }

    String getTime() {
        return time;
    }

    void setTime(String time) {
        this.time = time;
    }

    String getTimeLimit() {
        return timeLimit;
    }

    void setTimeLimit(String timeLimit) {
        this.timeLimit = timeLimit;
    }

    Integer getSize() {
        return size;
    }

    void setSize(Integer size) {
        this.size = size;
    }

    public boolean isForward() {
        return forward;
    }

    void setForward(boolean forward) {
        this.forward = forward;
    }

    DatastoreGeoQuery(JSONObject requestBody, String user) throws Exception {
        super(requestBody, user);
    }
}
