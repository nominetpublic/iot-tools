/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.geo.model;

import java.util.Objects;

public class OpenStreetMapSegment {
    private Long startNodeRef;
    private Long endNodeRef;
    private Long wayRef;

    public OpenStreetMapSegment() {}

    public OpenStreetMapSegment(Long startNodeRef, Long endNodeRef, Long wayRef) {
        this.startNodeRef = startNodeRef;
        this.endNodeRef = endNodeRef;
        this.wayRef = wayRef;
    }

    public Long getStartNodeRef() {
        return startNodeRef;
    }

    public Long getEndNodeRef() {
        return endNodeRef;
    }

    public Long getWayRef() {
        return wayRef;
    }

    @Override
    public String toString() {
        return String.format("%d,%d,%d", startNodeRef, endNodeRef, wayRef);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OpenStreetMapSegment that = (OpenStreetMapSegment) o;
        return Objects.equals(startNodeRef, that.startNodeRef) &&
                Objects.equals(endNodeRef, that.endNodeRef) &&
                Objects.equals(wayRef, that.wayRef);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startNodeRef, endNodeRef, wayRef);
    }
}
