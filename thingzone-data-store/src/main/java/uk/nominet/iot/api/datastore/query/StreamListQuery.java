/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.query;

import org.elasticsearch.index.query.*;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.json.JSONArray;
import org.json.JSONObject;
import uk.nominet.iot.api.datastore.errors.IllegalQueryException;
import uk.nominet.iot.api.datastore.utils.ArrayUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StreamListQuery extends DatastoreQuery<Object> implements ElasticsearchBasicQuery {

    public enum OutputFormat {
        STREAMS, STREAMKEYS
    }

    private OutputFormat outputType = null;
    private List<String> devices = new ArrayList<>();

    private String name;
    private List<String> secondaryNames;



    public StreamListQuery(JSONObject requestBody, String user) throws Exception {
        super(requestBody, user);
        getSchema().validate(requestBody);
        setUser(user);

        String outputTypeValue = requestBody.optString("outputType", "streams");
        try {
            setOutputType(OutputFormat.valueOf(outputTypeValue.toUpperCase()));
        } catch (IllegalArgumentException e) {
            throw new IllegalQueryException("outputType", outputTypeValue, "allowed values are: " +
                                                                           Arrays.toString(OutputFormat.values()));
        }

        if (requestBody.has("name")) {
            setName(requestBody.getString("name"));
        }

        if (requestBody.has("secondaryNames") && !requestBody.isNull("secondaryNames")) {
            setSecondaryNames(ArrayUtils.toStringList(requestBody.getJSONArray("secondaryNames")));
        }


        if (requestBody.has("devices") && !requestBody.isNull("devices")) {
            JSONArray deviceIdArray = requestBody.getJSONArray("devices");
            for (int i = 0; i < deviceIdArray.length(); i++) {
                getDevices().add(deviceIdArray.getString(i));
            }
        }
    }

    @Override
    public Object execute() {
        return null;
    }


    public OutputFormat getOutputType() {
        return outputType;
    }

    private List<String> getDevices() {
        return devices;
    }


    public void setOutputType(OutputFormat outputType) {
        this.outputType = outputType;
    }


    public void setDevices(List<String> devices) {
        this.devices = devices;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    public List<String> getSecondaryNames() {
        return secondaryNames;
    }

    public void setSecondaryNames(List<String> secondaryNames) {
        this.secondaryNames = secondaryNames;
    }



    @Override
    public QueryBuilder getQueryBuilder() {
        BoolQueryBuilder booleanQuery = QueryBuilders.boolQuery();

        if(devices != null && devices.size() > 0) {
            booleanQuery.must(QueryBuilders.termsQuery("devices.key.keyword", devices));
        }

        List<String> nameList = new ArrayList<>();
        if (name != null)  nameList.add(name);

        if (secondaryNames != null) nameList.addAll(secondaryNames);

        if(nameList.size() > 0 ) {
            booleanQuery.must(QueryBuilders.termsQuery("name.keyword", nameList));
        }


        return booleanQuery;
    }

    @Override
    public AggregationBuilder getAggregationBuilder() {
        return AggregationBuilders.terms("devices")
                                  .field("devices.key.keyword")
                                  .size(10000)
                                  .subAggregation(AggregationBuilders
                                                          .topHits("streams")
                                                          .size(10000));
    }

    @Override
    public String getSortName() {
        return null;
    }

    @Override
    public SortOrder getSortOrder() {
        return null;
    }

}
