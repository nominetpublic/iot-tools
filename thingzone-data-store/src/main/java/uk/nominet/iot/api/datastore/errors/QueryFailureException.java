/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.errors;

import io.searchbox.client.JestResult;

public class QueryFailureException extends Exception {
    private static final long serialVersionUID = -3574784009726516403L;
    private final JestResult result;

    public QueryFailureException(JestResult result) {
        this.result = result;
    }

    @Override
    public String getMessage() {
        return String.format("ElasticSearch query failed with the following HTTP %d response: %s",
                result.getResponseCode(), result.getErrorMessage());
    }
}
