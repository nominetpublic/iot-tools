/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.query;

import com.google.common.collect.ImmutableList;
import com.google.gson.JsonObject;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.filter.FilterAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.json.JSONArray;
import org.json.JSONObject;
import uk.nominet.iot.api.datastore.geo.GeoUtils;
import uk.nominet.iot.api.datastore.model.AnnotatedEvent;
import uk.nominet.iot.api.datastore.model.DataStreamValues;
import uk.nominet.iot.api.datastore.utils.ArrayUtils;
import uk.nominet.iot.api.datastore.utils.ElasticSearchUtils;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.model.Feedback;
import uk.nominet.iot.model.RelatedRelation;
import uk.nominet.iot.model.api.APIException;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;
import uk.nominet.iot.model.timeseries.Event;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static uk.nominet.iot.utils.ConversionUtils.asciiToHex;

public class EventListQuery extends DatastoreListQuery<AnnotatedEvent, List<DataStreamValues<AnnotatedEvent>>> implements ElasticsearchBasicQuery {
    private List<String>  eventType;
    private List<List<GeoPoint>> polygons = new ArrayList<>();
    private List<String> types;
    private List<Double> severity;
    private List<String> tags;
    private List<String> keywords;
    private List<String> status;
    private List<String> devices;
    private RelatedRelation relatedTo;
    private Boolean newEvent;

    public EventListQuery(JSONObject requestBody, String user) throws Exception {
        super(requestBody, user);
        getSchema().validate(requestBody);

        setUser(user);

        if (requestBody.has("streams"))
            setStreams(ArrayUtils.toStringArray(requestBody.getJSONArray("streams")));

        if (requestBody.has("time"))
            setTime(requestBody.getString("time"));

        if (requestBody.has("timeLimit"))
            setTimeLimit(requestBody.getString("timeLimit"));

        if (requestBody.has("forward"))
            setForward(requestBody.getBoolean("forward"));
        else
            setForward(false);

        if (requestBody.has("size"))
            setSize(requestBody.getInt("size"));

        if (requestBody.has("eventType")) {
            ArrayList<String> eventTypes = new ArrayList<>();
            requestBody.getJSONArray("eventType").toList().forEach(type -> eventTypes.add(type.toString()));

            if (eventTypes.size() > 0)
                setEventType(eventTypes);
        }

        if (requestBody.has("types")) {
            ArrayList<String> types = new ArrayList<>();
            requestBody.getJSONArray("types").toList().forEach(type -> types.add(type.toString()));

            if (types.size() > 0)
                setTypes(types);
        }

        if (requestBody.has("boundingPolygons")) {
            JSONArray polygons = requestBody.optJSONArray("boundingPolygons");
            List<List<GeoPoint>> parsedPolygons = GeoUtils.parsePolygons(polygons);
            if (parsedPolygons != null) {
                getPolygons().addAll(parsedPolygons);
            }
        }

        if (requestBody.has("severity"))
            setSeverity(ImmutableList.of(requestBody.getJSONArray("severity").getDouble(0),
                                               requestBody.getJSONArray("severity").getDouble(1)));


        if (requestBody.has("tags")) {
            ArrayList<String> tags = new ArrayList<>();
            requestBody.getJSONArray("tags").toList().forEach(tag -> tags.add(tag.toString()));

            if (tags.size() > 0)
                setTags(tags);
        }

        if (requestBody.has("keywords")) {
            ArrayList<String> keywords = new ArrayList<>();
            requestBody.getJSONArray("keywords").toList().forEach(item -> keywords.add(item.toString()));

            if (keywords.size() > 0)
                setKeywords(keywords);
        }

        if (requestBody.has("status")) {
            ArrayList<String> status = new ArrayList<>();
            requestBody.getJSONArray("status").toList().forEach(item -> status.add(item.toString()));

            if (status.size() > 0)
                setStatus(status);
        }

        if (requestBody.has("new"))
            setNewEvent(requestBody.getBoolean("new"));

        if (requestBody.has("devices")) {
            ArrayList<String> devices = new ArrayList<>();
            requestBody.getJSONArray("devices").toList().forEach(item -> devices.add(item.toString()));

            if (devices.size() > 0)
                setDevices(devices);
        }

        if (requestBody.has("relatedTo")) {
            relatedTo = jsonStringSerialiser.readObject(requestBody.get("relatedTo").toString(), RelatedRelation.class);
        }


    }

    public List<List<GeoPoint>> getPolygons() {
        return polygons;
    }

    public void setPolygons(List<List<GeoPoint>> polygons) {
        this.polygons = polygons;
    }

    public List<String> getTypes() {
        return types;
    }

    private void setTypes(List<String> types) {
        this.types = types;
    }

    public List<String>  getEventType() {
        return eventType;
    }

    private void setEventType(List<String> eventType) {
        this.eventType = eventType;
    }


    public List<Double> getSeverity() {
        return severity;
    }

    private void setSeverity(List<Double> severity) {
        this.severity = severity;
    }

    public List<String> getTags() {
        return tags;
    }

    private void setTags(List<String> tags) {
        this.tags = tags;
    }


    public List<String> getKeywords() {
        return keywords;
    }

    private void setKeywords(List<String> keyword) {
        this.keywords = keyword;
    }

    public List<String> getStatus() {
        return status;
    }

    private void setStatus(List<String> status) {
        this.status = status;
    }

    public Boolean getNewEvent() {
        return newEvent;
    }

    private void setNewEvent(Boolean newEvent) {
        this.newEvent = newEvent;
    }

    public List<String> getDevices() {
        return devices;
    }

    public void setDevices(List<String> devices) {
        this.devices = devices;
    }


    @Override
    public QueryBuilder getQueryBuilder() {
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.must(new MatchAllQueryBuilder());

        RangeQueryBuilder rangeQueryBuilder = ElasticSearchUtils.getRangeBuilder(time, timeLimit, forward);
        if (rangeQueryBuilder != null)  boolQueryBuilder.must(rangeQueryBuilder);

        if (streams != null) {
            TermsQueryBuilder termsQueryBuilder = new TermsQueryBuilder("streamKey.keyword", streams);
            boolQueryBuilder.filter(termsQueryBuilder);
        }

        // Severity range query
        if (severity != null) {
            RangeQueryBuilder severityRangeQueryBuilder = new RangeQueryBuilder("severity")
                                                          .gte(severity.get(0))
                                                          .lte(severity.get(1));
            boolQueryBuilder.filter(severityRangeQueryBuilder);
        }


        // Tags query
        if (tags != null) {
            BoolQueryBuilder tagsBoolQueryBuilder = new BoolQueryBuilder();

            for (String tag : tags) {
                TermQueryBuilder termQueryBuilder = new TermQueryBuilder("tags.keyword", tag);
                tagsBoolQueryBuilder.should(termQueryBuilder);
            }

            boolQueryBuilder.filter(tagsBoolQueryBuilder);
        }

        // Keyword query
        if (keywords!= null) {
            BoolQueryBuilder keywordBoolQueryBuilder = new BoolQueryBuilder();

            for (String term: keywords) {
                MultiMatchQueryBuilder multiMatchQueryBuilder =
                        new MultiMatchQueryBuilder(term, "name", "type", "category");
                keywordBoolQueryBuilder.should(multiMatchQueryBuilder);
            }

            boolQueryBuilder.filter(keywordBoolQueryBuilder);

        }

        // Types query
        if (eventType != null) {
            BoolQueryBuilder typeBoolQueryBuilder = new BoolQueryBuilder();

            for (String type : eventType) {
                MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("eventType.keyword", type);
                typeBoolQueryBuilder.should(matchQueryBuilder);
            }

            //MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("eventType.keyword", eventType);
            boolQueryBuilder.filter(typeBoolQueryBuilder);

        }

        // Types query
        if (types != null) {
            BoolQueryBuilder typeBoolQueryBuilder = new BoolQueryBuilder();

            for (String type : types) {
                MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("type.keyword", type);
                typeBoolQueryBuilder.should(matchQueryBuilder);
            }

            boolQueryBuilder.filter(typeBoolQueryBuilder);
        }

        //Status Query
        if (status != null) {
            BoolQueryBuilder statusBoolQueryBuilder = new BoolQueryBuilder();

            for (String statusItem: status) {
                if (!status.equals("")) {
                    MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("status.keyword", statusItem);
                    statusBoolQueryBuilder.should(matchQueryBuilder);
                }
            }

            boolQueryBuilder.filter(statusBoolQueryBuilder);
        }

        //Unread/read query
        if (newEvent != null) {
            BoolQueryBuilder readBoolQueryBuilder = new BoolQueryBuilder();
            readBoolQueryBuilder.must(new TermsQueryBuilder("feedback.type", "system"));
            readBoolQueryBuilder.must(new TermsQueryBuilder("feedback.category", "read"));
            readBoolQueryBuilder.must(new TermsQueryBuilder("feedback.user", getUser()));

            if (!newEvent) {
                boolQueryBuilder.filter(readBoolQueryBuilder);
            } else {
                BoolQueryBuilder unreadBoolQueryBuilder = new BoolQueryBuilder();
                unreadBoolQueryBuilder.mustNot(readBoolQueryBuilder);
                boolQueryBuilder.filter(unreadBoolQueryBuilder);
            }

        }

        if (devices != null) {
            BoolQueryBuilder devicesBoolQueryBuilder = new BoolQueryBuilder();

            for (String device: devices) {
                MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("relatedTo.key.keyword", device);
                devicesBoolQueryBuilder.should(matchQueryBuilder);
            }

            boolQueryBuilder.filter(devicesBoolQueryBuilder);
        }

        if (polygons != null && polygons.size() > 0) {
            if (polygons.size() > 1) {
                BoolQueryBuilder polygonsQuery = QueryBuilders.boolQuery();
                for (List<GeoPoint> polygon : polygons) {
                    polygonsQuery.should(ElasticSearchUtils.buildGeoShapeEnvelopeQuery(polygon, "location"));
                }
                polygonsQuery.minimumShouldMatch(1);
                boolQueryBuilder.filter(polygonsQuery);
            } else {
                boolQueryBuilder.filter(ElasticSearchUtils.buildGeoShapeEnvelopeQuery(polygons.get(0), "location"));
            }
        }

        if (relatedTo != null) {
            BoolQueryBuilder devicesBoolQueryBuilder = new BoolQueryBuilder();

            if (relatedTo.key != null) {
                MatchQueryBuilder matchQueryBuilderKey =
                        new MatchQueryBuilder("relatedTo.key.keyword", relatedTo.key);
                devicesBoolQueryBuilder.must(matchQueryBuilderKey);
            }
             if( relatedTo.klass != null) {
                 MatchQueryBuilder matchQueryBuilderKlass =
                         new MatchQueryBuilder("relatedTo.klass.keyword", relatedTo.klass);
                 devicesBoolQueryBuilder.must(matchQueryBuilderKlass);
             }

            boolQueryBuilder.filter(devicesBoolQueryBuilder);
        }

        return boolQueryBuilder;
    }

    @Override
    public AggregationBuilder getAggregationBuilder() {
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.must(new TermsQueryBuilder("feedback.type", "system"));
        boolQueryBuilder.must(new TermsQueryBuilder("feedback.category", "read"));
        boolQueryBuilder.must(new TermsQueryBuilder("feedback.user", getUser()));

        return new FilterAggregationBuilder("read", boolQueryBuilder);
    }


    @Override
    public String getSortName() {
        return "timestamp";
    }

    @Override
    public SortOrder getSortOrder() {
        if (forward)
            return SortOrder.ASC;
        else
            return SortOrder.DESC;
    }





    @Override
    public List<DataStreamValues<AnnotatedEvent>> execute() throws Exception {
        String index;
        try {
            index = ElasticSearchUtils.INDEX_PREFIX + asciiToHex(getUser());
        } catch (Exception e) {
            throw new APIException(400, "Could not identify elasticsearch index", e);
        }

        List<DataStreamValues<AnnotatedEvent>> results = new ArrayList<>();

        SearchSourceBuilder searchBuilder = new SearchSourceBuilder()
                                                    .query(getQueryBuilder())
                                                    .aggregation(getAggregationBuilder())
                                                    .sort(getSortName(), getSortOrder())
                                                    .size((getSize() != null) ? getSize() : 50);

        Search search = new Search.Builder(searchBuilder.toString())
                                .addIndex(index)
                                .addType(ElasticSearchUtils.EVENT_TYPE)
                                .build();

        SearchResult result;
        try {
            result = ElasticsearchDriver.getClient().execute(search);

            if (result.isSucceeded()) {

                List<AnnotatedEvent> annotatedEvents = new ArrayList<>();

                for (SearchResult.Hit<JsonObject, Void> jsonObjectVoidHit : result.getHits(JsonObject.class)) {
                    Event alert = jsonStringSerialiser.readObject(jsonObjectVoidHit.source, Event.class);
                    AnnotatedEvent annotatedEvent = annotateEvent(alert, getUser());
                    annotatedEvents.add(annotatedEvent);
                }

                Long total = result.getTotal();
                Long read = result.getAggregations().getFilterAggregation("read").getCount();

                List<String> streams = annotatedEvents
                                               .stream()
                                               .map(AbstractTimeseriesPoint::getStreamKey)
                                               .distinct()
                                               .collect(Collectors.toList());


                DataStreamValues<AnnotatedEvent> alerts = new DataStreamValues<>();
                alerts.setStreamKey(StringUtils.join(streams, ','));
                alerts.setMerged(true);
                alerts.setTotal(total);
                alerts.setUnread(total - read);
                alerts.setValues(annotatedEvents);

                // Change alert order
                if (getSortOrder().equals(SortOrder.ASC) && annotatedEvents.size() > 0) {
                   Collections.reverse(annotatedEvents);
                }

                results.add(alerts);

            } else {
                throw new APIException(400, result.getErrorMessage());
            }

        } catch (IOException e) {
            throw new APIException(400, "Could not send request to elasticsearch", e);
        }

        return results;
    }

    public static AnnotatedEvent annotateEvent(Event alert, String user) throws InvocationTargetException, IllegalAccessException{
        AnnotatedEvent annotatedEvent =  new AnnotatedEvent();
        BeanUtils.copyProperties(annotatedEvent, alert);
        annotatedEvent.setRead(alertWasRead(alert, user));
        return annotatedEvent;
    }

    /**
     * Return true if an alert was read from a specific user
     * @param alert alert json
     * @param user  username
     * @return true/false
     */
    private static Boolean alertWasRead(Event alert, String user) {

        List<Feedback> feedbackList = alert.getFeedback();

        if (feedbackList == null || feedbackList.size() == 0)
            return false;

        for (Feedback feedback : feedbackList) {

            if (feedback.getType() != null &&
                feedback.getCategory() != null &&
                feedback.getUser() != null &&
                feedback.getType().equals("system") &&
                feedback.getCategory() .equals("read") &&
                feedback.getUser().equals(user))
                return true;

        }

        return false;

    }
}
