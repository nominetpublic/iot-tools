/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.query;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.joda.time.Instant;
import org.json.JSONArray;
import org.json.JSONObject;
import uk.nominet.iot.api.datastore.errors.IllegalQueryException;
import uk.nominet.iot.api.datastore.geo.model.OpenStreetMapSegment;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RiskQuery extends DatastoreQuery<Object> {



    @Override
    public Object execute() {
        return null;
    }

    public enum Route {
        BASIC, COMPLEX
    }

    private Route routeId;
    private final List<OpenStreetMapSegment> routeNodes = new ArrayList<>();
    private Instant timestamp;


    public RiskQuery(JSONObject requestBody, String user) throws Exception {
        super(requestBody, user);
        getSchema().validate(requestBody);

        setUser(user);

        setTimestamp(Instant.parse(requestBody.getString("timestamp")));
        if(requestBody.has("routeId")) {
            try {
                setRouteId(RiskQuery.Route.valueOf(requestBody.getString("routeId").toUpperCase()));
            } catch(IllegalArgumentException e) {
                throw new IllegalQueryException("routeId", requestBody.getString("routeId"), "allowed values are: " +
                                                                                             Arrays.toString(RiskQuery.Route.values()).toLowerCase());
            }
        } else {
            JSONArray routeNodes = requestBody.getJSONArray("routeNodes");
            // getRouteNodes can trigger the reading of nodes from a file, but won't because the route ID is null
            List<OpenStreetMapSegment> segmentList = getRouteNodes();
            for(int i = 0; i < routeNodes.length(); i++) {
                JSONObject node = routeNodes.getJSONObject(i);
                OpenStreetMapSegment segment = new OpenStreetMapSegment(
                        node.getLong("start"),
                        node.getLong("end"),
                        node.getLong("way"));
                segmentList.add(segment);
            }
        }
    }

    public Route getRouteId() {
        return routeId;
    }

    public List<OpenStreetMapSegment> getRouteNodes() {
        if(routeId != null && routeNodes.isEmpty()) {
            // load appropriate route nodes from file
            try {
                ClassLoader loader = Thread.currentThread().getContextClassLoader();
                URL url = loader.getResource("routes/" + routeId.name().toLowerCase() + "_route.csv");
                if (url != null) {
                    Iterable<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(
                            new InputStreamReader(url.openStream()));
                    for(CSVRecord record : records) {
                        routeNodes.add(new OpenStreetMapSegment(
                                Long.parseLong(record.get("start_node_ref")),
                                Long.parseLong(record.get("end_node_ref")),
                                Long.parseLong(record.get("way_ref"))));
                    }
                }
            } catch(IOException e) {
                e.printStackTrace();
                return null;
            }
        }
        return routeNodes;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    private void setRouteId(Route routeId) {
        this.routeId = routeId;
    }

    private void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }
}
