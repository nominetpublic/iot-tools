/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.controller;

import com.google.gson.JsonObject;
import org.everit.json.schema.ValidationException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import spark.Request;
import spark.Response;
import spark.Service;
import uk.nominet.iot.api.datastore.errors.IllegalQueryException;
import uk.nominet.iot.api.datastore.model.DataStreamGeo;
import uk.nominet.iot.api.datastore.query.MetricsGeoQuery;
import uk.nominet.iot.http.APIController;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.api.ExceptionHandler;
import uk.nominet.iot.model.api.ResponseBuilder;
import uk.nominet.iot.model.timeseries.Metric;

import java.util.List;


public class MetricsGeoController implements APIController {
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    @Override
    public void publishAPI(Service http, String path) {
        http.path(path, () -> http.post("/geo", this::geo));
    }


    /**
     * Handle request for /metrics/geo
     * @param req http request
     * @param res http response
     * @return response string
     */
    private String geo(Request req, Response res) {
        // Necessary to use org.json dependencies (JSONObject etc.) due to Schema validation library requiring them

        JSONObject queryObject;
        try {
            queryObject = new JSONObject(new JSONTokener(req.body()));
        } catch (JSONException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

        try {
            MetricsGeoQuery query = new MetricsGeoQuery(queryObject, req.attribute("introspected_username"));
            List<DataStreamGeo<Metric>> dataStreams = query.execute();

            ResponseBuilder response = new ResponseBuilder(true)
                                               .setResponse(jsonStringSerialiser.getDefaultSerialiser()
                                                                                .toJsonTree(dataStreams)
                                                                                .getAsJsonArray());

            return response.build().toString();
        } catch (ValidationException e) {
            return ExceptionHandler.handle(res,
                                           new IllegalQueryException(e.getKeyword(),
                                                                     queryObject.toString(),
                                                                     e.getErrorMessage()),
                                           e.getAllMessages().toString());
        } catch (Exception e) {
            e.printStackTrace();
            return ExceptionHandler.handle(res, e);
        }
    }


    public JsonObject getGeoTimeseriesData(MetricsGeoQuery query) throws Exception {
        List<DataStreamGeo<Metric>> dataStreams = query.execute();

        ResponseBuilder response = new ResponseBuilder(true)
                                           .setResponse(jsonStringSerialiser.getDefaultSerialiser()
                                                                            .toJsonTree(dataStreams)
                                                                            .getAsJsonArray());

        return response.build();

    }

}
