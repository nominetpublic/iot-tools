/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.controller;

import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.core.search.aggregation.DateHistogramAggregation;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.everit.json.schema.ValidationException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import spark.Request;
import spark.Response;
import spark.Service;
import uk.nominet.iot.api.datastore.errors.IllegalQueryException;
import uk.nominet.iot.api.datastore.query.DatesQuery;
import uk.nominet.iot.api.datastore.utils.ElasticSearchUtils;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.http.APIController;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.api.APIException;
import uk.nominet.iot.model.api.ExceptionHandler;
import uk.nominet.iot.model.api.ResponseBuilder;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static uk.nominet.iot.utils.ConversionUtils.asciiToHex;

public class DatesController implements APIController {
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();



    @Override
    public void publishAPI(Service http, String path) {
        http.path(path, () -> http.post("/dates", this::dates));
    }

    private String dates(Request req, Response res) {
        JSONObject queryObject;
        try {
            queryObject = new JSONObject(new JSONTokener(req.body()));
        } catch (JSONException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

        try {
            DatesQuery query = new DatesQuery(queryObject, req.attribute("introspected_username"));
            query.setUser(req.attribute("introspected_username"));

            return getTimeseriesDates(query);

        } catch (ValidationException e) {
            return ExceptionHandler.handle(res, new IllegalQueryException(e.getKeyword(), queryObject.toString(),
                    e.getErrorMessage()), e.getAllMessages().toString());
        } catch (Exception e) {
            e.printStackTrace();
            return ExceptionHandler.handle(res, e);
        }
    }

    private String getTimeseriesDates(DatesQuery query) throws APIException {
        ResponseBuilder response;
        String index;
        try {
            index = ElasticSearchUtils.INDEX_PREFIX + asciiToHex(query.getUser());
        } catch (Exception e) {
            throw new APIException(400, "Could not identify elasticsearch index", e);
        }

        SearchSourceBuilder searchBuilder = new SearchSourceBuilder()
                .query(query.getQueryBuilder())
                .aggregation(query.getAggregationBuilder())
                .sort(query.getSortName(), query.getSortOrder())
                .size(0);

        Search search = new Search.Builder(searchBuilder.toString())
                .addIndex(index)
                .addType(ElasticSearchUtils.METRIC_TYPE)
                .build();

        SearchResult result;
        try {
            result = ElasticsearchDriver.getClient().execute(search);
            if (result.isSucceeded()) {

                DateHistogramAggregation agg = result.getAggregations().getDateHistogramAggregation("dates");
                List<String> dates = agg.getBuckets().stream().map(DateHistogramAggregation.DateHistogram::getTimeAsString).collect(Collectors.toList());


                response = new ResponseBuilder(true)
                        .addElement("dates", jsonStringSerialiser.getDefaultSerialiser()
                                .toJsonTree(dates));

            } else {
                response = new ResponseBuilder(false).setReason(result.getErrorMessage());
            }

        } catch (IOException e) {
            if (e.getCause() != null) {
                throw new APIException(400, "Could not send request to elasticsearch: " + e.getMessage(), e);
            } else {
                throw new APIException(400, "Could not send request to elasticsearch: " +
                        Objects.requireNonNull(e.getCause()).getMessage(), e);
            }
        }

        return response.build().toString();
    }
}
