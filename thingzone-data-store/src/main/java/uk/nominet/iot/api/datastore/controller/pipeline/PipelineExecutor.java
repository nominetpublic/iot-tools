/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.controller.pipeline;

import com.google.gson.JsonObject;
import uk.nominet.iot.api.datastore.DatastoreApp;
import uk.nominet.iot.api.datastore.model.PipelineRegistryEntity;
import uk.nominet.iot.json.JsonStringSerialiser;

import java.util.ArrayList;
import java.util.List;

public class PipelineExecutor {
    private final List<APIPipeline> pipeline;
    private final DatastoreApp api;
    private final boolean intermediate;
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    public PipelineExecutor(DatastoreApp api, boolean intermediate) {
        pipeline = new ArrayList<>();
        this.api = api;
        this.intermediate = intermediate;
    }

    /**
     * Add api call to pipeline
     * @param pipe pipe
     */
    public void add(APIPipeline pipe) {
        pipeline.add(pipe);
    }


    /**
     * Execute pipeline
     * @return pipeline results
     */
    public JsonObject execute() throws Exception {

        if (pipeline.size() == 0) throw new Exception("Pipeline is empty");

        List<List<PipelineRegistryEntity>> inputs = new ArrayList<>();
        List<PipelineRegistryEntity> input = null;

        if (pipeline.size() > 1) {
            for (int i = 0; i < (pipeline.size() - 1); i++) {
                input = pipeline.get(i).pipe(input, api);
                inputs.add(input);
            }
        }

        JsonObject response = pipeline.get(pipeline.size() -1 ).collect(input, api);
        if (intermediate) response.add("intermediate", jsonStringSerialiser.getDefaultSerialiser().toJsonTree(inputs));
        return response;
    }

}
