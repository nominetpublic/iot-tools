/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import uk.nominet.iot.http.APIController;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import spark.Service;
import uk.nominet.iot.model.api.ResponseBuilder;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

public class StatusController implements APIController {
    private static final Logger LOG = LoggerFactory.getLogger(StatusController.class);
    private String gitHash;

    public StatusController() {
        try {
            Enumeration<URL> mfs = Thread.currentThread().getContextClassLoader().getResources("META-INF/MANIFEST.MF");
            while (mfs.hasMoreElements()) {
                URL mfUrl = mfs.nextElement();
                Manifest mf = new Manifest(mfUrl.openStream());
                Attributes.Name GIT_HASH_ATTRIBUTE_NAME = new Attributes.Name("thingzone-data-store-git-SHA-1");
                if (mf.getMainAttributes().containsKey(GIT_HASH_ATTRIBUTE_NAME)) {
                    gitHash = mf.getMainAttributes().getValue(GIT_HASH_ATTRIBUTE_NAME);
                    break;
                }
            }
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
            e.printStackTrace();
        }
        if (gitHash == null) {
            LOG.error("Could not extract git hash from MANIFEST.MF file(s)");
        }
    }


    private String health(Request req, Response res) {
        res.type("application/json");
        JsonObject datastore = new JsonObject();
        if (gitHash != null) {
            datastore.addProperty("version-hash", gitHash);
        }


        String esURL = String.format("%s/_cluster/health", "");
        HttpClient elasticsearchClient = HttpClientBuilder.create().build();
        HttpGet getElasticsearchStatus = new HttpGet(esURL);

        HttpResponse rawResponse;
        JsonObject clusterContext = null;

        try {
            rawResponse = elasticsearchClient.execute(getElasticsearchStatus);
            if (rawResponse != null) {
                String rawJson = IOUtils.toString(rawResponse.getEntity().getContent(), StandardCharsets.UTF_8);
                clusterContext = new Gson().fromJson(rawJson, JsonObject.class);
            }
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
            e.printStackTrace();
        }

        ResponseBuilder response = new ResponseBuilder(gitHash != null && clusterContext != null)
                .addElement("datastore", datastore) // may be null, but that's fine
                .addElement("elasticsearch", clusterContext); // as above

        if (gitHash == null || clusterContext == null) {
            response.setReason(String.format("Could not obtain %s",
                    (gitHash == null && clusterContext == null) ? "version hash or ES cluster status" :
                            ((gitHash == null) ? "version hash" : "ES cluster status")));
        }

        return response.build().toString();
    }

    @Override
    public void publishAPI(Service http, String path) {
        http.path(path, () -> http.get("/health", this::health));
    }


}
