/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.geo;

import com.vividsolutions.jts.geom.*;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.spherical.twod.S2Point;
import org.elasticsearch.common.geo.GeoPoint;
import org.json.JSONArray;
import uk.nominet.iot.api.datastore.errors.IllegalQueryException;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.math3.util.FastMath.*;

// Import commonly used math functions for brevity's sake

/**
 * Parts of this code are adapted from the MIT-licensed code made available from:
 * https://www.movable-type.co.uk/scripts/latlong.html
 *
 * Copyright 2017 Chris Veness
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
public class GeoUtils {

    public static final GeometryFactory gFactory = new GeometryFactory();

    private static final Double EPSILON = 1e-10;
    // WGS84 constants
    private static final double WGS_84_A = 6378137.0;
    private static final double WGS_84_A_SQ = WGS_84_A * WGS_84_A;
    private static final double WGS_84_F = 1.0 / 298.257223563;
    private static final double WGS_84_B = WGS_84_A * (1.0 - WGS_84_F);
    private static final double WGS_84_B_SQ = WGS_84_B * WGS_84_B;
    public static double WGS_ECC_SQ = 1.0 - (WGS_84_B_SQ / WGS_84_A_SQ);
    // IUGG constants
    public static final double EARTH_MEAN_RADIUS_METRES = 6371008.8;

    private static double sin2(double input) {
        return sin(input) * sin(input);
    }

    private static double cos2(double input) {
        return cos(input) * cos(input);
    }

    private GeoUtils() {}


    /**
     * Convert elasticsearch style polygons to geotools multi polygon
     * @param polygons elasticsearch style polygon
     * @return geotools multipolygon
     */
    public static MultiPolygon getMultiPolygon(List<List<GeoPoint>> polygons) {

        List<Polygon> multiPolygonList = new ArrayList<>();

        for (List<GeoPoint> polygon : polygons) {

            Coordinate[] coordinates = polygon.stream()
                                              .map(coordinate -> new Coordinate(coordinate.lon(), coordinate.lat()))
                                              .toArray(Coordinate[]::new);

            Polygon geoPolygon = gFactory.createPolygon(coordinates);
            multiPolygonList.add(geoPolygon);
        }

        return gFactory.createMultiPolygon(multiPolygonList.toArray(new Polygon[0]));
    }




    // Begin MIT-licensed code blocks
    /**
     * Calculate the great circle distance between two lat,lon pairs, using the haversine formula.
     * @param radius the radius of the spheroid upon which the points are located
     * @param srcLat the source latitude (phi) in radians
     * @param srcLon the source longitude (theta) in radians
     * @param dstLat the destination latitude (phi) in radians
     * @param dstLon the destination longitude (theta) in radians
     * @return the distance between the source and destination radial points, in the same unit used for the radius
     */
    public static double haversineDistance(double radius, double srcLat, double srcLon, double dstLat, double dstLon) {
        double deltaLat = (dstLat - srcLat) / 2;
        double deltaLon = (dstLon - srcLon) / 2;
        double a = sin2(deltaLat) + (cos(srcLat) * cos(dstLat) *
                sin2(deltaLon));
        return radius * 2 * atan2(sqrt(a), sqrt(1 - a));
    }


    /**
     * Calculate the initial angular heading for travelling along a great circle from start to end
     * @param start the start lat,lon in radians
     * @param end the end lat,lon in radians
     * @return a heading from start to end in radians
     */
    public static double bearingBetweenRadians(S2Point start, S2Point end) {
        double y = sin(end.getTheta() - start.getTheta()) * cos(end.getPhi());
        double x = cos(start.getPhi()) * sin(end.getPhi()) -
                sin(start.getPhi()) * cos(end.getPhi()) * cos(end.getTheta() - start.getTheta());
        return atan2(y, x);
    }

    /**
     * Give a start point, an initial bearing, and a distance to travel, find the end point travelled to.
     * @param centroid the start point, a lat,lon in radians
     * @param bearingRadians the initial heading, in radians
     * @param distance the distance to travel, in metres
     * @return the location travelled to, a lat,lon in radians
     */
    public static S2Point findRelativePoint(S2Point centroid, double bearingRadians, double distance) {
        double relativeDistance = distance / EARTH_MEAN_RADIUS_METRES;
        double cosRelativeDistance = cos(relativeDistance);
        double sinRelativeDistance = sin(relativeDistance);
        double srcLat = centroid.getPhi();
        double srcLon = centroid.getTheta();
        double cosLat = cos(srcLat);
        double sinLat = sin(srcLat);
        double dstLat = asin(sinLat * cosRelativeDistance +
                cosLat * sinRelativeDistance * cos(bearingRadians));
        double dstLon = srcLon + atan2(sin(bearingRadians) * sinRelativeDistance * cosLat,
                cosRelativeDistance - sinLat * sin(dstLat));
        return new S2Point(dstLon, dstLat);
    }

    private static Vector3D toVector(S2Point p) {
        // right-handed vector: x -> 0°E,0°N; y -> 90°E,0°N, z -> 90°N
        return new Vector3D(cos(p.getPhi()) * cos(p.getTheta()), cos(p.getPhi()) * sin(p.getTheta()), sin(p.getPhi()));
    }

    private static S2Point toLatLonS(Vector3D v) {
        return new S2Point(atan2(v.getZ(), sqrt(v.getX()*v.getX() + v.getY()*v.getY())),
                atan2(v.getY(), v.getX()));
    }

    /**
     * Find the point that two great circles intersect. Currently works by translating the spherical coordinates into
     * 3D space, finding the intersection of those lines, then translating that point back into spherical coordinates.
     * @param s1 the start lat,lon of the first great circle arc, in radians
     * @param e1 the end lat,lon of the first great circle arc, in radians
     * @param s2 the start lat,lon of the second great circle arc, in radians
     * @param e2 the end lat,lon of the second great circle arc, in radians
     * @return a lat,lon representing the location the two great circles intersect, in radians
     */
    public static S2Point findIntersectionPoint(S2Point s1, S2Point e1, S2Point s2, S2Point e2) {
        Vector3D sv1 = toVector(s1);
        Vector3D sv2 = toVector(s2);
        Vector3D ev1 = toVector(e1);
        Vector3D ev2 = toVector(e2);
        // c1 & c2 are vectors defining great circles through start & end points; p × c gives initial bearing vector
        Vector3D c1 = sv1.crossProduct(ev1);
        Vector3D c2 = sv2.crossProduct(ev2);
        // there are two (antipodal) candidate intersection points; we have to choose which to return
        Vector3D i1 = c1.crossProduct(c2);
        Vector3D i2 = c2.crossProduct(c1);
        // select nearest intersection to mid-point of all points
        Vector3D mid = sv1.add(sv2).add(ev1).add(ev2);
        return toLatLonS(mid.dotProduct(i1) > 0 ? i1 : i2);
    }
    // End MIT-licensed code blocks

    /**
     * Given a list of lat,lon points in degrees, find the point that is the furthest to the top-left
     * @param points the points that form a closed polygon
     * @return the index of the most top-left point in the list, or -1 if no points are provided
     */
    private static int getTopLeftmostPointIndex(List<GeoPoint> points) {
        if(points.size() > 0) {
            int topLeftmost = 0;
            double latTL = points.get(0).lat();
            double lonTL = points.get(0).lon();
            // ignore the final point, which should be the same as the first point
            for(int i = 1; i < points.size() - 1; i++) {
                if(geq(points.get(i).lat(), latTL) && leq(points.get(i).lon(), lonTL)) {
                    topLeftmost = i;
                    latTL = points.get(i).lat();
                    lonTL = points.get(i).lon();
                }
            }
            return topLeftmost;
        }
        return -1;
    }

    /**
     * This code relies on longitudes being provided outside the usual -180 to 180 degree range when representing
     * polygons that cross over the antimeridian. By doing so, we can ensure a consistent ordering on all longitudes.
     *
     * @param points an array of (un-normalised) GeoPoints that may form a closed rectangle
     * @return an array of top-left and bottom-right corners as GeoPoints of the rectangle, if the polygon is a closed
     * rectangle, otherwise null
     */
    public static GeoPoint[] getTopLeftBottomRightRectangle(List<GeoPoint> points) {
        // a rectangle must consist of 5 points (includes a closing point)
        if(points.size() != 5) {
            return null;
        }
        // the closing point must be equal to the starting point
        if(!points.get(0).equals(points.get(4))) {
            return null;
        }
        // getTopLeftmostPointIndex cannot return -1, because of size check above, so we don't need to check for it
        int tlIndex = getTopLeftmostPointIndex(points);
        GeoPoint tl = points.get(tlIndex);
        // test for a clockwise ordered points that form a rectangle
        // floorMod because Java doesn't implement modulo properly (% is remainder!), -1 because of the final element
        GeoPoint tr = points.get(floorMod(tlIndex + 1, points.size() - 1));
        GeoPoint br = points.get(floorMod(tlIndex + 2, points.size() - 1));
        GeoPoint bl = points.get(floorMod(tlIndex + 3, points.size() - 1));
        if(tr.lat() == tl.lat() && br.lon() == tr.lon() && bl.lat() == br.lat() && bl.lon() == tl.lon()) {
            return new GeoPoint[] {tl, br};
        } else {
            // if it failed, try an anti-clockwise ordering instead
            tr = points.get(floorMod(tlIndex - 1, points.size() - 1));
            br = points.get(floorMod(tlIndex - 2, points.size() - 1));
            bl = points.get(floorMod(tlIndex - 3, points.size() - 1));
            if(tr.lat() == tl.lat() && br.lon() == tr.lon() && bl.lat() == br.lat() && bl.lon() == tl.lon()) {
                return new GeoPoint[] {tl, br};
            }
        }
        return null;
    }

    private static boolean geq(double a, double b) {
        return (a - b) > -EPSILON; // note: negative epsilon
    }

    private static boolean leq(double a, double b) {
        return (a - b) < EPSILON;
    }

    /**
     * Given a list of lists of lat,lon points in degrees, find the smallest rectangle that encloses all points
     * @param polygons a list of list of lat,lon points
     * @return a list of 4 numbers, representing top left (lon,lat), bottom right (lon, lat)
     */
    public static List<Double> getOuterBoundingBox(List<List<GeoPoint>> polygons) {
        List<Double> boundingBox = new ArrayList<>(4);

        double minLat = Double.POSITIVE_INFINITY;
        double minLon = Double.POSITIVE_INFINITY;
        double maxLat = Double.NEGATIVE_INFINITY;
        double maxLon = Double.NEGATIVE_INFINITY;
        for(List<GeoPoint> polygon: polygons) {
            for(GeoPoint point : polygon) {
                minLat = min(minLat, point.lat());
                minLon = min(minLon, point.lon());
                maxLat = max(maxLat, point.lat());
                maxLon = max(maxLon, point.lon());
            }
        }
        boundingBox.add(minLon);
        boundingBox.add(minLat);
        boundingBox.add(maxLon);
        boundingBox.add(maxLat);
        return boundingBox;
    }

    /**
     * Given a length in metres, calculate the closest geohash precision (i.e. the length of the geohash prefix that
     * is constant).
     * @param meters a length in metres
     * @return an integer between 1 and 12, where 1 is the least precise precision, and 12 is the most precise
     */
    public static int distanceToGeohashPrecision(double meters) {
        // convert distance into geohash precision size using the approximate length of the corresponding diagonals
        if(meters < 0.042) {
            return 12;
        } else if(meters < 0.21) {
            return 11;
        } else if(meters < 1.34) {
            return 10;
        } else if(meters < 6.79) {
            return 9;
        } else if(meters < 42.66) {
            return 8;
        } else if(meters < 215.88) {
            return 7;
        } else if(meters < 1345.87) {
            return 6;
        } else if(meters < 6929.65) {
            return 5;
        } else if(meters < 43692.79) {
            return 4;
        } else if(meters < 220971.15) {
            return 3;
        } else if(meters < 1399019.378) {
            return 2;
        }
        return 1;
    }


    public static List<List<GeoPoint>> parsePolygons(JSONArray polygons) {
        if(polygons == null) {
            return null;
        }
        ArrayList<List<GeoPoint>> polygonList = new ArrayList<>(polygons.length());
        for(int i = 0; i < polygons.length(); i++) {
            JSONArray points = polygons.getJSONArray(i);
            if(points.length() < 4) {
                throw new IllegalQueryException("boundingPolygons", polygons.toString(), "each polygon must consist " +
                                                                                         "of at least four points");
            }
            ArrayList<GeoPoint> polygon = new ArrayList<>(points.length());
            for (int j = 0; j < points.length(); j++) {
                JSONArray coord = points.getJSONArray(j);
                // Important: does not use conversion utils, to avoid normalising the longitude to the range [-180,180)
                // (as this is necessary information when the points represent a bounding box that crosses the
                // anti-meridian)
                polygon.add(new GeoPoint(coord.getDouble(1), coord.getDouble(0)));
            }
            if (!polygon.get(0).equals(polygon.get(polygon.size() - 1))) {
                throw new IllegalQueryException("boundingPolygons", polygons.toString(), "the coordinates for each " +
                                                                                         "bounding polygon must form a closed polygon");
            }
            polygonList.add(polygon);
        }
        return polygonList;
    }
}
