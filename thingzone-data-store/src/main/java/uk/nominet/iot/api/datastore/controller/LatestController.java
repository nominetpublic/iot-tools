/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.controller;

import com.google.gson.JsonObject;
import org.everit.json.schema.ValidationException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import spark.Request;
import spark.Response;
import spark.Service;
import uk.nominet.iot.api.datastore.errors.IllegalQueryException;
import uk.nominet.iot.api.datastore.query.DeviceLatestQuery;
import uk.nominet.iot.http.APIController;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.manager.ThingzoneEventManager;
import uk.nominet.iot.model.api.ExceptionHandler;
import uk.nominet.iot.model.api.ResponseBuilder;
import uk.nominet.iot.model.geo.DeviceFeatureCollection;


public class LatestController implements APIController {
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
    private static ThingzoneEventManager eventManager = ThingzoneEventManager.createManager();


    @Override
    public void publishAPI(Service http, String path) {
        http.path(path, () -> {
            http.post("/geo", this::geo);
            http.post("/list", this::list);
        });
    }


    /**
     * Handle list events request
     *
     * @param req http request
     * @param res http response
     * @return response body
     */
    private String list(Request req, Response res) {
        JSONObject queryObject = null;

        try {
            queryObject = new JSONObject(new JSONTokener(req.body()));
            DeviceLatestQuery query = new DeviceLatestQuery(queryObject, req.attribute("introspected_username"));
            DeviceFeatureCollection devices = query.execute();

            JsonObject responseObject = new JsonObject();
            responseObject.addProperty("total", devices.getTotal());
            responseObject.add("devices",
                    jsonStringSerialiser.getDefaultSerialiser().toJsonTree(devices.toDeviceList()));

            ResponseBuilder response = new ResponseBuilder(true)
                    .setResponse(responseObject);

            return response.build().toString();
        } catch (ValidationException e) {
            assert queryObject != null;
            return ExceptionHandler.handle(res, new IllegalQueryException(e.getKeyword(), queryObject.toString(),
                    e.getErrorMessage()), e.getAllMessages().toString());
        } catch (JSONException | IllegalQueryException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return ExceptionHandler.handle(res, e, "Unknown server error");
        }
    }


    /**
     * Handle geo events request
     *
     * @param req http request
     * @param res http response
     * @return response body
     */
    private String geo(Request req, Response res) {
        JSONObject queryObject = null;

        try {
            queryObject = new JSONObject(new JSONTokener(req.body()));
            DeviceLatestQuery query = new DeviceLatestQuery(queryObject, req.attribute("introspected_username"));
            DeviceFeatureCollection devices = query.execute();


            JsonObject responseObject = new JsonObject();
            responseObject.add("geojson", jsonStringSerialiser.getDefaultSerialiser().toJsonTree(devices));

            ResponseBuilder response = new ResponseBuilder(true)
                    .setResponse(responseObject);

            return response.build().toString();
        } catch (ValidationException e) {
            assert queryObject != null;
            return ExceptionHandler.handle(res, new IllegalQueryException(e.getKeyword(), queryObject.toString(),
                    e.getErrorMessage()), e.getAllMessages().toString());
        } catch (JSONException | IllegalQueryException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return ExceptionHandler.handle(res, e, "Unknown server error");
        }
    }


}
