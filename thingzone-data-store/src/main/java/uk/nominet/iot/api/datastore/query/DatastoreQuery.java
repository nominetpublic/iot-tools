/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.query;

import org.everit.json.schema.Schema;
import org.json.JSONObject;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.utils.SchemaUtils;

public abstract class DatastoreQuery<T> {
    static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
    private String user;
    private Schema schema;
    private final JSONObject requestBody;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    DatastoreQuery(JSONObject requestBody, String user) throws Exception {
        this.requestBody = requestBody;
        this.user = user;
        schema = SchemaUtils.loadSchemaFromResource(this.getClass().getSimpleName());
    }

    Schema getSchema() {
        return schema;
    }

    public JSONObject getRequestBody() {
        return requestBody;
    }

    protected abstract T execute() throws Exception;
}
