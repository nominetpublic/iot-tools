/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.query;

import com.google.common.collect.ImmutableList;
import com.google.gson.JsonObject;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.core.search.aggregation.ChildrenAggregation;
import io.searchbox.core.search.aggregation.TermsAggregation;
import io.searchbox.core.search.aggregation.TopHitsAggregation;
import org.apache.commons.beanutils.BeanUtils;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.index.query.*;
import org.elasticsearch.join.aggregations.ChildrenAggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filter.FilterAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.tophits.TopHitsAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.json.JSONArray;
import org.json.JSONObject;
import uk.nominet.iot.api.datastore.geo.GeoUtils;
import uk.nominet.iot.api.datastore.utils.ElasticSearchUtils;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.model.api.APIException;
import uk.nominet.iot.model.geo.DeviceFeatureCollection;
import uk.nominet.iot.model.registry.RegistryKey;
import uk.nominet.iot.model.registry.cache.DeviceDocument;
import uk.nominet.iot.model.registry.cache.RegistryDataStreamWithLatestPayload;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static uk.nominet.iot.utils.ConversionUtils.asciiToHex;

public class DeviceLatestQuery extends DatastoreQuery<DeviceFeatureCollection> implements ElasticsearchBasicQuery {

    private final List<String> PAYLOAD_INDEX_NAMES = ImmutableList.of("metric", "image", "json", "config", "event");
    private List<List<GeoPoint>> boundingPolygons;
    private List<String> types;
    private List<String> keywords;
    private int from;
    private int size;
    private String time;
    private String streamName;


    public DeviceLatestQuery(JSONObject requestBody, String user) throws Exception {
        super(requestBody, user);
        getSchema().validate(requestBody);
        setUser(user);

        if (requestBody.has("boundingPolygons")) {
            JSONArray polygons = requestBody.optJSONArray("boundingPolygons");
            List<List<GeoPoint>> parsedPolygons = GeoUtils.parsePolygons(polygons);
            if (parsedPolygons != null) {
                boundingPolygons = parsedPolygons;
            }
        }

        if (requestBody.has("time"))
            time = requestBody.getString("time");

        if (requestBody.has("keywords")) {
            ArrayList<String> keywords = new ArrayList<>();
            requestBody.getJSONArray("keywords").toList().forEach(item -> keywords.add(item.toString()));

            if (keywords.size() > 0)
                this.keywords = keywords;
        }

        if (requestBody.has("types")) {
            ArrayList<String> types = new ArrayList<>();
            requestBody.getJSONArray("types").toList().forEach(type -> types.add(type.toString()));

            if (types.size() > 0)
                this.types = types;
        }

        if (requestBody.has("from")) {
            from = requestBody.getInt("from");
        }

        if (requestBody.has("size")) {
            size = requestBody.getInt("size");
        } else {
            size = 10000;
        }

        if (requestBody.has("streamName")) {
            streamName = requestBody.getString("streamName");
        }

    }

    public List<List<GeoPoint>> getBoundingPolygons() {
        return boundingPolygons;
    }

    public void setBoundingPolygons(List<List<GeoPoint>> boundingPolygons) {
        this.boundingPolygons = boundingPolygons;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getStreamName() {
        return streamName;
    }

    public void setStreamName(String streamName) {
        this.streamName = streamName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "LatestGeoQuery{" +
                "boundingPolygons=" + boundingPolygons +
                ", types=" + types +
                ", keywords=" + keywords +
                '}';
    }

    @Override
    public DeviceFeatureCollection execute() throws Exception {
        String index;
        try {
            index = ElasticSearchUtils.INDEX_PREFIX + asciiToHex(getUser());
        } catch (Exception e) {
            throw new APIException(400, "Could not identify elasticsearch index", e);
        }


        List<DeviceDocument> devices;

        SearchSourceBuilder searchBuilder = new SearchSourceBuilder()
                .query(getQueryBuilder())
                .sort("name.keyword", SortOrder.ASC)
                .from(from)
                .size(size);

        Search devicesSearch = new Search.Builder(searchBuilder.toString())
                .addIndex(index)
                .addType(ElasticSearchUtils.DEVICE_TYPE)
                .build();

        SearchResult devicesSearchResults;
        try {
            devicesSearchResults = ElasticsearchDriver.getClient().execute(devicesSearch);
            if (devicesSearchResults.isSucceeded()) {
                devices = devicesSearchResults
                        .getHits(JsonObject.class)
                        .stream()
                        .map(deviceHit -> jsonStringSerialiser.readObject(deviceHit.source, DeviceDocument.class))
                        .collect(Collectors.toList());
            } else {
                throw new APIException(400, devicesSearchResults.getErrorMessage());
            }
        } catch (IOException e) {
            throw new APIException(400, "Could not send request to elasticsearch", e);
        }


        if (streamName == null) {
            return DeviceFeatureCollection.build(devices, null, devicesSearchResults.getTotal());
        }


        String[] deviceKeys = devices.stream().map(RegistryKey::getKey).toArray(String[]::new);


        String[] includeFields = {"name", "type", "key", "metadata", "devices"};

        SearchSourceBuilder streamsSearchBuilder = new SearchSourceBuilder()
                .query(getStreamQueryBuilder(deviceKeys))
                .fetchSource(includeFields, null)
                .from(0)
                .size(10000);

        for (AggregationBuilder aggregationBuilder : getStreamAggregationBuilder()) {
            streamsSearchBuilder.aggregation(aggregationBuilder);
        }

        Search streamsSearch = new Search.Builder(streamsSearchBuilder.toString())
                .addIndex(index)
                .addType(ElasticSearchUtils.STREAM_TYPE)
                .build();


        SearchResult streamSearchResults;
        try {


            streamSearchResults = ElasticsearchDriver.getClient().execute(streamsSearch);
            if (streamSearchResults.isSucceeded()) {
                List<RegistryDataStreamWithLatestPayload> streamsWithPayload =
                        streamSearchResults.getHits(JsonObject.class)
                                .stream().map(hit -> jsonStringSerialiser.readObject(
                                hit.source, RegistryDataStreamWithLatestPayload.class))
                                .collect(Collectors.toList());


                for (String indexName : PAYLOAD_INDEX_NAMES) {

                    ChildrenAggregation timeseriesAggregation =
                            streamSearchResults.getAggregations().getChildrenAggregation(indexName);
                    TermsAggregation streamsAggregation = timeseriesAggregation.getTermsAggregation("stream");

                    for (TermsAggregation.Entry streamBucket : streamsAggregation.getBuckets()) {
                        String streamKey = streamBucket.getKey();

                        TopHitsAggregation payloadsAggregation = streamBucket
                                .getFilterAggregation("time")
                                .getTopHitsAggregation("payloads");
                        JsonObject latestPayload = payloadsAggregation.getHits(JsonObject.class)
                                .stream()
                                .map(hit -> hit.source)
                                .findFirst()
                                .orElse(null);
                        if (latestPayload != null) {
                            for (int i = 0; i < streamsWithPayload.size(); i++) {
                                RegistryDataStreamWithLatestPayload stream = streamsWithPayload.get(i);
                                if (stream.getKey().equals(streamKey)) {
                                    RegistryDataStreamWithLatestPayload clonedStream =
                                            (RegistryDataStreamWithLatestPayload) BeanUtils.cloneBean(stream);
                                    clonedStream.setLatestPoint(latestPayload);
                                    streamsWithPayload.set(i, clonedStream);
                                    break;
                                }
                            }
                        }
                    }

                }

                return DeviceFeatureCollection.build(devices, streamsWithPayload, devicesSearchResults.getTotal());
            } else {
                throw new APIException(400, devicesSearchResults.getErrorMessage());
            }
        } catch (IOException e) {
            throw new APIException(400, "Could not send request to elasticsearch", e);
        }

    }


    @Override
    public QueryBuilder getQueryBuilder() {
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.must(new MatchAllQueryBuilder());

        // Keyword query
        if (keywords != null) {
            BoolQueryBuilder keywordBoolQueryBuilder = new BoolQueryBuilder();

            for (String term : keywords) {
                MultiMatchQueryBuilder multiMatchQueryBuilder =
                        new MultiMatchQueryBuilder(term, "name", "type", "category");
                keywordBoolQueryBuilder.should(multiMatchQueryBuilder);
            }

            boolQueryBuilder.filter(keywordBoolQueryBuilder);

        }

        // Types query
        if (types != null) {
            BoolQueryBuilder typeBoolQueryBuilder = new BoolQueryBuilder();

            for (String type : types) {
                MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("type.keyword", type);
                typeBoolQueryBuilder.should(matchQueryBuilder);
            }

            boolQueryBuilder.filter(typeBoolQueryBuilder);
        }


        if (boundingPolygons != null && getBoundingPolygons().size() > 0) {
            if (getBoundingPolygons().size() > 1) {
                BoolQueryBuilder polygonsQuery = QueryBuilders.boolQuery();
                for (List<GeoPoint> polygon : getBoundingPolygons()) {
                    polygonsQuery.should(ElasticSearchUtils.buildGeoQuery(polygon));
                }
                polygonsQuery.minimumShouldMatch(1);
                boolQueryBuilder.filter(polygonsQuery);
            } else {
                boolQueryBuilder.filter(ElasticSearchUtils.buildGeoQuery(getBoundingPolygons().get(0)));
            }
        }


        return boolQueryBuilder;
    }


    public QueryBuilder getStreamQueryBuilder(String[] devices) {
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.must(new MatchAllQueryBuilder());

        TermsQueryBuilder termsQueryBuilder = new TermsQueryBuilder("devices.key.keyword", devices);
        boolQueryBuilder.must(termsQueryBuilder);

        if (streamName != null) {
            TermQueryBuilder termQueryBuilder = new TermQueryBuilder("name.keyword", streamName);
            boolQueryBuilder.must(termQueryBuilder);
        }


//        if (time != null) {
//            RangeQueryBuilder rangeQueryBuilder = new RangeQueryBuilder("timestamp");
//            rangeQueryBuilder.lte(time);
//            boolQueryBuilder.must(rangeQueryBuilder);
//        }

        return boolQueryBuilder;
    }

    public List<AggregationBuilder> getStreamAggregationBuilder() {

        List<AggregationBuilder> aggregationList = new ArrayList<>();

        for (String childType : PAYLOAD_INDEX_NAMES) {
            ChildrenAggregationBuilder childrenAggregation = new ChildrenAggregationBuilder(childType, childType);

            TermsAggregationBuilder termsAggregationBuilder = AggregationBuilders.terms("stream")
                    .field("streamKey.keyword")
                    .size(10000);

            RangeQueryBuilder rangeQueryBuilder = new RangeQueryBuilder("timestamp");
            if (time != null) {
                rangeQueryBuilder.lte(time);
            } else {
                rangeQueryBuilder.lte("now");
            }

            FilterAggregationBuilder filterAggregationBuilder = AggregationBuilders.filter("time", rangeQueryBuilder);


            TopHitsAggregationBuilder topHitsAggregationBuilder = AggregationBuilders.topHits("payloads")
                    .size(1)
                    .sort("timestamp", SortOrder.DESC);
//            termsAggregationBuilder.subAggregation(topHitsAggregationBuilder);
//            childrenAggregation.subAggregation(termsAggregationBuilder);
//            aggregationList.add(childrenAggregation);

            filterAggregationBuilder.subAggregation(topHitsAggregationBuilder);
            termsAggregationBuilder.subAggregation(filterAggregationBuilder);
            childrenAggregation.subAggregation(termsAggregationBuilder);
            aggregationList.add(childrenAggregation);

        }

        return aggregationList;

    }


    @Override
    public AggregationBuilder getAggregationBuilder() {
        return null;
    }

    @Override
    public String getSortName() {
        return null;
    }

    @Override
    public SortOrder getSortOrder() {
        return null;
    }
}
