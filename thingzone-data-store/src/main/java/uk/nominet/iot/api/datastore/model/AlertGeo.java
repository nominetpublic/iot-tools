/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.model;

import uk.nominet.iot.model.timeseries.Event;

import java.util.ArrayList;
import java.util.List;

class AlertGeo<T> {
    private List<Event> alerts;

    public AlertGeo() {
        this.alerts = new ArrayList<>();
    }

    public List<Event> getAlerts() {
        return alerts;
    }

    public void setAlerts(List<Event> alerts) {
        this.alerts = alerts;
    }
}
