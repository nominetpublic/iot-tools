/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.utils;

import io.searchbox.core.search.aggregation.ExtendedStatsAggregation;
import uk.nominet.iot.model.timeseries.Stats;

import java.util.Arrays;
import java.util.List;

public class AggregationUtils {

    private static final String[] defaultFields =
            {"count", "min", "max", "avg","sum","sum_of_squares","variance","std_deviation"};

    public static Stats toStats(ExtendedStatsAggregation statsAggregation, String[] fields) {
        Stats stats = new Stats();

        List<String> requiredFields = (fields == null) ? Arrays.asList(defaultFields) : Arrays.asList(fields);

        if (requiredFields.contains("count")) stats.setCount(statsAggregation.getCount());
        if (requiredFields.contains("min")) stats.setMin(statsAggregation.getMin());
        if (requiredFields.contains("max")) stats.setMax(statsAggregation.getMax());
        if (requiredFields.contains("avg")) stats.setAvg(statsAggregation.getAvg());
        if (requiredFields.contains("sum")) stats.setSum(statsAggregation.getSum());
        if (requiredFields.contains("sum_of_squares")) stats.setSum_of_squares(statsAggregation.getSumOfSquares());
        if (requiredFields.contains("variance")) stats.setVariance(statsAggregation.getVariance());
        if (requiredFields.contains("std_deviation")) stats.setStd_deviation(statsAggregation.getStdDeviation());

        return stats;
    }

}
