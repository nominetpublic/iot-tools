/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.controller.pipeline;

import com.google.gson.JsonObject;
import uk.nominet.iot.api.datastore.DatastoreApp;
import uk.nominet.iot.api.datastore.model.PipelineRegistryEntity;
import uk.nominet.iot.api.datastore.query.EventListQuery;
import uk.nominet.iot.model.registry.RegistryEntity;

import java.util.ArrayList;
import java.util.List;

public class TimeseriesAlertListPipe implements APIPipeline {
    private EventListQuery query;

    public TimeseriesAlertListPipe(EventListQuery query) throws Exception {
        if (query == null)
            throw new Exception("Missing image aggregation query");
        this.query = query;
    }


    @Override
    public List<PipelineRegistryEntity> pipe(List<PipelineRegistryEntity> input, DatastoreApp api) throws Exception {
        throw new Exception("Alert List aggregation query can not be piped to another query");
    }

    @Override
    public List<PipelineRegistryEntity> pipe(DatastoreApp api) throws Exception {
        return pipe(null, api);
    }

    @Override
    public JsonObject collect(List<PipelineRegistryEntity> input, DatastoreApp api) throws Exception {
        if (api == null)
            throw new Exception("Datastore api not provided");

        if (input == null || input.size() == 0)
            throw new Exception("No data streams matching filter");

        List<String> devices = new ArrayList<>();

        for (RegistryEntity entity : input) {
            devices.add(entity.getKey());
        }


        query.setDevices(devices);

        return api.eventController.listEvents(query);
    }

    @Override
    public JsonObject collect(DatastoreApp api) throws Exception {
        return collect(null, api);
    }
}
