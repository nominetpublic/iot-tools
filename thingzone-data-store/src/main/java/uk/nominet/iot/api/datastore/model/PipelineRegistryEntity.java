/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.model;

import uk.nominet.iot.model.registry.RegistryEntity;

import java.util.List;

public class PipelineRegistryEntity extends RegistryEntity {
    private static final long serialVersionUID = -6630815570263248871L;
    private List<RegistryEntity> secondaryStreams;

    public List<RegistryEntity> getSecondaryStreams() {
        return secondaryStreams;
    }

    public void setSecondaryStreams(List<RegistryEntity> secondaryStreams) {
        this.secondaryStreams = secondaryStreams;
    }
}
