/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.utils;

import com.google.gson.JsonObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedHashMap;

/* example set of utils for simple potential fleet data */
public class FleetStatisticalUtils {

    private final LinkedHashMap<String, VehicleStatisticalUtils.StatParameters> vehicleStats = new LinkedHashMap<>();

    public void setVehicleStats(String vehicleId, VehicleStatisticalUtils.StatParameters vehicleStats) {
        this.vehicleStats.put(vehicleId, vehicleStats);
    }

    public JsonObject JsonFleetStat() {
        JsonObject stats = new JsonObject();
        long totalTimeAutonomy = 0;
        long totalTimeParking = 0;
        long totalTime = 0;

        double totalDistanceAutonomy = 0;
        double totalDistance = 0;
        //double totalVelocityAutonomy = 0;
        //double totalVelocityManual = 0;
        //double totalVelocity = 0;

        for (VehicleStatisticalUtils.StatParameters vehicleValue : vehicleStats.values()) {
            VehicleStatisticalUtils.DrivingTimeData drivenTime = vehicleValue.getTimeData();
            VehicleStatisticalUtils.DistanceAndVelocityData
                    drivenDistanceVelocity = vehicleValue.getDistanceAndVelocityData();

            totalTimeAutonomy += drivenTime.getTimeAutonomy();
            totalTimeParking += drivenTime.getTimeParking();
            totalTime += drivenTime.getTimeTotal();

            totalDistanceAutonomy += drivenDistanceVelocity.getDistanceAutonomy();
            totalDistance += drivenDistanceVelocity.getDistanceTotal();

            //totalVelocityAutonomy += drivenDistanceVelocity.getVelocityAutonomy();
            //totalVelocityManual += drivenDistanceVelocity.getVelocityManual();
            //totalVelocity += drivenDistanceVelocity.getVelocityTotal();

        }

        stats.addProperty("FleetAutonomyTimeMillisecond", totalTimeAutonomy);
        stats.addProperty("FleetTimeParkingMillisecond", totalTimeParking);
        stats.addProperty("FleetTimeMillisecond", totalTime);

        stats.addProperty("FleetAutonomyDistanceMeter", BigDecimal.valueOf(totalDistanceAutonomy)
                .setScale(2, RoundingMode.HALF_UP));
        stats.addProperty("FleetTotalDistance", BigDecimal.valueOf(totalDistance).
                setScale(2, RoundingMode.HALF_UP));

        // average velocity doesnt mean anything --- should find better way to represent velocity
        //stats.addProperty("totalAutonomyVelocity", totalVelocityAutonomy);
        //stats.addProperty("totalVelocityManual", totalVelocityManual);
        //stats.addProperty("totalVelocity", totalVelocity);

        return stats;
    }

}
