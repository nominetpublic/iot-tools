/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.query;

import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import org.elasticsearch.index.query.*;
import org.elasticsearch.join.query.HasParentQueryBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.json.JSONObject;
import uk.nominet.iot.api.datastore.model.DataStreamValues;
import uk.nominet.iot.api.datastore.utils.ArrayUtils;
import uk.nominet.iot.api.datastore.utils.ElasticSearchUtils;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.model.api.APIException;
import uk.nominet.iot.model.timeseries.Metric;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static uk.nominet.iot.utils.ConversionUtils.asciiToHex;

public class MetricsListQuery extends DatastoreListQuery<Metric, List<DataStreamValues<Metric>>> implements ElasticsearchBasicQuery {

    public MetricsListQuery(JSONObject requestBody, String user) throws Exception {
        super(requestBody, user);
        getSchema().validate(requestBody);
        setUser(user);
        if (requestBody.has("streams"))
            setStreams(ArrayUtils.toStringArray(requestBody.getJSONArray("streams")));

        if (requestBody.has("time"))
            setTime(requestBody.getString("time"));

        if (requestBody.has("timeLimit"))
            setTimeLimit(requestBody.getString("timeLimit"));

        if (requestBody.has("forward"))
            setForward(requestBody.getBoolean("forward"));
        else
            setForward(false);

        if (requestBody.has("size"))
            setSize(requestBody.getInt("size"));
    }


    @Override
    public String toString() {
        return "MetricsListQuery{" +
               "streams=" + Arrays.toString(streams) +
               ", time='" + time + '\'' +
               ", timeLimit='" + timeLimit + '\'' +
               ", forward=" + forward +
               ", size=" + size +
               '}';
    }

    @Override
    public QueryBuilder getQueryBuilder() {
        return getQueryBuilder(streams[0]);
    }

    private QueryBuilder getQueryBuilder(String streamKey) {
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();

        TermQueryBuilder termsQueryBuilder = new TermQueryBuilder("_id", streamKey);
        HasParentQueryBuilder parentQueryBuilder =
                new HasParentQueryBuilder("stream", termsQueryBuilder, false);

        boolQueryBuilder.must(parentQueryBuilder);

        RangeQueryBuilder rangeQueryBuilder = ElasticSearchUtils.getRangeBuilder(time, timeLimit, forward);
        if (rangeQueryBuilder != null)  boolQueryBuilder.must(rangeQueryBuilder);

        return boolQueryBuilder;
    }


    @Override
    public AggregationBuilder getAggregationBuilder() {
        return null;
    }

    @Override
    public String getSortName() {
        return "timestamp";
    }

    @Override
    public SortOrder getSortOrder() {
        if (forward)
            return SortOrder.ASC;
        else
            return SortOrder.DESC;
    }


    @Override
    public List<DataStreamValues<Metric>> execute() throws Exception {
        List<DataStreamValues<Metric>> results = new ArrayList<>();

        String index;
        try {
            index = ElasticSearchUtils.INDEX_PREFIX + asciiToHex(getUser());
        } catch (Exception e) {
            throw new APIException(400, "Could not identify elasticsearch index", e);
        }


        for (String stream : getStreams()) {

            JsonObject streamInfo;
            try {
                streamInfo = ElasticSearchUtils.getStreamInfo(index, stream);
            } catch (Exception e) {
                throw new APIException(400, "Could not get stream info", e);
            }

            try {
                DataStreamValues<Metric> dataStreamValues = new DataStreamValues<>();
                dataStreamValues.setStreamKey(streamInfo.get("key").getAsString());

                if (streamInfo.has("name"))
                    dataStreamValues.setName(streamInfo.get("name").getAsString());

                if (streamInfo.has("type"))
                    dataStreamValues.setType(streamInfo.get("type").getAsString());

                if (streamInfo.has("metadata")) {
                    Map<String, Object> metadata =
                            jsonStringSerialiser
                                    .getDefaultSerialiser()
                                    .fromJson(streamInfo.get("metadata").getAsJsonObject(),
                                              new TypeToken<Map<String, Object>>(){}.getType());
                    dataStreamValues.setMetadata(metadata);
                }

                dataStreamValues.setValues(getDataForStream(index, streamInfo));
                results.add(dataStreamValues);

            } catch (Exception e) {
                throw new APIException(400, "Could not send request to elasticsearch", e);
            }
        }

        return results;
    }


    /**
     * Get timeseries data for a specified stream
     .addIndex(index)
     * @param index elasticsearch index
     * @return List of json data points
     */
    private List<Metric> getDataForStream(String index, JsonObject streamInfo) throws Exception {
        SearchSourceBuilder searchBuilder = new SearchSourceBuilder()
                                                    .query(getQueryBuilder(streamInfo.get("key").getAsString()))
                                                    .sort(getSortName(), getSortOrder())
                                                    .size((size != null) ? size : 10000);

        Search search = new Search.Builder(searchBuilder.toString())
                                .addType(ElasticSearchUtils.METRIC_TYPE)
                                .build();

        SearchResult result;
        result = ElasticsearchDriver.getClient().execute(search);

        if (result.isSucceeded()) {
            List<Metric> metrics = new ArrayList<>();

            for (SearchResult.Hit<JsonObject, Void> hit : result.getHits(JsonObject.class)) {
                Class metricClass = Class.forName(hit.source.get("klass").getAsString());

                //noinspection unchecked
                metrics.add((Metric)jsonStringSerialiser.readObject(hit.source, metricClass));
            }

            return  metrics;
        } else {
            throw new Exception(result.getErrorMessage());
        }
    }
}
