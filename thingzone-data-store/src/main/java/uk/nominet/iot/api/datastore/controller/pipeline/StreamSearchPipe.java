/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.controller.pipeline;

import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import uk.nominet.iot.api.datastore.DatastoreApp;
import uk.nominet.iot.api.datastore.model.PipelineRegistryEntity;
import uk.nominet.iot.api.datastore.query.StreamListQuery;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.registry.RegistryKey;

import java.util.List;
import java.util.stream.Collectors;

public class StreamSearchPipe implements APIPipeline {
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
    private StreamListQuery query;
    private List<PipelineRegistryEntity> streamList;


    public StreamSearchPipe(StreamListQuery query) throws Exception {
        if (query == null) throw new Exception("Missing stream aggregation query");
        this.query = query;
    }

    public StreamSearchPipe(List<PipelineRegistryEntity> streamList) {
        this.streamList = streamList;
    }

    @Override
    public List<PipelineRegistryEntity> pipe(List<PipelineRegistryEntity> input, DatastoreApp api) throws Exception {
        if (streamList != null) return streamList;

        JsonObject streamSearchResponse = collect(input, api);

        return jsonStringSerialiser
                                         .getDefaultSerialiser()
                                         .fromJson(streamSearchResponse.getAsJsonArray("response"),
                                                   new TypeToken<List<PipelineRegistryEntity>>() {
                                                   }.getType());
    }

    @Override
    public List<PipelineRegistryEntity> pipe(DatastoreApp api) throws Exception {
        return pipe(null, api);
    }

    @Override
    public JsonObject collect(List<PipelineRegistryEntity> input, DatastoreApp api) throws Exception {
        if (api == null) throw new Exception("Datastore api not provided");
        if (input != null) {
            List<String> devices = input.stream().map(RegistryKey::getKey).collect(Collectors.toList());
            query.setDevices(devices);
        }
        return api.streamListController.getStreams(query);
    }

    @Override
    public JsonObject collect(DatastoreApp api) throws Exception {
        return collect(null, api);
    }
}
