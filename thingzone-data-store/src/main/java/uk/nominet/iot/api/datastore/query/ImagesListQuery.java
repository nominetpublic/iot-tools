/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.query;

import com.google.gson.JsonObject;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.json.JSONObject;
import uk.nominet.iot.api.datastore.model.DataStreamValues;
import uk.nominet.iot.api.datastore.model.ImagesHandler;
import uk.nominet.iot.api.datastore.utils.ArrayUtils;
import uk.nominet.iot.api.datastore.utils.ElasticSearchUtils;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.model.api.APIException;
import uk.nominet.iot.model.timeseries.Image;
import uk.nominet.iot.model.timeseries.ImageBurst;


import java.io.IOException;
import java.util.*;

import static uk.nominet.iot.utils.ConversionUtils.asciiToHex;

public class ImagesListQuery extends DatastoreListQuery<ImageBurst, List<DataStreamValues<ImageBurst>>> implements ElasticsearchBasicQuery {
    private Map<String, Object> metadata;
    private Integer burstInterval;
    private List<String> tags;
    private List<String> ids;

    public ImagesListQuery(JSONObject requestBody, String user) throws Exception {
        super(requestBody, user);
        getSchema().validate(requestBody);

        setUser(user);

        if (requestBody.has("streams")) {
            setStreams(ArrayUtils.toStringArray(requestBody.getJSONArray("streams")));
        }

        if (requestBody.has("time"))
            setTime(requestBody.getString("time"));

        if (requestBody.has("timeLimit"))
            setTimeLimit(requestBody.getString("timeLimit"));

        if (requestBody.has("forward"))
            setForward(requestBody.getBoolean("forward"));
        else
            setForward(false);

        if (requestBody.has("size"))
            setSize(requestBody.getInt("size"));

        if (requestBody.has("burstInterval"))
            setBurstInterval(requestBody.getInt("burstInterval"));

        if (requestBody.has("metadata"))
            setMetadata(requestBody.getJSONObject("metadata").toMap());

        if (requestBody.has("tags")) {
            ArrayList<String> tags = new ArrayList<>();
            requestBody.getJSONArray("tags").toList().forEach(tag -> tags.add(tag.toString()));

            if (tags.size() > 0)
                setTags(tags);
        }

        if (requestBody.has("ids")) {
            ArrayList<String> ids = new ArrayList<>();
            requestBody.getJSONArray("ids").toList().forEach(id -> ids.add(id.toString()));

            if (ids.size() > 0)
                setIds(ids);
        }

    }


    private Integer getBurstInterval() {
        return burstInterval;
    }

    private void setBurstInterval(Integer burstInterval) {
        this.burstInterval = burstInterval;
    }

    public Map<String, Object> getMetadata() {
        return metadata;
    }

    private void setMetadata(Map<String, Object> metadata) {
        this.metadata = metadata;
    }

    public List<String> getTags() {
        return tags;
    }

    private void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<String> getIds() {
        return ids;
    }

    public void setIds(List<String> ids) {
        this.ids = ids;
    }

    @Override
    public QueryBuilder getQueryBuilder() {
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.must(new MatchAllQueryBuilder());


        BoolQueryBuilder streamsBoolQueryBuilder = new BoolQueryBuilder();

        for (String stream : streams) {
            TermQueryBuilder termQueryBuilder = new TermQueryBuilder("streamKey.keyword", stream);
            streamsBoolQueryBuilder.should(termQueryBuilder);
        }

        boolQueryBuilder.filter(streamsBoolQueryBuilder);

        RangeQueryBuilder rangeQueryBuilder = ElasticSearchUtils.getRangeBuilder(time, timeLimit, forward);
        if (rangeQueryBuilder != null)  boolQueryBuilder.must(rangeQueryBuilder);

        //Metadata filter
        if (metadata != null && !metadata.isEmpty()) {
            BoolQueryBuilder metadataQueryBuilder = new BoolQueryBuilder();
            metadata.forEach((key, value) -> {
                TermQueryBuilder termQueryBuilder =
                        new TermQueryBuilder(String.format("metadata.%s.keyword", key), value);
                metadataQueryBuilder.should(termQueryBuilder);

            });
            boolQueryBuilder.filter(metadataQueryBuilder);
        }

        // Tags query
        if (tags != null) {
            BoolQueryBuilder tagsBoolQueryBuilder = new BoolQueryBuilder();

            for (String tag : tags) {
                TermQueryBuilder termQueryBuilder = new TermQueryBuilder("tags.keyword", tag);
                tagsBoolQueryBuilder.should(termQueryBuilder);
            }

            boolQueryBuilder.filter(tagsBoolQueryBuilder);
        }


        if (ids != null) {
            BoolQueryBuilder idsBoolQueryBuilder = new BoolQueryBuilder();
            for (String id : ids) {
                TermQueryBuilder termQueryBuilder = new TermQueryBuilder("id.keyword", id);
                idsBoolQueryBuilder.should(termQueryBuilder);

            }

            boolQueryBuilder.filter(idsBoolQueryBuilder);
        }

        return boolQueryBuilder;
    }


    @Override
    public AggregationBuilder getAggregationBuilder() {
        return null;
    }


    @Override
    public String getSortName() {
        return "timestamp";
    }

    @Override
    public SortOrder getSortOrder() {
        if (forward)
            return SortOrder.ASC;
        else
            return SortOrder.DESC;
    }

    @Override
    public String toString() {
        return "ImagesListQuery{" +
               "metadata=" + metadata +
               ", burstInterval=" + burstInterval +
               ", tags=" + tags +
               ", streams=" + Arrays.toString(streams) +
               ", time='" + time + '\'' +
               ", timeLimit='" + timeLimit + '\'' +
               ", forward=" + forward +
               ", size=" + size +
               '}';
    }

    @Override
    public List<DataStreamValues<ImageBurst>> execute() throws Exception {
        String index;
        try {
            index = ElasticSearchUtils.INDEX_PREFIX + asciiToHex(getUser());
        } catch (Exception e) {
            throw new APIException(400, "Could not identify elasticsearch index", e);
        }

        SearchSourceBuilder searchBuilder = new SearchSourceBuilder()
                                                    .query(getQueryBuilder())
                                                    //.aggregation(query.getListAggregationBuilder())
                                                    .sort(getSortName(), getSortOrder())
                                                    .size((size != null) ? size : 10000);


        Search search = new Search.Builder(searchBuilder.toString())
                                .addIndex(index)
                                .addType(ElasticSearchUtils.IMAGE_TYPE)
                                .build();

        SearchResult result;
        try {
            result = ElasticsearchDriver.getClient().execute(search);
            if (result.isSucceeded()) {
                List<DataStreamValues<ImageBurst>> results = new ArrayList<>();
                DataStreamValues<ImageBurst> imageBurstDataStreamValues = new DataStreamValues<>();
                imageBurstDataStreamValues.setStreamKey(StringUtils.join(getStreams(), ","));
                imageBurstDataStreamValues.setMerged(true);

                ImagesHandler resultHandler = new ImagesHandler(getBurstInterval());

                for (SearchResult.Hit<JsonObject, Void> image : result.getHits(JsonObject.class)) {
                    Image imageObject = jsonStringSerialiser.readObject(image.source, Image.class);
                    resultHandler.put(imageObject);
                }

                imageBurstDataStreamValues.setValues(resultHandler.getItems());
                results.add(imageBurstDataStreamValues);

                return results;

            } else {
                throw new APIException(400, result.getErrorMessage());
            }

        } catch (IOException e) {
            throw new APIException(400, "Could not send request to elasticsearch", e);
        }
    }
}
