/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.geo;

import de.topobyte.osm4j.core.model.iface.OsmNode;
import uk.nominet.iot.api.datastore.geo.model.Latitude;
import uk.nominet.iot.api.datastore.geo.model.Longitude;
import org.apache.commons.math3.geometry.spherical.twod.S2Point;
import org.apache.commons.math3.util.FastMath;
import org.elasticsearch.common.geo.GeoPoint;

public class ConversionUtils {
    private ConversionUtils() {}

    public static Latitude getLatitude(GeoPoint geoPoint) {
        return Latitude.getLatitudeFromDegrees(geoPoint.lat());
    }

    public static Longitude getLongitude(GeoPoint geoPoint) {
        return Longitude.getLongitudeFromDegrees(geoPoint.lon());
    }

    public static Latitude getLatitude(S2Point point) {
        return new Latitude((FastMath.PI / 2) - point.getPhi());
    }

    public static Longitude getLongitude(S2Point point) {
        return new Longitude(point.getTheta());
    }

    private static Latitude getLatitude(OsmNode node) {
        return Latitude.getLatitudeFromDegrees(node.getLatitude());
    }

    private static Longitude getLongitude(OsmNode node) {
        return Longitude.getLongitudeFromDegrees(node.getLongitude());
    }

    public static double[] getDegreesLatLonArray(S2Point point) {
        return new double[] {getLatitude(point).getDegrees(), getLongitude(point).getDegrees()};
    }

    public static GeoPoint createGeoPoint(Latitude latitude, Longitude longitude) {
        return new GeoPoint(latitude.getDegrees(), longitude.getDegrees());
    }

    public static GeoPoint createGeoPoint(S2Point point) {
        return createGeoPoint(getLatitude(point), getLongitude(point));
    }

    public static S2Point createS2Point(Latitude latitude, Longitude longitude) {
        return new S2Point(longitude.getRadians(), (FastMath.PI / 2) - latitude.getRadians());
    }

    public static S2Point createS2Point(OsmNode node) {
        return createS2Point(getLatitude(node), getLongitude(node));
    }

    public static String toString(S2Point point) {
        return String.format("(%.3f,%.3f)", getLatitude(point).getDegrees(), getLongitude(point).getDegrees());
    }

}
