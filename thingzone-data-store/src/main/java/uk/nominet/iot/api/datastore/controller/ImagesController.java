/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.nmote.iim4j.IIM;
import com.nmote.iim4j.IIMFile;
import com.nmote.iim4j.stream.JPEGUtil;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import org.everit.json.schema.ValidationException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import spark.Request;
import spark.Response;
import spark.Service;
import uk.nominet.iot.api.datastore.model.DataStreamValues;
import uk.nominet.iot.api.datastore.query.ImagesListQuery;
import uk.nominet.iot.driver.ImagesvrDriver;
import uk.nominet.iot.http.APIController;
import uk.nominet.iot.model.api.ExceptionHandler;
import uk.nominet.iot.api.datastore.errors.IllegalQueryException;
import uk.nominet.iot.api.datastore.utils.ElasticSearchUtils;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.error.DataCachingException;
import uk.nominet.iot.error.DataStorageException;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.manager.ThingzoneTimeseriesHandler;
import uk.nominet.iot.manager.ThingzoneTimeseriesManager;
import uk.nominet.iot.model.api.ResponseBuilder;
import uk.nominet.iot.model.core.Classification;
import uk.nominet.iot.model.timeseries.Image;
import uk.nominet.iot.model.timeseries.ImageBurst;
import uk.nominet.iot.model.timeseries.Json;


import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

import static uk.nominet.iot.utils.ConversionUtils.asciiToHex;

public class ImagesController implements APIController {
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
    private static ThingzoneTimeseriesManager timeseriesManager = ThingzoneTimeseriesManager.createManager();


    @Override
    public void publishAPI(Service http, String path) {
        http.path(path, () -> {
            http.post("/list", this::list);
            http.get("/:streamKey/:imageId", this::getImage);
            http.delete("/:streamKey/:imageId", this::deleteImage);
            http.put("/:streamKey/:imageId/tag/:tag", this::addTag);
            http.delete("/:streamKey/:imageId/tag/:tag", this::removeTag);
            http.get("/tags/suggest/:prefix", this::suggestTags);
            http.put("/:streamKey/:imageId/classification", this::replaceClassification);
        });
    }


    /**
     * Handle request for get individual image
     *
     * @param req http request
     * @param res http response
     * @return response body
     */
    private Object getImage(Request req, Response res) {
        String imageId = req.params(":imageId");
        String streamKey = req.params(":streamKey");

        ThingzoneTimeseriesHandler<Image> imageHandler;

        try {
            imageHandler = timeseriesManager.timeseries(streamKey, Image.class);
        } catch (Exception e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

        try {
            Image image = imageHandler.getPoint(UUID.fromString(imageId));

            byte[] imageRaw = ImagesvrDriver.getAPIClient().download.toBytes(image.getImageRef());


            IIMFile iimFile = new IIMFile();

            if (image.getTags() != null) {
                for (String tag : image.getTags()) {
                    iimFile.add(IIM.KEYWORDS, tag);
                }
            }

            if (image.getClassifications() != null) {
                for (Classification classification : image.getClassifications()) {
                    String cl = String.format("%s|%f|%s|%s|%s",
                            classification.getLabel(),
                            classification.getConfidence(),
                            classification.getOrigin(),
                            (classification.getTimestamp() != null) ?
                                    classification.getTimestamp().toString() : "",
                            classification.getSelection());
                    iimFile.add(IIM.CATEGORY, cl);
                    iimFile.add(IIM.KEYWORDS, classification.getLabel());
                }
            }

            res.header("Content-Disposition", "attachment; filename=" + image.getImageRef() + ".jpg");
            res.type("image/jpeg");
            HttpServletResponse raw = res.raw();

            JPEGUtil.insertIIMIntoJPEG(raw.getOutputStream(), iimFile, new ByteArrayInputStream(imageRaw));
            raw.getOutputStream().flush();
            raw.getOutputStream().close();

            return raw;

        } catch (Exception e) {
            e.printStackTrace();

            return ExceptionHandler.handle(res, e, e.getMessage());
        }

    }


    /**
     * Handle request for delete individual image
     *
     * @param req http request
     * @param res http response
     */
    private String deleteImage(Request req, Response res) {
        String imageId = req.params(":imageId");
        String streamKey = req.params(":streamKey");

        ThingzoneTimeseriesHandler<Image> imageHandler;

        try {
            imageHandler = timeseriesManager.timeseries(streamKey, Image.class);
        } catch (Exception e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

        try {
            imageHandler.removePoint(UUID.fromString(imageId));
        } catch (Exception e) {
            e.printStackTrace();

            return ExceptionHandler.handle(res, e, e.getMessage());
        }

        return new ResponseBuilder(true)
                .addProperty("message", "Image removed")
                .build().toString();
    }

    /**
     * Handle request for tags suggestion
     *
     * @param req http request
     * @param res http response
     * @return response body
     */
    private String suggestTags(Request req, Response res) {

        String user = req.attribute("introspected_username");
        String prefix = (req.params(":prefix") != null) ? req.params(":prefix") : "";

        String index;
        try {
            index = ElasticSearchUtils.INDEX_PREFIX + asciiToHex(user);
        } catch (Exception e) {
            return ExceptionHandler.handle(res, e, "Could not identify elasticsearch index");
        }


        Search search = new Search.Builder("{\n" +
                " \"_source\": \"suggest\", \n" +
                " \"suggest\": {\n" +
                "        \"tags\" : {\n" +
                "            \"prefix\" : \"" + prefix + "\", \n" +
                "            \"completion\" : { \n" +
                "                \"field\" : \"tags.suggest\" \n" +
                "            }\n" +
                "        }\n" +
                "    }\n" +
                "}")
                .addIndex(index)
                .addType(ElasticSearchUtils.IMAGE_TYPE)
                .build();


        SearchResult result;
        try {
            result = ElasticsearchDriver.getClient().execute(search);

            if (!result.isSucceeded()) {
                return new ResponseBuilder(false)
                        .setReason(result.getErrorMessage())
                        .build().toString();
            }


            JsonObject document = result.getJsonObject();


            JsonArray options = document.getAsJsonObject("suggest")
                    .get("tags")
                    .getAsJsonArray()
                    .get(0)
                    .getAsJsonObject()
                    .getAsJsonArray("options");

            ArrayList<String> suggestedTags = new ArrayList<>();

            options.forEach(option -> {
                String tag = option.getAsJsonObject().get("text").getAsString();
                String type = option.getAsJsonObject().get("_type").getAsString();
                if (tag != null && !suggestedTags.contains(tag) && type.equals("image"))
                    suggestedTags.add(tag);
            });

            return new ResponseBuilder(true)
                    .addElement("suggestions", new Gson().toJsonTree(suggestedTags))
                    .build().toString();

        } catch (IOException e) {
            return ExceptionHandler.handle(res, e, "Could not send request to elasticsearchSS");
        }


    }


    /**
     * Handle list images request
     *
     * @param req http request
     * @param res http response
     * @return response body
     */
    private String list(Request req, Response res) {
        JSONObject queryObject;
        try {
            queryObject = new JSONObject(new JSONTokener(req.body()));
        } catch (JSONException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

        try {
            ImagesListQuery query = new ImagesListQuery(queryObject, req.attribute("introspected_username"));
            return listImages(query).toString();
        } catch (ValidationException e) {
            return ExceptionHandler.handle(res,
                    new IllegalQueryException(e.getKeyword(),
                            queryObject.toString(),
                            e.getErrorMessage()),
                    e.getAllMessages().toString());
        } catch (Exception e) {
            return ExceptionHandler.handle(res, e);
        }
    }


    public JsonObject listImages(ImagesListQuery query) throws Exception {
        List<DataStreamValues<ImageBurst>> images = query.execute();
        ResponseBuilder response = new ResponseBuilder(true)
                .setResponse(jsonStringSerialiser
                        .getDefaultSerialiser()
                        .toJsonTree(images)
                        .getAsJsonArray());

        return response.build();
    }


    /**
     * Method for handling add tag to image requests
     *
     * @param req http request
     * @param res http response
     * @return response body
     */
    private String addTag(Request req, Response res) {

        String imageId = req.params(":imageId");
        String streamKey = req.params(":streamKey");
        String tag = req.params(":tag");

        ThingzoneTimeseriesHandler<Image> imageHandler;

        try {
            imageHandler = timeseriesManager.timeseries(streamKey, Image.class);
        } catch (Exception e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

        try {
            Image image = imageHandler.addTag(UUID.fromString(imageId), tag);
            return new ResponseBuilder(true)
                    .addElement("image", jsonStringSerialiser.getDefaultSerialiser().toJsonTree(image))
                    .build().toString();
        } catch (DataStorageException | DataCachingException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

    }

    private String replaceClassification(Request req, Response res) {

        String imageId = req.params(":imageId");
        String streamKey = req.params(":streamKey");

        List<Classification> classifications;
        try {
            JsonElement classificationResponse = new Gson().fromJson(req.body(), JsonElement.class);
            JsonArray classificationJson = classificationResponse.getAsJsonArray();
            System.out.println(classificationJson.toString());
            classifications = jsonStringSerialiser
                    .getDefaultSerialiser()
                    .fromJson(classificationJson, new TypeToken<List<Classification>>() {
                    }.getType());

        } catch (JSONException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

        ThingzoneTimeseriesHandler<Image> imageHandler;

        try {
            imageHandler = timeseriesManager.timeseries(streamKey, Image.class);
        } catch (Exception e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

        try {
            Image image = imageHandler.replaceClassification(UUID.fromString(imageId), classifications);
            return new ResponseBuilder(true)
                    .addElement("image", jsonStringSerialiser.getDefaultSerialiser().toJsonTree(image))
                    .build().toString();
        } catch (DataStorageException | DataCachingException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }
    }


    /**
     * Method for handling remove tag from image requests
     *
     * @param req http request
     * @param res http response
     * @return response body
     */
    private String removeTag(Request req, Response res) {
        String imageId = req.params(":imageId");
        String streamKey = req.params(":streamKey");
        String tag = req.params(":tag");

        ThingzoneTimeseriesHandler<Image> imageHandler;

        try {
            imageHandler = timeseriesManager.timeseries(streamKey, Image.class);
        } catch (Exception e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }

        try {
            Image image = imageHandler.removeTag(UUID.fromString(imageId), tag);
            return new ResponseBuilder(true)
                    .addElement("image", jsonStringSerialiser.getDefaultSerialiser().toJsonTree(image))
                    .build().toString();
        } catch (DataStorageException | DataCachingException e) {
            return ExceptionHandler.handle(res, e, e.getMessage());
        }


    }

}
