/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.query;

import com.github.filosganga.geogson.model.Feature;
import com.github.filosganga.geogson.model.positions.SinglePosition;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.vividsolutions.jts.geom.*;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.index.query.*;
import org.elasticsearch.join.query.HasParentQueryBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.json.JSONArray;
import org.json.JSONObject;
import uk.nominet.iot.api.datastore.geo.GeoUtils;
import uk.nominet.iot.api.datastore.model.DataStreamGeo;
import uk.nominet.iot.api.datastore.utils.ArrayUtils;
import uk.nominet.iot.api.datastore.utils.ElasticSearchUtils;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.model.api.APIException;
import uk.nominet.iot.model.geo.TimeseriesFeatureCollection;
import uk.nominet.iot.model.timeseries.Metric;
import uk.nominet.iot.model.timeseries.MetricLatLong;
import uk.nominet.iot.model.timeseries.TimeseriesGeoFeature;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static uk.nominet.iot.api.datastore.utils.ElasticSearchUtils.timeDelta;
import static uk.nominet.iot.utils.ConversionUtils.asciiToHex;

public class MetricsGeoQuery extends DatastoreGeoQuery<Metric, List<DataStreamGeo<Metric>>> implements ElasticsearchBasicQuery {

    private List<List<GeoPoint>> boundingPolygons;
    private Integer gap;
    private String geoOutput;

    public MetricsGeoQuery(JSONObject requestBody, String user) throws Exception {
        super(requestBody, user);
        getSchema().validate(requestBody);
        setUser(user);

        if (requestBody.has("streams"))
            setStreams(ArrayUtils.toStringArray(requestBody.getJSONArray("streams")));

        if (requestBody.has("time"))
            setTime(requestBody.getString("time"));

        if (requestBody.has("timeLimit"))
            setTimeLimit(requestBody.getString("timeLimit"));

        if (requestBody.has("forward"))
            setForward(requestBody.getBoolean("forward"));
        else
            setForward(false);

        if (requestBody.has("size"))
            setSize(requestBody.getInt("size"));

        if (requestBody.has("gap"))
            setGap(requestBody.getInt("gap"));

        if (requestBody.has("geoOutput"))
            setGeoOutput(requestBody.getString("geoOutput"));
        else setGeoOutput("points");

        if (requestBody.has("boundingPolygons")) {
            JSONArray polygons = requestBody.optJSONArray("boundingPolygons");
            List<List<GeoPoint>> parsedPolygons = GeoUtils.parsePolygons(polygons);
            if (parsedPolygons != null) {
                setBoundingPolygons(parsedPolygons);
            }
        }
    }

    public String getTime() {
        return time;
    }

    void setTime(String time) {
        this.time = time;
    }

    public String getTimeLimit() {
        return timeLimit;
    }

    void setTimeLimit(String timeLimit) {
        this.timeLimit = timeLimit;
    }

    public boolean isForward() {
        return forward;
    }

    void setForward(boolean forward) {
        this.forward = forward;
    }

    private String getGeoOutput() {
        return geoOutput;
    }

    private void setGeoOutput(String geoOutput) {
        this.geoOutput = geoOutput;
    }

    private List<List<GeoPoint>> getBoundingPolygons() {
        return boundingPolygons;
    }

    private void setBoundingPolygons(List<List<GeoPoint>> boundingPolygons) {
        this.boundingPolygons = boundingPolygons;
    }

    private Integer getGap() {
        return gap;
    }

    private void setGap(Integer gap) {
        this.gap = gap;
    }

    @Override
    public String toString() {
        return "MetricsGeoQuery{" +
               "boundingPolygons=" + boundingPolygons +
               ", streams=" + Arrays.toString(streams) +
               ", time='" + time + '\'' +
               ", timeLimit='" + timeLimit + '\'' +
               ", forward=" + forward +
               ", size=" + size +
               ", gap=" + gap +
               ", geoOutput='" + geoOutput + '\'' +
               '}';
    }

    @Override
    public QueryBuilder getQueryBuilder() {
        return getQueryBuilder(streams[0]);
    }

    private QueryBuilder getQueryBuilder(String streamKey) {
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();

        TermQueryBuilder termsQueryBuilder = new TermQueryBuilder("_id", streamKey);
        HasParentQueryBuilder parentQueryBuilder =
                new HasParentQueryBuilder(ElasticSearchUtils.STREAM_TYPE, termsQueryBuilder, false);

        boolQueryBuilder.must(parentQueryBuilder);


        RangeQueryBuilder rangeQueryBuilder = new RangeQueryBuilder("timestamp");

        String initialTime = (time != null) ? time : "now";

        if (forward) {
            rangeQueryBuilder = rangeQueryBuilder.gte(initialTime);
            if (timeLimit != null)
                rangeQueryBuilder = rangeQueryBuilder.lte(timeDelta(initialTime, timeLimit, forward));
        } else {
            rangeQueryBuilder = rangeQueryBuilder.lte(initialTime);

            if (timeLimit != null) {
                rangeQueryBuilder = rangeQueryBuilder.gte(timeDelta(initialTime, timeLimit, forward));
            }
        }

        boolQueryBuilder.must(rangeQueryBuilder);

        if (getBoundingPolygons() != null && getBoundingPolygons().size() > 0 && !geoOutput.equals("line")) {
            if (getBoundingPolygons().size() > 1) {
                BoolQueryBuilder polygonsQuery = QueryBuilders.boolQuery();
                for (List<GeoPoint> polygon : getBoundingPolygons()) {
                    polygonsQuery.should(ElasticSearchUtils.buildGeoQuery(polygon, "value_lat_long"));
                }
                polygonsQuery.minimumShouldMatch(1);
                boolQueryBuilder.filter(polygonsQuery);
            } else {
                boolQueryBuilder.filter(ElasticSearchUtils.buildGeoQuery(getBoundingPolygons().get(0), "value_lat_long"));
            }
        }

        return boolQueryBuilder;
    }




    @Override
    public AggregationBuilder getAggregationBuilder() {
        return null;
    }

    @Override
    public String getSortName() {
        return "timestamp";
    }

    @Override
    public SortOrder getSortOrder() {
        if (forward)
            return SortOrder.ASC;
        else
            return SortOrder.DESC;
    }

    @Override
    public List<DataStreamGeo<Metric>> execute() throws Exception {

        List<DataStreamGeo<Metric>> results = new ArrayList<>();

        String index;
        try {
            index = ElasticSearchUtils.INDEX_PREFIX + asciiToHex(getUser());
        } catch (Exception e) {
            throw new APIException(400, "Could not identify elasticsearch index", e);
        }


        for (String stream : getStreams()) {
            try {
                JsonObject streamInfo = ElasticSearchUtils.getStreamInfo(index, stream);
                DataStreamGeo<Metric> dataStreamGeo = new DataStreamGeo<>();
                if (streamInfo.has("name"))
                    dataStreamGeo.setName(streamInfo.get("name").getAsString());

                if (streamInfo.has("type"))
                    dataStreamGeo.setType(streamInfo.get("type").getAsString());

                if (streamInfo.has("metadata")) {
                    Map<String, Object> metadata =
                            jsonStringSerialiser
                                    .getDefaultSerialiser()
                                    .fromJson(streamInfo.get("metadata").getAsJsonObject(),
                                              new TypeToken<Map<String, Object>>() {
                                              }.getType());
                    dataStreamGeo.setMetadata(metadata);
                }

                dataStreamGeo.setFeatures(getGeoFeaturesForStream(index, streamInfo));
                results.add(dataStreamGeo);
            } catch (Exception e) {
                if (e.getCause() != null) {
                    throw new APIException(400, "Could not send request to elasticsearch: " + e.getMessage(), e);
                } else {
                    throw new APIException(400, "Could not send request to elasticsearch: " + Objects.requireNonNull(e.getCause()).getMessage(), e);
                }
            }
        }

        return results;
    }


    /**
     * Create a geojson feature for a specific data stream
     * @param index index
     * @param streamInfo stream info
     * @return Json object with geojson features
     */
    private TimeseriesFeatureCollection<Metric> getGeoFeaturesForStream(String index, JsonObject streamInfo)
            throws Exception {
        SearchSourceBuilder searchBuilder = new SearchSourceBuilder()
                                                    .query(getQueryBuilder(streamInfo.get("key").getAsString()))
                                                    .sort(getSortName(), getSortOrder())
                                                    .size((size != null) ? size : 10000);

        Search search = new Search.Builder(searchBuilder.toString())
                                .addIndex(index)
                                .addType(ElasticSearchUtils.METRIC_TYPE)
                                .build();


        SearchResult result;
        result = ElasticsearchDriver.getClient().execute(search);
        if (result.isSucceeded()) {
            JsonObject streamSummary = ElasticSearchUtils.getStreamSummary(streamInfo);

            if (getGeoOutput() == null || getGeoOutput().equals("points")) {
                //noinspection unchecked
                return extractPointsFromSearch(result, streamSummary);
            } else if (getGeoOutput().equals("line")) {
                //noinspection unchecked
                return extractLineFromSearchV2(result, streamSummary);
            } else {
                throw new Exception("output method not recognised");
            }
        } else {
            throw new Exception(result.getErrorMessage());
        }
    }


    /**
     * Extract geojson points from a search result
     * @param result        search result
     * @param streamSummary stream summary
     * @return geojson points
     */
    private TimeseriesFeatureCollection extractPointsFromSearch(SearchResult result, JsonObject streamSummary) throws Exception {
        TimeseriesFeatureCollection<MetricLatLong> featureCollection = new TimeseriesFeatureCollection<>();

        for (SearchResult.Hit<JsonObject, Void> hit : result.getHits(JsonObject.class)) {

            MetricLatLong metricLatLong = jsonStringSerialiser.readObject(hit.source, MetricLatLong.class);

            TimeseriesGeoFeature<MetricLatLong> timeseriesGeoFeature = new TimeseriesGeoFeature<>();

            timeseriesGeoFeature.setPoint(metricLatLong);

            Feature.Builder pointBuilder = Feature.builder().withGeometry(
                    com.github.filosganga.geogson.model.Point.from(metricLatLong.getValue().getLon(),
                                                                   metricLatLong.getValue().getLat()));

            timeseriesGeoFeature.setFeature(pointBuilder.build());

            featureCollection.add(timeseriesGeoFeature);

        }

        return featureCollection;
    }


    private TimeseriesFeatureCollection extractLineFromSearchV2(SearchResult result, JsonObject streamSummary) throws Exception {
        TimeseriesFeatureCollection<MetricLatLong> featureCollection = new TimeseriesFeatureCollection<>();

        Double previousLat = null;
        Double previousLon = null;

        for (SearchResult.Hit<JsonObject, Void> hit : result.getHits(JsonObject.class)) {

            MetricLatLong metricLatLong = jsonStringSerialiser.readObject(hit.source, MetricLatLong.class);

            Double lon = metricLatLong.getValue().getLon();
            Double lat = metricLatLong.getValue().getLat();

            if (previousLat == null || previousLon == null) {
                previousLat = lat;
                previousLon = lon;
                continue;
            }

            TimeseriesGeoFeature<MetricLatLong> timeseriesGeoFeature = new TimeseriesGeoFeature<>();

            timeseriesGeoFeature.setPoint(metricLatLong);

            Feature.Builder lineBuilder = Feature.builder().withGeometry(
                    com.github.filosganga.geogson.model.LineString.of(
                            new com.github.filosganga.geogson.model.Point(new SinglePosition(previousLon, previousLat, 0)),
                            new com.github.filosganga.geogson.model.Point(new SinglePosition(lon, lat, 0))));

            timeseriesGeoFeature.setFeature(lineBuilder.build());

            featureCollection.add(timeseriesGeoFeature);

            previousLat = lat;
            previousLon = lon;

        }

        return featureCollection;

    }

}
