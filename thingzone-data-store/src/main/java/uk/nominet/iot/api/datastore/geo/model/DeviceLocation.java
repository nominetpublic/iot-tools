/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.geo.model;

import org.apache.commons.math3.geometry.spherical.twod.S2Point;
import org.apache.commons.math3.ml.clustering.Clusterable;
import org.apache.commons.math3.util.FastMath;

import java.util.Objects;

public class DeviceLocation extends S2Point implements Clusterable {
    private static final long serialVersionUID = 5446276656739479732L;
    private final String id;
    private final String type;
    private final double[] point = new double[2];
    private final Integer index;

    public DeviceLocation(String id, String type, Latitude lat, Longitude lon, Integer index) throws IllegalArgumentException {
        super(lon.getRadians(), (FastMath.PI / 2) - lat.getRadians());
        this.id = id;
        this.type = type;
        this.point[0] = lat.getDegrees();
        this.point[1] = lon.getDegrees();
        this.index = index;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public Integer getIndex() {
        return index;
    }

    @Override
    public double[] getPoint() {
        return point;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof DeviceLocation)) {
            return false;
        }
        DeviceLocation other = (DeviceLocation) obj;
        return super.equals(obj) && id.equals(other.id) && type.equals(other.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, type);
    }

    @Override
    public String toString() {
        return String.format("%s,%s,%f,%f", id, type, point[0], point[1]);
    }
}
