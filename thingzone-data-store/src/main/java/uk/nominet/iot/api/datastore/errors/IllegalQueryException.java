/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.errors;

public class IllegalQueryException extends IllegalArgumentException {
    private static final long serialVersionUID = 3222923545025806610L;
    private final String key;
    private final String value;
    private final String constraint;

    public IllegalQueryException(String key, String value, String constraint) {
        this.key = key;
        this.value = value;
        this.constraint = constraint;
    }

    @Override
    public String getMessage() {
        return String.format("Illegal value '%s' for query property %s, because %s",
                value, key, constraint);
    }
}
