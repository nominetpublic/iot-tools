/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.integrationTesting;

import com.github.filosganga.geogson.model.Point;
import com.github.filosganga.geogson.model.positions.SinglePosition;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.restassured.RestAssured;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.utils.URIBuilder;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.testcontainers.shaded.com.google.common.collect.ImmutableList;
import uk.nominet.iot.api.datastore.DatastoreApp;
import uk.nominet.iot.api.datastore.config.DatastoreConstant;
import uk.nominet.iot.config.*;
import uk.nominet.iot.driver.CassandraDriver;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.init.CassandraInitService;
import uk.nominet.iot.init.ElasticInitService;
import uk.nominet.iot.manager.*;
import uk.nominet.iot.model.timeseries.*;
import uk.nominet.iot.registry.RegistryAPISession;
import uk.nominet.iot.test.data.MetricGenerator;
import uk.nominet.iot.test.docker.compose.ThingzoneComposeRuntimeContainer;
import uk.nominet.iot.test.docker.compose.ThingzoneEnvironment;
import uk.nominet.iot.tools.BootstrapRegistryData;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

@RunWith(Suite.class)
@Suite.SuiteClasses({
                            MetricsDatesITSuite.class,
                            MetricsAggITSuite.class,
                            MetricsListITSuite.class,
                            MetricsGeoITSuite.class,
                            AlertsITSuite.class,
                            DeviceITSuite.class,
                            ImagesListITSuite.class,
                            CombinationITSuite.class,
                            StatisticITSuite.class
                    })
public class DataStoreAPIIntegrationIT {
    private static final String DOCKER_COMPOSE_TEST_DATA_CONFIG_JSON = "docker_compose_test/test_data_config.json";
    private static final String DOCKER_COMPOSE_TEST_DATA_CSV = "docker_compose_test/test_data.csv";
    private static final String TEST_DATASTORE_PORT = "8085";
    private static final String TEST_DATASTORE_HOST = "localhost";
    private static final String TEST_DATASTORE_AGENT_USERNAME = "datastore_agent";
    private static final String TEST_DATASTORE_AGENT_PASSWORD = "datastore_agent";
    public static final String USERNAME = "test";
    private static final String PASSWORD = "test";


    public static String userSessionKey;
    public static final Instant initialTime = Instant.now();

    public static final String DATE_STREAM_D1 = "device1.stream.metric.double.dates";
    public static final String DATE_STREAM_D2 = "device2.stream.metric.double.dates";
    public static final String METRIC_DOUBLE_STREAM = "device1.stream.metric.double";
    public static final String METRIC_INT_STREAM = "device1.stream.metric.int";
    public static final String METRIC_STRING_STREAM = "device1.stream.metric.string";
    public static final String ALERT_STREAM = "device1.stream.alert";
    public static final String METRIC_LATLONG = "device1.stream.metric.latlong";
    public static final String IMAGE_STREAM = "device1.stream.image";
    private static final String EVENT_STREAM = "device1.stream.event";


    private static final Instant TEST_DATASTORE_DATE_DEVICE1_1 = Instant.parse("2018-10-01T11:11:11Z");
    private static final Instant TEST_DATASTORE_DATE_DEVICE1_2 = Instant.parse("2018-10-05T12:12:12Z");
    private static final Instant TEST_DATASTORE_DATE_DEVICE2_1 = Instant.parse("2018-10-07T13:13:13Z");
    private static final Instant TEST_DATASTORE_DATE_DEVICE2_2 = Instant.parse("2018-10-09T14:14:14Z");

    public static final List<UUID> alertIds = new ArrayList<>();


    @ClassRule
    public static ThingzoneComposeRuntimeContainer environment =
            new ThingzoneEnvironment()
                    .withServices(
                            ThingzoneEnvironment.SERVICE_ELASTICSEARCH,
                            ThingzoneEnvironment.SERVICE_CASSANDRA,
                            ThingzoneEnvironment.SERVICE_POSTGRES,
                            ThingzoneEnvironment.SERVICE_REGISTRY
                                 )
                    .getContainer();

    @BeforeClass
    public static void setUpClass() throws Exception {

        initialiseCassandraService();
        initialiseElasticsearchService();
        initialiseRegistryService("user_admin", "user_admin");


        // Bootstrap registry
        InputStream jobInputStream = new FileInputStream(DOCKER_COMPOSE_TEST_DATA_CONFIG_JSON);
        File deviceListCSVFile = new File(DOCKER_COMPOSE_TEST_DATA_CSV);
        String jsonTxt = IOUtils.toString(jobInputStream, StandardCharsets.UTF_8);
        JsonObject job = new Gson().fromJson(jsonTxt, JsonObject.class);

        BootstrapRegistryData bootstrapRegistryData = new BootstrapRegistryData(
                job,
                deviceListCSVFile,
                environment.getServicePort(ThingzoneEnvironment.SERVICE_REGISTRY)
        );
        bootstrapRegistryData.createRegistryUsers();
        bootstrapRegistryData.createFunctions();
        bootstrapRegistryData.createDevices();


        Properties configProp = new Properties();
        configProp.put(DatastoreConstant.DATASTORE_HOST, TEST_DATASTORE_HOST);
        configProp.put(DatastoreConstant.DATASTORE_PORT, TEST_DATASTORE_PORT);

        configProp.put(RegistryConstant.REGISTRY_SERVER_SCHEME, "http");
        configProp.put(RegistryConstant.REGISTRY_SERVER_HOST,
                       environment.getServiceHost(ThingzoneEnvironment.SERVICE_REGISTRY.getName(),
                                                  ThingzoneEnvironment.SERVICE_REGISTRY.getPort()));
        configProp.put(RegistryConstant.REGISTRY_SERVER_PORT,
                       environment.getServicePort(ThingzoneEnvironment.SERVICE_REGISTRY.getName(),
                                                  ThingzoneEnvironment.SERVICE_REGISTRY.getPort()).toString());
        configProp.put(RegistryConstant.REGISTRY_SERVER_PATH, "");
        configProp.put(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS, 3);


        configProp.put(RegistryConstant.REGISTRY_SERVER_USERNAME, TEST_DATASTORE_AGENT_USERNAME);
        configProp.put(RegistryConstant.REGISTRY_SERVER_PASSWORD, TEST_DATASTORE_AGENT_PASSWORD);


        configProp.put(ElasticsearchConstant.ELASTICSEARCH_SERVER_SCHEME, "http");
        configProp.put(ElasticsearchConstant.ELASTICSEARCH_SERVER_HOST,
                       environment.getServiceHost(ThingzoneEnvironment.SERVICE_ELASTICSEARCH.getName(),
                                                  ThingzoneEnvironment.SERVICE_ELASTICSEARCH.getPort()));
        configProp.put(ElasticsearchConstant.ELASTICSEARCH_SERVER_PORT,
                       environment.getServicePort(ThingzoneEnvironment.SERVICE_ELASTICSEARCH.getName(),
                                                  ThingzoneEnvironment.SERVICE_ELASTICSEARCH.getPort()).toString());
        configProp.put(ElasticsearchConstant.ELASTICSEARCH_SERVER_PATH, "");


        configProp.put(CassandraConstant.CASSANDRA_SERVER_HOST,
                       environment.getServiceHost(ThingzoneEnvironment.SERVICE_CASSANDRA.getName(),
                                                  ThingzoneEnvironment.SERVICE_CASSANDRA.getPort()));
        configProp.put(CassandraConstant.CASSANDRA_SERVER_PORT,
                       environment.getServicePort(ThingzoneEnvironment.SERVICE_CASSANDRA.getName(),
                                                  ThingzoneEnvironment.SERVICE_CASSANDRA.getPort()).toString());

        configProp.put(CassandraConstant.CASSANDRA_SERVER_PATH, "");

        initData();

        //Run the datastore app
        DatastoreApp app = new DatastoreApp(configProp);
        app.initDrivers();
        app.run();

        RestAssured.port = Integer.parseInt(TEST_DATASTORE_PORT);
        RestAssured.baseURI = String.format("http://%s", TEST_DATASTORE_HOST);
    }


    /**
     * Initialise data for the integration tests
     */
    private static void initData() throws Exception {
        DataStoreAPIIntegrationIT.initialiseRegistryService("bridge_agent", "bridge_agent");
        ThingzoneTimeseriesManager timeseriesManager =
                ThingzoneTimeseriesManager.createManager(new UserContext(USERNAME).setPassword(PASSWORD));

        ThingzoneEventManager alertManager =
                ThingzoneEventManager.createManager(new UserContext(USERNAME).setPassword(PASSWORD));

        System.out.print("Initialising metrics data ... ");
        initMetrics(timeseriesManager);
        System.out.println("DONE");

        System.out.print("Initialising image data ... ");
        initImages(timeseriesManager);
        System.out.println("DONE");

        System.out.print("Initialising time data ... ");
        initDates(timeseriesManager);
        System.out.println("DONE");

        System.out.print("Initialising alert data ... ");
        initAlerts(alertManager);
        System.out.println("DONE");

        System.out.print("Initialising event data ... ");
        initEvents(timeseriesManager);
        System.out.println("DONE");

        Thread.sleep(1500); //Wait for elasticsearch to refresh the index

        //DataStoreAPIIntegrationIT.setUpClass();
        DataStoreAPIIntegrationIT.initialiseRegistryService("datastore_agent", "datastore_agent");
        userSessionKey = DataStoreAPIIntegrationIT.getUserSession(USERNAME, PASSWORD);
        RestAssured.basePath = String.format("/api/v%d/metrics", DatastoreApp.VERSION);
    }

    private static void initAlerts(ThingzoneEventManager alertManager) throws Exception {
        ThingzoneEventHandler alertHandler = alertManager.timeseries(ALERT_STREAM);

        int i = 0;
        for (Instant time = initialTime.minus(1, ChronoUnit.DAYS);
             time.isBefore(initialTime) || time.equals(initialTime);
             time = time.plus(60, ChronoUnit.MINUTES)) {
            Event alert = new Event();

            UUID id = UUID.randomUUID();
            alertIds.add(id);

            alert.setId(id);
            alert.setStreamKey(ALERT_STREAM);
            alert.setTags((i % 2 == 0) ? ImmutableList.of("tag1", "tag2") : ImmutableList.of("tag3"));
            alert.setReceivers(ImmutableList.of("user1", "user2"));
            alert.setDescription("Alert " + i);
            alert.setName("Alert " + i);

            SinglePosition position = (i % 3 == 0) ?
                                      new SinglePosition(-1.2482662188959242, 51.74033567393796, 0.0) :
                                      new SinglePosition(-1.2482662188959242, 51.76033567393796, 0.0);

            alert.setLocation(new Point(position));
            alert.setCategory((i % 2 == 0) ? "categoryA" : "categoryB");
            alert.setEventType(ThingzoneEventType.ALERT);
            alert.setType((i % 2 == 0) ? "typeA" : "typeB");
            alert.setSeverity(0.03 * i);
            alert.setTimestamp(time);
            alertHandler.addPoint(alert);
            i++;
        }
    }

    private static void initEvents(ThingzoneTimeseriesManager timeseriesManager) throws Exception {
        ThingzoneTimeseriesHandler<Event> timeseries = timeseriesManager.timeseries(EVENT_STREAM, Event.class);

        int i = 0;
        for (Instant time = initialTime.minus(1, ChronoUnit.DAYS);
             time.isBefore(initialTime) || time.equals(initialTime);
             time = time.plus(60, ChronoUnit.MINUTES)) {
            Event event = new Event();

            UUID id = UUID.randomUUID();

            event.setId(id);
            event.setStreamKey(EVENT_STREAM);
            event.setTags((i % 2 == 0) ? ImmutableList.of("tag1", "tag2") : ImmutableList.of("tag3"));
            event.setDescription("Event " + i);
            event.setName("Event " + i);

            SinglePosition position = (i % 3 == 0) ?
                                      new SinglePosition(-1.2482662188959242, 51.74033567393796, 0.0) :
                                      new SinglePosition(-1.2482662188959242, 51.76033567393796, 0.0);

            event.setLocation(new Point(position));
            event.setCategory((i % 2 == 0) ? "categoryA" : "categoryB");
            event.setType((i % 2 == 0) ? "typeA" : "typeB");
            event.setEventType(ThingzoneEventType.GENERIC);
            event.setValue(0.03 * i);
            event.setTimestamp(time);
            timeseries.addPoint(event);
            i++;
        }
    }


    /**
     * Init metrics
     * @param timeseriesManager timeseries manager
     */
    private static void initImages(ThingzoneTimeseriesManager timeseriesManager) throws Exception {
        ThingzoneTimeseriesHandler<Image> timeseries = timeseriesManager.timeseries(IMAGE_STREAM, Image.class);

        int i = 1;
        for (Instant time = initialTime.minus(10, ChronoUnit.DAYS);
             time.isBefore(initialTime) || time.equals(initialTime);
             time = time.plus(60, ChronoUnit.MINUTES)) {

            if (i % 10 == 0) {
                int burst = 10;

                for (int i1 = 0; i1 < burst; i1++) {
                    Image image = new Image();
                    image.setId(UUID.randomUUID());
                    image.setTimestamp(time.plus(i1, ChronoUnit.SECONDS));
                    image.setStreamKey(IMAGE_STREAM);
                    image.setImageRef("REF_" + i + "_"+ i1);
                    timeseries.addPoint(image);
                }

            } else {

                Image image = new Image();
                image.setId(UUID.randomUUID());
                image.setTimestamp(time);
                image.setStreamKey(IMAGE_STREAM);
                image.setImageRef("REF_" + i);

                timeseries.addPoint(image);
            }

            i++;
        }
    }

    /**
     * Init dates
     * @param timeseriesManager timeseries manager
     */
    private static void initDates(ThingzoneTimeseriesManager timeseriesManager) throws Exception {
        /* ADD DATA TO STREAM_D1 */
        ThingzoneTimeseriesHandler<MetricDouble> device1Manager = timeseriesManager.timeseries(DATE_STREAM_D1, MetricDouble.class);
        device1Manager.addPoint(getRandomMetric(DATE_STREAM_D1, TEST_DATASTORE_DATE_DEVICE1_1));
        device1Manager.addPoint(getRandomMetric(DATE_STREAM_D1, TEST_DATASTORE_DATE_DEVICE1_2));

        /* ADD DATA TO STREAM_D2 */
        ThingzoneTimeseriesHandler<MetricDouble> device2Manager = timeseriesManager.timeseries(DATE_STREAM_D2, MetricDouble.class);
        device2Manager.addPoint(getRandomMetric(DATE_STREAM_D2, TEST_DATASTORE_DATE_DEVICE2_1));
        device2Manager.addPoint(getRandomMetric(DATE_STREAM_D2, TEST_DATASTORE_DATE_DEVICE2_2));
    }


    /**
     * Init metrics
     * @param timeseriesManager timeseries manager
     */
    private static void initMetrics(ThingzoneTimeseriesManager timeseriesManager) throws Exception {
        ThingzoneTimeseriesHandler<MetricDouble> timeseriesMetricDouble =
                timeseriesManager.timeseries(METRIC_DOUBLE_STREAM, MetricDouble.class);

        ThingzoneTimeseriesHandler<MetricInt> timeseriesMetricInt =
                timeseriesManager.timeseries(METRIC_INT_STREAM, MetricInt.class);

        ThingzoneTimeseriesHandler<MetricString> timeseriesMetricString =
                timeseriesManager.timeseries(METRIC_STRING_STREAM, MetricString.class);

        ThingzoneTimeseriesHandler<MetricLatLong> timeseriesLatLong =
                timeseriesManager.timeseries(METRIC_LATLONG, MetricLatLong.class);


        int i = 1;
        for (Instant time = initialTime.minus(10, ChronoUnit.DAYS);
             time.isBefore(initialTime) || time.equals(initialTime);
             time = time.plus(15, ChronoUnit.MINUTES)) {

            double valueDouble = (i % 10) + 0.2;

            MetricDouble metric = MetricGenerator.generateMetricDouble(
                    UUID.randomUUID(),
                    METRIC_DOUBLE_STREAM,
                    valueDouble);

            metric.setTimestamp(time);
            timeseriesMetricDouble.addPoint(metric);

            int valueInt = i % 10;

            MetricInt metricInt = MetricGenerator.generateMetricInt(
                    UUID.randomUUID(),
                    METRIC_INT_STREAM,
                    valueInt);

            metricInt.setTimestamp(time);
            timeseriesMetricInt.addPoint(metricInt);

            MetricString metricString = MetricGenerator.generateMetricString(
                    UUID.randomUUID(),
                    METRIC_STRING_STREAM,
                    "String_" + i);

            metricString.setTimestamp(time);
            timeseriesMetricString.addPoint(metricString);

            MetricLatLong metricLatLong = MetricGenerator.generateMetricLatLong(
                    UUID.randomUUID(),
                    METRIC_LATLONG,
                    51.75087933281515 - (0.001 * i),
                    -1.2574946880340576);

            metricLatLong.setTimestamp(time);
            timeseriesLatLong.addPoint(metricLatLong);

            i++;
        }
    }

    private static MetricDouble getRandomMetric(String stream, Instant time) {
        MetricDouble metric = MetricGenerator.generateMetricDouble(
                UUID.randomUUID(),
                stream,
                Math.random());
        metric.setTimestamp(time);
        return metric;
    }


    /**
     * Initialise the Cassandra driver and service
     */
    private static void initialiseCassandraService() throws Exception {
        if (!CassandraDriver.isInitialized()) {
            CassandraDriverConfig cassandraConfig = new CassandraDriverConfig();
            cassandraConfig.addProperty(CassandraConstant.CASSANDRA_SERVER_HOST,
                                        environment.getServiceHost(ThingzoneEnvironment.SERVICE_CASSANDRA));
            cassandraConfig.addProperty(CassandraConstant.CASSANDRA_SERVER_PORT,
                                        environment.getServicePort(ThingzoneEnvironment.SERVICE_CASSANDRA));
            CassandraDriver.setConnectionProperties(cassandraConfig);
            CassandraDriver.globalInitialise();
        }

        CassandraInitService cassandraInitService = new CassandraInitService();
        cassandraInitService.clean();
        cassandraInitService.init();
    }

    /**
     * Initialise Elasticsearch driver and service
     */
    private static void initialiseElasticsearchService() throws Exception {
        if (!ElasticsearchDriver.isInitialized()) {
            ElasticsearchDriverConfig elasticsearchConfig = new ElasticsearchDriverConfig();
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_SCHEME, "http");
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_HOST,
                                            environment.getServiceHost(ThingzoneEnvironment.SERVICE_ELASTICSEARCH));
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_PORT,
                                            environment.getServicePort(ThingzoneEnvironment.SERVICE_ELASTICSEARCH));
            ElasticsearchDriver.setConnectionProperties(elasticsearchConfig);
            ElasticsearchDriver.globalInitialise();
        }

        ElasticInitService elasticInitService = new ElasticInitService();
        elasticInitService.clean();
        elasticInitService.init();
    }


    public static void initialiseRegistryService(String username, String password) {
        if (RegistryAPIDriver.isInitialized())
            RegistryAPIDriver.globalDispose();
        if (!RegistryAPIDriver.isInitialized()) {
            RegistryDriverConfig registryConfig = new RegistryDriverConfig();
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_SCHEME, "http");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST,
                                       environment.getServiceHost("registry", 8081));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT,
                                       environment.getServicePort("registry", 8081));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_USERNAME, username);
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD, password);
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS, 3);
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }
    }

    public static String getUserSession(String username, String password) throws Exception {
        URIBuilder uriBuilder = new URIBuilder();
        uriBuilder.setScheme("http");
        uriBuilder.setHost(DataStoreAPIIntegrationIT.environment.getServiceHost("registry", 8081));
        uriBuilder.setPort(DataStoreAPIIntegrationIT.environment.getServicePort("registry", 8081));

        URI registryURI = uriBuilder.build();

        RegistryAPISession userAPISession =
                new RegistryAPISession(registryURI.toString(), username, password);
        return userAPISession.getSession().getSessionKey();
    }

    @AfterClass
    public static void tearDownClass() {
        CassandraDriver.globalDispose();
        RegistryAPIDriver.globalDispose();
        ElasticsearchDriver.globalDispose();
    }

}
