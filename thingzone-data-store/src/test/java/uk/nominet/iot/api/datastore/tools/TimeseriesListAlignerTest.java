/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.tools;

import org.junit.BeforeClass;
import org.junit.Test;
import uk.nominet.iot.api.datastore.model.DataStreamValues;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;
import uk.nominet.iot.model.timeseries.Metric;
import uk.nominet.iot.model.timeseries.MetricInt;
import uk.nominet.iot.model.timeseries.MetricString;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class TimeseriesListAlignerTest {
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
    private static final List<AbstractTimeseriesPoint> p1Points = new ArrayList<>();
    private static final List<AbstractTimeseriesPoint> p2Points = new ArrayList<>();
    private static final List<AbstractTimeseriesPoint> p3Points = new ArrayList<>();
    private static final List<AbstractTimeseriesPoint> p4Points = new ArrayList<>();


    @BeforeClass
    public static void setUpClass() {
        long startSeconds = 1561043840;

        for (int i = 0; i < 100; i++) {
            MetricInt metricIntP1 = new MetricInt();
            metricIntP1.setStreamKey("Stream1");
            metricIntP1.setId(UUID.randomUUID());
            metricIntP1.setTimestamp(Instant.ofEpochSecond(startSeconds + (i * 5)));
            metricIntP1.setValue(i);
            p1Points.add(metricIntP1);


            MetricInt metricIntP2 = new MetricInt();
            metricIntP2.setStreamKey("Stream2");
            metricIntP2.setId(UUID.randomUUID());
            metricIntP2.setTimestamp(Instant.ofEpochSecond((startSeconds - 10) + (i * 5)));
            metricIntP2.setValue(i);
            p2Points.add(metricIntP2);


            MetricInt metricIntP3 = new MetricInt();
            metricIntP3.setStreamKey("Stream3");
            metricIntP3.setId(UUID.randomUUID());
            metricIntP3.setTimestamp(Instant.ofEpochSecond((startSeconds + 10) + (i * 5)));
            metricIntP3.setValue(i);
            p3Points.add(metricIntP3);


            MetricString metricStringP4 = new MetricString();
            metricStringP4.setStreamKey("Stream4");
            metricStringP4.setId(UUID.randomUUID());
            metricStringP4.setTimestamp(Instant.ofEpochSecond(startSeconds + (i * 6)));
            metricStringP4.setValue("String " + i);
            p4Points.add(metricStringP4);
        }

        Instant from = Instant.now();


    }


    @Test
    public void alignTimeseriesesOverlapping() throws Exception {
        List<DataStreamValues<AbstractTimeseriesPoint>> primaryTimeseries = new ArrayList<>();
        Map<String, List<DataStreamValues<AbstractTimeseriesPoint>>> secondaryTimeserieses = new HashMap<>();

        DataStreamValues<AbstractTimeseriesPoint> primaryTimeseriesValues = new DataStreamValues<>();
        primaryTimeseriesValues.setStreamKey("Stream1");
        primaryTimeseriesValues.setValues(p1Points);
        primaryTimeseries.add(primaryTimeseriesValues);


        DataStreamValues<AbstractTimeseriesPoint> secondaryTimeseriesesValues1 = new DataStreamValues<>();
        secondaryTimeseriesesValues1.setStreamKey("Stream2");
        secondaryTimeseriesesValues1.setValues(p2Points);

        List<DataStreamValues<AbstractTimeseriesPoint>> secondaryTimeseriesList1 = new ArrayList<>();
        secondaryTimeseriesList1.add(secondaryTimeseriesesValues1);

        secondaryTimeserieses.put("foo", secondaryTimeseriesList1);

        TimeseriesListAligner timeseriesListAligner = new TimeseriesListAligner(primaryTimeseries, secondaryTimeserieses);

        List<DataStreamValues<AbstractTimeseriesPoint>> results = timeseriesListAligner.align();

        System.out.println("Results : " + jsonStringSerialiser.writeObject(results));

    }


    @Test
    public void alignTimeseriesesOverlapping1() throws Exception {
        List<DataStreamValues<AbstractTimeseriesPoint>> primaryTimeseries = new ArrayList<>();
        Map<String, List<DataStreamValues<AbstractTimeseriesPoint>>> secondaryTimeserieses = new HashMap<>();

        DataStreamValues<AbstractTimeseriesPoint> primaryTimeseriesValues = new DataStreamValues<>();
        primaryTimeseriesValues.setStreamKey("Stream1");
        primaryTimeseriesValues.setValues(p1Points);
        primaryTimeseries.add(primaryTimeseriesValues);


        DataStreamValues<AbstractTimeseriesPoint> secondaryTimeseriesesValues1 = new DataStreamValues<>();
        secondaryTimeseriesesValues1.setStreamKey("Stream3");
        secondaryTimeseriesesValues1.setValues(p3Points);

        List<DataStreamValues<AbstractTimeseriesPoint>> secondaryTimeseriesList1 = new ArrayList<>();
        secondaryTimeseriesList1.add(secondaryTimeseriesesValues1);

        secondaryTimeserieses.put("foo", secondaryTimeseriesList1);

        TimeseriesListAligner timeseriesListAligner = new TimeseriesListAligner(primaryTimeseries, secondaryTimeserieses);

        List<DataStreamValues<AbstractTimeseriesPoint>> results = timeseriesListAligner.align();

        System.out.println("Results : " + jsonStringSerialiser.writeObject(results));

    }


    @Test
    public void alignTimeseriesesMultiple() throws Exception {
        List<DataStreamValues<AbstractTimeseriesPoint>> primaryTimeseries = new ArrayList<>();
        Map<String, List<DataStreamValues<AbstractTimeseriesPoint>>> secondaryTimeserieses = new HashMap<>();

        DataStreamValues<AbstractTimeseriesPoint> primaryTimeseriesValues = new DataStreamValues<>();
        primaryTimeseriesValues.setStreamKey("Stream1");
        primaryTimeseriesValues.setValues(p1Points);
        primaryTimeseries.add(primaryTimeseriesValues);


        DataStreamValues<AbstractTimeseriesPoint> secondaryTimeseriesesValues1 = new DataStreamValues<>();
        secondaryTimeseriesesValues1.setStreamKey("Stream2");
        secondaryTimeseriesesValues1.setValues(p2Points);
        List<DataStreamValues<AbstractTimeseriesPoint>> secondaryTimeseriesList1 = new ArrayList<>();
        secondaryTimeseriesList1.add(secondaryTimeseriesesValues1);

        DataStreamValues<AbstractTimeseriesPoint> secondaryTimeseriesesValues2 = new DataStreamValues<>();
        secondaryTimeseriesesValues2.setStreamKey("Stream3");
        secondaryTimeseriesesValues2.setValues(p3Points);
        List<DataStreamValues<AbstractTimeseriesPoint>> secondaryTimeseriesList2 = new ArrayList<>();
        secondaryTimeseriesList2.add(secondaryTimeseriesesValues2);

        DataStreamValues<AbstractTimeseriesPoint> secondaryTimeseriesesValues3 = new DataStreamValues<>();
        secondaryTimeseriesesValues3.setStreamKey("Stream4");
        secondaryTimeseriesesValues3.setValues(p4Points);
        List<DataStreamValues<AbstractTimeseriesPoint>> secondaryTimeseriesList3 = new ArrayList<>();
        secondaryTimeseriesList3.add(secondaryTimeseriesesValues3);


        secondaryTimeserieses.put("foo", secondaryTimeseriesList1);
        secondaryTimeserieses.put("bar", secondaryTimeseriesList2);
        secondaryTimeserieses.put("baz", secondaryTimeseriesList3);

        TimeseriesListAligner timeseriesListAligner = new TimeseriesListAligner(primaryTimeseries, secondaryTimeserieses);

        List<DataStreamValues<AbstractTimeseriesPoint>> results = timeseriesListAligner.align();

        System.out.println("Results : " + jsonStringSerialiser.writeObject(results));

    }

    @Test
    public void testBinarySearch() {
        Instant now = Instant.now();
        Instant one = now.minus(10, ChronoUnit.MINUTES);
        Instant three = now.plus(10, ChronoUnit.MINUTES);

        List<AbstractTimeseriesPoint> points = new ArrayList<>();
        points.add(generateMetric(one));
        points.add(generateMetric(now));
        points.add(generateMetric(three));

        Metric pointToInsert = generateMetric(now.minus(-13, ChronoUnit.MINUTES));
        int index = Collections.binarySearch(points, pointToInsert, new TimeseriesTimeSorter());

        if (index < -1) index = -index - 2;

        System.out.println("Index: " + index);
    }


    private Metric generateMetric(Instant timestamp) {
        MetricInt metricInt = new MetricInt();
        metricInt.setStreamKey("Stream1");
        metricInt.setId(UUID.randomUUID());
        metricInt.setTimestamp(timestamp);
        metricInt.setValue(1);
        return metricInt;
    }
}
