/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.integrationTesting;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.restassured.RestAssured;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import uk.nominet.iot.api.datastore.DatastoreApp;
import uk.nominet.iot.test.utils.RetryRunner;
import uk.nominet.iot.test.utils.RetryRunnerConfiguration;

import java.time.Instant;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(RetryRunner.class)
@RetryRunnerConfiguration(retryCount = 3, delayMilliseconds = 2000)
public class MetricsDatesITSuite {


    @BeforeClass
    public static void setUpClass() {
        RestAssured.basePath = String.format("/api/v%d/metrics", DatastoreApp.VERSION);
    }

    @Test
    public void step001_oneStreamAllDates() {
        JsonObject query = new JsonObject();
        JsonArray streams = new JsonArray();
        streams.add(DataStoreAPIIntegrationIT.DATE_STREAM_D1);
        query.add("streams", streams);


        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/dates").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response.dates.size()", equalTo(2)).
                body("response.dates[0]", equalTo("2018-10-01T00:00:00.000Z")).
                body("response.dates[1]", equalTo("2018-10-05T00:00:00.000Z"));
    }

    @Test
    public void step002_multipleStreamsAllDates() {
        JsonObject query = new JsonObject();
        JsonArray streams = new JsonArray();
        streams.add(DataStoreAPIIntegrationIT.DATE_STREAM_D1);
        streams.add(DataStoreAPIIntegrationIT.DATE_STREAM_D2);
        query.add("streams", streams);


        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/dates").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response.dates.size()", equalTo(4)).
                body("response.dates[0]", equalTo("2018-10-01T00:00:00.000Z")).
                body("response.dates[1]", equalTo("2018-10-05T00:00:00.000Z")).
                body("response.dates[2]", equalTo("2018-10-07T00:00:00.000Z")).
                body("response.dates[3]", equalTo("2018-10-09T00:00:00.000Z"));
    }


    @Test
    public void step003_multipleStreamsSpecificDates() {
        JsonObject query = new JsonObject();
        JsonArray streams = new JsonArray();
        streams.add(DataStoreAPIIntegrationIT.DATE_STREAM_D1);
        streams.add(DataStoreAPIIntegrationIT.DATE_STREAM_D2);
        query.add("streams", streams);
        query.addProperty("from", "2018-10-02T18:20:00.000Z");
        query.addProperty("to", Instant.now().toString());

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/dates").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response.dates.size()", equalTo(3)).
                body("response.dates[0]", equalTo("2018-10-05T00:00:00.000Z")).
                body("response.dates[1]", equalTo("2018-10-07T00:00:00.000Z")).
                body("response.dates[2]", equalTo("2018-10-09T00:00:00.000Z"));
    }


    @Test
    public void step004_fail_on_missing_stream() {
        JsonObject query = new JsonObject();
        query.addProperty("from", "2018-10-02T18:20:00.000Z");
        query.addProperty("to", Instant.now().toString());

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/dates").
                prettyPeek().
        then().
                statusCode(400).
                header("Content-Type", "application/json");
    }

}
