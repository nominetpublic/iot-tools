/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.integrationTesting;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.restassured.RestAssured;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import uk.nominet.iot.api.datastore.DatastoreApp;
import uk.nominet.iot.manager.UserContext;
import uk.nominet.iot.manager.registry.RegistryDeviceHandler;
import uk.nominet.iot.manager.registry.RegistryDeviceManager;

import java.time.Instant;

import java.util.Optional;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DeviceITSuite {
    private static String userSessionKey;
    private static Instant initialTime = Instant.now();
    public static String TEST_DATASTORE_STREAM_NAME = "device1.stream1";

    @BeforeClass
    public static void setUpClass() throws Exception {

        // Create test data
        // Set registry service to test user
        DataStoreAPIIntegrationIT.initialiseRegistryService("datastore_agent", "datastore_agent");


        RegistryDeviceManager deviceManager =
                RegistryDeviceManager.createManager(new UserContext("test").setPassword("test"));

        for (int i = 0; i < 50; i++) {
            RegistryDeviceHandler device = deviceManager.create(Optional.of(String.format("device.%d", i)),
                                                                Optional.of(String.format("device.%d", i)),
                                                                Optional.of("deviceType"));

            JsonParser parser = new JsonParser();
            JsonObject metadata = parser.parse("{\"geometry\": {\n" +
                    "              \"type\": \"Point\",\n" +
                    "              \"coordinates\": [\n" +
                    "                -1.573276,\n" +
                    "                5.149294\n" +
                    "              ]\n" +
                    "            }, \"type\": \"foo\"}").getAsJsonObject();

            device.updateMetadata(metadata);

            // Index device to elasticsearch
            device.index();
        }

        Thread.sleep(1500); //Wait for elasticsearch to refresh the index

        //Set  registry service to datastore agent
        DataStoreAPIIntegrationIT.initialiseRegistryService("datastore_agent", "datastore_agent");
        userSessionKey = DataStoreAPIIntegrationIT.getUserSession("test", "test");
        RestAssured.basePath = String.format("/api/v%d/device", DatastoreApp.VERSION);
    }


    @Test
    public void step01_canGetStuffFromGeo() {
        given().
                header("X-Introspective-Session-Key", userSessionKey).
        when().
                get("/facets").
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response.facets[0].options.size()", equalTo(1));
    }
}
