/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.integrationTesting;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.restassured.RestAssured;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import uk.nominet.iot.api.datastore.DatastoreApp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MetricsGeoITSuite {

    @BeforeClass
    public static void setUpClass() {
        RestAssured.basePath = String.format("/api/v%d/metrics", DatastoreApp.VERSION);
    }


    @Test
    public void step01_canGetLocationPoints() {
        JsonArray streams = new JsonArray();
        streams.add(DataStoreAPIIntegrationIT.METRIC_LATLONG);

        JsonObject query = new JsonObject();
        query.add("streams", streams);
        query.addProperty("to", "now");
        query.addProperty("size", 5);
        query.addProperty("from", Instant.now().minus(10, ChronoUnit.DAYS).toString());
        query.addProperty("geoOutput", "points");

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/geo").
                prettyPeek().
        then().
                statusCode(200).

                header("Content-Type", "application/json").
                body("result", equalTo("ok"));

    }

}
