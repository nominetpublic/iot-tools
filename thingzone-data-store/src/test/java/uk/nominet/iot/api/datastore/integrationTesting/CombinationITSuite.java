/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.integrationTesting;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.restassured.RestAssured;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import uk.nominet.iot.api.datastore.DatastoreApp;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.test.utils.RetryRunner;
import uk.nominet.iot.test.utils.RetryRunnerConfiguration;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(RetryRunner.class)
@RetryRunnerConfiguration(retryCount = 3, delayMilliseconds = 2000)
public class CombinationITSuite {
    private final static JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();




    @BeforeClass
    public static void setUpClass() {
        RestAssured.basePath = String.format("/api/v%d/combination", DatastoreApp.VERSION);
    }

    @Test
    public void step001_canGetFullList() {

        //Primary
        JsonObject metricQuery = new JsonObject();
        JsonArray streams = new JsonArray();
        streams.add(DataStoreAPIIntegrationIT.METRIC_LATLONG);
        metricQuery.add("streams", streams);
        //metricQuery.addProperty("size", 10000);
        metricQuery.addProperty("size", 20);
        metricQuery.addProperty("time", Instant.now().toString());
        JsonObject primary = new JsonObject();
        primary.add("metricsListQuery", metricQuery);

        //Secondary 1
        JsonArray streams1 = new JsonArray();
        streams1.add(DataStoreAPIIntegrationIT.METRIC_INT_STREAM);

        JsonObject metricQuery1 = new JsonObject();
        metricQuery1.add("streams", streams1);
        metricQuery1.addProperty("size", 20);

        JsonObject metricQueryWrapper1 = new JsonObject();
        metricQueryWrapper1.add("metricsListQuery", metricQuery1);
        metricQueryWrapper1.addProperty("streamName", "MetricInt");

        //Secondary 2
        JsonArray streams2 = new JsonArray();
        streams2.add(DataStoreAPIIntegrationIT.METRIC_STRING_STREAM);

        JsonObject metricQuery2 = new JsonObject();
        metricQuery2.add("streams", streams2);
        metricQuery2.addProperty("size", 20);

        JsonObject metricQueryWrapper2 = new JsonObject();
        metricQueryWrapper2.add("metricsListQuery", metricQuery2);
        metricQueryWrapper2.addProperty("streamName", "MetricString");

        JsonArray secondary = new JsonArray();
        secondary.add(metricQueryWrapper1);
        secondary.add(metricQueryWrapper2);


        JsonObject query = new JsonObject();
        query.add("primary", primary);
        query.add("secondary", secondary);

        System.out.println("Query: " + query);

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/list").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].values.size()", equalTo(20));
    }



    @Test
    public void step002_canGetFullGeo() {

        //Primary
        JsonObject metricQuery = new JsonObject();
        JsonArray streams = new JsonArray();
        streams.add(DataStoreAPIIntegrationIT.METRIC_LATLONG);
        metricQuery.add("streams", streams);
        //metricQuery.addProperty("size", 10000);
        metricQuery.addProperty("time", "now");
        metricQuery.addProperty("timeLimit", "1d");
        metricQuery.addProperty("size", 20);
        JsonObject primary = new JsonObject();
        primary.add("metricsGeoQuery", metricQuery);

        //Secondary 1
        JsonArray streams1 = new JsonArray();
        streams1.add(DataStoreAPIIntegrationIT.METRIC_INT_STREAM);

        JsonObject metricQuery1 = new JsonObject();
        metricQuery1.add("streams", streams1);
        metricQuery1.addProperty("to", "now");
        metricQuery1.addProperty("size", 20);
        metricQuery1.addProperty("from", Instant.now().minus(10, ChronoUnit.DAYS).toString());

        JsonObject metricQueryWrapper1 = new JsonObject();
        metricQueryWrapper1.add("metricsListQuery", metricQuery1);
        metricQueryWrapper1.addProperty("streamName", "MetricInt");

        //Secondary 2
        JsonArray streams2 = new JsonArray();
        streams2.add(DataStoreAPIIntegrationIT.METRIC_STRING_STREAM);

        JsonObject metricQuery2 = new JsonObject();
        metricQuery2.add("streams", streams2);
        metricQuery2.addProperty("to", "now");
        metricQuery2.addProperty("size", 20);
        metricQuery2.addProperty("from", Instant.now().minus(10, ChronoUnit.DAYS).toString());

        JsonObject metricQueryWrapper2 = new JsonObject();
        metricQueryWrapper2.add("metricsListQuery", metricQuery2);
        metricQueryWrapper2.addProperty("streamName", "MetricString");

        JsonArray secondary = new JsonArray();
        secondary.add(metricQueryWrapper1);
        secondary.add(metricQueryWrapper2);



        JsonObject query = new JsonObject();
        query.add("primary", primary);
        query.add("secondary", secondary);

        System.out.println("Query: " + query);


        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/geo").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].geojson.features.size()", equalTo(20));
    }



    @Test
    public void step003_combinationListPipelineQuery() {
        String query = "{\n" +
                       "  \"intermediate\":true,\n" +
                       "  \"deviceQuery\": { " +
                       "      \"keyword\": {\n" +
                       "      \"values\": [\"device1\"]\n" +
                       "    }" +
                       "  },\n" +
                       "  \"streamListQuery\":{\n" +
                       "    \"name\":\"streamMetricLatLong\" \n" +
                       "  },\n" +
                       "  \"combinationListQuery\":{\n" +
                       "    \"primary\":{\n" +
                       "      \"metricsListQuery\":{\n" +
                       "        \"size\":20,\n" +
                       "        \"time\":\"" + Instant.now().toString()+ "\"\n" +
                       "      }\n" +
                       "    },\n" +
                       "    \"secondary\":[\n" +
                       "      {\n" +
                       "        \"metricsListQuery\":{\n" +
                       "          \"size\":20,\n" +
                       "        },\n" +
                       "        \"streamName\":\"streamMetricInt\"\n" +
                       "      },\n" +
                       "      {\n" +
                       "        \"metricsListQuery\":{\n" +
                       "          \"size\":20,\n" +
                       "        },\n" +
                       "        \"streamName\":\"streamMetricString\"\n" +
                       "      }\n" +
                       "    ]\n" +
                       "  }\n" +
                       "}";

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query).
        when().
                post("/query/list").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].values.size()", equalTo(20));
    }

    @Test
    public void step004_combinationGeoPipelineQuery() {
        String query = "{\n" +
                       "  \"intermediate\":true,\n" +
                       "  \"deviceQuery\": { " +
                       "      \"keyword\": {\n" +
                       "      \"values\": [\"device1\"]\n" +
                       "    }" +
                       "  },\n" +
                       "  \"streamListQuery\":{\n" +
                       "    \"name\":\"streamMetricLatLong\" \n" +
                       "  },\n" +
                       "  \"combinationGeoQuery\":{\n" +
                       "    \"primary\":{\n" +
                       "      \"metricsGeoQuery\":{\n" +
                       "        \"size\":20,\n" +
                       "        \"from\":\"" + Instant.now().toString()+ "\"\n" +
                       "      }\n" +
                       "    },\n" +
                       "    \"secondary\":[\n" +
                       "      {\n" +
                       "        \"metricsListQuery\":{\n" +
                       "          \"size\":20,\n" +
                       "        },\n" +
                       "        \"streamName\":\"streamMetricInt\"\n" +
                       "      },\n" +
                       "      {\n" +
                       "        \"metricsListQuery\":{\n" +
                       "          \"size\":20,\n" +
                       "        },\n" +
                       "        \"streamName\":\"streamMetricString\"\n" +
                       "      }\n" +
                       "    ]\n" +
                       "  }\n" +
                       "}";

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query).
        when().
                post("/query/geo").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].geojson.features.size()", equalTo(20));
    }





}
