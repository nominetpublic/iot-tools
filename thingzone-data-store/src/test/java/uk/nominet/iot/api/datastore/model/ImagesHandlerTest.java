/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import org.junit.BeforeClass;
import org.junit.Test;
import uk.nominet.iot.model.timeseries.Image;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ImagesHandlerTest {

    static Instant timestamp = Instant.parse("2018-12-18T14:09:42.595Z");

    private static final List<Image> testImages = new ArrayList<>();
    private static Instant timeForSeries = Instant.parse("2018-12-18T14:09:42.595Z");

    @BeforeClass
    public static void setUpClass() {
        testImages.add(createImageObject(decrease(10, ChronoUnit.SECONDS)));
        testImages.add(createImageObject(decrease(10, ChronoUnit.SECONDS)));
        testImages.add(createImageObject(decrease(10, ChronoUnit.SECONDS)));
        testImages.add(createImageObject(decrease(1, ChronoUnit.SECONDS)));
        testImages.add(createImageObject(decrease(1, ChronoUnit.SECONDS)));
        testImages.add(createImageObject(decrease(1, ChronoUnit.SECONDS)));
        testImages.add(createImageObject(decrease(10, ChronoUnit.DAYS)));
        testImages.add(createImageObject(decrease(10, ChronoUnit.SECONDS)));
        testImages.add(createImageObject(decrease(10, ChronoUnit.SECONDS)));
        testImages.add(createImageObject(decrease(10, ChronoUnit.SECONDS)));
        testImages.add(createImageObject(decrease(1, ChronoUnit.SECONDS)));
        testImages.add(createImageObject(decrease(1, ChronoUnit.SECONDS)));
        testImages.add(createImageObject(decrease(1, ChronoUnit.SECONDS)));
        testImages.add(createImageObject(decrease(1, ChronoUnit.SECONDS)));
        testImages.add(createImageObject(decrease(90, ChronoUnit.DAYS)));
        testImages.add(createImageObject(decrease(10, ChronoUnit.SECONDS)));
        testImages.add(createImageObject(decrease(10, ChronoUnit.SECONDS)));
        testImages.add(createImageObject(decrease(10, ChronoUnit.SECONDS)));
        testImages.add(createImageObject(decrease(5, ChronoUnit.DAYS)));
        testImages.add(createImageObject(decrease(5, ChronoUnit.DAYS)));
    }

    private static Instant decrease(long amount, TemporalUnit unit) {
        timeForSeries = timeForSeries.minus(amount, unit);
        return timeForSeries;
    }

    private static Image createImageObject(Instant timestamp) {
        Image image = new Image();
        image.setTimestamp(timestamp);
        image.setStreamKey("stream1");
        image.setImageRef("REF " + timestamp);
        return image;
    }



    @Test
    public void resultHandlerNoBurstNoGroup() {
        ImagesHandler resultHandler = new ImagesHandler(null);
        testImages.forEach(image -> {
            try {
                resultHandler.put(image);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });


        JsonElement result = resultHandler.toJsonElement();

        JsonArray imageBlocks = result.getAsJsonArray();

        assertEquals(20, imageBlocks.size());

    }


    @Test
    public void resultHandlerBurst() {
        ImagesHandler resultHandler = new ImagesHandler(1000);
        testImages.forEach(image -> {
            try {
                resultHandler.put(image);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });


        JsonElement result = resultHandler.toJsonElement();

        JsonArray imageBlocks = result.getAsJsonArray();

        System.out.println("Block " + imageBlocks);


        assertEquals(13, imageBlocks.size());
        assertEquals(4, imageBlocks.get(2).getAsJsonObject().getAsJsonArray("images").size());
        assertEquals(5, imageBlocks.get(6).getAsJsonObject().getAsJsonArray("images").size());


    }


}
