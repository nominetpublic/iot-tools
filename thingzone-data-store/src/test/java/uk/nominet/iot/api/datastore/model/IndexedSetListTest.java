/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.model;

import org.javers.common.collections.Sets;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class IndexedSetListTest {
    private IndexedSetList<Integer> emptySetList;
    private IndexedSetList<Integer> orderedSetList;
    private IndexedSetList<Integer> unorderedDuplicatedSetList;
    private final IndexedSetList[] lists = new IndexedSetList[3];
    private final Integer[] ordered = new Integer[]{1, 2, 3, 4};
    // This list should not contain every value in the above list, and should contain repeats
    private final Integer[] unorderedDuplicated = new Integer[]{1, 6, 1, 2, 9, 0, 2, 2, 7, 10, 1, 5, 7, 4, 1};
    // This list should correspond with the above list, but only keeping the first occurrence of repeated values
    private final Integer[] unorderedDeduplicated = new Integer[]{1, 6, 2, 9, 0, 7, 10, 5, 4};

    @Before
    public void setUp() {
        emptySetList = new IndexedSetList<>();
        orderedSetList = new IndexedSetList<>();
        Collections.addAll(orderedSetList, ordered);
        unorderedDuplicatedSetList = new IndexedSetList<>();
        Collections.addAll(unorderedDuplicatedSetList, unorderedDuplicated);
        lists[0] = emptySetList;
        lists[1] = orderedSetList;
        lists[2] = unorderedDuplicatedSetList;
    }

    @Test
    public void size() {
        assertEquals(0, emptySetList.size());
        assertEquals(ordered.length, orderedSetList.size());
        assertEquals(Sets.asSet(unorderedDuplicated).size(), unorderedDuplicatedSetList.size());
    }

    @Test
    public void isEmpty() {
        assertTrue(emptySetList.isEmpty());
        assertFalse(orderedSetList.isEmpty());
        assertFalse(unorderedDuplicatedSetList.isEmpty());
    }

    @Test
    public void contains() {
        for(Integer i : ordered) {
            assertFalse(emptySetList.contains(i));
            assertTrue(orderedSetList.contains(i));
        }
        for(Integer i : unorderedDuplicated) {
            assertFalse(emptySetList.contains(i));
            assertTrue(unorderedDuplicatedSetList.contains(i));
        }
    }

    @Test
    public void iterator() {
        assertNotNull(emptySetList.iterator());
        assertFalse(emptySetList.iterator().hasNext());
        assertNotNull(orderedSetList.iterator());
        Iterator<Integer> iterator = orderedSetList.iterator();
        int i = 0;
        while(iterator.hasNext()) {
            assertEquals(ordered[i], iterator.next());
            i++;
        }
        i = 0;
        iterator = unorderedDuplicatedSetList.iterator();
        while(iterator.hasNext()) {
            assertEquals(unorderedDeduplicated[i], iterator.next());
            i++;
        }
    }

    @Test
    public void get() {
        boolean caught = false;
        try {
            //noinspection ResultOfMethodCallIgnored
            emptySetList.get(0);
            fail("No exception thrown when attempting to access the empty set list");
        } catch(IndexOutOfBoundsException ex) {
            caught = true;
        } catch(Exception e) {
            e.printStackTrace();
            fail("Unexpected exception thrown by out of bounds access");
        }
        assertTrue(caught);
        for(int i = 0; i < ordered.length; i++) {
            assertEquals(ordered[i], orderedSetList.get(i));
        }

        for(int i = 0; i < unorderedDeduplicated.length; i++) {
            assertEquals(unorderedDeduplicated[i], unorderedDuplicatedSetList.get(i));
        }
    }

    @Test
    public void set() {
        for(IndexedSetList list: lists) {
            boolean caught = false;
            try {
                //noinspection unchecked
                list.set(0, 1);
            } catch (UnsupportedOperationException e) {
                caught = true;
            } catch (Exception e) {
                fail("Unexpected exception thrown: " + e.toString());
            }
            assertTrue(caught);
        }
    }

    @Test
    public void add() {
        Integer value = 1;
        assertTrue(emptySetList.isEmpty());
        emptySetList.add(value);
        assertFalse(emptySetList.isEmpty());
        assertEquals(value, emptySetList.get(0));
        // Test set behaviour
        assertTrue(orderedSetList.contains(value));
        orderedSetList.add(value);
        assertEquals(ordered.length, orderedSetList.size());
        assertEquals(ordered[0], orderedSetList.get(0));
    }

    @Test
    public void remove() {
        boolean caught = false;
        try {
            emptySetList.remove(0);
        } catch(IndexOutOfBoundsException e) {
            caught = true;
        }
        assertTrue(caught);
        orderedSetList.remove(0);
        assertEquals(ordered.length - 1, orderedSetList.size());
        assertFalse(orderedSetList.contains(ordered[0]));
        unorderedDuplicatedSetList.remove(0);
        assertFalse(unorderedDuplicatedSetList.contains(unorderedDuplicated[0]));
    }

    @Test
    public void remove1() {
        Integer value = 1;
        Integer badValue = 97;
        assertFalse(orderedSetList.contains(badValue));
        assertFalse(unorderedDuplicatedSetList.contains(badValue));
        assertFalse(emptySetList.remove(value));
        assertTrue(orderedSetList.remove(ordered[0]));
        assertEquals(ordered.length - 1, orderedSetList.size());
        assertFalse(orderedSetList.contains(ordered[0]));
        assertTrue(unorderedDuplicatedSetList.remove(value));
        assertFalse(unorderedDuplicatedSetList.contains(unorderedDuplicated[0]));
        assertFalse(orderedSetList.remove(badValue));
        assertFalse(unorderedDuplicatedSetList.remove(badValue));
    }

    @Test
    public void indexOf() {
        for(int i = 0; i < ordered.length; i++) {
            assertEquals(i, orderedSetList.indexOf(ordered[i]));
            assertEquals(-1, emptySetList.indexOf(ordered[i]));
        }
        for(int i = 0; i < unorderedDeduplicated.length; i++) {
            assertEquals(i, unorderedDuplicatedSetList.indexOf(unorderedDeduplicated[i]));
            assertEquals(-1, emptySetList.indexOf(unorderedDeduplicated[i]));
        }
    }

    @Test
    public void lastIndexOf() {
        for(int i = 0; i < ordered.length; i++) {
            assertEquals(i, orderedSetList.lastIndexOf(ordered[i]));
            assertEquals(-1, emptySetList.lastIndexOf(ordered[i]));
        }
        for(int i = 0; i < unorderedDeduplicated.length; i++) {
            assertEquals(i, unorderedDuplicatedSetList.lastIndexOf(unorderedDeduplicated[i]));
            assertEquals(-1, emptySetList.lastIndexOf(unorderedDeduplicated[i]));
        }
    }

    @Test
    public void listIterator() {
        assertNotNull(emptySetList.listIterator());
        assertFalse(emptySetList.listIterator().hasNext());
        assertFalse(emptySetList.listIterator().hasPrevious());
        ListIterator<Integer> iterator = orderedSetList.listIterator();
        int i = 0;
        while(iterator.hasNext()) {
            assertEquals(ordered[i], iterator.next());
            i++;
        }
        i = 0;
        iterator = unorderedDuplicatedSetList.listIterator();
        while(iterator.hasNext()) {
            assertEquals(unorderedDeduplicated[i], iterator.next());
            i++;
        }
    }

    @Test
    public void listIterator1() {
        boolean caught = false;
        try {
            emptySetList.listIterator(1);
        } catch(IndexOutOfBoundsException e) {
            caught = true;
        } catch(Exception e) {
            fail("Unexpected exception thrown: " + e.toString());
        }
        assertTrue(caught);
        int i = orderedSetList.size();
        ListIterator<Integer> iterator = orderedSetList.listIterator(i);
        while(iterator.hasPrevious()) {
            assertEquals(ordered[i-1], iterator.previous());
            i--;
        }
        i = unorderedDuplicatedSetList.size();
        iterator = unorderedDuplicatedSetList.listIterator(i);
        while(iterator.hasPrevious()) {
            assertEquals(unorderedDeduplicated[i-1], iterator.previous());
            i--;
        }
    }

    @Test
    public void subList() {
        assertTrue(emptySetList.subList(0, 0).isEmpty());
        boolean caught = false;
        try {
            emptySetList.subList(0, 1);
        } catch(IndexOutOfBoundsException e) {
            caught = true;
        } catch(Exception e) {
            fail("Unexpected exception thrown: " + e.toString());
        }
        assertTrue(caught);
        for(int i = 1; i < orderedSetList.size(); i++) {
            for(int j = 0; j < i; j++) {
                List<Integer> subList = orderedSetList.subList(j, i);
                assertEquals(i - j, subList.size());
            }
        }
        for(int i = 1; i < unorderedDuplicatedSetList.size(); i++) {
            for(int j = 0; j < i; j++) {
                List<Integer> subList = unorderedDuplicatedSetList.subList(j, i);
                assertEquals(i - j, subList.size());
            }
        }
    }

    @Test
    public void containsAll() {
        assertFalse(emptySetList.containsAll(Arrays.asList(ordered)));
        assertFalse(emptySetList.containsAll(Arrays.asList(unorderedDuplicated)));
        assertTrue(orderedSetList.containsAll(Arrays.asList(ordered)));
        assertFalse(orderedSetList.containsAll(Arrays.asList(unorderedDuplicated)));
        assertFalse(unorderedDuplicatedSetList.containsAll(Arrays.asList(ordered)));
        assertTrue(unorderedDuplicatedSetList.containsAll(Arrays.asList(unorderedDuplicated)));
    }

    @Test
    public void addAll() {
        emptySetList.addAll(Arrays.asList(ordered));
        assertTrue(emptySetList.containsAll(Arrays.asList(ordered)));
    }

    @Test
    public void addAll1() {
        for(IndexedSetList list: lists) {
            boolean caught = false;
            try {
                //noinspection unchecked
                assertFalse(list.addAll(0, Arrays.asList(ordered)));
            } catch(UnsupportedOperationException e) {
                caught = true;
            } catch(Exception e) {
                fail("Unexpected exception thrown: " + e.toString());
            }
            assertTrue(caught);
        }
    }

    @Test
    public void removeAll() {
        assertTrue(orderedSetList.removeAll(Arrays.asList(ordered)));
        assertTrue(orderedSetList.isEmpty());
        assertTrue(unorderedDuplicatedSetList.removeAll(Arrays.asList(unorderedDuplicated)));
        assertTrue(unorderedDuplicatedSetList.isEmpty());
        assertFalse(emptySetList.removeAll(Arrays.asList(ordered)));
        assertFalse(emptySetList.removeAll(Arrays.asList(unorderedDuplicated)));
    }

    @Test
    public void retainAll() {
        assertTrue(orderedSetList.retainAll(Arrays.asList(unorderedDeduplicated)));
        assertTrue(unorderedDuplicatedSetList.retainAll(Arrays.asList(ordered)));
        assertFalse(emptySetList.retainAll(Arrays.asList(ordered)));
        assertFalse(emptySetList.retainAll(Arrays.asList(unorderedDeduplicated)));
    }

    @Test
    public void clear() {
        try {
            emptySetList.clear();
            unorderedDuplicatedSetList.clear();
            orderedSetList.clear();
        } catch(Exception e) {
            fail("Unexpected exception thrown: " + e.toString());
        }
    }

    @Test
    public void toArray() {
        assertArrayEquals(new Object[]{}, emptySetList.toArray());
        assertArrayEquals(ordered, orderedSetList.toArray());
        assertArrayEquals(unorderedDeduplicated, unorderedDuplicatedSetList.toArray());
    }
    @Test
    public void add1() {
        for(IndexedSetList list: lists) {
            boolean caught = false;
            try {
                //noinspection unchecked
                list.add(0, 97);
            } catch(UnsupportedOperationException e) {
                caught = true;
            } catch(Exception e) {
                fail("Unexpected exception thrown: " + e.toString());
            }
            assertTrue(caught);
        }
    }

    @Test
    public void spliterator() {
        for(IndexedSetList list : lists) {
            assertNotNull(list.spliterator());
        }
    }

}
