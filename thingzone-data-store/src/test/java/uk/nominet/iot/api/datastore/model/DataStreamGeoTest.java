/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.model;

import com.github.filosganga.geogson.model.Feature;
import com.github.filosganga.geogson.model.Point;
import org.junit.Test;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.geo.TimeseriesFeatureCollection;
import uk.nominet.iot.model.timeseries.Event;
import uk.nominet.iot.model.timeseries.TimeseriesGeoFeature;

import java.time.Instant;
import java.util.UUID;

public class DataStreamGeoTest {
 private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    @Test
    public void canBuildGeojson() throws Exception {

        DataStreamGeo<Event> alertDataStreamGeo = new DataStreamGeo<>();
        alertDataStreamGeo.setStreamKey("stream 1");
        alertDataStreamGeo.setName("Datastream Geo API example");
        alertDataStreamGeo.setMerged(true);

        Event alert = new Event();
        alert.setStreamKey("stream1");
        alert.setTimestamp(Instant.now());
        alert.setId(UUID.randomUUID());
        alert.setDescription("some alert");
        alert.setLocation(Point.from(0.0,0.0));

        Feature feature = Feature.builder().withGeometry(alert.getLocation()).build();


        TimeseriesFeatureCollection<Event> timeseriesFeatureList = new TimeseriesFeatureCollection<>();
        TimeseriesGeoFeature<Event> alertTimeseriesFeature = new TimeseriesGeoFeature<>();
        alertTimeseriesFeature.setPoint(alert);
        alertTimeseriesFeature.setFeature(feature);
        timeseriesFeatureList.add(alertTimeseriesFeature);

        alertDataStreamGeo.setFeatures(timeseriesFeatureList);

        String json = jsonStringSerialiser.writeObject(alertDataStreamGeo);

        System.out.println("Json: " + json);


    }
}
