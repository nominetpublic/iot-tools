/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.integrationTesting;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.restassured.RestAssured;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import uk.nominet.iot.api.datastore.DatastoreApp;
import java.time.Instant;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MetricsAggITSuite {

    @BeforeClass
    public static void setUpClass() {
        RestAssured.basePath = String.format("/api/v%d/metrics", DatastoreApp.VERSION);
    }

    @Test
    public void step01_canGetSimpleAggregation() {
        JsonArray streams = new JsonArray();
        streams.add(DataStoreAPIIntegrationIT.METRIC_DOUBLE_STREAM);

        JsonObject query = new JsonObject();
        query.add("streams", streams);
        query.addProperty("metricType", "value_double");
        query.addProperty("time", Instant.now().toString());
        query.addProperty("timeLimit", "10d");


        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/agg").
                prettyPeek().
        then().
                statusCode(200).

                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].merged", equalTo(true)).
                body("response[0].streamKey", equalTo("device1.stream.metric.double")).
                body("response[0].values[0].count", equalTo(960));
    }

    @Test
    public void step02_canGetMultipleAggregatedValues() {
        JsonArray streams = new JsonArray();
        streams.add(DataStoreAPIIntegrationIT.METRIC_DOUBLE_STREAM);

        JsonObject query = new JsonObject();
        query.add("streams", streams);
        query.addProperty("metricType", "value_double");
        query.addProperty("time", Instant.now().toString());
        query.addProperty("timeLimit", "10d");
        query.addProperty("interval", "1d");


        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/agg").
                prettyPeek().
        then().
                statusCode(200).

                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].merged", equalTo(true)).
                body("response[0].streamKey", equalTo("device1.stream.metric.double")).
                body("response[0].values", hasSize(11));
    }


     @Test
    public void step03_canGetMultipleAggregatedValuesGroupByStream() {
        JsonArray streams = new JsonArray();
        streams.add(DataStoreAPIIntegrationIT.METRIC_DOUBLE_STREAM);

        JsonObject query = new JsonObject();
        query.add("streams", streams);
        query.addProperty("metricType", "value_double");
        query.addProperty("time", Instant.now().toString());
        query.addProperty("timeLimit", "10d");
        query.addProperty("interval", "1d");
        query.addProperty("groupByStream", true);



        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/agg").
                prettyPeek().
        then().
                statusCode(200).

                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].merged", equalTo(false)).
                body("response[0].streamKey", equalTo("device1.stream.metric.double")).
                body("response[0].values", hasSize(11));
    }

}
