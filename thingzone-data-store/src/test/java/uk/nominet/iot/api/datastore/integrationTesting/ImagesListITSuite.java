/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.integrationTesting;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.restassured.RestAssured;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import uk.nominet.iot.api.datastore.DatastoreApp;
import uk.nominet.iot.test.utils.RetryRunner;
import uk.nominet.iot.test.utils.RetryRunnerConfiguration;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(RetryRunner.class)
@RetryRunnerConfiguration(retryCount = 3, delayMilliseconds = 2000)
public class ImagesListITSuite {
    @BeforeClass
    public static void setUpClass() {
        RestAssured.basePath = String.format("/api/v%d/images", DatastoreApp.VERSION);
    }

    @Test
    public void step01_canGetImagesFromToTime() {
        JsonArray streams = new JsonArray();
        streams.add(DataStoreAPIIntegrationIT.IMAGE_STREAM);

        JsonObject query = new JsonObject();
        query.add("streams", streams);
        query.addProperty("time", Instant.now().minus(10, ChronoUnit.DAYS).toString());


        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/list").
                prettyPeek().
        then().
                statusCode(200).

                header("Content-Type", "application/json").
                body("result", equalTo("ok"));
    }

        @Test
    public void step01_canGetImagesFromToTimeBurst() {
        JsonArray streams = new JsonArray();
        streams.add(DataStoreAPIIntegrationIT.IMAGE_STREAM);

        JsonObject query = new JsonObject();
        query.add("streams", streams);
        query.addProperty("time", Instant.now().toString());
        query.addProperty("burstInterval", 2000);


        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/list").
                prettyPeek().
        then().
                statusCode(200).

                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].values", hasSize(241));
    }


}
