/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.integrationTesting;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.restassured.RestAssured;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import uk.nominet.iot.api.datastore.DatastoreApp;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class StatisticITSuite {
    @BeforeClass
    public static void setUpClass() {
        RestAssured.basePath = String.format("/api/v%d/stat", DatastoreApp.VERSION);
    }

    @Test
    public void step001_oneVehicle() {
        JsonObject query = new JsonObject();
        JsonArray streams = new JsonArray();
        streams.add(DataStoreAPIIntegrationIT.DATE_STREAM_D1);
        query.add("streams", streams);


        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/vehicle").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok"));

    }

}
