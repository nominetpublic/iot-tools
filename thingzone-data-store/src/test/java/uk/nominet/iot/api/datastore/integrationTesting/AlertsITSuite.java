/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.api.datastore.integrationTesting;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.restassured.RestAssured;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import uk.nominet.iot.api.datastore.DatastoreApp;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.Feedback;
import uk.nominet.iot.test.utils.RetryRunner;
import uk.nominet.iot.test.utils.RetryRunnerConfiguration;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(RetryRunner.class)
@RetryRunnerConfiguration(retryCount = 3, delayMilliseconds = 2000)
public class AlertsITSuite {
    private final static JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();



    @SafeVarargs
    private final JsonArray buildBoundingBox(Pair<Double, Double>... coordinates) {
        JsonArray boundingPolygons = new JsonArray();
        JsonArray box = new JsonArray();
        boundingPolygons.add(box);

        for (Pair<Double, Double> coordinate : coordinates) {
            JsonArray coord = new JsonArray();
            coord.add(coordinate.getLeft());
            coord.add(coordinate.getRight());
            box.add(coord);
        }

        return boundingPolygons;

    }

    @BeforeClass
    public static void setUpClass() {
        RestAssured.basePath = String.format("/api/v%d/event", DatastoreApp.VERSION);
    }

    @Test
    public void step001_canGetFullList() {
        JsonObject query = new JsonObject();
        JsonArray eventType = new JsonArray();
        eventType.add("ALERT");
        query.add("eventType", eventType);
        query.addProperty("size", 1000);

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/list").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].total", equalTo(25)).
                body("response[0].values.size()", equalTo(25)).
                body("response[0].values[0].name", equalTo("Alert 24"));
    }

    @Test
    public void step002_canGetListFrom() {
        JsonObject query = new JsonObject();
        JsonArray eventType = new JsonArray();
        eventType.add("ALERT");
        query.add("eventType", eventType);
        query.addProperty("time", DataStoreAPIIntegrationIT
                                          .initialTime.minus(10, ChronoUnit.HOURS).toString());


        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/list").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].total", equalTo(15)).
                body("response[0].values.size()", equalTo(15)).
                body("response[0].values[0].name", equalTo("Alert 14"));
    }


    @Test
    public void step003_canGetListFromTo() {
        JsonObject query = new JsonObject();
        JsonArray eventType = new JsonArray();
        eventType.add("ALERT");
        query.add("eventType", eventType);
        query.addProperty("time", DataStoreAPIIntegrationIT
                                          .initialTime.minus(5, ChronoUnit.HOURS).toString());
        query.addProperty("timeLimit", "5h");


        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/list").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].total", equalTo(6)).
                body("response[0].values.size()", equalTo(6)).
                body("response[0].values[0].name", equalTo("Alert 19"));
    }

     @Test
    public void step004_canGetListFromToSize() {
        JsonObject query = new JsonObject();
         JsonArray eventType = new JsonArray();
         eventType.add("ALERT");
         query.add("eventType", eventType);
         query.addProperty("size", 1000);
        query.addProperty("time", DataStoreAPIIntegrationIT
                                          .initialTime.minus(10, ChronoUnit.HOURS).toString());
        query.addProperty("timeLimit", "4h");
        query.addProperty("size", 2);


        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/list").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].total", equalTo(5)).
                body("response[0].values.size()", equalTo(2)).
                body("response[0].values[0].name", equalTo("Alert 14")).
                body("response[0].values[1].name", equalTo("Alert 13"));
    }

     @Test
    public void step005_canGetListTo() {
        JsonObject query = new JsonObject();
         JsonArray eventType = new JsonArray();
         eventType.add("ALERT");
         query.add("eventType", eventType);
        query.addProperty("forward", false);
        query.addProperty("time", DataStoreAPIIntegrationIT
                                        .initialTime.minus(4, ChronoUnit.HOURS).toString());

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/list").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].total", equalTo(21)).
                body("response[0].values.size()", equalTo(21)).
                body("response[0].values[0].name", equalTo("Alert 20")).
                body("response[0].values[1].name", equalTo("Alert 19"));
    }

    @Test
    public void step006_canGetListFromSize() {
        JsonObject query = new JsonObject();
        JsonArray eventType = new JsonArray();
        eventType.add("ALERT");
        query.add("eventType", eventType);
        query.addProperty("forward", true);
        query.addProperty("time", DataStoreAPIIntegrationIT
                                          .initialTime.minus(8, ChronoUnit.HOURS).toString());
        query.addProperty("size", 2);

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/list").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].total", equalTo(9)).
                body("response[0].values.size()", equalTo(2)).
                body("response[0].values[0].name", equalTo("Alert 17")).
                body("response[0].values[1].name", equalTo("Alert 16"));
    }

    @Test
    public void step006_canGetListToSize() {
        JsonObject query = new JsonObject();
        JsonArray eventType = new JsonArray();
        eventType.add("ALERT");
        query.add("eventType", eventType);
        query.addProperty("forward", false);
        query.addProperty("time", DataStoreAPIIntegrationIT
                                        .initialTime.minus(4, ChronoUnit.HOURS).toString());
        query.addProperty("size", 2);

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/list").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].total", equalTo(21)).
                body("response[0].values.size()", equalTo(2)).
                body("response[0].values[0].name", equalTo("Alert 20")).
                body("response[0].values[1].name", equalTo("Alert 19"));
    }

    @Test
    public void step007_canGetListByType() {
        JsonObject query = new JsonObject();
        query.addProperty("size", 1000);
        JsonArray eventType = new JsonArray();
        eventType.add("ALERT");
        query.add("eventType", eventType);
        JsonArray types = new JsonArray();
        types.add("typeB");
        query.add("types", types);

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/list").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].total", equalTo(12)).
                body("response[0].values.size()", equalTo(12)).
                body("response[0].values[0].name", equalTo("Alert 23")).
                body("response[0].values[0].type", equalTo("typeB")).
                body("response[0].values[1].name", equalTo("Alert 21"));
    }

     @Test
    public void step008_canGetListBySeverityRange() {
        JsonObject query = new JsonObject();
         query.addProperty("size", 1000);
         JsonArray eventType = new JsonArray();
         eventType.add("ALERT");
         query.add("eventType", eventType);

        JsonArray range = new JsonArray();
        range.add(0.2);
        range.add(0.58);

        query.add("severity", range);

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/list").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].total", equalTo(13)).
                body("response[0].values.size()", equalTo(13)).
                body("response[0].values[0].name", equalTo("Alert 19")).
                body("response[0].values[1].name", equalTo("Alert 18"));
    }

    @Test
    public void step009_canGetListBySingleTag() {
        JsonObject query = new JsonObject();
        query.addProperty("size", 1000);
        JsonArray eventType = new JsonArray();
        eventType.add("ALERT");
        query.add("eventType", eventType);
        JsonArray tags = new JsonArray();
        tags.add("tag3");

        query.add("tags", tags);

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/list").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].total", equalTo(12)).
                body("response[0].values.size()", equalTo(12)).
                body("response[0].values[0].name", equalTo("Alert 23")).
                body("response[0].values[0].tags[0]", equalTo("tag3")).
                body("response[0].values[1].name", equalTo("Alert 21"));
    }

     @Test
    public void step010_canGetListBySingleTags1() {
        JsonObject query = new JsonObject();
         query.addProperty("size", 1000);
         JsonArray eventType = new JsonArray();
         eventType.add("ALERT");
         query.add("eventType", eventType);

        JsonArray tags = new JsonArray();
        tags.add("tag1");
        tags.add("tag2");

        query.add("tags", tags);

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/list").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].total", equalTo(13)).
                body("response[0].values.size()", equalTo(13)).
                body("response[0].values[0].name", equalTo("Alert 24")).
                body("response[0].values[1].name", equalTo("Alert 22"));
    }

     @Test
    public void step011_canGetListBySingleTags2() {
        JsonObject query = new JsonObject();
         query.addProperty("size", 1000);
         JsonArray eventType = new JsonArray();
         eventType.add("ALERT");
         query.add("eventType", eventType);

        JsonArray tags = new JsonArray();
        tags.add("tag1");
        tags.add("tag2");
        tags.add("tag3");

        query.add("tags", tags);

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/list").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].total", equalTo(25)).
                body("response[0].values.size()", equalTo(25));
    }

      @Test
    public void step012_canGetListByKeyword() {
        JsonObject query = new JsonObject();
          JsonArray eventType = new JsonArray();
          eventType.add("ALERT");
          query.add("eventType", eventType);
        JsonArray keywords = new JsonArray();
        keywords.add("23");

        query.add("keywords", keywords);

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/list").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].total", equalTo(1)).
                body("response[0].values.size()", equalTo(1)).
                body("response[0].values[0].name", equalTo("Alert 23"));
    }

      @Test
    public void step013_canGetListByText2() {
        JsonObject query = new JsonObject();
          JsonArray eventType = new JsonArray();
          eventType.add("ALERT");
          query.add("eventType", eventType);

        JsonArray keywords = new JsonArray();
        keywords.add("19");

        query.add("keywords", keywords);

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/list").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].total", equalTo(1)).
                body("response[0].values.size()", equalTo(1)).
                body("response[0].values[0].name", equalTo("Alert 19"));
    }

     @Test
    public void step014_geoQuery() {
        JsonObject query = new JsonObject();
        query.addProperty("size", 1000);
         JsonArray eventType = new JsonArray();
         eventType.add("ALERT");
         query.add("eventType", eventType);
         JsonArray boundingPolygons = buildBoundingBox(Pair.of(-1.210937,51.709533),
                                                      Pair.of(-1.256251,51.709533),
                                                      Pair.of(-1.256251,51.758903),
                                                      Pair.of(-1.210937,51.758903),
                                                      Pair.of(-1.210937,51.709533));

        query.add("boundingPolygons", boundingPolygons);

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/geo").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response.geojson.features.size()", equalTo(1)).
                body("response.geojson.features[0].properties.combinedPoints.size()", equalTo(9));
    }

     @Test
    public void step015_geoQueryNoBoundingBox() throws Exception {
        Thread.sleep(3000); //Wait for elasticsearch to index
        JsonObject query = new JsonObject();
         query.addProperty("size", 1000);
         JsonArray eventType = new JsonArray();
         eventType.add("ALERT");
         query.add("eventType", eventType);

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/geo").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response.geojson.features.size()", equalTo(2)).
                body("response.geojson.features[0].properties.combinedPoints.size", greaterThan(8));
    }


     @Test
    public void step016_geoQueryNoResults() {
        JsonObject query = new JsonObject();
         JsonArray eventType = new JsonArray();
         eventType.add("ALERT");
         query.add("eventType", eventType);

         JsonArray boundingPolygons = buildBoundingBox(Pair.of(-1.210937,51.709533),
                                                      Pair.of(-1.216251,51.709533),
                                                      Pair.of(-1.216251,51.718903),
                                                      Pair.of(-1.210937,51.718903),
                                                      Pair.of(-1.210937,51.709533));

        query.add("boundingPolygons", boundingPolygons);

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/geo").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response.geojson.features.size()", equalTo(0));
    }


    @Test
    public void step017_geoQueryWithType() {
        JsonObject query = new JsonObject();
        query.addProperty("size", 1000);
        JsonArray boundingPolygons = buildBoundingBox(Pair.of(-1.210937,51.709533),
                                                      Pair.of(-1.256251,51.709533),
                                                      Pair.of(-1.256251,51.758903),
                                                      Pair.of(-1.210937,51.758903),
                                                      Pair.of(-1.210937,51.709533));

        query.add("boundingPolygons", boundingPolygons);

        JsonArray types = new JsonArray();
        types.add("typeB");

        query.add("types", types);

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/geo").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response.geojson.features.size()", equalTo(1)).
                body("response.geojson.features[0].properties.combinedPoints.size()", equalTo(4));
    }

    @Test
    public void step018_geoQueryWithSeverity() {
        JsonObject query = new JsonObject();
        JsonArray eventType = new JsonArray();
        eventType.add("ALERT");
        query.add("eventType", eventType);
        query.addProperty("size", 1000);

        JsonArray boundingPolygons = buildBoundingBox(Pair.of(-1.210937,51.709533),
                                                      Pair.of(-1.256251,51.709533),
                                                      Pair.of(-1.256251,51.758903),
                                                      Pair.of(-1.210937,51.758903),
                                                      Pair.of(-1.210937,51.709533));

        query.add("boundingPolygons", boundingPolygons);

        JsonArray range = new JsonArray();
        range.add(0.2);
        range.add(0.58);

        query.add("severity", range);

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/geo").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response.geojson.features.size()", equalTo(1)).
                body("response.geojson.features[0].properties.combinedPoints.size()", equalTo(4));
    }

    @Test
    public void step019_addFeedbackToAlert() throws Exception {
        Feedback feedback = new Feedback();
        feedback.setUser(DataStoreAPIIntegrationIT.USERNAME);
        feedback.setType("system");
        feedback.setCategory("read");
        feedback.setId(UUID.randomUUID());
        feedback.setTimestamp(Instant.now());
        feedback.setDescription("user have seen alert");



        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(jsonStringSerialiser.writeObject(feedback)).
        when().
                put(String.format("/%s/%s/feedback",
                                  DataStoreAPIIntegrationIT.ALERT_STREAM,
                                  DataStoreAPIIntegrationIT.alertIds.get(DataStoreAPIIntegrationIT.alertIds.size() - 1)
                                                                    .toString())).
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response.name", equalTo("Alert 24")).
                body("response.feedback[0].user", equalTo("test")).
                body("response.feedback[0].type", equalTo("system")).
                body("response.feedback[0].category", equalTo("read"));
    }

    @Test
    public void step020_getListAndCheckIfRead() throws Exception {

        Thread.sleep(2000); // Wait for elasticsearch to index
        JsonObject query = new JsonObject();
        JsonArray eventType = new JsonArray();
        eventType.add("ALERT");
        query.add("eventType", eventType);
        query.addProperty("size", 2);
        query.addProperty("forward", false);

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/list").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].total", equalTo(25)).
                body("response[0].unread", equalTo(24)).
                body("response[0].values.size()", equalTo(2)).
                body("response[0].values[0].name", equalTo("Alert 24")).
                body("response[0].values[0].read", equalTo(true));
    }

    @Test
    public void step021_setStatus() {
        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
        when().
                put(String.format("/%s/%s/status/%s",
                                  DataStoreAPIIntegrationIT.ALERT_STREAM,
                                  DataStoreAPIIntegrationIT.alertIds.get(0).toString(),
                                  "closed")).
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response.status", equalTo("closed"));
    }

    @Test
    public void step022_getsFacets() throws Exception {
        Thread.sleep(2000); // Wait for elasticsearch to index

         given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
        when().
                get("/facets").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response.facets.tags.size()", equalTo(3)).
                body("response.facets.tags[0].id", equalTo("tag1")).
                body("response.facets.tags[0].value" , equalTo("tag1")).
                body("response.facets.tags[0].count", equalTo(13)).
                body("response.facets.tags[1].id", equalTo("tag2")).
                body("response.facets.tags[1].value" , equalTo("tag2")).
                body("response.facets.tags[1].count", equalTo(13)).
                body("response.facets.tags[2].id", equalTo("tag3")).
                body("response.facets.tags[2].value" , equalTo("tag3")).
                body("response.facets.tags[2].count", equalTo(12)).
                body("response.facets.types.size()", equalTo(2)).
                body("response.facets.types[0].id", equalTo("typeA")).
                body("response.facets.types[0].value" , equalTo("typeA")).
                body("response.facets.types[0].count", equalTo(13)).
                body("response.facets.types[1].id", equalTo("typeB")).
                body("response.facets.types[1].value" , equalTo("typeB")).
                body("response.facets.types[1].count", equalTo(12)).
                body("response.facets.status[0].id", equalTo("closed")).
                body("response.facets.status[0].value" , equalTo("closed")).
                body("response.facets.status[0].count", equalTo(1)).
                body("response.facets.devices[0].id", equalTo("device1")).
                body("response.facets.devices[0].value" , equalTo("device1")).
                body("response.facets.devices[0].count", equalTo(25)).
                body("response.facets.new[0].id", equalTo("new")).
                body("response.facets.new[0].value" , equalTo("new")).
                body("response.facets.new[0].count", equalTo(24));
    }

    @Test
    public void step023_getSingleAlertRead() {

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
        when().
                get(String.format("/%s/%s",
                                  DataStoreAPIIntegrationIT.ALERT_STREAM,
                                  DataStoreAPIIntegrationIT.alertIds.get(DataStoreAPIIntegrationIT.alertIds.size() - 1)
                                                                    .toString())).
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response.name", equalTo("Alert 24")).
                body("response.read", equalTo(true)).
                body("response.feedback[0].user", equalTo("test")).
                body("response.feedback[0].type", equalTo("system")).
                body("response.feedback[0].category", equalTo("read"));
    }

    @Test
    public void step024_getSingleAlertUnread() {

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
        when().
                get(String.format("/%s/%s",
                                  DataStoreAPIIntegrationIT.ALERT_STREAM,
                                  DataStoreAPIIntegrationIT.alertIds.get(DataStoreAPIIntegrationIT.alertIds.size() - 2)
                                                                    .toString())).
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response.name", equalTo("Alert 23")).
                body("response.read", equalTo(false));
    }

     @Test
    public void step025_geoQueryAfterFeedback() {
        JsonObject query = new JsonObject();
         query.addProperty("size", 1000);
         JsonArray eventType = new JsonArray();
         eventType.add("ALERT");
         query.add("eventType", eventType);

         JsonArray boundingPolygons = buildBoundingBox(Pair.of(-1.210937,51.709533),
                                                      Pair.of(-1.256251,51.709533),
                                                      Pair.of(-1.256251,51.758903),
                                                      Pair.of(-1.210937,51.758903),
                                                      Pair.of(-1.210937,51.709533));

        query.add("boundingPolygons", boundingPolygons);

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/geo").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response.geojson.features.size()", equalTo(1)).
                body("response.geojson.features[0].properties.combinedPoints.size()", equalTo(12));
    }

    @Test
    public void step026_canGetListByStatuses() {
        JsonObject query = new JsonObject();
        JsonArray eventType = new JsonArray();
        eventType.add("ALERT");
        query.add("eventType", eventType);
        JsonArray status = new JsonArray();
        status.add("closed");
        query.add("status", status);

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/list").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].total", equalTo(1)).
                body("response[0].values.size()", equalTo(1)).
                body("response[0].values[0].name", equalTo("Alert 0"));

    }

     @Test
    public void step026_canGetListByUnread() {
        JsonObject query = new JsonObject();
         JsonArray eventType = new JsonArray();
         eventType.add("ALERT");
         query.add("eventType", eventType);

        query.addProperty("new", true);
         query.addProperty("size", 1000);
        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/list").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].total", equalTo(24)).
                body("response[0].values.size()", equalTo(24));
    }

     @Test
    public void step027_canGetListByRead() {
        JsonObject query = new JsonObject();
         JsonArray eventType = new JsonArray();
         eventType.add("ALERT");
         query.add("eventType", eventType);

        query.addProperty("new", false);

        given().
                header("X-Introspective-Session-Key", DataStoreAPIIntegrationIT.userSessionKey).
                header("Content-Type", "application/json").
                body(query.toString()).
        when().
                post("/list").
                prettyPeek().
        then().
                statusCode(200).
                header("Content-Type", "application/json").
                body("result", equalTo("ok")).
                body("response[0].total", equalTo(1)).
                body("response[0].values.size()", equalTo(1));
    }

}
