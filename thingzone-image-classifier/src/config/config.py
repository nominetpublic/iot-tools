"""
Copyright 2020 Nominet UK

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


import configparser


def init(config_files):
    config = configparser.ConfigParser(allow_no_value=True)

    files = config.read(config_files)
    if len(files) != len(config_files):
        raise ValueError("Failed to open/find all config files")

    # global PATH_TO_LABELS
    # PATH_TO_LABELS = config['LABELS']['PATH_TO_LABELS']

    global DEFAULT_MODEL
    DEFAULT_MODEL = config['MODELS']['DEFAULT_MODEL']

    # global NIGHT_MODEL
    # NIGHT_MODEL = config['MODELS']['NIGHT_MODEL']
    #
    # global DAY_MODEL
    # DAY_MODEL = config['MODELS']['DAY_MODEL']
