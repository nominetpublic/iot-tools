"""
Copyright 2020 Nominet UK

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


from flask import request, Flask, jsonify, make_response, abort

import src.model.image_classifier_run as image_classifier_run
import json
import sys

app = Flask(__name__)


@app.route('/hello')
def hello():
    return 'Hello World'


class BadRequest(Exception):
    """Custom exception class to be thrown when local error occurs."""
    def __init__(self, message, status=400, payload=None):
        self.message = message
        self.status = status
        self.payload = payload


@app.errorhandler(BadRequest)
def handle_bad_request(error):
    """Catch BadRequest exception globally, serialize into JSON, and respond with 400."""
    payload = dict(error.payload or ())
    payload['status'] = error.status
    payload['message'] = error.message
    return jsonify(payload), 400


@app.route('/get_objects', methods=['POST'])
def get_objects():

    # if request.headers['Content-Type'] != 'image/jpeg':
    print(request.form['dataStream'])

    # allowed_extensions = ["jpg", "png", "jpeg", "JPG"]
    try:
        img_file = request.files['image']
        data_stream = request.form['dataStream']
        data_stream_json = json.loads(data_stream)
        model_version = data_stream_json['metadata']['classifier']['modelVersion']
        mlux_threshold = data_stream_json['metadata']['classifier']['mLuxThreshold']
        print(model_version, mlux_threshold)
        try:
            # if img_file and img_file.filename[-3:] in allowed_extensions:
            if img_file:
                result = image_classifier_run.show_inference(img_file, model_version, mlux_threshold)
                print(json.dumps(result))
                response = app.response_class(response=json.dumps(result), status=200, mimetype='application/json')
                return response
        except:
            raise BadRequest('415 Unsupported Media Type or a non matched predication model', 415, {'ext': 1})
    except ValueError as e:
        raise BadRequest(e, 400, {'ext': 1})
    except:
        raise BadRequest(sys.exc_info()[0], 400, {'ext': 1})

        # app.response_class(response=json.dumps("data stream should be in json format"), status=415,
        #                           mimetype='application/json')


if __name__ == '__main__':
    app.run(host="image-classifier")
