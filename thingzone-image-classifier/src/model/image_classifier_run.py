"""
Copyright 2020 Nominet UK

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


import numpy as np
import io
from pathlib import Path
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf

from PIL import Image
from IPython.display import display
from PIL.ExifTags import TAGS

from object_detection.utils import ops as utils_ops
from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as vis_util
import re
import logging

from src.config import config

# # patch tf1 into `utils.ops`
utils_ops.tf = tf.compat.v1
#
# # Patch the location of gfile
tf.gfile = tf.io.gfile


# LOAD LABELS
def load_lables(model_name, night):
    path = "models/" + model_name
    if night:
        path = path + "/night"
        labels_path = Path(str(path))
    else:
        path = path + "/day"
        labels_path = Path(str(path))

    if labels_path.exists():
        labels_path = labels_path/"data"
        label_files = list(labels_path.glob('*.pbtxt'))

        return str(label_files[0])
    else:
        path = config.DEFAULT_MODEL
        if night:
            path = path + "/night"
            labels_path = Path(str(path))
        else:
            path = path + "/day"
            labels_path = Path(str(path))
        labels_path = labels_path / "data"
        label_files = list(labels_path.glob('*.pbtxt'))
        return str(label_files[0])


# PREPARING MODE - LOAD THE MODEL
def load_model(model_name, night):
    # base_url = 'http://download.tensorflow.org/models/object_detection/'
    # model_file = model_name + '.tar.gz'
    # model_dir = tf.keras.utils.get_file(
    #     fname=model_name,
    #     origin=base_url + model_file,
    #     untar=True)
    #
    # model_dir = pathlib.Path(model_dir)/"saved_model"
    path = "models/" + model_name
    if night:
        path = path + "/night"
        model_path = Path(path)
    else:
        path = path + "/day"
        model_path = Path(path)
    if model_path.exists():
        model_dir = model_path/"saved_model"
        model = tf.saved_model.load(str(model_dir))
        model = model.signatures['serving_default']
        logging.info("model found in ", model_dir)
        return model
    else:
        path = config.DEFAULT_MODEL
        if night:
            path = path + "/night"
            model_path = Path(path)
        else:
            path = path + "/day"
            model_path = Path(path)

        model_dir = model_path / "saved_model"
        model = tf.saved_model.load(str(model_dir))
        model = model.signatures['serving_default']
        logging.info("default model is loaded from ", model_dir)
        return model


# RUN THE MODEL FOR A SINGLE IMAGE
def run_inference_for_single_image(image, model, night):
    image = np.asarray(image)
    # The input needs to be a tensor, convert it using `tf.convert_to_tensor`.
    input_tensor = tf.convert_to_tensor(image)
    # The model expects a batch of images, so add an axis with `tf.newaxis`.
    input_tensor = input_tensor[tf.newaxis, ...]

    # Run inference
    detection_model = load_model(model, night=night)
    output_dict = detection_model(input_tensor)

    # All outputs are batches tensors.
    # Convert to numpy arrays, and take index [0] to remove the batch dimension.
    # We're only interested in the first num_detections.
    num_detections = int(output_dict.pop('num_detections'))

    output_dict = {key: value[0, :num_detections].numpy()
                   for key, value in output_dict.items()}
    output_dict['num_detections'] = num_detections
    # detection_classes should be ints.
    output_dict['detection_classes'] = output_dict['detection_classes'].astype(np.int64)

    # Handle models with masks:
    if 'detection_masks' in output_dict:
        # Reframe the the bbox mask to the image size.
        detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(
            output_dict['detection_masks'], output_dict['detection_boxes'],
            image.shape[0], image.shape[1])
        detection_masks_reframed = tf.cast(detection_masks_reframed > 0.5,
                                           tf.uint8)
        output_dict['detection_masks_reframed'] = detection_masks_reframed.numpy()
    return output_dict


def __check_mlux(image, mlux_threshold):
    img = Image.open(image)
    try:
        for (k, v) in img._getexif().items():
            if TAGS.get(k) == "ImageDescription":
                description = re.match(r'(.*)mLux:(.*)', v)
                if int(description.group(2)) >= int(mlux_threshold):
                    return 0  # 0 = day , 1 = night
                else:
                    return 1
    except:
        return 0


def get_field(exif, field):
    for (k, v) in exif.items():
        if TAGS.get(k) == field:
            return v


# VISUALISE THE OUTPUT OF MODEL AND PRINT THE RESULT LABELS, BOXES, AND CONFIDENCE LEVLE
def show_inference(image_path, model, mlux_threshold):
    night = __check_mlux(image_path, mlux_threshold)
    logging.info("night status ", night)
    # the array based representation of the image will be used later in order to prepare the
    # result image with boxes and labels on it.
    image_np = np.array(Image.open(image_path))
    # Actual detection.
    output_dict = run_inference_for_single_image(image_np, model, night)
    # Visualization of the results of a detection.

    # LOAD LABELS
    # List of the strings that is used to add correct label for each box.
    # PATH_TO_LABELS = 'models/research/object_detection/data/mscoco_label_map.pbtxt'
    # category_index = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS, use_display_name=True)

    category_index = label_map_util.create_category_index_from_labelmap(load_lables(model, night=night),
                                                                        use_display_name=True)

    # This is to get coordinates
    boxes = output_dict['detection_boxes']
    # get all boxes from an array
    max_boxes_to_draw = boxes.shape[0]
    # get scores to get a threshold
    scores = output_dict['detection_scores']
    # this is set as a default but can be adjusted based on our need
    min_score_thresh = .5
    # iterate over all objects found
    objects_list = []
    for i in range(min(max_boxes_to_draw, boxes.shape[0])):
        if scores is None or scores[i] > min_score_thresh:
            class_name = category_index[output_dict['detection_classes'][i]]['name']
            # objects_list = {"image": image_path.filename, "label": class_name,
            #                 "selection": boxes[i].tolist(), "confidence": float(scores[i])}
            box = "bbox" + str(boxes[i].tolist())
            objects_list.append({"label": class_name, "origin": model, "selection": box,
                            "confidence": float(scores[i])})
    return objects_list


# def initialise_model():
#     # DETECTION PHASE
#     model_name = 'img_mobilenet_v1'
#     return load_model(model_name)


config.init(['config/image_classifier_config.ini'])
# detection_model = initialise_model()
