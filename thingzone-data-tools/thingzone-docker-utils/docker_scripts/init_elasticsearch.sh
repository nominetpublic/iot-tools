#!/usr/bin/env bash
# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


while ! curl http://elasticsearch:9200/_cluster/health?pretty ; do
    echo "Waiting for elasticsearch to start ..."
    sleep 3
done

echo "Elasticsearch us up and running!"
echo ""

if [[ -z "${THINGZONE_DEV_UTILS_JAR}" ]]; then
  JAR=./thingzone-docker-utils.jar
else
  JAR="${THINGZONE_DEV_UTILS_JAR}"
fi

java -cp $JAR uk.nominet.iot.init.ElasticInitService init http://elasticsearch:9200
