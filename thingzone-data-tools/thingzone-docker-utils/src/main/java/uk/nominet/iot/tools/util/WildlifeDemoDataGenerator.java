/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.tools.util;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class WildlifeDemoDataGenerator {

    public static void main(String[] args) {
        String streamKey = "ept-wildlife";
        //String.format("%03d", yournumber);
        double startLon = 57.1; //+0.05
        double startLat = 20.0;


        JsonArray agents = new JsonArray();

        int i = 0;
        for (int y = 0; y < 8; y++) {
            for (int x = 0; x < 8; x++) {
                double lon = startLon + (0.05 * y);
                double lat = startLat - (0.05 * x);
                String name = String.format("ept-wf%02d", i);

                int index = (i % 11) +1;
                JsonObject agent = new JsonObject();
                agent.addProperty("name" ,name);
                agent.addProperty("klass" ,"uk.nominet.iot.tools.simulation.agent.EPT");
                agent.addProperty("imagePath" ,"config/wildlife-simulation/desert"+index+".png");

                JsonObject location = new JsonObject();
                location.addProperty("type", "Point");
                JsonArray coordinates = new JsonArray();
                coordinates.add(lon);
                coordinates.add(lat);
                location.add("coordinates", coordinates);
                agent.add("location", location);


                JsonObject activationArea = new JsonObject();
                activationArea.addProperty("type", "Polygon");

                JsonArray acCoordinatesWrapper = new JsonArray();
                JsonArray acCoordinates = new JsonArray();

                JsonArray c1 = new JsonArray();
                c1.add(lon);
                c1.add(lat);
                JsonArray c2 = new JsonArray();
                c2.add(lon+0.02);
                c2.add(lat+0.02);
                JsonArray c3 = new JsonArray();
                c3.add(lon);
                c3.add(lat+0.025);
                JsonArray c4 = new JsonArray();
                c4.add(lon-0.02);
                c4.add(lat+0.02);
                JsonArray c5 = new JsonArray();
                c5.add(lon);
                c5.add(lat);

                acCoordinates.add(c1);
                acCoordinates.add(c2);
                acCoordinates.add(c3);
                acCoordinates.add(c4);
                acCoordinates.add(c5);
                acCoordinatesWrapper.add(acCoordinates);
                activationArea.add("coordinates", acCoordinatesWrapper);
                agent.add("activationArea", activationArea);

                JsonObject other = new JsonObject();
                other.addProperty("symbol", "attraction-15");
                other.addProperty("imagesStream", name+".images");
                other.addProperty("reportStream", name+".report");
                other.addProperty("configStream", name+".config");
                other.addProperty("eventStream", name+".event");

                agent.add("other" , other);

                agents.add(agent);
                i++;
            }
        }

        System.out.println(agents);


    }
}

