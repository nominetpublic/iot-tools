/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.tools.util;

import io.searchbox.core.Bulk;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.elastic.ThingzoneTimeseriesIndex;
import uk.nominet.iot.manager.ThingzoneTimeseriesHandler;
import uk.nominet.iot.manager.ThingzoneTimeseriesManager;
import uk.nominet.iot.model.timeseries.*;

import java.util.ArrayList;
import java.util.List;


public class BuilkTimeseriesList extends ArrayList<Metric> {
    ThingzoneTimeseriesManager timeseriesManager;
    private long total;

    public BuilkTimeseriesList(ThingzoneTimeseriesManager timeseriesManager) {
        super();
        this.timeseriesManager = timeseriesManager;
    }

    public void bulkUpload() throws Exception {
        List<ThingzoneTimeseriesIndex> bulkIndexList = new ArrayList<>();

        for (Metric point : this) {
            if (point.getKlass().equals(MetricInt.class.getName())) {
                ThingzoneTimeseriesHandler<MetricInt> thingzoneTimeseriesHandlerInt =
                        timeseriesManager.timeseries(point.getStreamKey(), MetricInt.class);
                bulkIndexList.addAll(thingzoneTimeseriesHandlerInt.addPointForBuldIndexing((MetricInt) point));
            } else if (point.getKlass().equals(MetricDouble.class.getName())) {
                ThingzoneTimeseriesHandler<MetricDouble> thingzoneTimeseriesHandlerInt =
                        timeseriesManager.timeseries(point.getStreamKey(), MetricDouble.class);
                bulkIndexList.addAll(thingzoneTimeseriesHandlerInt.addPointForBuldIndexing((MetricDouble) point));
            } else if (point.getKlass().equals(MetricString.class.getName())) {
                ThingzoneTimeseriesHandler<MetricString> thingzoneTimeseriesHandlerInt =
                        timeseriesManager.timeseries(point.getStreamKey(), MetricString.class);
                bulkIndexList.addAll(thingzoneTimeseriesHandlerInt.addPointForBuldIndexing((MetricString) point));
            } else if (point.getKlass().equals(MetricLatLong.class.getName())) {
                ThingzoneTimeseriesHandler<MetricLatLong> thingzoneTimeseriesHandlerInt =
                        timeseriesManager.timeseries(point.getStreamKey(), MetricLatLong.class);
                bulkIndexList.addAll(thingzoneTimeseriesHandlerInt.addPointForBuldIndexing((MetricLatLong) point));
            } else if (point.getKlass().equals(Status.class.getName())) {
                ThingzoneTimeseriesHandler<Status> thingzoneTimeseriesHandlerInt =
                        timeseriesManager.timeseries(point.getStreamKey(), Status.class);
                bulkIndexList.addAll(thingzoneTimeseriesHandlerInt.addPointForBuldIndexing((Status) point));
            }

        }

        if (bulkIndexList.size() > 0) {
            Bulk bulk = new Bulk.Builder()
                                .addAction(bulkIndexList)
                                .build();


            ElasticsearchDriver.getClient().execute(bulk);
        }

        total += size();
        clear();
    }

    public long getTotal() {
        return total;
    }
}
