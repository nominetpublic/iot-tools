/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.tools;

import com.coreoz.wisp.Scheduler;
import com.coreoz.wisp.schedule.Schedules;
import com.github.filosganga.geogson.model.Point;
import com.github.filosganga.geogson.model.positions.SinglePosition;
import com.github.javafaker.Faker;
import com.google.common.collect.ImmutableList;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.ByteArrayPartSource;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import uk.nominet.iot.config.*;
import uk.nominet.iot.driver.CassandraDriver;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.error.DataCachingException;
import uk.nominet.iot.error.DataStorageException;
import uk.nominet.iot.manager.ThingzoneEventHandler;
import uk.nominet.iot.manager.ThingzoneEventManager;
import uk.nominet.iot.manager.UserContext;
import uk.nominet.iot.manager.registry.RegistryDataStreamHandler;
import uk.nominet.iot.manager.registry.RegistryDataStreamManager;
import uk.nominet.iot.manager.registry.RegistryDeviceHandler;
import uk.nominet.iot.manager.registry.RegistryDeviceManager;
import uk.nominet.iot.model.DerivedRelation;
import uk.nominet.iot.model.Feedback;
import uk.nominet.iot.model.registry.RegistryDevice;
import uk.nominet.iot.model.registry.RegistryDeviceReference;
import uk.nominet.iot.model.timeseries.Event;
import uk.nominet.iot.model.timeseries.Image;
import uk.nominet.iot.model.timeseries.ThingzoneEventType;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.Duration;
import java.time.Instant;
import java.util.*;

public class RandomAlertsGenerator {
    public static final String ALERTS_GENERATOR_TYPE = "alerts.generator.type";
    public static final String ALERTS_GENERATOR_STREAMS = "alerts.generator.streams";
    public static final String ALERTS_GENERATOR_INTERVAL = "alerts.generator.interval";
    public static final String ALERTS_GENERATOR_USERNAME = "alerts.generator.username";
    public static final String ALERTS_GENERATOR_PASSWORD = "alerts.generator.password";
    private Properties configProp;
    private int interval;
    private List<String> streamKeys;
    private Faker faker = new Faker();
    private ThingzoneEventManager alertManager;
    private String username;
    private String password;

    public RandomAlertsGenerator(Properties configProp) throws Exception {
        this.configProp = configProp;
        this.interval = Integer.parseInt(configProp.getProperty(ALERTS_GENERATOR_INTERVAL));
        this.streamKeys = Arrays.asList(configProp.getProperty(ALERTS_GENERATOR_STREAMS).split(","));
        this.username = configProp.getProperty(ALERTS_GENERATOR_USERNAME);
        this.password = configProp.getProperty(ALERTS_GENERATOR_PASSWORD);
        initDrivers();
        alertManager = ThingzoneEventManager.createManager();
    }



    public static int randInt(int min, int max) {
        Random rand = new Random();
        return rand.ints(1,min,max).findFirst().getAsInt();
    }

    public static double randDouble(double min, double max) {
        Random rand = new Random();
        return rand.doubles(1,min,max).findFirst().getAsDouble();
    }


    public static List<String> randomListStringOf(String ...items) {
        ArrayList<String> result = new ArrayList<>();

        for(String item: items) {
            if (randInt(0,19) > 5) {
                result.add(item);
            }
        }

        return result;
    }

    public static String randomFromList(List<String> list) {
        return list.get(randInt(0, list.size()));
    }


    public void initDrivers() throws DataStorageException {
        if (!RegistryAPIDriver.isInitialized()) {
            RegistryDriverConfig registryConfig = new RegistryDriverConfig();
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_SCHEME,
                                       configProp.getProperty(RegistryConstant.REGISTRY_SERVER_SCHEME));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST,
                                       configProp.getProperty(RegistryConstant.REGISTRY_SERVER_HOST));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT,
                                       configProp.getProperty(RegistryConstant.REGISTRY_SERVER_PORT));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_USERNAME,
                                       configProp.getProperty(RegistryConstant.REGISTRY_SERVER_USERNAME));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD,
                                       configProp.getProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS,
                                       configProp.getProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS));
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }

        if (!CassandraDriver.isInitialized()) {
            CassandraDriverConfig cassandraConfig = new CassandraDriverConfig();
            cassandraConfig.addProperty(CassandraConstant.CASSANDRA_SERVER_HOST,
                                        configProp.getProperty(CassandraConstant.CASSANDRA_SERVER_HOST));
            cassandraConfig.addProperty(CassandraConstant.CASSANDRA_SERVER_PORT,
                                        configProp.getProperty(CassandraConstant.CASSANDRA_SERVER_PORT));
            CassandraDriver.setConnectionProperties(cassandraConfig);
            CassandraDriver.globalInitialise();
        }

        if (!ElasticsearchDriver.isInitialized()) {
            ElasticsearchDriverConfig elasticsearchConfig = new ElasticsearchDriverConfig();
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_SCHEME,
                                            configProp.getProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_SCHEME));
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_HOST,
                                            configProp.getProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_HOST));
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_PORT,
                                            configProp.getProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_PORT));
            ElasticsearchDriver.setConnectionProperties(elasticsearchConfig);
            ElasticsearchDriver.globalInitialise();
        }
    }


    public List<String> randomTags(int max) {
        ArrayList<String> tags = new ArrayList<>();
        int numberOfTags = randInt(0, max);

        for(int i = 0; i < numberOfTags; i++) {
            tags.add(faker.hipster().word());
        }

        return tags;
    }

    public List<String> randomPeople(int max) {
        ArrayList<String> tags = new ArrayList<>();
        int numberOfTags = randInt(1, max);

        for(int i = 0; i < numberOfTags; i++) {
            tags.add(faker.starTrek().character());
        }

        return tags;
    }


    public Point randomPoint(double latMin, double latMax, double lonMin, double lonMax) {
        return new Point(new SinglePosition(randDouble(lonMin, lonMax), randDouble(latMin, latMax), 0.0));
    }


    public byte[] downloadUrl(URL toDownload) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        try {
            byte[] chunk = new byte[4096];
            int bytesRead;
            InputStream stream = toDownload.openStream();

            while ((bytesRead = stream.read(chunk)) > 0) {
                outputStream.write(chunk, 0, bytesRead);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return outputStream.toByteArray();
    }

    public String uploadImageToServer(byte[] image) {
        HttpConnectionManager httpConnectionManager = new MultiThreadedHttpConnectionManager();
        HttpClient httpClient = new HttpClient(httpConnectionManager);

        PostMethod postMethod = new PostMethod("http://localhost:8082/upload");


        ByteArrayPartSource byteArrayPartSource = new ByteArrayPartSource("file", image);
        FilePart filePart = new FilePart("file", byteArrayPartSource);
        Part[] parts = {filePart};
        try {
            MultipartRequestEntity multipartRequestEntity = new MultipartRequestEntity(parts, postMethod.getParams());
            postMethod.setRequestEntity(multipartRequestEntity);
            httpClient.executeMethod(postMethod);
            String responseStr = null;
            responseStr = postMethod.getResponseBodyAsString();

            return responseStr;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void generateRandomAlert() {
        String streamKey = randomFromList(streamKeys);
        System.out.println("Generating Random Event for " +  streamKey);

        //Get stream
        RegistryDevice device = null;
        try {
            RegistryDataStreamManager streamManager = RegistryDataStreamManager.createManager();
            RegistryDataStreamHandler streamHandler = streamManager.stream(streamKey);
            RegistryDeviceReference deviceReference = streamHandler.get().getDevices().get(0);
            UserContext userContext = new UserContext(username).setPassword(password);
            RegistryDeviceManager deviceManager = RegistryDeviceManager.createManager(userContext);
            RegistryDeviceHandler deviceHandler = deviceManager.device(deviceReference.getKey());
            device = deviceHandler.get();
        } catch (Exception e) {
            e.printStackTrace();
        }

        ThingzoneEventHandler alertHandler = null;
        try {
            alertHandler = alertManager.timeseries(streamKey);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Event alert = new Event();
        alert.setId(UUID.randomUUID());
        alert.setStreamKey(streamKey);
        alert.setTags(randomTags(6));
        //alert.setTags(randomListStringOf("tag1", "tag2", "tag3", "tag4"));
        alert.setReceivers(randomPeople(3));
        alert.setDescription(faker.chuckNorris().fact());
        //alert.setName(device.getName());
        if (device.getType().equals("camera"))
            alert.setName(randomFromList(ImmutableList.of("Movement Detection", "Animal Detection", "Human Activity Detection")));
        else
            alert.setName(device.getName() + " trigger");

        if (randInt(0, 10) == 4) {
            Point point = (Point) device.getLocation();

            Point pointTranslated = new Point(new SinglePosition(
                    point.lon() + randDouble(0.0001, 0.002),
                    point.lat() + randDouble(0.0001, 0.002),
                    0.0
            ));

            alert.setLocation(pointTranslated);

        } else {
            alert.setLocation(device.getLocation());
        }

        if (configProp.getProperty(ALERTS_GENERATOR_TYPE).equals("camera")) {
            DerivedRelation derivedRelation = new DerivedRelation();
            derivedRelation.setStreamKey(String.format("%s.images", device.getKey()));
            derivedRelation.setType("triggeredByImage");
            derivedRelation.setKlass(Image.class.getName());
            derivedRelation.setDescription("Triggered by image stream");
            alert.setDerivedFrom(new ArrayList<>());
            alert.getDerivedFrom().add(derivedRelation);
        }

        alert.setCategory(faker.color().name());
        alert.setEventType(ThingzoneEventType.ALERT);
        alert.setSeverity(randDouble(0, 1));
        alert.setTimestamp(Instant.now());

        // Generate thumbnail
//        byte[] imageData;
//
//        try {
//            imageData = downloadUrl(new URL("http://placeimg.com/64/64"));
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//            return;
//        }
//
//        String response = uploadImageToServer(imageData);
//        JsonObject responseJson = new Gson().fromJson(response, JsonObject.class);
//        String reference = responseJson.get("name").getAsString();
//        alert.setThumbnails(ImmutableList.of(reference));

        if (username != null && randInt(0,10) > 5) {
            Feedback feedback = new Feedback();
            feedback.setUser(username);
            feedback.setType("system");
            feedback.setCategory("read");
            feedback.setId(UUID.randomUUID());
            feedback.setTimestamp(Instant.now());
            feedback.setDescription("user have seen alert");
            alert.setFeedback(ImmutableList.of(feedback));
        }

        try {
            alertHandler.addPoint(alert);
        } catch (DataStorageException e) {
            e.printStackTrace();
        } catch (DataCachingException e) {
            e.printStackTrace();
        }

    }


    public void run() {
        System.out.println("RandomAlertsGenerator app is running");

        Scheduler scheduler = new Scheduler();

        scheduler.schedule(() -> generateRandomAlert(),
                           Schedules.afterInitialDelay(
                                   Schedules.fixedDelaySchedule(Duration.ofSeconds(interval)),
                                   Duration.ZERO));

    }

    public static void main(String[] args) throws Exception {
        Properties configProp = ConfigurationLoader.fromFileName(args[0]);
        RandomAlertsGenerator app = new RandomAlertsGenerator(configProp);
        if (app != null) app.run();
    }
}
