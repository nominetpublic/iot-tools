/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.tools;

import com.coreoz.wisp.Scheduler;
import com.coreoz.wisp.schedule.Schedules;
import com.github.filosganga.geogson.model.Point;
import com.github.filosganga.geogson.model.positions.SinglePosition;
import com.github.javafaker.Faker;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.ByteArrayPartSource;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import uk.nominet.iot.config.*;
import uk.nominet.iot.driver.CassandraDriver;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.error.DataCachingException;
import uk.nominet.iot.error.DataStorageException;
import uk.nominet.iot.manager.ThingzoneTimeseriesHandler;
import uk.nominet.iot.manager.ThingzoneTimeseriesManager;
import uk.nominet.iot.model.timeseries.Image;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.*;

public class RandomImageGenerator {
    private Properties configProp;
    private int interval;
    private String streamKey;
    private Faker faker = new Faker();
    private ThingzoneTimeseriesManager timeseriesManager;
    private ThingzoneTimeseriesHandler<Image> timeseriesHandler;
    private String username;

    public RandomImageGenerator(Properties configProp, int interval, String streamKey) throws Exception {
        this.configProp = configProp;
        this.interval = interval;
        this.streamKey = streamKey;
        initDrivers();
        timeseriesManager = ThingzoneTimeseriesManager.createManager();
        timeseriesHandler = timeseriesManager.timeseries(streamKey, Image.class);
    }

    public RandomImageGenerator(Properties configProp, int interval, String streamKey, String username) throws Exception {
        this(configProp, interval, streamKey);
        this.username = username;
    }


    public static int randInt(int min, int max) {
        Random rand = new Random();
        return rand.ints(1, min, max).findFirst().getAsInt();
    }

    public static double randDouble(double min, double max) {
        Random rand = new Random();
        return rand.doubles(1, min, max).findFirst().getAsDouble();
    }


    public static List<String> randomListStringOf(String... items) {
        ArrayList<String> result = new ArrayList<>();

        for (String item : items) {
            if (randInt(0, 19) > 5) {
                result.add(item);
            }
        }

        return result;
    }


    public void initDrivers() throws DataStorageException {
        if (!RegistryAPIDriver.isInitialized()) {
            RegistryDriverConfig registryConfig = new RegistryDriverConfig();
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_SCHEME,
                                       configProp.getProperty(RegistryConstant.REGISTRY_SERVER_SCHEME));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST,
                                       configProp.getProperty(RegistryConstant.REGISTRY_SERVER_HOST));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT,
                                       configProp.getProperty(RegistryConstant.REGISTRY_SERVER_PORT));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_USERNAME,
                                       configProp.getProperty(RegistryConstant.REGISTRY_SERVER_USERNAME));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD,
                                       configProp.getProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS,
                                       configProp.getProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS));
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }

        if (!CassandraDriver.isInitialized()) {
            CassandraDriverConfig cassandraConfig = new CassandraDriverConfig();
            cassandraConfig.addProperty(CassandraConstant.CASSANDRA_SERVER_HOST,
                                        configProp.getProperty(CassandraConstant.CASSANDRA_SERVER_HOST));
            cassandraConfig.addProperty(CassandraConstant.CASSANDRA_SERVER_PORT,
                                        configProp.getProperty(CassandraConstant.CASSANDRA_SERVER_PORT));
            CassandraDriver.setConnectionProperties(cassandraConfig);
            CassandraDriver.globalInitialise();
        }

        if (!ElasticsearchDriver.isInitialized()) {
            ElasticsearchDriverConfig elasticsearchConfig = new ElasticsearchDriverConfig();
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_SCHEME,
                                            configProp.getProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_SCHEME));
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_HOST,
                                            configProp.getProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_HOST));
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_PORT,
                                            configProp.getProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_PORT));
            ElasticsearchDriver.setConnectionProperties(elasticsearchConfig);
            ElasticsearchDriver.globalInitialise();
        }

    }


    public List<String> randomTags(int max) {
        ArrayList<String> tags = new ArrayList<>();
        int numberOfTags = randInt(0, max);

        for (int i = 0; i < numberOfTags; i++) {
            tags.add(faker.hipster().word());
        }

        return tags;
    }

    public List<String> randomPeople(int max) {
        ArrayList<String> tags = new ArrayList<>();
        int numberOfTags = randInt(1, max);

        for (int i = 0; i < numberOfTags; i++) {
            tags.add(faker.starTrek().character());
        }

        return tags;
    }


    public Point randomPoint(double latMin, double latMax, double lonMin, double lonMax) {
        return new Point(new SinglePosition(randDouble(lonMin, lonMax), randDouble(latMin, latMax), 0.0));
    }


    // https://loremflickr.com/1024/768/animal
    public byte[] downloadUrl(URL toDownload) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        try {
            byte[] chunk = new byte[4096];
            int bytesRead;
            InputStream stream = toDownload.openStream();

            while ((bytesRead = stream.read(chunk)) > 0) {
                outputStream.write(chunk, 0, bytesRead);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return outputStream.toByteArray();
    }

    public String uploadImageToServer(byte[] image) {
        HttpConnectionManager httpConnectionManager = new MultiThreadedHttpConnectionManager();
        HttpClient httpClient = new HttpClient(httpConnectionManager);

        PostMethod postMethod = new PostMethod("http://localhost:8082/upload");


        ByteArrayPartSource byteArrayPartSource = new ByteArrayPartSource("file", image);
        FilePart filePart = new FilePart("file", byteArrayPartSource);
        Part[] parts = {filePart};
        try {
            MultipartRequestEntity multipartRequestEntity = new MultipartRequestEntity(parts, postMethod.getParams());
            postMethod.setRequestEntity(multipartRequestEntity);
            httpClient.executeMethod(postMethod);
            String responseStr = null;
            responseStr = postMethod.getResponseBodyAsString();

            return responseStr;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void generateRandomImage(Instant timestamp) {
        System.out.println("Generating Random Image ..." + timestamp);

        byte[] imageData;

        try {
            imageData = downloadUrl(new URL("http://placeimg.com/1024/768"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return;
        }

        String response = uploadImageToServer(imageData);
        JsonObject responseJson = new Gson().fromJson(response, JsonObject.class);
        String reference = responseJson.get("name").getAsString();


        Image image = new Image();
        image.setId(UUID.randomUUID());
        image.setStreamKey(streamKey);
        image.setTimestamp(timestamp);
        image.setTags(randomTags(6));
        image.setImageRef(reference);

        System.out.println("Image data point: " + image);


        try {
            timeseriesHandler.addPoint(image);
        } catch (DataStorageException e) {
            e.printStackTrace();
        } catch (DataCachingException e) {
            e.printStackTrace();
        }

    }


    public void generateRandomImageOrBurst() {

        if (randInt(0,5) == 3) {
            int burstSize = randInt(3,30);

            Instant timestamp = Instant.now();
            for (int i = 0; i < burstSize; i++) {
                generateRandomImage(timestamp.plus(200 * i, ChronoUnit.MILLIS));
            }

        } else {
            generateRandomImage(Instant.now());
        }
    }

    public void run() {
        System.out.println("RandomImageGenerator app is running");

        Scheduler scheduler = new Scheduler();

        scheduler.schedule(() -> generateRandomImageOrBurst(),
                           Schedules.afterInitialDelay(
                                   Schedules.fixedDelaySchedule(Duration.ofSeconds(interval)),
                                   Duration.ZERO));

    }

    public static void main(String[] args) throws Exception {

        if (args.length < 3) {
            System.out.println("Usage: RandomImageGenerator configFile interval_seconds stream_key [username]");
            System.exit(1);
        }

        Properties configProp = ConfigurationLoader.fromFileName(args[0]);

        RandomImageGenerator app = null;
        if (args.length == 3)
            app = new RandomImageGenerator(configProp, Integer.parseInt(args[1]), args[2]);
        if (args.length == 4)
            app = new RandomImageGenerator(configProp, Integer.parseInt(args[1]), args[2], args[3]);
        if (app != null)
            app.run();

    }
}
