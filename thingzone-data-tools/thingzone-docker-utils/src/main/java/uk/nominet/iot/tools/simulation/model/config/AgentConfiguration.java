/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.tools.simulation.model.config;


import com.github.filosganga.geogson.model.Point;
import com.github.filosganga.geogson.model.Polygon;
import java.util.Map;

public class AgentConfiguration {
    private String name;
    private String klass;
    private Point location;
    private String imagePath;
    private int imageOffset;
    private Polygon activationArea;
    private AgentPathConfiguration path;
    private Map<String,Object> other;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKlass() {
        return klass;
    }

    public void setKlass(String klass) {
        this.klass = klass;
    }

    public Point getLocation() {
        return location;
    }

    public void setLocation(Point location) {
        this.location = location;
    }

    public Polygon getActivationArea() {
        return activationArea;
    }

    public void setActivationArea(Polygon activationArea) {
        this.activationArea = activationArea;
    }

    public AgentPathConfiguration getPath() {
        return path;
    }

    public void setPath(AgentPathConfiguration path) {
        this.path = path;
    }

    public Map<String, Object> getOther() {
        return other;
    }

    public void setOther(Map<String, Object> other) {
        this.other = other;
    }


    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public int getImageOffset() {
        return imageOffset;
    }

    public void setImageOffset(int imageOffset) {
        this.imageOffset = imageOffset;
    }

    @Override
    public String toString() {
        return "AgentConfiguration{" +
               "name='" + name + '\'' +
               ", klass='" + klass + '\'' +
               ", location=" + location +
               ", imagePath='" + imagePath + '\'' +
               ", imageOffset=" + imageOffset +
               ", activationArea=" + activationArea +
               ", path=" + path +
               ", other=" + other +
               '}';
    }
}
