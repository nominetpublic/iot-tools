/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.tools;

import org.apache.commons.io.FileUtils;
import uk.nominet.iot.config.RegistryConstant;
import uk.nominet.iot.config.RegistryDriverConfig;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.error.DataStorageException;
import uk.nominet.iot.manager.UserContext;
import uk.nominet.iot.manager.registry.RegistryFunctionHandler;
import uk.nominet.iot.manager.registry.RegistryFunctionManager;
import uk.nominet.iot.utils.AnsiChar;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;

import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardWatchEventKinds.*;

public class TransformFunctionsObserver {

    public static final String REGISTRY_SCHEME = "http";
    public static final String REGISTRY_HOST = "localhost";
    public static final int REGISTRY_PORT = 8081;
    public static final String REGISTRY_USERNAME = "user";
    public static final String REGISTRY_PASSWORD = "user";
    RegistryFunctionManager functionManager;
    File dir;


    public TransformFunctionsObserver(File dir) throws DataStorageException {
        this.dir = dir;
        UserContext userContext = new UserContext(REGISTRY_USERNAME);
        userContext.setPassword(REGISTRY_PASSWORD);

        functionManager = RegistryFunctionManager.createManager(userContext);
    }

    public void watchDirectoryPath() {
        Path path = dir.toPath();
        // Sanity check - Check if path is a folder
        try {
            Boolean isFolder = (Boolean) Files.getAttribute(path,
                                                            "basic:isDirectory", NOFOLLOW_LINKS);
            if (!isFolder) {
                throw new IllegalArgumentException("Path: " + path
                                                   + " is not a folder");
            }
        } catch (IOException ioe) {
            // Folder does not exists
            ioe.printStackTrace();
        }

        System.out.println("Watching path: " + path);

        FileSystem fs = path.getFileSystem();

        try (WatchService service = fs.newWatchService()) {

            path.register(service, ENTRY_CREATE, ENTRY_MODIFY, ENTRY_DELETE);


            WatchKey key;
            while (true) {
                key = service.take();

                WatchEvent.Kind<?> eventKind;
                for (WatchEvent<?> watchEvent : key.pollEvents()) {
                    eventKind = watchEvent.kind();
                    if (OVERFLOW == eventKind) {
                        continue;
                    } else if (ENTRY_CREATE == eventKind || ENTRY_MODIFY == eventKind) {
                        Path eventPath = ((WatchEvent<Path>) watchEvent).context();
                        if (!eventPath.toString().endsWith("_")) {
                            String functionName = eventPath.toFile().getName().replace(".js","");

                            String  functionContent =
                                    FileUtils.readFileToString(new File(dir.toString()+"/"+eventPath.toString()));

                            System.out.print(functionName + " ");

                            RegistryFunctionHandler registryFunctionHandler = null;
                            try {

                                registryFunctionHandler = functionManager.create(
                                        functionName,
                                        "javascript",
                                        functionContent,
                                        false);

                                System.out.println(AnsiChar.ANSI_GREEN + " OK" + AnsiChar.ANSI_RESET);
                            } catch (Exception e) {
                                System.out.print(AnsiChar.ANSI_PURPLE + " (function exists) " + AnsiChar.ANSI_RESET);
                            } finally {
                                if (registryFunctionHandler == null) {
                                    try {
                                        registryFunctionHandler = functionManager.function(
                                                functionName,
                                                false);

                                        registryFunctionHandler.updateFunction(
                                                "javascript",
                                                functionContent
                                                                              );
                                        System.out.println(AnsiChar.ANSI_GREEN + " UPDATED" + AnsiChar.ANSI_RESET);
                                    } catch (Exception e1) {
                                        System.out.println(AnsiChar.ANSI_RED + " FAILED " + e1.getMessage() + AnsiChar.ANSI_RESET);
                                    }
                                }
                            }



                        }
                    } else if (ENTRY_DELETE == eventKind) {
                        Path eventPath = ((WatchEvent<Path>) watchEvent).context();
                        if (!eventPath.toString().endsWith("_"))
                            System.out.println("New path deleted: " + eventPath);
                    }
                }

                if (!key.reset()) {
                    break;
                }
            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }

    }

    public static void main(String[] args) throws DataStorageException {
        if (!RegistryAPIDriver.isInitialized()) {
            RegistryDriverConfig registryConfig = new RegistryDriverConfig();
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_SCHEME, REGISTRY_SCHEME);
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST, REGISTRY_HOST);
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT, REGISTRY_PORT);
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_USERNAME, REGISTRY_USERNAME);
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD, REGISTRY_PASSWORD);
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS, 3);
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }


        File dir = new File("data_init/js");

        TransformFunctionsObserver observer = new TransformFunctionsObserver(dir);
        observer.watchDirectoryPath();
    }

}
