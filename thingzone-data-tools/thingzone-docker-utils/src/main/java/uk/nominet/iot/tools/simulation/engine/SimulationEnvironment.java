/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.tools.simulation.engine;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mapbox.geojson.*;
import com.mapbox.turf.TurfMeasurement;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.methods.multipart.ByteArrayPartSource;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import spark.Request;
import spark.Response;
import spark.Service;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.http.CORSEnabler;
import uk.nominet.iot.tools.simulation.SimulationLoader;
import uk.nominet.iot.tools.simulation.agent.Agent;
import uk.nominet.iot.tools.simulation.model.config.SimulationConfig;
import uk.nominet.iot.tools.simulation.utils.Image;
import uk.nominet.iot.utils.AnsiChar;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAmount;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

import static spark.Service.ignite;

public class SimulationEnvironment {
    public List<Agent> agents;
    public Instant startTime;
    public Instant endTime;
    public TemporalAmount step;
    public int stepCounter;
    public int delayMilliseconds;
    private ScheduledExecutorService service;
    private SimulationRunner simulationRunner;
    private ScheduledFuture<?> queue;
    private SimulationConfig configuration;

    public SimulationEnvironment(SimulationLoader loader) {
        configuration = loader.getConfiguration();
        startTime = (configuration.getStartTime() != null) ? configuration.getStartTime() : Instant.now();
        endTime = configuration.getEndTime();
        step = Duration.of(configuration.getDelayMilliseconds(), ChronoUnit.MILLIS);
        delayMilliseconds = configuration.getDelayMilliseconds();

        //Load all agents
        agents = loader.getAgents();

        // Other initialisations
        stepCounter = 0;
        service = Executors.newScheduledThreadPool(5);
        simulationRunner = new SimulationRunner(this);

        // Init web server
        Service http = ignite()
                               .port(8888)
                               .ipAddress("0.0.0.0")
                               .threadPool(20);

        http.staticFileLocation("/www");

        // Attach security components
        new CORSEnabler("*",
                        "GET,PUT,POST,DELETE,OPTIONS",
                        "Content-Type,Authorization,X-Requested-With," +
                        "Content-Length,Accept,Origin,X-Introspective-Session-Key")
                .attachTo(http);

        http.before("/*", (request, response) -> {
            response.type("application/json");
        });

        http.get("/locations", (req, res) -> locations(req, res));
        http.get("/play", (req, res) -> playRequest(req, res));
        http.get("/stop", (req, res) -> stopRequest(req, res));
    }

    public Instant getSimulationTime() {
        //return startTime.plus(stepCounter * delayMilliseconds, ChronoUnit.MILLIS);
        return Instant.now();
    }


    private String stopRequest(Request req, Response res) {
        stop();
        return "ok";
    }

    private String playRequest(Request req, Response res) {
        play();
        return "ok";
    }

    public String locations(Request req, Response res) {
        //FeatureCollection collection = FeatureCollection.fromFeatures()
        List<Feature> features = new ArrayList<>();
        List<Point> points = new ArrayList<>();
        agents.forEach(agent -> {
            if (agent.location != null) {
                features.add(generateFeature(agent, agent.location));
                points.add(agent.location);
            }
            if (agent.activationArea != null)
                features.add(generateFeature(agent, agent.activationArea));

            if (agent.path != null && agent.path.getLine() != null) {
                features.add(generateFeature(agent, agent.path.getLine()));
            }
        });


        MultiPoint multiPoint = MultiPoint.fromLngLats(points);
        double[] bboxcoords = TurfMeasurement.bbox(multiPoint);


        BoundingBox bbox = BoundingBox.fromPoints(Point.fromLngLat( bboxcoords[0],bboxcoords[1] ), Point.fromLngLat( bboxcoords[2], bboxcoords[3]));

        FeatureCollection collection = FeatureCollection.fromFeatures(features, bbox);


        return collection.toJson();
    }

    public Feature generateFeature(Agent agent, Geometry geometry) {
        Feature feature = Feature.fromGeometry(geometry);
        feature.addStringProperty("name", agent.name);
        if (agent.configuration.getOther() != null && agent.configuration.getOther().containsKey("symbol")) {
            feature.addStringProperty("symbol", agent.configuration.getOther().get("symbol").toString());
        }

        return feature;

    }

    public void addAgent(Agent agent) {
        agents.add(agent);
    }

    public void play() {
        if (queue == null || queue.isCancelled())
            queue = service.scheduleAtFixedRate(simulationRunner, 0, delayMilliseconds, TimeUnit.MILLISECONDS);

    }

    public void stop() {
        queue.cancel(false);
    }

    public void reset() {
        stepCounter = 0;
    }

    public SimulationConfig getConfiguration() {
        return configuration;
    }


    public JsonObject sendImagePayload(BufferedImage image, String streamKey, int event, int sequence) throws Exception {
        byte[] imageBytes = Image.toBytes(image);

        String sessionKey = RegistryAPIDriver.getUserSession(getConfiguration().getSender().getUsername(),
                                                             getConfiguration().getSender().getPassword())
                                             .getSession()
                                             .getSessionKey();


        HttpConnectionManager httpConnectionManager = new MultiThreadedHttpConnectionManager();
        HttpClient httpClient = new HttpClient(httpConnectionManager);

        System.out.println("Sending image to : " + getConfiguration().getBridgeConnection().getUploadserver() + streamKey);

        PostMethod postMethod = new PostMethod(getConfiguration().getBridgeConnection().getUploadserver());
        //postMethod.addParameter("event", ""+event);
        //postMethod.addParameter("sequece", ""+sequence);

        ByteArrayPartSource byteArrayPartSource = new ByteArrayPartSource("file", imageBytes);
        FilePart filePart = new FilePart("file", byteArrayPartSource);
        filePart.setContentType("image/png");
        Part[] parts = {filePart};
        try {
            MultipartRequestEntity multipartRequestEntity = new MultipartRequestEntity(parts, postMethod.getParams());
            postMethod.setRequestEntity(multipartRequestEntity);
            postMethod.addRequestHeader(new Header("X-Introspective-Session-Key",sessionKey));
            httpClient.executeMethod(postMethod);

            JsonObject responseJson = new Gson().fromJson(postMethod.getResponseBodyAsString(), JsonObject.class);
            System.out.println("Response: " + responseJson);

            return responseJson;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public JsonObject sendJsonPayload(String payload, String streamKey) throws Exception {

        String sessionKey = RegistryAPIDriver.getUserSession(getConfiguration().getSender().getUsername(),
                                                             getConfiguration().getSender().getPassword())
                                             .getSession()
                                             .getSessionKey();


        HttpConnectionManager httpConnectionManager = new MultiThreadedHttpConnectionManager();
        HttpClient httpClient = new HttpClient(httpConnectionManager);

        System.out.println("Sending json to : " + getConfiguration().getBridgeConnection().getUploadserver() + streamKey);

        PostMethod postMethod = new PostMethod(getConfiguration().getBridgeConnection().getUploadserver() + streamKey);

        StringRequestEntity requestEntity = new StringRequestEntity(
                payload,
                "application/json",
                "UTF-8");

        try {
            postMethod.addRequestHeader(new Header("X-Introspective-Session-Key",sessionKey));
            postMethod.setRequestEntity(requestEntity);
            httpClient.executeMethod(postMethod);

            JsonObject responseJson = new Gson().fromJson(postMethod.getResponseBodyAsString(), JsonObject.class);
            System.out.println("Response: " + responseJson);

            return responseJson;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void log(String message) {
        System.out.println(String.format("%s - %s", Instant.now(), message));

    }

    public void log(String agentName, String message) {
        System.out.println(AnsiChar.ANSI_PURPLE + String.format("%s [%s] - %s", Instant.now(), agentName, message) + AnsiChar.ANSI_RESET);

    }
}
