/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.tools.simulation.agent;

import com.mapbox.geojson.LineString;
import com.mapbox.geojson.Point;
import com.mapbox.geojson.Polygon;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.tools.simulation.engine.SimulationEnvironment;
import uk.nominet.iot.tools.simulation.model.config.AgentConfiguration;
import uk.nominet.iot.tools.simulation.model.AgentPath;


import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;

public abstract class Agent implements Comparable<Agent> {
    public static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    public Point location;
    public Polygon activationArea;
    public AgentPath path;
    public String name;
    public AgentConfiguration configuration;
    public BufferedImage image;

    public Agent(AgentConfiguration configuration) throws Exception {
        if (configuration.getName() != null)
            this.name = configuration.getName();

        if (configuration.getLocation() != null)
            this.location = Point.fromJson(jsonStringSerialiser.writeObject(configuration.getLocation()));

        if (configuration.getActivationArea() != null)
            this.activationArea = Polygon.fromJson(jsonStringSerialiser.writeObject(configuration.getActivationArea()));

        if (configuration.getPath() != null) {
            AgentPath pathConfig = new AgentPath();
            pathConfig.setStepInMeters(configuration.getPath().getStepInMeters());
            pathConfig.setRepeat(configuration.getPath().getRepeat());
            pathConfig.setLine(LineString.fromJson(jsonStringSerialiser.writeObject(configuration.getPath().getLine())));
            path = pathConfig;
        }

        if (configuration.getImagePath() != null) {
            image = ImageIO.read(new File(configuration.getImagePath()));
        }

        this.configuration = configuration;
    }

    public abstract void step(SimulationEnvironment env);

    @Override
    public int compareTo(Agent anotherAgent) {
        return name.compareTo(anotherAgent.name);
    }

}
