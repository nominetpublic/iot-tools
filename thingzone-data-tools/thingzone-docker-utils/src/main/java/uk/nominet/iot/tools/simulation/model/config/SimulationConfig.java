/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.tools.simulation.model.config;

import java.time.Instant;
import java.util.List;

public class SimulationConfig {
    private String name;
    private Instant startTime;
    private Instant endTime;
    private int delayMilliseconds;
    private List<AgentConfiguration> agents;
    private ConnectionConfiguration registryConnection;
    private ConnectionConfiguration cassandraConnection;
    private ConnectionConfiguration elasticsearchConnection;
    private SenderCredentialsConfiguration sender;
    private BridgeConfiguration bridgeConnection;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    public Instant getEndTime() {
        return endTime;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }

    public int getDelayMilliseconds() {
        return delayMilliseconds;
    }

    public void setDelayMilliseconds(int delayMilliseconds) {
        this.delayMilliseconds = delayMilliseconds;
    }

    public List<AgentConfiguration> getAgents() {
        return agents;
    }

    public void setAgents(List<AgentConfiguration> agents) {
        this.agents = agents;
    }

    public ConnectionConfiguration getRegistryConnection() {
        return registryConnection;
    }

    public void setRegistryConnection(ConnectionConfiguration registryConnection) {
        this.registryConnection = registryConnection;
    }

    public ConnectionConfiguration getCassandraConnection() {
        return cassandraConnection;
    }

    public void setCassandraConnection(ConnectionConfiguration cassandraConnection) {
        this.cassandraConnection = cassandraConnection;
    }

    public ConnectionConfiguration getElasticsearchConnection() {
        return elasticsearchConnection;
    }

    public void setElasticsearchConnection(ConnectionConfiguration elasticsearchConnection) {
        this.elasticsearchConnection = elasticsearchConnection;
    }

    public SenderCredentialsConfiguration getSender() {
        return sender;
    }

    public void setSender(SenderCredentialsConfiguration sender) {
        this.sender = sender;
    }

    public BridgeConfiguration getBridgeConnection() {
        return bridgeConnection;
    }

    public void setBridgeConnection(BridgeConfiguration bridgeConnection) {
        this.bridgeConnection = bridgeConnection;
    }

    @Override
    public String toString() {
        return "SimulationConfig{" +
               "name='" + name + '\'' +
               ", startTime=" + startTime +
               ", endTime=" + endTime +
               ", delayMilliseconds=" + delayMilliseconds +
               ", agents=" + agents +
               ", registryConnection=" + registryConnection +
               ", cassandraConnection=" + cassandraConnection +
               ", elasticsearchConnection=" + elasticsearchConnection +
               ", alerter=" + sender +
               ", bridgeConnection=" + bridgeConnection +
               '}';
    }
}
