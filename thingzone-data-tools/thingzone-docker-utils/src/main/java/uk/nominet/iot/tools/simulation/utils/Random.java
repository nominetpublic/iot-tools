/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.tools.simulation.utils;

import com.github.javafaker.Faker;

import java.util.ArrayList;
import java.util.List;

public class Random {
    public static Faker faker = new Faker();

    public static int randInt(int min, int max) {
        java.util.Random rand = new java.util.Random();
        return rand.ints(1, min, max).findFirst().getAsInt();
    }

    public static List<String> randomTags(int max) {
        ArrayList<String> tags = new ArrayList<>();
        int numberOfTags = randInt(0, max);

        for (int i = 0; i < numberOfTags; i++) {
            tags.add(faker.hipster().word());
        }

        return tags;
    }

    public static List<String> randomPeople(int max) {
        ArrayList<String> tags = new ArrayList<>();
        int numberOfTags = randInt(1, max);

        for(int i = 0; i < numberOfTags; i++) {
            tags.add(faker.starTrek().character());
        }

        return tags;
    }

    public static String randomFromList(List<String> list) {
        return list.get(randInt(0, list.size()));
    }

    public static double randDouble(double min, double max) {
        java.util.Random rand = new java.util.Random();
        return rand.doubles(1,min,max).findFirst().getAsDouble();
    }


}
