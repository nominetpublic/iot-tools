/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.tools.simulation;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import uk.nominet.iot.config.*;
import uk.nominet.iot.driver.CassandraDriver;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.tools.simulation.agent.Agent;
import uk.nominet.iot.tools.simulation.model.config.AgentConfiguration;
import uk.nominet.iot.tools.simulation.model.config.SimulationConfig;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

public class SimulationLoader {
    private SimulationConfig configuration;
    private final static JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
    public List<Agent> agents;

    public SimulationLoader(SimulationConfig configuration) {
        this.configuration = configuration;
        agents = new ArrayList<>();

        //Initialise drivers
        if (!RegistryAPIDriver.isInitialized()) {
            RegistryDriverConfig registryConfig = new RegistryDriverConfig();
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_SCHEME, configuration.getRegistryConnection().getScheme());
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST, configuration.getRegistryConnection().getHost());
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT, configuration.getRegistryConnection().getPort());
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_USERNAME,configuration.getRegistryConnection().getUsername());
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD,configuration.getRegistryConnection().getPassword());
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS, 3);
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }

        if (!CassandraDriver.isInitialized()) {
            CassandraDriverConfig cassandraConfig = new CassandraDriverConfig();
            cassandraConfig.addProperty(CassandraConstant.CASSANDRA_SERVER_HOST,configuration.getCassandraConnection().getHost());
            cassandraConfig.addProperty(CassandraConstant.CASSANDRA_SERVER_PORT,configuration.getCassandraConnection().getPort());
            CassandraDriver.setConnectionProperties(cassandraConfig);
            CassandraDriver.globalInitialise();
        }

        if (!ElasticsearchDriver.isInitialized()) {
            ElasticsearchDriverConfig elasticsearchConfig = new ElasticsearchDriverConfig();
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_SCHEME,configuration.getElasticsearchConnection().getScheme());
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_HOST, configuration.getElasticsearchConnection().getHost());
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_PORT,configuration.getElasticsearchConnection().getPort());
            ElasticsearchDriver.setConnectionProperties(elasticsearchConfig);
            ElasticsearchDriver.globalInitialise();
        }

        // Define agents
        configuration.getAgents().forEach(agentConfig -> {
            try {
                Class<Agent> agentClass = (Class<Agent>)Class.forName(agentConfig.getKlass());

                Class[] types = {AgentConfiguration.class};
                Constructor constructor = agentClass.getConstructor(types);

                Object[] parameters = {agentConfig};
                Agent agentInstance =  (Agent)constructor.newInstance(parameters);

                agents.add(agentInstance);

            } catch (Exception e) {
                e.printStackTrace();
                System.exit(1);
            }
        });
    }


    public static SimulationLoader loadFromJsonFile(String configurationFilePath) throws Exception {
        BufferedReader br = new BufferedReader(new FileReader(configurationFilePath));
        JsonParser parser = new JsonParser();
        JsonObject simulationConfigJson = parser.parse(br).getAsJsonObject();

        SimulationConfig simulationConfig = jsonStringSerialiser.readObject(simulationConfigJson, SimulationConfig.class);
        return new SimulationLoader(simulationConfig);
    }

    public SimulationConfig getConfiguration() {
        return configuration;
    }

    public List<Agent> getAgents() {
        return agents;
    }
}
