/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.tools.simulation.utils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.ByteArrayPartSource;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;

import java.io.IOException;

public class ThingzoneBridge {

    /**
     * Upload image to teh image server and get a reference in return
     * @param uri uri of the imageserver upload method
     * @param image image data
     * @return reference key
     */
    public static String uploadImageToServer(String uri, byte[] image) {
        HttpConnectionManager httpConnectionManager = new MultiThreadedHttpConnectionManager();
        HttpClient httpClient = new HttpClient(httpConnectionManager);

        PostMethod postMethod = new PostMethod(uri);


        ByteArrayPartSource byteArrayPartSource = new ByteArrayPartSource("file", image);
        FilePart filePart = new FilePart("file", byteArrayPartSource);
        Part[] parts = {filePart};
        try {
            MultipartRequestEntity multipartRequestEntity = new MultipartRequestEntity(parts, postMethod.getParams());
            postMethod.setRequestEntity(multipartRequestEntity);
            httpClient.executeMethod(postMethod);

            JsonObject responseJson = new Gson().fromJson(postMethod.getResponseBodyAsString(), JsonObject.class);
            String reference = responseJson.get("name").getAsString();
            return reference;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
