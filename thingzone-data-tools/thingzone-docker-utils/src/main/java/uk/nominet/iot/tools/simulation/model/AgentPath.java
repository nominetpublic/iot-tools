/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.tools.simulation.model;


import com.mapbox.geojson.LineString;

public class AgentPath {
    private LineString line;
    private Repeat repeat;
    private double stepInMeters;

    public LineString getLine() {
        return line;
    }

    public void setLine(LineString line) {
        this.line = line;
    }

    public Repeat getRepeat() {
        return repeat;
    }

    public void setRepeat(Repeat repeat) {
        this.repeat = repeat;
    }

    public double getStepInMeters() {
        return stepInMeters;
    }

    public void setStepInMeters(double stepInMeters) {
        this.stepInMeters = stepInMeters;
    }

    @Override
    public String toString() {
        return "AgentPath{" +
               "line=" + line +
               ", repeat=" + repeat +
               ", stepInMeters=" + stepInMeters +
               '}';
    }
}
