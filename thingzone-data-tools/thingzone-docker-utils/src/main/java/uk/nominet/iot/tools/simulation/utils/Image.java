/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.tools.simulation.utils;

import uk.nominet.iot.tools.simulation.agent.Agent;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

public class Image {
    /**
     * Overlay two images on top of each other
     * @param background background image
     * @param foregroundImages foreground images
     * @param type       type of image jpeg, png
     * @param x          number of pixels to move the foreground image in the x axis
     * @return bufferend image
     */
    public static BufferedImage overlay(BufferedImage background, List<Agent> foregroundAgents, String type, int x) {

        try {
            int imageType = "png".equalsIgnoreCase(type) ? BufferedImage.TYPE_INT_ARGB : BufferedImage.TYPE_INT_RGB;
            BufferedImage overlay = new BufferedImage(background.getWidth(), background.getHeight(), imageType);

            Graphics2D g = overlay.createGraphics();
            int compositeRule = AlphaComposite.SRC_OVER;
            float alpha = 1.0f;
            AlphaComposite ac = AlphaComposite.getInstance(compositeRule, alpha);
            g.drawImage(background, 0, 0, null);
            g.setComposite(ac);

            foregroundAgents.forEach(agent -> {
                if (agent.image != null) {
                    int heightDifference = background.getHeight() - agent.image.getHeight();
                    int widthDifference = background.getWidth() - agent.image.getWidth();
                    int leftShift = (int) (widthDifference / 2) + x + agent.configuration.getImageOffset();

                    g.drawImage(agent.image, leftShift, heightDifference, null);
                    g.setComposite(ac);
                }
            });

            return overlay;
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Generate image thumbnnail
     * @param image image buffer
     * @param width width
     * @param height height
     * @return thumbnail image buffer
     */
    public static BufferedImage thimbnail(BufferedImage image, int width, int height) {
        BufferedImage thumbnail = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D tGraphics2D = thumbnail.createGraphics(); //create a graphics object to paint to
        //tGraphics2D.setBackground( Color.WHITE );
        //tGraphics2D.setPaint( Color.WHITE );
        //tGraphics2D.fillRect( 0, 0, width, height );
        tGraphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        tGraphics2D.drawImage(image, 0, 0, width, height, null); //draw the image scaled
        return thumbnail;
    }

    /**
     * Convert image to base 64 string
     * @param image image buffer
     * @return base 64 string
     */
    public static String toBase64String(BufferedImage image) throws IOException {
        final ByteArrayOutputStream os = new ByteArrayOutputStream();

        ImageIO.write(image, "PNG", os);
        return Base64.getEncoder().encodeToString(os.toByteArray());

    }

    /**
     * Convert image to base 64 string
     * @param image image buffer
     * @return base 64 string
     */
    public static byte[] toBytes(BufferedImage image) throws IOException {
        final ByteArrayOutputStream os = new ByteArrayOutputStream();

        ImageIO.write(image, "PNG", os);
        return os.toByteArray();
    }


}
