/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {JavascriptExecutor} from "./JavascriptExecutor";
import {OutputHandler} from "./OutputHandler";

import kafka = require("kafka-node");
import * as config from '../config.js' ;
import {ExecutionTask, ExecutionTaskOutput} from "./model/thingzone-data-model";

const client = new kafka.KafkaClient({kafkaHost: config.kafkaHost});

const consumer = new kafka.Consumer(client,
    [
        {topic: config.inputTopic, partition: 0}
    ],
    {
        groupId: 'thingzone-javascript-sandbox',
        autoCommit: true,
        autoCommitIntervalMs: 5000,
        encoding: 'utf8',
        keyEncoding: 'utf8'
    }
);

const producer = new kafka.Producer(client);

consumer.on('message', function (message) {

    console.log("Processing task: ", message.key);

    let task: ExecutionTask = JSON.parse(message.value);

    JavascriptExecutor.runSandboxedTask(task, message.key).then(taskOutpuit => {
        let outputMessages = [
            {topic: config.outputTopic,
                key: taskOutpuit.messageKey,
                messages: JSON.stringify(taskOutpuit.output),
                partition: 0}
        ];

        producer.send(outputMessages, function (err, data) {
            console.log(err, data);
        });
    });


});


