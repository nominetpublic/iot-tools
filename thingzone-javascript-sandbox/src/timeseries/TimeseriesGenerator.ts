/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
    AbstractTimeseriesPoint, Event, Feedback, Geometry, Image, LatLong,
    MetricDouble,
    MetricDoubles,
    MetricInt, MetricInts, MetricLatLong, MetricString, MetricStrings,
    QualityIndicator, ThingzoneEventType
} from "../model/thingzone-data-model";

import getUuidByString = require("uuid-by-string");
import uuidv4 = require('uuid/v4');

export class TimeseriesGenerator {
    private pointBuilder(klass: string,
                         timestamp: Date,
                         metadata?: { [index: string]: any },
                         quality?: QualityIndicator[],
                         keyForId?: string): AbstractTimeseriesPoint {

        let point:any = {
            timestamp: timestamp,
            klass: klass,
            id: (keyForId) ? getUuidByString(keyForId) : uuidv4(),
        };

        if (metadata) point.metadata = metadata;
        if (quality) point.quality = quality;
        return point;

    }

    public double(timestamp: Date,
                  value: number,
                  metadata?: { [index: string]: any },
                  quality?: QualityIndicator[],
                  keyForId?: string): AbstractTimeseriesPoint {
        let point: MetricDouble = {
            ...this.pointBuilder('uk.nominet.iot.model.timeseries.MetricDouble', timestamp, metadata, quality, keyForId),
            value_double: value
        };

        return point;
    }

    public status(timestamp: Date,
                  value: number,
                  metadata?: { [index: string]: any },
                  quality?: QualityIndicator[],
                  keyForId?: string): AbstractTimeseriesPoint {
        let point: MetricDouble = {
            ...this.pointBuilder('uk.nominet.iot.model.timeseries.Status', timestamp, metadata, quality, keyForId),
            value_double: value
        };

        return point;
    }

    public doubleList(timestamp: Date,
                      value: number[],
                      metadata?: { [index: string]: any },
                      quality?: QualityIndicator[],
                      keyForId?: string): AbstractTimeseriesPoint {
        let point: MetricDoubles = {
            ...this.pointBuilder('uk.nominet.iot.model.timeseries.MetricDoubles', timestamp, metadata, quality, keyForId),
            value_list_double: value
        };

        return point;
    }

    public int(timestamp: Date,
               value: number,
               metadata?: { [index: string]: any },
               quality?: QualityIndicator[],
               keyForId?: string): AbstractTimeseriesPoint {
        let point: MetricInt = {
            ...this.pointBuilder('uk.nominet.iot.model.timeseries.MetricInt', timestamp, metadata, quality, keyForId),
            value_int: value
        };

        return point;
    }

    public intList(timestamp: Date,
                   value: number[],
                   metadata?: { [index: string]: any },
                   quality?: QualityIndicator[],
                   keyForId?: string): AbstractTimeseriesPoint {
        let point: MetricInts = {
            ...this.pointBuilder('uk.nominet.iot.model.timeseries.MetricInts', timestamp, metadata, quality, keyForId),
            value_list_int: value
        };

        return point;
    }

    public latLong(timestamp: Date,
                   lat: number,
                   long: number,
                   metadata?: { [index: string]: any },
                   quality?: QualityIndicator[],
                   keyForId?: string): AbstractTimeseriesPoint {

        let coordinates: LatLong = {
            lat: lat,
            lon: long
        };

        let point: MetricLatLong = {
            ...this.pointBuilder('uk.nominet.iot.model.timeseries.MetricLatLong', timestamp, metadata, quality, keyForId),
            value_lat_long: coordinates
        };

        return point;
    }


    public text(timestamp: Date,
                value: string,
                metadata?: { [index: string]: any },
                quality?: QualityIndicator[],
                keyForId?: string): AbstractTimeseriesPoint {
        let point: MetricString = {
            ...this.pointBuilder('uk.nominet.iot.model.timeseries.MetricString', timestamp, metadata, quality, keyForId),
            value_text: value
        };

        return point;
    }

    public textList(timestamp: Date,
                    value: string[],
                    metadata?: { [index: string]: any },
                    quality?: QualityIndicator[],
                    keyForId?: string): AbstractTimeseriesPoint {
        let point: MetricStrings = {
            ...this.pointBuilder('uk.nominet.iot.model.timeseries.MetricStrings', timestamp, metadata, quality, keyForId),
            value_list_text: value
        };

        return point;
    }

    public image(timestamp: Date,
                 imageRef: string,
                 metadata?: { [index: string]: any },
                 quality?: QualityIndicator[],
                 keyForId?: string): AbstractTimeseriesPoint {
        let point: Image = {
            ...this.pointBuilder('uk.nominet.iot.model.timeseries.Image', timestamp, metadata, quality, keyForId),
            imageRef: imageRef
        };

        return point;
    }

    public event(timestamp: Date,
                 name: string,
                 eventType: ThingzoneEventType,
                 description: string,
                 category: string,
                 startTimestamp: Date,
                 endTimestamp: Date,
                 location: Geometry<any>,
                 metadata?: { [index: string]: any },
                 type?: string,
                 value?: number,
                 quality?: QualityIndicator[],
                 keyForId?: string): AbstractTimeseriesPoint {
        let point: Event = {
            ...this.pointBuilder('uk.nominet.iot.model.timeseries.Event', timestamp, metadata, quality, keyForId),
            name: name,
            eventType: eventType,
            description: description,
            category: category,
            receivers: null,
            configuration: null,
            feedback: null,
            severity: 0,
            status: null,
            relatedTo: null,
            derivedFrom: null,
            startTimestamp: startTimestamp,
            endTimestamp: endTimestamp,
            location: location,
            type: type,
            value: value
        };

        return point;
    }


}
