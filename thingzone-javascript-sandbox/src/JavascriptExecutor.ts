/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {OutputHandler} from "./OutputHandler";
import {VM, VMScript} from "vm2";
import {ExecutionTask, ExecutionTaskOutput} from "./model/thingzone-data-model";
import * as check from "syntax-error";
import {TimeseriesGenerator} from "./timeseries/TimeseriesGenerator";
import {Base64} from "./utils/Base64";
import {ProtoBuf} from "./utils/ProtoBuf";


class ExecutionTaskOutputMessage {
    public output: ExecutionTaskOutput;
    public messageKey: string;
}

export class JavascriptExecutor {



    /**
     * Run a sanboxex execution task
     * @param task execution task
     */
    static async runSandboxedTask(task:ExecutionTask, messageKey:string):Promise<ExecutionTaskOutputMessage> {
        let output = new OutputHandler(task);
        // Define basic VM
        let vm = new VM ( {
            timeout: 1000,
           // wrapper: "none",
            sandbox: {
                sources: task.sources,
                parameters: task.parameters,
                metadata: task.metadata,
                result: output
            }
        });


        var promise = Promise.resolve();

        // Add additional proxied modules
        vm.freeze(new TimeseriesGenerator(), 'Point');
        vm.freeze(promise, 'promise');
        vm.freeze(Base64, 'Base64');
        vm.freeze(ProtoBuf, 'ProtoBuf');


        // Check if the script is missing
        if (!task.script) {
            output.log.error('Missing transform script');
            return {
                output: output.taskOutput,
                messageKey: messageKey
            }
        }

        // Compile and syntax check script
        try {
            var script = new VMScript(task.script.code).compile();
        } catch (err) {
            // Using different error checker for better output
            var err = check(task.script.code, task.transformKey+'/'+task.script.name);
            if (err) output.log.error(err.toString());
        }

        // Run the script
        var result;
        try {
            result = vm.run(script);
        } catch (err) {
            output.log.error(err.toString());
        }

        return Promise.resolve(result).then( () => {
            return {
                output: output.taskOutput,
                messageKey: messageKey
            }
        });
    }


}
