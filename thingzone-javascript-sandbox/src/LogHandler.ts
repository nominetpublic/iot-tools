/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {LogEntry} from "./model/thingzone-data-model";

/**
 * Handle task execution logs
 */
export class LogHandler {
    private _logs: LogEntry[];

    constructor(logEntries: LogEntry[]) {
        this._logs = logEntries;
    }


    /**
     * Append long entries to log sequence
     * @param entryType type of log info, error or warn
     * @param logString log string
     */
    private appendLog(entryType: string, logString: string) {
        if (logString.length > 0) {
            this._logs.push({
                timestamp: new Date(),
                entryType: entryType,
                message: logString
            })
        }
    }

    /**
     * Log info level messages
     * @param messages info messages
     */
    public info(...messages: any[]) {
        let logString = messages.filter(msg => (typeof msg !== 'undefined' && msg !== null)).join(' ');
        this.appendLog('info', logString);
    }

    /**
     * Log error level messages
     * @param messages error messages
     */
    public error(...messages: any[]) {
        let logString = messages.filter(msg => (typeof msg !== 'undefined' && msg !== null)).join(' ');
        this.appendLog('error', logString);
    }

    /**
     * Log warning level messages
     * @param messageswarning messages
     */
    public warn(...messages: any[]) {
        let logString = messages.filter(msg => (typeof msg !== 'undefined' && msg !== null)).join(' ');
        this.appendLog('warn', logString);
    }
}
