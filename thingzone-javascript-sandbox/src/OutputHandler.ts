/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {AbstractTimeseriesPoint, ExecutionTask, ExecutionTaskOutput, LogEntry} from "./model/thingzone-data-model";
import {uuid} from "uuid/v1";
import {LogHandler} from "./LogHandler";

/**
 * Handle the output of a thingzone task
 */
export class OutputHandler {

    public transformKey:string;
    public inputs: string[];
    public transformStream: AbstractTimeseriesPoint;
    public streams: { [index: string]: AbstractTimeseriesPoint };
    private logs: LogEntry[];

    private _log:LogHandler;

    constructor(task:ExecutionTask) {
        //Initialise output aliases
        if (task.outputAliases) {
            this.streams = {};
            task.outputAliases.forEach(alias => this.streams[alias]=null);
        }

        //Prevent the streams obejct to be extended
        Object.preventExtensions(this.streams);
        this.logs = [];
        this._log = new LogHandler(this.logs);
        this.transformKey = task.transformKey;

        var sourcesMD = Object.keys(task.sources).map(source => {
            if (Array.isArray(task.sources[source])) {
                return  task.sources[source].map(item => item.id);
            } else {
                return task.sources[source].id
            }
        });

        this.inputs = [].concat.apply([], sourcesMD);
    }

    /**
     * Get the execution task output
     */
    get taskOutput(): ExecutionTaskOutput {
        return {
            inputs: this.inputs,
            transformKey: this.transformKey,
            transformOutput: this.transformStream,
            streamOutputs: this.streams,
            logs: this.logs
        }
    }

    /**
     * Get a reference to the log handler
     */
    get log(): LogHandler {
        return this._log;
    }

}
