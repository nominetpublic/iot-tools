/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {OutputHandler} from "../src/OutputHandler";
import {ExecutionTask} from "../src/model/thingzone-data-model";
import {expect, assert} from 'chai';
import {use} from 'chai';

import chaiExclude = require('chai-exclude');
import {TimeseriesGenerator} from "../src/timeseries/TimeseriesGenerator";

use(chaiExclude);

let task: ExecutionTask = {
    transformKey: 'device1.stream1',
    parameters: {
        foo: 123,
        bar: 123
    },
    sources: {
        alias1: {
            'some': 'data'
        },
        alias2: {
            'some': 'other data'
        }
    },
    outputAliases: ['foo', 'bar'],
    metadata: null,
    script: {
        name: 'testScript',
        type: 'transform',
        code: 'log.info("Hello World");'
    }
};

let timestamp = new Date();

describe('Logging', function () {

    it('can create log entries', () => {
        let output = new OutputHandler(task);
        output.log.info('this', 'is', 'a test', 'message');
        output.log.error('this', 'is', 'a test', 'message');

        expect(output.taskOutput.logs[0].message).to.equal('this is a test message');
        expect(output.taskOutput.logs[0].entryType).to.equal('info');
        expect(output.taskOutput.logs[0].timestamp).not.to.be.null;

        expect(output.taskOutput.logs[1].message).to.equal('this is a test message');
        expect(output.taskOutput.logs[1].entryType).to.equal('error');
        expect(output.taskOutput.logs[1].timestamp).not.to.be.null;

    });

    it('should not log a null message', function () {
        let output = new OutputHandler(task);
        output.log.info(null);

        expect(output.taskOutput.logs).to.be.empty;
    });

    it('should not log a undefined message', function () {
        let output = new OutputHandler(task);

        output.log.info(undefined);
        expect(output.taskOutput.logs).to.be.empty;
    });

});

describe('Sandbox timeseries', function() {
    let point = new TimeseriesGenerator();

    it('can assign a double to the transform stream', function() {
        let output = new OutputHandler(task);
        output.transformStream = point.double(timestamp, 1.5);

        expect(output.taskOutput.transformOutput).excluding('id').to.deep.equal(
            {   timestamp: timestamp,
                klass: 'uk.nominet.iot.model.timeseries.MetricDouble',
                value_double: 1.5 }
        );

    });


    it('can assign a double with custom key for id the transform stream', function() {
        let output = new OutputHandler(task);
        output.transformStream = point.double(timestamp, 1.5, null, null, "12345");
        expect(output.taskOutput.transformOutput.id).to.equal("23D19B30-45E4-4EC3-B469-D36E18AB097D");
    });


    it('can assign a double to an output stream alias', function() {
        let output = new OutputHandler(task);
        output.streams.foo = point.double(timestamp, 1.5);

        expect(output.taskOutput.streamOutputs.foo).excluding('id').to.deep.equal(
            {   timestamp: timestamp,
                klass: 'uk.nominet.iot.model.timeseries.MetricDouble',
                value_double: 1.5 }
        );
    });

    it('fails when assigning a timeseries payload to an invalid output stream alias', function() {
        let output = new OutputHandler(task);

        expect(function() {
            output.streams.invalid = point.double(timestamp, 1.5)
        }).to.throw('Cannot add property invalid, object is not extensible');
    });


});

