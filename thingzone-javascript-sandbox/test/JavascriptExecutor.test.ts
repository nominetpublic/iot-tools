/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {ExecutionTask, ExecutionTaskOutput} from "../src/model/thingzone-data-model";
import {expect} from 'chai';
import {use} from 'chai';
import chaiExclude = require('chai-exclude');
import {JavascriptExecutor} from "../src/JavascriptExecutor";

use(chaiExclude);

let task: ExecutionTask = {
    transformKey: 'device1.transfrom1',
    parameters: {
        foo: 123,
        bar: 123
    },
    sources: {
        alias1: {
            'some': 'data'
        },
        alias2: {
            'some': 'other data'
        },
        image1: {
            'base64Data': '/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUTEBIVFRUVGBUVFhYVFhUWFRUVFRUXGBUVGBUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLi0BCgoKDg0OFw8QFS0dFR0tKysrKy0tLS0tKy0tLSstLS0rLS0rLS0tLS0tLTc3Ky0tKy03Ny0rKy03NzcrKysrK//AABEIALEBHAMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAADAQIEBQYABwj/xAA+EAABAwIEBAMHAgQFAwUAAAABAAIRAyEEEjFBBQZRYSJxgQcTMpGhsfBCwTNSctEUI2Ky4YKi8TRDg5LS/8QAGAEAAwEBAAAAAAAAAAAAAAAAAAECAwT/xAAfEQEBAQEAAgMBAQEAAAAAAAAAARECITESQVEDMhP/2gAMAwEAAhEDEQA/APJAFzmojWri1RrGe0YtStYnvICNQoFwYQRNRxaBMaRcnQDX5IaD8LZNRq3fC2wATpmWG4c8NqAOMQSD5hbfhTTYDciExx6WNVsTANnEeh0VlVYIZAjw/VQy6TUB2jbcKTXc7Ky4Dbjue6FDigBDi7wgSZuAkwZJlzYLd5/YIXEKgbQOR/iFndCPJA4XiMlMRqVHVVI0dKn4dj+yznMfF2Ug9tswggdfVTuJ8T91QcYtBk6x3Xj3G+Jue8yZHSSQPJacfqevxb47m6r+l0QZHrsq8c11oc3Ob6djrZZ57pTAVpekzmPSeVuZzXPuax8UeB3WNQe6lcUYvNMNXLHBzTBaQQe4XqPCcHWx1AVaLc2zhIEO31UXyfplazbrmhaZnI2OqOj3OXu5zQPpKtcL7MMSRL6lNp6DM76woyq2MQAkIW2xns2xbGywsqHcAlp9J1VbieSMcwSaM/0uafoleaJYzBanNan16LmGHtLT0cCD9U1pWaz2tSlqVpSlBAlqTIildCDR3MQKjFOcFHqtTJCc1IGojgmhAcGp2VK1PAVEYWJpajQkIQEctTMqO4JkIAATajoT1DxlTZVI5ufNCo4iKjXG4DgfkdVYV6z8RXdRpEFr6hNPYAyfHPcTP9RQMO+nmoAtBAM1JA8RLrgkbfm60/M78O11Kth2BlRhAdl0e2CJgDXS/RW3UeM4FiWYmnQqtDKroDTPhdf4s25vHoFfUOKVcBW93jad7EFkQRYTbXTzlQeLcwOquov/AFU3ZhPlH9k2rxj31ek+p+gmSRMAxa6A3fDMfTrve6k8Oa4E+XmNlYktLaeYxqCY2WIwmNYziDTSADazYeBYA3gwPJa01hZpnUwToEqDONOBgU2uE/8AcOq7Asu0QYkT0VoKDa38WBlFiLG/ZOwGGyGNVlfa5WV9oFdzQCx2tjBA/wDIXmNd0m633tHZFTTUbafQrz2otp6QGUgSlSeG8OqV35aTC47xGnrZMGYbDvqENY0uJ2An8C+ifZZy4/CYaKk5nnMWn9MjRVXs+5Bbh8taoPHaxFxbodO69Lp040StwhQxLlXZkMlAFSwChAp4cmSu4vwKhiGltam107xceR1C8w5k9nNak4vwv+Yz+T9bf/0vYmlNeyUrJfZyvnTE8Lr0v4lJ7fNpj5qlxvFGssLn6Bexe1z3zcIfczMwcovF189E9UvhDlqaeJvmcxRMLxFwNzKrClaU8ga2hig4DYlLVCoMDVIcIV/mkKOucOVEqBMRagQSs1HtTwhtRGp6Dglhc1OhOEC8JiM8IJRRAMqiV6cvAVoGKuxoyva5ayOfj2u+O8Jw7cOx9ExVaAXCbOnXyhZ99ZzgBJspmJxGZpg2O3dF5f4f72oAdJuRshugiiTf+6lYbAkkd+xgLY8Qw1CgMoaH2uXRDZsPzsqp/GRTAYwDY6R9UBRYsmjUD2H4SII0Xo+EearKFVpaQ7Xz8lU8HpDFkscwOp5SHbAToR1Kl8CoOwxqYR5sxwq0j/NTfY+odMos+yauu4BwGhhFw9QOcD03QXulrZCdS1EaH5LKqZ32jcIdUZ7xjc2WZgbdbLyOo26+i6cGYMjp1VNjeUcLXfJoNLibkSPmAtOfxN8PJeXuVq+LP+W0hm74JA7ea9m9nvJP+DBe8gudrYi3ktLwXgVOgxoawNAFgNArlqv0ndPpgBPzJtlD4pxajh2l9Z4aB1KnDTlypOD8y4fEmKFUOvEXB+RV2FWFpwTXFOTHBIHMciyo7SiNKAhcZwoqU3NIBkHXRfL3OHCDhcQ5hFiSRrfrqvq54leR+2Tl1rqZrsa7M0ScoBBjz2iTZMPECU5qanBSpJw5utBhD4Vn8OCtBhhDQjv0I6ogFHeglYVccE9qYE8JGK1PTGlPCuJplRAKkVEAoog9MKu43T8IPdTqL1F41doC6HNz7VuCplxHSVuMPRp0aJ92fGYvMaxIvosdgTBH5+aqwNSJkkjaT1UN0niuMLjlEwJvrmPnuqzLmgme6eynmMEneylCgWi/qO1yg2k5PxzKYDDY6mf1flkvMfFG08TQeIFnh0Ro4tEf9v0WRxWKDdARH5qqx+Mc6o1zjMEa9jKCe5vxTXtYWyLCfl0RW1pGXZRMBWY9jXAfE1hEdwpj2NbHeyzqiUKkGLrTcFw8DMRBPzhVuGwmhLQegV5hqn+n01W3HOTWXXW+E8JcqCJCBxPiApUy8iY20lMG8Y4ozDUnVKrg0NBNyBPYL5w5l4/WxuILnE3dlY28CSAAB3srbnnmmpjKkXaxs+Gd/L0Wdw1AmzTmk2AzZp0tF+2iMNo/Zi+qziLKcQZPvJBloZINtnZjF/2X0VT0WE9nHJP+Dpe+rtivUAzAmSxuoZN73vG/ktng8RmkGxCCSyoHFMc2iwue4ADcqS6qJgLzD251XihRDbD3hnuMjggLjhftKwdat7oOc28BzhAcZiy3THSF8j1qTqZaSdQHggg2PkTB7G6+ivZrxh2IwVNzrub4CepG6RtjKpOa+Hmvh3sESRZW4ckfdEFfJ/MHCH4as5jgQJsTN/VVzAvZ/a5yyHtGIbIcLOj4XDbMOo2PdeTs4Y+dErkpy+DMIwk2V61sCELC4QMHdFcVn11qoG5DKe5DKzURPCanBICNKIChtCIArhU16AUZ6AUUQlIrsddqSmiVB4TK2jm+1dQ1AUiu8W3O2mnf5KvqVCD028gpuBqgQYsCJ8juit1nwjh1WsJY11twD912Lwzm2No7i503W04PxRrWjKbLK83Vx70lsXh3rEGeqRsti3ySopR6l5UeEybHlLmz3BDaxJYIAi5AFgPqpnF+dahf/knwC8EarBSjNqzY2RMFe68q8xf4tmsEAToRPlstThqgaJLj+fReJcg8Xp0nFr873HQSA0emrvtZewcKxDXNa57QJ+Ueq1t2Mcyr3C4xrrAz5GVS874J1fDuayJiRrNvJXTHDKMkAegCWrJb8V1CnzZV4NiC/L7p5Lpg6hxGsdT2Xqvsx5D9wW4nE3q6sZsy2pH832W2wWGaHF2QCdSLT6D91PoPBJ6KsLUt2iralK5I3UyriWi0oFM32SMTBYcNCyXtg4camAc9gM0nseYJHhPheSNCAHEwei21MWso3EaAqU303iQ9rmkdiIQNfKmIoG8ZRMTAjTsSvTfY9xlzC7Duu03HUdbLz3jeDdQr1KRnwki4IkTAPlY33V97PKDnYluVpib7D5whT6FaJXEIdGwCLmSJkvaNh82EeQJgg+V9V4w1e98z0c+Hqt6tdrbZeBGxv95+oWH9vqlKc5BcU8uQnFRzWsMJTUpTU6oqc1NRGBEAjQnprQnlUkF6CUd6CQlVQOipJFlEolS5steXLVFjB4ilwleJGxtdH4hTUBgTbS+Flh+IVKdmmyZi8UX6mTv8tFG93b6eqeGRqJm6MPTahGyBVbf6opkkwkqndAAKaUQhNIQEnhmJLKjTbUaid+m69i4TxA1MrviBidtRIEADKLdl4nlV7y7x11F2UkQTq68aXN9AAqlT1H0HgMcIAzCToBBPkBKmOq2Ob1XmWD4sxtRuWTmguJMDsddFr8ViLQZcD0Nvl+pNKxbxNjTGbXQb/JVnM3Gq1Gi+rh2tc5jQYcDoHDPpf4Z+Sy3F8aaQJbZx3sXdj/xsgcN5mY+WvqAGYOkEmIk2ub2R8jnJnDPak8kjE0rf6DfzE6/NXWD9pOG1Ocf1NJ/2rznmjgga81KMBpl0DSd8sbKJwXl6riXABwa3qZiPIa66Sj5U/jHvvL/OOGrHJTqguucpPigCSY/daBtYOuF5xyly3h8EMwJfULYL3QLWJaG6AT3kwFq6PFm6T28k0WK3m3k3DYt2c0y1+7mmJ6zGv3U7gPL9LDNAptiPM363UtuJD9xa/p5ahE/xo0tPrp5pURLZUHUT20XPdH5ZQxVadr/f1RM9o2+oSiiY0B7HAmJB3XgvMdFlGs5orNqEknw7T1XpntA4n7vDv93WDJBaSCDlJ7TZeBGoSZJul3zLMpc+9aBjwdClKrMHWjVWNOqCsbxjWVzgmwiEJMqVM0BFakaEQBApwSFKkIRqQ3oaM4IZCVXEGk5SmvUFhRPeLWMLydiRIVY5t1Y55USuFR8mN+n3RC7ToNkJhlPOypRA6NELdPMAlMlIy1GwhFEcbobkg5McnBIQgLThXFC0+K41ANxP3AsNOi9b4dxAOwzSQKdpAvPrcyfVeIsEEH/n6L0PgWOAb/EaDAGbKYgDTOd97KomrHjDszHOcbkb720INwsNgKWZlcjVsEfMSt9WeHU3AEusT8MAg3kzoFjeWmkuxLQJlp79f7IsV/P2q3YlzTlDnZSIgkx5KRh+NV6cNpvI2GltVMx/DHyzwiY2FpgfJSuDcFa7O+oxxa20tE30t9Evi3+PnDW8zYsNcXPJAA6fEd9N9xotTynia1SXOtmAMDtuJ77Kg5hwDaZoYKld5IdUOpnefr8lveC4RlJgYSMwGkEeXhOuvZORj/TJci4Y50Aw4RadiPJGflbcwDraR3Tc5piXQ4egIk2MeaoMRjhTcSXS1zm2mwzeGR3zAAjv6oZNFhuKsccgd4tQL3HqELi3GxRGY3t1jfW/mFguJcepNcQ05KgPhIIgkzcTA2FpGpCyHM/NT8U3IWFkaiZHoPkfn1T3Bmm858wuxVYusABEgQXDo7qs5KalU26uTBWuU3BvM3Ve0otOogNCCkKhYSvNgphWfUwtOan50GU0uUUakhycFED0Vj0pDg5ahFOL0MuQ0irexzTlc0tPRwLT8jdMLl61jeHe8pZKhbVnUvDZgREPykgi5AO8XCy+O5EEE0ax/peAdr+Np8tlqjGLFRI4yrHjPLlfD+JwDm9Wz9Wm/wB9VUU3oLCkZU6m6/pP9giMp5p8lFYYN9JVSgVzfz880JEzaJHgJgwppK4rgEjIUkpSEiAQlS8BiC0gzcaWmPIKGn0kBueF8SnKKtS51Ezbv1N/JW/L3D/duqua0+8Jc1rctz4fDJNj8Zv2XnWFxWQ5pJO2m3n/AML0XlXmltSmKVUgPkljjYEgEnS4Pf8AuqlKeLrccR5ZdLYBPhDROpAi37eieOFNw9ISLNBcXE2kERm+pjsVDwXH6jQNz2g9DqCD9Pmhcx4s1aJOJqCkwgB4afEQDZs6/kK/k1/6YxXLlD3+IrYmpN3OyTu0Hwgd4hbvC8QbGbKDA8UTMdQe3TXXVYulxCmw5aY8IHhnexObNvIg/NQKXMByPkw5puR+pt8ro7R9FOsL5bTmLipawPDhlcC1riJEwfA8dDETpI7ry7jHH3+8qBjjkfBLTByu1IB89+iHxbmJ1RsNMTBI2m02029ZVC4yUrTkLVqFxJJJnWf3TYXQuUqcklcVyAVPaEwFLKAnYSpBsrZrrLPscrTB1wRCOpsR1EpyaSnFNLVjpElPa5DhdKcODZ0mZDBTgitI9UFVt8zr3t4ha1unyCE/GsBgOInaMwF9NT02CI7DaZsxHZwMd439JTW0Mpll9dRl/efUhaeAaa4u0m15An5xpHyUHiXAMNiGnOyH/pqMAD520EEX0NrKwLHG8sAta5k3BBJgIFJhbcyLiIbF+vfVSbA1eAvoVMriHNNg4WvAOUjY39VB49wv3QDhcmJjQL07HBtUQGF5d4TERBBHiOn7rEcbwlWiNPe0R+l3xsBFiHaka3M90aWMYHJxN0lSJlunfVMJVkVcClauTAoNr9kGo2DKdFlzr3QAylYUuyakD6lrJ9KtcfLyGsITWyn+76pBOZxeoGkNeRInXSyJiOJvcXZnE7XMwZ/v9lXZE7LbumFmziOam5hMOGXJ/wBOo9fv5qtOJPiBJFo9QRH2QhS/CudSPTTWeqNLAw5Hpsae32TW0TuInSVwZ0m3n80Ga6nl19D1TCpzmZmQ25G25HWPz6KFCASFyVIQgOC5clQCgotJ8FBTmlOEu6D7XRwVVYZx3KsWLL+nP3EuempzguhTFQgCckSoW9GAcZh4knVthHm2VKy2/wAwsJ08TomNNtVVUyXzbe9zU0HWYnsnUXumGuY4EkwWOtB2g3PZaBYAHUFgjUbb2A/53QziwBd7XNtLhpOw6b/dRqjDoXEkw67dp0+LTzHRFa8iWtymBJgZQDeQSJGsmEgWvVLzAJaIvlOUx56gx0UCvW92++Z1N4IuHOgzpAEwQCexlGMklznNBuBu0NNt9TvomQ2GtcSTNnT4XWsLbz9SgPPeY8CxlQuo/AdtY9eippXpnEsGKoLXWcyPHl2fNjsWmD6g9wvPuL4Q0qr2xYG3lt9ETr6HXOeUUFOadZQwnt3VpON9ErW2SsauNQDRMjHm8BOcANdUPN0JSOCRjUqgGoT6teToLxYWQG3RckHugHNqCUanlP4P3RMPw1z2l4BjsDYRPr/5V5guVqlRmamI7kxbYxE/m6Aom4Yk2a4/nZXnDMBAmIIsCQT8gPzyVpwTl+ox0PIzOBLWnQx13urirw6tnLXOHiEktaJ+U/D27ICjbwsOB/y2zuTqR+0oWP4I0Amm7M/Usa2QBveYn/haOjwog2bm28RJttbSbaxZWf8AhCBBAAtEXknqB/4RCeYv4USJZZ4tEa/sd9OioawIJB13Xr/EOFZ/0C36gIP7rI818shrDWpOktANRmpA/n7d/KUxrFAInZOFh3K7TXVIwzCQlcUkoBQngJgT2oA1NWGHf+Sq9ilUTexKrNTVhCQhEZdI4LKzCgS6VxTVK9ac4iq4AZmzrBLhtYGU7/G1m/FkP+p7gLzsMsgd1TU6jnGzwSNrCD5Ht9lKpMfMF/eMwgfIgdN91WrXuBx7IJeGybSXWtuBBJ9bI5qNtkqOgG7WlkHr03+UqooMBIblY4mImPqZJCmOLQPga3T4nMgG+m432GqCNFSoXT8P+pxJsTYAkGPSFBqVGe8Huy8vkWGjiD5wpeIw7XiSbGBYi5m1xpf7dEIUW05NOD2aYcZ22tNvRBp2Pc7+Ygn9JOZoh1xM23MdlnuO8PbUlzSHEASBOhBgjqLG6sKd3APq5RIcGOBa8mfhI/V5hPw1aA3M0EQ5oIkgtzHwyTG9ipv6rnz4edVKRBXNC2PMvCKbh72i2ALOA2PRzTosxSoTbdXz1qOucAc6LIJU9/DnShHCOkiE0ooT2BSHYQjY/LVWGH4Y4gWMm0X6JhEw7G6Fpc7QBpiSdJt1Vuzg9Uup5mlgc5ou28amSJ6G1v2Vtypyw99VhPgA8Rzfy2O2hg7xqNCvVaHDKQiWtygWETrrr1lBKnhnBGimz/LAECA2depnXWZUjA4N5Ja8QAbmNYMCfor2mQfh0mFxHinSdfSEjV9Thgc7Odb+Ufn3TBRbBc6D56QDa359VPryRlbvH9yg0cMM0uu0bbSjSUuDwEgugjM4nzvYdx/dTqWH6AHq47n8+6si4ETHl2E9E8NEAafsmFNVwcGS3MepufINUSthyf8A2WmReYm8yLWutC8jQD1/NFzWbD7aKomvAOZ+COw1UggBpcSwSDDTdoMTFvsVTFe486cDbiKUFxlsmzRJHQdD+QvEsXSyOI6GEWHKjFcnFNUqcnBNT2HtKAe1SsOVFaj0SqiatsOUR4QcG20zA6mAEPE41osDm7jRLqeSgpadghOchsxpdceHbufSUw1GjUqLFJvD9Pn90atofRcuUX21g1L+C70+4SP+I/0n7LlyZVc4P+H/APGP9rkfBat8v2XLkAzGfxKXr9lFw/8AD/6qn+9y5cpvpXP+jePa+izdL4ly5HI79rRvxNTW6/JcuWsZJv6j/TT/AN6ueB/HS/pd9wkXKiabhXxf/b7hXLPib+bFcuSoTqPw/ndPOg81y5SZ42/NlHr6H83XLkwa3Qef7prf2C5cmBW6n+pqcf3K5cqiahcU/huXh/Ov/qD/AEhcuRRyzqRcuUqKVzVy5APYjU/3XLlUKpeM0Hkq1yRcjoRIZsmDdcuUh//Z'
        }
    },
    outputAliases: ['outAlias1', 'outAlias2'],
    metadata: {},
    script: {
        name: 'testScript',
        type: 'transform',
        code: "var a = 1 + 2;"
    }
};

//let timestamp = new Date();

describe('JS execution', () => {

    it('can handle callbacks', done => {
        task.script.code = `    
             result.log.info("1");
             var buff = Base64.toBuffer(sources.image1.base64Data);
             
             
             promise.then( () => {
                return true;
             });      
        `;

        JavascriptExecutor.runSandboxedTask(task).then( output => {
            //console.log(JSON.stringify(output, null, 2));
            expect(output.transformKey).to.be.equal('device1.transfrom1');
            expect(output.transformOutput).not.to.be.null;
            expect(output.logs).not.to.be.null;
            expect(output.streamOutputs).not.to.be.null;

            done();
        });

    });

    it('can log', done => {
        task.script.code = `
          result.log.info('hello world!');
        `;

        JavascriptExecutor.runSandboxedTask(task).then( output => {
            expect(output.logs[0].message).to.equal('hello world!');
            done();
        });

    });

    it('timeouts with infinite loop',  done => {
        task.script.code = `
          while(true);
        `;

        JavascriptExecutor.runSandboxedTask(task).then( output => {
            expect(output.logs[0].message).to.equal('Error: Script execution timed out after 1000ms');
            done();
        });
    }).timeout(5000);

    it("can not require external modules",  done => {
        task.script.code = `
          var fs = require('fs');
        `;

        JavascriptExecutor.runSandboxedTask(task).then( output => {
            expect(output.logs[0].message).to.equal('ReferenceError: require is not defined');
            done();
        });
    });

    it("shows syntax errors",  done => {
        task.script.code = `
          var s = i23 [] abc;
        `;

        JavascriptExecutor.runSandboxedTask(task).then( output => {
            expect(output.logs[0].message).to.equal('\ndevice1.transfrom1/testScript:2\n' +
                '          var s = i23 [] abc;\n' +
                '                       ^\nParseError: Unexpected token');
            done();
        });

    });

    it("can generate a data point",  done => {
        task.script.code = `
          result.transformStream = Point.double(new Date(), 1.45);
        `;

        JavascriptExecutor.runSandboxedTask(task).then( output => {

            expect(output.transformKey).to.be.equal('device1.transfrom1');
            expect(output.transformOutput).not.to.be.null;
            expect(output.streamOutputs).not.to.be.null;
            done();
        });

    });

});
