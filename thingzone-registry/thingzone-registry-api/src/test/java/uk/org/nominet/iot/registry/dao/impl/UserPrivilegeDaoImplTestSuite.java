/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.UserDao;
import uk.org.nominet.iot.registry.dao.UserPrivilegeDao;
import uk.org.nominet.iot.registry.model.Privilege;
import uk.org.nominet.iot.registry.model.User;

import javax.persistence.NoResultException;
import java.util.List;

import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class UserPrivilegeDaoImplTestSuite {

    UserDao userDao;
    UserPrivilegeDao dao;

    @Before
    public void setUp() throws Exception {
        DaoImplTest.setUp();
        DaoFactory df = DaoFactoryManager.getFactory();
        userDao = df.userDao();
        dao = df.userPrivilegeDao();
    }

    @After
    public void tearDown() {
        DaoImplTest.tearDown();
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Test
    public void testPrivileges() {
        User user = userDao.create();

        // no privileges by default
        assertEquals(0, dao.get(user).size());
        assertFalse(dao.exists(user, Privilege.CREATE_USER)); // for example

        // add one
        dao.create(user, Privilege.CREATE_DATASTREAM);
        assertEquals(1, dao.get(user).size());
        assertEquals(Privilege.CREATE_DATASTREAM, dao.get(user).get(0));
        assertTrue(dao.exists(user, Privilege.CREATE_DATASTREAM));
        assertFalse(dao.exists(user, Privilege.CREATE_USER));

        // add another
        dao.create(user, Privilege.CREATE_USER);
        List<Privilege> privs = dao.get(user);
        assertEquals(2, privs.size());
        assertThat(privs, (Matcher) hasItem(Privilege.CREATE_USER));
        assertThat(privs, (Matcher) hasItem(Privilege.CREATE_DATASTREAM));
        assertTrue(dao.exists(user, Privilege.CREATE_USER));
        assertTrue(dao.exists(user, Privilege.CREATE_DATASTREAM));
        assertFalse(dao.exists(user, Privilege.USER_ADMIN));

        // remove one
        dao.remove(user, Privilege.CREATE_DATASTREAM);
        privs = dao.get(user);
        assertEquals(1, privs.size());
        assertThat(privs, (Matcher) hasItem(Privilege.CREATE_USER));
        assertTrue(dao.exists(user, Privilege.CREATE_USER));
        assertFalse(dao.exists(user, Privilege.CREATE_DATASTREAM));

        // remove something nonexistent
        try {
            dao.remove(user, Privilege.USER_ADMIN);
            fail();
        } catch (NoResultException e) {
            privs = dao.get(user);
            assertEquals(1, privs.size());
        }
    }

}
