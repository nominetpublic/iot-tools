/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.GroupDao;
import uk.org.nominet.iot.registry.dao.GroupUserDao;
import uk.org.nominet.iot.registry.dao.UserDao;
import uk.org.nominet.iot.registry.model.Group;
import uk.org.nominet.iot.registry.model.GroupUser;
import uk.org.nominet.iot.registry.model.User;

public class GroupDaoImplTestSuite {

    private GroupDao dao;
    private UserDao userDao;
    private GroupUserDao groupUserDao;

    private User owner;

    @Before
    public void setUp() throws Exception {
        DaoImplTest.setUp();
        DaoFactory df = DaoFactoryManager.getFactory();
        dao = df.groupDao();
        userDao = df.userDao();
        groupUserDao = df.groupUserDao();

        owner = userDao.create();
    }

    @After
    public void tearDown() {
        DaoImplTest.tearDown();
    }

    @Test
    public void create() {
        Group group = dao.create(owner, "group");
        assertNotNull(group);
    }

    @Test
    public void getByName() {
        Group original = dao.create(owner, "group");
        assertNotNull(original);

        // should be a matching Device
        Group found = dao.getByName("group");
        assertNotNull(found);
        assertEquals(original.getName(), found.getName());
    }

    @Test
    public void getByOwner() {
        Group original = dao.create(owner, "group");
        assertNotNull(original);

        List<Group> found = dao.listOwnedGroupsByUser(owner);
        assertNotNull(found);
        assertEquals(found.size(), 1);
        assertEquals(found.get(0).getId(), original.getId());
        assertEquals(found.get(0).getOwner().getId(), owner.getId());
    }

    @Test
    public void addUser() {
        Group original = dao.create(owner, "group");
        assertNotNull(original);

        GroupUser created = groupUserDao.create(original, owner, false);
        assertNotNull(created);
    }

    @Test
    public void removeUser() {
        Group original = dao.create(owner, "group");
        assertNotNull(original);

        GroupUser created = groupUserDao.create(original, owner, false);
        assertNotNull(created);
        groupUserDao.remove(original, owner);
        assertFalse(groupUserDao.exists(original, owner));
    }

    @Test
    public void getByUser() {
        Group original = dao.create(owner, "group");
        assertNotNull(original);

        User newUser = userDao.create();

        GroupUser created = groupUserDao.create(original, newUser, false);
        assertNotNull(created);

        List<Group> found = groupUserDao.listGroupsByUser(newUser);
        assertNotNull(found);
        assertEquals(found.size(), 1);
        assertEquals(found.get(0).getId(), original.getId());
    }
}
