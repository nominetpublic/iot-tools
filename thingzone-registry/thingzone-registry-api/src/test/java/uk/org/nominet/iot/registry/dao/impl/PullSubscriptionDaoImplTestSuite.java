/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import javax.persistence.NoResultException;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.DataStreamDao;
import uk.org.nominet.iot.registry.dao.PullSubscriptionDao;
import uk.org.nominet.iot.registry.database.DatabaseDriver;

import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.model.PullSubscription;
import uk.org.nominet.iot.registry.model.User;

/**
 * Test for Pull subscription DAO
 */
@RunWith(JUnit4.class)
public class PullSubscriptionDaoImplTestSuite {

    DaoFactory df;

    PullSubscriptionDao dao;
    DataStreamDao dataStreamDao;

    // test data
    DataStream ds1, ds2, ds3;
    String uri1, uri2;
    String uPs;

    @Before
    public void setUp() throws Exception {
        DaoImplTest.setUp();
        df = DaoFactoryManager.getFactory();

        dao = df.pullSubscriptionDao();
        dataStreamDao = df.dataStreamDao();

        ds1 = dataStreamDao.create();
        ds2 = dataStreamDao.create();
        ds3 = dataStreamDao.create();
        assertNotNull(ds1);
        assertNotNull(ds2);
        assertNotNull(ds3);

        uri1 = "http://www.google.com";
        uri2 = "mqtt://user:pass@host.example:42/foo/bar?clientid=clientid";

        uPs = "{\"httpHeaders\":[\"Accept: text/plain\", \"Cache-Control: no-cache\"]}";
    }

    @After
    public void tearDown() {
        DaoImplTest.tearDown();
    }

    @Test
    public void create() {
        assertNotNull(dao.create(ds1, uri1, uPs, 42, null));
    }

    @Ignore
    @Test
    public void getAll() {
        Account publicAccount = getPublicAccount();

        // empty list
        List<PullSubscription> found = dao.getAllowed(Arrays.asList(publicAccount), Permission.MODIFY);
        assertNotNull(found);
        assertEquals("no Pull Subscriptions", 0, found.size());

        // list with 3, of which 2 will be accessible
        assertNotNull(dao.create(ds1, uri1, uPs, 42, null));
        assertNotNull(dao.create(ds2, uri1, uPs, 42, null));
        assertNotNull(dao.create(ds3, uri1, uPs, 42, null));

        df.aclDao().create(ds1.getKey(), publicAccount, Permission.MODIFY);
        df.aclDao().create(ds2.getKey(), publicAccount, Permission.MODIFY);
        df.aclDao().create(ds3.getKey(), publicAccount, Permission.DISCOVER);

        found = dao.getAllowed(Arrays.asList(publicAccount), Permission.MODIFY);
        assertNotNull(found);
        assertEquals("2 Pull Subscriptions", 2, found.size());
    }

    @Test(expected = NoResultException.class)
    public void getForStreamNonexistent() {
        dao.getForStream(ds1);
        fail();
    }

    @Test
    public void getForStream() {
        PullSubscription ps = dao.create(ds1, uri1, uPs, 42, null);
        assertNotNull(ps);
        PullSubscription found = dao.getForStream(ds1);
        assertNotNull(found);
    }

    @Test
    public void update() {
        PullSubscription ps = dao.create(ds1, uri1, uPs, 42, null);
        assertNotNull(ps);
        ps.setInterval(43);
        dao.persist(ps);
        ps = dao.getForStream(ds1);
        assertEquals("update value", 43, ps.getInterval().intValue());
    }

    @Test(expected = NoResultException.class)
    public void deleteNonexistent() {
        dao.delete(ds1);
        fail();
    }

    @Test(expected = NoResultException.class)
    public void delete() {
        PullSubscription ps = dao.create(ds1, uri1, uPs, 42, null);
        assertNotNull(ps);
        dao.delete(ds1);

        dao.getForStream(ds1);
        fail();
    }

    public Account getPublicAccount() {
        User publicUser = df.userDao().getUserByUsername(DatabaseDriver.PUBLIC_USERNAME);
        return df.accountDao().getByOwner(publicUser);
    }

}
