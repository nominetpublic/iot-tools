/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.upsert;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import uk.org.nominet.iot.registry.service.dto.PushSubscriberSummaryDto;

public class PushSubscriberUpsertMetaServiceTest {

    @Test
    public void decodeWithSingleKVP() {
        List<PushSubscriberSummaryDto> decoded = PushSubscriberUpsertMetaService.decodeFromJson(
            "[{" + "  \"uri\" : \"http://example.com\"," + "  \"uriParams\" : { \"foo\": \"bar\" }\n" + "}]", "key1");
        assertEquals(1, decoded.size());
        PushSubscriberSummaryDto d0 = decoded.get(0);
        assertEquals("key1", d0.dataStream);
        assertEquals("http://example.com", d0.uri);
        assertEquals("{\"foo\":\"bar\"}", d0.uriParams);
    }

    @Test
    public void decodeWithDeeperData() {
        List<PushSubscriberSummaryDto> decoded = PushSubscriberUpsertMetaService.decodeFromJson(
            "[{" + "  \"uri\" : \"http://example.com\",\n" + "  \"uriParams\" : {\n" + "    \"httpHeaders\" : {\n"
                + "       \"SessionHeaderId\" : \"1234\"\n" + "    }\n" + "  }\n" + "}]",
            "key2");
        assertEquals(1, decoded.size());
        PushSubscriberSummaryDto d0 = decoded.get(0);
        assertEquals("key2", d0.dataStream);
        assertEquals("http://example.com", d0.uri);
        assertEquals("{\"httpHeaders\":{\"SessionHeaderId\":\"1234\"}}", d0.uriParams);
    }

    @Test
    public void decodeWithMinData() {
        List<PushSubscriberSummaryDto> decoded = PushSubscriberUpsertMetaService.decodeFromJson(
            "[{" + "  \"uri\" : \"http://example.com\",\n" + "  \"uriParams\" : {}\n" + "}]", "key3");
        assertEquals(1, decoded.size());
        PushSubscriberSummaryDto d0 = decoded.get(0);
        assertEquals("key3", d0.dataStream);
        assertEquals("http://example.com", d0.uri);
        assertEquals("{}", d0.uriParams);
    }

}
