/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.common;

import static org.junit.Assert.*;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import uk.org.nominet.iot.registry.common.UidGenerator;

@RunWith(JUnit4.class)
public class UidGeneratorTest {

    @Test
    public void userUidIsCorrectLength() {
        String uid = UidGenerator.generateUserUid();
        assertEquals(UidGenerator.UID_ENTROPY_BITS / UidGenerator.BITS_PER_HEX, uid.length());
        assertEquals(256 / 4, uid.length());
    }

    @Test
    public void uidsAreDifferent() {
        String uid1 = UidGenerator.generateUserUid();
        String uid2 = UidGenerator.generateUserUid();
        assertNotEquals(uid1, uid2);
    }

    @Test
    public void deviceKey() {
        // blank
        String blankUid = UidGenerator.generateDeterministicDeviceKey("", "", "", "");
        assertEquals("4oymiquy7qobjgx36tejs35zeq", blankUid);

        // all parameters are used
        String uid1234 = UidGenerator.generateDeterministicDeviceKey("one", "two", "three", "four");
        assertEquals("tjcazih4xqy7pzpia767k3bk2n", uid1234);

        assertNotEquals(uid1234, UidGenerator.generateDeterministicDeviceKey("xxx", "two", "three", "four"));
        assertNotEquals(uid1234, UidGenerator.generateDeterministicDeviceKey("one", "xxx", "three", "four"));
        assertNotEquals(uid1234, UidGenerator.generateDeterministicDeviceKey("one", "two", "xxxxx", "four"));
        assertNotEquals(uid1234, UidGenerator.generateDeterministicDeviceKey("one", "two", "three", "xxxx"));
    }

}
