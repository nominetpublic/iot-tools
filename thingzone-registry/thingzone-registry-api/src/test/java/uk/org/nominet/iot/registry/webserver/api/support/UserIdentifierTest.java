/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api.support;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import uk.nominet.iot.utils.error.ProgramDefectException;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class UserIdentifierTest {

    @Test
    public void testPublicUser() {
        UserIdentifier ui = new UserIdentifier(null, null);
        assertEquals(true, ui.isPublicUser());
        assertEquals("(no user specified)", ui.getLogDescription());
    }

    @Test
    public void testSessionKey() {
        UserIdentifier ui = new UserIdentifier("sk1", null);
        assertFalse(ui.isPublicUser());

        try {
            ui.getUsername();
            fail();
        } catch (ProgramDefectException e) {
        }

        assertEquals("sk1", ui.getSessionKey());
        assertEquals("sessionkey=\"sk1\"", ui.getLogDescription());
    }

    @Test
    public void testUsername() {
        UserIdentifier ui = new UserIdentifier(null, "user1");
        assertFalse(ui.isPublicUser());

        try {
            ui.getSessionKey();
            fail();
        } catch (ProgramDefectException e) {
        }
        assertEquals("user1", ui.getUsername());
        assertEquals("username=\"user1\"", ui.getLogDescription());
    }

}
