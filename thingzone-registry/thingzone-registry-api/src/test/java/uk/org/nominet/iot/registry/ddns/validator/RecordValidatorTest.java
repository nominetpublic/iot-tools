/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.ddns.validator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.service.dto.DNSdataDto;

@RunWith(JUnit4.class)
public class RecordValidatorTest {

    @Test
    public void validARecord() {
        RecordValidator.validate("A", "1.2.3.4");
        RecordValidator.validate("A", "0.0.0.0");
    }

    @Test
    public void validAAAARecord() {
        RecordValidator.validate("AAAA", "dead::beef");
    }

    @Test(expected = DataException.class)
    public void ipv6WithEmbeddedIpv4Refused() {
        RecordValidator.validate("AAAA", " ::ffff:192.0.2.128");
    }

    @Test(expected = DataException.class)
    public void typeCheckedA() {
        RecordValidator.validate("AAAA", "0.0.0.0");
    }

    @Test(expected = DataException.class)
    public void typeCheckedAAAA() {
        RecordValidator.validate("A", "dead::beef");
    }

    @Test
    public void validCNAMERecord() {
        RecordValidator.validate("CNAME", "foo.com");
    }

    // sub domain tests...
    DNSdataDto withSubdomain(String subdomain) {
        return new DNSdataDto(null, null, subdomain, null, "A", "1.2.3.4");
    }

    @Test
    public void trivialSubdomainIsValid() {
        RecordValidator.validate(withSubdomain("a"));
    }

    @Test
    public void emptySubdomainIsValid() {
        RecordValidator.validate(withSubdomain(""));
    }

    @Test
    public void nullSubdomainIsValid() {
        RecordValidator.validate(withSubdomain(""));
    }

    @Test(expected = DataException.class)
    public void bogusSubdomainIsInvalid() {
        RecordValidator.validate(withSubdomain("..."));
    }

    @Test(expected = DataException.class)
    public void bogusSubdomain2IsInvalid() {
        RecordValidator.validate(withSubdomain("-"));
    }

}
