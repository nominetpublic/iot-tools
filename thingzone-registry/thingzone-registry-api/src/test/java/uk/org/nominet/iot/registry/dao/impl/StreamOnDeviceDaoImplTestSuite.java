/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.NoResultException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.DataStreamDao;
import uk.org.nominet.iot.registry.dao.DeviceDao;
import uk.org.nominet.iot.registry.dao.HistoryDao;
import uk.org.nominet.iot.registry.dao.StreamOnDeviceDao;

import uk.org.nominet.iot.registry.dao.dto.StreamOnDeviceRow;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.Device;
import uk.org.nominet.iot.registry.model.History;
import uk.org.nominet.iot.registry.model.HistoryAction;
import uk.org.nominet.iot.registry.model.StreamOnDevice;

/**
 * Test for Streaming output using the Device DAO
 */
@RunWith(JUnit4.class)
public class StreamOnDeviceDaoImplTestSuite {

    String testKey = "aaaa.bbbb.cccc.dddd.eeee.ffff.gggg.h.i";
    String dummyKey = "beef.dead.dead.beef.dead.beef.dead.beef.h.i";

    private StreamOnDeviceDao dao;
    private DeviceDao deviceDao;
    private DataStreamDao dataStreamDao;
    private HistoryDao historyDao;

    @Before
    public void setUp() throws Exception {
        DaoImplTest.setUp();
        DaoFactory df = DaoFactoryManager.getFactory();

        dao = df.streamOnDeviceDao();
        deviceDao = df.deviceDao();
        dataStreamDao = df.dataStreamDao();
        historyDao = df.historyDao();
    }

    @After
    public void tearDown() {
        DaoImplTest.tearDown();
    }

    @Test
    public void create() {
        DataStream dataStream = dataStreamDao.create();
        Device device = deviceDao.create();
        StreamOnDevice sod = dao.create(device, dataStream, "test");
        assertNotNull(sod);
    }

    @Test
    public void streamAllowedWithMultipleDevices() {
        DataStream dataStream = dataStreamDao.create();
        Device device = deviceDao.create();

        Device alternateDevice = deviceDao.create();

        StreamOnDevice sod = dao.create(device, dataStream, "test");
        assertNotNull(sod);

        // should be allowed
        sod = dao.create(alternateDevice, dataStream, "test");
        assertNotNull(sod);
    }

    @Test
    public void deviceAllowedWithMultipleStreams() {
        DataStream dataStream = dataStreamDao.create();
        Device device = deviceDao.create();

        DataStream alternateDataStream = dataStreamDao.create();

        StreamOnDevice sod = dao.create(device, dataStream, "test");
        assertNotNull(sod);

        // these 2 should be allowed
        sod = dao.create(device, alternateDataStream, "test");
        assertNotNull(sod);
    }

    @Test
    public void duplicatePrevented() {
        DataStream dataStream = dataStreamDao.create();
        Device device = deviceDao.create();

        StreamOnDevice sod = dao.create(device, dataStream, "test");

        assertNotNull(sod);
        try {
            dao.create(device, dataStream, "test");
            fail();
        } catch (DataException e) {
            assertThat(e.getMessage(), containsString("already exists"));
        }
    }

    @Test
    public void findByX() {
        DataStream dataStream1 = dataStreamDao.create();
        DataStream dataStream2 = dataStreamDao.create();
        DataStream dataStream3 = dataStreamDao.create();
        Device device1 = deviceDao.create();
        Device device2 = deviceDao.create();
        Device device3 = deviceDao.create();

        dao.create(device1, dataStream1, "test_11");
        dao.create(device1, dataStream2, "test_12");
        dao.create(device2, dataStream1, "test_21");
        dao.create(device2, dataStream2, "test_22");
        dao.create(device3, dataStream3, "test_22");

        assertEquals(2, dao.findByDevice(device1).size());
        assertEquals(2, dao.findByDevice(device2).size());
        assertEquals(1, dao.findByDevice(device3).size());
        assertEquals(2, dao.findByDataStream(dataStream1).size());
        assertEquals(2, dao.findByDataStream(dataStream2).size());
        assertEquals(1, dao.findByDataStream(dataStream3).size());
    }

    @Test
    public void delete() {
        DataStream dataStream = dataStreamDao.create();
        Device device = deviceDao.create();

        StreamOnDevice sod = dao.create(device, dataStream, "test");
        assertNotNull(sod);
        assertEquals(1, dao.findByDevice(device).size());
        assertEquals(1, dao.findByDataStream(dataStream).size());

        dao.delete(sod);

        assertEquals(0, dao.findByDevice(device).size());
        assertEquals(0, dao.findByDataStream(dataStream).size());
    }

    @Test
    public void testAdd() {
        DataStream dataStream = dataStreamDao.create();
        Device device = deviceDao.create();

        assertEquals("empty before addition", 0, dao.getDevices(dataStream).size());
        assertEquals("empty before addition", 0, dao.getDataStreams(device).size());

        List<History> listDataStreamActionEmpty
            = historyDao.listByKeyAction(dataStream.getKey().getKey(), HistoryAction.RELATIONSHIP);
        assertEquals(listDataStreamActionEmpty.isEmpty(), true);

        dao.addDataStream(device, dataStream, "foo");

        assertEquals("present after addition", 1, dao.getDevices(dataStream).size());
        assertEquals("present after addition", 1, dao.getDataStreams(device).size());

        List<History> listDataStreamAction
            = historyDao.listByKeyAction(dataStream.getKey().getKey(), HistoryAction.RELATIONSHIP);
        assertEquals(listDataStreamAction.isEmpty(), false);

        List<History> listDeviceAction
            = historyDao.listByKeyAction(device.getKey().getKey(), HistoryAction.RELATIONSHIP);
        assertEquals(listDeviceAction.isEmpty(), false);
    }

    @Test
    public void testAddWhenAlreadyPresent() {
        DataStream dataStream = dataStreamDao.create();
        Device device = deviceDao.create();

        assertEquals("empty before addition", 0, dao.getDevices(dataStream).size());
        assertEquals("empty before addition", 0, dao.getDataStreams(device).size());

        dao.addDataStream(device, dataStream, "foo");
        dao.addDataStream(device, dataStream, "bar");

        assertEquals("present after addition", 1, dao.getDevices(dataStream).size());
        assertEquals("present after addition", 1, dao.getDataStreams(device).size());
    }

    @Test
    public void testAddMultiple() {
        DataStream dataStream1 = dataStreamDao.create();
        DataStream dataStream2 = dataStreamDao.create();
        DataStream dataStream3 = dataStreamDao.create();
        Device device1 = deviceDao.create();
        Device device2 = deviceDao.create();
        Device device3 = deviceDao.create();

        assertEquals("empty before addition", 0, dao.getDevices(dataStream1).size());
        assertEquals("empty before addition", 0, dao.getDevices(dataStream2).size());
        assertEquals("empty before addition", 0, dao.getDevices(dataStream3).size());
        assertEquals("empty before addition", 0, dao.getDataStreams(device1).size());
        assertEquals("empty before addition", 0, dao.getDataStreams(device2).size());
        assertEquals("empty before addition", 0, dao.getDataStreams(device3).size());

        dao.addDataStream(device1, dataStream1, "1_1");
        dao.addDataStream(device1, dataStream2, "1_2");
        dao.addDataStream(device1, dataStream3, "1_3");
        dao.addDataStream(device2, dataStream1, "2_1");
        dao.addDataStream(device2, dataStream2, "2_2");
        dao.addDataStream(device3, dataStream2, "3_2");
        dao.removeDataStream(device3, dataStream2);

        assertEquals(2, dao.getDevices(dataStream1).size());
        assertEquals(2, dao.getDevices(dataStream2).size());
        assertEquals(1, dao.getDevices(dataStream3).size());
        assertEquals(3, dao.getDataStreams(device1).size());
        assertEquals(2, dao.getDataStreams(device2).size());
        assertEquals(0, dao.getDataStreams(device3).size());
    }

    @Test
    public void testRemove() {
        DataStream dataStream = dataStreamDao.create();
        Device device = deviceDao.create();

        assertEquals("empty before addition", 0, dao.getDevices(dataStream).size());
        assertEquals("empty before addition", 0, dao.getDataStreams(device).size());

        dao.addDataStream(device, dataStream, "foo");
        dao.removeDataStream(device, dataStream);

        assertEquals("removed again", 0, dao.getDevices(dataStream).size());
        assertEquals("removed again", 0, dao.getDataStreams(device).size());
    }

    @Test
    public void testRemoveNonexistent() {
        DataStream dataStream = dataStreamDao.create();
        Device device = deviceDao.create();
        try {
            dao.removeDataStream(device, dataStream);
            fail();
        } catch (NoResultException e) {

        }

    }

    @Test
    public void getUnmanagedByDataStream() {
        DataStream dataStream0 = dataStreamDao.create();
        DataStream dataStream1 = dataStreamDao.create();
        DataStream dataStream2 = dataStreamDao.create();
        Device device_a = deviceDao.create();
        Device device_b = deviceDao.create();

        dao.create(device_a, dataStream1, "test_1");
        dao.create(device_a, dataStream2, "test_2a");
        dao.create(device_b, dataStream2, "test_2b");

        List<StreamOnDeviceRow> found;
        found = dao.unmanagedGetByDataStream(dataStream0.getKey());
        assertEquals(0, found.size());
        found = dao.unmanagedGetByDataStream(dataStream1.getKey());
        assertEquals(1, found.size());
        assertEquals(found.get(0).getDataStreamKey(), dataStream1.getKey());
        assertEquals(found.get(0).getDeviceKey(), device_a.getKey());
        assertEquals(found.get(0).getName(), "test_1");
        found = dao.unmanagedGetByDataStream(dataStream2.getKey());
        assertEquals(2, found.size());
    }

}
