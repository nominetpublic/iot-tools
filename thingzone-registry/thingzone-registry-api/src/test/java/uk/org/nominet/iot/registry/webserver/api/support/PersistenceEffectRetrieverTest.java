/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api.support;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import uk.nominet.iot.utils.error.ProgramDefectException;

@RunWith(JUnit4.class)
public class PersistenceEffectRetrieverTest {

    @Test(expected = ProgramDefectException.class)
    public void absentAnnotationThrows() {
        PersistenceEffectRetriever.getPersistenceEffectAnnotation();
    }

    @Test(expected = ProgramDefectException.class)
    @PersistenceEffect()
    public void defaultAnnotationThrows() {
        PersistenceEffectRetriever.getPersistenceEffectAnnotation();
    }

    @Test
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public void readAnnotationIsRetreived() {
        assertEquals(PersistenceEffect.Effect.READ, PersistenceEffectRetriever.getPersistenceEffectAnnotation());
    }

    @Test
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public void modifyAnnotationIsRetreived() {
        assertEquals(PersistenceEffect.Effect.MODIFY, PersistenceEffectRetriever.getPersistenceEffectAnnotation());
    }

}
