/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.dto;

import org.junit.Test;

import static org.junit.Assert.*;

public class AccountIdTest {

    @Test
    public void identity() {
        AccountId ai = new AccountId(42);
        assertEquals(42, ai.getId());
    }

    @Test
    public void comparison() {
        AccountId ai42 = new AccountId(42);
        AccountId ai43 = new AccountId(43);
        assertTrue(ai42.compareTo(ai43) != 0);

        assertFalse(ai42 == ai43);

        AccountId ai42_bis = new AccountId(42);
        assertTrue(ai42.compareTo(ai42_bis) == 0);
        assertTrue(ai42.equals(ai42_bis));
        assertEquals(ai42.hashCode(), ai42_bis.hashCode());
    }

}
