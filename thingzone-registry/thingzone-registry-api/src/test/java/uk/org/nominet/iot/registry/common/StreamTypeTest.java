/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.common;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.containsString;

public class StreamTypeTest {

    @Test
    public void testIsValid() {
        assertEquals(false, StreamType.isValid(null));
        assertEquals(false, StreamType.isValid(""));
        assertEquals(false, StreamType.isValid("nonsense"));
        assertEquals(true, StreamType.isValid("stream"));
        assertEquals(true, StreamType.isValid("queue"));
        assertEquals(true, StreamType.isValid("Queue"));
    }

    @Test
    public void testFromString() {
        assertEquals(StreamType.STREAM, StreamType.fromString("stream"));
        assertEquals(StreamType.QUEUE, StreamType.fromString("queue"));
        assertEquals(StreamType.QUEUE, StreamType.fromString("QUEUE"));
    }

    @Test
    public void exceptionForEmptyString() {
        try {
            StreamType.fromString(null);
            fail("no exception");
        } catch (Exception e) {
            assertThat(e.getMessage(), containsString("cannot be empty"));
        }
    }

    @Test
    public void exceptionForBogusString() {
        try {
            StreamType.fromString("this is bogus");
            fail("no exception");
        } catch (Exception e) {
            assertThat(e.getMessage(), containsString("not a valid"));
        }
    }

}
