/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nominet.iot.test.docker.compose.ThingzoneComposeRuntimeContainer;
import uk.nominet.iot.test.docker.compose.ThingzoneEnvironment;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;

import uk.org.nominet.iot.registry.database.DatabaseDriver;
import uk.org.nominet.iot.registry.database.SchemaCreator;

import java.util.Properties;

@RunWith(Suite.class)
@Suite.SuiteClasses({ ACLDaoImplTestSuite.class,
                      AccountDaoImplTestSuite.class,
                      DataStreamDaoImplTestSuite.class,
                      DeviceDaoImplTestSuite.class,
                      DNSdataDaoImplTestSuite.class,
                      DNSkeyDaoImplTestSuite.class,
                      GroupDaoImplTestSuite.class,
                      PullSubscriptionDaoImplTestSuite.class,
                      PushSubscriberDaoImplTestSuite.class,
                      RegistryPropertyDaoImplTestSuite.class,
                      SessionDaoImplTestSuite.class,
                      StreamOnDeviceDaoImplTestSuite.class,
                      TransformDaoImplTestSuite.class,
                      TransformFunctionDaoImplTestSuite.class,
                      TransformSourceDaoImplTestSuite.class,
                      UserCredentialsDaoImplTestSuite.class,
                      UserDaoImplTestSuite.class,
                      UserPrivilegeDaoImplTestSuite.class,
                      UserTokenDaoImplTestSuite.class,
                      ZoneFileChangeDaoImplTestSuite.class })
public class DaoImplTest {
    private final static Logger log = LoggerFactory.getLogger(DaoImplTest.class);

    @ClassRule
    public static ThingzoneComposeRuntimeContainer environment = new ThingzoneEnvironment().withServices(
        ThingzoneEnvironment.SERVICE_POSTGRES).getContainer();

    public static void setUp() {
        // simulate config to be able to run tests
        uk.org.nominet.iot.registry.webserver.TestConfigFactory.initialise();

        // we use real DAOs for these tests
        DaoFactoryManager.setFactory(new DatabaseDaoFactory());

        // recreate database schemas - first time only
        if (!DatabaseDriver.isDatabaseRebuilt()) {
            log.info("DaoImplTestBase - rebuilding database schema...");
            DatabaseDriver.setEclipseLinkProperties(SchemaCreator.propertiesForCreatingTables(true));
            uk.org.nominet.iot.registry.database.ConfigFactory.loadConfig("config/db.properties.e2e_test");

            Properties properties = uk.org.nominet.iot.registry.database.ConfigFactory.getConfig().getJdbcProperties();

            // Injecting postgres container configuration
            String jdbcUrl = properties.getProperty("javax.persistence.jdbc.url");
            jdbcUrl = jdbcUrl.replace("localhost", environment.getServiceHost(ThingzoneEnvironment.SERVICE_POSTGRES))
                             .replace("5432",
                                 environment.getServicePort(ThingzoneEnvironment.SERVICE_POSTGRES).toString());
            properties.setProperty("javax.persistence.jdbc.url", jdbcUrl);

            DatabaseDriver.setConnectionProperties(properties);

            DatabaseDriver.globalInitialise();
            new SchemaCreator().setupDbInitialData();
            DatabaseDriver.globalDispose();
            DatabaseDriver.setEclipseLinkProperties(null);
            log.info("DaoImplTestBase - rebuilt database schema");
        }

        if (!DatabaseDriver.isInitialized()) {
            log.info("DaoImplTestBase - global initialise!");
            DatabaseDriver.globalInitialise();
        }

        log.info("DB: starting transaction for test");
        DatabaseDriver.beginMutableTransaction();
    }

    public static void tearDown() {
        // log.info("DB: rolling back transaction");
        try {
            DatabaseDriver.abortTransaction();
        } catch (RuntimeException e) {
        }
        DaoFactoryManager.clearFactory();
    }

}
