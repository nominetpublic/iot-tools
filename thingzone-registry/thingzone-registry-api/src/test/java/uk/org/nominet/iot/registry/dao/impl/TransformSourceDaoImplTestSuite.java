/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.DataStreamDao;
import uk.org.nominet.iot.registry.dao.TransformDao;
import uk.org.nominet.iot.registry.dao.TransformFunctionDao;
import uk.org.nominet.iot.registry.dao.TransformSourceDao;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.Transform;
import uk.org.nominet.iot.registry.model.TransformFunction;
import uk.org.nominet.iot.registry.model.TransformSource;
import uk.org.nominet.iot.registry.model.User;

@RunWith(JUnit4.class)
public class TransformSourceDaoImplTestSuite {

    TransformDao dao;
    TransformFunctionDao tfDao;
    TransformSourceDao tsDao;
    DataStreamDao dataStreamDao;
    final static String dummyJson = "{\"key\":\"value\"}";
    User testUser;

    @Before
    public void setUp() throws Exception {
        DaoImplTest.setUp();
        DaoFactory df = DaoFactoryManager.getFactory();

        dao = df.transformDao();
        tfDao = df.transformFunctionDao();
        tsDao = df.transformSourceDao();
        dataStreamDao = df.dataStreamDao();

        testUser = df.userDao().create();
    }

    @After
    public void tearDown() {
        DaoImplTest.tearDown();
    }

    @Test
    public void create() {
        TransformFunction tf1 = tfDao.create("foo", testUser, "javascript", "{}");
        Transform t1 = dao.create(dataStreamDao.create(), tf1, "{\"p1\":42}", "");
        assertNotNull(t1);

        // create parameters
        TransformSource ts1 = tsDao.create(t1, dataStreamDao.create(), "0", false, null, null);
        assertNotNull(ts1);

        // create source with aggregation data
        TransformSource ts2 = tsDao.create(t1, dataStreamDao.create(), "1", false, "type", "spec");
        assertNotNull(ts2);

        // create source with incorrect aggregation data
        try {
            tsDao.create(t1, dataStreamDao.create(), "2", false, null, "spec");
            fail();
        } catch (DataException e) {
            assertThat(e.getMessage(), containsString("requires both"));
        }
    }

    @Test
    public void exists() {
        TransformFunction tf1 = tfDao.create("foo", testUser, "javascript", "{}");
        Transform t1 = dao.create(dataStreamDao.create(), tf1, "{\"p1\":42}", "");
        Transform t2 = dao.create(dataStreamDao.create(), tf1, "{\"p1\":42}", "");
        assertNotNull(t1);

        TransformSource ts1 = tsDao.create(t1, dataStreamDao.create(), "0", true, null, null);
        TransformSource ts2 = tsDao.create(t1, dataStreamDao.create(), "1", false, "type", "spec");
        assertNotNull(ts1);
        assertNotNull(ts2);

        assertTrue(tsDao.exists(t1, "0"));
        assertTrue(tsDao.exists(t1, "1"));
        assertFalse(tsDao.exists(t2, "0"));
        assertFalse(tsDao.exists(t1, "-1"));
    }

    @Test
    public void overwrites() {
        TransformFunction tf1 = tfDao.create("foo", testUser, "javascript", "{}");
        Transform t1 = dao.create(dataStreamDao.create(), tf1, "{\"p1\":42}", "");
        assertNotNull(t1);

        DataStream dataSource1 = dataStreamDao.create();
        DataStream dataSource2 = dataStreamDao.create();

        TransformSource ts1 = tsDao.create(t1, dataSource1, "0", true, null, null);
        assertTrue(tsDao.exists(t1, "0"));
        TransformSource ts2 = tsDao.createOrUpdate(t1, dataSource2, "0", false, null, null);
        assertNotNull(ts1);
        assertNotNull(ts2);
        assertNull(ts2.getAggregationSpec());
        ts2 = tsDao.createOrUpdate(t1, dataSource2, "0", false, "type", "spec");
        assertEquals(ts2.getAggregationSpec(), "spec");

        assertTrue(tsDao.exists(t1, "0"));
        assertEquals(1, tsDao.getByTransform(t1).size());

        TransformSource found = tsDao.get(t1, "0");
        assertEquals(dataSource2, found.getSourceStream());
    }

    @Test(expected = PersistenceException.class)
    public void duplicateError() {
        TransformFunction tf1 = tfDao.create("foo", testUser, "javascript", "{}");
        Transform t1 = dao.create(dataStreamDao.create(), tf1, "{\"p1\":42}", "");
        assertNotNull(t1);

        DataStream dataSource1 = dataStreamDao.create();
        DataStream dataSource2 = dataStreamDao.create();

        tsDao.create(t1, dataSource1, "0", true, null, null);
        tsDao.create(t1, dataSource2, "0", true, null, null);
        tfDao.flush();
        fail();
    }

    @Test
    public void delete() {
        TransformFunction tf1 = tfDao.create("foo", testUser, "javascript", "{}");
        Transform t1 = dao.create(dataStreamDao.create(), tf1, "{\"p1\":42}", "");
        assertNotNull(t1);
        DataStream dataSource1 = dataStreamDao.create();

        TransformSource ts1 = tsDao.create(t1, dataSource1, "0", true, null, null);
        assertNotNull(ts1);

        assertTrue(tsDao.exists(t1, "0"));
        tsDao.delete(t1, "0");
        assertFalse(tsDao.exists(t1, "0"));
    }

    @Test(expected = NoResultException.class)
    public void deleteNonexistent() {
        TransformFunction tf1 = tfDao.create("foo", testUser, "javascript", "{}");
        Transform t1 = dao.create(dataStreamDao.create(), tf1, "{\"p1\":42}", "");
        assertNotNull(t1);

        tsDao.delete(t1, "0");
        fail();
    }

    @Test
    public void getByTransform() {
        TransformFunction tf1 = tfDao.create("foo", testUser, "javascript", "{}");
        Transform t1 = dao.create(dataStreamDao.create(), tf1, "{\"p1\":42}", "");
        Transform t2 = dao.create(dataStreamDao.create(), tf1, "{\"p1\":42}", "");
        assertNotNull(t1);

        DataStream dataSource1 = dataStreamDao.create();
        DataStream dataSource2 = dataStreamDao.create();
        DataStream dataSource3 = dataStreamDao.create();

        TransformSource ts1 = tsDao.create(t1, dataSource1, "0", true, null, null);
        TransformSource ts2 = tsDao.create(t1, dataSource2, "1", false, null, null);
        TransformSource ts3 = tsDao.create(t2, dataSource3, "-1", true, "type", "spec");
        assertNotNull(ts1);
        assertNotNull(ts2);
        assertNotNull(ts3);

        assertEquals(2, tsDao.getByTransform(t1).size());
        assertEquals(1, tsDao.getByTransform(t2).size());
    }

}
