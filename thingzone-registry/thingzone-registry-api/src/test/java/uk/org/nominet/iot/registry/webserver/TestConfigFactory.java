/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver;

import java.util.List;
import java.util.Properties;

public class TestConfigFactory {

    static class TestWebConfig implements WebConfig {
        public Properties getProperties() {
            return null;
        }

        public List<String> getHostnames() {
            return null;
        }

        public List<String> getPrivilegedClients() {
            return null;
        }

        public int getPort() {
            return 42;
        }

        public int getIdleTimeout() {
            return 42;
        }

        public int getSessionTimeout() {
            return 42;
        }

        public KeyValidationType getKeyValidationType() {
            return KeyValidationType.DNSKEY;
        }

        public KeyGenerationType getKeyGenerationType() {
            return KeyGenerationType.EXTERNAL;
        }
    }

    public static void initialise() {
        initialise(new TestWebConfig());
    }

    public static void initialise(WebConfig webConfig) {
        ConfigFactory.setConfig(webConfig);
    }
}
