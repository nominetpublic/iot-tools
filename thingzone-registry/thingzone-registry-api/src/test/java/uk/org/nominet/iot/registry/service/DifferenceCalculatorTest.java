/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.model.Privilege;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class DifferenceCalculatorTest {

    DifferenceCalculator<Permission> calc = new DifferenceCalculator<>();
    List<DifferenceCalculator<Permission>.Difference> diffs;

    @Test
    public void singleElementToAdd() {
        diffs = calc.calculate(Arrays.asList(Permission.ANNOTATE), new ArrayList<Permission>());
        assertEquals(1, diffs.size());
        assertEquals(diffs.get(0), calc.new Difference(DifferenceCalculator.Type.ADD, Permission.ANNOTATE));
    }

    @Test
    public void elementToAdd() {
        diffs = calc.calculate(Arrays.asList(Permission.ANNOTATE, Permission.ADMIN), Arrays.asList(Permission.ADMIN));
        assertEquals(1, diffs.size());
        assertEquals(diffs.get(0), calc.new Difference(DifferenceCalculator.Type.ADD, Permission.ANNOTATE));
    }

    @Test
    public void singleElementToRemove() {
        diffs = calc.calculate(new ArrayList<Permission>(), Arrays.asList(Permission.ANNOTATE));
        assertEquals(1, diffs.size());
        assertEquals(diffs.get(0), calc.new Difference(DifferenceCalculator.Type.REMOVE, Permission.ANNOTATE));
    }

    @Test
    public void singleSwap() {
        diffs = calc.calculate(Arrays.asList(Permission.ANNOTATE), Arrays.asList(Permission.ADMIN));
        assertEquals(2, diffs.size());
        assertTrue(diffs.contains(calc.new Difference(DifferenceCalculator.Type.REMOVE, Permission.ADMIN)));
        assertTrue(diffs.contains(calc.new Difference(DifferenceCalculator.Type.ADD, Permission.ANNOTATE)));
    }

    @Test
    public void multiSwap() {
        diffs = calc.calculate(Arrays.asList(Permission.ANNOTATE, Permission.ADMIN, Permission.CONSUME),
            Arrays.asList(Permission.ADMIN, Permission.UPLOAD, Permission.DISCOVER));
        assertEquals(4, diffs.size());
        assertTrue(diffs.contains(calc.new Difference(DifferenceCalculator.Type.REMOVE, Permission.UPLOAD)));
        assertTrue(diffs.contains(calc.new Difference(DifferenceCalculator.Type.REMOVE, Permission.DISCOVER)));
        assertTrue(diffs.contains(calc.new Difference(DifferenceCalculator.Type.ADD, Permission.ANNOTATE)));
        assertTrue(diffs.contains(calc.new Difference(DifferenceCalculator.Type.ADD, Permission.CONSUME)));
    }

    @Test
    public void multiSwapPrivilege() {

        DifferenceCalculator<Privilege> calc = new DifferenceCalculator<>();
        List<DifferenceCalculator<Privilege>.Difference> diffs;

        diffs = calc.calculate(Arrays.asList(Privilege.CREATE_USER, Privilege.CREATE_TRANSFORM, Privilege.DUMP_ALL),
            Arrays.asList(Privilege.CREATE_DEVICE, Privilege.CREATE_USER, Privilege.USE_SESSION));
        assertEquals(4, diffs.size());
        assertTrue(diffs.contains(calc.new Difference(DifferenceCalculator.Type.ADD, Privilege.CREATE_TRANSFORM)));
        assertTrue(diffs.contains(calc.new Difference(DifferenceCalculator.Type.ADD, Privilege.DUMP_ALL)));
        assertTrue(diffs.contains(calc.new Difference(DifferenceCalculator.Type.REMOVE, Privilege.CREATE_DEVICE)));
        assertTrue(diffs.contains(calc.new Difference(DifferenceCalculator.Type.REMOVE, Privilege.USE_SESSION)));
    }

}
