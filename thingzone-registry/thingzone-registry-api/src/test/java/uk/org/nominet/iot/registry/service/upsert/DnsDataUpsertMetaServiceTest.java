/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.upsert;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.service.dto.DNSdataDto;

public class DnsDataUpsertMetaServiceTest {

    @Test
    public void decodeWithData() {
        List<DNSdataDto> decoded
            = DnsDataUpsertMetaService.decodeFromJson("[{" + "\"ttl\" : 600," + "\"rdata\" : \"1.2.3.4\","
                + "\"description\" : \"desc\"," + "\"subdomain\" : \"gup-b\"," + "\"rrtype\" : \"A\"" + "}]", "key1");
        assertEquals(1, decoded.size());
        DNSdataDto d0 = decoded.get(0);
        assertEquals(600, d0.getTtl().intValue());
        assertEquals("1.2.3.4", d0.getRdata());
        assertEquals("A", d0.getRrtype());
        assertEquals("desc", d0.getDescription());
        assertEquals("gup-b", d0.getSubdomain());
    }

    @Test
    public void decodeWithEmptyArray() {
        List<DNSdataDto> decoded = DnsDataUpsertMetaService.decodeFromJson("[]", "key1");
        assertEquals(0, decoded.size());
    }

    @Test
    public void decodeFailsWithBogusJson() {
        try {
            DnsDataUpsertMetaService.decodeFromJson(" this is not json", "key");
            fail();
        } catch (Exception e) {
            assertThat(e, instanceOf(DataException.class));
            assertThat(e.getMessage(), containsString("bad JSON"));
            assertThat(e.getMessage(), containsString("Unrecognized token"));
        }
    }

    @Test
    public void decodeFailsWithJsonInWrongForm() {
        try {
            DnsDataUpsertMetaService.decodeFromJson("{ \"foo\": false }", "key");
            fail();
        } catch (Exception e) {
            assertThat(e, instanceOf(DataException.class));
            assertThat(e.getMessage(), containsString("bad JSON"));
            assertThat(e.getMessage(), containsString("Can not deserialize"));
        }
    }

}
