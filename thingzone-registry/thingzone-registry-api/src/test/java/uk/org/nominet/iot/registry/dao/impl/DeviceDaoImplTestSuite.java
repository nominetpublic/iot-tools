/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.DNSkeyDao;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.DeviceDao;
import uk.org.nominet.iot.registry.dao.HistoryDao;
import uk.org.nominet.iot.registry.database.DbObjectStreamer;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.Device;
import uk.org.nominet.iot.registry.model.EntityStatus;
import uk.org.nominet.iot.registry.model.History;
import uk.org.nominet.iot.registry.model.HistoryAction;

/**
 * Test for Device DAO
 */
@SuppressWarnings("unused") // ignore in test classes
@RunWith(JUnit4.class)
public class DeviceDaoImplTestSuite {

    private String testKey = "aaaa.bbbb.cccc.dddd.eeee.ffff.gggg.h.i";
    private String dummyKey = "beef.dead.dead.beef.dead.beef.dead.beef.h.i";

    private DeviceDao dao;
    private DNSkeyDao dnsKeyDao;
    private HistoryDao historyDao;

    @Before
    public void setUp() {

        DaoImplTest.setUp();
        DaoFactory df = DaoFactoryManager.getFactory();

        dao = df.deviceDao();
        dnsKeyDao = df.dnsKeyDao();
        historyDao = df.historyDao();
    }

    @After
    public void tearDown() {
        DaoImplTest.tearDown();
    }

    @Test
    public void create() {
        Device device = dao.create();
        assertNotNull(device);
    }

    @Test
    public void createKnownKey() {
        // create a key
        String testKey = "aaaa.bbbb.cccc.dddd.eeee.ffff.gggg.h.i";
        Device device = dao.createKnownKey(testKey);
        assertNotNull(device);
        assertEquals(testKey, device.getKey().getKey());
        assertEquals(device.getKey().getStatus(), EntityStatus.DEVICE);

        List<History> listByKeyAction = historyDao.listByKeyAction(testKey, HistoryAction.CREATED);
        assertEquals(listByKeyAction.isEmpty(), false);
    }

    @Test
    public void getByName() {
        Device original = dao.create();
        assertNotNull(original);

        // should be a matching Device
        Device found = dao.getByName(original.getKey().getKey());
        assertNotNull(found);
        assertEquals(original.getKey(), found.getKey());

        // and a matching (parent) DNSkey
        DNSkey foundKey = dnsKeyDao.getByKey(original.getKey().getKey());
        assertNotNull(foundKey);
        assertEquals(original.getKey().getKey(), foundKey.getKey());
    }

    @Test(expected = NoResultException.class)
    public void getByNameNonexistent() {
        dao.getByName(dummyKey);
        fail();
    }

    @Test
    public void createInvalidKeyFails() {
        // attempt to create an invalid key
        try {
            Device device = dao.createKnownKey("INVALID");
            fail();
        } catch (DataException e) {
        }
    }

    @Test
    public void createPreExistingKeyFails() {
        // create a key
        Device device = dao.createKnownKey(testKey);
        assertNotNull(device);
        assertEquals(testKey, device.getKey().getKey());

        try {
            dao.createKnownKey(testKey);
            fail();
        } catch (DataException e) {
        }
    }

    @Test
    public void deleteByValue() {
        Device original = dao.create();
        assertNotNull(original);
        assertEquals(true, dao.exists(original.getKey().getKey()));

        // delete
        dao.delete(original);

        // check Device gone
        assertEquals(false, dao.exists(original.getKey().getKey()));
        // and DNSkey is set to deleted
        try {
            dnsKeyDao.getByKey(original.getKey().getKey());
            fail();
        } catch (DataException e) {
        }

        List<History> listByKeyAction = historyDao.listByKeyAction(original.getKey().getKey(), HistoryAction.DELETED);
        assertEquals(listByKeyAction.isEmpty(), false);
    }

    @Test
    public void deleteNonexistentByValue() {
        Device device = new Device();
        DNSkey dnsKey = new DNSkey(dummyKey);
        device.setKey(dnsKey);
        try {
            dao.delete(device);
            // fail(); // deleteEntity doesn't throw
        } catch (NoResultException e) {
        }
    }

    @Test
    public void list() {
        List<Device> devices = dao.findAll();
        assertEquals("no devices created before test", 0, devices.size());

        dao.create();
        dao.create();

        devices = dao.findAll();
        assertEquals("2 devices created", 2, devices.size());
    }

    @Test
    public void updateMetadata() {
        Device device = dao.create();
        assertNotNull(device);

        device.setMetadata("{}");
        dao.persist(device);
        dao.flush();
    }

    @Test
    public void updateBogusMetadata() {
        Device device = dao.create();
        assertNotNull(device);

        device.setMetadata("{invalid json");
        dao.persist(device);
        try {
            dao.flush();
            fail();
        } catch (PersistenceException e) {
        }
    }

    @Test
    public void streamAll() {
        List<Device> devices = dao.findAll();
        assertEquals("no devices created before test", 0, devices.size());

        dao.create();
        dao.create();

        DbObjectStreamer<Device> stream = dao.streamAll();
        List<Device> found = new ArrayList<>();
        while (!stream.atEnd()) {
            found.add(stream.read());
        }
        stream.close();

        assertEquals("devices read via stream", 2, found.size());
    }

}
