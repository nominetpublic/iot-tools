/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import javax.persistence.NoResultException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.TransformFunctionDao;
import uk.org.nominet.iot.registry.model.TransformFunction;
import uk.org.nominet.iot.registry.model.User;

/**
 * Test for Transform Function DAO
 */
@RunWith(JUnit4.class)
public class TransformFunctionDaoImplTestSuite {

    DaoFactory df;

    TransformFunctionDao dao;
    final static String dummyJson = "{\"key\":\"value\"}";

    User testUser;

    @Before
    public void setUp() throws Exception {
        DaoImplTest.setUp();
        df = DaoFactoryManager.getFactory();

        dao = df.transformFunctionDao();
        testUser = df.userDao().create();
    }

    @After
    public void tearDown() {
        DaoImplTest.tearDown();
    }

    @Test
    public void create() {
        assertNotNull(dao.create("foo", testUser, "javascript", "{}"));
    }

    @Test
    public void getByName() {
        TransformFunction tf1 = dao.create("foo", testUser, "javascript", "{}");
        TransformFunction tf2 = dao.create("bar", testUser, "javascript", "{}");
        TransformFunction tf3 = dao.create("baz", testUser, "javascript", "{}");
        assertNotNull(tf1);
        assertNotNull(tf2);
        assertNotNull(tf3);

        TransformFunction found = dao.getByName("foo");
        assertEquals(found, tf1);

        found = dao.getByName("bar");
        assertEquals(found, tf2);

        found = dao.getByName("baz");
        assertEquals(found, tf3);
    }

    @Test
    public void duplicateNamesRejected() {
        dao.create("foo", testUser, "javascript", "{}");
        try {
            dao.create("foo", testUser, "javascript", "{}");
            fail();
        } catch (DataException e) {
            assertThat(e.getMessage(), containsString("exists already"));
        }
    }

    @Test(expected = NoResultException.class)
    public void getByNameNonexistent() {
        dao.getByName("foo");
        fail();
    }

    @Test
    public void list() {
        List<TransformFunction> tfs = dao.findAll();
        assertEquals("no TransformFunctions created before test", 0, tfs.size());

        dao.create("foo", testUser, "javascript", "{}");
        dao.create("bar", testUser, "javascript", "{}");
        dao.create("baz", testUser, "javascript", "{}");

        tfs = dao.findAll();
        assertEquals("3 transform functions created", 3, tfs.size());
    }

    @Test
    public void updateName() {
        TransformFunction tf1 = dao.create("foo", testUser, "javascript", "{}");
        assertNotNull(tf1);

        dao.updateName("foo", "bar");
        dao.flush();

        assertTrue(dao.exists("bar"));
        assertFalse(dao.exists("foo"));
    }

    @Test
    public void updateFunction() {
        TransformFunction tf1 = dao.create("foo", testUser, "test", "{}");
        assertNotNull(tf1);

        String functionType = "javascript";
        String functionContent = "{\"foo\":\"bar\"}";
        dao.updateFunction("foo", functionType, functionContent);
        dao.flush();

        TransformFunction found = dao.getByName("foo");
        assertNotNull(found);
        assertEquals(functionType, found.getFunctionType());
        assertEquals(functionContent, found.getFunctionContent());
    }
}
