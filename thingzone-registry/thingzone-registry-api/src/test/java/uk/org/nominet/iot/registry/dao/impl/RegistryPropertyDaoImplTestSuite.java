/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import static org.junit.Assert.*;

import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.RegistryPropertyDao;
import uk.org.nominet.iot.registry.model.RegistryProperty;

/**
 * Integration test for registry properties
 */
@RunWith(JUnit4.class)
public class RegistryPropertyDaoImplTestSuite {

    RegistryPropertyDao dao;

    @Before
    public void setUp() throws Exception {
        DaoImplTest.setUp();
        DaoFactory df = DaoFactoryManager.getFactory();
        dao = df.registryPropertyDao();
    }

    @After
    public void tearDown() {
        DaoImplTest.tearDown();
    }

    @Test
    public void propertiesIncludesDbCreationDate() {

        Properties props = dao.getProperties();
        assertTrue(props.containsKey(RegistryProperty.PROPERTY_DB_CREATED));
    }

    @Test
    public void propertiesIncludesDbCreationDate2() {

        Properties props = dao.getProperties();
        assertTrue(props.containsKey(RegistryProperty.PROPERTY_DB_CREATED));
    }

}
