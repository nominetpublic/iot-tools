/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import javax.persistence.NoResultException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.DataStreamDao;
import uk.org.nominet.iot.registry.dao.PushSubscriberDao;
import uk.org.nominet.iot.registry.database.DatabaseDriver;

import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.model.PushSubscriber;
import uk.org.nominet.iot.registry.model.User;

/**
 * Test for Push Subscriber DAO
 */
@RunWith(JUnit4.class)
public class PushSubscriberDaoImplTestSuite {

    DaoFactory df;

    PushSubscriberDao dao;
    DataStreamDao dataStreamDao;

    // test data
    DataStream ds1, ds2;
    String uri1, uri2;
    String uPs;
    String headers;
    String method;
    String retrySequence;

    @Before
    public void setUp() throws Exception {
        DaoImplTest.setUp();
        df = DaoFactoryManager.getFactory();

        dao = df.pushSubscriberDao();
        dataStreamDao = df.dataStreamDao();

        ds1 = dataStreamDao.create();
        ds2 = dataStreamDao.create();
        assertNotNull(ds1);
        assertNotNull(ds2);

        uri1 = "http://www.google.com";
        uri2 = "mqtt://user:pass@host.example:42/foo/bar?clientid=clientid";

        uPs = "{\"foo\":\"bar\"}";
        headers
            = "{\"httpHeaders\":[\"Accept: text/plain\", \"Cache-Control: no-cache\"], \"httpContentType\":\"text/plain\"}";
        method = "POST";
        retrySequence = "";
    }

    @After
    public void tearDown() {
        DaoImplTest.tearDown();
    }

    @Test
    public void create() {
        assertNotNull(dao.create(ds1, uri1, uPs, headers, method, retrySequence));
    }

    @Test
    public void getAll() {
        Account publicAccount = getPublicAccount();

        // empty list
        List<PushSubscriber> found = dao.getAllowed(Arrays.asList(publicAccount), Permission.CONSUME);
        assertNotNull(found);
        assertEquals("no Push Subscribers", 0, found.size());

        // list with 2
        assertNotNull(dao.create(ds1, uri1, uPs, headers, method, retrySequence));
        assertNotNull(dao.create(ds1, uri2, uPs, headers, method, retrySequence));
        assertNotNull(dao.create(ds2, uri1, uPs, headers, method, retrySequence));

        df.aclDao().create(ds1.getKey(), publicAccount, Permission.CONSUME);
        df.aclDao().create(ds2.getKey(), publicAccount, Permission.CONSUME);

        found = dao.getAllowed(Arrays.asList(publicAccount), Permission.CONSUME);
        assertNotNull(found);
        assertEquals("3 Push Subscribers", 3, found.size());
    }

    @Test(expected = NoResultException.class)
    public void getNonexistent() {
        dao.get(ds1, "arse");
        fail();
    }

    @Test
    public void getForStreamNonexistent() {
        List<PushSubscriber> found = dao.getForStream(ds1);
        assertNotNull(found);
        assertEquals("no Push Subscribers", 0, found.size());
    }

    @Test
    public void getForStream() {
        PushSubscriber ps1 = dao.create(ds1, uri1, uPs, headers, method, retrySequence);
        PushSubscriber ps2 = dao.create(ds1, uri2, uPs, headers, method, retrySequence);
        assertNotNull(ps1);
        assertNotNull(ps2);
        List<PushSubscriber> found = dao.getForStream(ds1);
        assertNotNull(found);
        assertEquals("2 Push Subscribers", 2, found.size());
    }

    @Test
    public void get() {
        PushSubscriber ps1 = dao.create(ds1, uri1, uPs, headers, method, retrySequence);
        PushSubscriber ps2 = dao.create(ds1, uri2, uPs, headers, method, retrySequence);
        assertNotNull(ps1);
        assertNotNull(ps2);
        PushSubscriber found = dao.get(ds1, uri1);
        assertNotNull(found);
    }

    @Test
    public void updateContext() {
        PushSubscriber ps = dao.create(ds1, uri1, uPs, headers, method, retrySequence);
        assertNotNull(ps);
        ps.setUriParams("{}");
        dao.persist(ps);
        ps = dao.get(ds1, uri1);
        assertEquals("update value", "{}", ps.getUriParams());
    }

    @Test(expected = NoResultException.class)
    public void deleteNonexistent() {
        dao.delete(ds1, uri1);
        fail();
    }

    @Test(expected = NoResultException.class)
    public void delete() {
        PushSubscriber ps = dao.create(ds1, uri1, uPs, headers, method, retrySequence);
        assertNotNull(ps);
        dao.delete(ds1, uri1);

        dao.get(ds1, uri1);
        fail();
    }

    public Account getPublicAccount() {
        User publicUser = df.userDao().getUserByUsername(DatabaseDriver.PUBLIC_USERNAME);
        return df.accountDao().getByOwner(publicUser);
    }

}
