/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.DataStreamDao;
import uk.org.nominet.iot.registry.dao.TransformDao;
import uk.org.nominet.iot.registry.dao.TransformFunctionDao;
import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.model.Transform;
import uk.org.nominet.iot.registry.model.TransformFunction;
import uk.org.nominet.iot.registry.model.User;

/**
 * Test for Transform DAO
 */
@RunWith(JUnit4.class)
public class TransformDaoImplTestSuite {

    DaoFactory df;

    TransformDao dao;
    TransformFunctionDao tfDao;
    DataStreamDao dataStreamDao;
    final static String dummyJson = "{\"key\":\"value\"}";
    Account testAccount;
    User testUser;

    @Before
    public void setUp() throws Exception {
        DaoImplTest.setUp();
        df = DaoFactoryManager.getFactory();

        dao = df.transformDao();
        tfDao = df.transformFunctionDao();
        dataStreamDao = df.dataStreamDao();

        testUser = df.userDao().create();
        testAccount = df.accountDao().create(testUser);
    }

    @After
    public void tearDown() {
        DaoImplTest.tearDown();
    }

    @Test
    public void create() {
        // with pre-existing datastream
        TransformFunction tf1 = tfDao.create("foo", testUser, "javascript", "{}");
        DataStream ds = dataStreamDao.create();
        Transform t1 = dao.create(ds, tf1, "{\"p1\":42}", "");
        assertNotNull(t1);
        assertEquals("", t1.getRunSchedule());

        // with create-on-the-fly datastream
        Transform t2 = dao.create(tf1, "{\"p1\":42}", "");
        assertNotNull(t2);

        // with runSchedule
        Transform t3 = dao.create(tf1, "{\"p1\":42}", "daily");
        assertNotNull(t3);
        assertEquals("daily", t3.getRunSchedule());
    }

    @Test
    public void get() {
        TransformFunction tf = tfDao.create("foo", testUser, "javascript", "{}");
        Transform t = dao.create(tf, "{\"p1\":42}", "");
        assertNotNull(t);

        Transform found = dao.getById(t.getId());
        assertNotNull(found);

        found = dao.getByOutputStream(t.getDataStream());
        assertNotNull(found);
    }

    @Test
    public void getAllowed() {
        TransformFunction tf = tfDao.create("foo", testUser, "javascript", "{}");
        Transform t = dao.create(tf, "{\"p1\":42}", "");
        assertNotNull(t);

        Transform t2 = dao.create(tf, "{\"p1\":42}", "");
        assertNotNull(t2);

        df.aclDao().create(t.getDataStream().getKey(), testAccount, Permission.MODIFY);
        df.aclDao().create(t2.getDataStream().getKey(), testAccount, Permission.CONSUME);

        // get all
        List<Transform> found
            = dao.getAllowed(Arrays.asList(testAccount), Arrays.asList(Permission.MODIFY, Permission.CONSUME));
        assertNotNull(found);
        assertEquals(2, found.size());

        // get with MODIFY permission
        found = dao.getAllowed(Arrays.asList(testAccount), Permission.MODIFY);
        assertNotNull(found);
        assertEquals(1, found.size());
    }

    @Test
    public void getTransformWithRunSchedule() {
        TransformFunction tf = tfDao.create("foo", testUser, "javascript", "{}");
        Transform t1 = dao.createKnownKey("t1", tf, "{}", "");
        Transform t2 = dao.createKnownKey("t2", tf, "{}", "");
        Transform t3 = dao.createKnownKey("t3", tf, "{}", "");
        assertNotNull(t1);
        assertNotNull(t2);
        assertNotNull(t3);

        List<Transform> found;

        // none have runSchedule
        found = dao.getTransformsWithRunSchedule();
        assertNotNull(found);
        assertEquals(0, found.size());

        // all have runSchedule
        t1.setRunSchedule("shed1");
        t2.setRunSchedule("shed2");
        t3.setRunSchedule("shed3");
        found = dao.getTransformsWithRunSchedule();
        assertNotNull(found);
        assertEquals(3, found.size());

        // one has runSchedule
        t1.setRunSchedule("");
        t2.setRunSchedule("shed2");
        t3.setRunSchedule("");
        found = dao.getTransformsWithRunSchedule();
        assertNotNull(found);
        assertEquals(1, found.size());

    }

}
