/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.common;

import static org.junit.Assert.*;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import uk.org.nominet.iot.registry.common.UidGenerator;

@RunWith(JUnit4.class)
public class DeviceKeyFormatterTest {

    @Test
    public void acceptsOwnOutput() {
        String src = UidGenerator.generateRandomDeviceKey();
        String key = DeviceKeyFormatter.format(src);
        assertTrue(DeviceKeyFormatter.isValid(key));
    }

    @Test
    public void validKeysAllowed() {
        assertTrue(DeviceKeyFormatter.isValid("vafi-llqp-yl3o-32vc-6k2w-5dva.g.a"));
        assertTrue(DeviceKeyFormatter.isValid("vafi-llqp-yl3o-32vc-6k2w-5dva.g.a"));
    }

    @Test
    public void invalidKeysAreSpotted() {
        assertFalse(DeviceKeyFormatter.isValid(""));
        assertFalse(DeviceKeyFormatter.isValid(" vafi-llqp-yl3o-32vc-6k2w-5dva.g.a")); // leading space
        assertFalse(DeviceKeyFormatter.isValid("vafi-llqp-yl3o-32vc-6k2w-5dva.g.a ")); // trailing space
        assertFalse(DeviceKeyFormatter.isValid("0afi-llqp-yl3o-32vc-6k2w-5dva.g.a")); // 0 is not allowed
        assertFalse(DeviceKeyFormatter.isValid("vafi-llqp-yl3o-32vc6k2w-5dva.g.a")); // misformatted
        assertFalse(DeviceKeyFormatter.isValid("Vafi-llqp-yl3o-32vc-6k2w-5dva.g.a")); // case
        assertFalse(DeviceKeyFormatter.isValid("vafi-llqp-yl3o-32vc-6k2w-5dva.g-a")); // dot is escaped in pattern :-)
    }

}
