/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.org.nominet.iot.registry.dao.DNSkeyDao;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.ZoneFileChangeDao;

import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.ZoneFileChange;
import uk.org.nominet.iot.registry.model.ZoneFileChangeOperation;
import uk.org.nominet.iot.registry.service.dto.DNSdataDto;

/**
 * Test for time zone changing DAO
 */
@RunWith(JUnit4.class)
public class ZoneFileChangeDaoImplTestSuite {
    private final static Logger log = LoggerFactory.getLogger(ZoneFileChangeDaoImplTestSuite.class);

    DNSkey dummyDnsKey;
    DNSkey dummyDnsKey2;

    DNSkeyDao dnsKeyDao;
    ZoneFileChangeDao dao;

    @Before
    public void setUp() throws Exception {

        DaoImplTest.setUp();
        DaoFactory df = DaoFactoryManager.getFactory();

        dao = df.zoneFileChangeDao();
        dnsKeyDao = df.dnsKeyDao();
        dummyDnsKey = dnsKeyDao.create();
        dummyDnsKey2 = dnsKeyDao.create();
    }

    @After
    public void tearDown() {
        DaoImplTest.tearDown();
    }

    @Test
    public void create() {
        DNSdataDto ddr = new DNSdataDto(dummyDnsKey, "foo", "sd", 42, "A", "1.2.3.4");

        ZoneFileChange zfc = dao.createRecord(ZoneFileChangeOperation.ADD, ddr);
        assertNotNull(zfc);

        List<ZoneFileChange> changes = dao.getUnprocessedChanges(100);
        assertEquals(1, changes.size());
    }

    @Test
    public void limitsAndOrdering() throws InterruptedException {
        // at least on my machine, this runs fast enough to create
        // several updates per millisecond, demonstrating the 2nd-level order-by
        for (int i = 0; i < 25; ++i) {
            DNSdataDto ddr = new DNSdataDto(dummyDnsKey, "foo", "sd", i, "A", "1.2.3.4");
            ZoneFileChange zfc = dao.createRecord(ZoneFileChangeOperation.ADD, ddr);
            assertNotNull(zfc);
        }

        List<ZoneFileChange> changes = dao.getUnprocessedChanges(20);
        assertEquals(20, changes.size());

        for (ZoneFileChange change : changes) {
            log.info("ttl={} trans={} ts={} ms={}", change.getTtl(), change.getTransaction(), change.getTimestamp(),
                change.getTimestamp().getTime());
        }

        assertEquals(0, changes.get(0).getTtl().intValue());
        assertEquals(1, changes.get(1).getTtl().intValue());
        assertEquals(18, changes.get(18).getTtl().intValue());
        assertEquals(19, changes.get(19).getTtl().intValue());
    }

    @Test
    public void uniqueTransactions() throws InterruptedException {
        dao.createRecord(ZoneFileChangeOperation.ADD, new DNSdataDto(dummyDnsKey, "foo", "sd", 42, "A", "1.2.3.4"));
        dao.createRecord(ZoneFileChangeOperation.ADD, new DNSdataDto(dummyDnsKey, "foo", "sd", 69, "A", "1.2.3.4"));

        List<ZoneFileChange> changes = dao.getUnprocessedChanges(100);
        assertEquals(2, changes.size());

        for (ZoneFileChange change : changes) {
            log.info("ttl={} trans={} ts={} ms={}", change.getTtl(), change.getTransaction(), change.getTimestamp(),
                change.getTimestamp().getTime());
        }

        assertEquals(42, changes.get(0).getTtl().intValue());
        assertEquals(69, changes.get(1).getTtl().intValue());

        Long tx1 = changes.get(0).getTransaction();
        Long tx2 = changes.get(1).getTransaction();
        assertEquals(1, tx2 - tx1);
    }

    @Test
    public void writeSerial() throws InterruptedException {
        dao.createRecord(ZoneFileChangeOperation.ADD, new DNSdataDto(dummyDnsKey, "foo", "sd", 42, "A", "1.2.3.4"));

        List<ZoneFileChange> changes = dao.getUnprocessedChanges(100);
        assertEquals(1, changes.size());

        ZoneFileChange zfc = changes.get(0);
        assertNull(zfc.getSerial());
        dao.setSerial(zfc, 42);

        changes = dao.getUnprocessedChanges(100);
        assertEquals(0, changes.size());
    }

}
