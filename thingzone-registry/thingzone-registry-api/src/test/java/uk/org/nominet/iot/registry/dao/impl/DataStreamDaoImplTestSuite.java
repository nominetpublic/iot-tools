/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;

import javax.persistence.NoResultException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.DNSkeyDao;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.DataStreamDao;
import uk.org.nominet.iot.registry.dao.HistoryDao;
import uk.org.nominet.iot.registry.database.DatabaseDriver;
import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.EntityStatus;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.model.User;

/**
 * Test for DataStream DAO
 */
@SuppressWarnings("unused") // ignore in test classes
@RunWith(JUnit4.class)
public class DataStreamDaoImplTestSuite {

    String testKey = "aaaa.bbbb.cccc.dddd.eeee.ffff.gggg.h.i";
    String dummyKey = "dead.beef.dead.beef.dead.beef.dead.beef.h.i";

    private DataStreamDao dao;
    private DNSkeyDao dnsKeyDao;
    private DaoFactory df;
    private HistoryDao historyDao;

    @Before
    public void setUp() throws Exception {
        DaoImplTest.setUp();
        df = DaoFactoryManager.getFactory();
        dao = df.dataStreamDao();
        dnsKeyDao = df.dnsKeyDao();
        historyDao = df.historyDao();
    }

    @After
    public void tearDown() {
        DaoImplTest.tearDown();
    }

    @Test
    public void create() {
        DataStream dataStream = dao.create();
        assertNotNull(dataStream);
    }

    @Test
    public void createKnownKey() {
        // create a key
        String testKey = "aaaa.bbbb.cccc.dddd.eeee.ffff.gggg.h.i";
        DataStream dataStream = dao.createKnownKey(testKey);
        assertNotNull(dataStream);
        assertEquals(testKey, dataStream.getKey().getKey());
        assertEquals(dataStream.getKey().getStatus(), EntityStatus.DATASTREAM);
    }

    @Test
    public void getByName() {
        DataStream original = dao.create();
        assertNotNull(original);

        // should be a matching DataStream
        DataStream found = dao.getByName(original.getKey().getKey());
        assertNotNull(found);
        assertEquals(original.getKey(), found.getKey());

        // and a matching (parent) DNSkey
        DNSkey foundKey = dnsKeyDao.getByKey(original.getKey().getKey());
        assertNotNull(foundKey);
        assertEquals(original.getKey().getKey(), foundKey.getKey());
    }

    @Test(expected = NoResultException.class)
    public void getByNameNonexistent() {
        dao.getByName(dummyKey);
        fail();
    }

    @Test
    public void createInvalidKeyFails() {
        // attempt to create an invalid key
        try {
            DataStream datastream = dao.createKnownKey("INVALID");
            fail();
        } catch (DataException e) {
        }
    }

    @Test
    public void createPreExistingKeyFails() {
        // create a key
        DataStream dataStream = dao.createKnownKey(testKey);
        assertNotNull(dataStream);
        assertEquals(testKey, dataStream.getKey().getKey());

        try {
            dao.createKnownKey(testKey);
            fail();
        } catch (DataException e) {
        }
    }

    @Test
    public void deleteByValue() {
        DataStream original = dao.create();
        assertNotNull(original);
        assertEquals(true, dao.exists(original.getKey().getKey()));

        // delete
        dao.delete(original);

        // check DataStream gone
        assertEquals(false, dao.exists(original.getKey().getKey()));
        // and dnsKey has been set to deleted
        try {
            dnsKeyDao.getByKey(original.getKey().getKey());
            fail();
        } catch (DataException e) {
        }
    }

    @Test
    public void deleteNonexistentByValue() {
        DataStream dataStream = new DataStream();
        DNSkey dnsKey = new DNSkey(dummyKey);
        dataStream.setKey(dnsKey);
        try {
            dao.delete(dataStream);
            // fail(); // deleteEntity not throwing
        } catch (NoResultException e) {
        }
    }

    @Test
    public void list() {
        List<DataStream> dataStreams = dao.findAll();
        assertEquals("no dataStreams created before test", 0, dataStreams.size());

        dao.create();
        dao.create();

        dataStreams = dao.findAll();
        assertEquals("2 dataStreams created", 2, dataStreams.size());
    }

    @Test
    public void listVisibleToPublic() {
        List<DataStream> dataStreams = dao.findAll();
        assertEquals("no dataStreams created before test", 0, dataStreams.size());

        Account publicAccount = getPublicAccount();

        // not visible to public when initially created
        DataStream dataStream1 = dao.create();
        DataStream dataStream2 = dao.create();
        dataStreams = dao.getAllowed(Arrays.asList(publicAccount), Permission.DISCOVER);
        assertEquals(0, dataStreams.size());

        // add public privilege to 1
        df.aclDao().create(dataStream1.getKey(), publicAccount, Permission.DISCOVER);
        dataStreams = dao.getAllowed(Arrays.asList(publicAccount), Permission.DISCOVER);
        assertEquals(1, dataStreams.size());

        // add public privilege to other
        df.aclDao().create(dataStream2.getKey(), publicAccount, Permission.DISCOVER);
        dataStreams = dao.getAllowed(Arrays.asList(publicAccount), Permission.DISCOVER);
        assertEquals(2, dataStreams.size());
    }

    @Test
    public void listVisibleToUser() {
        List<DataStream> dataStreams = dao.findAll();
        assertEquals("no dataStreams created before test", 0, dataStreams.size());

        // Account publicAccount = getPublicAccount();
        Account userAccount = df.accountDao().create(null);

        // not visible to user when initially created
        DataStream dataStream1 = dao.create();
        dataStreams = dao.getAllowed(Arrays.asList(userAccount), Permission.DISCOVER);
        assertEquals(0, dataStreams.size());

        // add public privilege to 1
        df.aclDao().create(dataStream1.getKey(), userAccount, Permission.DISCOVER);
        dataStreams = dao.getAllowed(Arrays.asList(userAccount), Permission.DISCOVER);
        assertEquals(1, dataStreams.size());
    }

    @Test
    public void listVisibleToUserAndPublic() {
        List<DataStream> dataStreams = dao.findAll();
        assertEquals("no dataStreams created before test", 0, dataStreams.size());

        Account publicAccount = getPublicAccount();
        Account userAccount = df.accountDao().create(null);

        DataStream dataStream1 = dao.create();
        DataStream dataStream2 = dao.create();

        // neither visible to user when initially created
        dataStreams = dao.getAllowed(Arrays.asList(userAccount), Permission.DISCOVER);
        assertEquals(0, dataStreams.size());

        // neither visible to public when initially created
        dataStreams = dao.getAllowed(Arrays.asList(publicAccount), Permission.DISCOVER);
        assertEquals(0, dataStreams.size());

        // add public privilege to one, user privilege to other
        df.aclDao().create(dataStream1.getKey(), userAccount, Permission.DISCOVER);
        df.aclDao().create(dataStream2.getKey(), publicAccount, Permission.DISCOVER);

        // user can see one
        dataStreams = dao.getAllowed(Arrays.asList(userAccount), Permission.DISCOVER);
        assertEquals(1, dataStreams.size());

        // public can see other
        dataStreams = dao.getAllowed(Arrays.asList(publicAccount), Permission.DISCOVER);
        assertEquals(1, dataStreams.size());

        // with both accounts, can see both streams
        dataStreams = dao.getAllowed(Arrays.asList(userAccount, publicAccount), Permission.DISCOVER);
        assertEquals(2, dataStreams.size());
    }

    @Test
    public void listSingleStreamWhenMultiplyVisible() {
        List<DataStream> dataStreams = dao.findAll();
        assertEquals("no dataStreams created before test", 0, dataStreams.size());

        Account publicAccount = getPublicAccount();
        Account userAccount = df.accountDao().create(null);

        DataStream dataStream1 = dao.create();

        // add public privilege & user privilege
        df.aclDao().create(dataStream1.getKey(), userAccount, Permission.DISCOVER);
        df.aclDao().create(dataStream1.getKey(), publicAccount, Permission.DISCOVER);

        // user just gets presented with 1 dataStream
        dataStreams = dao.getAllowed(Arrays.asList(userAccount, publicAccount), Permission.DISCOVER);
        assertEquals(1, dataStreams.size());
    }

    public Account getPublicAccount() {
        User publicUser = df.userDao().getUserByUsername(DatabaseDriver.PUBLIC_USERNAME);
        return df.accountDao().getByOwner(publicUser);
    }
}
