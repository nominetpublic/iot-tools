/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.Instant;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.UserCredentialsDao;
import uk.org.nominet.iot.registry.dao.UserDao;
import uk.org.nominet.iot.registry.dao.UserTokenDao;
import uk.org.nominet.iot.registry.model.User;
import uk.org.nominet.iot.registry.model.UserCredentials;
import uk.org.nominet.iot.registry.model.UserToken;
import uk.org.nominet.iot.registry.model.UserTokenType;

public class UserTokenDaoImplTestSuite {

    private UserTokenDao dao;
    private UserDao userDao;
    private UserCredentialsDao userCredentialsDao;

    @Before
    public void setUp() throws Exception {
        DaoImplTest.setUp();
        DaoFactory df = DaoFactoryManager.getFactory();
        dao = df.userTokenDao();
        userDao = df.userDao();
        userCredentialsDao = df.userCredentialsDao();
    }

    @After
    public void tearDown() {
        DaoImplTest.tearDown();
    }

    @Test
    public void create() {
        User user = userDao.create();
        UserCredentials userCredentials = userCredentialsDao.create("test", "password", user, null);

        UserToken token = dao.create(userCredentials, UserTokenType.PASSWORD, 60 * 60 * 24);
        assertNotNull(token);

        assertTrue(dao.exists(token.getToken()));
        assertTrue(dao.valid(token));
    }

    @Test
    public void usedTokenIsInvalid() {
        User user = userDao.create();
        UserCredentials userCredentials = userCredentialsDao.create("test", "password", user, null);

        UserToken token = dao.create(userCredentials, UserTokenType.PASSWORD, 60 * 60 * 24);
        assertNotNull(token);
        token.setUsed(Instant.now());

        assertFalse(dao.valid(token));
    }

    @Test
    public void expiredTokenIsInvalid() {
        User user = userDao.create();
        UserCredentials userCredentials = userCredentialsDao.create("test", "password", user, null);

        UserToken token = dao.create(userCredentials, UserTokenType.PASSWORD, 60 * 60 * 24);
        assertNotNull(token);
        token.setExpiry(Instant.now().minusSeconds(60));

        assertFalse(dao.valid(token));
    }
}
