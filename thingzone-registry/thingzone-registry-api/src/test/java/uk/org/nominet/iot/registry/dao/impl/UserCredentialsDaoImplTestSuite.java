/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.Instant;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.UserCredentialsDao;
import uk.org.nominet.iot.registry.dao.UserDao;
import uk.org.nominet.iot.registry.dao.UserTokenDao;
import uk.org.nominet.iot.registry.database.DatabaseDriver;
import uk.org.nominet.iot.registry.model.User;
import uk.org.nominet.iot.registry.model.UserCredentials;
import uk.org.nominet.iot.registry.model.UserToken;
import uk.org.nominet.iot.registry.model.UserTokenType;

@RunWith(JUnit4.class)
public class UserCredentialsDaoImplTestSuite {

    private UserCredentialsDao dao;
    private UserDao userDao;
    private UserTokenDao userTokenDao;

    @Before
    public void setUp() throws Exception {
        DaoImplTest.setUp();
        DaoFactory df = DaoFactoryManager.getFactory();
        dao = df.userCredentialsDao();
        userDao = df.userDao();
        userTokenDao = df.userTokenDao();
    }

    @After
    public void tearDown() {
        DaoImplTest.tearDown();
    }


    @Test
    public void create() {
        User user = userDao.create();
        assertNotNull(user);

        assertFalse(dao.exists("mr foo"));
        dao.create("mr foo", "password", user, null);
        assertTrue(dao.exists("mr foo"));
    }

    @Test
    public void validLoginAuthenticates() {
        User user = userDao.create();
        assertNotNull(user);

        dao.create("mr foo", "password", user, null);

        User found = dao.authenticate("mr foo", "password");
        assertNotNull(found);
        assertEquals(found, user);
    }

    @Test(expected = DataException.class)
    public void invalidLoginFails() {
        User user = userDao.create();
        assertNotNull(user);

        dao.create("mr foo", "password", user, null);

        dao.authenticate("mr foo", "wrong");
        fail();
    }

    @Test
    public void validPasswordUpdate() {
        User user = userDao.create();
        assertNotNull(user);

        String username = "Mr. Foo";

        dao.create(username, "password", user, null);
        dao.updatePassword(username, "password", "different");

        // new password works
        User found = dao.authenticate(username, "different");
        assertNotNull(found);
        assertEquals(found, user);

        // & old password fails
        try {
            dao.authenticate(username, "password");
            fail();
        } catch (DataException e) {
        }
    }

    @Test
    public void invalidPasswordUpdate() {
        User user = userDao.create();
        assertNotNull(user);

        String username = "Mr. Foo";

        dao.create(username, "password", user, null);
        try {
            dao.updatePassword(username, "wrong old password", "different");
            fail();
        } catch (DataException e) {
        }

        // old password still works
        dao.authenticate(username, "password");

        // & new password fails
        try {
            dao.authenticate(username, "different");
            fail();
        } catch (DataException e) {
        }
    }

    @Test
    public void validTokenPasswordUpdate() {
        User user = userDao.create();
        assertNotNull(user);

        String username = "test";

        UserCredentials userCredentials = dao.create(username, "password", user, null);
        UserToken token = userTokenDao.create(userCredentials, UserTokenType.PASSWORD, 60*60*24);

        dao.tokenUpdatePassword(username, token, "different");
        // new password works
        User found = dao.authenticate(username, "different");
        assertNotNull(found);
        assertEquals(found, user);

        // & old password fails
        try {
            dao.authenticate(username, "password");
            fail();
        } catch (DataException e) {
        }
    }

    @Test
    public void usedTokenPasswordUpdate() {
        User user = userDao.create();
        assertNotNull(user);

        String username = "test";

        UserCredentials userCredentials = dao.create(username, "password", user, null);
        UserToken token = userTokenDao.create(userCredentials, UserTokenType.PASSWORD, 60*60*24);
        token.setUsed(Instant.now());

        try {
            dao.tokenUpdatePassword(username, token, "different");
            fail();
        } catch (DataException e) {
        }
        // old password still works
        dao.authenticate(username, "password");

        // & new password fails
        try {
            dao.authenticate(username, "different");
            fail();
        } catch (DataException e) {
        }
    }

    @Test
    public void expiredTokenPasswordUpdate() {
        User user = userDao.create();
        assertNotNull(user);

        String username = "test";

        UserCredentials userCredentials = dao.create(username, "password", user, null);
        UserToken token = userTokenDao.create(userCredentials, UserTokenType.PASSWORD, 60*60*24);
        token.setExpiry(Instant.now().minusSeconds(60));

        try {
            dao.tokenUpdatePassword(username, token, "different");
            fail();
        } catch (DataException e) {
        }
        // old password still works
        dao.authenticate(username, "password");

        // & new password fails
        try {
            dao.authenticate(username, "different");
            fail();
        } catch (DataException e) {
        }
    }


    @Test
    public void getPublicUser() {
        User found = dao.getUserByUsername("public");
        assertNotNull(found);
    }

    @Test
    public void getUserAdminUser() {
        User found = dao.getUserByUsername(DatabaseDriver.USERADMIN_USERNAME);
        assertNotNull(found);
    }

    @Test
    public void getUser() {
        User user = userDao.create();
        assertNotNull(user);

        dao.create("mr foo", "password", user, null);

        User found = dao.getUserByUsername("mr foo");
        assertEquals(user, found);

        String username = dao.getUsernameByUser(found);
        assertEquals(username, "mr foo");
    }

    @Test
    public void getUserCredentials() {
        User user = userDao.create();
        assertNotNull(user);

        UserCredentials created = dao.create("mr foo", "password", user, "foo@example.com");

        UserCredentials found = dao.get("mr foo");
        assertEquals(created, found);
        assertEquals("foo@example.com", found.getEmail());
    }

    @Test
    public void passwordInitialisationNotAllowedForNormalUsers() {
        User user = userDao.create();
        assertNotNull(user);

        dao.create("mr foo", "password", user, null);
        assertTrue(dao.isPasswordInitialised("mr foo"));
        try {
            dao.initialisePassword("mr foo", "different");
            fail();
        } catch (DataException e) {
            assertEquals("authentication error", e.getMessage());
        }
    }

    // NB this test isn't idempotent
    @Test
    public void passwordInitialisationForUserAdminUser() {
        // user_admin user exists already
        assertFalse(dao.isPasswordInitialised(DatabaseDriver.USERADMIN_USERNAME));
        dao.initialisePassword(DatabaseDriver.USERADMIN_USERNAME, "password");
        assertTrue(dao.isPasswordInitialised(DatabaseDriver.USERADMIN_USERNAME));
    }

    @Test
    public void passwordInitialisationForOtherUsers() {
        User user = userDao.create();
        assertNotNull(user);

        dao.create("mr foo", "password", user, null);
        assertTrue(dao.isPasswordInitialised("mr foo"));
        try {
            dao.initialisePassword("mr foo", "different");
            fail();
        } catch (DataException e) {
            assertEquals("authentication error", e.getMessage());
        }
    }

}
