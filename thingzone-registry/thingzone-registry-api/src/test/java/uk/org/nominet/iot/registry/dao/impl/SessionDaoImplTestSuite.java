/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import static org.junit.Assert.*;

import static org.hamcrest.CoreMatchers.containsString;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.common.Util;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.SessionDao;

import uk.org.nominet.iot.registry.model.GenericSession;
import uk.org.nominet.iot.registry.model.Session;

@RunWith(JUnit4.class)
public class SessionDaoImplTestSuite {

    SessionDao dao;

    String username = "slartibartfast@example.com";
    String ipv4 = "1.2.3.4";
    String ipv6 = "fd0e:1701:f391:9e18::beef";

    @Before
    public void setUp() throws Exception {
        DaoImplTest.setUp();
        DaoFactory df = DaoFactoryManager.getFactory();

        dao = df.sessionDao();

    }

    @After
    public void tearDown() {
        DaoImplTest.tearDown();
    }

    @Test
    public void login() {
        Session session = dao.open(username, ipv4);
        assertNotNull(session);

        assertEquals(username, session.getUsername());
        assertEquals(ipv4, session.getIpAddr());
    }

    @Test
    public void find() {
        Session session = dao.open(username, ipv6);

        GenericSession found = dao.find(session.getSessionKey());
        assertNotNull(found);
    }

    @Test
    public void mismatchedSessionKeyRefused() {
        GenericSession session = dao.open(username, ipv4);
        assertNotNull(session);

        try {
            dao.find("invalid");
            fail();
        } catch (DataException e) {
            assertThat(e.getMessage(), containsString("not found"));
        }
    }

    @Test
    public void closedSessionRefused() {
        Session session = dao.open(username, ipv4);
        assertNotNull(session);
        session.close();

        try {
            dao.find(session.getSessionKey());
            fail();
        } catch (DataException e) {
            assertThat(e.getMessage(), containsString("session is closed"));
        }
    }

    @Test
    public void expiredSessionRefused() {
        Session session = dao.open(username, ipv4);
        assertNotNull(session);
        session.TESTsetCreated(Util.minutesAgo(11));
        session.TESTsetRefreshed(Util.minutesAgo(10));

        try {
            dao.find(session.getSessionKey());
            fail();
        } catch (DataException e) {
            assertThat(e.getMessage(), containsString("session has expired"));
        }
    }

}
