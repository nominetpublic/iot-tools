/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.DNSdataDao;
import uk.org.nominet.iot.registry.dao.DNSkeyDao;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;

import uk.org.nominet.iot.registry.model.DNSdata;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.service.dto.DNSdataDto;

/**
 * Test for DNS data DAO
 */
@RunWith(JUnit4.class)
public class DNSdataDaoImplTestSuite {

    DNSkey dummyDnsKey;
    DNSkey dummyDnsKey2;

    DNSkeyDao dnsKeyDao;
    DNSdataDao dao;

    @Before
    public void setUp() throws Exception {
        DaoImplTest.setUp();
        DaoFactory df = DaoFactoryManager.getFactory();

        dao = df.dnsDataDao();
        dnsKeyDao = df.dnsKeyDao();
        dummyDnsKey = dnsKeyDao.create();
        dummyDnsKey2 = dnsKeyDao.create();
    }

    @After
    public void tearDown() {
        DaoImplTest.tearDown();
    }

    static DNSdataDto mddr(DNSkey dnsKey, String subdomain, Integer ttl, String rrtype, String rdata) {
        return new DNSdataDto(dnsKey, "foo", subdomain, ttl, rrtype, rdata);
    }

    static boolean contains(List<DNSdata> dnsDatas, DNSdataDto dto) {
        for (DNSdata d : dnsDatas) {
            if (dto.contentsEqual(d)) {
                return true;
            }
        }
        return false;
    }

    @Test
    public void create() {
        assertNotNull(dao.create(mddr(dummyDnsKey, "sd", 42, "A", "1.2.3.4")));
        assertNotNull(dao.create(mddr(dummyDnsKey, "", 60, "AAAA", "dead::beef")));
        assertNotNull(dao.create(mddr(dummyDnsKey, "latest", 60, "URI", "http://thingzone.io/ftw")));
    }

    @Test(expected = DataException.class)
    public void createDuplicatePrevented() {
        assertNotNull(dao.create(mddr(dummyDnsKey, "sd", 42, "A", "1.2.3.4")));
        assertNotNull(dao.create(mddr(dummyDnsKey, "sd", 45, "A", "1.2.3.4"))); // TTL irrelevant
        fail();
    }

    @Test
    public void createDifferentAllowed() {
        assertNotNull(dao.create(mddr(dummyDnsKey, "sd", 42, "A", "1.2.3.4")));
        assertNotNull(dao.create(mddr(dummyDnsKey2, "sd", 42, "A", "1.2.3.4")));
        assertNotNull(dao.create(mddr(dummyDnsKey, "sd2", 42, "A", "1.2.3.4")));
        assertNotNull(dao.create(mddr(dummyDnsKey, "sd", 42, "A", "1.2.3.5")));
    }

    @Ignore
    @Test(expected = DataException.class)
    public void rejectBogusSubdomain() {
        assertNotNull(dao.create(mddr(dummyDnsKey, "has a space", 42, "A", "1.2.3.4")));
    }

    @Ignore
    @Test(expected = DataException.class)
    public void rejectBogusTTL() {
        assertNotNull(dao.create(mddr(dummyDnsKey, "sd", -5, "A", "1.2.3.4")));
    }

    @Ignore
    @Test(expected = DataException.class)
    public void rejectBogusARecord() {
        assertNotNull(dao.create(mddr(dummyDnsKey, "sd", 60, "A", "nonsense")));
    }

    @Ignore
    @Test(expected = DataException.class)
    public void rejectBogusAAAARecord() {
        assertNotNull(dao.create(mddr(dummyDnsKey, "sd", 60, "A", "nonsense")));
    }

    @Ignore
    @Test(expected = DataException.class)
    public void rejectUnhandledRRTYPE() {
        assertNotNull(dao.create(mddr(dummyDnsKey, "sd", 60, "FOO", "nonsense")));
    }

    @Ignore
    @Test(expected = DataException.class)
    public void rejectEmptyRRTYPE() {
        assertNotNull(dao.create(mddr(dummyDnsKey, "sd", 60, "", "eh")));
    }

    @Test
    public void list() {
        List<DNSdata> ds = dao.findAll();
        assertEquals("no devices created before test", 0, ds.size());

        assertNotNull(dao.create(mddr(dummyDnsKey, "sd", 42, "A", "1.2.3.4")));
        assertNotNull(dao.create(mddr(dummyDnsKey, "", 60, "AAAA", "dead::beef")));
        assertNotNull(dao.create(mddr(dummyDnsKey, "latest", 60, "URI", "http://thingzone.io/ftw")));

        ds = dao.listAll();
        assertEquals("3 DNSdata created", 3, ds.size());
    }

    @Test
    public void listByKey() {
        DNSdataDto data1 = mddr(dummyDnsKey, "sd", 42, "A", "1.2.3.4");
        DNSdataDto data2 = mddr(dummyDnsKey, "", 60, "AAAA", "dead::beef");
        DNSdataDto data3 = mddr(dummyDnsKey2, "latest", 60, "URI", "http://thingzone.io/ftw");
        assertNotNull(dao.create(data1));
        assertNotNull(dao.create(data2));
        assertNotNull(dao.create(data3));

        List<DNSdata> ds = dao.listByKey(dummyDnsKey);
        assertEquals("2 DNSdata created for key 1", 2, ds.size());
        assertTrue(contains(ds, data1));
        assertTrue(contains(ds, data2));
        ds = dao.listByKey(dummyDnsKey2);
        assertEquals("1 DNSdata created for key 2", 1, ds.size());
        assertTrue(data3.contentsEqual(ds.get(0)));
    }

    @Test
    public void unmanagedListByKey() {
        DNSdataDto data1 = mddr(dummyDnsKey, "sd", 42, "A", "1.2.3.4");
        DNSdataDto data2 = mddr(dummyDnsKey, "", 60, "AAAA", "dead::beef");
        DNSdataDto data3 = mddr(dummyDnsKey2, "latest", 60, "URI", "http://thingzone.io/ftw");
        assertNotNull(dao.create(data1));
        assertNotNull(dao.create(data2));
        assertNotNull(dao.create(data3));

        List<DNSdata> ds = dao.unmanagedListByKey(dummyDnsKey);
        assertEquals("2 DNSdata created for key 1", 2, ds.size());
        assertTrue(contains(ds, data1));
        assertTrue(contains(ds, data2));
        ds = dao.listByKey(dummyDnsKey2);
        assertEquals("1 DNSdata created for key 2", 1, ds.size());
        assertTrue(data3.contentsEqual(ds.get(0)));
    }

    @Test
    public void exists() {
        assertNotNull(dao.create(mddr(dummyDnsKey, "sd", 42, "A", "1.2.3.4")));
        assertNotNull(dao.create(mddr(dummyDnsKey, "", 60, "AAAA", "dead::beef")));
        assertNotNull(dao.create(mddr(dummyDnsKey2, "latest", 60, "URI", "http://thingzone.io/ftw")));

        assertTrue(dao.exists(mddr(dummyDnsKey, "sd", 0, "A", "1.2.3.4")));
        assertTrue(dao.exists(mddr(dummyDnsKey, "", 0, "AAAA", "dead::beef")));
        assertTrue(dao.exists(mddr(dummyDnsKey2, "latest", 0, "URI", "http://thingzone.io/ftw")));

        assertFalse(dao.exists(mddr(dummyDnsKey2, "sd", 0, "A", "1.2.3.4")));
        assertFalse(dao.exists(mddr(dummyDnsKey, "", 0, "A", "1.2.3.4")));
        assertFalse(dao.exists(mddr(dummyDnsKey, "sd", 0, "A", "1.2.3.5")));
        assertFalse(dao.exists(mddr(dummyDnsKey, "sd", 0, "URI", "http://thingzone.io/ftw")));
    }

    @Test
    public void delete() {
        assertNotNull(dao.create(mddr(dummyDnsKey, "sd", 42, "A", "1.2.3.4")));
        assertTrue(dao.exists(mddr(dummyDnsKey, "sd", 0, "A", "1.2.3.4")));

        DNSdata dd = dao.get(mddr(dummyDnsKey, "sd", 0, "A", "1.2.3.4"));
        assertNotNull(dd);
        dao.delete(dd);

        assertFalse(dao.exists(mddr(dummyDnsKey, "sd", 0, "A", "1.2.3.4")));
    }
}
