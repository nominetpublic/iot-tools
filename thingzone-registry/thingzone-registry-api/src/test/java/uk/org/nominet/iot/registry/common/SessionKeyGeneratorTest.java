/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.common;

import static org.junit.Assert.*;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class SessionKeyGeneratorTest {

    @Test
    public void keyIsCorrectLength() {
        String key = SessionKeyGenerator.generateKey();
        assertEquals(SessionKeyGenerator.SESSION_KEY_BITS / SessionKeyGenerator.BITS_PER_BASE64, key.length());
        assertEquals(22, key.length());
    }

    @Test
    public void uidsAreDifferent() {
        String key1 = SessionKeyGenerator.generateKey();
        String key2 = SessionKeyGenerator.generateKey();
        assertNotEquals(key1, key2);
    }

}
