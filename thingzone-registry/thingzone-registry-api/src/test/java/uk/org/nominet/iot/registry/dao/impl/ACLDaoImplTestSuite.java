/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import static org.junit.Assert.*;

import java.util.*;

import javax.persistence.PersistenceException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.ACLDao;
import uk.org.nominet.iot.registry.dao.AccountDao;
import uk.org.nominet.iot.registry.dao.DNSkeyDao;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.HistoryDao;
import uk.org.nominet.iot.registry.dao.UserDao;
import uk.org.nominet.iot.registry.dao.dto.AccountId;

import uk.org.nominet.iot.registry.database.DatabaseDriver;
import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.History;
import uk.org.nominet.iot.registry.model.HistoryAction;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.model.User;

@SuppressWarnings("unused") // ignore in test classes
@RunWith(JUnit4.class)
public class ACLDaoImplTestSuite {

    private ACLDao dao;

    private DNSkeyDao dnsKeyDao;
    private UserDao userDao;
    private AccountDao accountDao;

    private DNSkey key1;
    private User user1;
    private Account account1;

    private User user2;
    private Account account2;
    private HistoryDao historyDao;

    @Before
    public void setUp() {
        DaoImplTest.setUp();
        DaoFactory df = DaoFactoryManager.getFactory();
        dao = df.aclDao();

        dnsKeyDao = df.dnsKeyDao();
        userDao = df.userDao();
        accountDao = df.accountDao();
        historyDao = df.historyDao();

        key1 = dnsKeyDao.create();
        user1 = userDao.create();
        user2 = userDao.create();
        account1 = accountDao.create(user1);
        account2 = accountDao.create(user2);
    }

    @After
    public void tearDown() {
        DaoImplTest.tearDown();
    }

    @Test
    public void create() {
        dao.create(key1, account1, Permission.ADMIN);
    }

    @Test
    public void duplicatesIgnored() {
        dao.create(key1, account1, Permission.ADMIN);
        try {
            dao.create(key1, account1, Permission.ADMIN);
            fail();
        } catch (DataException e) {
        }
    }

    @Test
    public void duplicatesPreventedByDbConstraint() {
        dao.create(key1, account1, Permission.ADMIN);
        try {
            ACLDaoImpl daoImpl = (ACLDaoImpl) dao;
            daoImpl.createWithoutCheck(key1, account1, Permission.ADMIN);
            dao.flush();
            fail();
        } catch (PersistenceException e) {
        }
    }

    @Test
    public void retrievePermission() {
        dao.create(key1, account1, Permission.DISCOVER);
        dao.create(key1, account1, Permission.ADMIN);

        List<Permission> perms = dao.get(key1, account1);
        assertEquals(2, perms.size());
        assertTrue(perms.contains(Permission.ADMIN));
        assertTrue(perms.contains(Permission.DISCOVER));
    }

    @Test
    public void retrieveAccount() {
        dao.create(key1, account1, Permission.DISCOVER);
        dao.create(key1, account1, Permission.ADMIN);
        dao.create(key1, account2, Permission.DISCOVER);

        List<Account> accs = dao.get(key1, Permission.DISCOVER);
        assertEquals(2, accs.size());
        assertTrue(accs.contains(account1));
        assertTrue(accs.contains(account2));
    }

    @Test
    public void isAllowed() {

        // nothing allowed to start with
        assertEquals(false, dao.isAllowed(key1, new ArrayList<Account>(), Permission.DISCOVER));
        assertEquals(false, dao.isAllowed(key1, Arrays.asList(account1), Permission.DISCOVER));
        assertEquals(false, dao.isAllowed(key1, Arrays.asList(account2), Permission.DISCOVER));
        assertEquals(false, dao.isAllowed(key1, Arrays.asList(account1, account2), Permission.DISCOVER));

        dao.create(key1, account1, Permission.DISCOVER);
        dao.create(key1, account1, Permission.ADMIN);
        dao.create(key1, account2, Permission.DISCOVER);

        // single things
        assertEquals(true, dao.isAllowed(key1, Arrays.asList(account1), Permission.DISCOVER));
        assertEquals(true, dao.isAllowed(key1, Arrays.asList(account2), Permission.DISCOVER));
        assertEquals(true, dao.isAllowed(key1, Arrays.asList(account1), Permission.ADMIN));
        assertEquals(false, dao.isAllowed(key1, Arrays.asList(account2), Permission.ADMIN));

        // multiples
        assertEquals(true, dao.isAllowed(key1, Arrays.asList(account1, account2), Permission.DISCOVER));
        assertEquals(true, dao.isAllowed(key1, Arrays.asList(account2, account1), Permission.DISCOVER));
        assertEquals(true, dao.isAllowed(key1, Arrays.asList(account1, account2), Permission.ADMIN));
        assertEquals(true, dao.isAllowed(key1, Arrays.asList(account2, account1), Permission.ADMIN));

        assertEquals(false, dao.isAllowed(key1, Arrays.asList(account2, account1), Permission.CONSUME));

        List<History> listByKeyAction = historyDao.listByKeyAction(key1.getKey(), HistoryAction.PERMISSION_CREATED);
        assertEquals(listByKeyAction.isEmpty(), false);
    }

    @Test
    public void getMultiple() {
        // nothing allowed to start with
        assertEquals(false, dao.isAllowed(key1, new ArrayList<Account>(), Permission.DISCOVER));
        assertEquals(false, dao.isAllowed(key1, Arrays.asList(account1), Permission.DISCOVER));
        assertEquals(false, dao.isAllowed(key1, Arrays.asList(account2), Permission.DISCOVER));
        assertEquals(false, dao.isAllowed(key1, Arrays.asList(account1, account2), Permission.DISCOVER));

        dao.create(key1, account1, Permission.DISCOVER);
        dao.create(key1, account1, Permission.ADMIN);
        dao.create(key1, account2, Permission.DISCOVER);

        List<Permission> perms;

        // single things
        perms = dao.get(key1, Arrays.asList(account1));
        assertEquals(2, perms.size());
        assertTrue(perms.contains(Permission.DISCOVER));
        assertTrue(perms.contains(Permission.ADMIN));

        perms = dao.get(key1, Arrays.asList(account2));
        assertEquals(1, perms.size());
        assertTrue(perms.contains(Permission.DISCOVER));

        // multiples get merged
        perms = dao.get(key1, Arrays.asList(account1, account2));
        assertEquals(2, perms.size());
        assertTrue(perms.contains(Permission.DISCOVER));
        assertTrue(perms.contains(Permission.ADMIN));

        dao.create(key1, account2, Permission.CONSUME);
        perms = dao.get(key1, Arrays.asList(account1, account2));
        assertEquals(3, perms.size());
        assertTrue(perms.contains(Permission.DISCOVER));
        assertTrue(perms.contains(Permission.CONSUME));
        assertTrue(perms.contains(Permission.ADMIN));

    }

    @Test
    public void testRemove() {
        dao.create(key1, account1, Permission.DISCOVER);
        dao.create(key1, account1, Permission.ADMIN);

        List<Permission> perms = dao.get(key1, account1);
        assertEquals(2, perms.size());

        dao.remove(key1, account1, Permission.ADMIN);
        assertEquals(1, dao.get(key1, account1).size());
        assertFalse(dao.exists(key1, account1, Permission.ADMIN));
        assertTrue(dao.exists(key1, account1, Permission.DISCOVER));

        List<History> listByKeyAction = historyDao.listByKeyAction(key1.getKey(), HistoryAction.PERMISSION_REMOVED);
        assertEquals(listByKeyAction.isEmpty(), false);
    }

    @Test
    public void testGetGranted() {
        DNSkey key2 = dnsKeyDao.create();

        // nothing allowed to start with
        List<DNSkey> granted = dao.getGranted(account1, Permission.DISCOVER);
        assertNotNull(granted);
        assertEquals(0, granted.size());

        // grant
        dao.create(key1, account1, Permission.DISCOVER);
        dao.create(key1, account2, Permission.ADMIN);

        // DISCOVER was granted to account1
        granted = dao.getGranted(account1, Permission.DISCOVER);
        assertNotNull(granted);
        assertEquals(1, granted.size());
        assertEquals(key1.getKey(), granted.get(0).getKey());

        // ADMIN was not granted to account1
        granted = dao.getGranted(account1, Permission.ADMIN);
        assertNotNull(granted);
        assertEquals(0, granted.size());

        // grant on another key
        dao.create(key2, account1, Permission.DISCOVER);

        // DISCOVER was granted to account1 for both keys
        granted = dao.getGranted(account1, Permission.DISCOVER);
        assertNotNull(granted);
        assertEquals(2, granted.size());
        assertTrue(granted.contains(key1));
        assertTrue(granted.contains(key2));
    }

    @Test
    public void testRemoveAll() {
        dao.create(key1, account1, Permission.DISCOVER);
        dao.create(key1, account1, Permission.ADMIN);
        dao.create(key1, account2, Permission.MODIFY);

        assertEquals(2, dao.get(key1, account1).size());
        assertEquals(1, dao.get(key1, account2).size());

        dao.removeAll(key1);

        assertEquals(0, dao.get(key1, account1).size());
        assertEquals(0, dao.get(key1, account2).size());
    }

    @Test
    public void testUnmanagedGetByKey() {
        dao.create(key1, account1, Permission.DISCOVER);
        dao.create(key1, account1, Permission.ADMIN);
        dao.create(key1, account1, Permission.MODIFY);
        dao.create(key1, account2, Permission.MODIFY);
        dao.flush();

        Map<AccountId, Set<Permission>> found = dao.unmanagedGetByKey(key1);

        assertEquals(2, found.size());
        assertTrue(found.containsKey(new AccountId(account1.getId())));
        assertTrue(found.containsKey(new AccountId(account2.getId())));

        Set<Permission> account1_perms = found.get(new AccountId(account1.getId()));
        assertNotNull(account1_perms);
        Set<Permission> account2_perms = found.get(new AccountId(account2.getId()));
        assertNotNull(account2_perms);
        assertEquals(3, account1_perms.size());
        assertEquals(1, account2_perms.size());
    }

}
