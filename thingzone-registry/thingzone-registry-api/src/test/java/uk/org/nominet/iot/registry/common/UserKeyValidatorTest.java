/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.common;

import org.junit.Test;

import static org.junit.Assert.*;

public class UserKeyValidatorTest {

    @Test
    public void testValid() {
        assertEquals(true, UserKeyValidator.validate("foo"));
        assertEquals(true, UserKeyValidator.validate("foo-bar"));
        assertEquals(true, UserKeyValidator.validate("4-6-2"));
        assertEquals(true, UserKeyValidator.validate("f-oo.bar.baz"));
    }

    @Test
    public void testInvalid() {
        assertEquals(false, UserKeyValidator.validate("foo!"));
        assertEquals(false, UserKeyValidator.validate("-foo"));
        assertEquals(false, UserKeyValidator.validate("foo-"));
        assertEquals(false, UserKeyValidator.validate("fo--o"));
        assertEquals(false, UserKeyValidator.validate("Foo"));
        assertEquals(false, UserKeyValidator.validate("foo.-bar"));
    }
}
