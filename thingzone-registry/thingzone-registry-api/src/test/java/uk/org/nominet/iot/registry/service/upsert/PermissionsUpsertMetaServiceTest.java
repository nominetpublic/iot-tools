/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.upsert;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Map;
import java.util.Set;

import org.hamcrest.Matchers;
import org.hamcrest.core.IsEqual;
import org.junit.Test;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.model.Permission;

@SuppressWarnings("unchecked")
public class PermissionsUpsertMetaServiceTest {

    @Test
    public void decodePermissionsWithData() {
        Map<String, Set<Permission>> decoded = PermissionsUpsertMetaService.decodeFromJson(
            "{ \"public\" : [\"DISCOVER\"], \"user1\" : [\"ADMIN\",\"UPLOAD\"] }");
        assertEquals(2, decoded.size());
        assertThat(decoded, hasEntry(is("public"), Matchers.contains(IsEqual.equalTo(Permission.DISCOVER))));
        assertThat(decoded, hasEntry(is("user1"),
            Matchers.containsInAnyOrder(IsEqual.equalTo(Permission.ADMIN), IsEqual.equalTo(Permission.UPLOAD))));
    }

    @Test
    public void decodePermissionsWithEmptyArrays() {
        Map<String, Set<Permission>> decoded
            = PermissionsUpsertMetaService.decodeFromJson("{ \"public\" : [], \"user1\" : [\"ADMIN\",\"UPLOAD\"] }");
        assertEquals(2, decoded.size());
        assertThat(decoded, hasEntry(is("public"), Matchers.emptyCollectionOf(Permission.class)));
        assertThat(decoded, hasEntry(is("user1"),
            Matchers.containsInAnyOrder(IsEqual.equalTo(Permission.ADMIN), IsEqual.equalTo(Permission.UPLOAD))));
    }

    @Test
    public void decodePermissionsWithEmptyData() {
        Map<String, Set<Permission>> decoded = PermissionsUpsertMetaService.decodeFromJson("{ }");
        assertEquals(0, decoded.size());
    }

    @Test
    public void decodePermissionsFailsWithBogusJson() {
        try {
            PermissionsUpsertMetaService.decodeFromJson(" this is not json");
            fail();
        } catch (Exception e) {
            assertThat(e, instanceOf(DataException.class));
            assertThat(e.getMessage(), containsString("bad JSON"));
            assertThat(e.getMessage(), containsString("Unrecognized token"));
        }
    }

    @Test
    public void decodePermissionsFailsWithJsonInWrongForm() {
        try {
            PermissionsUpsertMetaService.decodeFromJson("{ \"foo\": false }");
            fail();
        } catch (Exception e) {
            assertThat(e, instanceOf(DataException.class));
            assertThat(e.getMessage(), containsString("bad JSON"));
            assertThat(e.getMessage(), containsString("Can not deserialize"));
        }
    }

    @Test
    public void decodePermissionsFailsWithJsonWithWrongContent() {
        try {
            PermissionsUpsertMetaService.decodeFromJson("{ \"public\" : [\"NOT_A_VALID_PERMISSION\"] }");
            fail();
        } catch (Exception e) {
            assertThat(e, instanceOf(DataException.class));
            assertThat(e.getMessage(), containsString("bad JSON"));
            assertThat(e.getMessage(), containsString("invalid Permission"));
        }
    }

    @Test
    public void permissionAreSame() {
        assertTrue(PermissionsUpsertMetaService.areSame(decode("{ }"), decode("{ }")));
        assertTrue(PermissionsUpsertMetaService.areSame(
            decode("{ \"public\" : [\"DISCOVER\"], \"user1\" : [\"ADMIN\",\"UPLOAD\"] }"),
            decode("{ \"public\" : [\"DISCOVER\"], \"user1\" : [\"ADMIN\",\"UPLOAD\"] }")));
    }

    @Test
    public void permissionAreNotSame() {
        assertFalse(PermissionsUpsertMetaService.areSame(decode("{ }"), decode("{ \"public\" : [\"DISCOVER\"] }")));
        assertFalse(PermissionsUpsertMetaService.areSame(
            decode("{ \"public\" : [\"DISCOVER\"], \"user1\" : [\"ADMIN\",\"UPLOAD\"] }"),
            decode("{ \"public\" : [\"DISCOVER\"], \"user1\" : [\"ADMIN\"] }")));
    }

    // brevity in tests
    private Map<String, Set<Permission>> decode(String json) {
        return PermissionsUpsertMetaService.decodeFromJson(json);
    }
}
