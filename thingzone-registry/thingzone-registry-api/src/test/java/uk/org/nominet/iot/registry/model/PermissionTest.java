/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import uk.nominet.iot.utils.error.ProgramDefectException;

@RunWith(JUnit4.class)
public class PermissionTest {

    @Test
    public void testToString() {
        Permission p = Permission.ADMIN;
        assertEquals(p.toString(), "ADMIN");
    }

    @Test
    public void testFromString() {
        Permission p = Permission.fromString("ADMIN");
        assertEquals(Permission.ADMIN, p);
        p = Permission.fromString("DISCOVER");
        assertEquals(Permission.DISCOVER, p);
    }

    @Test(expected = ProgramDefectException.class)
    public void testIllegalFromString() {
        Permission.fromString("wibble");
    }

}
