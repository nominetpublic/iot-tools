/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import javax.persistence.NoResultException;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.DNSkeyDao;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.model.DNSkey;

@RunWith(JUnit4.class)
public class DNSkeyDaoImplTestSuite {

    String testKey = "aaaa.bbbb.cccc.dddd.eeee.ffff.gggg.h.i";
    String dummyKey = "dead.beef.dead.beef.dead.beef.dead.beef.h.i";
    DNSkeyDao dao;

    @Before
    public void setUp() throws Exception {
        DaoImplTest.setUp();
        DaoFactory df = DaoFactoryManager.getFactory();

        dao = df.dnsKeyDao();
    }

    @After
    public void tearDown() {
        DaoImplTest.tearDown();
    }

    @Test
    public void create() {
        DNSkey key = dao.create();
        assertNotNull(key);
    }

    @Test
    public void createKnownKey() {
        // create a key
        DNSkey key = dao.createKnownKey(testKey);
        assertNotNull(key);
        assertEquals(testKey, key.getKey());
    }

    @Test
    public void getByName() {
        DNSkey original = dao.create();
        assertNotNull(original);

        DNSkey found = dao.getByKey(original.getKey());
        assertNotNull(found);
        assertEquals(original.getKey(), found.getKey());
    }

    @Test(expected = NoResultException.class)
    public void getByNameNonexistent() {
        dao.getByKey(dummyKey);
        fail();
    }

    @Ignore
    @Test
    public void createBogusKeyFails() {
        // create a key
        DNSkey key = dao.createKnownKey("bogus");
        assertNull(key);
    }

    @Test
    public void createPreExistingKeyFails() {
        // create a key
        DNSkey key = dao.createKnownKey(testKey);
        assertNotNull(key);
        assertEquals(testKey, key.getKey());

        try {
            dao.createKnownKey(testKey);
            fail();
        } catch (DataException e) {
        }
    }

    @Test
    public void deleteByValue() {
        DNSkey original = dao.create();
        assertNotNull(original);
        assertEquals(true, dao.isAlive(original.getKey()));

        // soft delete
        dao.delete(original);

        assertEquals(false, dao.isAlive(original.getKey()));
        assertEquals(true, dao.exists(original.getKey()));
        try {
            dao.getByKey(original.getKey());
            fail();
        } catch (DataException e) {
        }
    }

    @Test
    public void deleteNonexistentByValue() {
        DNSkey key = new DNSkey(dummyKey);
        try {
            dao.delete(key);
            fail();
        } catch (NoResultException e) {
        }
    }
}
