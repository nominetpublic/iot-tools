/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api.support;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import uk.org.nominet.iot.registry.webserver.WebConfig;

public class SessionWrapperTest {

    @Mock
    HttpServletRequest hsr = null;
    @Mock
    WebConfig webConfig = null;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Before
    public void setUp() throws Exception {
        hsr = mock(HttpServletRequest.class);
        webConfig = mock(WebConfig.class);
    }

    @SuppressWarnings("static-access")
    @Test
    public void isPriviligedAddress() {
        when(hsr.getRemoteAddr()).thenReturn("2.2.2.2");

        uk.org.nominet.iot.registry.webserver.TestConfigFactory.initialise(webConfig);
        SessionWrapper sw = new SessionWrapper(hsr);

        // single item
        when(webConfig.getPrivilegedClients()).thenReturn(Arrays.asList("1.2.3.4"));
        assertTrue(sw.isPrivilegedAddress("1.2.3.4"));
        assertFalse(sw.isPrivilegedAddress("1.1.1.1"));
        assertFalse(sw.isPrivilegedAddress());

        // one of several
        when(webConfig.getPrivilegedClients()).thenReturn(Arrays.asList("1.2.3.4", "2.3.4.5", "3.4.5.6"));
        assertTrue(sw.isPrivilegedAddress("1.2.3.4"));
        assertTrue(sw.isPrivilegedAddress("2.3.4.5"));
        assertFalse(sw.isPrivilegedAddress("1.1.1.1"));
        assertFalse(sw.isPrivilegedAddress());

        // special case for 0.0.0.0
        when(webConfig.getPrivilegedClients()).thenReturn(Arrays.asList("1.2.3.4", "2.3.4.5", "0.0.0.0"));
        assertTrue(sw.isPrivilegedAddress("1.2.3.4"));
        assertTrue(sw.isPrivilegedAddress("2.3.4.5"));
        assertTrue(sw.isPrivilegedAddress("1.1.1.1"));
        assertTrue(sw.isPrivilegedAddress());
    }
}
