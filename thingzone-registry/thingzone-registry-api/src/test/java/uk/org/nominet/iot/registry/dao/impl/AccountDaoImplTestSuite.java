/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import static org.junit.Assert.*;

import javax.persistence.PersistenceException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import uk.org.nominet.iot.registry.dao.AccountDao;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.UserDao;

import uk.org.nominet.iot.registry.dao.dto.AccountId;
import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.User;

@SuppressWarnings("unused") // ignore in test classes
@RunWith(JUnit4.class)
public class AccountDaoImplTestSuite {

    UserDao userDao;
    AccountDao dao;

    @Before
    public void setUp() {
        DaoImplTest.setUp();
        DaoFactory df = DaoFactoryManager.getFactory();
        userDao = df.userDao();
        dao = df.accountDao();
    }

    @After
    public void tearDown() {
        DaoImplTest.tearDown();
    }

    @Test
    public void create() {
        User user = userDao.create();
        assertNotNull(user);
        Account account = dao.create(user);
        assertNotNull(account);
    }

    @Test
    public void createWithoutOwner() {
        Account account = dao.create(null);
        assertNotNull(account);
    }

    @Test
    public void getByOwner() {
        User user = userDao.create();
        assertNotNull(user);
        Account account = dao.create(user);
        assertNotNull(account);

        Account found = dao.getByOwner(user);
        assertNotNull(found);
    }

    @Test
    public void getById() {
        User user = userDao.create();
        assertNotNull(user);
        Account account = dao.create(user);
        assertNotNull(account);

        Account found = dao.getById(new AccountId(account.getId()));
        assertNotNull(found);
    }

    @Test(expected = PersistenceException.class)
    public void accountsAreUniquePerUser() {
        User user = userDao.create();
        assertNotNull(user);
        Account account = dao.create(user);
        assertNotNull(account);
        Account duplicate = dao.create(user);
        userDao.flush();
        fail();
    }

}
