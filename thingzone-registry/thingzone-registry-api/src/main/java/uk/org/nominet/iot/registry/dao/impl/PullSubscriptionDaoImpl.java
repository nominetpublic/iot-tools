/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Optional;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.AbstractJpaDao;
import uk.org.nominet.iot.registry.dao.PullSubscriptionDao;
import uk.org.nominet.iot.registry.dao.dto.PullSubscriptionId;
import uk.org.nominet.iot.registry.dao.dto.PullSubscriptionRow;
import uk.org.nominet.iot.registry.model.*;
import uk.nominet.iot.utils.error.ProgramDefectException;

class PullSubscriptionDaoImpl extends AbstractJpaDao<PullSubscription> implements PullSubscriptionDao {
    private final static Logger log = LoggerFactory.getLogger(PullSubscriptionDaoImpl.class);

    PullSubscriptionDaoImpl() {
        super(PullSubscription.class);
    }

    @Override
    public PullSubscription create(DataStream dataStream,
                                   String uri,
                                   String uriParams,
                                   Integer interval,
                                   Integer aligned) {
        if (exists(dataStream)) {
            throw new DataException("PullSubscription exists already for DataStream \"" + dataStream.getKey() + "\"",
                                    Status.CONFLICT);
        }

        PullSubscription ps = new PullSubscription(dataStream, uri, uriParams, interval, aligned);
        persist(ps);
        log.info("created PullSubscription for DataStream={}, URI={}", dataStream.getKey(), uri);
        return ps;
    }

    @Override
    public boolean exists(DataStream dataStream) {
        try {
            getForStream(dataStream);
            return true;
        } catch (PersistenceException e) {
            if (e instanceof NoResultException) {
                return false;
            } else {
                throw e;
            }
        }
    }

    void delete(PullSubscription pullSubscription) {
        deleteEntity(pullSubscription);
    }

    @Override
    public void delete(DataStream dataStream) {
        delete(getForStream(dataStream));
    }

    @Override
    public PullSubscription getForStream(DataStream dataStream) {
        return loadByMatch("dataStream", dataStream);
    }

    @Override
    public List<PullSubscription> getAllowed(List<Account> accounts, Permission permission) {
        return getAllowed(accounts, Collections.singletonList(permission));
    }

    @Override
    public List<PullSubscription> getAllowed(List<Account> accounts, List<Permission> perms) {
        checkTransaction();

        TypedQuery<PullSubscription> query = getEntityManager().createQuery(
            "SELECT DISTINCT ps FROM PullSubscription ps, ACL ac"
                + " WHERE ac.account in :accounts  AND ps.dataStream.key = ac.key" + " AND ac.permission in :perms",
            getEntityClass());
        query.setParameter("accounts", accounts);
        query.setParameter("perms", perms);

        return query.getResultList();
    }

    @Override
    public Optional<PullSubscriptionRow> unmanagedGetForStream(DataStream dataStream) {
        List<PullSubscriptionRow> pss = new ArrayList<>();
        try {
            String query = "SELECT id, streamKey, uri, uri_params, interval, aligned "
                + "FROM thingzone.PullSubscriptions WHERE streamKey = ?";
            PreparedStatement st = getConnection().prepareStatement(query);
            st.setString(1, dataStream.getKey().getKey());
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                pss.add(
                    new PullSubscriptionRow(new PullSubscriptionId(rs.getLong("id")),
                                            new DNSkey(rs.getString("streamKey")), rs.getString("uri"),
                                            rs.getString("uri_params"), rs.getInt("interval"), rs.getInt("aligned")));
            }
            st.close();
        } catch (SQLException e) {
            throw new ProgramDefectException("JDBC error", e);
        }
        if (pss.size() > 0) {
            return Optional.of(pss.get(0));
        } else {
            return Optional.empty();
        }
    }

}
