/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.impl.DatabaseDaoFactory;

import com.google.common.base.Strings;

public class DatabaseApp {
    private final static Logger log = LoggerFactory.getLogger(DatabaseApp.class);

    public static void initialiseDatabase() {
        DaoFactoryManager.setFactory(new DatabaseDaoFactory());

        String dbPropertiesFile = System.getProperty("db.properties");
        if (Strings.isNullOrEmpty(dbPropertiesFile)) {
            log.info("no db.properties specified; using default");
            dbPropertiesFile = "config/db.properties";
        }

        // database config
        uk.org.nominet.iot.registry.database.ConfigFactory.loadConfig(dbPropertiesFile);
        DatabaseDriver.setConnectionProperties(
            uk.org.nominet.iot.registry.database.ConfigFactory.getConfig().getJdbcProperties());

        // start DB driver (which'll use DB config)
        DatabaseDriver.globalInitialise();
    }

}
