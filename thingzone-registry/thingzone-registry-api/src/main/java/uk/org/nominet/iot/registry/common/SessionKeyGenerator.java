/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.common;

import java.security.SecureRandom;

import com.google.common.io.BaseEncoding;

public class SessionKeyGenerator {

    public final static int BITS_PER_BYTE = 8;
    public final static int BITS_PER_BASE64 = 6;

    public final static int SESSION_KEY_BITS = 132;

    /**
     * Generates a random session key
     *
     * @return Hex encoded String containing SESSION_KEY_BITS bits of entropy
     */
    public static String generateKey() {
        // get entropy
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[SESSION_KEY_BITS / BITS_PER_BYTE]; // round up not down
        random.nextBytes(bytes);

        // convert to base64 (RFC 4648 URL-safe version)
        String result = BaseEncoding.base64Url().encode(bytes);

        return result.substring(0, SESSION_KEY_BITS / BITS_PER_BASE64);
    }

}
