/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.ddns.validator;

import org.xbill.DNS.AAAARecord;
import org.xbill.DNS.Type;

import uk.nominet.iot.utils.dns.IpAddressValidator;

import uk.org.nominet.iot.registry.common.DataException;

public class AAAARecordValidator extends AbstractRecordValidator {
    @Override
    public void validate(String rdata) {

        // manually check IP address
        if (!IpAddressValidator.isValidIPv6(rdata)) {
            throw new DataException("invalid AAAA record");
        }

        super.validate(rdata);
    }

    int expectedType() {
        return Type.AAAA;
    }

    Class<?> expectedClass() {
        return AAAARecord.class;
    }

    String expectedName() {
        return "AAAA";
    }
}
