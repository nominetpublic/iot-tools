/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.AbstractJpaDao;
import uk.org.nominet.iot.registry.dao.StreamOnDeviceDao;
import uk.org.nominet.iot.registry.dao.dto.StreamOnDeviceId;
import uk.org.nominet.iot.registry.dao.dto.StreamOnDeviceRow;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.Device;
import uk.org.nominet.iot.registry.model.StreamOnDevice;
import uk.nominet.iot.utils.error.ProgramDefectException;

class StreamOnDeviceDaoImpl extends AbstractJpaDao<StreamOnDevice> implements StreamOnDeviceDao {
    private final static Logger log = LoggerFactory.getLogger(StreamOnDeviceDaoImpl.class);

    StreamOnDeviceDaoImpl() {
        super(StreamOnDevice.class);
    }

    @Override
    public StreamOnDevice create(Device device, DataStream dataStream, String name) {

        if (exists(device, dataStream)) {
            throw new DataException("DataStream already exists for Device \"" + device.getKey() + "\" & DataStream \""
                + dataStream.getKey() + "\"", Status.CONFLICT);
        }
        StreamOnDevice sod = new StreamOnDevice();
        sod.setDataStream(dataStream);
        sod.setDevice(device);
        sod.setName(name);

        persist(sod);
        flush();
        return sod;
    }

    @Override
    public List<StreamOnDevice> findByDevice(Device device) {
        return findByFilter("device", device);
    }

    @Override
    public List<StreamOnDevice> findByDataStream(DataStream dataStream) {
        return findByFilter("dataStream", dataStream);
    }

    @Override
    public void delete(StreamOnDevice sod) {
        deleteEntity(sod);
    }

    @Override
    public boolean exists(Device device, DataStream dataStream) {
        try {
            find(device, dataStream);
            return true;
        } catch (PersistenceException e) {
            if (e instanceof NoResultException) {
                return false;
            } else {
                throw e;
            }
        }
    }

    private StreamOnDevice find(Device device, DataStream dataStream) {
        return loadByMatch("device", device, "dataStream", dataStream);

    }

    @Override
    public void addDataStream(Device device, DataStream dataStream, String name) {
        try {
            create(device, dataStream, name);
        } catch (DataException exception) {
            if (exception.getStatus() == Status.CONFLICT) {
                // for idempotency, allow an "add" when already in existence; just update name
                log.info("updating name=\"{}\" for dataStream {} on device {}", name, dataStream, device);
                StreamOnDevice sod = find(device, dataStream);
                sod.setName(name);
            } else {
                throw exception;
            }
        }
    }

    @Override
    public void removeDataStream(Device device, DataStream dataStream) {
        StreamOnDevice sod = find(device, dataStream);
        delete(sod);
    }

    @Override
    public List<DataStream> getDataStreams(Device device) {
        List<StreamOnDevice> sods = findByFilter("device", device);
        List<DataStream> dataStreams = new ArrayList<>();
        for (StreamOnDevice sod : sods) {
            dataStreams.add(sod.getDataStream());
        }
        return ImmutableList.copyOf(dataStreams);
    }

    @Override
    public List<Device> getDevices(DataStream dataStream) {
        List<StreamOnDevice> sods = findByFilter("dataStream", dataStream);
        List<Device> devices = new ArrayList<>();
        for (StreamOnDevice sod : sods) {
            devices.add(sod.getDevice());
        }
        return ImmutableList.copyOf(devices);
    }

    @Override
    public List<StreamOnDeviceRow> unmanagedGetByDataStream(DNSkey key) {
        return unmanagedGet(key, "dataStreamKey");
    }

    @Override
    public List<StreamOnDeviceRow> unmanagedGetByDevice(DNSkey key) {
        return unmanagedGet(key, "deviceKey");
    }

    private List<StreamOnDeviceRow> unmanagedGet(DNSkey key, String itemColumnName) {
        List<StreamOnDeviceRow> sods = new ArrayList<>();

        try {
            String query = "SELECT id, dataStreamKey, deviceKey, name FROM thingzone.StreamsOnDevices WHERE "
                + itemColumnName + "= ?";
            PreparedStatement st = getConnection().prepareStatement(query);
            st.setString(1, key.getKey());
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                sods.add(new StreamOnDeviceRow(new StreamOnDeviceId(rs.getLong("id")),
                                               new DNSkey(rs.getString("dataStreamKey")),
                                               new DNSkey(rs.getString("deviceKey")), rs.getString("name")));
            }
            st.close();
        } catch (SQLException e) {
            throw new ProgramDefectException("JDBC error", e);
        }
        return sods;
    }

}
