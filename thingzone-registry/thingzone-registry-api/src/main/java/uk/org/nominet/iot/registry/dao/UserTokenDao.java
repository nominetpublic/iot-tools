/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao;

import uk.org.nominet.iot.registry.model.UserCredentials;
import uk.org.nominet.iot.registry.model.UserToken;
import uk.org.nominet.iot.registry.model.UserTokenType;

public interface UserTokenDao {

    UserToken create(UserCredentials userCredentials, UserTokenType type, long secondsExpiry);

    UserToken get(String token, UserTokenType type);

    boolean exists(String token);

    boolean valid(UserToken userToken);
}
