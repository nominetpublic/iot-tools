/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.dto;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import uk.org.nominet.iot.registry.dao.dto.PushSubscriberRow;
import uk.org.nominet.iot.registry.model.DNSdata;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.Permission;

/**
 * As well as the main "/dump" API, we also return similar objects from upsert (and we may well do in other places)
 * DumpObjects provides standard objects to make this easier to keep consistent
 */
public class DumpObjects {

    @JsonPropertyOrder(value = { "device" })
    public static class DeviceWithPermissions {
        @JsonInclude(JsonInclude.Include.ALWAYS)
        public DeviceSummaryDetailDto device;

        public DeviceWithPermissions(DeviceSummaryDetailDto deviceSummaryDto) {
            this.device = deviceSummaryDto;
        }
    }

    @JsonPropertyOrder(value = { "dataStream" })
    public static class DataStreamWithPermissionsAndDeviceKeys {
        @JsonInclude(JsonInclude.Include.ALWAYS)
        public DataStreamSummaryDto dataStream;

        public DataStreamWithPermissionsAndDeviceKeys(DataStreamSummaryDto dataStreamSummaryDetailDto) {
            this.dataStream = dataStreamSummaryDetailDto;
        }
    }

    @JsonPropertyOrder(value = { "transform" })
    public static class TransformWithPermissions {
        @JsonInclude(JsonInclude.Include.ALWAYS)
        public TransformSummaryDetailDto transform;

        public TransformWithPermissions(TransformSummaryDetailDto transformSummaryDetailDto) {
            this.transform = transformSummaryDetailDto;
        }
    }

    public static class HistoryDto {
        public String key;

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public String entityType;

        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        public Set<String> removedUserPermissions;

        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        public Set<String> removedGroupPermissions;

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public DeviceSummaryDetailDto device;

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public DataStreamSummaryDto dataStream;

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public TransformSummaryDetailDto transform;

        public HistoryDto(String key) {
            this.key = key;
        }

        public void setDevice(DeviceSummaryDetailDto device) {
            this.device = device;
        }

        public void setDataStream(DataStreamSummaryDto dataStream) {
            this.dataStream = dataStream;
        }

        public void setEntityType(String type) {
            this.entityType = type;
        }

        public void setTransform(TransformSummaryDetailDto transform) {
            this.transform = transform;
        }
    }

    @JsonPropertyOrder(value = { "key", "linkName" })
    public static class DeviceStreamLink {
        @JsonInclude(JsonInclude.Include.NON_NULL)
        public String key;
        @JsonInclude(JsonInclude.Include.NON_NULL)
        public String linkName;

        public DeviceStreamLink() {}

        public DeviceStreamLink(DNSkey key, String name) {
            this.key = key.getKey();
            this.linkName = name;
        }

        @Override
        public String toString() {
            return "DeviceStreamLink{" + "key='" + key + '\'' + ", linkName='" + linkName + '\'' + '}';
        }

        // (equals/hashcode used to help work out changes needed for upsert)
        @Override
        public boolean equals(Object o) {
            if (this == o)
                return true;
            if (o == null || getClass() != o.getClass())
                return false;
            DeviceStreamLink that = (DeviceStreamLink) o;
            return Objects.equals(key, that.key) && Objects.equals(linkName, that.linkName);
        }

        @Override
        public int hashCode() {
            return Objects.hash(key, linkName);
        }
    }

    public static class GenericStreamDetails {
        public List<DNSdata> dnsDatas;
        public Map<String, Set<Permission>> permissionsAsUsers;
        public Map<String, Set<Permission>> permissionsAsGroups;
        public List<DumpObjects.DeviceStreamLink> deviceLinks;
        public List<PushSubscriberRow> pushSubscribers;

        public GenericStreamDetails(List<DNSdata> dnsDatas, Map<String, Set<Permission>> permissionsAsUsers,
                                    Map<String, Set<Permission>> permissionsAsGroups,
                                    List<DumpObjects.DeviceStreamLink> deviceLinks,
                                    List<PushSubscriberRow> pushSubscribers) {
            this.dnsDatas = dnsDatas;
            this.permissionsAsUsers = permissionsAsUsers;
            this.permissionsAsGroups = permissionsAsGroups;
            this.deviceLinks = deviceLinks;
            this.pushSubscribers = pushSubscribers;
        }
    }
}
