/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import org.eclipse.persistence.annotations.Customizer;

import uk.org.nominet.iot.registry.database.ColumnPosition;
import uk.org.nominet.iot.registry.database.ColumnPositionCustomizer;
import uk.org.nominet.iot.registry.database.DatabaseDriver;
import uk.org.nominet.iot.registry.database.HistoryListener;

import javax.persistence.*;

@Entity
@EntityListeners(HistoryListener.class)
@Table(name = "StreamsOnDevices", schema = DatabaseDriver.SCHEMA_NAME,
       uniqueConstraints = { @UniqueConstraint(name = "UQ_SOD", columnNames = { "deviceKey", "dataStreamKey" }), })
@Customizer(ColumnPositionCustomizer.class)
public class StreamOnDevice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "dataStreamKey", referencedColumnName = "DNSkey", nullable = false)
    @ColumnPosition(position = 1)
    private DataStream dataStream;

    @ManyToOne
    @JoinColumn(name = "deviceKey", referencedColumnName = "DNSkey", nullable = false)
    @ColumnPosition(position = 2)
    private Device device;

    @Column(name = "name", nullable = false) // must at least be empty string
    @ColumnPosition(position = 3)
    private String name;

    @Override
    public String toString() {
        return "StreamOnDevice{" + "id=" + id + " device=" + device.getKey().getKey() + ", dataStream="
            + dataStream.getKey().getKey() + ", name='" + name + '\'' + '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public DataStream getDataStream() {
        return dataStream;
    }

    public void setDataStream(DataStream dataStream) {
        this.dataStream = dataStream;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
