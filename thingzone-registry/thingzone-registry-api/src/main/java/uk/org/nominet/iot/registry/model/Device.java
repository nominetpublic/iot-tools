/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.eclipse.persistence.annotations.Customizer;

import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import uk.org.nominet.iot.registry.database.ColumnPosition;
import uk.org.nominet.iot.registry.database.ColumnPositionCustomizer;
import uk.org.nominet.iot.registry.database.DatabaseDriver;
import uk.org.nominet.iot.registry.database.HistoryListener;

@Entity
@EntityListeners(HistoryListener.class)
@Table(name = "Devices", schema = DatabaseDriver.SCHEMA_NAME,
       uniqueConstraints = { @UniqueConstraint(name = "Device_UQ_KEY", columnNames = { "DNSkey" }), })
@Customizer(ColumnPositionCustomizer.class)
public class Device {
    @Id // primary key (& hence not null)
    @ColumnPosition(position = 1)
    @Column(name = "key")
    private String id;

    @MapsId
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DNSkey", referencedColumnName = "DNSkey", nullable = false)
    private DNSkey key;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "metadata", columnDefinition = "JSONB", nullable = false) // empty="{}"
    @Convert(converter = JsonbStringConverter.class) // our custom (to/from String) converter
    @ColumnPosition(position = 2)
    private String metadata;

    @Column(name = "location", columnDefinition = "JSONB")
    @Convert(converter = JsonbStringConverter.class) // our custom (to/from String) converter
    @ColumnPosition(position = 3)
    private String location;

    @OneToMany(mappedBy = "device")
    Set<StreamOnDevice> dataStreamLinks;

    @Override
    public String toString() {
        return "Device{" + "key=" + key.toString() + '}';
    }

    @JsonRawValue
    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    @JsonUnwrapped
    public DNSkey getKey() {
        return key;
    }

    public void setKey(DNSkey key) {
        this.key = key;
    }

    @JsonRawValue
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
