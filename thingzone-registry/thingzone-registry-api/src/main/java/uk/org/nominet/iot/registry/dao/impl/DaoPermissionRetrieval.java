/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.Device;
import uk.org.nominet.iot.registry.model.Permission;
import uk.nominet.iot.utils.error.ProgramDefectException;

class DaoPermissionRetrieval<T> {
    // work around type erasure, & let generic class know its name at run-time
    private Class<T> entityClass;

    private Class<T> getEntityClass() {
        return entityClass;
    }

    private String getEntityClassName() {
        return entityClass.getSimpleName();
    }

    // entityClass is checked against T at compile-time; keep a copy for run-time
    // use
    private DaoPermissionRetrieval(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    // syntactic sugar
    static DaoPermissionRetrieval<DataStream> forDataStream() {
        return new DaoPermissionRetrieval<>(DataStream.class);
    }

    static DaoPermissionRetrieval<Device> forDevice() {
        return new DaoPermissionRetrieval<>(Device.class);
    }

    public List<T> getVisibleTo(EntityManager em, List<Account> accounts) {
        return getAllowed(em, accounts, Collections.singletonList(Permission.DISCOVER));
    }

    public List<T> getAllowed(EntityManager em, List<Account> accounts, List<Permission> perms) {
        checkTransaction(em);

        TypedQuery<T> query = em.createQuery(
            "SELECT DISTINCT entity" + " FROM " + getEntityClassName() + " entity, ACL ac"
                + " WHERE ac.account in :accounts" + " AND entity.key = ac.key" + " AND ac.permission in :perms",
            getEntityClass());
        query.setParameter("accounts", accounts);
        query.setParameter("perms", perms);

        return query.getResultList();
    }

    public void checkTransaction(EntityManager em) {
        if (!em.isJoinedToTransaction())
            throw new ProgramDefectException("no DB transaction");
        if (!em.getTransaction().isActive())
            throw new ProgramDefectException("no active DB transaction");
    }

}
