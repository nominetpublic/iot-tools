/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.google.common.collect.ImmutableList;

import uk.org.nominet.iot.registry.dao.dto.TransformSourceRow;
import uk.org.nominet.iot.registry.model.TransformSource;

public class TransformSourceDto {
    public String source;

    @JsonInclude(Include.NON_EMPTY)
    public String type;

    public String alias;

    public Boolean trigger;

    @JsonInclude(Include.NON_EMPTY)
    public String aggregationType;

    @JsonInclude(Include.NON_EMPTY)
    public String aggregationSpec;

    public TransformSourceDto() {
    }

    private TransformSourceDto(TransformSource ts) {
        this.source = ts.getSourceStream().getKey().getKey();
        this.type = ts.getSourceStream().getKey().getType();
        this.alias = ts.getAlias();
        this.trigger = ts.getTrigger();
        this.aggregationType = ts.getAggregationType();
        this.aggregationSpec = ts.getAggregationSpec();
    }

    public TransformSourceDto(TransformSourceRow tsr) {
        this.source = tsr.getSourceStreamKey().getKey();
        this.type = tsr.getSourceStreamKey().getType();
        this.alias = tsr.getAlias();
        this.trigger = tsr.getTrigger();
        this.aggregationType = tsr.getAggregationType();
        this.aggregationSpec = tsr.getAggregationSpec();
    }

    static public List<TransformSourceDto> toDto(List<TransformSource> transformSources) {
        List<TransformSourceDto> tsd = new ArrayList<>();
        for (TransformSource ts : transformSources) {
            tsd.add(new TransformSourceDto(ts));
        }
        return ImmutableList.copyOf(tsd);
    }

    public static List<TransformSourceDto> asListFromRow(List<TransformSourceRow> tsrs) {
        if (tsrs == null)
            return null;
        return tsrs.stream().map(TransformSourceDto::new).collect(Collectors.toList());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((aggregationSpec == null) ? 0 : aggregationSpec.hashCode());
        result = prime * result + ((aggregationType == null) ? 0 : aggregationType.hashCode());
        result = prime * result + ((alias == null) ? 0 : alias.hashCode());
        result = prime * result + ((source == null) ? 0 : source.hashCode());
        result = prime * result + ((trigger == null) ? 0 : trigger.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TransformSourceDto other = (TransformSourceDto) obj;
        if (aggregationSpec == null) {
            if (other.aggregationSpec != null)
                return false;
        } else if (!aggregationSpec.equals(other.aggregationSpec))
            return false;
        if (aggregationType == null) {
            if (other.aggregationType != null)
                return false;
        } else if (!aggregationType.equals(other.aggregationType))
            return false;
        if (alias == null) {
            if (other.alias != null)
                return false;
        } else if (!alias.equals(other.alias))
            return false;
        if (source == null) {
            if (other.source != null)
                return false;
        } else if (!source.equals(other.source))
            return false;
        if (trigger == null) {
            if (other.trigger != null)
                return false;
        } else if (!trigger.equals(other.trigger))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }

}
