/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.eclipse.persistence.annotations.Customizer;

import uk.org.nominet.iot.registry.database.ColumnPositionCustomizer;
import uk.org.nominet.iot.registry.database.DatabaseDriver;

/**
 * Transform output streams
 */
@Entity
@Table(name = "transform_outputstreams", schema = DatabaseDriver.SCHEMA_NAME,
       uniqueConstraints = { @UniqueConstraint(name = "uq_transform_outputstream_datastream",
                                               columnNames = { "transform_id, datastream_id" }), })
@Customizer(ColumnPositionCustomizer.class)
@IdClass(TransformOutputStreamKey.class)
public class TransformOutputStream {
    @Id
    @Column(name = "transform_id")
    private Long transformId;

    @Id
    @Column(name = "alias")
    private String alias;

    @Column(name = "datastream_id", length = DatabaseDriver.DNSKEY_LENGTH, nullable = false)
    private String datastreamId;

    @ManyToOne
    @PrimaryKeyJoinColumn(name = "transform_id", referencedColumnName = "id")
    private Transform transform;

    @ManyToOne
    @JoinColumn(name = "datastream_id", referencedColumnName = "dnskey", nullable = false, insertable = false,
                updatable = false)
    private DataStream dataStream;

    public TransformOutputStream() {
    }

    public TransformOutputStream(String alias, Transform transform, DataStream datastream) {
        this.transformId = transform.getId();
        this.alias = alias;
        this.datastreamId = datastream.getKey().getKey();
        this.transform = transform;
        this.dataStream = datastream;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Transform getTransform() {
        return transform;
    }

    public void setTransform(Transform transform) {
        this.transform = transform;
        this.transformId = transform.getId();
    }

    public DataStream getDatastream() {
        return dataStream;
    }

    public void setDatastream(DataStream datastream) {
        this.dataStream = datastream;
        this.datastreamId = datastream.getKey().getKey();
    }

}
