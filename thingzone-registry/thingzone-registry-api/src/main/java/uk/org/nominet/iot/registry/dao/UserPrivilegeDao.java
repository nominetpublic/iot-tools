/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao;

import uk.org.nominet.iot.registry.model.Privilege;
import uk.org.nominet.iot.registry.model.User;
import uk.org.nominet.iot.registry.model.UserPrivilege;

import java.util.List;

public interface UserPrivilegeDao extends JpaDao<UserPrivilege> {

    UserPrivilege create(User user, Privilege privilege);

    void remove(User user, Privilege privilege);

    List<Privilege> get(User user);

    boolean exists(User user, Privilege privilege);
}
