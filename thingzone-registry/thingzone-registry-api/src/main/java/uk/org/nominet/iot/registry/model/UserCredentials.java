/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.eclipse.persistence.annotations.Customizer;
import org.mindrot.jbcrypt.BCrypt;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Strings;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.database.ColumnPosition;
import uk.org.nominet.iot.registry.database.ColumnPositionCustomizer;
import uk.org.nominet.iot.registry.database.DatabaseDriver;
import uk.nominet.iot.utils.error.ProgramDefectException;

@Entity
@Table(name = "user_credentials", schema = DatabaseDriver.SCHEMA_NAME,
       uniqueConstraints = { @UniqueConstraint(name = "UQ_USER_CREDENTIALS_USERNAME", columnNames = { "username" }) })
@Customizer(ColumnPositionCustomizer.class)
public class UserCredentials {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ColumnPosition(position = 1)
    @JsonIgnore
    private Long id;

    @Column(name = "username", nullable = false)
    @ColumnPosition(position = 2)
    private String username;

    @Column(name = "pw_hash", nullable = false)
    @ColumnPosition(position = 3)
    private String pwHash;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    @ColumnPosition(position = 4)
    private User user;

    @Column(name = "email", nullable = false,
            columnDefinition = "character varying(255) DEFAULT ''::character varying NOT NULL")
    @ColumnPosition(position = 5)
    private String email;

    @Column(name = "email_validated", nullable = false, columnDefinition = "boolean NOT NULL DEFAULT false")
    @ColumnPosition(position = 6)
    private Boolean emailValidated;

    private UserCredentials() {
    }

    public UserCredentials(String username, String password, User user, String email) {
        this.username = username;
        this.pwHash = hashPassword(password);
        this.user = user;
        this.email = email;
        this.emailValidated = Boolean.FALSE;
    }

    // constructor to allow creation of "public" user
    static public UserCredentials createNoLogon(String username, User user) {
        UserCredentials uc = new UserCredentials();
        uc.username = username;
        uc.pwHash = ""; // no password, so can't log on as this user
        uc.user = user;
        uc.email = "";
        uc.emailValidated = Boolean.FALSE;
        return uc;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    // no need to expose password hash
    @SuppressWarnings("unused")
    private String getPwHash() {
        throw new ProgramDefectException("cannot see password hash");
    }

    public boolean verifyPassword(String password) {
        return !Strings.isNullOrEmpty(password) && !Strings.isNullOrEmpty(pwHash) && checkPwHash(password, pwHash);
    }

    public boolean updatePassword(String oldPassword, String newPassword) {
        // check new password
        if (!isValidPassword(newPassword)) {
            throw new DataException("invalid password");
        }

        if (checkPwHash(oldPassword, pwHash)) {
            pwHash = hashPassword(newPassword);
            return true;
        } else {
            return false;
        }
    }

    public boolean setPassword(String newPassword) {
        // set the user password
        if (!isValidPassword(newPassword)) {
            throw new DataException("invalid password");
        }
        pwHash = hashPassword(newPassword);
        return true;
    }

    // bootstrapping
    public boolean isPasswordSet() {
        return !Strings.isNullOrEmpty(pwHash);
    }

    public void initialisePassword(String newPassword) {
        if (!isPasswordSet()) {
            pwHash = hashPassword(newPassword);
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User getUser() {
        return user;
    }

    @Override
    public String toString() {
        // don't log the password
        return "UserCredentials{" + "id=" + id + ", username='" + username + ", password=********" + ", user_id='"
            + user.getId() + '}';
    }

    // ====================================================
    // Utility, internal

    // work factor is 2**log_rounds
    private final static int DEFAULT_LOG_ROUNDS = 10;
    private final static int MIN_PASSWORD_LENGTH = 6;

    private static String hashPassword(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt(DEFAULT_LOG_ROUNDS));
    }

    private static boolean checkPwHash(String password, String pwHash) {
        return BCrypt.checkpw(password, pwHash);
    }

    private static boolean isValidPassword(String password) {
        return !Strings.isNullOrEmpty(password) && password.length() >= MIN_PASSWORD_LENGTH;
    }

}
