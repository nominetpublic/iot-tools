/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.common;

import java.util.Calendar;
import java.util.Date;

public class Util {

    /**
     * Strips any trailing period from a string
     */
    public static String stripTrailingPeriod(String s) {
        s = s.trim();
        return s.endsWith(".") ? s.substring(0, s.length() - 1) : s;
    }

    /**
     * Ensures a trailing period on a string
     *
     * @param s FQDN with/without trailing period
     * @return FQDN with 1 trailing period
     */
    public static String ensureTrailingPeriod(String s) {
        s = s.trim();
        return s.endsWith(".") ? s : s + ".";
    }

    /**
     * Sleep for specified number of <b>seconds</b>
     *
     * @param seconds Number of seconds to sleep for
     */
    public static void sleep(long seconds) {
        milliSleep(seconds * 1000);
    }

    /**
     * Sleep for specified number of <b>milliseconds</b>
     *
     * @param ms Number of milliseconds to sleep for
     */
    public static void milliSleep(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            // sadly we don't find out how much time was left (cf nanosleep)
            // so we just return, albeit earlier than intended
        }
    }

    /**
     * Return a time specified number of seconds ago
     *
     * @param seconds Number of seconds
     */
    public static Date secondsAgo(int seconds) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, -seconds);
        return cal.getTime();
    }

    /**
     * Return a time specified number of minutes ago
     *
     * @param minutes Number of minutes
     */
    public static Date minutesAgo(int minutes) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, -minutes);
        return cal.getTime();
    }

}
