/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.dto;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonRawValue;

import uk.org.nominet.iot.registry.model.PushSubscriber;

public class PushSubscriberDetailDto {
    @JsonInclude(Include.NON_NULL)
    public DataStreamSummaryDto dataStream;

    @JsonInclude(Include.NON_NULL)
    public String uri;

    @JsonInclude(Include.NON_NULL)
    @JsonRawValue
    public String uriParams;

    @JsonInclude(Include.NON_NULL)
    @JsonRawValue
    public String headers;

    @JsonInclude(Include.NON_NULL)
    public String method;

    @JsonInclude(Include.NON_NULL)
    public String retrySequence;

    public PushSubscriberDetailDto(DataStreamSummaryDto dataStream,
                                   String uri,
                                   String uriParams,
                                   String headers,
                                   String method, String retrySequence) {
        this.dataStream = dataStream;
        this.uri = uri;
        this.uriParams = uriParams;
        this.headers = headers;
        this.method = method;
        this.retrySequence = retrySequence;
    }

    public PushSubscriberDetailDto(PushSubscriber ps) {
        this.dataStream = DataStreamSummaryDto.basic(ps.getDataStream(), null, null);
        this.uri = ps.getUri();
        this.uriParams = ps.getUriParams();
        this.headers = ps.getHeaders();
        this.method = ps.getMethod();
        this.retrySequence = ps.getRetrySequence();
    }

    public static List<PushSubscriberDetailDto> asList(List<PushSubscriber> subs) {
        if (subs == null)
            return null;
        List<PushSubscriberDetailDto> details
            = subs.stream().map(ps -> new PushSubscriberDetailDto(ps)).collect(Collectors.toList());
        return details;
    }
}
