/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.AbstractJpaDao;
import uk.org.nominet.iot.registry.dao.TransformSourceDao;
import uk.org.nominet.iot.registry.dao.dto.TransformId;
import uk.org.nominet.iot.registry.dao.dto.TransformSourceId;
import uk.org.nominet.iot.registry.dao.dto.TransformSourceRow;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.Transform;
import uk.org.nominet.iot.registry.model.TransformSource;
import uk.nominet.iot.utils.error.ProgramDefectException;

class TransformSourceDaoImpl extends AbstractJpaDao<TransformSource> implements TransformSourceDao {
    private final static Logger log = LoggerFactory.getLogger(TransformSourceDaoImpl.class);

    TransformSourceDaoImpl() {
        super(TransformSource.class);
    }

    @Override
    public TransformSource create(Transform transform, DataStream sourceStream, String alias, Boolean trigger,
                                  String aggregationType, String aggregationSpec) {
        if (checkData(aggregationType, aggregationSpec)) {
            throw new DataException("optional aggregation data requires both aggregationType and aggregationSpec to be set");
        }
        TransformSource tp = new TransformSource(transform,
                                                 sourceStream,
                                                 alias,
                                                 trigger,
                                                 aggregationType,
                                                 aggregationSpec);
        persist(tp);
        log.info(
            "created TransformSource for transform={}, alias={}, sourceStream={}, "
                + "trigger={}, aggregationType={}, aggregationSpec={}",
            transform.getDataStream().getKey(), alias, sourceStream.getKey(), trigger, aggregationType,
            aggregationSpec);
        return tp;
    }

    private boolean checkData(String aggregationType, String aggregationSpec) {
        return (!Strings.isNullOrEmpty(aggregationType) && Strings.isNullOrEmpty(aggregationSpec))
            || (Strings.isNullOrEmpty(aggregationType) && !Strings.isNullOrEmpty(aggregationSpec));
    }

    @Override
    public TransformSource createOrUpdate(Transform transform, DataStream sourceStream, String alias, Boolean trigger,
                                          String aggregationType, String aggregationSpec) {

        if (checkData(aggregationType, aggregationSpec)) {
            throw new DataException("optional aggregation data requires both aggregationType and aggregationSpec to be set");
        }
        if (exists(transform, alias)) {
            TransformSource ts = find(transform, alias);
            ts.setSourceStream(sourceStream);
            ts.setTrigger(trigger);
            ts.setAggregationType(aggregationType);
            ts.setAggregationSpec(aggregationSpec);
            log.info(
                "updated TransformSource for transform={}, alias={} - "
                    + "set sourceStream={}, trigger={}, aggregationType={}, aggregationSpec={}",
                transform.getDataStream().getKey(), alias, sourceStream.getKey(), trigger, aggregationType,
                aggregationSpec);
            return ts;
        }
        return create(transform, sourceStream, alias, trigger, aggregationType, aggregationSpec);
    }

    @Override
    public boolean exists(Transform transform, String alias) {
        try {
            find(transform, alias);
            return true;
        } catch (NoResultException e) {
            return false;
        }
    }

    @Override
    public boolean exists(Transform transform, DataStream datastream) {
        try {
            loadByMatch("transform", transform, "sourceStream", datastream);
            return true;
        } catch (PersistenceException e) {
            if (e instanceof NoResultException) {
                return false;
            } else {
                throw e;
            }
        }
    }

    @Override
    public TransformSource get(Transform transform, String alias) {
        return find(transform, alias);
    }

    private TransformSource find(Transform transform, String alias) {
        checkTransaction();
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<TransformSource> criteriaQuery = criteriaBuilder.createQuery(getEntityClass());
        Root<TransformSource> model = criteriaQuery.from(getEntityClass());
        criteriaQuery.where(criteriaBuilder.and(criteriaBuilder.equal(model.get("transform"), transform),
            criteriaBuilder.equal(model.get("alias"), alias)));
        TypedQuery<TransformSource> typedQuery = getEntityManager().createQuery(criteriaQuery);
        return typedQuery.getSingleResult();
    }

    @Override
    public void delete(Transform transform, String alias) {
        deleteEntity(find(transform, alias));
    }

    @Override
    public List<TransformSource> getByTransform(Transform transform) {
        return ImmutableList.copyOf(findByFilter("transform", transform));
    }

    @Override
    public List<TransformSource> getBySourceStream(DataStream dataStream) {
        return ImmutableList.copyOf(findByFilter("sourceStream", dataStream));
    }

    /**
     * returns objects that aren't JPA-managed (for performance)
     */
    @Override
    public List<TransformSourceRow> unmanagedGetSources(TransformId id) {
        List<TransformSourceRow> sources = new ArrayList<>();

        try {
            String query = "SELECT id, transformId, sourceStream, trigger, alias, aggregationType, aggregationSpec "
                + "FROM thingzone.TransformSources WHERE transformId = ?";
            PreparedStatement st = getConnection().prepareStatement(query);
            st.setLong(1, id.getId());
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                sources.add(new TransformSourceRow(new TransformSourceId(rs.getLong("id")),
                                                   new TransformId(rs.getLong("transformId")),
                                                   new DNSkey(rs.getString("sourceStream")), rs.getBoolean("trigger"),
                                                   rs.getString("alias"), rs.getString("aggregationType"),
                                                   rs.getString("aggregationSpec")));
            }
            st.close();
        } catch (SQLException e) {
            throw new ProgramDefectException("JDBC error", e);
        }
        return sources;
    }
}
