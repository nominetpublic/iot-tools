/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import java.sql.SQLException;

import javax.persistence.Converter;

import org.postgresql.util.PGobject;

import uk.nominet.iot.utils.error.ProgramDefectException;

public enum UserTokenType implements PersistableEnum<PGobject> {
    PASSWORD,
    EMAIL;

    public static boolean isValid(String s) {
        try {
            UserTokenType.valueOf(s);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    public static UserTokenType fromString(String s) {
        try {
            return UserTokenType.valueOf(s);
        } catch (IllegalArgumentException e) {
            throw new ProgramDefectException("invalid token type: \'" + s + "\'");
        }
    }

    @Override
    public PGobject getValue() {
        PGobject object = new PGobject();
        object.setType("user_token_type");
        try {
            object.setValue(this.toString());
        } catch (SQLException e) {
            System.out.println("this will fail!");
            throw new IllegalArgumentException("Error when creating PostgreSQL enum", e);
        }
        return object;
    }

    @Converter
    public static class UserTokenTypeConverter extends AbstractEnumConverter<UserTokenType, PGobject> {
        public UserTokenTypeConverter() {
            super(UserTokenType.class);
        }
    }
}
