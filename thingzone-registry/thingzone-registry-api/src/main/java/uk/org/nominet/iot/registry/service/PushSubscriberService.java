/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service;

import static uk.org.nominet.iot.registry.model.Privilege.QUERY_STREAMS;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.org.nominet.iot.registry.common.AuthException;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.PushSubscriberDao;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.model.PushSubscriber;
import uk.org.nominet.iot.registry.service.dto.PushSubscriberSummaryDto;
import uk.org.nominet.iot.registry.webserver.api.support.EntityLoader;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;

public class PushSubscriberService {
    private final static Logger log = LoggerFactory.getLogger(PushSubscriberService.class);

    private UserPrivilegeService userPrivilegeService;
    private PushSubscriberDao dao;

    public PushSubscriberService() {
        userPrivilegeService = new UserPrivilegeService();
        dao = DaoFactoryManager.getFactory().pushSubscriberDao();
    }

    public List<PushSubscriber> getVisible(SessionPermissionAssistant spa) {
        // only view if MODIFY allowed on DataStream
        return dao.getAllowed(spa.getRelevantAccounts(), Permission.MODIFY);
    }

    public List<PushSubscriber> getAll(SessionPermissionAssistant spa) {
        if (!userPrivilegeService.canQueryStreams(spa)) {
            throw new AuthException(spa, "need privilege " + QUERY_STREAMS);
        }
        return dao.getAll();
    }

    public void set(SessionPermissionAssistant spa, PushSubscriberSummaryDto ps) {
        set(spa, ps.dataStream, ps.uri, ps.uriParams, ps.headers, ps.method, ps.retrySequence);
    }

    // create or update
    public void set(SessionPermissionAssistant spa, String dataStreamKey, String uri, String uriParams, String headers,
                    String method, String retrySequence) {
        // needs MODIFY permission on DataStream
        DataStream dataStream = EntityLoader.getDataStream(spa, dataStreamKey, Permission.MODIFY);

        if (dao.exists(dataStream, uri)) {
            PushSubscriber ps = dao.get(dataStream, uri);
            ps.setUriParams(uriParams);
            ps.setHeaders(headers);
            ps.setMethod(method);
            ps.setRetrySequence(retrySequence);
        } else {
            dao.create(dataStream, uri, uriParams, headers, method, retrySequence);
        }
    }

    public PushSubscriber get(SessionPermissionAssistant spa, String dataStreamKey, String uri) {
        // needs MODIFY permission on DataStream
        DataStream dataStream = EntityLoader.getDataStream(spa, dataStreamKey, Permission.MODIFY);

        if (dao.exists(dataStream, uri)) {
            return dao.get(dataStream, uri);
        } else {
            return null;
        }
    }

    public List<PushSubscriber> getForStream(SessionPermissionAssistant spa, String dataStreamKey) {
        // needs MODIFY permission on DataStream
        DataStream dataStream = EntityLoader.getDataStream(spa, dataStreamKey, Permission.MODIFY);

        return dao.getForStream(dataStream);
    }

    public void delete(SessionPermissionAssistant spa, String dataStreamKey, String uri) {
        // needs MODIFY permission
        DataStream dataStream = EntityLoader.getDataStream(spa, dataStreamKey, Permission.MODIFY);

        delete(dataStream, uri);
    }

    public void delete(DataStream dataStream, String uri) {
        if (dao.exists(dataStream, uri)) {
            dao.delete(dataStream, uri);
            log.info("removed pushSubscriber \"{}\" from {}", uri, dataStream.getKey().getKey());
        } else {
            log.warn("no pushSubscriber \"{}\" to remove from {}", uri, dataStream.getKey().getKey());
        }
    }

}
