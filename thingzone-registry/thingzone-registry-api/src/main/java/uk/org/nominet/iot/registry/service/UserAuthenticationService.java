/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service;

import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.UserCredentialsDao;
import uk.org.nominet.iot.registry.database.DatabaseDriver;
import uk.org.nominet.iot.registry.model.User;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;

public class UserAuthenticationService {
    private UserCredentialsDao userCredentialsDao;

    public UserAuthenticationService() {
        userCredentialsDao = DaoFactoryManager.getFactory().userCredentialsDao();
    }

    /**
     * some users are allowed to login & set initial password, to bootstrap system private Set<String> BOOTSTRAP_USERS =
     * new TreeSet<>(Collections.singletonList(DatabaseDriver.USERADMIN_USERNAME));
     */
    public User authenticate(String username, String password) {
        // allow admin to bootstrap system & create further users
        if (isBootstrapUserWithoutPassword(username)) {
            userCredentialsDao.initialisePassword(username, password);
        }

        return userCredentialsDao.authenticate(username, password);
    }

    private boolean isBootstrapUserWithoutPassword(String username) {
        return isUserAdmin(username) && !userCredentialsDao.isPasswordInitialised(username);
    }

    private static boolean isUserAdmin(String username) {
        return DatabaseDriver.USERADMIN_USERNAME.equals(username);
    }

    public void changePassword(SessionPermissionAssistant spa, String existingPassword, String newPassword) {
        userCredentialsDao.updatePassword(spa.getUsername(), existingPassword, newPassword);
    }

}
