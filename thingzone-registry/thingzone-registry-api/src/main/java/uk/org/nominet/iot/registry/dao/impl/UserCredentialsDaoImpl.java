/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.AbstractJpaDao;
import uk.org.nominet.iot.registry.dao.UserCredentialsDao;
import uk.org.nominet.iot.registry.model.Group;
import uk.org.nominet.iot.registry.model.Privilege;
import uk.org.nominet.iot.registry.model.User;
import uk.org.nominet.iot.registry.model.UserCredentials;
import uk.org.nominet.iot.registry.model.UserToken;

class UserCredentialsDaoImpl extends AbstractJpaDao<UserCredentials> implements UserCredentialsDao {
    private final static Logger log = LoggerFactory.getLogger(UserCredentialsDaoImpl.class);

    UserCredentialsDaoImpl() {
        super(UserCredentials.class);
    }

    private final static String AUTH_ERRMSG = "authentication error";

    @Override
    public UserCredentials create(String username, String password, User user, String email) {
        if (exists(username)) {
            log.info("Username \"{}\" exists already", username);
            throw new DataException(AUTH_ERRMSG, Status.CONFLICT);
        }
        if (email == null)
            email = "";
        UserCredentials uc = new UserCredentials(username, password, user, email);
        persist(uc);
        log.info("created user {} with ID={}", username, user.getId());
        return uc;
    }

    @Override
    public void createNoLogon(String username, User user) {
        if (exists(username)) {
            log.info("Username \"{}\" exists already", username);
            throw new DataException(AUTH_ERRMSG, Status.CONFLICT);
        }
        UserCredentials uc = UserCredentials.createNoLogon(username, user);
        persist(uc);
        log.info("created no-logon user {} with ID={}", username, user.getId());
    }

    @Override
    public boolean exists(String username) {
        try {
            loadByName(username);
            return true;
        } catch (PersistenceException e) {
            if (e instanceof NoResultException) {
                return false;
            } else {
                throw e;
            }
        }
    }

    @Override
    public User authenticate(String username, String password) {
        UserCredentials uc;
        try {
            uc = loadByName(username);
        } catch (PersistenceException e) {
            if (e instanceof NoResultException) {
                // user doesn't exist
                throw new DataException(AUTH_ERRMSG, Status.UNAUTHORIZED);
            } else {
                throw e;
            }
        }

        if (uc.verifyPassword(password)) {
            return uc.getUser();
        } else {
            log.info("authentication error for username {}", username, Status.UNAUTHORIZED);
            throw new DataException(AUTH_ERRMSG, Status.UNAUTHORIZED);
        }
    }

    @Override
    public void updatePassword(String username, String oldPassword, String newPassword) {
        if (!exists(username)) {
            log.info("Username \"{}\" does not exist", username);
            throw new DataException(AUTH_ERRMSG, Status.UNAUTHORIZED);
        }
        UserCredentials uc = loadByName(username);
        if (uc.verifyPassword(oldPassword)) {
            uc.updatePassword(oldPassword, newPassword);
        } else {
            log.info("authentication error for username {}", username);
            throw new DataException(AUTH_ERRMSG, Status.UNAUTHORIZED);
        }
    }

    @Override
    public void tokenUpdatePassword(String username, UserToken token, String newPassword) {
        if (!exists(username)) {
            log.info("Username \"{}\" does not exist", username);
            throw new DataException("Invalid token", Status.FORBIDDEN);
        }
        UserCredentials uc = loadByName(username);

        if (token.getUserCredentials() != uc || !new UserTokenDaoImpl().valid(token)) {
            throw new DataException("Invalid token", Status.FORBIDDEN);
        }
        uc.setPassword(newPassword);
        token.setUsed(Instant.now());
    }

    @Override
    public void initialisePassword(String username, String newPassword) {
        if (isPasswordInitialised(username)) {
            log.info("username {} cannot initialise password because it is already set", username);
            throw new DataException(AUTH_ERRMSG);
        }
        loadByName(username).initialisePassword(newPassword);
    }

    @Override
    public boolean isPasswordInitialised(String username) {
        if (!exists(username)) {
            log.info("Username \"{}\" does not exist", username);
            throw new DataException(AUTH_ERRMSG, Status.UNAUTHORIZED);
        }
        UserCredentials uc = loadByName(username);
        return uc.isPasswordSet();
    }

    @Override
    public UserCredentials get(String username) {
        UserCredentials uc = loadByName(username);
        return uc;
    }

    @Override
    public User getUserByUsername(String username) {
        UserCredentials uc = loadByName(username);
        return uc.getUser();
    }

    @Override
    public String getUsernameByUser(User user) {
        return loadByMatch("user", user).getUsername();
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Boolean> listUsernamesByGroup(Group group) {
        checkTransaction();

        Map<String, Boolean> groupUsers = new TreeMap<>();
        // Not using a named query due to needing fields from multiple tables
        EntityManager em = getEntityManager();
        Query query = em.createQuery("SELECT uc.username, gu.admin FROM UserCredentials uc "
            + "JOIN GroupUser gu ON uc.user = gu.user " + "WHERE gu.group = :group ORDER BY uc.username");
        query.setParameter("group", group);
        List<Object[]> rows = query.getResultList();
        for (Object[] row : rows) {
            String username = (String) row[0];
            Boolean admin = (Boolean) row[1];
            groupUsers.put(username, admin);
        }
        return groupUsers;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<String> getStandardUsers() {
        checkTransaction();

        EntityManager em = getEntityManager();
        Query query = em.createNativeQuery("SELECT uc.username, up.privilege FROM thingzone.user_credentials uc"
            + " LEFT JOIN thingzone.UserPrivileges up ON uc.user_id = up.user_id ORDER BY uc.username");
        List<Object[]> rows = query.getResultList();

        Set<String> adminUsernames = new TreeSet<String>();
        Set<String> usernames = new TreeSet<String>();

        List<Privilege> adminPrivileges = Arrays.asList(Privilege.USE_SESSION, Privilege.DUMP_ALL);
        for (Object[] row : rows) {
            String username = (String) row[0];
            usernames.add(username);
            if (row[1] != null) {
                Privilege privilege = Privilege.fromString((String) row[1]);
                if (adminPrivileges.contains(privilege)) {
                    adminUsernames.add(username);
                }
            }
        }
        return usernames.stream().filter(p -> !adminUsernames.contains(p)).collect(Collectors.toList());
    }

    // ====================================================
    // Utility, internal

    private UserCredentials loadByName(String username) {
        return loadByMatch("username", username);
    }

}
