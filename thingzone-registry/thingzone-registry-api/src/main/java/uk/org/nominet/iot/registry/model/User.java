/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;

import com.fasterxml.jackson.annotation.JsonIgnore;

import uk.org.nominet.iot.registry.database.ColumnPosition;
import uk.org.nominet.iot.registry.database.ColumnPositionCustomizer;
import uk.org.nominet.iot.registry.database.DatabaseDriver;

@Entity
@Table(name = "Users", schema = DatabaseDriver.SCHEMA_NAME, uniqueConstraints = {})
@NamedQueries({ @NamedQuery(name = "User.findByUsername",
                            query = "SELECT us FROM User us JOIN UserCredentials uc ON uc.user = us "
                                + "WHERE uc.username = :username") })
@Customizer(ColumnPositionCustomizer.class)
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ColumnPosition(position = 1)
    @JsonIgnore
    private Long id;

    @Column(name = "preferences", columnDefinition = "JSONB", nullable = false) // empty="{}"
    @Convert(converter = JsonbStringConverter.class) // our custom (to/from String) converter
    @ColumnPosition(position = 2)
    private String preferences;

    @Column(name = "details", columnDefinition = "JSONB", nullable = false) // empty="{}"
    @Convert(converter = JsonbStringConverter.class) // our custom (to/from String) converter
    @ColumnPosition(position = 3)
    private String details;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "owner")
    private Account account;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "user")
    private UserCredentials userCredentials;

    @OneToMany(mappedBy = "user")
    private List<GroupUser> groups;

    @OneToMany(mappedBy = "owner")
    private List<Group> ownedGroups;

    @OneToMany(mappedBy = "user")
    private List<UserPrivilege> userPrivileges;

    User() {}

    public User(String preferences, String details) {
        this.preferences = preferences;
        this.details = details;
    }

    public Long getId() {
        return id;
    }

    public String getPreferences() {
        return preferences;
    }

    public void setPreferences(String preferences) {
        this.preferences = preferences;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", preferences='" + preferences + '\'' + ", details='" + details + '\'' + '}';
    }

    // comparison just on id
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    // comparison just on id
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User other = (User) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

}
