/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.upsert;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.service.DNSdataService;
import uk.org.nominet.iot.registry.service.dto.DNSdataDto;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;
import uk.nominet.iot.utils.error.ProgramDefectException;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * DnsDataUpsertMetaService is a meta-service to help with upserts It helps upsert DNS data, so is used for
 * Device/DataStream/Transform upsert
 */
public class DnsDataUpsertMetaService {
    private final static Logger log = LoggerFactory.getLogger(new Object() {}.getClass().getEnclosingClass());

    private final DNSdataService dnsDataService = new DNSdataService();

    public void upsert(SessionPermissionAssistant spa, String key, String dnsRecordsAsJsonString) {
        List<DNSdataDto> requestedRecords = decodeFromJson(dnsRecordsAsJsonString, key);

        // log.info("DNSrecords received={}", dnsRecordsAsJsonString);
        // log.info("DNSrecords inferred={}", requestedRecords);
        List<DNSdataDto> existingRecords
            = dnsDataService.dnsDataListByKey(spa, key).stream().map(DNSdataDto::new).collect(Collectors.toList());
        // log.info("DNSrecords existing ={}", existingRecords);

        if (areSame(requestedRecords, existingRecords)) {
            log.info("DNS records are already as requested: no need to update");
        } else {
            updateDnsRecords(spa, requestedRecords, existingRecords);
        }
    }

    private void updateDnsRecords(SessionPermissionAssistant spa, List<DNSdataDto> requestedRecords,
                                  List<DNSdataDto> existingRecords) {
        // first, delete any DNS records that are entirely removed
        existingRecords.stream().filter(x -> requestedRecords.stream().noneMatch(y -> y.refersToSameDbEntity(x)))
                       .forEach(x -> dnsDataService.removeRecord(spa, x));

        // then update all the new/modified ones
        requestedRecords.forEach(x -> dnsDataService.updateRecord(spa, x));
    }

    static List<DNSdataDto> decodeFromJson(String dnsJson, String parentKey) {
        try {
            DNSdataDto[] decoded = new ObjectMapper().readValue(dnsJson, DNSdataDto[].class);
            return Arrays.stream(decoded).map(x -> DNSdataDto.withDnsKey(x, new DNSkey(parentKey)))
                         .collect(Collectors.toList());
        } catch (IOException | ProgramDefectException e) {
            throw new DataException("bad JSON: " + e.getMessage());
        }
    }

    static private boolean areSame(List<DNSdataDto> lhs, List<DNSdataDto> rhs) {
        if (lhs.size() != rhs.size())
            return false;
        for (DNSdataDto x : lhs) {
            if (rhs.stream().filter(x::contentsEqual).count() != 1)
                return false;
        }
        return true;
    }
}
