/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service;

import uk.org.nominet.iot.registry.dao.*;
import uk.org.nominet.iot.registry.dao.dto.*;
import uk.org.nominet.iot.registry.model.*;
import uk.org.nominet.iot.registry.service.dto.*;
import uk.org.nominet.iot.registry.service.dto.DumpObjects.DataStreamWithPermissionsAndDeviceKeys;
import uk.org.nominet.iot.registry.service.dto.DumpObjects.DeviceWithPermissions;

import java.util.*;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * DumpRetriever - retrieves DB entities for "dump" & formats into JSON-ready objects - uses pluggable DB access: either
 * normal (+ permission check) or fast JDBC/unmanaged
 */
class DumpRetriever {
    private final AccountIdNameCache accountIdNameCache = new AccountIdNameCache();

    private final DumpRetrieverAccessor accessor;

    DumpRetriever(DumpRetrieverAccessor accessor) {
        this.accessor = accessor;
    }

    // dump just one datastream, to get parent devices & permissions
    DataStreamWithPermissionsAndDeviceKeys dumpDataStream(String dataStreamKey) {
        DataStream dataStream = getDataStream(dataStreamKey);
        DumpObjects.GenericStreamDetails genericStreamDetails = getGenericStreamDetails(dataStream);
        return getDataStreamSpecificDetails(dataStream, genericStreamDetails);
    }

    // dump just one datastream, to get parent devices & permissions
    DeviceWithPermissions dumpDevice(Device device) {
        // get DNSdata for the device
        List<DNSdata> dnsDatas = listDnsDataByKey(device.getKey());

        // get permissions
        Map<String, Set<Permission>> permissionsAsUsers = getUserPermissionsForObject(device.getKey());
        Map<String, Set<Permission>> permissionsAsGroups = getGroupPermissionsForObject(device.getKey());

        // get links to DataStreams
        List<DataStreamSummaryDto> dataStreamLinks = new ArrayList<>();
        for (StreamOnDeviceRow sod : listStreamOnDeviceByDevice(device.getKey())) {
            dataStreamLinks.add(DataStreamSummaryDto.basicStreamLink(sod.getDataStreamKey(), sod.getName()));
        }

        return new DumpObjects.DeviceWithPermissions(
            new DeviceSummaryDetailDto(device.getKey(), device.getMetadata(), device.getLocation(), dataStreamLinks,
                                       dnsDatas.stream().map(DNSdataDto::new).collect(Collectors.toList()),
                                       null, permissionsAsUsers, permissionsAsGroups));

    }

    DumpObjects.GenericStreamDetails getGenericStreamDetails(DataStream dataStream) {
        // get DNSdata for the dataStream
        List<DNSdata> dnsDatas = listDnsDataByKey(dataStream.getKey());

        // get permissions
        Map<String, Set<Permission>> permissionsAsUsers = getUserPermissionsForObject(dataStream.getKey());
        Map<String, Set<Permission>> permissionsAsGroups = getGroupPermissionsForObject(dataStream.getKey());

        // get links to Devices
        List<DumpObjects.DeviceStreamLink> deviceLinks = new ArrayList<>();
        for (StreamOnDeviceRow sod : listStreamOnDeviceByStream(dataStream.getKey())) {
            deviceLinks.add(new DumpObjects.DeviceStreamLink(sod.getDeviceKey(), sod.getName()));
        }

        // both datastreams & transforms can have push subscribers
        List<PushSubscriberRow> pushSubscribers = listPushSubscribers(dataStream);

        return new DumpObjects.GenericStreamDetails(dnsDatas, permissionsAsUsers, permissionsAsGroups, deviceLinks,
                                                    pushSubscribers);
    }

    DataStreamWithPermissionsAndDeviceKeys getDataStreamSpecificDetails(DataStream dataStream,
                                                                        DumpObjects.GenericStreamDetails genericStreamDetails) {
        return new DumpObjects.DataStreamWithPermissionsAndDeviceKeys(DataStreamSummaryDto.details(dataStream,
            dataStream.getMetadata(),
            genericStreamDetails.dnsDatas.stream().map(dns -> new DNSdataDto(dns)).collect(Collectors.toList()),
            genericStreamDetails.permissionsAsUsers, genericStreamDetails.permissionsAsGroups, null,
            genericStreamDetails.deviceLinks.stream()
                                            .map(d -> DeviceSummaryDetailDto.basicStreamLink(d.key, d.linkName))
                                            .collect(Collectors.toList()),
            PullSubscriptionSummaryDto.fromOptionalRow(getPullSubscription(dataStream)),
            PushSubscriberSummaryDto.asListFromRow(genericStreamDetails.pushSubscribers)));
    }

    DumpObjects.TransformWithPermissions getTransformSpecificDetails(DataStream dataStream,
                                                                     DumpObjects.GenericStreamDetails genericStreamDetails,
                                                                     User publicUser) {
        // get the enclosing transform
        Transform transform = getTransformByOutputStream(dataStream);

        // get the sources and output streams
        List<TransformSourceRow> transformSources = getTransformSources(transform);
        List<TransformOutputStreamRow> transformStreams = getTransformOutputStreams(transform);

        return new DumpObjects.TransformWithPermissions(TransformSummaryDetailDto.details(transform,
            DataStreamSummaryDto.details(dataStream, dataStream.getMetadata(),
                genericStreamDetails.dnsDatas.stream().map(dns -> new DNSdataDto(dns)).collect(Collectors.toList()),
                genericStreamDetails.permissionsAsUsers, genericStreamDetails.permissionsAsGroups, null,
                genericStreamDetails.deviceLinks.stream()
                                                .map(d -> DeviceSummaryDetailDto.basicStreamLink(d.key, d.linkName))
                                                .collect(Collectors.toList()),
                PullSubscriptionSummaryDto.fromOptionalRow(getPullSubscription(dataStream)),
                PushSubscriberSummaryDto.asListFromRow(genericStreamDetails.pushSubscribers)),
            TransformSourceDto.asListFromRow(transformSources),
            TransformOutputStreamDto.asListFromRow(transformStreams), publicUser));
    }

    String getUsernameForAccountId(Long accountId) {
        return accountIdNameCache.getUsernameFromAccountId(new AccountId(accountId));
    }

    String getGroupForAccountId(Long accountId) {
        return accountIdNameCache.getGroupFromAccountId(new AccountId(accountId));
    }

    Map<String, Set<Permission>> getUserPermissionsForObject(DNSkey key) {
        return accessor.getUserPermissions(key);
    }

    Map<String, Set<Permission>> getGroupPermissionsForObject(DNSkey key) {
        return accessor.getGroupPermissions(key);
    }

    // look up & cache AccountId -> Username mapping
    // (NB we rely on this being "small" but otherwise we'd either have
    // to join every ACL lookup (tedious in JDBC) or look up every time (slow) )
    private class AccountIdNameCache {
        private UserService userService;
        private AccountDao accountDao;

        // cache accountId -> username mapping
        private Map<AccountId, String> accountIdUsernameCache = new HashMap<>();
        private Map<AccountId, String> accountIdGroupCache = new HashMap<>();

        AccountIdNameCache() {
            accountDao = DaoFactoryManager.getFactory().accountDao();
            userService = new UserService();
        }

        private String getUsernameFromAccountId(AccountId accountId) {
            if (accountIdUsernameCache.containsKey(accountId)) {
                return accountIdUsernameCache.get(accountId);
            } else {
                return updateNameFromAccountId(accountId, false);
            }
        }

        private String getGroupFromAccountId(AccountId accountId) {
            if (accountIdGroupCache.containsKey(accountId)) {
                return accountIdGroupCache.get(accountId);
            } else {
                return updateNameFromAccountId(accountId, true);
            }
        }

        private String updateNameFromAccountId(AccountId accountId, boolean group) {
            // look up in cache
            Account account = accountDao.getById(accountId);
            if (group && account.getGroup() != null) {
                String groupName = account.getGroup().getName();
                accountIdGroupCache.put(accountId, groupName);
                return groupName;
            } else if (!group && account.getOwner() != null) {
                String username = userService.getUsernameByUser(account.getOwner());
                accountIdUsernameCache.put(accountId, username);
                return username;
            }
            return null;
        }
    }

    private List<DNSdata> listDnsDataByKey(DNSkey key) {
        return accessor.listDnsDataByKey(key);
    }

    private List<StreamOnDeviceRow> listStreamOnDeviceByStream(DNSkey key) {
        return accessor.listStreamOnDeviceByStream(key);
    }

    private List<StreamOnDeviceRow> listStreamOnDeviceByDevice(DNSkey key) {
        return accessor.listStreamOnDeviceByDevice(key);
    }

    private List<PushSubscriberRow> listPushSubscribers(DataStream dataStream) {
        return accessor.listPushSubscribers(dataStream);
    }

    private Optional<PullSubscriptionRow> getPullSubscription(DataStream dataStream) {
        return accessor.getPullSubscription(dataStream);
    }

    private DataStream getDataStream(String key) {
        return accessor.getDataStream(key);
    }

    private Transform getTransformByOutputStream(DataStream dataStream) {
        return accessor.getTransformByOutputStream(dataStream);
    }

    private List<TransformSourceRow> getTransformSources(Transform transform) {
        return accessor.getTransformSources(transform);
    }

    private List<TransformOutputStreamRow> getTransformOutputStreams(Transform transform) {
        return accessor.getTransformOutputStreams(transform);
    }
}
