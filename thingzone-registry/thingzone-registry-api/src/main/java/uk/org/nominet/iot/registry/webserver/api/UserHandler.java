/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.database.DatabaseTransaction;
import uk.org.nominet.iot.registry.database.ReadOnlyDatabaseOperation;
import uk.org.nominet.iot.registry.database.SimpleDatabaseOperation;
import uk.org.nominet.iot.registry.model.Privilege;
import uk.org.nominet.iot.registry.model.UserTokenType;
import uk.org.nominet.iot.registry.service.UserAuthenticationService;
import uk.org.nominet.iot.registry.service.UserService;
import uk.org.nominet.iot.registry.webserver.api.support.JsonErrorResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSingleResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSuccessResult;
import uk.org.nominet.iot.registry.webserver.api.support.PersistenceEffect;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;
import uk.org.nominet.iot.registry.webserver.api.support.SessionWrapper;

/**
 * The API handler for user actions.
 */
@Path("/user")
public class UserHandler {
    private final static Logger log = LoggerFactory.getLogger(UserHandler.class);

    @Context
    Request request;

    @Context
    private javax.servlet.http.HttpServletRequest hsr;

    private UserAuthenticationService userAuthenticationService;
    private UserService userService;

    /**
     * Instantiates a new user handler.
     */
    public UserHandler() {
        userAuthenticationService = new UserAuthenticationService();
        userService = new UserService();
    }

    /**
     * Create a new user
     *
     * If the password is not set then create a random password and return a password reset token to the user.
     *
     * @param username the username for the new user
     * @param password if this is not set then a password reset token will be created
     * @param email the user email - if set and validated this will be used to contact the user for things like password
     *        reset tokens
     * @param privilegesString the list of privileges for the new user
     * @param expiry overrides the 24 hour default expiry period for the password reset token
     * @return the object
     */
    @POST
    @Path("create")
    @Produces(MediaType.APPLICATION_JSON)
    public Object createUser(final @FormParam("username") String username,
                             final @FormParam("password") String password,
                             final @FormParam("email") String email,
                             final @FormParam("privileges") String privilegesString,
                             final @FormParam("expiry") Long expiry) {
        if (Strings.isNullOrEmpty(username))
            return new JsonErrorResult("missing username").asResponse(Status.BAD_REQUEST);
        if (password != null && password.isEmpty())
            return new JsonErrorResult("Password cannot be empty if set").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        long secondsExpiry = expiry == null ? 60 * 60 * 24 : expiry.longValue();

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[user/create] (username={}, password={}, email={}, privileges={}, expiry={})", username,
                    Strings.isNullOrEmpty(password) ? "" : "***", email, privilegesString, secondsExpiry);

                List<Privilege> privileges = validatePrivileges(privilegesString);
                userService.createUser(spa, username, password, email, privileges);

                if (password == null) {
                    String token = userService.getToken(spa, username, UserTokenType.PASSWORD, secondsExpiry);
                    return new JsonSingleResult("token", token).output();
                }
                return new JsonSuccessResult();
            }
        });
    }

    /**
     * Change password.
     *
     * @param existingPassword the existing password
     * @param newPassword the new password
     * @return success or failure
     */
    @PUT
    @Path("change_password")
    @Produces(MediaType.APPLICATION_JSON)
    public Object changePassword(final @FormParam("existingPassword") String existingPassword,
                                 final @FormParam("newPassword") String newPassword) {
        if (Strings.isNullOrEmpty(existingPassword))
            return new JsonErrorResult("missing existingPassword").asResponse(Status.BAD_REQUEST);
        if (Strings.isNullOrEmpty(newPassword))
            return new JsonErrorResult("missing newPassword").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                // don't log the passwords
                log.info("[user/change_password] (username={})");

                userAuthenticationService.changePassword(spa, existingPassword, newPassword);

                return new JsonSuccessResult();
            }
        });
    }

    /**
     * Gets the user details.
     *
     * @param usernameOption the username option
     * @return the user details
     */
    @GET
    @Path("get")
    @Produces(MediaType.APPLICATION_JSON)
    public Object getUserDetails(final @QueryParam("username") String usernameOption) {

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new ReadOnlyDatabaseOperation(hsr) {
            public Object execute() {

                log.info("[user/get] ()");

                return new JsonSingleResult("user", userService.getDetails(spa, usernameOption)).output();
            }
        });
    }

    /**
     * List users.
     *
     * @return an array of usernames
     */
    @GET
    @Path("list_names")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object list() {

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            @Override
            public Object execute() {
                List<String> usernames = userService.getUsernames(spa);

                return new JsonSingleResult("users", usernames).output();
            }
        });
    }

    /**
     * Update user details for current user.
     *
     * @param username the username
     * @param updatedName specify a new username for the user
     * @param details the details
     * @param preferences the preferences
     * @return success or failure
     */
    @PUT
    @Path("update")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Object update(final @FormDataParam("username") String username,
                         final @FormDataParam("updatedName") String updatedName,
                         final @FormDataParam("details") String details,
                         final @FormDataParam("preferences") String preferences) {
        if (Strings.isNullOrEmpty(username)) {
            if (!Strings.isNullOrEmpty(updatedName)) {
                return new JsonErrorResult("Can't update own username").asResponse(Status.BAD_REQUEST);
            }
            if (Strings.isNullOrEmpty(details) && Strings.isNullOrEmpty(preferences)) {
                return new JsonErrorResult("missing form part \"details\" or \"preferences\"").asResponse(
                    Status.BAD_REQUEST);
            }
        } else {
            if (Strings.isNullOrEmpty(details) && Strings.isNullOrEmpty(preferences)
                && Strings.isNullOrEmpty(updatedName)) {
                return new JsonErrorResult("missing form part \"details\" or \"preferences\" or \"updatedName\"").asResponse(
                    Status.BAD_REQUEST);
            }
        }
        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[user/update] (details={} preferences={})", details, preferences);

                userService.update(spa, username, updatedName, details, preferences);

                return new JsonSuccessResult();
            }
        });
    }

    /**
     * Sets the privileges.
     *
     * @param username the username
     * @param privilegesString the privileges string
     * @return success or failure
     */
    @PUT
    @Path("set_privileges")
    @Produces(MediaType.APPLICATION_JSON)
    public Object setPrivileges(final @FormParam("username") String username,
                                final @FormParam("privileges") String privilegesString) {
        if (Strings.isNullOrEmpty(username))
            return new JsonErrorResult("missing param \"username\"").asResponse(Status.BAD_REQUEST);
        if (privilegesString == null) // but CAN be empty
            return new JsonErrorResult("missing param \"privileges\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[permission/set] (username={} privileges={})", username, privilegesString);

                SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);
                List<Privilege> privileges = validatePrivileges(privilegesString);
                userService.setPrivileges(spa, username, privileges);

                return new JsonSuccessResult();
            }
        });
    }

    /**
     * Request a password reset token.
     *
     * @param username the username of the user to reset the password of
     * @param expiry how long the token should be active for in seconds
     * @return the generated token
     */
    @GET
    @Path("request_token")
    @Produces(MediaType.APPLICATION_JSON)
    public Object requestToken(final @QueryParam("username") String username, final @QueryParam("expiry") Long expiry) {
        if (Strings.isNullOrEmpty(username))
            return new JsonErrorResult("missing param \"username\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        // Default expiry to 1 day if not specified
        long secondsExpiry = expiry == null ? 60 * 60 * 24 : expiry.longValue();

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[user/request_token] (username={})", username);

                String token = userService.getToken(spa, username, UserTokenType.PASSWORD, secondsExpiry);

                return new JsonSingleResult("token", token).output();
            }
        });
    }

    /**
     * Sets the password.
     *
     * @param username the username
     * @param token a valid password reset token
     * @param newPassword the new password
     * @return success or failure
     */
    @PUT
    @Path("set_password")
    @Produces(MediaType.APPLICATION_JSON)
    public Object setPassword(final @FormParam("username") String username,
                              final @FormParam("token") String token,
                              final @FormParam("newPassword") String newPassword) {
        if (Strings.isNullOrEmpty(username))
            return new JsonErrorResult("missing param \"username\"").asResponse(Status.BAD_REQUEST);
        if (Strings.isNullOrEmpty(token))
            return new JsonErrorResult("missing param \"token\"").asResponse(Status.BAD_REQUEST);
        if (Strings.isNullOrEmpty(newPassword))
            return new JsonErrorResult("missing param \"newPassword\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[user/set_password] (username={})", username);

                userService.setPassword(username, token, newPassword);

                return new JsonSuccessResult();
            }
        });
    }

    /**
     * Validate privileges.
     *
     * @param privilegesString the privileges string
     * @return the list
     */
    private static List<Privilege> validatePrivileges(String privilegesString) {
        // parse & validate array of privileges
        List<Privilege> privileges = new ArrayList<>();

        if (Strings.isNullOrEmpty(privilegesString)) {
            return privileges; // empty list
        }

        for (String p : Splitter.on(CharMatcher.anyOf(", ")).trimResults().omitEmptyStrings().split(privilegesString)) {
            if (!Privilege.isValid(p)) {
                throw new DataException("invalid privilege type:\"" + p + "\"");
            }
            privileges.add(Privilege.fromString(p));
        }
        return privileges;
    }

}
