/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.dto;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.google.common.collect.ImmutableList;

import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.Permission;

public class DeviceSummaryDetailDto {

    @JsonUnwrapped
    DnsKeyDetailDto basicEntity;

    @JsonRawValue
    @JsonInclude(Include.NON_EMPTY)
    public String location;

    @JsonRawValue
    @JsonInclude(Include.NON_EMPTY)
    public String metadata;

    @JsonInclude(Include.NON_EMPTY)
    public List<DataStreamSummaryDto> dataStreams;

    @JsonInclude(Include.NON_NULL)
    public String linkName;

    /**
    * All possible device information available for output
    * @param dnsKey the entity info
    * @param metadata this is separated to check for VIEW_METADATA permission
    * @param location this is separated to check for VIEW_LOCATION permission
    * @param dataStreams any linked dataStreams
    * @param dnsRecords the DNS records for the key entity
    * @param streamName the stream name if this is linked to a dataStream
    * @param userPermissions the user permissions for the key - requires ADMIN
    * @param groupPermissions the group permissions for the key - requires ADMIN
    */
    public DeviceSummaryDetailDto(DNSkey dnsKey,
            String metadata,
            String location,
            List<DataStreamSummaryDto> dataStreams,
            List<DNSdataDto> dnsRecords,
            String streamName,
            Map<String, Set<Permission>> userPermissions,
            Map<String, Set<Permission>> groupPermissions) {
        this.basicEntity = DnsKeyDetailDto.detail(dnsKey, dnsRecords, userPermissions, groupPermissions);
        this.location = location;
        this.metadata = metadata;
        this.dataStreams = dataStreams;
        this.linkName = streamName;
    }

    public DeviceSummaryDetailDto(DnsKeyDetailDto entity, String metadata, String location) {
        this.basicEntity = entity;
        this.location = location;
        this.metadata = metadata;
    }

    /**
     * Basic device summary
     */
    public static DeviceSummaryDetailDto basic(DNSkey dnsKey,
            String metadata,
            String location,
            List<DataStreamSummaryDto> dataStreams,
            List<DNSdataDto> dnsRecords) {
        return new DeviceSummaryDetailDto(dnsKey, metadata, location, dataStreams, dnsRecords, null,
                null, null);
    }

    /**
    * Basic streamLink
    * @param dnsKey the entity info
    * @param streamName the name of the stream
    */
    public static DeviceSummaryDetailDto basicStreamLink(String dnsKey, String streamName) {
        return new DeviceSummaryDetailDto(new DNSkey(dnsKey), null, null, ImmutableList.of(), ImmutableList.of(), streamName,
                null, null);
    }
}
