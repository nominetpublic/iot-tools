/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.dto;

import java.io.UnsupportedEncodingException;

import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import uk.org.nominet.iot.registry.model.Resource;

public class ResourceDetailDto {
    @JsonUnwrapped
    DnsKeyDetailDto basicEntity;

    public String resourceName;

    @JsonRawValue
    public String content;

    public ResourceDetailDto(Resource resource) throws UnsupportedEncodingException {
        this.basicEntity = DnsKeyDetailDto.basic(resource.getKey());
        this.resourceName = resource.getName();
        this.content = new String(resource.getContent(), "UTF-8");
    }
}
