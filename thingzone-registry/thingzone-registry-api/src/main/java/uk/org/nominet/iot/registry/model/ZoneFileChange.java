/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Customizer;

import com.google.common.base.Strings;

import uk.org.nominet.iot.registry.database.ColumnPosition;
import uk.org.nominet.iot.registry.database.ColumnPositionCustomizer;
import uk.org.nominet.iot.registry.database.DatabaseDriver;
import uk.org.nominet.iot.registry.service.dto.DNSdataDto;

@Entity
@Table(name = "ZoneFileChanges", schema = DatabaseDriver.SCHEMA_NAME)
@Customizer(ColumnPositionCustomizer.class)
public class ZoneFileChange {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ColumnPosition(position = 1)
    private Long id;

    // populate from sequence
    @Column(name = "transaction", nullable = false)
    @ColumnPosition(position = 2)
    private Long transaction;

    @Column(name = "operation", nullable = false)
    @ColumnPosition(position = 3)
    @Enumerated(EnumType.ORDINAL)
    private ZoneFileChangeOperation operation;

    @Lob
    @Column(name = "rrname", nullable = false)
    @ColumnPosition(position = 4)
    private String rrname;

    @Column(name = "ttl", nullable = false)
    @ColumnPosition(position = 5)
    private Integer ttl;

    @Column(name = "rrtype", nullable = false)
    @ColumnPosition(position = 6)
    private String rrtype;

    @Lob
    @Column(name = "rdata", nullable = false)
    @ColumnPosition(position = 7)
    private String rdata;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "timestamp", updatable = false, nullable = false)
    @ColumnPosition(position = 8)
    private Date timestamp;

    @PrePersist
    void initTimestamp() {
        this.timestamp = new Date();
    }

    @Column(nullable = true)
    @ColumnPosition(position = 9)
    private Long serial;

    ZoneFileChange() {}

    public ZoneFileChange(Long transaction, ZoneFileChangeOperation operation, DNSdataDto ddr) {
        this.rrname = ddr.getDnsKey().getKey().toString();
        if (!Strings.isNullOrEmpty(ddr.getSubdomain())) {
            this.rrname = ddr.getSubdomain() + "." + this.rrname;
        }
        this.transaction = transaction;
        this.operation = operation;
        this.ttl = ddr.getTtl();
        this.rrtype = ddr.getRrtype();
        this.rdata = ddr.getRdata();
    }

    @Override
    public String toString() {
        return rrname + " " + transaction + (operation == ZoneFileChangeOperation.ADD ? " ADD " : " REMOVE ") + ttl
            + " " + rrtype + " " + rdata;
    }

    public String getRrname() {
        return rrname;
    }

    public Long getTransaction() {
        return transaction;
    }

    public ZoneFileChangeOperation getOperation() {
        return operation;
    }

    public Integer getTtl() {
        return ttl;
    }

    public String getRrtype() {
        return rrtype;
    }

    public String getRdata() {
        return rdata;
    }

    public Long getSerial() {
        return serial;
    }

    public void setSerial(Long serial) {
        this.serial = serial;
    }

    public Date getTimestamp() {
        return timestamp;
    }

}
