/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import uk.org.nominet.iot.registry.database.DatabaseTransaction;
import uk.org.nominet.iot.registry.database.ReadOnlyDatabaseOperation;
import uk.org.nominet.iot.registry.database.SimpleDatabaseOperation;
import uk.org.nominet.iot.registry.service.DumpService;
import uk.org.nominet.iot.registry.webserver.api.support.JsonErrorResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSingleResult;
import uk.org.nominet.iot.registry.webserver.api.support.PersistenceEffect;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;
import uk.org.nominet.iot.registry.webserver.api.support.SessionWrapper;

/**
 * Api handler for streaming data.
 */
@Path("/dump")
public class DumpHandler {
    private final static Logger log = LoggerFactory.getLogger(DumpHandler.class);

    @Context
    Request request;

    @Context
    private javax.servlet.http.HttpServletRequest hsr;

    private DumpService dumpService;

    /**
     * Instantiates a new dump handler.
     */
    public DumpHandler() {
        dumpService = new DumpService();
    }

    /**
     * Dump every entity in the database to an output stream
     *
     * @return the entities
     */
    @GET
    @Path("all")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object all() {

        final SessionWrapper sw = SessionWrapper.createPrivilegedSession(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        // NB the DB operation needs to be within the output-stream inner class
        // (otherwise the transaction will be closed prematurely)
        log.info("[dump/all]");
        StreamingOutput stream = new StreamingOutput() {
            @Override
            public void write(OutputStream os) throws IOException, WebApplicationException {
                final Writer writer = new BufferedWriter(new OutputStreamWriter(os));
                log.info("dump: writing output");

                Object error = new DatabaseTransaction().executeInTransaction(new ReadOnlyDatabaseOperation(hsr) {
                    public Object execute() throws IOException {

                        // do an initial check, so we throw a standard error message on failure
                        // (it won't parse nicely if emitted inside the streamed output)
                        dumpService.checkUserCanDump(spa);

                        // NB: to make it easier to decode, we use a different format!
                        // Still keeping JSON, we return an array instead of an object
                        // and delimit the elements with carriage returns
                        // The result is the final element (which has nice side-effect of returning => 1
                        // elements)
                        writer.write("[\n");
                        dumpService.dumpDevicesToStreamAsJson(spa, writer);
                        dumpService.dumpDataStreamsToStreamAsJson(spa, writer);
                        writer.write("{\"result\":\"ok\"}\n");
                        writer.write("]");

                        return null; // not used
                    }
                });

                // internal errors end up with a JsonDataExceptionResult being returned,
                // so pass that back to the user
                // (NB concatenating this will probably be bad JSON, but at least we
                // get something of a message back to the user)
                if (error != null) {
                    log.info("dump threw an error: {}", error);
                    if (error instanceof Response) {
                        writer.write(((Response) error).getEntity().toString());
                    }
                }

                log.info("dump: finished writing output");
                writer.flush();
            }
        };

        return Response.ok(stream).build();
    }

    /**
     * get DataStream's parent Devices & permissions.
     *
     * @param dataStream the data stream
     * @return the data stream
     */
    @GET
    @Path("datastream")
    @Produces(MediaType.APPLICATION_JSON)
    public Object getDataStream(final @QueryParam("dataStream") String dataStream) {
        if (Strings.isNullOrEmpty(dataStream)) {
            return new JsonErrorResult("missing param \"dataStream\"").asResponse(Status.BAD_REQUEST);
        }

        final SessionWrapper sw = SessionWrapper.createPrivilegedSession(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[internal/datastream_properties] (dataStream={})", dataStream);

                dumpService.checkUserCanDump(spa);

                return new JsonSingleResult("dataStream", dumpService.dumpDataStream(dataStream).dataStream).output();
            }
        });
    }

    /**
     * Get all changes since the specified date as an output stream
     *
     * @param dateString the date string
     * @return the stream of changes
     */
    @GET
    @Path("changes/{date}")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object history(final @PathParam(value = "date") String dateString) {
        final Date date;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(dateString);
        } catch (ParseException e) {
            return new JsonErrorResult("Incorrect date format, style used is: 2018-06-18T16:07:00+0100").asResponse(
                Status.BAD_REQUEST);
        }

        final SessionWrapper sw = SessionWrapper.createPrivilegedSession(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        // NB the DB operation needs to be within the output-stream inner class
        // (otherwise the transaction will be closed prematurely)
        log.info("[dump/history]");
        StreamingOutput stream = new StreamingOutput() {
            @Override
            public void write(OutputStream os) throws IOException, WebApplicationException {
                final Writer writer = new BufferedWriter(new OutputStreamWriter(os));
                log.info("dump: writing output");

                Object error = new DatabaseTransaction().executeInTransaction(new ReadOnlyDatabaseOperation(hsr) {
                    public Object execute() throws IOException {

                        // do an initial check, so we throw a standard error message on failure
                        // (it won't parse nicely if emitted inside the streamed output)
                        dumpService.checkUserCanDump(spa);

                        // NB: to make it easier to decode, we use a different format!
                        // Still keeping JSON, we return an array instead of an object
                        // and delimit the elements with carriage returns
                        // The result is the final element (which has nice side-effect of returning => 1
                        // elements)
                        writer.write("[\n");
                        dumpService.dumpStreamChangesAsJson(spa, writer, date);

                        writer.write("{\"result\":\"ok\"}\n");
                        writer.write("]");

                        return null; // not used
                    }
                });

                // internal errors end up with a JsonDataExceptionResult being returned,
                // so pass that back to the user
                // (NB concatenating this will probably be bad JSON, but at least we
                // get something of a message back to the user)
                if (error != null) {
                    log.info("dump threw an error: {}", error);
                    if (error instanceof Response) {
                        writer.write(((Response) error).getEntity().toString());
                    }
                }

                log.info("dump: finished writing output");
                writer.flush();
            }
        };
        return Response.ok(stream).build();
    }
}
