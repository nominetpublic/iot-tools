/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

import uk.nominet.iot.utils.error.ProgramDefectException;
import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.ACLDao;
import uk.org.nominet.iot.registry.dao.AbstractJpaDao;
import uk.org.nominet.iot.registry.dao.dto.AccountId;
import uk.org.nominet.iot.registry.dao.dto.AclId;
import uk.org.nominet.iot.registry.dao.dto.AclRow;
import uk.org.nominet.iot.registry.dao.dto.KeyPermissions;
import uk.org.nominet.iot.registry.model.ACL;
import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.Group;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.model.UserCredentials;


class ACLDaoImpl extends AbstractJpaDao<ACL> implements ACLDao {
    private final static Logger log = LoggerFactory.getLogger(ACLDaoImpl.class);

    ACLDaoImpl() {
        super(ACL.class);
    }

    @Override
    public ACL create(DNSkey key, Account account, Permission permission) {
        if (exists(key, account, permission)) {
            log.info("ACL exists already");
            throw new DataException("ACL exists already", Status.CONFLICT);
        }
        ACL acl = new ACL(key, account);
        acl.setPermission(permission);
        persist(acl);
        log.info("created ACL: {}", acl);
        return acl;
    }

    @Override
    public boolean exists(DNSkey key, Account account, Permission permission) {
        try {
            get(key, account, permission);
            return true;
        } catch (PersistenceException e) {
            if (e instanceof NoResultException) {
                return false;
            } else {
                throw e;
            }
        }
    }

    @Override
    public List<Permission> get(DNSkey key, Account account) {
        List<ACL> acls = findByFilter("key", key, "account", account);
        List<Permission> permissions = new ArrayList<>();
        for (ACL acl : acls) {
            permissions.add(acl.getPermission());
        }
        return ImmutableList.copyOf(permissions);
    }

    @Override
    public List<Account> get(DNSkey key, Permission permission) {
        List<ACL> acls = findByFilter("key", key, "permission", permission);
        List<Account> accounts = new ArrayList<>();
        for (ACL acl : acls) {
            accounts.add(acl.getAccount());
        }
        return ImmutableList.copyOf(accounts);
    }

    @Override
    public void remove(DNSkey key, Account account, Permission permission) {
        ACL acl = get(key, account, permission);
        remove(acl);
    }

    @Override
    public void removeAll(DNSkey key) {
        for (ACL acl : findByFilter("key", key)) {
            remove(acl);
            log.info("removed acl {} from {} for {}", acl.getPermission(), acl.getKey().getKey(), acl.getAccount());
        }
    }

    /**
     * List group permissions.
     *
     * @param key         the key
     * @param groupFilter the group filter or null
     */
    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Set<Permission>> listGroupPermissions(DNSkey key, String groupFilter) {
        Map<String, Set<Permission>> permissions = new TreeMap<>();
        // Not using a named query due to needing fields from multiple tables
        EntityManager em = getEntityManager();
        Query query = em.createQuery(
            "SELECT g.name, acl.permission FROM ACL acl " + "JOIN Account a ON acl.account = a "
                + "JOIN Group g ON g = a.group " + "WHERE acl.key = :key " + "AND (:name is null OR g.name = :name) ");
        query.setParameter("key", key);
        query.setParameter("name", groupFilter);
        List<Object[]> rows = query.getResultList();
        for (Object[] row : rows) {
            String groupname = (String) row[0];
            Permission permission = (Permission) row[1];
            if (!permissions.containsKey(groupname)) {
                permissions.put(groupname, new TreeSet<Permission>());
            }
            permissions.get(groupname).add(permission);
        }
        // If filtered by a specific groupname then always return that groupname even if
        // empty
        if (!Strings.isNullOrEmpty(groupFilter)) {
            if (!permissions.containsKey(groupFilter)) {
                permissions.put(groupFilter, new TreeSet<Permission>());
            }
        }
        return permissions;
    }

    /**
     * List user permissions.
     *
     * @param key        the key
     * @param userFilter the user filter or null
     */
    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Set<Permission>> listUserPermissions(DNSkey key, String userFilter) {
        Map<String, Set<Permission>> permissions = new TreeMap<>();
        // Not using a named query due to needing fields from multiple tables
        EntityManager em = getEntityManager();
        Query query = em.createQuery("SELECT uc.username, acl.permission FROM ACL acl "
            + "JOIN Account a ON acl.account = a " + "JOIN UserCredentials uc ON uc.user = a.owner "
            + "WHERE acl.key = :key " + "AND (:name is null OR uc.username = :name) ");
        query.setParameter("key", key);
        query.setParameter("name", userFilter);
        List<Object[]> rows = query.getResultList();
        for (Object[] row : rows) {
            String username = (String) row[0];
            Permission permission = (Permission) row[1];
            if (!permissions.containsKey(username)) {
                permissions.put(username, new TreeSet<Permission>());
            }
            permissions.get(username).add(permission);
        }
        // If filtered by a specific username then always return that username even if
        // empty
        if (!Strings.isNullOrEmpty(userFilter)) {
            if (!permissions.containsKey(userFilter)) {
                permissions.put(userFilter, new TreeSet<Permission>());
            }
        }
        return permissions;
    }

    /**
     * List all user and group permissions for the key.
     *
     * @param  key the key
     * @return     the key permissions
     */
    @SuppressWarnings("unchecked")
    @Override
    public KeyPermissions listAllPermissions(DNSkey key) {
        Map<String, Set<Permission>> userPermissions = new TreeMap<>();
        Map<String, Set<Permission>> groupPermissions = new TreeMap<>();
        // Not using a named query due to needing fields from multiple tables
        EntityManager em = getEntityManager();
        Query query = em.createQuery("SELECT u.userCredentials, a.group, acl.permission FROM ACL acl "
            + "JOIN Account a ON acl.account = a " + "LEFT JOIN a.owner as u " + "LEFT JOIN FETCH u.userCredentials "
            + "LEFT JOIN FETCH a.group " + "WHERE acl.key = :key ");
        query.setParameter("key", key);
        List<Object[]> rows = query.getResultList();
        for (Object[] row : rows) {
            Permission permission = (Permission) row[2];
            if (row[0] != null) {
                UserCredentials uc = (UserCredentials) row[0];
                if (!userPermissions.containsKey(uc.getUsername())) {
                    userPermissions.put(uc.getUsername(), new TreeSet<Permission>());
                }
                userPermissions.get(uc.getUsername()).add(permission);
            }
            if (row[1] != null) {
                Group g = (Group) row[1];
                if (!groupPermissions.containsKey(g.getName())) {
                    groupPermissions.put(g.getName(), new TreeSet<Permission>());
                }
                groupPermissions.get(g.getName()).add(permission);
            }
        }
        return new KeyPermissions(key, userPermissions, groupPermissions);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, KeyPermissions> listPermissionsForAccount(Account account, String name, int size, int page) {
        checkTransaction();
        EntityManager em = getEntityManager();
        Query query = em.createNativeQuery(
            "SELECT ac.key, COUNT(ac.key) FILTER (WHERE ac.permission = 'ADMIN') AS ADMIN, "
                + "COUNT(ac.key) FILTER (WHERE ac.permission = 'UPLOAD') AS UPLOAD, "
                + "COUNT(ac.key) FILTER (WHERE ac.permission = 'CONSUME') AS CONSUME,"
                + "COUNT(ac.key) FILTER (WHERE ac.permission = 'MODIFY') AS MODIFY, "
                + "COUNT(ac.key) FILTER (WHERE ac.permission = 'ANNOTATE') AS ANNOTATE, "
                + "COUNT(ac.key) FILTER (WHERE ac.permission = 'DISCOVER') AS DISCOVER, "
                + "COUNT(ac.key) FILTER (WHERE ac.permission = 'UPLOAD_EVENTS') AS UPLOAD_EVENTS, "
                + "COUNT(ac.key) FILTER (WHERE ac.permission = 'CONSUME_EVENTS') AS CONSUME_EVENTS, "
                + "COUNT(ac.key) FILTER (WHERE ac.permission = 'VIEW_LOCATION') AS VIEW_LOCATION, "
                + "COUNT(ac.key) FILTER (WHERE ac.permission = 'VIEW_METADATA') AS VIEW_METADATA "
                + "FROM thingzone.ACLs ac "
                + "WHERE ac.account_id = ? "
                + "GROUP BY ac.key ORDER BY ac.key");
        query.setParameter(1, account.getId());
        if (size > 0) {
            query.setFirstResult((page - 1) * size);
            query.setMaxResults(size);
        }

        List<Object[]> listPermissionsForKeys = query.getResultList();
        Map<String, KeyPermissions> keyMap = new HashMap<>();
        for (Object[] row : listPermissionsForKeys) {
            Map<String, Set<Permission>> accountPermissions = new TreeMap<>();
            TreeSet<Permission> permissionsSet = new TreeSet<Permission>();
            String key = (String) row[0];
            if ((long) row[1] >= 1) {
                permissionsSet.add(Permission.ADMIN);
            }
            if ((long) row[2] >= 1) {
                permissionsSet.add(Permission.UPLOAD);
            }
            if ((long) row[3] >= 1) {
                permissionsSet.add(Permission.CONSUME);
            }
            if ((long) row[3] >= 1) {
                permissionsSet.add(Permission.MODIFY);
            }
            if ((long) row[4] >= 1) {
                permissionsSet.add(Permission.ANNOTATE);
            }
            if ((long) row[5] >= 1) {
                permissionsSet.add(Permission.DISCOVER);
            }
            if ((long) row[6] >= 1) {
                permissionsSet.add(Permission.UPLOAD_EVENTS);
            }
            if ((long) row[8] >= 1) {
                permissionsSet.add(Permission.CONSUME_EVENTS);
            }
            if ((long) row[10] >= 1) {
                permissionsSet.add(Permission.VIEW_LOCATION);
            }
            if ((long) row[11] >= 1) {
                permissionsSet.add(Permission.VIEW_METADATA);
            }
            accountPermissions.put(name, permissionsSet);
            KeyPermissions permissions = new KeyPermissions(new DNSkey(key), null, null);
            if (account.getGroup() != null) {
                permissions = new KeyPermissions(new DNSkey(key), null, accountPermissions);
            } else if (account.getOwner() != null) {
                permissions = new KeyPermissions(new DNSkey(key), accountPermissions, null);
            }
            keyMap.put(key, permissions);
        }
        return keyMap;
    }

    @Override
    public boolean isAllowed(DNSkey key, List<Account> accounts, Permission permission) {
        checkTransaction();
        return isAllowed(key, accounts, ImmutableList.of(permission));
    }

    @Override
    public boolean isAllowed(DNSkey key, List<Account> accounts, List<Permission> permissions) {
        checkTransaction();
        if (accounts.isEmpty())
            return false;

        TypedQuery<ACL> query = getEntityManager().createNamedQuery("ACL.keyIsAllowed", getEntityClass());
        query.setParameter("key", key);
        query.setParameter("accounts", accounts);
        query.setParameter("perms", permissions);
        List<ACL> resultList = query.getResultList();
        if (!resultList.isEmpty()) {
            return true;
        }
        return false;
    }

    @Override
    public List<Permission> get(DNSkey key, List<Account> accounts) {
        Set<Permission> perms = new TreeSet<>();
        for (Account account : accounts) {
            perms.addAll(get(key, account));
        }
        return new ArrayList<>(perms);
    }

    @Override
    public List<DNSkey> getGranted(Account account, Permission permission) {
        List<DNSkey> dnsKeys = new ArrayList<>();
        for (ACL acl : findByFilter("account", account, "permission", permission)) {
            dnsKeys.add(acl.getKey());
        }
        return dnsKeys;
    }

    @Override
    public Map<AccountId, Set<Permission>> unmanagedGetByKey(DNSkey key) {
        Map<AccountId, Set<Permission>> permissions = new TreeMap<>();

        for (AclRow acl : unmanagedGetAclsByKey(key)) {
            if (!permissions.containsKey(acl.getAccountId())) {
                permissions.put(acl.getAccountId(), new TreeSet<Permission>());
            }
            permissions.get(acl.getAccountId()).add(acl.getPermission());
        }
        return permissions;
    }

    private List<AclRow> unmanagedGetAclsByKey(DNSkey key) {
        List<AclRow> acls = new ArrayList<>();
        try {
            String query = "SELECT id, key, account_id, permission FROM thingzone.acls WHERE key = ?";
            PreparedStatement st = getConnection().prepareStatement(query);
            st.setString(1, key.getKey());
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                acls.add(new AclRow(new AclId(rs.getLong("id")), new DNSkey(rs.getString("key")),
                                    new AccountId(rs.getLong("account_id")),
                                    Permission.fromString(rs.getString("permission"))));
            }
            st.close();
        } catch (SQLException e) {
            throw new ProgramDefectException("JDBC error", e);
        }
        return acls;
    }

    @Override
    public Map<AccountId, Set<Permission>> getByKey(DNSkey key) {
        Map<AccountId, Set<Permission>> permissions = new TreeMap<>();

        for (ACL acl : getAclsByKey(key)) {
            AccountId accountId = new AccountId(acl.getAccount().getId());
            if (!permissions.containsKey(accountId)) {
                permissions.put(accountId, new TreeSet<Permission>());
            }
            permissions.get(accountId).add(acl.getPermission());
        }
        return permissions;
    }

    private List<ACL> getAclsByKey(DNSkey key) {
        return findByFilter("key", key);
    }

    @Override
    public Map<String, Set<Permission>> unmanagedGroupPermissions(DNSkey key) {
        Map<String, Set<Permission>> permissions = new TreeMap<>();

        try {
            String query = "SELECT g.name, acl.permission " + "FROM thingzone.acls acl "
                + "JOIN thingzone.accounts a ON acl.account_id = a.id "
                + "JOIN thingzone.groups g ON g.id = a.group_id " + "WHERE acl.key = ?";
            PreparedStatement st = getConnection().prepareStatement(query);
            st.setString(1, key.getKey());
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                String username = rs.getString("name");
                Permission permission = Permission.valueOf(rs.getString("permission"));
                if (!permissions.containsKey(username)) {
                    permissions.put(username, new TreeSet<Permission>());
                }
                permissions.get(username).add(permission);
            }
            st.close();
        } catch (SQLException e) {
            throw new ProgramDefectException("JDBC error", e);
        }
        return permissions;
    }

    @Override
    public Map<String, Set<Permission>> unmanagedUserPermissions(DNSkey key) {
        Map<String, Set<Permission>> permissions = new TreeMap<>();

        try {
            String query = "SELECT uc.username, acl.permission " + "FROM thingzone.acls acl "
                + "JOIN thingzone.accounts a ON acl.account_id = a.id "
                + "JOIN thingzone.user_credentials uc ON uc.user_id = a.owner_id " + "WHERE acl.key = ?";
            PreparedStatement st = getConnection().prepareStatement(query);
            st.setString(1, key.getKey());
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                String username = rs.getString("username");
                Permission permission = Permission.valueOf(rs.getString("permission"));
                if (!permissions.containsKey(username)) {
                    permissions.put(username, new TreeSet<Permission>());
                }
                permissions.get(username).add(permission);
            }
            st.close();
        } catch (SQLException e) {
            throw new ProgramDefectException("JDBC error", e);
        }
        return permissions;
    }

    // ====================================================
    // Utility, internal

    ACL get(DNSkey key, Account account, Permission permission) {
        return loadByMatch("key", key, "account", account, "permission", permission);
    }

    /** to verify that DB constraint works */
    ACL createWithoutCheck(DNSkey key, Account account, Permission permission) {
        ACL acl = new ACL(key, account);
        acl.setPermission(permission);
        persist(acl);
        return acl;
    }

}
