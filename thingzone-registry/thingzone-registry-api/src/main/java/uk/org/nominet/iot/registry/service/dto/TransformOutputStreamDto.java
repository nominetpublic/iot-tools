/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.dto;

import java.util.List;
import java.util.stream.Collectors;

import uk.org.nominet.iot.registry.dao.dto.TransformOutputStreamRow;
import uk.org.nominet.iot.registry.model.TransformOutputStream;

public class TransformOutputStreamDto {
    public String key;
    public String alias;

    public TransformOutputStreamDto() {
    }

    public TransformOutputStreamDto(String alias, String streamKey) {
        this.alias = alias;
        this.key = streamKey;
    }

    static public List<TransformOutputStreamDto> toDto(List<TransformOutputStream> streams) {
        return streams.stream()
                      .map(s -> new TransformOutputStreamDto(s.getAlias(), s.getDatastream().getKey().getKey()))
                      .collect(Collectors.toList());
    }

    public static List<TransformOutputStreamDto> asListFromRow(List<TransformOutputStreamRow> tsrs) {
        if (tsrs == null)
            return null;
        return tsrs.stream().map(s -> new TransformOutputStreamDto(s.getAlias(), s.getStreamKey().getKey()))
                   .collect(Collectors.toList());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((alias == null) ? 0 : alias.hashCode());
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TransformOutputStreamDto other = (TransformOutputStreamDto) obj;
        if (alias == null) {
            if (other.alias != null)
                return false;
        } else if (!alias.equals(other.alias))
            return false;
        if (key == null) {
            if (other.key != null)
                return false;
        } else if (!key.equals(other.key))
            return false;
        return true;
    }
}
