/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import uk.org.nominet.iot.registry.dao.AbstractJpaDao;
import uk.org.nominet.iot.registry.dao.ZoneFileChangeDao;
import uk.org.nominet.iot.registry.database.DatabaseDriver;
import uk.org.nominet.iot.registry.model.ZoneFileChange;
import uk.org.nominet.iot.registry.model.ZoneFileChangeOperation;
import uk.org.nominet.iot.registry.service.dto.DNSdataDto;

class ZoneFileChangeDaoImpl extends AbstractJpaDao<ZoneFileChange> implements ZoneFileChangeDao {

    ZoneFileChangeDaoImpl() {
        super(ZoneFileChange.class);
    }

    @Override
    public ZoneFileChange createRecord(ZoneFileChangeOperation operation, DNSdataDto ddr) {
        Long transaction = generateTransaction();

        ZoneFileChange zfc = new ZoneFileChange(transaction, operation, ddr);
        persist(zfc);
        // flush(); // no need to get the Id populated; just used for PK
        return zfc;
    }

    // uses ID sequence from dummy table "ZoneFileChangesTransactions"
    private final static String TRANSACTION_SEQUENCE_SQL = "select nextval('" + DatabaseDriver.SCHEMA_NAME
        + ".zonefilechangestransactions_id_seq')";

    private Long generateTransaction() {
        Query q = getEntityManager().createNativeQuery(TRANSACTION_SEQUENCE_SQL);
        return (Long) q.getSingleResult();
    }

    @Override
    public List<ZoneFileChange> getUnprocessedChanges(int limit) {
        checkTransaction();
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<ZoneFileChange> criteriaQuery = criteriaBuilder.createQuery(getEntityClass());
        Root<ZoneFileChange> model = criteriaQuery.from(getEntityClass());
        // "isNull"
        criteriaQuery.where(model.get("serial").isNull());
        // "orderBy" - ordering by timestamp and then transaction
        criteriaQuery.orderBy(criteriaBuilder.asc(model.get("timestamp")),
            criteriaBuilder.asc(model.get("transaction")));
        TypedQuery<ZoneFileChange> typedQuery = getEntityManager().createQuery(criteriaQuery);
        // "limit"
        typedQuery.setFirstResult(0);
        typedQuery.setMaxResults(limit);
        return typedQuery.getResultList();
    }

    @Override
    public void setSerial(ZoneFileChange change, long serial) {
        change.setSerial(serial);
    }

}
