/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service;

import java.util.ArrayList;
import java.util.List;

class DifferenceCalculator<T> {
    public enum Type {
        REMOVE,
        ADD
    }

    class Difference {
        Type difference;
        T value;

        Difference(Type differenceType, T value) {
            this.difference = differenceType;
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o)
                return true;
            if (o == null || getClass() != o.getClass())
                return false;
            @SuppressWarnings("unchecked")
            Difference that = (Difference) o;
            if (difference != that.difference)
                return false;
            return value.equals(that.value);
        }

        @Override
        public int hashCode() {
            int result = difference.hashCode();
            result = 31 * result + value.hashCode();
            return result;
        }
    }

    // returns a list of changes needed to "comparison", to make it like "reference"
    List<Difference> calculate(List<T> reference, List<T> comparison) {
        List<T> myReference = new ArrayList<>(reference);
        List<T> myComparison = new ArrayList<>(comparison);

        List<Difference> diffs = new ArrayList<>();

        while (true) {
            T omission = omits(myReference, myComparison);
            if (omission == null)
                break;
            diffs.add(new Difference(Type.ADD, omission));
            myComparison.add(omission);
        }

        while (true) {
            T omission = omits(myComparison, myReference);
            if (omission == null)
                break;
            diffs.add(new Difference(Type.REMOVE, omission));
            myComparison.remove(omission);
        }

        return diffs;
    }

    private T omits(List<T> reference, List<T> comparison) {
        for (T p : reference) {
            if (!comparison.contains(p))
                return p;
        }
        return null;
    }
}
