/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.common;

import com.google.common.base.CharMatcher;

/**
 * Validates a user-supplied key against our rules
 *
 */
public class UserKeyValidator {

    public static boolean validate(String key) {

        CharMatcher ch = CharMatcher.inRange('a', 'z').or(CharMatcher.inRange('0', '9')).or(CharMatcher.is('-'))
                                    .or(CharMatcher.is('.'));
        if (!ch.matchesAllOf(key)) {
            return false;
        }

        // no leading/trailing "-" or "." & no doubles
        if (key.charAt(0) == '-' || key.charAt(key.length() - 1) == '-' || key.charAt(0) == '.'
            || key.charAt(key.length() - 1) == '.') {
            return false;
        }
        if (key.contains("--") || key.contains("src/test") || key.contains(".-") || key.contains("-.")) {
            return false;
        }

        // OK
        return true;
    }

}
