/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao;

import java.sql.Connection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.eclipse.persistence.jpa.JpaHelper;
import org.eclipse.persistence.queries.CursoredStream;
import org.eclipse.persistence.queries.ReadAllQuery;

import uk.nominet.iot.utils.error.ProgramDefectException;
import uk.org.nominet.iot.registry.database.DatabaseDriver;
import uk.org.nominet.iot.registry.database.DbObjectStreamer;

public class AbstractJpaDao<T> implements JpaDao<T> {

    private static final int CURSOR_INITIAL_READ_SIZE = 10;
    private static final int CURSOR_PAGE_SIZE = 10;

    private Class<T> entityClass;

    public AbstractJpaDao(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    @Override
    public Class<T> getEntityClass() {
        return entityClass;
    }

    @Override
    public List<T> findAll() {
        checkTransaction();
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(getEntityClass());
        Root<T> model = criteriaQuery.from(getEntityClass());
        criteriaQuery.select(model);
        TypedQuery<T> typedQuery = getEntityManager().createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

    @Override
    public T findById(Long id) {
        checkTransaction();
        return getEntityManager().find(getEntityClass(), id);
    }

    @Override
    public void deleteById(Long id) {
        checkTransaction();
        T result = getEntityManager().find(getEntityClass(), id);
        getEntityManager().remove(result);
    }

    @Override
    public void persist(T entity) {
        checkTransaction();
        getEntityManager().persist(entity);
    }

    @Override
    public void detach(T entity) {
        checkTransaction();
        getEntityManager().detach(entity);
    }

    @Override
    public void remove(T entity) {
        checkTransaction();
        getEntityManager().remove(entity);
    }

    @Override
    public void flush() {
        checkTransaction();
        getEntityManager().flush();
    }

    public EntityManager getEntityManager() {
        return DatabaseDriver.getEntityManager();
    }

    protected Connection getConnection() {
        checkTransaction();
        return getEntityManager().unwrap(java.sql.Connection.class);
    }

    /**
     * load a single matching entity (throwing NoResultException on error) this
     * looks like a lot of code, but we guess that it's faster than parsing JPQL
     */
    public T loadByMatch(String entityName, Object entityValue) {
        checkTransaction();
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(getEntityClass());
        Root<T> model = criteriaQuery.from(getEntityClass());
        criteriaQuery.where(criteriaBuilder.equal(model.get(entityName), entityValue));
        TypedQuery<T> typedQuery = getEntityManager().createQuery(criteriaQuery);
        return typedQuery.getSingleResult();
    }

    public T loadByMatch(String entityName1, Object entityValue1, String entityName2, Object entityValue2) {
        checkTransaction();
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(getEntityClass());
        Root<T> model = criteriaQuery.from(getEntityClass());
        criteriaQuery.where(criteriaBuilder.and(criteriaBuilder.equal(model.get(entityName1), entityValue1),
            criteriaBuilder.equal(model.get(entityName2), entityValue2)));
        TypedQuery<T> typedQuery = getEntityManager().createQuery(criteriaQuery);
        return typedQuery.getSingleResult();
    }

    public T loadByMatch(String entityName1, Object entityValue1, String entityName2, Object entityValue2,
                         String entityName3, Object entityValue3) {
        checkTransaction();
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(getEntityClass());
        Root<T> model = criteriaQuery.from(getEntityClass());
        criteriaQuery.where(criteriaBuilder.and(criteriaBuilder.equal(model.get(entityName1), entityValue1),
            criteriaBuilder.equal(model.get(entityName2), entityValue2),
            criteriaBuilder.equal(model.get(entityName3), entityValue3)));
        TypedQuery<T> typedQuery = getEntityManager().createQuery(criteriaQuery);
        return typedQuery.getSingleResult();
    }

    public T loadByMatch(String entityName1, Object entityValue1, String entityName2, Object entityValue2,
                         String entityName3, Object entityValue3, String entityName4, Object entityValue4) {
        checkTransaction();
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(getEntityClass());
        Root<T> model = criteriaQuery.from(getEntityClass());
        criteriaQuery.where(criteriaBuilder.and(criteriaBuilder.equal(model.get(entityName1), entityValue1),
            criteriaBuilder.equal(model.get(entityName2), entityValue2),
            criteriaBuilder.equal(model.get(entityName3), entityValue3),
            criteriaBuilder.equal(model.get(entityName4), entityValue4)));
        TypedQuery<T> typedQuery = getEntityManager().createQuery(criteriaQuery);
        return typedQuery.getSingleResult();
    }

    /**
     * load multiple matching entities
     */
    public List<T> findByFilter(String filterName, Object filterValue) {
        checkTransaction();
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(getEntityClass());
        Root<T> model = criteriaQuery.from(getEntityClass());
        criteriaQuery.where(criteriaBuilder.equal(model.get(filterName), filterValue));
        TypedQuery<T> typedQuery = getEntityManager().createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

    public List<T> findByFilter(String filterName1, Object filterValue1, String filterName2, Object filterValue2) {
        checkTransaction();
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(getEntityClass());
        Root<T> model = criteriaQuery.from(getEntityClass());
        criteriaQuery.where(criteriaBuilder.and(criteriaBuilder.equal(model.get(filterName1), filterValue1),
            criteriaBuilder.equal(model.get(filterName2), filterValue2)));
        TypedQuery<T> typedQuery = getEntityManager().createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

    /** delete matching entity */
    public void deleteByMatch(String entityName, Object entityValue) {
        checkTransaction();
        getEntityManager().remove(loadByMatch(entityName, entityValue));
    }

    public void deleteEntity(T entity) {
        checkTransaction();
        getEntityManager().remove(entity);
    }

    public void checkTransaction() {
        if (!getEntityManager().isJoinedToTransaction())
            throw new ProgramDefectException("no DB transaction");
        if (!getEntityManager().getTransaction().isActive())
            throw new ProgramDefectException("no active DB transaction");
    }

    public DbObjectStreamer<T> streamAll() {
        checkTransaction();

        // NB EclipseLink specific functionality
        ReadAllQuery raq = new ReadAllQuery(getEntityClass());
        // we want the results as a stream
        raq.useCursoredStream(CURSOR_INITIAL_READ_SIZE, CURSOR_PAGE_SIZE);
        // don't cache the result (we're doing a table scan
        raq.doNotCacheQueryResults();
        // need to join query & entity (& not obvious how; use JpaHelper)
        Query query = JpaHelper.createQuery(raq, getEntityManager());
        // single result is a cursor/stream
        CursoredStream stream = (CursoredStream) query.getSingleResult();
        // wrap up for typing
        return new DbObjectStreamer<>(stream);
    }

    public List<T> getAll() {
        checkTransaction();
        CriteriaQuery<T> criteria = getEntityManager().getCriteriaBuilder().createQuery(getEntityClass());
        criteria.select(criteria.from(getEntityClass()));
        return getEntityManager().createQuery(criteria).getResultList();
    }

}
