/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service;

import java.util.List;

import uk.org.nominet.iot.registry.dao.ACLDao;
import uk.org.nominet.iot.registry.dao.DNSkeyDao;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;

/**
 * Access control layer service
 */
public class ACLService {
    ACLDao aclDao;
    DNSkeyDao dnsKeyDao;

    public ACLService() {
        DaoFactory df = DaoFactoryManager.getFactory();
        aclDao = df.aclDao();
        dnsKeyDao = df.dnsKeyDao();
    }

    void setOwnerPrivileges(Account account, DNSkey key) {
        // owner permission
        aclDao.create(key, account, Permission.ADMIN);
        // subsidiary permissions
        aclDao.create(key, account, Permission.UPLOAD);
        aclDao.create(key, account, Permission.CONSUME);
        aclDao.create(key, account, Permission.MODIFY);
        aclDao.create(key, account, Permission.ANNOTATE);
        aclDao.create(key, account, Permission.DISCOVER);
    }

    boolean isAllowed(SessionPermissionAssistant spa, DNSkey dnsKey, Permission permission) {
        return aclDao.isAllowed(dnsKey, spa.getRelevantAccounts(), permission);
    }

    boolean isAllowed(SessionPermissionAssistant spa, DNSkey dnsKey, List<Permission> permissions) {
        return aclDao.isAllowed(dnsKey, spa.getRelevantAccounts(), permissions);
    }

    boolean isAllowed(SessionPermissionAssistant spa, String key, Permission permission) {
        DNSkey dnsKey = dnsKeyDao.getByKey(key);
        return isAllowed(spa, dnsKey, permission);
    }

    void removeForDelete(SessionPermissionAssistant spa, DNSkey key) {
        aclDao.removeAll(key);
    }

}
