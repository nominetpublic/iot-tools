/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.dto;

import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.TransformSource;

@SuppressWarnings("unused")
public class TransformSourceRow {
    private TransformSourceId transformSourceId;
    private TransformId transformId; // FK
    private DNSkey sourceStreamKey;
    private Boolean trigger;
    private String alias;
    private String aggregationType;
    private String aggregationSpec;

    public TransformSourceRow(TransformSourceId transformSourceId,
                              TransformId transformId,
                              DNSkey sourceStreamKey,
                              Boolean trigger,
                              String alias,
                              String aggregationType,
                              String aggregationSpec) {
        this.transformSourceId = transformSourceId;
        this.transformId = transformId;
        this.sourceStreamKey = sourceStreamKey;
        this.trigger = trigger;
        this.alias = alias;
        this.aggregationType = aggregationType;
        this.aggregationSpec = aggregationSpec;
    }

    public TransformSourceRow(TransformSource transformSource) {
        this.transformSourceId = new TransformSourceId(transformSource.getId());
        this.sourceStreamKey = transformSource.getSourceStream().getKey();
        this.trigger = transformSource.getTrigger();
        this.alias = transformSource.getAlias();
        this.aggregationType = transformSource.getAggregationType();
        this.aggregationSpec = transformSource.getAggregationSpec();
    }

    public TransformSourceId getTransformSourceId() {
        return transformSourceId;
    }

    public TransformId getTransformId() {
        return transformId;
    }

    public DNSkey getSourceStreamKey() {
        return sourceStreamKey;
    }

    public Boolean getTrigger() {
        return trigger;
    }

    public String getAlias() {
        return alias;
    }

    public String getAggregationType() {
        return aggregationType;
    }

    public String getAggregationSpec() {
        return aggregationSpec;
    }
}
