/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;

import uk.org.nominet.iot.registry.dao.dto.AccountId;
import uk.org.nominet.iot.registry.dao.dto.KeyPermissions;
import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.ACL;
import uk.org.nominet.iot.registry.model.Permission;

public interface ACLDao extends JpaDao<ACL> {

    ACL create(DNSkey key, Account account, Permission permission);

    boolean exists(DNSkey key, Account account, Permission permission);

    void remove(DNSkey key, Account account, Permission permission);

    void removeAll(DNSkey key);

    List<Permission> get(DNSkey key, Account account);

    List<Permission> get(DNSkey key, List<Account> accounts);

    List<Account> get(DNSkey key, Permission permission);

    List<DNSkey> getGranted(Account account, Permission permission);

    boolean isAllowed(DNSkey key, List<Account> accounts, Permission permission);

    boolean isAllowed(DNSkey key, List<Account> accounts, List<Permission> permission);

    Map<String, Set<Permission>> listGroupPermissions(DNSkey key, String groupFilter);

    Map<String, Set<Permission>> listUserPermissions(DNSkey key, String usernameFilter);

    KeyPermissions listAllPermissions(DNSkey key);

    Map<String, Set<Permission>> unmanagedGroupPermissions(DNSkey key);

    Map<String, Set<Permission>> unmanagedUserPermissions(DNSkey key);

    Map<AccountId, Set<Permission>> getByKey(DNSkey key);

    Map<AccountId, Set<Permission>> unmanagedGetByKey(DNSkey key);

    Map<String, KeyPermissions> listPermissionsForAccount(Account account, String name, int size, int page);

}
