/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.AbstractJpaDao;
import uk.org.nominet.iot.registry.dao.GroupDao;
import uk.org.nominet.iot.registry.model.Group;
import uk.org.nominet.iot.registry.model.User;

public class GroupDaoImpl extends AbstractJpaDao<Group> implements GroupDao {
    private final static Logger log = LoggerFactory.getLogger(GroupDaoImpl.class);

    GroupDaoImpl() {
        super(Group.class);
    }

    @Override
    public Group create(User owner, String name) {
        Group group = new Group(owner, name);
        persist(group);
        flush(); // get the Id populated
        log.info("created group with ID={}", group.getId());
        return group;
    }

    @Override
    public Group getByName(String groupName) {
        try {
            return loadByName(groupName);
        } catch (NoResultException e) {
            throw new DataException("Group not found", Status.NOT_FOUND);
        }
    }

    @Override
    public boolean exists(String username) {
        try {
            loadByName(username);
            return true;
        } catch (PersistenceException e) {
            if (e instanceof NoResultException) {
                return false;
            } else {
                throw e;
            }
        }
    }

    @Override
    public List<Group> listOwnedGroupsByUser(User user) {
        return findByFilter("owner", user);
    }

    @Override
    public List<Group> listGroupsByNames(List<String> names) {
        checkTransaction();

        TypedQuery<Group> query = getEntityManager().createNamedQuery("Group.findGroupsByNames", getEntityClass());
        query.setParameter("names", names);
        return query.getResultList();
    }

    private Group loadByName(String name) {
        return loadByMatch("name", name);
    }
}
