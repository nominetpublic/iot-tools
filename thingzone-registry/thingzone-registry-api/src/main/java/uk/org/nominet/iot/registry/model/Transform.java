/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import java.util.List;

import javax.persistence.*;

import com.google.common.base.Strings;
import org.eclipse.persistence.annotations.Customizer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonRawValue;

import uk.org.nominet.iot.registry.database.ColumnPosition;
import uk.org.nominet.iot.registry.database.ColumnPositionCustomizer;
import uk.org.nominet.iot.registry.database.DatabaseDriver;
import uk.org.nominet.iot.registry.database.HistoryListener;

/**
 * Transform Streams
 *
 * A TransformStream instance receives data from at least 1 data stream, and is
 * updated (i.e. recalculated) when that data stream is updated, using the
 * linked TransformFunction. Parameters (static) can also be supplied to the
 * transformation.
 */
@Entity
@EntityListeners(HistoryListener.class)
@Table(name = "Transforms", schema = DatabaseDriver.SCHEMA_NAME)
@Customizer(ColumnPositionCustomizer.class)

@NamedQueries({ @NamedQuery(name = "Transforms.getWithRunSchedule",
                            query = "SELECT transform" + " FROM Transform transform"
                                + " WHERE transform.runSchedule != '' "),
                @NamedQuery(name = "Transforms.getAllowed",
                            query = "SELECT DISTINCT transform FROM Transform transform, ACL ac"
                                + " WHERE ac.account in :accounts" + " AND transform.dataStream.key = ac.key"
                                + " AND ac.permission in :perms") })

public class Transform {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ColumnPosition(position = 1)
    @JsonIgnore
    private Long id;

    @JoinColumn(name = "outputKey", referencedColumnName = "DNSkey", nullable = false)
    @ColumnPosition(position = 2)
    private DataStream dataStream;

    @JoinColumn(name = "functionId", referencedColumnName = "ID", nullable = false)
    @ColumnPosition(position = 3)
    private TransformFunction function;

    @Column(name = "parameter_values", columnDefinition = "JSONB NOT NULL", nullable = false) // empty="{}"
    @Convert(converter = JsonbStringConverter.class) // our custom (to/from String) converter
    @ColumnPosition(position = 4)
    @JsonRawValue
    private String parameterValues;

    @Column(columnDefinition = "BOOLEAN NOT NULL DEFAULT FALSE", nullable = false)
    @ColumnPosition(position = 5)
    private boolean multiplexer;

    @Column(columnDefinition = "character varying(255) DEFAULT ''", nullable = false)
    @ColumnPosition(position = 6)
    private String runSchedule;

    @OneToMany(mappedBy = "transform")
    private List<TransformOutputStream> outputStreams;

    public Transform(DataStream outputStream, TransformFunction function, String parameterValues, String runSchedule) {
        this.dataStream = outputStream;
        this.function = function;
        this.parameterValues = parameterValues;
        this.multiplexer = false;
        this.runSchedule = runSchedule;
    }

    Transform() {}

    @Override
    public String toString() {
        return "Transform{" + "id=" + id + ", dataStream=" + dataStream.getKey().getKey() + ", function="
            + function.toString() + ", parameterValues='" + parameterValues + ", runSchedule=" + runSchedule + '}';
    }

    public Long getId() {
        return id;
    }

    public DataStream getDataStream() {
        return dataStream;
    }

    public void setFunction(TransformFunction function) {
        this.function = function;
    }

    public TransformFunction getFunction() {
        return function;
    }

    public void setParameterValues(String parameterValues) {
        this.parameterValues = parameterValues;
    }

    public String getParameterValues() {
        return parameterValues;
    }

    public boolean getMultiplexer() {
        return multiplexer;
    }

    public void setRunSchedule(String runSchedule) {
        this.runSchedule = Strings.isNullOrEmpty(runSchedule) ? "" : runSchedule;
    }

    public String getRunSchedule() {
        return runSchedule;
    }

    public List<TransformOutputStream> getOutputStreams() {
        return outputStreams;
    }

}
