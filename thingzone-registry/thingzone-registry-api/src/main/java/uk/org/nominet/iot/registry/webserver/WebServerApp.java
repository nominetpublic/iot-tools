/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Strings;

import uk.org.nominet.iot.registry.database.*;

import java.lang.invoke.MethodHandles;
import java.util.TimeZone;

public class WebServerApp {
    private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public static void main(String[] args) {
        log.info("running Web server App...");

        try {
            WebServerApp app = new WebServerApp();
            app.run(args);
        } catch (Throwable e) {
            // we need exception info to be logged, not just printed to stderr
            log.error("exception thrown from main():", e);
        } finally {
            DatabaseDriver.globalDispose();
        }
    }

    private void run(String[] args) throws Exception {
        // Make sure the JVM is running in UTC
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

        // start & check database
        DatabaseApp.initialiseDatabase();
        DatabaseChecker.checkConnection();
        DatabaseChecker.checkMincStatus();

        // configure & run webserver
        initialiseWebServerConfig();
        runWebServer();
    }

    private static void runWebServer() throws Exception {
        WebServer.webMain();
    }

    public static void initialiseWebServerConfig() {
        String webPropertiesFile = System.getProperty("web.properties");
        if (Strings.isNullOrEmpty(webPropertiesFile)) {
            log.info("no web.properties specified; using default");
            webPropertiesFile = "config/web.properties";
        }

        // web config
        uk.org.nominet.iot.registry.webserver.ConfigFactory.loadConfig(webPropertiesFile);
    }

}
