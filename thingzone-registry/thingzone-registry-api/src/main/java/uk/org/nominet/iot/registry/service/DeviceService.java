/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.NoResultException;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

import uk.org.nominet.iot.registry.common.AuthException;
import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.DeviceDao;
import uk.org.nominet.iot.registry.dao.StreamOnDeviceDao;
import uk.org.nominet.iot.registry.dao.dto.KeyPermissions;
import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.Device;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.model.Privilege;
import uk.org.nominet.iot.registry.model.StreamOnDevice;
import uk.org.nominet.iot.registry.service.dto.DNSdataDto;
import uk.org.nominet.iot.registry.service.dto.DataStreamSummaryDto;
import uk.org.nominet.iot.registry.service.dto.DeviceSummaryDetailDto;
import uk.org.nominet.iot.registry.webserver.ConfigFactory;
import uk.org.nominet.iot.registry.webserver.KeyGenerationType;
import uk.org.nominet.iot.registry.webserver.api.support.EntityLoader;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;

/**
 * Device Service
 */
public class DeviceService {
    private final static Logger log = LoggerFactory.getLogger(DeviceService.class);

    private DeviceDao deviceDao;
    private StreamOnDeviceDao sodDao;

    private ACLService aclService;
    private UserPrivilegeService userPrivilegeService;
    private DNSdataService dnsDataService;
    private PermissionService permissionService;

    private final Privilege PRIVILEGE_REQUIRED_FOR_CREATE = Privilege.CREATE_DEVICE;

    public DeviceService() {
        DaoFactory df = DaoFactoryManager.getFactory();
        deviceDao = df.deviceDao();
        sodDao = df.streamOnDeviceDao();

        aclService = new ACLService();
        userPrivilegeService = new UserPrivilegeService();
        dnsDataService = new DNSdataService();
        permissionService = new PermissionService();
    }

    // DAO wrappers extracted to service, so that Dao isn't called by handler...
    public List<Device> getVisibleTo(SessionPermissionAssistant spa) {
        return getAllowed(spa, Collections.singletonList(Permission.DISCOVER));
    }

    public List<Device> getAllowed(SessionPermissionAssistant spa, List<Permission> perms) {
        return deviceDao.getAllowed(spa.getRelevantAccounts(), perms);
    }

    public Device create(SessionPermissionAssistant spa,
                         boolean externallyNamed,
                         String key,
                         String name,
                         String type) {
        if (!userPrivilegeService.canCreate(spa, PRIVILEGE_REQUIRED_FOR_CREATE)) {
            throw new AuthException(spa, "need privilege " + PRIVILEGE_REQUIRED_FOR_CREATE);
        }

        Device device;
        if (!externallyNamed) {
            // user didn't specify a key
            device = deviceDao.create();
        } else {
            // user did specify a key:
            if (ConfigFactory.getConfig().getKeyGenerationType() != KeyGenerationType.EXTERNAL) {
                throw new DataException("external key generation is not configured");
            }
            device = deviceDao.createKnownKey(key);
        }
        DNSkey dnsKey = device.getKey();
        if (!Strings.isNullOrEmpty(name)) {
            dnsKey.setName(name);
        }
        if (!Strings.isNullOrEmpty(type)) {
            dnsKey.setType(type);
        }

        // create & attach "owner" ACL
        Account account = spa.getUserAccount();
        aclService.setOwnerPrivileges(account, dnsKey);

        return device;
    }

    public Device upsert(SessionPermissionAssistant spa, String key, boolean externallyNamed) {
        if (deviceDao.exists(key)) {
            return getDevice(spa, key);
        } else {
            // if name or type are set then they will be updated later as part of the upsert
            return create(spa, externallyNamed, key, null, null);
        }
    }

    public Device getDevice(SessionPermissionAssistant spa, String deviceKey) {
        return EntityLoader.getDevice(spa, deviceKey, Permission.DISCOVER);
    }

    public boolean exists(String key) {
        return deviceDao.exists(key);
    }

    public void addDataStream(SessionPermissionAssistant spa, String deviceKey, String dataStreamKey, String sodName) {
        Device device = EntityLoader.getDevice(spa, deviceKey, Permission.MODIFY);
        DataStream dataStream = EntityLoader.getDataStream(spa, dataStreamKey, Permission.DISCOVER);

        sodDao.addDataStream(device, dataStream, sodName);
    }

    public void removeDataStream(SessionPermissionAssistant spa, String deviceKey, String dataStreamKey) {
        Device device = EntityLoader.getDevice(spa, deviceKey, Permission.MODIFY);

        // it's OK not to check permissions on a DataStream that we're removing from a
        // device
        DataStream dataStream = EntityLoader.getDataStreamWithoutCheckingPermission(dataStreamKey);
        try {
            sodDao.removeDataStream(device, dataStream);
        } catch (NoResultException e) {
            throw new DataException("DataStream not associated with Device", Status.NOT_FOUND);
        }
    }

    public List<DataStream> getDataStreams(Device device) {
        return sodDao.getDataStreams(device);
    }

    public DeviceSummaryDetailDto getDeviceSummaryDetail(SessionPermissionAssistant spa,
                                                         String deviceKey,
                                                         boolean withPermissions) {
        Device device = EntityLoader.getDevice(spa, deviceKey, Permission.DISCOVER);
        return getDeviceSummaryDetail(spa, device, withPermissions);
    }

    private DeviceSummaryDetailDto getDeviceSummaryDetail(SessionPermissionAssistant spa,
                                                          Device device,
                                                          boolean showPermissions) {
        // we allow retrieval of streams, with DISCOVER permission or QUERY_STREAMS
        // privilege
        boolean canQueryStreams = userPrivilegeService.canQueryStreams(spa);
        // get child streams
        List<DataStreamSummaryDto> streamDtos = new ArrayList<>();
        for (StreamOnDevice streamOnDevice : sodDao.findByDevice(device)) {
            DataStream dataStream = streamOnDevice.getDataStream();
            // only display streams that the user has permission to see
            DNSkey dsKey = dataStream.getKey();
            if (canQueryStreams || aclService.isAllowed(spa, dsKey, Permission.DISCOVER)) {
                List<DNSdataDto> streamDns = DNSdataService.asDto(dnsDataService.dnsDataListByKey(spa, dsKey));
                KeyPermissions datastreamPermissions = new KeyPermissions(dsKey, null, null);
                if (showPermissions) {
                    if (canQueryStreams || aclService.isAllowed(spa, dsKey, Permission.ADMIN)) {
                        datastreamPermissions = permissionService.listAllPermissionsForKey(spa, dsKey.getKey());
                    }
                }

                String streamMetadata = null;
                if (canQueryStreams || aclService.isAllowed(spa, device.getKey(),
                    ImmutableList.of(Permission.ADMIN, Permission.MODIFY, Permission.VIEW_METADATA))) {
                    streamMetadata = dataStream.getMetadata();
                }

                streamDtos.add(DataStreamSummaryDto.streamLink(dataStream, streamMetadata, streamDns,
                    datastreamPermissions.getUserPermissions(), datastreamPermissions.getGroupPermissions(),
                    streamOnDevice.getName()));
            }
        }

        // get DNSdata for the device
        List<DNSdataDto> dnsDataDtos = null;
        if (aclService.isAllowed(spa, device.getKey(), Permission.DISCOVER)) {
            dnsDataDtos = DNSdataService.asDto(dnsDataService.dnsDataListByKey(spa, device.getKey()));
        }

        KeyPermissions devicePermissions = new KeyPermissions(device.getKey(), null, null);
        if (showPermissions) {
            if (aclService.isAllowed(spa, device.getKey(), Permission.ADMIN)) {
                devicePermissions = permissionService.listAllPermissionsForKey(spa, device.getKey().getKey());
            }
        }

        String location = null;
        if (aclService.isAllowed(spa, device.getKey(),
            ImmutableList.of(Permission.ADMIN, Permission.MODIFY, Permission.VIEW_LOCATION))) {
            location = device.getLocation();
        }

        String metadata = null;
        if (aclService.isAllowed(spa, device.getKey(),
            ImmutableList.of(Permission.ADMIN, Permission.MODIFY, Permission.VIEW_METADATA))) {
            metadata = device.getMetadata();
        }

        return new DeviceSummaryDetailDto(device.getKey(), metadata, location, streamDtos, dnsDataDtos, null,
                                          devicePermissions.getUserPermissions(),
                                          devicePermissions.getGroupPermissions());
    }

    public DeviceSummaryDetailDto getSimpleDeviceSummary(SessionPermissionAssistant spa, Device device) {
        String location = null;
        if (aclService.isAllowed(spa, device.getKey(),
            ImmutableList.of(Permission.ADMIN, Permission.MODIFY, Permission.VIEW_LOCATION))) {
            location = device.getLocation();
        }

        String metadata = null;
        if (aclService.isAllowed(spa, device.getKey(),
            ImmutableList.of(Permission.ADMIN, Permission.MODIFY, Permission.VIEW_METADATA))) {
            metadata = device.getMetadata();
        }

        return new DeviceSummaryDetailDto(device.getKey(), metadata, location, ImmutableList.of(), ImmutableList.of(),
                                          null, null, null);
    }

    public DeviceSummaryDetailDto getDeviceSummary(SessionPermissionAssistant spa, Device device) {
        // we allow retrieval of streams, with DISCOVER permission or QUERY_STREAMS
        // privilege
        boolean canQueryStreams = userPrivilegeService.canQueryStreams(spa);
        // get child streams
        // List<DataStreamSummaryDto> streamDtos = new ArrayList<>();
        List<DataStreamSummaryDto> visibleStreams = new ArrayList<>();
        for (StreamOnDevice streamOnDevice : sodDao.findByDevice(device)) {
            // only display those that we can access
            if (canQueryStreams
                || aclService.isAllowed(spa, streamOnDevice.getDataStream().getKey(), Permission.DISCOVER)) {
                visibleStreams.add(DataStreamSummaryDto.basicStreamLink(streamOnDevice.getDataStream().getKey(),
                    streamOnDevice.getName()));
            }
        }

        // get DNSdata for the device
        List<DNSdataDto> dnsDataDtos = null;
        if (aclService.isAllowed(spa, device.getKey(), Permission.DISCOVER)) {
            dnsDataDtos = DNSdataService.asDto(dnsDataService.dnsDataListByKey(spa, device.getKey()));
        }

        String location = null;
        if (aclService.isAllowed(spa, device.getKey(),
            ImmutableList.of(Permission.ADMIN, Permission.MODIFY, Permission.VIEW_LOCATION))) {
            location = device.getLocation();
        }

        String metadata = null;
        if (aclService.isAllowed(spa, device.getKey(),
            ImmutableList.of(Permission.ADMIN, Permission.MODIFY, Permission.VIEW_METADATA))) {
            metadata = device.getMetadata();
        }

        return DeviceSummaryDetailDto.basic(device.getKey(), metadata, location, visibleStreams, dnsDataDtos);
    }

    /**
     * set metadata - requires MODIFY permission
     */
    public void setMetadata(SessionPermissionAssistant spa, String deviceKey, String metadata) {
        Device device = EntityLoader.getDevice(spa, deviceKey, Permission.MODIFY);
        device.setMetadata(metadata);
    }

    /**
     * set location - requires MODIFY permission
     */
    public void setLocation(SessionPermissionAssistant spa, String deviceKey, String location) {
        Device device = EntityLoader.getDevice(spa, deviceKey, Permission.MODIFY);
        String setLocation = location.isEmpty() ? null : location;
        device.setLocation(setLocation);
    }

    /**
     * delete - requires ADMIN permission
     */
    public void delete(SessionPermissionAssistant spa, String deviceKey) {
        Device device = EntityLoader.getDevice(spa, deviceKey, Permission.ADMIN);
        // delete DNS records
        dnsDataService.removeAllRecords(device.getKey());
        // delete device itself
        deviceDao.delete(device);
        // & remove ACLs
        aclService.removeForDelete(spa, device.getKey());
        log.info("deleted device {}", deviceKey);
    }

    public List<StreamOnDevice> getDataStreamsAttachedToDevice(Device device) {
        return sodDao.findByDevice(device);
    }

}
