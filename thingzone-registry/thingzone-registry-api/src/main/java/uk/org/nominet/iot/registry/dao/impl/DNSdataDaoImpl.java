/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.ws.rs.core.Response.Status;

import com.google.common.collect.ImmutableList;

import uk.nominet.iot.utils.error.ProgramDefectException;
import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.AbstractJpaDao;
import uk.org.nominet.iot.registry.dao.DNSdataDao;
import uk.org.nominet.iot.registry.model.DNSdata;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.service.dto.DNSdataDto;

class DNSdataDaoImpl extends AbstractJpaDao<DNSdata> implements DNSdataDao {

    DNSdataDaoImpl() {
        super(DNSdata.class);
    }

    @Override
    public DNSdata create(DNSdataDto ddr) {

        if (exists(ddr)) {
            throw new DataException("DNSdata exists already", Status.CONFLICT);
        }

        DNSdata dnsData = new DNSdata(ddr);
        persist(dnsData);
        // flush(); // no need to get the Id populated; just used for PK
        return dnsData;
    }

    @Override
    public boolean exists(DNSdataDto ddr) {
        try {
            get(ddr);
            return true;
        } catch (NoResultException e) {
            return false;
        } catch (Throwable e) {
            throw e;
        }
    }

    @Override
    public DNSdata get(DNSdataDto ddr) {
        return loadByMatch("key", ddr.getDnsKey(), "subdomain", ddr.getSubdomain(), "rrtype", ddr.getRrtype(), "rdata",
            ddr.getRdata());
    }

    @Override
    public void delete(DNSdata dnsData) {
        deleteEntity(dnsData);
    }

    @Override
    public List<DNSdata> listByKey(DNSkey key) {
        return ImmutableList.copyOf(findByFilter("key", key));
    }

    @Override
    public List<DNSdata> unmanagedListByKey(DNSkey key) {
        List<DNSdata> dnsDatas = new ArrayList<>();

        try {
            String query = "SELECT DNSkey, description, subdomain, TTL, rrtype, rdata FROM thingzone.DNSdata WHERE DNSkey = ?";
            PreparedStatement st = getConnection().prepareStatement(query);
            st.setString(1, key.getKey());
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                dnsDatas.add(new DNSdata(
                    new DNSdataDto(
                        new DNSkey(rs.getString("DNSkey")),
                                   rs.getString("description"),
                                   rs.getString("subdomain"),
                                   rs.getInt("TTL"),
                                   rs.getString("rrtype"),
                                   rs.getString("rdata"))));
            }
            st.close();
        } catch (SQLException e) {
            throw new ProgramDefectException("JDBC error", e);
        }
        return dnsDatas;
    }

    @Override
    public List<DNSdata> listAll() {
        return ImmutableList.copyOf(findAll());
    }

}
