/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;

import uk.org.nominet.iot.registry.database.ColumnPosition;
import uk.org.nominet.iot.registry.database.DatabaseDriver;
import uk.org.nominet.iot.registry.database.HistoryListener;

@Entity
@EntityListeners(HistoryListener.class)
@Table(name = "DNSkeys", schema = DatabaseDriver.SCHEMA_NAME,
       uniqueConstraints = { @UniqueConstraint(name = "DNSkeys_UQ_KEY", columnNames = { "DNSkey" }), })
@NamedQueries({ @NamedQuery(name = "DNSkeys.getAllowed",
                            query = "SELECT DISTINCT k FROM ACL ac, DNSkey k WHERE ac.key = k AND ac.account in :accounts "
                                + "AND ac.permission in :perms AND k.status <> :status"),
                @NamedQuery(name = "DNSkeys.findByType",
                            query = "SELECT DISTINCT dk FROM DNSkey dk JOIN ACL ac WHERE dk = ac.key AND "
                                + "ac.account in :accounts AND ac.permission in :perms AND dk.entity_type = :type"), })
public class DNSkey {
    @Id // primary key (& hence not null)
    @ColumnPosition(position = 1)
    @Column(name = "DNSkey", length = DatabaseDriver.DNSKEY_LENGTH)
    private String key;

    @Column(name = "entity_type", nullable = false)
    @ColumnPosition(position = 2)
    private String entity_type;

    @Column(name = "name", nullable = false)
    @ColumnPosition(position = 3)
    private String name;

    @JsonIgnore
    @Column(name = "status", columnDefinition = DatabaseDriver.SCHEMA_NAME + ".ENTITY_STATUS_TYPE", nullable = false)
    @Convert(converter = EntityStatus.EntityStatusConverter.class)
    @ColumnPosition(position = 4)
    private EntityStatus status;

    @OneToMany(mappedBy = "key")
    private List<ACL> permissions;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "key")
    private Device device;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "key")
    private DataStream dataStream;

    DNSkey() {}

    public DNSkey(String key) {
        this.key = key;
        this.entity_type = "";
        this.name = "";
        this.status = EntityStatus.KEY;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getType() {
        return entity_type;
    }

    public void setType(String type) {
        this.entity_type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EntityStatus getStatus() {
        return status;
    }

    public void setStatus(EntityStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "DNSkey [key=" + key + ", entity_type=" + entity_type + ", name=" + name + ", status=" + status + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DNSkey other = (DNSkey) obj;
        if (key == null) {
            if (other.key != null)
                return false;
        } else if (!key.equals(other.key))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        result = prime * result + ((entity_type == null) ? 0 : entity_type.hashCode());
        return result;
    }

}
