/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;

import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.org.nominet.iot.registry.webserver.api.support.JsonErrorResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSingleResult;

@Path("/test")
public class TestHandler {
    private final static Logger log = LoggerFactory.getLogger(DataStreamHandler.class);

    @Context
    Request request;

    @Context
    private HttpServletRequest hsr;

    public void dump() {
        log.info("dumping request:");
        log.info("====================");
        log.info("method={}", hsr.getMethod());
        log.info("getContextPath={}", hsr.getContextPath());
        log.info("getPathInfo={}", hsr.getPathInfo());
        log.info("getQueryString={}", hsr.getQueryString());

        log.info("getCharacterEncoding={}", hsr.getCharacterEncoding());
        log.info("getContentLength={}", hsr.getContentLength());
        log.info("getContentType={}", hsr.getContentType());

        log.info("getLocalAddr={}", hsr.getLocalAddr());
        log.info("getLocalPort={}", hsr.getLocalPort());

        log.info("getProtocol={}", hsr.getProtocol());

        log.info("headers:");
        Enumeration<String> headerNames = hsr.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String header = headerNames.nextElement();
            log.info("header: {}={}", header, hsr.getHeader(header));
        }

        log.info("parameters:");
        Enumeration<String> parameterNames = hsr.getParameterNames();
        boolean gotParams = false;
        while (parameterNames.hasMoreElements()) {
            String parameter = parameterNames.nextElement();
            log.info("parameter: {}={}", parameter, hsr.getParameterValues(parameter));
            gotParams = true;
        }
        if (!gotParams)
            log.info("(no params)");
    }

    String sanitise(byte[] buf, int sz) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < sz; ++i) {
            byte b = buf[i];
            if (b > 32 && b < 127) {
                sb.append(new String(new byte[] { b }));
            } else {
                sb.append(String.format("{x%02x}", b));
            }
        }
        return sb.toString();
    }

    // consume stream content
    // - you can only do this once!
    String receiveStreamContent(HttpServletRequest hsr) {
        log.info("receiveStreamContent");
        try {
            InputStream is = hsr.getInputStream();
            byte buf[] = new byte[10];
            int bytes_read;
            while ((bytes_read = is.read(buf)) > 0) {
                log.info("inputStream len {} contents=\"{}\"", bytes_read, sanitise(buf, bytes_read));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "aaah";
    }

    @SuppressWarnings("unchecked")
    static String dumpLinkedHashMap(final Map<String, Object> m, String... offset) {
        String retval = "";
        String delta = offset.length == 0 ? "" : offset[0];
        for (Map.Entry<String, Object> e : m.entrySet()) {
            retval += delta + "[" + e.getKey() + "] -> ";
            Object value = e.getValue();
            if (value instanceof Map) {
                retval += "(Hash)\n" + dumpLinkedHashMap((Map<String, Object>) value, delta + "  ");
            } else if (value instanceof List) {
                retval += "{";
                for (Object element : (List<Object>) value) {
                    retval += element + ", ";
                }
                retval += "}\n";
            } else {
                retval += "[" + value.toString() + "]\n";
            }
        }
        return retval + "\n";
    }

    /** receives JSON but as a String (not deserialised), for insertion to DB */
    @POST
    @Path("rx_raw_json")
    @Produces(MediaType.APPLICATION_JSON)
    public Object rx_raw_json(String s) {
        dump();
        return new JsonErrorResult("playground");
    }

    public static class SimpleObject {
        public String stringVal;
        public int intVal;
    }

    /** receives JSON as a typed object */
    @POST
    @Path("rx_json_typed_object")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Object rx_json_typed_object(SimpleObject so) {
        dump();

        log.info("so.stringVal={}", so.stringVal);
        log.info("so.intVal={}", so.intVal);

        LinkedHashMap<String, Object> resultContainer = new LinkedHashMap<String, Object>();
        resultContainer.put("typed object", so.stringVal);

        return new JsonSingleResult("received", resultContainer).output();
    }

    /** receives JSON as untyped object */
    @POST
    @Path("rx_json_object")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Object rx_json_object(LinkedHashMap<String, Object> o) {
        dump();

        log.info("o class={}", o.getClass());
        log.info("o contents=...\n{}", dumpLinkedHashMap(o));

        LinkedHashMap<String, Object> resultContainer = new LinkedHashMap<String, Object>();
        resultContainer.put("untyped object", "??");

        return new JsonSingleResult("received", resultContainer).output();
    }

    /** receives JSON as text */
    @POST
    @Path("rx_json_text")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Object rx_json_text() {
        dump();

        String content = receiveStreamContent(hsr);
        log.info("stream content={}", content);

        LinkedHashMap<String, Object> resultContainer = new LinkedHashMap<String, Object>();
        resultContainer.put("text", "??");

        return new JsonSingleResult("received", resultContainer).output();
    }

    /** multipart; receives JSON as text */
    @POST
    @Path("multipart")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Object multipart(@FormDataParam("name") String s1, @FormDataParam("json") String s2) {
        dump();

        log.info("s1={}", s1);
        log.info("s2={}", s2);

        LinkedHashMap<String, Object> resultContainer = new LinkedHashMap<String, Object>();
        resultContainer.put("multipart", "??");

        return new JsonSingleResult("received", resultContainer).output();
    }

    /** returns JSON as text */
    @GET
    @Path("tx_json")
    @Produces(MediaType.APPLICATION_JSON)
    public String tx_json() {
        return "{\"stringVal\":\"forty two\",\"intVal\":42}";
    }

}
