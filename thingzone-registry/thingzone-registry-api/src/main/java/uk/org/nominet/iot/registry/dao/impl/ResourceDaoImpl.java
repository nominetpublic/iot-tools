/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.AbstractJpaDao;
import uk.org.nominet.iot.registry.dao.ResourceDao;
import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.model.Resource;

public class ResourceDaoImpl extends AbstractJpaDao<Resource> implements ResourceDao {
    private final static Logger log = LoggerFactory.getLogger(ResourceDaoImpl.class);

    ResourceDaoImpl() {
        super(Resource.class);
    }

    @Override
    public Resource createKnownKey(String key, String name, String type, String contentType, byte[] content) {
        DNSkey dnskey = new DNSkeyDaoImpl().getByKey(key);
        Resource resource = new Resource(dnskey, name, type, contentType, content);
        create(resource); // will throw an exception if the key does not exist
        return resource;
    }

    Resource create(Resource resource) {
        // check the key is already created
        if (exists(resource.getKey().getKey(), resource.getName())) {
            throw new DataException("Resource \"" + resource.getKey().getKey() + "\", \"" + resource.getName()
                + "\" already exists", Status.CONFLICT);
        }
        persist(resource);
        log.info("created resource: {}", resource);
        return resource;
    }

    @Override
    public Resource find(String key, String name) {
        return loadByKeyName(key, name);
    }

    private Resource loadByKeyName(String key, String name) {
        DNSkey dnsKey = new DNSkey(key);
        return loadByMatch("key", dnsKey, "name", name);
    }

    @Override
    public void delete(Resource resource) {
        deleteEntity(resource);
    }

    @Override
    public List<Resource> listByKey(String key, List<Account> accounts, List<Permission> permissions) {
        checkTransaction();
        DNSkey dnsKey = new DNSkey(key);

        TypedQuery<Resource> query = getEntityManager().createNamedQuery("Resources.listByKey", getEntityClass());
        query.setParameter("key", dnsKey);
        query.setParameter("accounts", accounts);
        query.setParameter("perms", permissions);
        return query.getResultList();
    }

    @Override
    public List<Resource> listByKey(String key, List<Account> accounts, Permission permission) {
        return listByKey(key, accounts, ImmutableList.of(permission));
    }

    @Override
    public List<Resource> getAllowed(List<Account> accounts, List<Permission> permissions) {
        checkTransaction();

        TypedQuery<Resource> query = getEntityManager().createNamedQuery("Resources.getAllowed", getEntityClass());
        query.setParameter("accounts", accounts);
        query.setParameter("perms", permissions);
        return query.getResultList();
    }

    @Override
    public List<Resource> getAllowed(List<Account> accounts, Permission permission) {
        return getAllowed(accounts, ImmutableList.of(permission));
    }

    @Override
    public boolean exists(String key, String name) {
        try {
            loadByKeyName(key, name);
            return true;
        } catch (PersistenceException e) {
            if (e instanceof NoResultException) {
                return false;
            } else {
                throw e;
            }
        }
    }

    @Override
    public List<Resource> listByType(List<Account> accounts,
                                     List<Permission> permissions,
                                     String type,
                                     String contentType) {
        checkTransaction();

        TypedQuery<Resource> query = getEntityManager().createNamedQuery("Resources.listByEntityType",
            getEntityClass());
        query.setParameter("accounts", accounts);
        query.setParameter("perms", permissions);
        query.setParameter("type", type);
        if (contentType == null) {
            return query.getResultList().stream().collect(Collectors.toList());
        }
        return query.getResultList().stream().filter(resource -> resource.getContentType().equals(contentType))
                    .collect(Collectors.toList());
    }

}
