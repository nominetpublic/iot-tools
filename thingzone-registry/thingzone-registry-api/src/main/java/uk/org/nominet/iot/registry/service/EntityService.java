/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.persistence.NoResultException;
import javax.ws.rs.core.Response.Status;

import org.postgresql.util.PGobject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

import uk.org.nominet.iot.registry.common.AuthException;
import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.AccountDao;
import uk.org.nominet.iot.registry.dao.DNSkeyDao;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.Device;
import uk.org.nominet.iot.registry.model.EntityStatus;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.model.Privilege;
import uk.org.nominet.iot.registry.service.dto.DataStreamSummaryDto;
import uk.org.nominet.iot.registry.service.dto.DeviceSummaryDetailDto;
import uk.org.nominet.iot.registry.service.dto.DnsKeyDetailDto;
import uk.org.nominet.iot.registry.webserver.ConfigFactory;
import uk.org.nominet.iot.registry.webserver.KeyGenerationType;
import uk.org.nominet.iot.registry.webserver.api.support.EntityLoader;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;

/**
 * Entity service
 */
public class EntityService {
    private final static Logger log = LoggerFactory.getLogger(EntityService.class);

    private DNSkeyDao dnsKeyDao;
    private ACLService aclService;
    private AccountDao accountDao;
    private UserPrivilegeService userPrivilegeService;

    private final Privilege PRIVILEGE_REQUIRED_FOR_CREATE = Privilege.CREATE_ENTITY;

    public EntityService() {
        DaoFactory df = DaoFactoryManager.getFactory();
        dnsKeyDao = df.dnsKeyDao();
        accountDao = df.accountDao();
        aclService = new ACLService();
        userPrivilegeService = new UserPrivilegeService();
    }

    public DNSkey createKey(SessionPermissionAssistant spa, boolean externallyNamed, String key) {
        if (!userPrivilegeService.canCreate(spa, PRIVILEGE_REQUIRED_FOR_CREATE)) {
            throw new AuthException(spa, "need privilege " + PRIVILEGE_REQUIRED_FOR_CREATE);
        }

        DNSkey dnsKey;
        if (!externallyNamed) {
            // user didn't specify a key
            dnsKey = dnsKeyDao.create();
        } else {
            // user did specify a key:
            if (ConfigFactory.getConfig().getKeyGenerationType() != KeyGenerationType.EXTERNAL) {
                throw new DataException("external key generation is not configured");
            }
            dnsKey = dnsKeyDao.createKnownKey(key);
        }
        // create & attach "owner" ACL
        Account account = spa.getUserAccount();
        aclService.setOwnerPrivileges(account, dnsKey);

        return dnsKey;
    }

    /**
     * return true if the key exists and is not deleted
     */
    public boolean isAlive(String key) {
        return dnsKeyDao.isAlive(key);
    }

    /**
     * Update the values for the entity record
     *
     * @param spa the session
     * @param key the key of the entity to update
     * @param type the entity type or null to not update
     * @param name the name of the entity or null to not update
     */
    public void setValues(SessionPermissionAssistant spa, String key, String type, String name) {
        DNSkey dnsKey = EntityLoader.getDnsKey(spa, key, Permission.MODIFY);
        setValues(dnsKey, type, name);
    }

    /**
     * Update the values for the entity record if values are not null
     */
    public void setValues(DNSkey dnsKey, String type, String name) {
        if (type != null)
            dnsKey.setType(type);
        if (name != null)
            dnsKey.setName(name);
    }

    public List<DNSkey> getByType(SessionPermissionAssistant spa, String type, int size, int page) {
        if (type == null) {
            return dnsKeyDao.getAllowed(spa.getRelevantAccounts(), ImmutableList.of(Permission.DISCOVER), size, page);
        }
        return dnsKeyDao.getByType(spa.getRelevantAccounts(), type, size, page);
    }

    public List<Object> listAdminEntities(SessionPermissionAssistant spa,
                                          String type,
                                          String name,
                                          boolean withPermissions,
                                          String username,
                                          String groupname,
                                          int size,
                                          int page) {
        List<Object> entities = new ArrayList<>();
        if (withPermissions) {
            Account account;
            boolean isUser = true;
            if (!Strings.isNullOrEmpty(username)) {
                try {
                    account = this.accountDao.getByUsername(username);
                } catch (NoResultException e) {
                    throw new DataException("No user found with name '" + username + "'", Status.NOT_FOUND);
                }
            } else {
                try {
                    account = this.accountDao.getByGroupname(groupname);
                } catch (NoResultException e) {
                    throw new DataException("No group found with name '" + groupname + "'", Status.NOT_FOUND);
                }
                isUser = false;
            }
            List<Object[]> listEntitiesRelated = dnsKeyDao.listEntitiesRelatedWithPermissions(spa.getRelevantAccounts(),
                ImmutableList.of(Permission.ADMIN), type, name, account, size, page);
            for (Object[] row : listEntitiesRelated) {
                String key = (String) row[0];
                String keyType = (String) row[1];
                String keyName = (String) row[2];
                String keyStatus = (String) row[3];
                Map<String, Set<Permission>> keyPermissions = new TreeMap<>();
                TreeSet<Permission> permissionsSet = new TreeSet<Permission>();
                if (row[12] != null && (long) row[12] >= 1) {
                    permissionsSet.add(Permission.ADMIN);
                }
                if (row[12] != null && (long) row[13] >= 1) {
                    permissionsSet.add(Permission.UPLOAD);
                }
                if (row[12] != null && (long) row[14] >= 1) {
                    permissionsSet.add(Permission.CONSUME);
                }
                if (row[12] != null && (long) row[15] >= 1) {
                    permissionsSet.add(Permission.MODIFY);
                }
                if (row[12] != null && (long) row[16] >= 1) {
                    permissionsSet.add(Permission.ANNOTATE);
                }
                if (row[12] != null && (long) row[17] >= 1) {
                    permissionsSet.add(Permission.DISCOVER);
                }
                if (row[12] != null && (long) row[18] >= 1) {
                    permissionsSet.add(Permission.UPLOAD_EVENTS);
                }
                if (row[12] != null && (long) row[19] >= 1) {
                    permissionsSet.add(Permission.CONSUME_EVENTS);
                }
                if (row[12] != null && (long) row[20] >= 1) {
                    permissionsSet.add(Permission.VIEW_LOCATION);
                }
                if (row[12] != null && (long) row[21] >= 1) {
                    permissionsSet.add(Permission.VIEW_METADATA);
                }
                DnsKeyDetailDto entity;
                if (isUser) {
                    keyPermissions.put(username, permissionsSet);
                    entity = new DnsKeyDetailDto(key, keyType, keyName, keyStatus, keyPermissions, null);
                } else {
                    keyPermissions.put(groupname, permissionsSet);
                    entity = new DnsKeyDetailDto(key, keyType, keyName, keyStatus, null, keyPermissions);
                }
                String deviceKey = (String) row[4];
                String datastreamKey = (String) row[7];
                if (!Strings.isNullOrEmpty(deviceKey)) {
                    String metadata = row[5] != null ? ((PGobject) row[5]).toString() : null;
                    String location = row[6] != null ? ((PGobject) row[6]).toString() : null;
                    entities.add(new DeviceSummaryDetailDto(entity, metadata, location));
                } else if (!Strings.isNullOrEmpty(datastreamKey)) {
                    String metadata = row[8] != null ? ((PGobject) row[8]).toString() : null;
                    Boolean isTransform = (Boolean) row[9];
                    entities.add(new DataStreamSummaryDto(entity, metadata, isTransform));
                } else {
                    entities.add(entity);
                }
            }
            return entities;
        }
        List<Object[]> listEntitiesRelated = dnsKeyDao.listEntitiesRelated(spa.getRelevantAccounts(),
            ImmutableList.of(Permission.ADMIN), type, name, size, page);

        for (Object[] row : listEntitiesRelated) {
            DNSkey dnsKey = (DNSkey) row[0];
            if (row[2] != null) {
                DataStream stream = (DataStream) row[2];
                entities.add(DataStreamSummaryDto.basic(stream, stream.getMetadata(), null));
            } else if (row[1] != null) {
                Device device = (Device) row[1];
                entities.add(
                    DeviceSummaryDetailDto.basic(dnsKey, device.getMetadata(), device.getLocation(), null, null));
            } else {
                entities.add(DnsKeyDetailDto.basic(dnsKey));
            }
        }
        return entities;
    }

    /**
     * delete - requires ADMIN permission, only works if entity is not a device or dataStream
     */
    public void delete(SessionPermissionAssistant spa, String key) {
        DNSkey entity = EntityLoader.getDnsKey(spa, key, Permission.ADMIN);
        if (entity.getStatus() == EntityStatus.KEY) {
            dnsKeyDao.delete(entity);
            // remove ACLs
            aclService.removeForDelete(spa, entity);

            log.info("deleted entity {}", entity);
        } else {
            throw new DataException("This key is a device or a datastream", Status.FORBIDDEN);
        }
    }
}
