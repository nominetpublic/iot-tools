/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.AbstractJpaDao;
import uk.org.nominet.iot.registry.dao.PushSubscriberDao;
import uk.org.nominet.iot.registry.dao.dto.PushSubscriberId;
import uk.org.nominet.iot.registry.dao.dto.PushSubscriberRow;
import uk.org.nominet.iot.registry.model.*;
import uk.nominet.iot.utils.error.ProgramDefectException;

class PushSubscriberDaoImpl extends AbstractJpaDao<PushSubscriber> implements PushSubscriberDao {
    private final static Logger log = LoggerFactory.getLogger(PushSubscriberDaoImpl.class);

    PushSubscriberDaoImpl() {
        super(PushSubscriber.class);
    }

    @Override
    public PushSubscriber create(DataStream dataStream,
                                 String uri,
                                 String uriParams,
                                 String headers,
                                 String method,
                                 String retrySequence) {
        if (exists(dataStream, uri)) {
            throw new DataException("PullSubscription exists already for DataStream \"" + dataStream.getKey() + "\"",
                                    Status.CONFLICT);
        }

        PushSubscriber ps = new PushSubscriber(dataStream, uri, uriParams, headers, method, retrySequence);
        persist(ps);
        // (no flush() - not required since we don't use Id)
        log.info("created PushSubscriber for DataStream={}, URI={}", dataStream.getKey(), uri);
        return ps;
    }

    @Override
    public boolean exists(DataStream dataStream, String uri) {
        try {
            get(dataStream, uri);
            return true;
        } catch (PersistenceException e) {
            if (e instanceof NoResultException) {
                return false;
            } else {
                throw e;
            }
        }
    }

    void delete(PushSubscriber pushSubscriber) {
        deleteEntity(pushSubscriber);
    }

    @Override
    public void delete(DataStream dataStream, String uri) {
        delete(get(dataStream, uri));
    }

    @Override
    public List<PushSubscriber> getForStream(DataStream dataStream) {
        return findByFilter("dataStream", dataStream);
    }

    @Override
    public PushSubscriber get(DataStream dataStream, String uri) {
        return loadByMatch("dataStream", dataStream, "uri", uri);
    }

    @Override
    public List<PushSubscriber> getAllowed(List<Account> accounts, List<Permission> perms) {
        checkTransaction();

        TypedQuery<PushSubscriber> query = getEntityManager().createQuery(
            "SELECT DISTINCT ps FROM PushSubscriber ps, ACL ac"
                + " WHERE ac.account in :accounts AND ps.dataStream.key = ac.key" + " AND ac.permission in :perms",
            getEntityClass());
        query.setParameter("accounts", accounts);
        query.setParameter("perms", perms);

        return query.getResultList();
    }

    @Override
    public List<PushSubscriber> getAllowed(List<Account> accounts, Permission permission) {
        return getAllowed(accounts, Collections.singletonList(permission));
    }

    @Override
    public List<PushSubscriber> getAll() {

        TypedQuery<PushSubscriber> query = getEntityManager().createQuery(
            "SELECT DISTINCT ps FROM PushSubscriber ps, ACL ac" + " WHERE ps.dataStream.key = ac.key",
            getEntityClass());

        return query.getResultList();
    }

    @Override
    public List<PushSubscriberRow> unmanagedGetForStream(DataStream dataStream) {
        List<PushSubscriberRow> pss = new ArrayList<>();
        try {
            String query = "SELECT id, streamKey, uri, uri_params FROM thingzone.PushSubscribers WHERE streamKey = ?";
            PreparedStatement st = getConnection().prepareStatement(query);
            st.setString(1, dataStream.getKey().getKey());
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                pss.add(
                    new PushSubscriberRow(new PushSubscriberId(rs.getLong("id")),
                                          new DNSkey(rs.getString("streamKey")),
                                          rs.getString("uri"), rs.getString("uri_params")));
            }
            st.close();
        } catch (SQLException e) {
            throw new ProgramDefectException("JDBC error", e);
        }
        return pss;
    }

}
