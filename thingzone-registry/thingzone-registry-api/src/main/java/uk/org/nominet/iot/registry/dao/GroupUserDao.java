/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao;

import java.util.List;

import uk.org.nominet.iot.registry.model.Group;
import uk.org.nominet.iot.registry.model.GroupUser;
import uk.org.nominet.iot.registry.model.User;

public interface GroupUserDao extends JpaDao<GroupUser> {

    GroupUser create(Group group, User user, boolean isAdmin);

    void remove(Group group, User user);

    boolean exists(Group group, User user);

    boolean isAdmin(Group group, User user);

    void setAdmin(Group group, User user, boolean setAdmin);

    List<Group> listGroupsByUser(User user);

    List<User> listUsersByGroup(Group group);
}
