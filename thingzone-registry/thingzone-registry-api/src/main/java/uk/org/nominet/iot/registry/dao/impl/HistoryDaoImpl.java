/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import java.time.Instant;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.eclipse.persistence.jpa.JpaHelper;
import org.eclipse.persistence.queries.CursoredStream;
import org.eclipse.persistence.queries.ReadAllQuery;

import com.google.common.collect.ImmutableList;

import uk.org.nominet.iot.registry.dao.AbstractJpaDao;
import uk.org.nominet.iot.registry.dao.HistoryDao;
import uk.org.nominet.iot.registry.database.DbObjectStreamer;
import uk.org.nominet.iot.registry.model.History;
import uk.org.nominet.iot.registry.model.HistoryAction;
import uk.org.nominet.iot.registry.model.ObjectType;

public class HistoryDaoImpl extends AbstractJpaDao<History> implements HistoryDao {

    private static final int CURSOR_INITIAL_READ_SIZE = 10;
    private static final int CURSOR_PAGE_SIZE = 10;

    HistoryDaoImpl() {
        super(History.class);
    }

    @Override
    public History create(String key, Date updated, ObjectType objectType, HistoryAction action) {
        History history = new History();
        history.setKey(key);
        history.setUpdated(Date.from(Instant.now()));
        history.setObjectType(objectType);
        history.setAction(action);
        return history;
    }

    @Override
    public List<History> listByKey(String key) {
        return findByFilter("key", key);
    }

    @Override
    public List<History> listByKeyAction(String key, HistoryAction action) {
        return findByFilter("key", key, "action", action);
    }

    @Override
    public DbObjectStreamer<History> streamFrom(Date from) {
        checkTransaction();

        ReadAllQuery raq = new ReadAllQuery(History.class);
        raq.setJPQLString("SELECT history FROM History history WHERE history.updated > :date ORDER BY history.key");
        raq.setArguments(ImmutableList.of("date"));

        raq.useCursoredStream(CURSOR_INITIAL_READ_SIZE, CURSOR_PAGE_SIZE);
        raq.doNotCacheQueryResults();

        // need to join query & entity (& not obvious how; use JpaHelper)
        Query query = JpaHelper.createQuery(raq, getEntityManager());
        query.setParameter("date", from, TemporalType.TIMESTAMP);
        // single result is a cursor/stream
        CursoredStream stream = (CursoredStream) query.getSingleResult();
        // wrap up for typing
        return new DbObjectStreamer<>(stream);
    }
}
