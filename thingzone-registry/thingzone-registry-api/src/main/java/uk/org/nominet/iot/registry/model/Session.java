/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.eclipse.persistence.annotations.Customizer;

import uk.org.nominet.iot.registry.database.ColumnPosition;
import uk.org.nominet.iot.registry.database.ColumnPositionCustomizer;
import uk.org.nominet.iot.registry.database.DatabaseDriver;

/**
 * DB "Sessions" table
 *
 * Deliberately not linked to Users / UserCredentials, because we want fast
 * access to this table, and it will need periodic clean-up, which can be
 * decoupled from the rest of the system..
 */

@Entity
@Table(name = "Sessions", schema = DatabaseDriver.SCHEMA_NAME,
       uniqueConstraints = { @UniqueConstraint(name = "UQ_SESSION", columnNames = { "sessionKey" }), })
@Customizer(ColumnPositionCustomizer.class)
public class Session implements GenericSession {
    @Id // primary key (& hence not null)
    @Column(name = "sessionKey")
    @ColumnPosition(position = 1)
    private String sessionKey;

    // deliberately not a FK
    @Column(name = "username", nullable = false)
    @ColumnPosition(position = 2)
    private String username;

    @Column(name = "ipAddr", nullable = false)
    @ColumnPosition(position = 3)
    private String ipAddr;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false)
    @ColumnPosition(position = 4)
    private Date created;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "refreshed", nullable = false)
    @ColumnPosition(position = 5)
    private Date refreshed;

    @Column(name = "closed", nullable = false)
    @ColumnPosition(position = 6)
    private boolean closed;

    Session() {}

    public Session(String sessionKey, String username, String ipAddr) {
        this.sessionKey = sessionKey;
        this.username = username;
        this.ipAddr = ipAddr;
        Date now = new Date();
        created = now;
        refreshed = now;
        closed = false;
    }

    @Override
    public String toString() {
        return "Session{" + "sessionKey=" + sessionKey + ", username='" + username + '\'' + ", ipAddr='" + ipAddr
            + ", created='" + created + '\'' + ", refreshed='" + refreshed + '\'' + ", closed=" + closed + '}';
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isLoggedIn() {
        return true; // real user - has logged in
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public String getIpAddr() {
        return ipAddr;
    }

    public int getTimeSinceCreated() {
        Date now = new Date();
        long age_ms = now.getTime() - created.getTime();
        return (int) (age_ms);
    }

    public int getTimeSinceRefreshed() {
        if (refreshed == null)
            return Integer.MAX_VALUE;
        Date now = new Date();
        long age_ms = now.getTime() - refreshed.getTime();
        return (int) (age_ms);
    }

    public void refresh() {
        refreshed = new Date();
    }

    public void close() {
        closed = true;
    }

    public boolean isClosed() {
        return closed;
    }

    // for unit tests
    public void TESTsetRefreshed(Date refreshed) {
        this.refreshed = refreshed;
    }

    public void TESTsetCreated(Date created) {
        this.created = created;
    }
}
