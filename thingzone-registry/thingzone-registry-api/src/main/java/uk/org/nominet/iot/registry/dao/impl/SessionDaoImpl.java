/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import java.util.Optional;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.common.SessionKeyGenerator;
import uk.org.nominet.iot.registry.dao.AbstractJpaDao;
import uk.org.nominet.iot.registry.dao.SessionDao;
import uk.org.nominet.iot.registry.model.Session;
import uk.org.nominet.iot.registry.webserver.ConfigFactory;
import uk.nominet.iot.utils.error.ProgramDefectException;

class SessionDaoImpl extends AbstractJpaDao<Session> implements SessionDao {
    private final static Logger log = LoggerFactory.getLogger(SessionDaoImpl.class);

    // minimum period before refreshing session (to save DB writes)
    private final static int MINIMUM_REFRESH_PERIOD_MS = 1000;

    SessionDaoImpl() {
        super(Session.class);
    }

    @Override
    public Session open(String username, String srcAddr) {
        String key = generateSessionKey();
        Session session = new Session(key, username, srcAddr);
        persist(session);
        return session;
    }

    @Override
    public Session find(String sessionKey) {
        return find(sessionKey, false);
    }

    @Override
    public Session findByReference(String sessionKey) {
        return find(sessionKey, true);
    }

    private Session find(String sessionKey, boolean usedAsReference) {
        // for logging/error messages, distinguish between normal login session
        // and sessions being used by reference (i.e. from internal API)
        String sessionDescription = usedAsReference ? "reference session" : "session";

        Optional<Session> sessionOption = findIfExists(sessionKey);
        if (!sessionOption.isPresent()) {
            throw new DataException(sessionDescription + " " + sessionKey + " not found", Status.UNAUTHORIZED);
        }

        Session session = sessionOption.get();

        // check for closure
        if (session.isClosed()) {
            log.warn("{} {} is closed", sessionDescription, sessionKey);
            throw new DataException(sessionDescription + " is closed", Status.UNAUTHORIZED);
        }

        // check for expiry
        if (getTimeRemaining(session) < 0) {
            log.warn("{} {} has expired", sessionDescription, sessionKey);
            throw new DataException(sessionDescription + " has expired", Status.UNAUTHORIZED);
        }

        return session;
    }

    @Override
    public void refresh(Session session) {
        if (session.getTimeSinceRefreshed() < MINIMUM_REFRESH_PERIOD_MS) {
            // don't bother refreshing unless older than minumum (save a few DB writes)
            log.info(String.format("not bothering to refresh session %s (%.3fs since refresh)", session.getSessionKey(),
                (float) session.getTimeSinceRefreshed() / 1000));
        } else {
            log.info(String.format("refreshing session %s (%.3fs since refresh)", session.getSessionKey(),
                (float) session.getTimeSinceRefreshed() / 1000));

            // update time & return
            session.refresh();
        }
    }

    @Override
    public void close(Session session) {
        session.close();
    }

    @Override
    public void detach(Session session) {
        flush();
        super.detach(session);
    }

    @Override
    public int getTimeRemaining(Session session) {
        return ConfigFactory.getConfig().getSessionTimeout() - session.getTimeSinceRefreshed();
    }

    @Override
    public void cleanOldSessions(int expiryPeriod) {
        throw new ProgramDefectException("TODO");
    }

    private String generateSessionKey() {
        String key;
        while (true) {
            key = SessionKeyGenerator.generateKey();

            // check for _any_ DNSkey existing
            if (!exists(key))
                return key;
            // we have a duplicate; try again
            log.warn("sessionKey collision with key \"{}\"", key);
        }
    }

    private Session findMatch(String sessionKey) {
        return loadByMatch("sessionKey", sessionKey);
    }

    @Override
    public boolean exists(String sessionKey) {
        Optional<Session> sessionOption = findIfExists(sessionKey);
        return sessionOption.isPresent();
    }

    private Optional<Session> findIfExists(String sessionKey) {
        try {
            Session session = findMatch(sessionKey);
            return Optional.of(session);
        } catch (PersistenceException e) {
            if (e instanceof NoResultException) {
                return Optional.empty();
            } else {
                throw e;
            }
        }
    }

}
