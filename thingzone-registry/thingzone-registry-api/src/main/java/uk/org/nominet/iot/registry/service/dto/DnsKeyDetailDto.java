/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.dto;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import uk.org.nominet.iot.registry.model.DNSdata;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.EntityStatus;
import uk.org.nominet.iot.registry.model.Permission;

public class DnsKeyDetailDto {
    public String key;

    @JsonInclude(Include.NON_EMPTY)
    public String type;

    @JsonInclude(Include.NON_EMPTY)
    public String name;

    public EntityStatus entityType;

    @JsonInclude(Include.NON_EMPTY)
    public List<DNSdataDto> dnsRecords;

    @JsonInclude(Include.NON_NULL)
    public Map<String, Set<Permission>> userPermissions;

    @JsonInclude(Include.NON_NULL)
    public Map<String, Set<Permission>> groupPermissions;

    private DnsKeyDetailDto(DNSkey dnsKey, List<DNSdataDto> dnsRecords, Map<String, Set<Permission>> userPermissions,
                            Map<String, Set<Permission>> groupPermissions) {
        this.key = dnsKey.getKey();
        this.type = dnsKey.getType();
        this.name = dnsKey.getName();
        this.entityType = dnsKey.getStatus();
        this.dnsRecords = dnsRecords;
        this.userPermissions = userPermissions;
        this.groupPermissions = groupPermissions;
    }

    public DnsKeyDetailDto(String key,
                           String type,
                           String name,
                           String entityType,
                           Map<String, Set<Permission>> userPermissions,
                           Map<String, Set<Permission>> groupPermissions) {
        this.key = key;
        this.type = type;
        this.name = name;
        this.entityType = (entityType != null) ? EntityStatus.fromString(entityType): null;
        this.userPermissions = userPermissions;
        this.groupPermissions = groupPermissions;
    }

    public static DnsKeyDetailDto withRecords(DNSkey dnsKey, List<DNSdata> dnsRecords) {
        List<DNSdataDto> dnsRecordOutput = null;
        if (dnsRecords != null && !dnsRecords.isEmpty()) {
            dnsRecordOutput = dnsRecords.stream().map(d -> new DNSdataDto(d)).collect(Collectors.toList());
        }
        return new DnsKeyDetailDto(dnsKey, dnsRecordOutput, null, null);
    }

    public static DnsKeyDetailDto basic(DNSkey dnsKey) {
        return new DnsKeyDetailDto(dnsKey, null, null, null);
    }

    public static DnsKeyDetailDto detail(DNSkey dnsKey,
                                         List<DNSdataDto> dnsRecords,
                                         Map<String, Set<Permission>> userPermissions,
                                         Map<String, Set<Permission>> groupPermissions) {
        return new DnsKeyDetailDto(dnsKey, dnsRecords, userPermissions, groupPermissions);
    }
}
