/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.common.RegistryUtilities;
import uk.org.nominet.iot.registry.common.RegistryUtilities.Detail;
import uk.org.nominet.iot.registry.database.DatabaseTransaction;
import uk.org.nominet.iot.registry.database.SimpleDatabaseOperation;
import uk.org.nominet.iot.registry.model.Transform;
import uk.org.nominet.iot.registry.service.DataStreamService;
import uk.org.nominet.iot.registry.service.EntityService;
import uk.org.nominet.iot.registry.service.TransformFunctionService;
import uk.org.nominet.iot.registry.service.TransformService;
import uk.org.nominet.iot.registry.service.upsert.DeviceLinkUpsertMetaService;
import uk.org.nominet.iot.registry.service.upsert.DnsDataUpsertMetaService;
import uk.org.nominet.iot.registry.service.upsert.PermissionsUpsertMetaService;
import uk.org.nominet.iot.registry.service.upsert.PushSubscriberUpsertMetaService;
import uk.org.nominet.iot.registry.service.upsert.TransformOutputStreamsUpsertMetaService;
import uk.org.nominet.iot.registry.service.upsert.TransformSourcesUpsertMetaService;
import uk.org.nominet.iot.registry.webserver.api.support.JsonErrorResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSingleResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSuccessResult;
import uk.org.nominet.iot.registry.webserver.api.support.PersistenceEffect;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;
import uk.org.nominet.iot.registry.webserver.api.support.SessionWrapper;

/**
 * The api handler for transforms and transform functions.
 */
@Path("/transform")
public class TransformHandler {
    private final static Logger log = LoggerFactory.getLogger(TransformHandler.class);

    @Context
    Request request;

    @Context
    private javax.servlet.http.HttpServletRequest hsr;

    private TransformService transformService;
    private TransformFunctionService transformFunctionService;
    private DataStreamService dataStreamService;
    private EntityService entityService;

    /**
     * Instantiates a new transform handler.
     */
    public TransformHandler() {
        transformService = new TransformService();
        transformFunctionService = new TransformFunctionService();
        dataStreamService = new DataStreamService();
        entityService = new EntityService();
    }

    /**
     * returns a list of transform functions.
     *
     * @param detailOption the detail option
     * @return the available transform functions
     */
    @GET
    @Path("list_functions")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object listTransformFunctions(final @QueryParam("detail") String detailOption) {
        final Detail detail = RegistryUtilities.DETAIL_FULL.equals(detailOption) ? Detail.FULL : Detail.SIMPLE;

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[transform/list_functions]");

                return new JsonSingleResult("transformFunctions", transformFunctionService.listFunctions(spa, detail)).output();
            }
        });
    }

    /**
     * creates a new transform function multipart; receives JSON as text.
     *
     * @param name the function name
     * @param functionType the function type
     * @param functionContent the function content
     * @return success or failure
     */
    @PUT
    @Path("create_function")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object createTransformFunction(final @FormDataParam("name") String name,
                                          final @FormDataParam("functionType") String functionType,
                                          final @FormDataParam("functionContent") String functionContent) {
        if (Strings.isNullOrEmpty(name))
            return new JsonErrorResult("missing form part \"name\"").asResponse(Status.BAD_REQUEST);
        if (Strings.isNullOrEmpty(functionType))
            return new JsonErrorResult("missing form part \"functionType\"").asResponse(Status.BAD_REQUEST);
        if (Strings.isNullOrEmpty(functionContent))
            return new JsonErrorResult("missing form part \"functionContent\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[transform/create_function] name={}, functionType={}, functioncontent={}", name, functionType,
                    functionContent);

                transformFunctionService.createFunction(spa, name, functionType, functionContent);
                return new JsonSuccessResult();
            }
        });
    }

    /**
     * returns function contents, including metadata.
     *
     * @param name the name
     * @return the transform function
     */
    @GET
    @Path("get_function")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object getTransformFunction(final @QueryParam("name") String name) {
        if (Strings.isNullOrEmpty(name))
            return new JsonErrorResult("missing query param \"name\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[transform/get_function] name={}", name);

                return new JsonSingleResult("function", transformFunctionService.getFunction(spa, name)).output();
            }
        });
    }

    /**
     * modifies a transform function; optionally modifies name multipart; receives
     * JSON as text.
     *
     * @param name the name
     * @param functionType the function type
     * @param functionContent the function content
     * @param updatedNameOption the updated name option
     * @return success or failure
     */
    @PUT
    @Path("modify_function")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object modifyTransformFunction(final @FormDataParam("name") String name,
                                          final @FormDataParam("functionType") String functionType,
                                          final @FormDataParam("functionContent") String functionContent,
                                          final @FormDataParam("updatedName") String updatedNameOption) {
        if (Strings.isNullOrEmpty(name))
            return new JsonErrorResult("missing form part \"name\"").asResponse(Status.BAD_REQUEST);
        if ((Strings.isNullOrEmpty(functionType) && !Strings.isNullOrEmpty(functionContent))
            || (!Strings.isNullOrEmpty(functionType) && Strings.isNullOrEmpty(functionContent)))
            return new JsonErrorResult("to modify the function content \"functionType\" and \"functionContent\" must be set").asResponse(
                Status.BAD_REQUEST);
        if (Strings.isNullOrEmpty(updatedNameOption) && Strings.isNullOrEmpty(functionContent))
            return new JsonErrorResult("to modify the function either the \"updatedName\""
                + " or the \"functionType\" and \"functionContent\" must be set").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[transform/modify_function] name={} functionType={}, functionContent={}, updatedName={}",
                    name, functionType, functionContent, updatedNameOption);

                if (!Strings.isNullOrEmpty(functionContent) && !Strings.isNullOrEmpty(functionType)) {
                    transformFunctionService.modifyFunction(spa, name, functionType, functionContent);
                }

                if (!Strings.isNullOrEmpty(updatedNameOption)) {
                    transformFunctionService.modifyFunctionName(spa, name, updatedNameOption);
                }
                return new JsonSuccessResult();
            }
        });
    }

    /**
     * deletes a transform function.
     *
     * @param name the name of the function
     * @return success or failure
     */
    @DELETE
    @Path("delete_function")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object deleteTransformFunction(final @QueryParam("name") String name) {
        if (Strings.isNullOrEmpty(name))
            return new JsonErrorResult("missing query param \"name\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[transform/delete_function] name={}", name);
                transformFunctionService.deleteFunction(spa, name);
                return new JsonSuccessResult();
            }
        });
    }

    /**
     * returns a list of transform keys.
     *
     * @return an array of transform keys
     */
    @GET
    @Path("list")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object list() {

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[transform/list]");
                return new JsonSingleResult("transforms", transformService.listTransforms(spa)).output();
            }
        });
    }

    /**
     * creates a new transform, with the specified transform function.
     *
     * @param name the name
     * @param type the type
     * @param functionName the function name
     * @param parameterValuesOption the parameter values option
     * @param transformKey the transform key
     * @param runScheduleOption the run schedule option
     * @return the object
     */
    @POST
    @Path("create")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object create(final @FormDataParam("name") String name,
                         final @FormDataParam("type") String type,
                         final @FormDataParam("functionName") String functionName,
                         final @FormDataParam("parameterValues") String parameterValuesOption,
                         final @FormDataParam("transform") String transformKey,
                         final @FormDataParam("runSchedule") String runScheduleOption) {
        if (Strings.isNullOrEmpty(functionName))
            return new JsonErrorResult("missing form part \"functionName\"").asResponse(Status.BAD_REQUEST);
        final String parameterValues = Strings.isNullOrEmpty(parameterValuesOption) ? "{}" : parameterValuesOption;
        final boolean externallyNamed = !Strings.isNullOrEmpty(transformKey);
        final String runSchedule = (runScheduleOption == null) ? "" : runScheduleOption;

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info(
                    "[transform/create] name={}, type={}, functionName={}  parameterValues={} transformKey={} "
                        + "runSchedule={}",
                    name, type, functionName, parameterValues, externallyNamed ? transformKey : "(unspecified)",
                    runSchedule);

                Transform transform = transformService.createTransform(spa, name, type, functionName, parameterValues,
                    externallyNamed, transformKey, runSchedule);

                return new JsonSingleResult("transform", transform.getDataStream().getKey().getKey()).output();
            }
        });
    }

    /**
     * returns transform details.
     *
     * @param transformKey the transform key
     * @param detailOption the detail option
     * @param permissionsOption the permissions option
     * @return the object
     */
    @GET
    @Path("get")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object get(final @QueryParam("transform") String transformKey,
                      final @QueryParam("detail") String detailOption,
                      final @QueryParam("showPermissions") Boolean permissionsOption) {
        if (Strings.isNullOrEmpty(transformKey))
            return new JsonErrorResult("missing param \"transform\"").asResponse(Status.BAD_REQUEST);
        final Detail detail = RegistryUtilities.DETAIL_FULL.equals(detailOption) ? Detail.FULL : Detail.SIMPLE;
        final boolean withPermissions = permissionsOption != null && permissionsOption.booleanValue() ? true : false;

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[transform/get] transform={}", transformKey);

                if (detail == Detail.SIMPLE) {
                    return new JsonSingleResult("transform", transformService.getSummary(spa, transformKey)).output();
                } else {
                    return new JsonSingleResult("transform", transformService.getSummaryDetail(spa, transformKey,
                        withPermissions)).output();
                }
            }
        });
    }

    /**
     * Update the transform function of a transform
     *
     * @param transformKey the transform key
     * @param functionName the function name
     * @return success or failure
     */
    @PUT
    @Path("update_function")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object updateFunction(final @FormParam("transform") String transformKey,
                                 final @FormParam("functionName") String functionName) {
        if (Strings.isNullOrEmpty(transformKey))
            return new JsonErrorResult("missing form param \"transformKey\"").asResponse(Status.BAD_REQUEST);
        if (Strings.isNullOrEmpty(functionName))
            return new JsonErrorResult("missing form param \"functionName\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[transform/update_function] transform={} functionName={}", transformKey, functionName);

                transformService.updateFunction(spa, transformKey, functionName);
                return new JsonSuccessResult();
            }
        });
    }

    /**
     * Update parameter values of a transfrom
     *
     * @param transformKey the transform key
     * @param parameterValues the parameter values
     * @return success or failure
     */
    @PUT
    @Path("update_parameters")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object updateParameterValues(final @FormDataParam("transform") String transformKey,
                                        final @FormDataParam("parameterValues") String parameterValues) {
        if (Strings.isNullOrEmpty(transformKey))
            return new JsonErrorResult("missing form param \"transformKey\"").asResponse(Status.BAD_REQUEST);
        if (Strings.isNullOrEmpty(parameterValues))
            return new JsonErrorResult("missing form param \"parameterValues\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[transform/update_parameters] transform={} parameterValues={}", transformKey,
                    parameterValues);

                transformService.updateParameters(spa, transformKey, parameterValues);

                return new JsonSuccessResult();
            }
        });
    }

    /**
     * Update the run schedule of a transform.
     *
     * @param transformKey the transform key
     * @param runSchedule the run schedule
     * @return success or failure
     */
    @PUT
    @Path("update_runSchedule")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object updateRunSchedule(final @FormParam("transform") String transformKey,
                                    final @FormParam("runSchedule") String runSchedule) {
        if (Strings.isNullOrEmpty(transformKey))
            return new JsonErrorResult("missing form param \"transformKey\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[transform/update_runSchedule] transform={} runSchedule={}", transformKey, runSchedule);

                transformService.updateRunSchedule(spa, transformKey, runSchedule);

                return new JsonSuccessResult();
            }
        });
    }

    /**
     * Update the source streams for a transform.
     *
     * @param transformKey the transform key
     * @param sourceStreamKey the source stream key
     * @param alias the alias
     * @param triggerOption the trigger option
     * @param aggregationType the aggregation type
     * @param aggregationSpec the aggregation spec
     * @return success or failure
     */
    @PUT
    @Path("update_source")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object updateSource(final @FormParam("transform") String transformKey,
                               final @FormParam("stream") String sourceStreamKey,
                               final @FormParam("alias") String alias,
                               final @FormParam("trigger") Boolean triggerOption,
                               final @FormParam("aggregationType") String aggregationType,
                               final @FormParam("aggregationSpec") String aggregationSpec) {
        if (Strings.isNullOrEmpty(transformKey))
            return new JsonErrorResult("missing form param \"transformKey\"").asResponse(Status.BAD_REQUEST);
        // sourceStreamKey can be null, to remove
        if (Strings.isNullOrEmpty(alias))
            return new JsonErrorResult("missing form param \"alias\"").asResponse(Status.BAD_REQUEST);

        if (Strings.isNullOrEmpty(aggregationSpec) && !Strings.isNullOrEmpty(aggregationType)
            || !Strings.isNullOrEmpty(aggregationSpec) && Strings.isNullOrEmpty(aggregationType)) {
            return new JsonErrorResult("both \"aggregationSpec\" and \"aggregationType\" must be set").asResponse(
                Status.BAD_REQUEST);
        }
        if ((aggregationSpec != null && aggregationSpec.isEmpty())
            && (aggregationType != null && aggregationType.isEmpty()))
            return new JsonErrorResult("both \"aggregationSpec\" and \"aggregationType\" are empty").asResponse(
                Status.BAD_REQUEST);

        final Boolean trigger = (triggerOption == null) ? true : triggerOption;
        final boolean removeSource = Strings.isNullOrEmpty(sourceStreamKey);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info(
                    "[transform/update_source] transform={}, source={}, alias={}, trigger={}, aggregationType={}, aggregationSpec={}",
                    transformKey, sourceStreamKey, alias, trigger, aggregationType, aggregationSpec);

                if (removeSource) {
                    transformService.removeSource(spa, transformKey, alias);
                } else {
                    transformService.createOrUpdateSource(spa, transformKey, sourceStreamKey, alias, trigger,
                        aggregationType, aggregationSpec);
                }

                return new JsonSuccessResult();
            }
        });
    }

    /**
     * Update output streams of a transform.
     *
     * @param transformKey the transform key
     * @param streamKey the stream key
     * @param alias the alias
     * @return success or failure
     */
    @PUT
    @Path("update_outputStream")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object updateOutputStreams(final @FormParam("transform") String transformKey,
                                      final @FormParam("stream") String streamKey,
                                      final @FormParam("alias") String alias) {
        if (Strings.isNullOrEmpty(transformKey))
            return new JsonErrorResult("missing form param \"transformKey\"").asResponse(Status.BAD_REQUEST);
        // streamKey can be null, to remove
        if (Strings.isNullOrEmpty(alias))
            return new JsonErrorResult("missing form param \"alias\"").asResponse(Status.BAD_REQUEST);

        final boolean removeStream = Strings.isNullOrEmpty(streamKey);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[transform/update_source] transform={}, stream={}, alias={}", transformKey, streamKey, alias);

                if (removeStream) {
                    transformService.removeOutputStream(spa, transformKey, alias);
                } else {
                    transformService.createOutputStream(spa, transformKey, streamKey, alias);
                }

                return new JsonSuccessResult();
            }
        });
    }

    /**
     * deletes a Transform, including disconnecting sources & deleting output DataStream.
     *
     * @param transformKey the transform key
     * @return success or failure
     */
    @DELETE
    @Path("delete")
    @Produces(MediaType.APPLICATION_JSON)
    public Object delete(final @QueryParam("transform") String transformKey) {
        if (Strings.isNullOrEmpty(transformKey))
            return new JsonErrorResult("missing param \"transform\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[transform/delete] (transform={})", transformKey);

                transformService.delete(spa, transformKey);

                return new JsonSuccessResult();
            }
        });
    }

    /**
     * upserts i.e. creates/updates a Transform multipart; receives JSON as text
     *
     * @param transformKey the transform key
     * @param functionName the function name
     * @param parameterValues the parameter values
     * @param runScheduleOption the run schedule option
     * @param metadata the metadata
     * @param deviceLinks the device links
     * @param sources the sources
     * @param outputStreams the output streams
     * @param pushSubscribers the push subscribers
     * @param userPermissions the user permissions
     * @param groupPermissions the group permissions
     * @param dnsRecords the dns records
     * @param name the name
     * @param type the type
     * @return success or failure
     */
    @PUT
    @Path("upsert")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Object upsert(final @FormDataParam("transform") String transformKey,
                         final @FormDataParam("functionName") String functionName,
                         final @FormDataParam("parameterValues") String parameterValues,
                         final @FormDataParam("runSchedule") String runScheduleOption,
                         final @FormDataParam("metadata") String metadata,
                         final @FormDataParam("devices") String deviceLinks,
                         final @FormDataParam("sources") String sources,
                         final @FormDataParam("outputStreams") String outputStreams,
                         final @FormDataParam("pushSubscribers") String pushSubscribers,
                         final @FormDataParam("userPermissions") String userPermissions,
                         final @FormDataParam("groupPermissions") String groupPermissions,
                         final @FormDataParam("dnsRecords") String dnsRecords,
                         final @FormDataParam("name") String name,
                         final @FormDataParam("type") String type) {
        if (Strings.isNullOrEmpty(transformKey))
            return new JsonErrorResult("missing form part \"transform\"").asResponse(Status.BAD_REQUEST);
        // the other items can each be empty, to imply "no effect"

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[transform/upsert] (transform={})", transformKey);

                // function + params are used at creation, so it's less simple...
                if (!transformService.exists(spa, transformKey)) {
                    // create new transform
                    if (Strings.isNullOrEmpty(functionName)) {
                        throw new DataException("functionName is needed to create a new transform");
                    }

                    // apply default for runSchedule if not supplied
                    String runSchedule = Strings.isNullOrEmpty(runScheduleOption) ? "" : runScheduleOption;
                    boolean externallyNamed = true; // must be true for upsert!
                    transformService.createTransform(spa, name, type, functionName, parameterValues, externallyNamed,
                        transformKey, runSchedule);
                } else {
                    transformService.getByOutputStreamKey(spa, transformKey);

                    if (!Strings.isNullOrEmpty(functionName)) {
                        transformService.updateFunction(spa, transformKey, functionName);
                    }
                    if (!Strings.isNullOrEmpty(parameterValues)) {
                        transformService.updateParameters(spa, transformKey, parameterValues);
                    }
                    if (runScheduleOption != null) {
                        transformService.updateRunSchedule(spa, transformKey, runScheduleOption);
                    }
                }

                if (!Strings.isNullOrEmpty(metadata)) {
                    dataStreamService.setMetadata(spa, transformKey, metadata);
                }
                if (type != null || name != null) {
                    entityService.setValues(spa, transformKey, type, name);
                }
                if (!Strings.isNullOrEmpty(deviceLinks)) {
                    new DeviceLinkUpsertMetaService().upsert(spa, transformKey, deviceLinks);
                }
                if (!Strings.isNullOrEmpty(sources)) {
                    new TransformSourcesUpsertMetaService().upsert(spa, transformKey, sources);
                }
                if (!Strings.isNullOrEmpty(outputStreams)) {
                    new TransformOutputStreamsUpsertMetaService().upsert(spa, transformKey, outputStreams);
                }
                if (!Strings.isNullOrEmpty(userPermissions)) {
                    new PermissionsUpsertMetaService().upsertUserPermissions(spa, transformKey, userPermissions);
                }
                if (!Strings.isNullOrEmpty(groupPermissions)) {
                    new PermissionsUpsertMetaService().upsertGroupPermissions(spa, transformKey, groupPermissions);
                }
                if (!Strings.isNullOrEmpty(dnsRecords)) {
                    new DnsDataUpsertMetaService().upsert(spa, transformKey, dnsRecords);
                }
                if (!Strings.isNullOrEmpty(pushSubscribers)) {
                    new PushSubscriberUpsertMetaService().upsert(spa, transformKey, pushSubscribers);
                }

                return new JsonSingleResult("transform",
                                            transformService.getSummaryDetail(spa, transformKey, true)).output();
            }
        });
    }

}
