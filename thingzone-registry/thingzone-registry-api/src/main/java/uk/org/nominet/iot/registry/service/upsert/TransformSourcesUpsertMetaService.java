/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.upsert;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.TransformSourceDao;
import uk.org.nominet.iot.registry.model.Transform;
import uk.org.nominet.iot.registry.service.TransformService;
import uk.org.nominet.iot.registry.service.dto.TransformSourceDto;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;
import uk.nominet.iot.utils.error.ProgramDefectException;

public class TransformSourcesUpsertMetaService {
    private final static Logger log = LoggerFactory.getLogger(new Object() {}.getClass().getEnclosingClass());

    private final TransformService transformService = new TransformService();
    private TransformSourceDao transformSourceDao;

    public TransformSourcesUpsertMetaService() {
        DaoFactory df = DaoFactoryManager.getFactory();
        transformSourceDao = df.transformSourceDao();
    }

    public void upsert(SessionPermissionAssistant spa, String transformKey, String sourcesAsJsonString) {
        List<TransformSourceDto> requestedSources = decodeFromJson(sourcesAsJsonString);
        Transform transform = transformService.getTransform(spa, transformKey);

        List<TransformSourceDto> existingSources
            = TransformSourceDto.toDto(transformSourceDao.getByTransform(transform));
        // log.info("device links existing ={}", existingLinks);

        if (areSame(requestedSources, existingSources)) {
            log.info("Device links already are already as requested: no need to update");
        } else {
            updateSources(spa, transformKey, requestedSources, existingSources);
        }
    }

    private void updateSources(SessionPermissionAssistant spa,
                               String transformKey,
                               List<TransformSourceDto> requestedSources,
                               List<TransformSourceDto> existingSources) {
        // first, delete any sources that are entirely removed
        existingSources.stream().filter(x -> requestedSources.stream().noneMatch(y -> y.equals(x)))
                       .forEach(x -> transformService.removeSource(spa, transformKey, x.alias));

        // then update all the new/modified ones
        for (TransformSourceDto requestedSource : requestedSources) {
            if (existingSources.contains(requestedSource)) {
                transformService.removeSource(spa, transformKey, requestedSource.alias);
            }
            transformService.createOrUpdateSource(spa, transformKey, requestedSource.source, requestedSource.alias,
                requestedSource.trigger, requestedSource.aggregationType, requestedSource.aggregationSpec);
        }
    }

    static List<TransformSourceDto> decodeFromJson(String json) {
        try {
            return Arrays.asList(new ObjectMapper().readValue(json, TransformSourceDto[].class));
        } catch (IOException | ProgramDefectException e) {
            throw new DataException("bad JSON: " + e.getMessage());
        }
    }

    static private boolean areSame(List<TransformSourceDto> lhs, List<TransformSourceDto> rhs) {
        if (lhs.size() != rhs.size())
            return false;
        for (TransformSourceDto dl : lhs) {
            if (!rhs.contains(dl))
                return false;
        }
        return true;
    }
}
