/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.upsert;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.TransformOutputStreamDao;
import uk.org.nominet.iot.registry.model.Transform;
import uk.org.nominet.iot.registry.service.TransformService;
import uk.org.nominet.iot.registry.service.dto.TransformOutputStreamDto;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;
import uk.nominet.iot.utils.error.ProgramDefectException;

public class TransformOutputStreamsUpsertMetaService {
    private final static Logger log = LoggerFactory.getLogger(new Object() {}.getClass().getEnclosingClass());

    private final TransformService transformService = new TransformService();
    private TransformOutputStreamDao transformOutputStreamDao;

    public TransformOutputStreamsUpsertMetaService() {
        DaoFactory df = DaoFactoryManager.getFactory();
        transformOutputStreamDao = df.transformOutputStreamDao();
    }

    public void upsert(SessionPermissionAssistant spa, String transformKey, String outputStreamsAsJsonString) {
        List<TransformOutputStreamDto> requestedSources = decodeFromJson(outputStreamsAsJsonString);
        Transform transform = transformService.getTransform(spa, transformKey);

        List<TransformOutputStreamDto> existingStreams
            = TransformOutputStreamDto.toDto(transformOutputStreamDao.getByTransform(transform));
        // log.info("device links existing ={}", existingLinks);

        if (areSame(requestedSources, existingStreams)) {
            log.info("Device links already are already as requested: no need to update");
        } else {
            updateOutputStreams(spa, transformKey, requestedSources, existingStreams);
        }
    }

    private void updateOutputStreams(SessionPermissionAssistant spa,
                                     String transformKey,
                                     List<TransformOutputStreamDto> requestedStreams,
                                     List<TransformOutputStreamDto> existingStreams) {
        // first, delete any streams that are entirely removed
        existingStreams.stream().filter(x -> requestedStreams.stream().noneMatch(y -> y.equals(x)))
                       .forEach(x -> transformService.removeOutputStream(spa, transformKey, x.alias));

        // then update all the new/modified ones
        for (TransformOutputStreamDto requestedStream : requestedStreams) {
            if (existingStreams.contains(requestedStream)) {
                transformService.removeOutputStream(spa, transformKey, requestedStream.alias);
            }
            transformService.createOutputStream(spa, transformKey, requestedStream.key, requestedStream.alias);
        }
    }

    static List<TransformOutputStreamDto> decodeFromJson(String json) {
        try {
            return Arrays.asList(new ObjectMapper().readValue(json, TransformOutputStreamDto[].class));
        } catch (IOException | ProgramDefectException e) {
            throw new DataException("bad JSON: " + e.getMessage());
        }
    }

    static private boolean areSame(List<TransformOutputStreamDto> lhs, List<TransformOutputStreamDto> rhs) {
        if (lhs.size() != rhs.size())
            return false;
        for (TransformOutputStreamDto dl : lhs) {
            if (!rhs.contains(dl))
                return false;
        }
        return true;
    }
}
