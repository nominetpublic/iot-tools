/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Customizer;

import uk.org.nominet.iot.registry.database.ColumnPosition;
import uk.org.nominet.iot.registry.database.ColumnPositionCustomizer;
import uk.org.nominet.iot.registry.database.DatabaseDriver;

@Entity
@Table(name = "user_tokens", schema = DatabaseDriver.SCHEMA_NAME)
@Customizer(ColumnPositionCustomizer.class)
public class UserToken {
    @Id
    @Column(name = "token", length = 64)
    @ColumnPosition(position = 1)
    private String token;

    @JoinColumn(name = "user_credentials_id", referencedColumnName = "id", nullable = false)
    @ColumnPosition(position = 2)
    private UserCredentials userCredentials;

    @Column(name = "token_type", columnDefinition = DatabaseDriver.SCHEMA_NAME + ".USER_TOKEN_TYPE", nullable = false)
    @Convert(converter = UserTokenType.UserTokenTypeConverter.class) // our custom converter
    @ColumnPosition(position = 3)
    private UserTokenType tokenType;

    @Column(name = "created", updatable = false, nullable = false)
    @Convert(converter = InstantAttributeConverter.class)
    @ColumnPosition(position = 4)
    @Temporal(TemporalType.TIMESTAMP)
    private Instant created;

    @Column(name = "expiry", updatable = false, nullable = false)
    @Convert(converter = InstantAttributeConverter.class)
    @ColumnPosition(position = 5)
    @Temporal(TemporalType.TIMESTAMP)
    private Instant expiry;

    @Column(name = "used", nullable = true)
    @Convert(converter = InstantAttributeConverter.class)
    @ColumnPosition(position = 6)
    @Temporal(TemporalType.TIMESTAMP)
    private Instant used;

    public UserToken() {
    }

    public UserToken(String token, UserTokenType tokenType, UserCredentials userCredentials, long expirySeconds) {
        this.token = token;
        this.tokenType = tokenType;
        this.userCredentials = userCredentials;
        Instant now = Instant.now();
        this.created = now;
        this.expiry = now.plusSeconds(expirySeconds);
    }

    public String getToken() {
        return token;
    }

    public UserCredentials getUserCredentials() {
        return userCredentials;
    }

    public UserTokenType getTokenType() {
        return tokenType;
    }

    public void setTokenType(UserTokenType tokenType) {
        this.tokenType = tokenType;
    }

    public Instant getCreated() {
        return created;
    }

    public Instant getExpiry() {
        return expiry;
    }

    public void setExpiry(Instant expiry) {
        this.expiry = expiry;
    }

    public Instant getUsed() {
        return used;
    }

    public void setUsed(Instant used) {
        this.used = used;
    }

    @Override
    public String toString() {
        return "UserToken [token=" + token + ", user=" + userCredentials + ", tokenType=" + tokenType + "]";
    }

}
