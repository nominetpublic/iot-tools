/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api.support;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nominet.iot.utils.error.ProgramDefectException;

public class PersistenceEffectRetriever {
    private final static Logger log = LoggerFactory.getLogger(PersistenceEffectRetriever.class);

    public static PersistenceEffect.Effect getPersistenceEffectAnnotation() {
        for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
            Class<?> c;
            try {
                c = Class.forName(ste.getClassName());
            } catch (ClassNotFoundException e1) {
                continue; // keep looking
            }

            Method method = null;
            int matchingMethods = 0;
            for (Method m : c.getMethods()) {
                if (m.getName().equals(ste.getMethodName())) {
                    matchingMethods++;
                    method = m; // don't worry about overwriting: we don't handle overloads
                }
            }
            if (matchingMethods == 0) {
                continue; // keep looking down call stack
            } else if (matchingMethods > 1) {
                // we can't easily disambiguate annotations on overloaded names
                continue; // keep looking down call stack
            }

            PersistenceEffect pe = method.getAnnotation(PersistenceEffect.class);
            if (pe != null) {
                log.info("found PersistenceEffect annotation in call stack: {}", pe.toString());

                // check it was set correctly
                if (pe.type() == PersistenceEffect.Effect.INVALID) {
                    throw new ProgramDefectException("Uninitialised PersistenceEffect annotation");
                }

                // found a valid annotation :-)
                return pe.type();
            }
        }

        // expect to have found something by now
        throw new ProgramDefectException("Missing PersistenceEffect annotation");
    }

}
