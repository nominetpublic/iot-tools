/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import java.time.Instant;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.org.nominet.iot.registry.common.SecureTokenGenerator;
import uk.org.nominet.iot.registry.dao.AbstractJpaDao;
import uk.org.nominet.iot.registry.dao.UserTokenDao;
import uk.org.nominet.iot.registry.model.UserCredentials;
import uk.org.nominet.iot.registry.model.UserToken;
import uk.org.nominet.iot.registry.model.UserTokenType;

public class UserTokenDaoImpl extends AbstractJpaDao<UserToken> implements UserTokenDao {
    private final static Logger log = LoggerFactory.getLogger(UserTokenDaoImpl.class);

    public UserTokenDaoImpl() {
        super(UserToken.class);
    }

    @Override
    public UserToken create(UserCredentials userCredentials, UserTokenType tokenType, long secondsExpiry) {
        String token;
        while (true) {
            token = SecureTokenGenerator.generateToken();
            if (!exists(token))
                break;
            // we have a duplicate; try again
            log.warn("token collision with \"{}\"", token);
        }
        UserToken userToken = new UserToken(token, tokenType, userCredentials, secondsExpiry);
        expireDuplicates(userCredentials, tokenType);
        persist(userToken);
        return userToken;
    }

    /**
     * There should only be one active token per type per user, all others should be
     * expired
     */
    private void expireDuplicates(UserCredentials userCredentials, UserTokenType tokenType) {
        Instant expires = Instant.now();
        for (UserToken userToken : findByFilter("userCredentials", userCredentials, "tokenType", tokenType)) {
            userToken.setExpiry(expires);
        }

    }

    @Override
    public UserToken get(String token, UserTokenType type) {
        return loadByMatch("token", token, "tokenType", type);
    }

    @Override
    public boolean exists(String token) {
        try {
            loadByMatch("token", token);
            return true;
        } catch (PersistenceException e) {
            if (e instanceof NoResultException) {
                return false;
            } else {
                throw e;
            }
        }
    }

    @Override
    public boolean valid(UserToken userToken) {
        if (userToken.getUsed() != null || userToken.getExpiry().isBefore(Instant.now())) {
            return false;
        }
        return true;
    }

}
