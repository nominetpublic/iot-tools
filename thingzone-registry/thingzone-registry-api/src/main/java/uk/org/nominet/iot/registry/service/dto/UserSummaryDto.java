/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRawValue;
import uk.org.nominet.iot.registry.model.Privilege;

import java.util.List;

public class UserSummaryDto {
    public String username;

    @JsonRawValue
    public String details;

    @JsonRawValue
    public String preferences;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public List<Privilege> privileges;

    public UserSummaryDto(String username, String details, String preferences, List<Privilege> privileges) {
        this.username = username;
        this.details = details;
        this.preferences = preferences;
        this.privileges = privileges;
    }
}
