/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api;

import java.util.Map;
import java.util.Set;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import uk.org.nominet.iot.registry.database.DatabaseTransaction;
import uk.org.nominet.iot.registry.database.SimpleDatabaseOperation;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.service.PermissionService;
import uk.org.nominet.iot.registry.service.dto.PermissionDto;
import uk.org.nominet.iot.registry.webserver.api.support.JsonErrorResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonFlattenedMapResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSingleResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSuccessResult;
import uk.org.nominet.iot.registry.webserver.api.support.PersistenceEffect;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;
import uk.org.nominet.iot.registry.webserver.api.support.SessionWrapper;

/**
 * The api handler for displaying and updating permissions
 */
@Path("/permission")
public class PermissionHandler {
    private final static Logger log = LoggerFactory.getLogger(PermissionHandler.class);

    @Context
    Request request;

    @Context
    private javax.servlet.http.HttpServletRequest hsr;

    PermissionService permissionService;

    public PermissionHandler() {
        permissionService = new PermissionService();
    }

    /**
     * get the permissions that apply to you
     */
    @GET
    @Path("get")
    @Produces(MediaType.APPLICATION_JSON)
    public Object get(final @QueryParam("key") String key) {
        if (Strings.isNullOrEmpty(key))
            return new JsonErrorResult("missing key").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[permission/get] (key={})", key);

                PermissionDto permissions = permissionService.getPermissions(spa, key);
                return new JsonSingleResult("permissions", permissions).output();
            }
        });
    }

    /**
     * if you are the owner, get permissions you have set for others
     */
    @GET
    @Path("list")
    @Produces(MediaType.APPLICATION_JSON)
    public Object list(final @QueryParam("key") String key,
                       final @QueryParam("username") String usernameOption,
                       final @QueryParam("group") String groupOption) {
        if (Strings.isNullOrEmpty(key))
            return new JsonErrorResult("missing key").asResponse(Status.BAD_REQUEST);
        // (username can be omitted, to query access as self)
        if (!Strings.isNullOrEmpty(groupOption) && !Strings.isNullOrEmpty(usernameOption))
            return new JsonErrorResult("cannot individually query both username and group").asResponse(
                Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[permission/list] (key={} username={} groupname={})", key, usernameOption, groupOption);
                JsonFlattenedMapResult result = new JsonFlattenedMapResult();
                result.put("key", key);

                if (Strings.isNullOrEmpty(groupOption)) {
                    Map<String, Set<Permission>> userPermissions = permissionService.listUserPermissions(spa, key, usernameOption);
                    result.put("userPermissions", userPermissions);
                }

                if (Strings.isNullOrEmpty(usernameOption)) {
                    Map<String, Set<Permission>> groupPermissions = permissionService.listGroupPermissions(spa, key, groupOption);
                    result.put("groupPermissions", groupPermissions);
                }

                return result.output();
            }
        });
    }

    @PUT
    @Path("set")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object update(final @FormParam("key") String key,
                         final @FormParam("username") String username,
                         final @FormParam("group") String groupName,
                         final @FormParam("permissions") String permissions) {
        if (Strings.isNullOrEmpty(key))
            return new JsonErrorResult("missing param \"key\"").asResponse(Status.BAD_REQUEST);
        if (Strings.isNullOrEmpty(username) && Strings.isNullOrEmpty(groupName))
            return new JsonErrorResult("\"username\" or \"group\" must be set").asResponse(Status.BAD_REQUEST);
        if (permissions == null) // but CAN be empty
            return new JsonErrorResult("missing param \"permissions\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[permission/set] (key={} username={} group={} permissions={})", key, username, groupName, permissions);

                SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);
                if (!Strings.isNullOrEmpty(username))
                    permissionService.setUserPermissions(spa, key, username, PermissionService.validatePermissions(permissions));

                if (!Strings.isNullOrEmpty(groupName))
                    permissionService.setGroupPermissions(spa, key, groupName, PermissionService.validatePermissions(permissions));

                return new JsonSuccessResult();
            }
        });
    }

    @PUT
    @Path("set_keys")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object setKeys(final @FormParam("keys") String keys,
                          final @FormParam("username") String username,
                          final @FormParam("group") String groupName,
                          final @FormParam("permissions") String permissions,
                          final @FormParam("remove") Boolean removeOption) {
        if (Strings.isNullOrEmpty(keys))
            return new JsonErrorResult("missing param \"keys\"").asResponse(Status.BAD_REQUEST);
        if (Strings.isNullOrEmpty(username) && Strings.isNullOrEmpty(groupName))
            return new JsonErrorResult("\"username\" or \"group\" must be set").asResponse(Status.BAD_REQUEST);
        if (Strings.isNullOrEmpty(permissions))
            return new JsonErrorResult("missing param \"permissions\"").asResponse(Status.BAD_REQUEST);

        final boolean remove = removeOption != null && removeOption.booleanValue() ? true : false;

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[permission/set_keys] (keys={} username={} group={} permissions={})", keys, username, groupName,
                    permissions);

                SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);
                if (!Strings.isNullOrEmpty(username))
                    permissionService.setUserPermissionsKeys(spa, keys, username,
                        PermissionService.validatePermissions(permissions), remove);

                if (!Strings.isNullOrEmpty(groupName))
                    permissionService.setGroupPermissionsKeys(spa, keys, groupName,
                        PermissionService.validatePermissions(permissions), remove);

                return new JsonSuccessResult();
            }
        });
    }

}
