/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import uk.org.nominet.iot.registry.database.DatabaseTransaction;
import uk.org.nominet.iot.registry.database.SimpleDatabaseOperation;
import uk.org.nominet.iot.registry.model.Resource;
import uk.org.nominet.iot.registry.service.ResourceService;
import uk.org.nominet.iot.registry.service.dto.ResourceSummaryDto;
import uk.org.nominet.iot.registry.webserver.api.support.JsonErrorResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSingleResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSuccessResult;
import uk.org.nominet.iot.registry.webserver.api.support.PersistenceEffect;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;
import uk.org.nominet.iot.registry.webserver.api.support.SessionWrapper;

/**
 * The API handler for resources.
 */
@Path("/resource")
public class ResourceHandler {
    private final static Logger log = LoggerFactory.getLogger(ResourceHandler.class);

    @Context
    Request request;

    @Context
    private javax.servlet.http.HttpServletRequest hsr;

    ResourceService resourceService;

    /**
     * Instantiates a new resource handler.
     */
    public ResourceHandler() {
        resourceService = new ResourceService();
    }

    /**
     * returns a list of all resources where the user has DISCOVER permission. can be filtered by type
     *
     * @param type the entity type
     * @param resourceType the resource type
     * @return the resources
     */
    @GET
    @Path("/list")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object list(final @QueryParam("type") String type,
                       final @QueryParam("resource_type") String resourceType) {

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[resource/list], (type={}, resourceType={})", type, resourceType);
                List<ResourceSummaryDto> resources;
                if (Strings.isNullOrEmpty(type)) {
                    resources = resourceService.listAll(spa, resourceType);
                } else {
                    resources = resourceService.listByType(spa, type, resourceType);
                }
                return new JsonSingleResult("resources", resources).output();
            }
        });
    }

    /**
     * returns a list of all resources with DISCOVER permission for the given key.
     *
     * @param key the entity key
     * @param resourceType the resource type
     * @return json array of resource names
     */
    @GET
    @Path("/list/{key}")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object listByKey(final @PathParam("key") String key,
                            final @QueryParam("resource_type") String resourceType) {
        if (Strings.isNullOrEmpty(key))
            return new JsonErrorResult("missing parameter \"key\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[resource/list/{}, (resourceType={})]", key, resourceType);

                return new JsonSingleResult("resources", resourceService.listByKey(spa, key, resourceType)).output();
            }
        });
    }

    /**
     * returns a list of all json resources for the given type.
     *
     * @param type the entity type
     * @param resourceType the resource type
     * @return json array of resource names
     */
    @GET
    @Path("/json")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object listJsonByType(final @QueryParam("type") String type,
                                 final @QueryParam("resource_type") String resourceType) {
        if (type == null)
            return new JsonErrorResult("missing parameter \"type\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[resource/json/], (type={}, resourceType={})", type, resourceType);

                return new JsonSingleResult("resources",
                                            resourceService.getJsonByType(spa, type, resourceType)).output();
            }
        });
    }

    /**
     * returns the Resource as a response with the content-type header the value from the store, requires DISCOVER
     * permission on the resource.
     *
     * @param key the entity key
     * @param name the resource name
     * @return the resource file
     */
    @GET
    @Path("/{key}/{name}")
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object get(final @PathParam("key") String key,
                      final @PathParam("name") String name) {
        if (Strings.isNullOrEmpty(key))
            return new JsonErrorResult("missing parameter \"key\"").asResponse(Status.BAD_REQUEST);
        if (Strings.isNullOrEmpty(name))
            return new JsonErrorResult("missing parameter \"name\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return Response.status(Status.FORBIDDEN).entity(sw.getError()).build();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[resource/get] (key={}, name={})", key, name);

                Resource resource = resourceService.getResource(spa, key, name);
                return Response.ok(resource.getContent(), resource.getContentType()).build();
            }
        });
    }

    /**
     * If the key matches an existing record that does not have a resource with the given name, then a new resource will
     * be created for that key.
     *
     * If the key and name match an existing resource then that resource will be updated.
     *
     * multipart; receives JSON as text
     *
     * @param key the entity key
     * @param type the entity type
     * @param name the entity name
     * @param resourceName the resource name
     * @param resourceType the resource type
     * @param content the content
     * @param contentBodyPart the content body part
     * @return the resource details
     */
    @PUT
    @Path("/upsert")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object upsert(final @FormDataParam("key") String key,
                         final @FormDataParam("type") String type,
                         final @FormDataParam("name") String name,
                         final @FormDataParam("resource_name") String resourceName,
                         final @FormDataParam("resource_type") String resourceType,
                         final @FormDataParam("content") InputStream content,
                         final @FormDataParam("content") FormDataBodyPart contentBodyPart) {
        if (Strings.isNullOrEmpty(resourceName))
            return new JsonErrorResult("missing parameter \"resource_name\"").asResponse(Status.BAD_REQUEST);
        if (content == null)
            return new JsonErrorResult("\"content\" is a required parameter").asResponse(Status.BAD_REQUEST);

        final String contentType = contentBodyPart.getMediaType().toString();

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = content.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            byte[] byteArray = out.toByteArray();

            if (contentType.equals("application/json") && !resourceService.checkJson(byteArray)) {
                return new JsonErrorResult("The uploaded data is not valid json").asResponse(Status.BAD_REQUEST);
            }

            return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
                public Object execute() {
                    log.info("[resource/upsert] (key={}, name={}, content_type={})", key, resourceName, contentType);

                    Resource resource = resourceService.upsert(spa, key, name, type, resourceName, resourceType,
                        contentType, byteArray);

                    return new JsonSingleResult("resource", new ResourceSummaryDto(resource)).output();
                }
            });
        } catch (IOException e) {
            return new JsonErrorResult("error reading \"content\"").asResponse(Status.BAD_REQUEST);
        }
    }

    /**
     * Deletes the matching resource.
     *
     * @param key the entity key
     * @param name the resource name
     * @return success or failure
     */
    @DELETE
    @Path("/{key}/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Object delete(final @PathParam("key") String key,
                         final @PathParam("name") String name) {
        if (Strings.isNullOrEmpty(key))
            return new JsonErrorResult("missing parameter \"key\"").asResponse(Status.BAD_REQUEST);
        if (Strings.isNullOrEmpty(name))
            return new JsonErrorResult("missing parameter \"name\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return Response.status(Status.FORBIDDEN).entity(sw.getError()).build();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[resource/get] (key={}, name={})", key, name);

                resourceService.remove(spa, key, name);
                return new JsonSuccessResult();
            }
        });
    }

}
