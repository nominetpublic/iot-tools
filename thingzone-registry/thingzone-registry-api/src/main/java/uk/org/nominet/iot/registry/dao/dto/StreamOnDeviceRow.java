/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.dto;

import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.StreamOnDevice;

// raw DB row object
public class StreamOnDeviceRow {
    private StreamOnDeviceId id;
    private DNSkey dataStreamKey;
    private DNSkey deviceKey;
    private String name;

    public StreamOnDeviceRow(StreamOnDeviceId id, DNSkey dataStreamKey, DNSkey deviceKey, String name) {
        this.id = id;
        this.dataStreamKey = dataStreamKey;
        this.deviceKey = deviceKey;
        this.name = name;
    }

    public StreamOnDeviceRow(StreamOnDevice sod) {
        this.id = new StreamOnDeviceId(sod.getId());
        this.dataStreamKey = sod.getDataStream().getKey();
        this.deviceKey = sod.getDevice().getKey();
        this.name = sod.getName();
    }

    public StreamOnDeviceId getId() {
        return id;
    }

    public DNSkey getDataStreamKey() {
        return dataStreamKey;
    }

    public DNSkey getDeviceKey() {
        return deviceKey;
    }

    public String getName() {
        return name;
    }
}
