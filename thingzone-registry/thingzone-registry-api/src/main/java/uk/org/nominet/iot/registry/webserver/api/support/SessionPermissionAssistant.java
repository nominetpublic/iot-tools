/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api.support;

import java.util.Arrays;
import java.util.List;

import com.google.common.collect.ImmutableList;

import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.database.DatabaseDriver;
import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.Group;
import uk.org.nominet.iot.registry.model.User;

/**
 * SessionPermissionAssistant - starting with info from login session, retrieves & caches: - - User - - list of Accounts
 * applying to User ...
 */
public class SessionPermissionAssistant {
    private SessionWrapper sw;
    private User user = null; // cached
    private User publicUser = null; // cached
    private Account userAccount = null; // cached
    private Account publicAccount = null; // cached
    private List<Group> groups = null; // cached
    private List<Account> groupAccounts = null; // cached

    public SessionPermissionAssistant(SessionWrapper sw) {
        this.sw = sw;
    }

    public String getUsername() {
        return sw.getSession().getUsername();
    }

    public User getUser() {
        return getCachedUser();
    }

    public User getPublicUser() {
        return getCachedPublicUser();
    }

    public List<Group> getGroups() {
        return getCachedUserGroups();
    }

    public List<Account> getGroupAccounts() {
        return getCachedGroupAccounts();
    }

    public Account getUserAccount() {
        return getCachedUserAccount();
    }

    public Account getPublicAccount() {
        return getCachedPublicAccount();
    }

    /**
     * returns a list containing all the accounts for a user (incl 'public') and the groups they are a member of
     */
    public List<Account> getRelevantAccounts() {
        if (!sw.getSession().isLoggedIn()) {
            // public user; can just see public things
            return Arrays.asList(getPublicAccount());
        } else {
            // real user, is logged in
            // - so can see their user'+'public'+'groups'
            ImmutableList.Builder<Account> accountList = ImmutableList.builder();
            accountList.add(getUserAccount(), getPublicAccount());
            accountList.addAll(getGroupAccounts());
            return accountList.build();
        }
    }

    public boolean isPrivilegedConnection() {
        return sw.isPrivilegedAddress();
    }

    // -----------------------------------------------------------------------

    private User getCachedUser() {
        if (user == null) {
            user = DaoFactoryManager.getFactory().userDao().getUserByUsername(sw.getSession().getUsername());
        }
        return user;
    }

    private User getCachedPublicUser() {
        if (publicUser == null) {
            publicUser = DaoFactoryManager.getFactory().userDao().getUserByUsername(DatabaseDriver.PUBLIC_USERNAME);
        }
        return publicUser;
    }

    private List<Group> getCachedUserGroups() {
        if (groups == null) {
            groups = DaoFactoryManager.getFactory().groupUserDao().listGroupsByUser(getCachedUser());
        }
        return groups;
    }

    private List<Account> getCachedGroupAccounts() {
        if (groupAccounts == null) {
            groupAccounts = DaoFactoryManager.getFactory().accountDao().listByGroups(getCachedUserGroups());
        }
        if (groupAccounts == null) {
            return ImmutableList.of();
        }
        return groupAccounts;
    }

    private Account getCachedUserAccount() {
        if (userAccount == null) {
            userAccount = DaoFactoryManager.getFactory().accountDao().getByOwner(getCachedUser());
        }
        return userAccount;
    }

    private Account getCachedPublicAccount() {
        if (publicAccount == null) {
            publicAccount = DaoFactoryManager.getFactory().accountDao().getByOwner(getCachedPublicUser());
        }
        return publicAccount;
    }

}
