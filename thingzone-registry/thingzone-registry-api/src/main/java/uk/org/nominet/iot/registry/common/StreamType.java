/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.common;

public enum StreamType {

    STREAM {
        @Override
        public String toString() {
            return STREAM_TEXT;
        }
    },
    QUEUE {
        @Override
        public String toString() {
            return QUEUE_TEXT;
        }
    };

    final static String STREAM_TEXT = "stream";
    final static String QUEUE_TEXT = "queue";

    public static boolean isValid(String s) {
        if (s == null || s.isEmpty())
            return false;

        try {
            fromString(s);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    public static StreamType fromString(String s) {
        if (s == null || s.isEmpty())
            throw new IllegalArgumentException("StreamType cannot be empty");

        switch (s.toLowerCase()) {
        case STREAM_TEXT:
            return STREAM;
        case QUEUE_TEXT:
            return QUEUE;
        default:
            throw new IllegalArgumentException("s is not a valid StreamType");
        }
    }

}
