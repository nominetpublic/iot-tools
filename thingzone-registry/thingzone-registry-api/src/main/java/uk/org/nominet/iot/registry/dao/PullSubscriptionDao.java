/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao;

import java.util.List;
import java.util.Optional;

import uk.org.nominet.iot.registry.dao.dto.PullSubscriptionRow;
import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.model.PullSubscription;

public interface PullSubscriptionDao extends JpaDao<PullSubscription> {
    PullSubscription create(DataStream dataStream, String uri, String uriParams, Integer interval, Integer aligned);

    List<PullSubscription> getAllowed(List<Account> accounts, List<Permission> perms);

    List<PullSubscription> getAllowed(List<Account> accounts, Permission permission);

    PullSubscription getForStream(DataStream dataStream);

    boolean exists(DataStream dataStream); // no throw

    void delete(DataStream dataStream);

    // returns objects that aren't JPA-managed (for performance)
    Optional<PullSubscriptionRow> unmanagedGetForStream(DataStream dataStream);
}
