/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.database;

import static java.util.Collections.reverseOrder;
import static uk.org.nominet.iot.registry.database.LatestMinc.LATEST_MINC;
import static uk.org.nominet.iot.registry.model.RegistryProperty.PROPERTY_DB_CREATED_BY;
import static uk.org.nominet.iot.registry.model.RegistryProperty.PROPVAL_SCHEMA_CREATOR;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.RegistryPropertyDao;

public class DatabaseChecker {
    private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    final static Pattern validMincPattern = Pattern.compile("^minc\\.\\d\\d\\d$");

    // do a dummy query - this'll throw a pile of errors if it fails
    public static void checkConnection() {
        log.info("checking DB connection (by doing a dummy session query)...");
        new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(null) {
            public Object execute() {
                DaoFactoryManager.getFactory().sessionDao().exists("");
                return null;
            }
        });
        log.info("DB seems to be available");
    }

    public static void checkMincStatus() {
        log.info("checking DB minc status");
        new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(null) {
            public Object execute() {
                RegistryPropertyDao dao = DaoFactoryManager.getFactory().registryPropertyDao();
                Properties props = dao.getProperties();
                if (createdBySchemaCreator(props)) {
                    // we're created by schema creator: all's good
                    log.info("DB was created by schema creator, so there's no value in checking mincs");
                } else {
                    checkMincs(props);
                }
                return null;
            }
        });
        log.info("DB seems to be up-to-date minc-wise");
    }

    private static boolean createdBySchemaCreator(Properties props) {
        return PROPVAL_SCHEMA_CREATOR.equals(props.getProperty(PROPERTY_DB_CREATED_BY));
    }

    private static void checkMincs(Properties props) {
        List<Object> appliedMincs = props.entrySet().stream()
                                         .filter(x -> validMincPattern.matcher((String) x.getKey()).matches())
                                         .map(Map.Entry::getKey).sorted(reverseOrder()).collect(Collectors.toList());
        if (appliedMincs.isEmpty()) {
            throw new RuntimeException("No database mincs applied: presume that mincs aren't up-to-date");
        } else {
            String latestAppliedMinc = (String) appliedMincs.get(0);
            log.info("DB latest minc applied is {}", latestAppliedMinc);
            if (!latestAppliedMinc.equals(LATEST_MINC)) {
                throw new RuntimeException("Latest minc applied to DB is " + latestAppliedMinc
                    + " but server is expecting " + LATEST_MINC);
            }
        }
    }

}
