/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import com.google.common.collect.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.nominet.iot.registry.dao.AbstractJpaDao;
import uk.org.nominet.iot.registry.dao.UserPrivilegeDao;
import uk.org.nominet.iot.registry.model.Privilege;
import uk.org.nominet.iot.registry.model.User;
import uk.org.nominet.iot.registry.model.UserPrivilege;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import java.util.ArrayList;
import java.util.List;

class UserPrivilegeDaoImpl extends AbstractJpaDao<UserPrivilege> implements UserPrivilegeDao {
    private final static Logger log = LoggerFactory.getLogger(UserPrivilegeDaoImpl.class);

    UserPrivilegeDaoImpl() {
        super(UserPrivilege.class);
    }

    @Override
    public UserPrivilege create(User user, Privilege privilege) {
        UserPrivilege up = new UserPrivilege(user, privilege);
        persist(up);
        log.info("added privilege {}", up);
        return up;
    }

    @Override
    public boolean exists(User user, Privilege privilege) {
        try {
            get(user, privilege);
            return true;
        } catch (PersistenceException e) {
            if (e instanceof NoResultException) {
                return false;
            } else {
                throw e;
            }
        }
    }

    @Override
    public List<Privilege> get(User user) {
        List<UserPrivilege> ups = findByFilter("user", user);
        List<Privilege> ps = new ArrayList<>();
        for (UserPrivilege up : ups) {
            ps.add(up.getPrivilege());
        }
        return ImmutableList.copyOf(ps);
    }

    @Override
    public void remove(User user, Privilege privilege) {
        UserPrivilege up = get(user, privilege);
        remove(up);
        log.info("removed privilege {}", up);
    }

    private UserPrivilege get(User user, Privilege privilege) {
        return loadByMatch("user", user, "privilege", privilege);
    }

}
