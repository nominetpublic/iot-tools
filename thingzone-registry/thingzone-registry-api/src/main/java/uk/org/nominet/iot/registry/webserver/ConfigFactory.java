/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nominet.iot.utils.config.ConfigFile;
import uk.nominet.iot.utils.error.ProgramDefectException;
import uk.nominet.iot.utils.signals.SigHupHandler;
import uk.nominet.iot.utils.signals.SignalReceiver;

public class ConfigFactory implements SignalReceiver {
    private final static Logger log = LoggerFactory.getLogger(ConfigFactory.class);

    // singleton
    private static ConfigFactory instance = null;

    protected ConfigFactory() {}

    static ConfigFactory getInstance() {
        return (instance != null) ? instance : getOrCreateInstance();
    }

    private static synchronized ConfigFactory getOrCreateInstance() {
        if (instance == null) {
            instance = new ConfigFactory();
        }
        return instance;
    }

    private WebConfig configProps = null;
    private String configFilename = null;

    static void loadConfig(String filename) {
        ConfigFactory instance = getInstance();
        instance.configFilename = filename;
        log.info("loading config from file \"{}\"", filename);
        instance.load();
        SigHupHandler.getInstance().register(instance);
    }

    // (static interface for compatibility)
    public static WebConfig getConfig() {
        ConfigFactory instance = getInstance();
        if (instance.configProps == null)
            throw new ProgramDefectException("config not yet loaded");
        else
            return instance.configProps;
    }

    @Override
    public void handleSigHup() {
        log.info("SIGHUP: reloading config from file \"{}\"", configFilename);
        load();
    }

    private void load() {
        ConfigFile config = new ConfigFile(configFilename);
        // JLS: Writes to and reads of references are always atomic
        this.configProps = new WebConfigProperties(config.getProperties());
    }

    // ======================================================
    // JUnit injection support

    static boolean isConfigLoaded() {
        ConfigFactory instance = getInstance();
        return instance.configProps != null;
    }

    static void setConfig(WebConfig config) {
        ConfigFactory instance = getInstance();
        instance.configProps = config;
    }
}
