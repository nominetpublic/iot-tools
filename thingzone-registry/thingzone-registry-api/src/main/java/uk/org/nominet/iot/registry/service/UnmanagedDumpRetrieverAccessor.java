/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service;

import uk.org.nominet.iot.registry.dao.*;
import uk.org.nominet.iot.registry.dao.dto.*;
import uk.org.nominet.iot.registry.model.*;
import uk.org.nominet.iot.registry.webserver.api.support.EntityLoader;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * UnmanagedDumpRetrieverAccessor - retrieves DB entities for "dump" - using the fast JDBC / unmanaged route
 */
public class UnmanagedDumpRetrieverAccessor implements DumpRetrieverAccessor {
    private ACLDao aclDao;
    private DNSdataDao dnsDataDao;
    private StreamOnDeviceDao sodDao;
    private PushSubscriberDao pushSubscriberDao;
    private PullSubscriptionDao pullSubscriptionDao;
    private TransformDao transformDao;
    private TransformSourceDao transformSourceDao;
    private TransformOutputStreamDao transformOutputStreamDao;

    UnmanagedDumpRetrieverAccessor() {
        DaoFactory df = DaoFactoryManager.getFactory();
        aclDao = df.aclDao();
        dnsDataDao = df.dnsDataDao();
        sodDao = df.streamOnDeviceDao();
        pushSubscriberDao = df.pushSubscriberDao();
        pullSubscriptionDao = df.pullSubscriptionDao();
        transformDao = df.transformDao();
        transformSourceDao = df.transformSourceDao();
        transformOutputStreamDao = df.transformOutputStreamDao();
    }

    @Override
    public List<DNSdata> listDnsDataByKey(DNSkey key) {
        return dnsDataDao.unmanagedListByKey(key);
    }

    @Override
    public List<StreamOnDeviceRow> listStreamOnDeviceByStream(DNSkey key) {
        return sodDao.unmanagedGetByDataStream(key);
    }

    @Override
    public List<StreamOnDeviceRow> listStreamOnDeviceByDevice(DNSkey key) {
        return sodDao.unmanagedGetByDevice(key);
    }

    @Override
    public List<PushSubscriberRow> listPushSubscribers(DataStream dataStream) {
        return pushSubscriberDao.unmanagedGetForStream(dataStream);
    }

    @Override
    public Optional<PullSubscriptionRow> getPullSubscription(DataStream dataStream) {
        return pullSubscriptionDao.unmanagedGetForStream(dataStream);
    }

    @Override
    public DataStream getDataStream(String key) {
        return EntityLoader.getDataStreamWithoutCheckingPermission(key);
    }

    @Override
    public Transform getTransformByOutputStream(DataStream dataStream) {
        return transformDao.getByOutputStream(dataStream);
    }

    @Override
    public List<TransformSourceRow> getTransformSources(Transform transform) {
        return transformSourceDao.unmanagedGetSources(new TransformId(transform.getId()));
    }

    @Override
    public List<TransformOutputStreamRow> getTransformOutputStreams(Transform transform) {
        return transformOutputStreamDao.unmanagedGetOutputStreams(new TransformId(transform.getId()));
    }

    @Override
    public Map<String, Set<Permission>> getGroupPermissions(DNSkey key) {
        return aclDao.unmanagedGroupPermissions(key);
    }

    @Override
    public Map<String, Set<Permission>> getUserPermissions(DNSkey key) {
        return aclDao.unmanagedUserPermissions(key);
    }

}
