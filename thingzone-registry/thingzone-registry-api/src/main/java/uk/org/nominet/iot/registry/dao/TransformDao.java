/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao;

import java.util.List;

import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.model.Transform;
import uk.org.nominet.iot.registry.model.TransformFunction;

public interface TransformDao extends JpaDao<Transform> {

    Transform create(TransformFunction function, String parameterValues, String runSchedule);

    Transform createKnownKey(String key, TransformFunction function, String parameterValues, String runSchedule);

    Transform create(DataStream outputStream, TransformFunction function, String parameterValues, String runSchedule);

    Transform getById(Long id);

    Transform getByOutputStream(DataStream dataStream);

    List<Transform> getAllowed(List<Account> accounts, List<Permission> perms);

    List<Transform> getAllowed(List<Account> accounts, Permission permission);

    List<Transform> getTransformsWithRunSchedule();

    void delete(Transform transform);

    boolean exists(DataStream dataStream); // no throw
}
