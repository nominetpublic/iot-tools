/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import uk.org.nominet.iot.registry.dao.ACLDao;
import uk.org.nominet.iot.registry.dao.DNSdataDao;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.DataStreamDao;
import uk.org.nominet.iot.registry.dao.DeviceDao;
import uk.org.nominet.iot.registry.dao.PullSubscriptionDao;
import uk.org.nominet.iot.registry.dao.PushSubscriberDao;
import uk.org.nominet.iot.registry.dao.StreamOnDeviceDao;
import uk.org.nominet.iot.registry.dao.TransformDao;
import uk.org.nominet.iot.registry.dao.TransformOutputStreamDao;
import uk.org.nominet.iot.registry.dao.TransformSourceDao;
import uk.org.nominet.iot.registry.dao.dto.PullSubscriptionRow;
import uk.org.nominet.iot.registry.dao.dto.PushSubscriberRow;
import uk.org.nominet.iot.registry.dao.dto.StreamOnDeviceRow;
import uk.org.nominet.iot.registry.dao.dto.TransformOutputStreamRow;
import uk.org.nominet.iot.registry.dao.dto.TransformSourceRow;
import uk.org.nominet.iot.registry.model.DNSdata;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.model.Transform;
import uk.org.nominet.iot.registry.webserver.api.support.EntityLoader;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;

/**
 * DefaultDumpRetrieverAccessor - retrieves DB entities for "dump" - using permission checks and JPA (i.e. "normal"
 * operation)
 */
public class DefaultDumpRetrieverAccessor implements DumpRetrieverAccessor {
    private final ACLDao aclDao;
    private final DNSdataDao dnsDataDao;
    private final StreamOnDeviceDao sodDao;
    private final DataStreamDao dataStreamDao;
    private final DeviceDao deviceDao;
    private final PushSubscriberDao pushSubscriberDao;
    private final PullSubscriptionDao pullSubscriptionDao;
    private final TransformDao transformDao;
    private final TransformSourceDao transformSourceDao;
    private final TransformOutputStreamDao transformOutputStreamDao;

    private final SessionPermissionAssistant spa;

    DefaultDumpRetrieverAccessor(SessionPermissionAssistant spa) {
        DaoFactory df = DaoFactoryManager.getFactory();
        aclDao = df.aclDao();
        dnsDataDao = df.dnsDataDao();
        sodDao = df.streamOnDeviceDao();
        dataStreamDao = df.dataStreamDao();
        deviceDao = df.deviceDao();
        pushSubscriberDao = df.pushSubscriberDao();
        pullSubscriptionDao = df.pullSubscriptionDao();
        transformDao = df.transformDao();
        transformSourceDao = df.transformSourceDao();
        transformOutputStreamDao = df.transformOutputStreamDao();

        this.spa = spa;
    }

    @Override
    public List<DNSdata> listDnsDataByKey(DNSkey key) {
        return dnsDataDao.listByKey(key);
    }

    @Override
    public List<StreamOnDeviceRow> listStreamOnDeviceByStream(DNSkey key) {
        return sodDao.findByDataStream(dataStreamDao.getByName(key.getKey())).stream().map(StreamOnDeviceRow::new)
                     .collect(Collectors.toList());
    }

    @Override
    public List<StreamOnDeviceRow> listStreamOnDeviceByDevice(DNSkey key) {
        return sodDao.findByDevice(deviceDao.getByName(key.getKey())).stream().map(StreamOnDeviceRow::new)
                     .collect(Collectors.toList());
    }

    @Override
    public List<PushSubscriberRow> listPushSubscribers(DataStream dataStream) {
        return pushSubscriberDao.getForStream(dataStream).stream().map(PushSubscriberRow::new)
                                .collect(Collectors.toList());
    }

    @Override
    public Optional<PullSubscriptionRow> getPullSubscription(DataStream dataStream) {
        if (pullSubscriptionDao.exists(dataStream)) {
            return Optional.of(new PullSubscriptionRow(pullSubscriptionDao.getForStream(dataStream)));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public DataStream getDataStream(String key) {
        return EntityLoader.getDataStream(spa, key, Permission.MODIFY);
    }

    @Override
    public Transform getTransformByOutputStream(DataStream dataStream) {
        return transformDao.getByOutputStream(dataStream);
    }

    @Override
    public List<TransformSourceRow> getTransformSources(Transform transform) {
        return transformSourceDao.getByTransform(transform).stream().map(TransformSourceRow::new)
                                 .collect(Collectors.toList());
    }

    @Override
    public List<TransformOutputStreamRow> getTransformOutputStreams(Transform transform) {
        return transformOutputStreamDao.getByTransform(transform).stream().map(TransformOutputStreamRow::new)
                                       .collect(Collectors.toList());
    }

    @Override
    public Map<String, Set<Permission>> getGroupPermissions(DNSkey key) {
        return aclDao.listGroupPermissions(key, null);
    }

    @Override
    public Map<String, Set<Permission>> getUserPermissions(DNSkey key) {
        return aclDao.listUserPermissions(key, null);
    }

}
