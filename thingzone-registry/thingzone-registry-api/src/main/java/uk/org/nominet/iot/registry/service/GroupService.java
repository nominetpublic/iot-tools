/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service;

import static uk.org.nominet.iot.registry.model.Privilege.USE_SESSION;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.NoResultException;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nominet.iot.utils.error.ProgramDefectException;
import uk.org.nominet.iot.registry.common.AuthException;
import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.AccountDao;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.GroupDao;
import uk.org.nominet.iot.registry.dao.GroupUserDao;
import uk.org.nominet.iot.registry.dao.UserCredentialsDao;
import uk.org.nominet.iot.registry.dao.UserDao;
import uk.org.nominet.iot.registry.model.Group;
import uk.org.nominet.iot.registry.model.Privilege;
import uk.org.nominet.iot.registry.model.Session;
import uk.org.nominet.iot.registry.model.User;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;
import uk.org.nominet.iot.registry.webserver.api.support.UserIdentifier;

public class GroupService {

    /** The Constant log. */
    private final static Logger log = LoggerFactory.getLogger(GroupService.class);

    /** The privilege required for create. */
    private final Privilege PRIVILEGE_REQUIRED_FOR_CREATE = Privilege.CREATE_GROUP;

    /** The group dao. */
    private GroupDao groupDao;

    /** The group user dao. */
    private GroupUserDao groupUserDao;

    /** The account dao. */
    private AccountDao accountDao;

    /** The user dao. */
    private UserDao userDao;

    /** The user credentials dao. */
    private UserCredentialsDao userCredentialsDao;

    /** The user privilege service. */
    private UserPrivilegeService userPrivilegeService;

    /** The session service. */
    private SessionService sessionService;

    /**
     * Instantiates a new group service.
     */
    public GroupService() {
        DaoFactory df = DaoFactoryManager.getFactory();
        groupDao = df.groupDao();
        groupUserDao = df.groupUserDao();
        userCredentialsDao = df.userCredentialsDao();
        accountDao = df.accountDao();
        userDao = df.userDao();

        userPrivilegeService = new UserPrivilegeService();
        sessionService = new SessionService();
    }

    /**
     * Create a new group with the current user as the owner.
     *
     * @param spa the spa
     * @param name the name
     * @return the group
     */
    public Group createGroup(SessionPermissionAssistant spa, String name) {
        if (!userPrivilegeService.canCreate(spa, PRIVILEGE_REQUIRED_FOR_CREATE)) {
            throw new AuthException(spa, "need privilege " + PRIVILEGE_REQUIRED_FOR_CREATE);
        }
        Group group = groupDao.create(spa.getUser(), name);
        // create & attach account
        accountDao.createGroupAccount(group);

        return group;
    }

    /**
     * Update the group name, if you are the owner of the group.
     *
     * @param spa the spa
     * @param name the current group name
     * @param newName the new group name
     * @return the group
     */
    public Group updateGroup(SessionPermissionAssistant spa, String name, String newName) {
        Group group = groupDao.getByName(name);
        if (spa.getUser() != group.getOwner()) {
            throw new AuthException(spa, "Not the group owner");
        }
        if (groupDao.exists(newName)) {
            throw new DataException("A group with the new name already exists", Status.CONFLICT);
        } else {
            group.setName(newName);
            return group;
        }
    }

    /**
     * List the names of groups the user is either a member of or owns.
     *
     * @param spa the current user session details
     * @param showOwned whether to only show groups that the user owns
     * @param showAll whether to show all group names, requires USER_ADMIN
     * @return the group names
     */
    public List<String> getGroupNames(SessionPermissionAssistant spa, boolean showOwned, boolean showAll) {
        if (showAll) {
            if (!userPrivilegeService.hasUserAdminPrivilege(spa)) {
                throw new AuthException(spa, "need privilege " + Privilege.USER_ADMIN);
            } else {
                return groupDao.findAll().stream().map(e -> e.getName()).collect(Collectors.toList());
            }
        }
        if (showOwned) {
            return groupDao.listOwnedGroupsByUser(spa.getUser()).stream().map(e -> e.getName())
                           .collect(Collectors.toList());
        }
        return groupUserDao.listGroupsByUser(spa.getUser()).stream().map(e -> e.getName()).collect(Collectors.toList());
    }

    /**
     * List users in the group - requires current user to be a group admin or the group owner.
     *
     * @param spa the current user session details
     * @param groupName the name of the group to list
     * @return the map
     */
    public Map<String, Boolean> listUsers(SessionPermissionAssistant spa, String groupName) {
        Group group = groupDao.getByName(groupName);
        if (spa.getUser() != group.getOwner() && !groupUserDao.isAdmin(group, spa.getUser())) {
            throw new AuthException(spa, "Not a group admin");
        }
        return userCredentialsDao.listUsernamesByGroup(group);
    }

    /**
     * Add users to group - requires current user to be a group admin or the group owner.
     *
     * @param spa the current user session details
     * @param groupName the name of the group to add the users to
     * @param usernames a comma separated string of usernames
     */
    public void addUsers(SessionPermissionAssistant spa, String groupName, String usernames) {
        Group group = groupDao.getByName(groupName);
        if (spa.getUser() != group.getOwner() && !groupUserDao.isAdmin(group, spa.getUser())) {
            throw new AuthException(spa, "Not a group admin");
        }
        for (String username : Arrays.asList(usernames.split("\\s*,\\s*"))) {
            User user;
            try {
                user = userDao.getUserByUsername(username);
            } catch (NoResultException e) {
                throw new DataException("User not found: " + username);
            }
            if (groupUserDao.exists(group, user)) {
                log.info("[user: {} is already a member of group: {}", username, groupName);
            } else {
                groupUserDao.create(group, user, false);
            }
        }
    }

    /**
     * Removes the users.
     *
     * @param spa the spa
     * @param groupName the group name
     * @param usernames the usernames
     */
    public void removeUsers(SessionPermissionAssistant spa, String groupName, String usernames) {
        Group group = groupDao.getByName(groupName);
        if (spa.getUser() != group.getOwner() && !groupUserDao.isAdmin(group, spa.getUser())) {
            throw new AuthException(spa, "Not a group admin");
        }
        for (String username : Arrays.asList(usernames.split("\\s*,\\s*"))) {
            User user;
            try {
                user = userDao.getUserByUsername(username);
            } catch (NoResultException e) {
                throw new DataException("User not found: " + username);
            }
            groupUserDao.remove(group, user);
        }
    }

    /**
     * Sets the admin.
     *
     * @param spa the spa
     * @param groupName the group name
     * @param username the username
     * @param setAdmin the set admin
     */
    public void setAdmin(SessionPermissionAssistant spa, String groupName, String username, Boolean setAdmin) {
        Group group = groupDao.getByName(groupName);
        if (spa.getUser() != group.getOwner() && !groupUserDao.isAdmin(group, spa.getUser())) {
            throw new AuthException(spa, "Current user is not an admin of " + groupName);
        }
        User user;
        try {
            user = userDao.getUserByUsername(username);
        } catch (NoResultException e) {
            throw new DataException("User not found: " + username);
        }
        groupUserDao.setAdmin(group, user, setAdmin);
    }

    /**
     * Used in internal to find the groups a user is a member of.
     *
     * @param spa the current user session details
     * @param userIdentifier the user to query
     * @return the groups for user
     */
    public List<String> getGroupsForUser(SessionPermissionAssistant spa, UserIdentifier userIdentifier) {
        if (!userPrivilegeService.canUseSession(spa)) {
            throw new AuthException(spa, "need privilege " + USE_SESSION);
        }
        if (!userIdentifier.isPublicUser()) {
            User user;
            if (userIdentifier.hasSessionKey()) {
                // get the relevant user's session
                Session userSession = sessionService.getSessionAsReference(userIdentifier.getSessionKey());
                // get the user for the session
                user = userDao.getUserByUsername(userSession.getUsername());
            } else if (userIdentifier.hasUsername()) {
                final String username = userIdentifier.getUsername();

                // get set of accounts to use, based on sessionKey
                if (!userCredentialsDao.exists(username)) {
                    throw new DataException("user not found", Status.NOT_FOUND);
                }
                user = userDao.getUserByUsername(username);
            } else {
                throw new ProgramDefectException("");
            }
            return groupUserDao.listGroupsByUser(user).stream().map(e -> e.getName()).collect(Collectors.toList());
        }
        return groupUserDao.listGroupsByUser(spa.getPublicUser()).stream().map(e -> e.getName())
                           .collect(Collectors.toList());
    }

}
