/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver;

import java.lang.invoke.MethodHandles;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.nominet.iot.utils.error.FatalError;
import uk.nominet.iot.utils.config.ValidatingConfigProperties;

class WebConfigProperties extends ValidatingConfigProperties implements WebConfig {
    private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private List<String> hostnames;
    private List<String> privilegedClients;
    private KeyValidationType keyValidationType;
    private KeyGenerationType keyGenerationType;

    private final static String PROPNAME_HOSTNAMES = "Hostnames";
    private final static String PROPNAME_PORT = "Port";
    private final static String PROPNAME_IDLE_TIMEOUT = "IdleTimeout";
    private final static String PROPNAME_SESSION_TIMEOUT = "SessionTimeout";
    private final static String PROPNAME_PRIVILEGED_CLIENTS = "PrivilegedClients";
    private final static String PROPNAME_KEY_VALIDATION = "KeyValidation";
    private final static String PROPNAME_KEY_GENERATION = "KeyGeneration";

    WebConfigProperties(Properties properties) {
        // superclass will validate, calling our overridden methods
        super(properties);
    }

    private List<String> extractStringListProperty(Properties props, String propertyKey) {
        List<String> propertyList = new ArrayList<>();
        if (!props.containsKey(propertyKey)) {
            throw new FatalError("Config file missing parameter \"" + propertyKey + "\"");
        }
        String value = props.getProperty(propertyKey);
        propertyList.addAll(Arrays.asList(value.split(",\\s*")));
        props.remove(propertyKey);

        log.info("read config:  {} \"{}\"", propertyKey, propertyList);

        return propertyList;
    }

    @Override
    protected Properties customValidation(Properties props) {
        // extract string lists
        hostnames = extractStringListProperty(props, PROPNAME_HOSTNAMES);
        privilegedClients = extractStringListProperty(props, PROPNAME_PRIVILEGED_CLIENTS);

        // convert KeyValidation to enum
        if (!props.containsKey(PROPNAME_KEY_VALIDATION)) {
            throw new FatalError("Config file missing parameter \"" + PROPNAME_KEY_VALIDATION + "\"");
        }
        String value = props.getProperty(PROPNAME_KEY_VALIDATION);
        keyValidationType = KeyValidationType.fromString(value);
        props.remove(PROPNAME_KEY_VALIDATION);
        log.info(String.format("read config: %s = \"%s\"", PROPNAME_KEY_VALIDATION, value));

        // convert KeyGeneration to enum
        if (!props.containsKey(PROPNAME_KEY_GENERATION)) {
            throw new FatalError("Config file missing parameter \"" + PROPNAME_KEY_GENERATION + "\"");
        }
        value = props.getProperty(PROPNAME_KEY_GENERATION);
        keyGenerationType = KeyGenerationType.fromString(value);
        props.remove(PROPNAME_KEY_GENERATION);
        log.info(String.format("read config: %s = \"%s\"", PROPNAME_KEY_GENERATION, value));

        return props;
    }

    private final static String[] INTEGER_PROPERTY_NAMES
        = new String[] { PROPNAME_PORT, PROPNAME_IDLE_TIMEOUT, PROPNAME_SESSION_TIMEOUT, };
    private final static String[] STRING_PROPERTY_NAMES = new String[] {
        // Hostnames is not included because we handled it manually
    };

    @Override
    protected String[] getIntegerPropertyNames() {
        return INTEGER_PROPERTY_NAMES;
    }

    @Override
    protected String[] getStringPropertyNames() {
        return STRING_PROPERTY_NAMES;
    }

    @Override
    public List<String> getHostnames() {
        return Collections.unmodifiableList(hostnames);
    }

    @Override
    public List<String> getPrivilegedClients() {
        return Collections.unmodifiableList(privilegedClients);
    }

    @Override
    public int getPort() {
        return intProps.get(PROPNAME_PORT);
    }

    @Override
    public int getIdleTimeout() {
        return intProps.get(PROPNAME_IDLE_TIMEOUT);
    }

    @Override
    public int getSessionTimeout() {
        return intProps.get(PROPNAME_SESSION_TIMEOUT);
    }

    @Override
    public KeyValidationType getKeyValidationType() {
        return keyValidationType;
    }

    @Override
    public KeyGenerationType getKeyGenerationType() {
        return keyGenerationType;
    }

}
