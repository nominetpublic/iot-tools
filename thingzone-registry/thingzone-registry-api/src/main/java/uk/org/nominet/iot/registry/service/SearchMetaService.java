/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service;

import java.util.Optional;

import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.Device;
import uk.org.nominet.iot.registry.model.EntityStatus;
import uk.org.nominet.iot.registry.model.Transform;
import uk.org.nominet.iot.registry.service.dto.DataStreamSummaryDto;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;

public class SearchMetaService {

    private final DeviceService deviceService;
    private final DataStreamService dataStreamService;
    private final DNSkeyService dnsKeyService;
    private final TransformService transformService;

    public SearchMetaService() {
        deviceService = new DeviceService();
        dataStreamService = new DataStreamService();
        dnsKeyService = new DNSkeyService();
        transformService = new TransformService();
    }

    public static class SearchResult {
        final public String description;
        final public Object value;

        SearchResult(String description, Object value) {
            this.description = description;
            this.value = value;
        }
    }

    public Optional<SearchResult> identify(SessionPermissionAssistant spa,
                                           String key,
                                           boolean withDetail,
                                           boolean withPermissions) {
        DNSkey dnsKey = dnsKeyService.getDnsKey(spa, key);
        if (dnsKey.getStatus() == EntityStatus.DEVICE) {
            // it's a Device
            Device device = deviceService.getDevice(spa, key);
            if (withDetail) {
                return Optional.of(
                    new SearchResult("device", deviceService.getDeviceSummaryDetail(spa, key, withPermissions)));
            }
            return Optional.of(new SearchResult("device", deviceService.getDeviceSummary(spa, device)));
        } else if (dnsKey.getStatus() == EntityStatus.DATASTREAM) {
            // it's a DataStream, but might be the output of a transform...
            DataStream dataStream = dataStreamService.getDataStream(spa, key);

            if (dataStream.getIsTransform()) {
                // it's a Transform
                Transform transform = transformService.getByOutputStream(dataStream);
                if (withDetail) {
                    return Optional.of(
                        new SearchResult("transform",
                                         transformService.getSummaryDetail(spa, transform, withPermissions)));
                }
                return Optional.of(new SearchResult("transform", transformService.getSummary(spa, transform)));
            } else {
                // it's a DataStream
                if (withDetail) {
                    DataStreamSummaryDto dataStreamDetail
                        = dataStreamService.getDataStreamSummaryDetail(spa, dataStream, withPermissions, true, true);
                    return Optional.of(new SearchResult("dataStream", dataStreamDetail));
                }
                return Optional.of(
                    new SearchResult("dataStream", dataStreamService.getDataStreamSummary(spa, dataStream)));
            }
        } else if (dnsKey.getStatus() == EntityStatus.KEY) {
            // it's a DNSkey
            if (withDetail) {
                return Optional.of(new SearchResult("dnsKey", dnsKeyService.getDetail(spa, dnsKey)));
            }
            return Optional.of(new SearchResult("dnsKey", dnsKeyService.getSummary(spa, dnsKey)));
        } else {
            return Optional.empty();
        }
    }

}
