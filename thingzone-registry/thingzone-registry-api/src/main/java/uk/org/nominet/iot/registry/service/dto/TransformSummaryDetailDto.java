/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import uk.org.nominet.iot.registry.model.Transform;
import uk.org.nominet.iot.registry.model.User;

public class TransformSummaryDetailDto {

    @JsonUnwrapped
    public DataStreamSummaryDto dataStream;

    @JsonInclude(Include.NON_EMPTY)
    public TransformFunctionDto function;

    @JsonInclude(Include.NON_EMPTY)
    public List<TransformSourceDto> sources;

    @JsonRawValue
    @JsonInclude(Include.NON_NULL)
    public String parameterValues;

    @JsonInclude(Include.NON_NULL)
    public String runSchedule;

    @JsonInclude(Include.NON_EMPTY)
    public List<TransformOutputStreamDto> outputStreams;

    private TransformSummaryDetailDto(Transform transform,
                                      DataStreamSummaryDto dataStream,
                                      List<TransformSourceDto> transformSourceDtos,
                                      List<TransformOutputStreamDto> outputStreams,
                                      boolean details,
                                      boolean showFunctionContent) {
        this.dataStream = dataStream;
        this.dataStream.isTransform = null;
        this.outputStreams = outputStreams;
        this.function = new TransformFunctionDto(transform.getFunction(), showFunctionContent);

        if (details) {
            this.sources = transformSourceDtos;
            this.parameterValues = transform.getParameterValues();
            this.runSchedule = transform.getRunSchedule();
        }
    }

    public static TransformSummaryDetailDto details(Transform transform,
                                                    DataStreamSummaryDto dataStream,
                                                    List<TransformSourceDto> transformSourceDtos,
                                                    List<TransformOutputStreamDto> outputStreams,
                                                    User publicUser) {
        return new TransformSummaryDetailDto(transform, dataStream, transformSourceDtos, outputStreams, true, false);
    }

    public static TransformSummaryDetailDto withFunction(Transform transform,
                                                         DataStreamSummaryDto dataStream,
                                                         List<TransformSourceDto> transformSourceDtos,
                                                         List<TransformOutputStreamDto> outputStreams) {
        return new TransformSummaryDetailDto(transform, dataStream, transformSourceDtos, outputStreams, true, true);
    }

    public static TransformSummaryDetailDto basic(Transform transform,
                                                  DataStreamSummaryDto dataStream,
                                                  boolean modifyPermission) {
        return new TransformSummaryDetailDto(transform, dataStream, null, null, modifyPermission, false);
    }
}
