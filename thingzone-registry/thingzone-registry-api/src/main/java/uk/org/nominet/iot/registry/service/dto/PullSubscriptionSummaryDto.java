/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.dto;

import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonRawValue;

import uk.org.nominet.iot.registry.dao.dto.PullSubscriptionRow;
import uk.org.nominet.iot.registry.model.PullSubscription;

public class PullSubscriptionSummaryDto {
    @JsonInclude(Include.NON_NULL)
    public String dataStream;

    public String uri;

    @JsonRawValue
    public String uriParams;

    public Integer interval;

    public Integer aligned;

    public PullSubscriptionSummaryDto(String dataStream,
                                      String uri,
                                      String uriParams,
                                      Integer interval,
                                      Integer aligned) {
        this.dataStream = dataStream;
        this.uri = uri;
        this.uriParams = uriParams;
        this.interval = interval;
        this.aligned = aligned;
    }

    public PullSubscriptionSummaryDto(PullSubscription ps) {
        this.dataStream = ps.getDataStream().getKey().getKey();
        this.uri = ps.getUri();
        this.uriParams = ps.getUriParams();
        this.interval = ps.getInterval();
        this.aligned = ps.getAligned();
    }

    public PullSubscriptionSummaryDto(PullSubscriptionRow ps) {
        this.dataStream = ps.getStreamKey().getKey();
        this.uri = ps.getUri();
        this.uriParams = ps.getUriParams();
        this.interval = ps.getInterval();
        this.aligned = ps.getAligned();
    }

    public static Optional<PullSubscriptionSummaryDto> fromOptional(Optional<PullSubscription> pso) {
        if (pso.isPresent()) {
            return Optional.of(new PullSubscriptionSummaryDto(pso.get()));
        } else {
            return Optional.empty();
        }
    }

    public static Optional<PullSubscriptionSummaryDto> fromOptionalRow(Optional<PullSubscriptionRow> pso) {
        if (pso.isPresent()) {
            return Optional.of(new PullSubscriptionSummaryDto(pso.get()));
        } else {
            return Optional.empty();
        }
    }

    // allow removal of back-reference when serialising as a child of a DataStream
    void clearDataStream() {
        this.dataStream = null;
    }

    @Override
    public String toString() {
        return "PullSubscriptionSummaryDto{" + "dataStream='" + dataStream + '\'' + ", uri='" + uri + '\''
            + ", uriParams='" + uriParams + '\'' + ", interval=" + interval + ", aligned=" + aligned + '}';
    }
}
