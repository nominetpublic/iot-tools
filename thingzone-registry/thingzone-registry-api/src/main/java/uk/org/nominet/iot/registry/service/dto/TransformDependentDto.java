/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import uk.org.nominet.iot.registry.model.Transform;
import uk.org.nominet.iot.registry.model.TransformOutputStream;
import uk.org.nominet.iot.registry.model.TransformSource;

public class TransformDependentDto {
    public String alias;
    public Boolean trigger;
    public TransformDependentWithFunction transform;

    class TransformDependentWithFunction {
        @JsonUnwrapped
        DnsKeyDetailDto basicEntity;

        @JsonRawValue
        public String metadata;

        @JsonRawValue
        public String parameterValues;

        public List<TransformSourceDto> sources;

        public List<TransformOutputStreamDto> outputStreams;

        public TransformFunctionDto function;

        TransformDependentWithFunction(Transform transform,
                                       List<TransformSource> sources,
                                       List<TransformOutputStream> outputStreams) {
            this.basicEntity = DnsKeyDetailDto.basic(transform.getDataStream().getKey());
            this.metadata = transform.getDataStream().getMetadata();
            this.parameterValues = transform.getParameterValues();
            this.sources = TransformSourceDto.toDto(sources);
            this.outputStreams = TransformOutputStreamDto.toDto(outputStreams);
            this.function = new TransformFunctionDto(transform.getFunction(), true);
        }
    }

    public TransformDependentDto(TransformSource transformSource,
                                 List<TransformSource> sources,
                                 List<TransformOutputStream> outputStreams) {
        this.alias = transformSource.getAlias();
        this.trigger = transformSource.getTrigger();
        this.transform = new TransformDependentWithFunction(transformSource.getTransform(), sources, outputStreams);
    }
}
