/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao;

import java.util.List;

import uk.org.nominet.iot.registry.dao.dto.TransformId;
import uk.org.nominet.iot.registry.dao.dto.TransformSourceRow;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.Transform;
import uk.org.nominet.iot.registry.model.TransformSource;

public interface TransformSourceDao extends JpaDao<TransformSource> {
    TransformSource create(Transform transform,
                           DataStream sourceStream,
                           String alias,
                           Boolean trigger,
                           String aggregationType,
                           String aggregationSpec);

    TransformSource createOrUpdate(Transform transform,
                                   DataStream sourceStream,
                                   String alias,
                                   Boolean trigger,
                                   String aggregationType,
                                   String aggregationSpec);

    List<TransformSource> getByTransform(Transform transform);

    List<TransformSource> getBySourceStream(DataStream sourceStream);

    TransformSource get(Transform transform, String alias);

    void delete(Transform transform, String alias);

    boolean exists(Transform transform, String alias); // no throw

    boolean exists(Transform transform, DataStream sourceStream);

    // returns objects that aren't JPA-managed (for performance)
    List<TransformSourceRow> unmanagedGetSources(TransformId id);
}
