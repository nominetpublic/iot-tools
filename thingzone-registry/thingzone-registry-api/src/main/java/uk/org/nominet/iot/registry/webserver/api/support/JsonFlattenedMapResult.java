/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api.support;

import java.util.LinkedHashMap;

/**
 * JsonFlattenedMapResult is a container for a "success" response, with a list of name -> value pairs
 *
 * {"result":"ok","thing":{ <csomething> } [, "another thing" : { <something else> }]}
 *
 * Usage example: return new JsonFlattenedMapResult().put("foo", "value for foo").put("bar", 42).output();
 *
 */
public class JsonFlattenedMapResult {
    private final static String SUCCESS = "ok";

    private LinkedHashMap<String, Object> resultContainer = new LinkedHashMap<>();

    public JsonFlattenedMapResult() {
        resultContainer.put("result", SUCCESS);
    }

    public JsonFlattenedMapResult put(String name, Object contents) {
        resultContainer.put(name, contents);
        return this;
    }

    public LinkedHashMap<String, Object> output() {
        return resultContainer;
    }
}
