/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api.support;

import java.util.LinkedHashMap;

/**
 * JsonMapResult is a container for a "success" response, with named contents - contents name set at creation - values
 * can be added to contents
 *
 * {"result":"ok","<contents name>":{ <contents stored> }}
 *
 * Usage example: return new JsonResult("thingy") .put("foo", "value for foo") .put("bar", 42) .output();
 *
 */
class JsonMapResult {
    private final static String SUCCESS = "ok";
    private LinkedHashMap<String, Object> resultContainer = new LinkedHashMap<>();
    private LinkedHashMap<String, Object> resultContents = new LinkedHashMap<>();

    public JsonMapResult(String valueName) {
        resultContainer.put("result", SUCCESS);
        resultContainer.put(valueName, resultContents);
    }

    public JsonMapResult put(String name, Object contents) {
        resultContents.put(name, contents);
        return this;
    }

    public LinkedHashMap<String, Object> output() {
        return resultContainer;
    }
}
