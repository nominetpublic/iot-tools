/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.ddns.validator;

import java.io.IOException;

import org.xbill.DNS.DClass;
import org.xbill.DNS.Name;
import org.xbill.DNS.Record;
import org.xbill.DNS.TextParseException;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.common.Util;
import uk.nominet.iot.utils.error.ProgramDefectException;

public abstract class AbstractRecordValidator {

    abstract int expectedType();

    abstract Class<?> expectedClass();

    abstract String expectedName();

    public void validate(String rdata) {
        Name origin = makeName("example.com");
        Name name = makeName("foo");
        Record r;
        try {
            r = Record.fromString(name, expectedType(), DClass.IN, 0, rdata, origin);
        } catch (IOException e) {
            throw new DataException("failed to parse " + expectedName() + " record (" + rdata + ")");
        }

        if (r.getClass() != expectedClass()) {
            throw new DataException("record parse error: expected " + expectedClass() + " but got "
                + r.getClass().toString());
        }
    }

    /**
     * dnsjava uses Name objects for FQDNs; this is a convenience function
     *
     * (we should have validated the nameserver name already & so we don't expect
     * this to fail)
     */
    Name makeName(String s) {
        // dnsjava requires a trailing period on all Name objects
        s = Util.ensureTrailingPeriod(s);
        Name name;
        try {
            name = new Name(s);
        } catch (TextParseException e) {
            throw new ProgramDefectException("Failed to make Name from \"" + s + "\": " + e.getMessage());
        }
        return name;
    }

}
