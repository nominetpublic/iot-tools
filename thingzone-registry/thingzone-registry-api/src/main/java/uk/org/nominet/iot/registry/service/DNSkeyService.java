/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service;

import java.util.List;

import uk.org.nominet.iot.registry.dao.ACLDao;
import uk.org.nominet.iot.registry.dao.DNSkeyDao;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.service.dto.DnsKeyDetailDto;
import uk.org.nominet.iot.registry.webserver.api.support.EntityLoader;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;

/**
 * DNSkey Service
 */
public class DNSkeyService {
    DNSkeyDao dnsKeyDao;
    ACLDao aclDao;
    ACLService aclService;
    DNSdataService dnsDataService;

    public DNSkeyService() {
        DaoFactory df = DaoFactoryManager.getFactory();
        dnsKeyDao = df.dnsKeyDao();
        aclDao = df.aclDao();

        aclService = new ACLService();
        dnsDataService = new DNSdataService();
    }

    public DNSkey getDnsKey(SessionPermissionAssistant spa, String key) {
        return EntityLoader.getDnsKey(spa, key, Permission.DISCOVER);
    }

    public DnsKeyDetailDto getDetail(SessionPermissionAssistant spa, DNSkey dnsKey) {
        return DnsKeyDetailDto.withRecords(dnsKey, dnsDataService.dnsDataListByKey(spa, dnsKey));
    }

    public DnsKeyDetailDto getSummary(SessionPermissionAssistant spa, DNSkey dnsKey) {
        return DnsKeyDetailDto.basic(dnsKey);
    }

    public List<DNSkey> getAllowed(SessionPermissionAssistant spa, List<Permission> perms, int size, int page) {
        return dnsKeyDao.getAllowed(spa.getRelevantAccounts(), perms, size, page);
    }
}
