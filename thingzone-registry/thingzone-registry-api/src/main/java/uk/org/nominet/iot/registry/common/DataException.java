/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.common;

import javax.ws.rs.core.Response.Status;

public class DataException extends RuntimeException {

    private static final long serialVersionUID = 7549707592054943821L;
    private Status status;

    public DataException(String message, Status status) {
        super(message);
        this.status = status;
    }

    public DataException(String message) {
        super(message);
        this.status = Status.BAD_REQUEST;
    }

    public DataException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public String formatWithCause() {
        StringBuilder sb = new StringBuilder();
        sb.append(getMessage());
        if (getCause() != null) {
            sb.append("\n").append(getCause().getMessage());
        }
        return sb.toString();
    }

    public Status getStatus() {
        return status;
    }
}
