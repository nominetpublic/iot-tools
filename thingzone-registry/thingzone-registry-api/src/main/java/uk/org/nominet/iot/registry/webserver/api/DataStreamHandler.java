/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import uk.org.nominet.iot.registry.common.RegistryUtilities;
import uk.org.nominet.iot.registry.common.RegistryUtilities.Detail;
import uk.org.nominet.iot.registry.database.DatabaseTransaction;
import uk.org.nominet.iot.registry.database.SimpleDatabaseOperation;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.service.DataStreamService;
import uk.org.nominet.iot.registry.service.EntityService;
import uk.org.nominet.iot.registry.service.dto.DataStreamSummaryDto;
import uk.org.nominet.iot.registry.service.upsert.DeviceLinkUpsertMetaService;
import uk.org.nominet.iot.registry.service.upsert.DnsDataUpsertMetaService;
import uk.org.nominet.iot.registry.service.upsert.PermissionsUpsertMetaService;
import uk.org.nominet.iot.registry.service.upsert.PullSubscriptionUpsertMetaService;
import uk.org.nominet.iot.registry.service.upsert.PushSubscriberUpsertMetaService;
import uk.org.nominet.iot.registry.webserver.api.support.JsonErrorResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSingleResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSuccessResult;
import uk.org.nominet.iot.registry.webserver.api.support.PersistenceEffect;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;
import uk.org.nominet.iot.registry.webserver.api.support.SessionWrapper;

/**
 * The API handler for DataStreams.
 */
@Path("/datastream")
public class DataStreamHandler {
    private final static Logger log = LoggerFactory.getLogger(DataStreamHandler.class);

    @Context
    Request request;

    @Context
    private javax.servlet.http.HttpServletRequest hsr;

    private final DataStreamService dataStreamService;
    private final EntityService entityService;

    /**
     * Instantiates a new data stream handler.
     */
    public DataStreamHandler() {
        dataStreamService = new DataStreamService();
        entityService = new EntityService();
    }

    /**
     * returns a list of data streams.
     *
     * @return the datastreams
     */
    @GET
    @Path("list")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object list() {
        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[datastream/list] ()");

                List<String> keys = dataStreamService.getVisibleTo(spa).stream()
                                                     .map(dataStream -> dataStream.getKey().getKey())
                                                     .collect(Collectors.toList());
                return new JsonSingleResult("dataStreams", keys).output();
            }
        });
    }

    /**
     * creates a DataStream, owned by the current user returns name of DataStream (dnsKey).
     *
     * @param dataStreamKey the data stream key
     * @param name the name
     * @param type the type
     * @return the new datastream key
     */
    @POST
    @Path("create")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object createNamed(final @FormParam("dataStream") String dataStreamKey,
                              final @FormParam("name") String name,
                              final @FormParam("type") String type) {
        final boolean externallyNamed = !Strings.isNullOrEmpty(dataStreamKey);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[datastream/create] (dataStreamKey={}, name={}, type={})",
                    externallyNamed ? dataStreamKey : "(unspecified)", name, type);

                // create a new datastream
                DataStream created = dataStreamService.create(spa, externallyNamed, dataStreamKey, name, type);
                log.info("created DataStream {}", created.getKey());

                return new JsonSingleResult("dataStream", created.getKey().getKey()).output();
            }
        });
    }

    /**
     * returns DataStream contents, including metadata.
     *
     * @param dataStreamKey the data stream key
     * @param detailOption the detail option
     * @param permissionsOption the permissions option
     * @return the datastream
     */
    @GET
    @Path("get")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object get(final @QueryParam("dataStream") String dataStreamKey,
                      final @QueryParam("detail") String detailOption,
                      final @QueryParam("showPermissions") Boolean permissionsOption) {
        if (Strings.isNullOrEmpty(dataStreamKey))
            return new JsonErrorResult("missing param \"dataStream\"").asResponse(Status.BAD_REQUEST);
        final Detail detail = RegistryUtilities.DETAIL_FULL.equals(detailOption) ? Detail.FULL : Detail.SIMPLE;
        final boolean withPermissions = permissionsOption != null && permissionsOption.booleanValue() ? true : false;

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[datastream/get] (datastream={} detail={})", dataStreamKey, detail);

                DataStream dataStream = dataStreamService.getDataStream(spa, dataStreamKey);
                if (detail == Detail.SIMPLE) {
                    DataStreamSummaryDto dataStreamOut = dataStreamService.getDataStreamSummary(spa, dataStream);
                    return new JsonSingleResult("dataStream", dataStreamOut).output();
                } else {
                    DataStreamSummaryDto dataStreamDetail
                        = dataStreamService.getDataStreamSummaryDetail(spa, dataStream, withPermissions, true, true);
                    return new JsonSingleResult("dataStream", dataStreamDetail).output();
                }
            }
        });
    }

    /**
     * updates the metadata for a DataStream; replaces any previous value multipart; receives JSON as text.
     *
     * @param dataStreamKey the data stream key
     * @param metadata the metadata
     * @param name the name
     * @param type the type
     * @return success or failure
     */
    @PUT
    @Path("update")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object update(final @FormDataParam("dataStream") String dataStreamKey,
                         final @FormDataParam("metadata") String metadata,
                         final @FormDataParam("name") String name,
                         final @FormDataParam("type") String type) {
        if (Strings.isNullOrEmpty(dataStreamKey))
            return new JsonErrorResult("missing form part \"dataStream\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[datastream/update] (datastream={}, metadata={}, name={}, type={})", dataStreamKey, metadata,
                    name, type);

                dataStreamService.setMetadata(spa, dataStreamKey, metadata);

                if (type != null || name != null) {
                    entityService.setValues(spa, dataStreamKey, type, name);
                }

                return new JsonSuccessResult();
            }
        });
    }

    /**
     * deletes a DataStream & associated push/pull.
     *
     * @param dataStreamKey the data stream key
     * @return success or failure
     */
    @DELETE
    @Path("delete")
    @Produces(MediaType.APPLICATION_JSON)
    public Object delete(final @QueryParam("dataStream") String dataStreamKey) {
        if (Strings.isNullOrEmpty(dataStreamKey))
            return new JsonErrorResult("missing param \"dataStream\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[datastream/delete] (datastream={})", dataStreamKey);

                dataStreamService.delete(spa, dataStreamKey);

                return new JsonSuccessResult();
            }
        });
    }

    /**
     * upserts i.e. creates/updates a DataStream multipart; receives JSON as text
     *
     * @param dataStreamKey the data stream key
     * @param metadata the metadata
     * @param deviceLinks the device links
     * @param userPermissions the user permissions
     * @param groupPermissions the group permissions
     * @param dnsRecords the dns records
     * @param pushSubscribers the push subscribers
     * @param pullSubscription the pull subscription
     * @param type the type
     * @param name the name
     * @return the updated datastream
     */
    @PUT
    @Path("upsert")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Object upsert(final @FormDataParam("dataStream") String dataStreamKey,
                         final @FormDataParam("metadata") String metadata,
                         final @FormDataParam("devices") String deviceLinks,
                         final @FormDataParam("userPermissions") String userPermissions,
                         final @FormDataParam("groupPermissions") String groupPermissions,
                         final @FormDataParam("dnsRecords") String dnsRecords,
                         final @FormDataParam("pushSubscribers") String pushSubscribers,
                         final @FormDataParam("pullSubscription") String pullSubscription,
                         final @FormDataParam("type") String type,
                         final @FormDataParam("name") String name) {
        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        final boolean externallyNamed = !Strings.isNullOrEmpty(dataStreamKey);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {

                DataStream dataStream = dataStreamService.upsert(spa, dataStreamKey, externallyNamed);
                String upsertKey = dataStream.getKey().getKey();
                log.info("[datastream/upsert] (datastream={})", upsertKey);

                if (type != null || name != null) {
                    entityService.setValues(spa, upsertKey, type, name);
                }
                if (!Strings.isNullOrEmpty(metadata)) {
                    dataStreamService.setMetadata(spa, upsertKey, metadata);
                }
                if (!Strings.isNullOrEmpty(deviceLinks)) {
                    new DeviceLinkUpsertMetaService().upsert(spa, upsertKey, deviceLinks);
                }
                if (!Strings.isNullOrEmpty(userPermissions)) {
                    new PermissionsUpsertMetaService().upsertUserPermissions(spa, upsertKey, userPermissions);
                }
                if (!Strings.isNullOrEmpty(groupPermissions)) {
                    new PermissionsUpsertMetaService().upsertGroupPermissions(spa, upsertKey, groupPermissions);
                }
                if (!Strings.isNullOrEmpty(dnsRecords)) {
                    new DnsDataUpsertMetaService().upsert(spa, upsertKey, dnsRecords);
                }
                if (!Strings.isNullOrEmpty(pushSubscribers)) {
                    new PushSubscriberUpsertMetaService().upsert(spa, upsertKey, pushSubscribers);
                }
                if (!Strings.isNullOrEmpty(pullSubscription)) {
                    new PullSubscriptionUpsertMetaService().upsert(spa, upsertKey, pullSubscription);
                }

                return new JsonSingleResult("dataStream", dataStreamService.getDataStreamSummaryDetail(spa, dataStream,
                    true, true, true)).output();
            }
        });
    }

}
