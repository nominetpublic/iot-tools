/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api;

import static uk.org.nominet.iot.registry.model.Privilege.QUERY_STREAMS;
import static uk.org.nominet.iot.registry.model.Privilege.USE_SESSION;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import uk.org.nominet.iot.registry.common.AuthException;
import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.common.RegistryUtilities;
import uk.org.nominet.iot.registry.database.DatabaseTransaction;
import uk.org.nominet.iot.registry.database.SimpleDatabaseOperation;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.PushSubscriber;
import uk.org.nominet.iot.registry.model.Resource;
import uk.org.nominet.iot.registry.model.Session;
import uk.org.nominet.iot.registry.service.GroupService;
import uk.org.nominet.iot.registry.service.PermissionService;
import uk.org.nominet.iot.registry.service.PushSubscriberService;
import uk.org.nominet.iot.registry.service.ResourceService;
import uk.org.nominet.iot.registry.service.SearchMetaService;
import uk.org.nominet.iot.registry.service.SessionService;
import uk.org.nominet.iot.registry.service.TransformService;
import uk.org.nominet.iot.registry.service.UserPrivilegeService;
import uk.org.nominet.iot.registry.service.dto.DnsKeyDetailDto;
import uk.org.nominet.iot.registry.service.dto.PushSubscriberDetailDto;
import uk.org.nominet.iot.registry.service.dto.TransformDependentDto;
import uk.org.nominet.iot.registry.webserver.api.support.JsonErrorResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonFlattenedMapResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSingleResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSuccessResult;
import uk.org.nominet.iot.registry.webserver.api.support.PersistenceEffect;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;
import uk.org.nominet.iot.registry.webserver.api.support.SessionWrapper;
import uk.org.nominet.iot.registry.webserver.api.support.UserIdentifier;

/**
 * API handler for internal application requests.
 */
@Path("/internal")
public class InternalHandler {
    private final static Logger log = LoggerFactory.getLogger(InternalHandler.class);

    @Context
    Request request;

    @Context
    private javax.servlet.http.HttpServletRequest hsr;

    private final PermissionService permissionService;
    private final SessionService sessionService;
    private final UserPrivilegeService userPrivilegeService;
    private final TransformService transformService;
    private final SearchMetaService searchMetaService;
    private final GroupService groupService;
    private final ResourceService resourceService;
    private final PushSubscriberService pushSubscriberService;

    /**
     * Instantiates a new internal handler.
     */
    public InternalHandler() {
        permissionService = new PermissionService();
        sessionService = new SessionService();
        userPrivilegeService = new UserPrivilegeService();
        transformService = new TransformService();
        searchMetaService = new SearchMetaService();
        groupService = new GroupService();
        resourceService = new ResourceService();
        pushSubscriberService = new PushSubscriberService();
    }

    /**
     * checks that session has the permissions specified, on the DNSkeys specified
     * (for use by Bridge/Datastore).
     *
     * @param keys the keys
     * @param permissions the permissions
     * @param sessionKeyOption the session key
     * @param usernameOption the username
     * @return success or failure
     */
    @GET
    @Path("permission_check")
    @Produces(MediaType.APPLICATION_JSON)
    public Object permissionCheck(final @QueryParam("keys") String keys,
                                  final @QueryParam("permissions") String permissions,
                                  final @QueryParam("sessionKey") String sessionKeyOption,
                                  final @QueryParam("username") String usernameOption) {
        if (Strings.isNullOrEmpty(keys))
            return new JsonErrorResult("missing param \"keys\"").asResponse(Status.BAD_REQUEST);
        if (Strings.isNullOrEmpty(permissions))
            return new JsonErrorResult("missing param \"permissions\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = SessionWrapper.createPrivilegedSession(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);
        final UserIdentifier userIdentifier = new UserIdentifier(sessionKeyOption, usernameOption);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            @Override
            public Object execute() {
                log.info("[internal/permission_check] (keys={} permissions={}, {})", keys, permissions,
                    userIdentifier.getLogDescription());

                if (!userPrivilegeService.canUseSession(spa)) {
                    throw new AuthException(spa, "need privilege " + USE_SESSION);
                }

                permissionService.checkPermissions(spa, keys, permissions, userIdentifier);
                return new JsonSuccessResult().output();
            }
        });
    }

    /**
     * checks that session has the permissions specified, on the DNSkeys specified
     * (for use by Bridge/Datastore).
     *
     * @param keys the keys
     * @param permissions the permissions
     * @param sessionKeyOption the session key
     * @param usernameOption the username
     * @return the keys
     */
    @GET
    @Path("permission_filter")
    @Produces(MediaType.APPLICATION_JSON)
    public Object permissionFilter(final @QueryParam("keys") String keys,
                                   final @QueryParam("permissions") String permissions,
                                   final @QueryParam("sessionKey") String sessionKeyOption,
                                   final @QueryParam("username") String usernameOption) {
        if (Strings.isNullOrEmpty(keys))
            return new JsonErrorResult("missing param \"keys\"").asResponse(Status.BAD_REQUEST);
        if (Strings.isNullOrEmpty(permissions))
            return new JsonErrorResult("missing param \"permissions\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = SessionWrapper.createPrivilegedSession(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);
        final UserIdentifier userIdentifier = new UserIdentifier(sessionKeyOption, usernameOption);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            @Override
            public Object execute() {
                List<DNSkey> filteredKeys;
                log.info("[internal/permission_filter] (keys={} permissions={}, {})", keys, permissions,
                    userIdentifier.getLogDescription());

                if (!userPrivilegeService.canUseSession(spa)) {
                    throw new AuthException(spa, "need privilege " + USE_SESSION);
                }
                filteredKeys = permissionService.filterPermissions(spa, keys, permissions, userIdentifier);
                // convert to list of strings
                List<String> filtered = filteredKeys.stream().map(k -> k.getKey()).collect(Collectors.toList());
                return new JsonSingleResult("DNSkeys", filtered).output();
            }
        });
    }

    /**
     * checks that session has the permissions specified, on the DNSkeys specified
     * (for use by Bridge/Datastore).
     *
     * @param key the key
     * @param permissions the permissions
     * @return the permitted users
     */
    @GET
    @Path("permitted_users")
    @Produces(MediaType.APPLICATION_JSON)
    public Object getPermittedUsers(final @QueryParam("key") String key,
                                    final @QueryParam("permissions") String permissions) {
        if (Strings.isNullOrEmpty(key))
            return new JsonErrorResult("missing param \"key\"").asResponse(Status.BAD_REQUEST);
        if (Strings.isNullOrEmpty(permissions))
            return new JsonErrorResult("missing param \"permissions\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = SessionWrapper.createPrivilegedSession(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            @Override
            public Object execute() {
                log.info("[internal/permitted_users] (key={} permissions={}, {})", key, permissions);

                if (!userPrivilegeService.canUseSession(spa)) {
                    throw new AuthException(spa, "need privilege " + USE_SESSION);
                }
                Set<String> permittedUsers = permissionService.getInternalPermittedUsers(spa, key, permissions);
                return new JsonSingleResult("users", permittedUsers).output();
            }
        });
    }

    /**
     * Gets the username.
     *
     * @param sessionKey the session key
     * @return the username
     */
    @GET
    @Path("get_username")
    @Produces(MediaType.APPLICATION_JSON)
    public Object getUsername(final @QueryParam("sessionKey") String sessionKey) {
        if (Strings.isNullOrEmpty(sessionKey))
            return new JsonErrorResult("missing param \"sessionKey\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = SessionWrapper.createPrivilegedSession(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            @Override
            public Object execute() {
                log.info("[internal/get_username] (sessionKey={})", sessionKey);

                if (!userPrivilegeService.canUseSession(spa)) {
                    throw new AuthException(spa, "need privilege " + USE_SESSION);
                }
                Session session = sessionService.getSessionAsReference(sessionKey);

                JsonFlattenedMapResult result = new JsonFlattenedMapResult();
                result.put("username", session.getUsername());
                result.put("ttl", sessionService.getTimeRemaining(session));
                return result.output();
            }
        });
    }

    /**
     * Gets the groups that the user is a member of.
     *
     * @param sessionKeyOption the session key option
     * @param usernameOption the username option
     * @return the groups
     */
    @GET
    @Path("user_groups")
    @Produces(MediaType.APPLICATION_JSON)
    public Object userGroups(final @QueryParam("sessionKey") String sessionKeyOption,
                             final @QueryParam("username") String usernameOption) {

        final SessionWrapper sw = SessionWrapper.createPrivilegedSession(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);
        final UserIdentifier userIdentifier = new UserIdentifier(sessionKeyOption, usernameOption);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            @Override
            public Object execute() {
                List<String> groups;
                log.info("[internal/user_groups] ({})", userIdentifier.getLogDescription());

                if (!userPrivilegeService.canUseSession(spa)) {
                    throw new AuthException(spa, "need privilege " + USE_SESSION);
                }
                groups = groupService.getGroupsForUser(spa, userIdentifier);
                return new JsonSingleResult("groups", groups).output();
            }
        });
    }

    /**
     * Gets the permissions for key.
     *
     * @param key the key
     * @return the permissions for key
     */
    @GET
    @Path("permissions_for_key")
    @Produces(MediaType.APPLICATION_JSON)
    public Object getPermissionsForKey(final @QueryParam("key") String key) {
        if (Strings.isNullOrEmpty(key))
            return new JsonErrorResult("missing param \"key\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = SessionWrapper.createPrivilegedSession(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            @Override
            public Object execute() {
                log.info("[internal/permissions_for_key] (key={}", key);

                if (!userPrivilegeService.canUseSession(spa)) {
                    throw new AuthException(spa, "need privilege " + USE_SESSION);
                }
                DnsKeyDetailDto permissionsForKey = permissionService.getPermissionsForKey(spa, key);
                return new JsonSingleResult("permissions", permissionsForKey).output();
            }
        });
    }

    /**
     * Gets the transforms dependent on stream.
     *
     * @param key the key
     * @return the transforms dependent on stream
     */
    @GET
    @Path("transforms_dependent_on_stream")
    @Produces(MediaType.APPLICATION_JSON)
    public Object getTransformsDependentOnStream(final @QueryParam("key") String key) {
        if (Strings.isNullOrEmpty(key))
            return new JsonErrorResult("missing param \"key\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = SessionWrapper.createPrivilegedSession(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            @Override
            public Object execute() {
                log.info("[internal/get_stream_dependent_transforms] (key={}", key);

                if (!userPrivilegeService.canQueryStreams(spa)) {
                    throw new AuthException(spa, "need privilege " + QUERY_STREAMS);
                }
                List<TransformDependentDto> transforms = transformService.listDependentsBySourceStream(spa, key);
                return new JsonSingleResult("dependent_transforms", transforms).output();
            }
        });
    }

    /**
     * work out what kind of entity a specified DNSkey refers to, and return details.
     *
     * @param key the key
     * @param detailOption the detail option
     * @param permissionsOption the permissions option
     * @return the entity
     */
    @GET
    @Path("identify")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object identify(final @QueryParam("key") String key,
                           final @QueryParam("detail") String detailOption,
                           final @QueryParam("showPermissions") Boolean permissionsOption) {
        if (Strings.isNullOrEmpty(key))
            return new JsonErrorResult("missing query param \"key\"").asResponse(Status.BAD_REQUEST);
        final boolean withDetail = RegistryUtilities.DETAIL_FULL.equals(detailOption) ? true : false;
        final boolean withPermissions = permissionsOption != null && permissionsOption.booleanValue() ? true : false;

        final SessionWrapper sw = SessionWrapper.createPrivilegedSession(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            @Override
            public Object execute() {
                log.info("[internal/identify] key={}", key);

                if (!userPrivilegeService.canQueryStreams(spa)) {
                    throw new AuthException(spa, "need privilege " + QUERY_STREAMS);
                }

                Optional<SearchMetaService.SearchResult> result = searchMetaService.identify(spa, key, withDetail,
                    withPermissions);
                if (result.isPresent()) {
                    return new JsonSingleResult(result.get().description, result.get().value).output();
                } else {
                    throw new DataException("not recognised", Status.NOT_FOUND);
                }
            }
        });
    }

    /**
     * Gets the resource.
     *
     * @param key the key
     * @param name the name
     * @return the resource
     */
    @GET
    @Path("resource/{key}/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Object getResource(final @PathParam("key") String key,
                              final @PathParam("name") String name) {
        if (Strings.isNullOrEmpty(key))
            return new JsonErrorResult("missing param \"key\"").asResponse(Status.BAD_REQUEST);
        if (Strings.isNullOrEmpty(name))
            return new JsonErrorResult("missing parameter \"name\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = SessionWrapper.createPrivilegedSession(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            @Override
            public Object execute() {
                log.info("[resource/get] (key={}, name={})", key, name);

                if (!userPrivilegeService.canQueryStreams(spa)) {
                    throw new AuthException(spa, "need privilege " + QUERY_STREAMS);
                }
                Resource resource = resourceService.getResource(spa, key, name);
                return Response.ok(resource.getContent(), resource.getContentType()).build();
            }
        });
    }

    /**
     * return a list of all the transforms that have a "runSchedule" property.
     *
     * @return the transforms
     */
    @GET
    @Path("transforms_with_runSchedule")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object transformsWithRunSchedule() {

        final SessionWrapper sw = SessionWrapper.createPrivilegedSession(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            @Override
            public Object execute() {
                log.info("[internal/transforms_with_runSchedule]");

                if (!userPrivilegeService.canQueryStreams(spa)) {
                    throw new AuthException(spa, "need privilege " + QUERY_STREAMS);
                }
                return new JsonSingleResult("transforms", transformService.listTransformsWithRunSchedule(spa)).output();
            }
        });
    }

    /**
     * return a list of all pushsubscribers.
     *
     * @return the push subscribers
     */
    @GET
    @Path("push_subscribers")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object push_subscribers() {

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[push_subscribers/list]");

                if (!userPrivilegeService.canQueryStreams(spa)) {
                    throw new AuthException(spa, "need privilege " + QUERY_STREAMS);
                }
                List<PushSubscriber> subs = pushSubscriberService.getAll(spa);
                return new JsonSingleResult("pushSubscribers", PushSubscriberDetailDto.asList(subs)).output();
            }
        });
    }

}
