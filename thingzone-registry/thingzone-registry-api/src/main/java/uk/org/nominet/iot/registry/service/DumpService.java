/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.persistence.NoResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableSet;

import uk.org.nominet.iot.registry.common.AuthException;
import uk.org.nominet.iot.registry.dao.DNSdataDao;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.DataStreamDao;
import uk.org.nominet.iot.registry.dao.DeviceDao;
import uk.org.nominet.iot.registry.dao.HistoryDao;
import uk.org.nominet.iot.registry.dao.UserDao;
import uk.org.nominet.iot.registry.database.DatabaseDriver;
import uk.org.nominet.iot.registry.database.DbObjectStreamer;
import uk.org.nominet.iot.registry.model.DNSdata;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.Device;
import uk.org.nominet.iot.registry.model.History;
import uk.org.nominet.iot.registry.model.HistoryAction;
import uk.org.nominet.iot.registry.model.ObjectType;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.model.Privilege;
import uk.org.nominet.iot.registry.model.User;
import uk.org.nominet.iot.registry.service.dto.DNSdataDto;
import uk.org.nominet.iot.registry.service.dto.DeviceSummaryDetailDto;
import uk.org.nominet.iot.registry.service.dto.DumpObjects;
import uk.org.nominet.iot.registry.service.dto.DumpObjects.DataStreamWithPermissionsAndDeviceKeys;
import uk.org.nominet.iot.registry.service.dto.DumpObjects.DeviceWithPermissions;
import uk.org.nominet.iot.registry.service.dto.DumpObjects.HistoryDto;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;

public class DumpService {
    private final static Logger log = LoggerFactory.getLogger(DumpService.class);

    private DeviceDao deviceDao;
    private DataStreamDao dataStreamDao;
    private DNSdataDao dnsDataDao;
    private HistoryDao historyDao;
    private UserDao userDao;

    private UserPrivilegeService userPrivilegeService;

    // we hang onto DumpRetriever because it caches some stuff
    private DumpRetriever dumpRetriever;

    public DumpService() {
        DaoFactory df = DaoFactoryManager.getFactory();

        deviceDao = df.deviceDao();
        dataStreamDao = df.dataStreamDao();
        dnsDataDao = df.dnsDataDao();
        userDao = df.userDao();

        userPrivilegeService = new UserPrivilegeService();
        historyDao = df.historyDao();

        dumpRetriever = new DumpRetriever(new UnmanagedDumpRetrieverAccessor());
    }

    public void checkUserCanDump(SessionPermissionAssistant spa) {
        if (!userPrivilegeService.canDump(spa)) {
            throw new AuthException(spa, "need privilege " + Privilege.DUMP_ALL);
        }
    }

    public void dumpDevicesToStreamAsJson(SessionPermissionAssistant spa, Writer outputWriter) throws IOException {
        checkUserCanDump(spa);

        ObjectMapper mapper = new ObjectMapper();

        DbObjectStreamer<Device> deviceStreamer = deviceDao.streamAll();
        log.info("dumping devices ...");
        int deviceCount = 0;
        int dnsCount = 0;
        int permissionSetCount = 0;
        int permissionCount = 0;
        while (!deviceStreamer.atEnd()) {
            Device device = deviceStreamer.read();

            // get DNSdata for the device
            List<DNSdata> dnsDatas = dnsDataDao.unmanagedListByKey(device.getKey());

            // get permissions
            Map<String, Set<Permission>> permissionsAsUsers
                = dumpRetriever.getUserPermissionsForObject(device.getKey());
            Map<String, Set<Permission>> permissionsAsGroups
                = dumpRetriever.getGroupPermissionsForObject(device.getKey());

            DumpObjects.DeviceWithPermissions output
                = new DumpObjects.DeviceWithPermissions(new DeviceSummaryDetailDto(device.getKey(),
                                                                                   device.getMetadata(),
                                                                                   device.getLocation(),
                                                                                   null,
                                                                                   DnsDataToDtos(dnsDatas),
                                                                                   null,
                                                                                   permissionsAsUsers,
                                                                                   permissionsAsGroups));
            outputWriter.write(mapper.writeValueAsString(output));
            outputWriter.write(",\n");
            deviceCount++;
            dnsCount += dnsDatas.size();
            permissionSetCount += permissionsAsUsers.size();
            for (Set<Permission> perms : permissionsAsUsers.values()) {
                permissionCount += perms.size();
            }
            if (deviceCount % 1000 == 0) {
                log.info("dump progress: {} devices", deviceCount);
            }
        }
        deviceStreamer.close();
        log.info("dumped {} devices, including {} DNS records and {} permissions (in {} sets)", deviceCount, dnsCount,
            permissionCount, permissionSetCount);
    }

    public void dumpDataStreamsToStreamAsJson(SessionPermissionAssistant spa, Writer outputWriter) throws IOException {
        checkUserCanDump(spa);

        ObjectMapper mapper = new ObjectMapper();

        User publicUser = userDao.getUserByUsername(DatabaseDriver.PUBLIC_USERNAME);

        DbObjectStreamer<DataStream> dataStreamStreamer = dataStreamDao.streamAll();
        log.info("dumping dataStreams ...");
        int dataStreamCount = 0;
        int transformCount = 0;
        int dnsCount = 0;
        int permissionSetCount = 0;
        int permissionCount = 0;
        int pushSubscriberCount = 0;
        int pullSubscriptionCount = 0;
        while (!dataStreamStreamer.atEnd()) {
            DataStream dataStream = dataStreamStreamer.read();

            DumpObjects.GenericStreamDetails genericStreamDetails = dumpRetriever.getGenericStreamDetails(dataStream);

            if (dataStream.getIsTransform()) {
                // transform has transform properties incl. source streams
                DumpObjects.TransformWithPermissions twp
                    = dumpRetriever.getTransformSpecificDetails(dataStream, genericStreamDetails, publicUser);
                outputWriter.write(mapper.writeValueAsString(twp));
                transformCount++;
            } else {
                // dataStream can also have a pull subscription
                DataStreamWithPermissionsAndDeviceKeys dwp
                    = dumpRetriever.getDataStreamSpecificDetails(dataStream, genericStreamDetails);
                outputWriter.write(mapper.writeValueAsString(dwp));
                dataStreamCount++;
            }

            // write delimiter to HTTP stream
            outputWriter.write(",\n");
            dnsCount += genericStreamDetails.dnsDatas.size();
            permissionSetCount += genericStreamDetails.permissionsAsUsers.size();
            for (Set<Permission> perms : genericStreamDetails.permissionsAsUsers.values()) {
                permissionCount += perms.size();
            }
            pushSubscriberCount += genericStreamDetails.pushSubscribers.size();
            if ((dataStreamCount + transformCount) % 1000 == 0) {
                log.info("dump progress: {} dataStreams {} transforms", dataStreamCount, transformCount);
            }
        }
        log.info(
            "dumped {} DataStreams & {} Transforms" + ", including {} DNS records and {} permissions (in {} sets)"
                + ", {} PullSubscriptions, {} PushSubscribers",
            dataStreamCount, transformCount, dnsCount, permissionCount, permissionSetCount, pullSubscriptionCount,
            pushSubscriberCount);
        dataStreamStreamer.close();
    }

    // dump just one datastream, to get parent devices & permissions
    public DataStreamWithPermissionsAndDeviceKeys dumpDataStream(String dataStreamKey) {
        return dumpRetriever.dumpDataStream(dataStreamKey);
    }

    private List<DNSdataDto> DnsDataToDtos(List<DNSdata> dnsDatas) {
        List<DNSdataDto> dnsDataDtos = new ArrayList<>();
        for (DNSdata dnsData : dnsDatas) {
            dnsDataDtos.add(new DNSdataDto(dnsData));
        }
        return dnsDataDtos;
    }

    public void dumpStreamChangesAsJson(SessionPermissionAssistant spa,
                                        Writer outputWriter,
                                        Date date) throws IOException {
        checkUserCanDump(spa);

        User publicUser = userDao.getUserByUsername(DatabaseDriver.PUBLIC_USERNAME);
        ObjectMapper mapper = new ObjectMapper();
        int historyCount = 0;
        String key = null;
        DumpObjects.HistoryDto outputData = null;
        Set<Long> removedPermissions = new HashSet<>();

        try (DbObjectStreamer<History> historyStreamer = historyDao.streamFrom(date)) {
            log.info("dumping changes since {}.", date);
            while (!historyStreamer.atEnd()) {
                History history = historyStreamer.read();
                if (key == null) {
                    key = history.getKey();
                    outputData = initialiseHistoryOutput(history);
                } else if (!key.equals(history.getKey())) {
                    // started a new record - output the data for the previous key
                    writeOutputData(outputData, removedPermissions, key, publicUser, outputWriter, mapper);

                    key = history.getKey();
                    outputData = initialiseHistoryOutput(history);
                }
                if (history.getObjectType() == ObjectType.DATASTREAM && outputData.dataStream == null) {
                    setDataStreamData(key, publicUser, outputData);
                } else if (history.getObjectType() == ObjectType.DEVICE && outputData.device == null) {
                    setDeviceData(key, outputData);
                }

                if (history.getAction() == HistoryAction.PERMISSION_REMOVED) {
                    removedPermissions.add(history.getAccountId());
                }
            }
        }
        writeOutputData(outputData, removedPermissions, key, publicUser, outputWriter, mapper);
        log.info("dumped {} history records", historyCount);
    }

    /**
     * Initialise the history dto for outputting the data for the changes
     */
    private DumpObjects.HistoryDto initialiseHistoryOutput(History history) {
        log.info("new key {}", history.getKey());
        return new DumpObjects.HistoryDto(history.getKey());
    }

    /**
     * Add the dataStream or Transform data to the History output
     */
    private void setDataStreamData(String key, User publicUser, DumpObjects.HistoryDto outputData) {
        outputData.setEntityType(ObjectType.DATASTREAM.toString());
        try {
            DataStream dataStream = dataStreamDao.getByName(key);
            DumpObjects.GenericStreamDetails genericStreamDetails = dumpRetriever.getGenericStreamDetails(dataStream);
            if (dataStream.getIsTransform()) {
                DumpObjects.TransformWithPermissions twp
                    = dumpRetriever.getTransformSpecificDetails(dataStream, genericStreamDetails, publicUser);
                outputData.setTransform(twp.transform);
            } else {
                DataStreamWithPermissionsAndDeviceKeys dumpDataStream
                    = dumpRetriever.getDataStreamSpecificDetails(dataStream, genericStreamDetails);
                outputData.setDataStream(dumpDataStream.dataStream);
            }
        } catch (NoResultException e) {
            log.info("DataStream deleted {}", key);
        }
    }

    /**
     * add the device info to the History output
     */
    private void setDeviceData(String key, DumpObjects.HistoryDto outputData) {
        outputData.setEntityType(ObjectType.DEVICE.toString());
        try {
            Device device = deviceDao.getByName(key);
            DeviceWithPermissions dumpDevice = dumpRetriever.dumpDevice(device);
            outputData.setDevice(dumpDevice.device);
        } catch (NoResultException e) {
            log.info("Device deleted {}", key);
        }
    }

    /**
     * Do some clean up and write the History DTO to the output stream
     */
    private void writeOutputData(HistoryDto outputData,
                                 Set<Long> removedAccountPermissions,
                                 String key,
                                 User publicUser,
                                 Writer outputWriter,
                                 ObjectMapper mapper) throws JsonProcessingException, IOException {
        if (outputData == null)
            return;
        // deal with permissions
        if (!removedAccountPermissions.isEmpty()) {
            Map<String, Set<Permission>> permissionsAsUsers
                = dumpRetriever.getUserPermissionsForObject(new DNSkey(outputData.key));
            Map<String, Set<Permission>> permissionsAsGroups
                = dumpRetriever.getGroupPermissionsForObject(new DNSkey(outputData.key));

            Map<String, Set<Permission>> userPermissions = new TreeMap<>();
            Map<String, Set<Permission>> groupPermissions = new TreeMap<>();

            for (Long accountId : removedAccountPermissions) {
                // check user then group for account details
                String usernameForAccountId = dumpRetriever.getUsernameForAccountId(accountId);
                if (usernameForAccountId != null) {
                    if (!permissionsAsUsers.containsKey(usernameForAccountId)) {
                        userPermissions.put(usernameForAccountId, ImmutableSet.of());
                    }
                } else {
                    String groupnameForAccountId = dumpRetriever.getGroupForAccountId(accountId);
                    if (groupnameForAccountId != null && !permissionsAsGroups.containsKey(groupnameForAccountId)) {
                        groupPermissions.put(groupnameForAccountId, ImmutableSet.of());
                    }
                }
            }
            outputData.removedUserPermissions = userPermissions.keySet();
            outputData.removedGroupPermissions = groupPermissions.keySet();
            removedAccountPermissions.clear();
        }
        if (outputData.entityType == null) {
            updateEntityType(key, publicUser, outputData);
        }
        // write the output
        outputWriter.write(mapper.writeValueAsString(outputData));
        outputWriter.write(",\n");
    }

    /**
     * If the key has had no device or datastream specific changes, then lookup the type and populate the data for it.
     */
    private void updateEntityType(String key, User publicUser, HistoryDto outputData) {
        if (deviceDao.exists(key)) {
            setDeviceData(key, outputData);
        } else if (dataStreamDao.exists(key)) {
            setDataStreamData(key, publicUser, outputData);
        } else {
            outputData.setEntityType(ObjectType.ENTITY.toString());
        }
    }

}
