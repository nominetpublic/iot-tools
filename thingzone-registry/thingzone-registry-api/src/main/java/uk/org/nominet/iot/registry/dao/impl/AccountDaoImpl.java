/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import java.util.List;

import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;

import uk.org.nominet.iot.registry.dao.AbstractJpaDao;
import uk.org.nominet.iot.registry.dao.AccountDao;
import uk.org.nominet.iot.registry.dao.dto.AccountId;
import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.Group;
import uk.org.nominet.iot.registry.model.User;

class AccountDaoImpl extends AbstractJpaDao<Account> implements AccountDao {
    private final static Logger log = LoggerFactory.getLogger(AccountDaoImpl.class);

    AccountDaoImpl() {
        super(Account.class);
    }

    @Override
    public Account create(User owner) {
        Account account = new Account(owner, null);
        persist(account);
        flush(); // get the Id populated
        if (owner != null) {
            log.info("created account for user id={}", owner.getId());
        } else {
            log.info("created account for limited user");
        }
        return account;
    }

    @Override
    public Account getByOwner(User owner) {
        return loadByMatch("owner", owner);
    }

    @Override
    public Account getById(AccountId id) {
        return loadByMatch("id", id.getId());
    }

    @Override
    public Account createGroupAccount(Group group) {
        Account account = new Account(null, group);
        persist(account);
        flush(); // get the Id populated
        log.info("created account for group = {}", group.getName());
        return account;
    }

    @Override
    public Account getByGroup(Group group) {
        return loadByMatch("group", group);
    }

    @Override
    public List<Account> listByGroups(List<Group> groups) {
        checkTransaction();
        if (groups == null || groups.isEmpty())
            return ImmutableList.of();

        TypedQuery<Account> query = getEntityManager().createNamedQuery("Account.findByGroups", getEntityClass());
        query.setParameter("groups", groups);
        return query.getResultList();
    }

    @Override
    public Account getByGroupname(String groupname) {
        checkTransaction();

        TypedQuery<Account> query = getEntityManager().createQuery(
            "SELECT ac FROM Account ac JOIN Group g ON ac.group = g WHERE g.name = :groupname",
            getEntityClass());
        query.setParameter("groupname", groupname);
        return query.getSingleResult();
    }

    @Override
    public Account getByUsername(String username) {
        checkTransaction();

        TypedQuery<Account> query = getEntityManager().createQuery(
            "SELECT ac FROM Account ac JOIN User u ON ac.owner = u JOIN UserCredentials uc ON uc.user = u "
                + " WHERE uc.username = :username",
            getEntityClass());
        query.setParameter("username", username);
        return query.getSingleResult();
    }

}
