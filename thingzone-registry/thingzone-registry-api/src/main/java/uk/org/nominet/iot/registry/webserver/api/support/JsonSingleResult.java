/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api.support;

import java.util.LinkedHashMap;

/**
 * JsonSingleResult is a container for a "success" response, with single named content - contents name set at creation -
 * single content set at creation
 *
 * {"result":"ok","<contents name>":<contents stored> }
 *
 * Usage example: return new JsonResult("thingy", "stuff").output();
 *
 */
public class JsonSingleResult {
    final static String SUCCESS = "ok";

    LinkedHashMap<String, Object> resultContainer = new LinkedHashMap<String, Object>();

    public JsonSingleResult(String contentName, Object content) {
        resultContainer.put("result", SUCCESS);
        resultContainer.put(contentName, content);
    }

    public LinkedHashMap<String, Object> output() {
        return resultContainer;
    }
}
