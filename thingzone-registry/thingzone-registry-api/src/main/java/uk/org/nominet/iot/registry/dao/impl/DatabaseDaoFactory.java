/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import uk.org.nominet.iot.registry.dao.*;

public class DatabaseDaoFactory implements DaoFactory {

    @Override
    public DNSkeyDao dnsKeyDao() {
        return new DNSkeyDaoImpl();
    }

    @Override
    public DataStreamDao dataStreamDao() {
        return new DataStreamDaoImpl();
    }

    @Override
    public RegistryPropertyDao registryPropertyDao() {
        return new RegistryPropertyDaoImpl();
    }

    @Override
    public UserDao userDao() {
        return new UserDaoImpl();
    }

    public UserPrivilegeDao userPrivilegeDao() {
        return new UserPrivilegeDaoImpl();
    }

    @Override
    public UserCredentialsDao userCredentialsDao() {
        return new UserCredentialsDaoImpl();
    }

    @Override
    public SessionDao sessionDao() {
        return new SessionDaoImpl();
    }

    @Override
    public DeviceDao deviceDao() {
        return new DeviceDaoImpl();
    }

    @Override
    public StreamOnDeviceDao streamOnDeviceDao() {
        return new StreamOnDeviceDaoImpl();
    }

    @Override
    public TransformFunctionDao transformFunctionDao() {
        return new TransformFunctionDaoImpl();
    }

    @Override
    public TransformDao transformDao() {
        return new TransformDaoImpl();
    }

    @Override
    public TransformSourceDao transformSourceDao() {
        return new TransformSourceDaoImpl();
    }

    @Override
    public TransformOutputStreamDao transformOutputStreamDao() {
        return new TransformOutputStreamDaoImpl();
    }

    @Override
    public DNSdataDao dnsDataDao() {
        return new DNSdataDaoImpl();
    }

    @Override
    public ZoneFileChangeDao zoneFileChangeDao() {
        return new ZoneFileChangeDaoImpl();
    }

    @Override
    public PushSubscriberDao pushSubscriberDao() {
        return new PushSubscriberDaoImpl();
    }

    @Override
    public PullSubscriptionDao pullSubscriptionDao() {
        return new PullSubscriptionDaoImpl();
    }

    @Override
    public AccountDao accountDao() {
        return new AccountDaoImpl();
    }

    @Override
    public ACLDao aclDao() {
        return new ACLDaoImpl();
    }

    @Override
    public ResourceDao resourceDao() {
        return new ResourceDaoImpl();
    }

    @Override
    public HistoryDao historyDao() {
        return new HistoryDaoImpl();
    }

    @Override
    public GroupDao groupDao() {
        return new GroupDaoImpl();
    }

    @Override
    public GroupUserDao groupUserDao() {
        return new GroupUserDaoImpl();
    }

    @Override
    public UserTokenDao userTokenDao() {
        return new UserTokenDaoImpl();
    }

}
