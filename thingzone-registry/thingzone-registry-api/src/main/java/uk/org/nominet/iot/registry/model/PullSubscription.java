/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import org.eclipse.persistence.annotations.Customizer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonRawValue;

import uk.org.nominet.iot.registry.database.ColumnPosition;
import uk.org.nominet.iot.registry.database.ColumnPositionCustomizer;
import uk.org.nominet.iot.registry.database.DatabaseDriver;

import javax.persistence.*;

import java.util.Date;

@Entity
@Table(name = "PullSubscriptions", schema = DatabaseDriver.SCHEMA_NAME,
       uniqueConstraints = { @UniqueConstraint(name = "UQ_PullSubscriptions", columnNames = { "streamKey" }) })
@Customizer(ColumnPositionCustomizer.class)
public class PullSubscription {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ColumnPosition(position = 1)
    @JsonIgnore
    private Long id;

    @JoinColumn(name = "streamKey", referencedColumnName = "DNSkey", nullable = false)
    @ColumnPosition(position = 2)
    private DataStream dataStream;

    @Column(name = "uri", nullable = false)
    @ColumnPosition(position = 3)
    private String uri;

    @Column(name = "uri_params", columnDefinition = "JSONB NOT NULL", nullable = false) // empty="{}"
    @Convert(converter = JsonbStringConverter.class) // our custom (to/from String) converter
    @ColumnPosition(position = 4)
    @JsonRawValue
    private String uriParams;

    @Column(name = "interval", nullable = false) // seconds
    @ColumnPosition(position = 5)
    private Integer interval;

    @Column(name = "aligned") // default = null, not aligned
    @ColumnPosition(position = 6)
    private Integer aligned;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "LAST_UPDATED", updatable = false, nullable = false)
    @ColumnPosition(position = 7)
    @JsonIgnore
    private Date lastUpdated;

    @PrePersist
    void initLastUpdated() {
        this.lastUpdated = new Date();
    }

    PullSubscription() {}

    public PullSubscription(DataStream dataStream, String uri, String uriParams, Integer interval, Integer aligned) {
        this.dataStream = dataStream;
        this.uri = uri;
        this.uriParams = uriParams;
        this.interval = interval;
        this.aligned = aligned;
    }

    public DataStream getDataStream() {
        return dataStream;
    }

    public Long getId() {
        return id;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getUri() {
        return uri;
    }

    public void setUriParams(String uriParams) {
        this.uriParams = uriParams;
    }

    public String getUriParams() {
        return uriParams;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    public Integer getInterval() {
        return interval;
    }

    public void setAligned(Integer aligned) {
        this.aligned = aligned;
    }

    public Integer getAligned() {
        return aligned;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    @Override
    public String toString() {
        return "PullSubscription{" + "id=" + id + ", dataStream=" + dataStream + ", uri='" + uri + '\''
            + ", uriParams='" + uriParams + '\'' + ", interval=" + interval + ", aligned=" + aligned + ", lastUpdate="
            + lastUpdated + '}';
    }

}
