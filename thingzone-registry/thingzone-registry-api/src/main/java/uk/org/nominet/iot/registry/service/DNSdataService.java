/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.org.nominet.iot.registry.common.AuthException;
import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.ACLDao;
import uk.org.nominet.iot.registry.dao.DNSdataDao;
import uk.org.nominet.iot.registry.dao.DNSkeyDao;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.ZoneFileChangeDao;
import uk.org.nominet.iot.registry.ddns.validator.RecordValidator;
import uk.org.nominet.iot.registry.model.DNSdata;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.model.ZoneFileChange;
import uk.org.nominet.iot.registry.model.ZoneFileChangeOperation;
import uk.org.nominet.iot.registry.service.dto.DNSdataDto;
import uk.org.nominet.iot.registry.webserver.api.support.EntityLoader;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;

/**
 * DNSdataService handles the dual aspects of DNS records; - they're updated etc by API in DNSdata table - they're
 * written by API to ZoneFileChanges table - they're read/updated by DDNS in ZoneFileChanges table
 */
public class DNSdataService {
    private final static Logger log = LoggerFactory.getLogger(DNSdataService.class);

    DNSkeyDao dnsKeyDao;
    DNSdataDao dnsDataDao;
    ZoneFileChangeDao zoneFileChangeDao;
    ACLDao aclDao;
    ACLService aclService;

    public DNSdataService() {
        DaoFactory df = DaoFactoryManager.getFactory();
        dnsDataDao = df.dnsDataDao();
        zoneFileChangeDao = df.zoneFileChangeDao();
        dnsKeyDao = df.dnsKeyDao();
        aclDao = df.aclDao();
        aclService = new ACLService();
    }

    public List<DNSdata> dnsDataListByKey(SessionPermissionAssistant spa, String key) {
        DNSkey dnsKey = EntityLoader.getDnsKey(spa, key, Permission.DISCOVER);
        return dnsDataListByKey(spa, dnsKey);
    }

    public List<DNSdata> dnsDataListByKey(SessionPermissionAssistant spa, DNSkey dnsKey) {
        // require MODIFY to retrieve DNSdata
        if (aclService.isAllowed(spa, dnsKey, Permission.MODIFY)) {
            return dnsDataDao.unmanagedListByKey(dnsKey);
        } else {
            return null; // null, not an empty list, so hidden from user
        }
    }

    public List<DNSdata> dnsDataListAll(SessionPermissionAssistant spa, int size, int page) {
        List<DNSdata> dnsData = new ArrayList<>();
        for (DNSkey dnsKey : dnsKeyDao.getAllowed(spa.getRelevantAccounts(), Permission.DISCOVER, size, page)) {
            dnsData.addAll(dnsDataDao.unmanagedListByKey(dnsKey));
        }
        return dnsData;
    }

    public boolean dnsDataExists(DNSdataDto ddr) {
        return dnsDataDao.exists(ddr);
    }

    public void addRecord(SessionPermissionAssistant spa, DNSdataDto ddr) {
        if (!aclService.isAllowed(spa, ddr.getDnsKey(), Permission.MODIFY)) {
            throw new AuthException(spa, ddr.getDnsKey(), "need MODIFY permission");
        }

        // check record
        RecordValidator.validate(ddr);

        // store
        dnsDataDao.create(ddr);

        ZoneFileChange zfc = zoneFileChangeDao.createRecord(ZoneFileChangeOperation.ADD, ddr);
        log.info("stored ZFC: {}", zfc);
    }

    public void removeRecord(SessionPermissionAssistant spa, DNSdataDto ddr) {
        if (!aclService.isAllowed(spa, ddr.getDnsKey(), Permission.MODIFY)) {
            throw new AuthException(spa, ddr.getDnsKey(), "need MODIFY permission");
        }

        try {
            DNSdata dnsData = dnsDataDao.get(ddr);
            dnsDataDao.delete(dnsData);

            ZoneFileChange zfc = zoneFileChangeDao.createRecord(ZoneFileChangeOperation.REMOVE, ddr);
            log.info("stored ZFC: {}", zfc);
        } catch (NoResultException e) {
            throw new DataException("DNS record does not exist", Status.NOT_FOUND);
        }
    }

    public void updateRecord(SessionPermissionAssistant spa, DNSdataDto ddr) {
        // remove if exists
        if (dnsDataDao.exists(ddr)) {
            // not worth updating in place, as we need to write to zonefile anyway
            removeRecord(spa, ddr);
        }
        addRecord(spa, ddr);
    }

    public void removeAllRecords(DNSkey dnsKey) {
        for (DNSdata dnsData : dnsDataDao.listByKey(dnsKey)) {
            ZoneFileChange zfc
                = zoneFileChangeDao.createRecord(ZoneFileChangeOperation.REMOVE, new DNSdataDto(dnsData));
            log.info("stored ZFC: {}", zfc);
            dnsDataDao.delete(dnsData);
            log.info("deleted DNS data {}", dnsData);
        }
    }

    public List<ZoneFileChange> getUnprocessedChanges(int limit) {
        return zoneFileChangeDao.getUnprocessedChanges(limit);
    }

    public void setSerial(ZoneFileChange change, long serial) {
        zoneFileChangeDao.setSerial(change, serial);
    }

    public static List<DNSdataDto> asDto(List<DNSdata> dnsDatas) {
        // get DNSdata per stream
        List<DNSdataDto> dtos;
        if (dnsDatas == null) {
            dtos = null;
        } else {
            dtos = new ArrayList<>();
            for (DNSdata d : dnsDatas) {
                dtos.add(new DNSdataDto(d));
            }
        }
        return dtos;
    }

}
