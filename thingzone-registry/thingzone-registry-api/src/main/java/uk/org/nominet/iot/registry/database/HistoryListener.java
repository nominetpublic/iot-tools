/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.database;

import java.time.Instant;
import java.util.Date;

import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;

import org.eclipse.persistence.descriptors.DescriptorEvent;
import org.eclipse.persistence.descriptors.DescriptorEventAdapter;

import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.HistoryDao;
import uk.org.nominet.iot.registry.model.ACL;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.Device;
import uk.org.nominet.iot.registry.model.History;
import uk.org.nominet.iot.registry.model.HistoryAction;
import uk.org.nominet.iot.registry.model.ObjectType;
import uk.org.nominet.iot.registry.model.StreamOnDevice;
import uk.org.nominet.iot.registry.model.Transform;

public class HistoryListener extends DescriptorEventAdapter {

    private HistoryDao historyDao;

    public HistoryListener() {
        DaoFactory df = DaoFactoryManager.getFactory();
        historyDao = df.historyDao();
    }

    @PostUpdate
    public void postUpdate(DescriptorEvent event) {
        createLogEntryFor(event, HistoryAction.MODIFIED);
    }

    @PostPersist
    public void postInsert(DescriptorEvent event) {
        createLogEntryFor(event, HistoryAction.CREATED);
    }

    @PostRemove
    public void postDelete(DescriptorEvent event) {
        createLogEntryFor(event, HistoryAction.DELETED);
    }

    private void createLogEntryFor(DescriptorEvent event, HistoryAction action) {
        Object entity = event.getObject();
        Instant now = Instant.now();
        Date updated = Date.from(now);
        if (entity instanceof Device) {
            String key = ((Device) entity).getKey().getKey();
            History history = historyDao.create(key, updated, ObjectType.DEVICE, action);
            event.getSession().insertObject(history);
        }
        if (entity instanceof DNSkey) {
            String key = ((DNSkey) entity).getKey();
            History history = historyDao.create(key, updated, ObjectType.ENTITY, action);
            event.getSession().insertObject(history);
        }
        if (entity instanceof DataStream) {
            String key = ((DataStream) entity).getKey().getKey();
            History history = historyDao.create(key, updated, ObjectType.DATASTREAM, action);
            event.getSession().insertObject(history);
        }
        if (entity instanceof Transform) {
            String key = ((Transform) entity).getDataStream().getKey().getKey();
            History history = historyDao.create(key, updated, ObjectType.DATASTREAM, action);
            event.getSession().insertObject(history);
        }
        if (entity instanceof ACL) {
            HistoryAction permissionAction = (action == HistoryAction.CREATED) ? HistoryAction.PERMISSION_CREATED
                : HistoryAction.PERMISSION_REMOVED;
            String aclKey = ((ACL) entity).getKey().getKey();
            History history = historyDao.create(aclKey, updated, ObjectType.ENTITY, permissionAction);
            history.setAccountId(((ACL) entity).getAccount().getId());
            event.getSession().insertObject(history);
        }
        if (entity instanceof StreamOnDevice) {
            String dataStreamKey = ((StreamOnDevice) entity).getDataStream().getKey().getKey();
            History dataStreamHistory = historyDao.create(dataStreamKey, updated, ObjectType.DATASTREAM,
                HistoryAction.RELATIONSHIP);
            event.getSession().insertObject(dataStreamHistory);

            String deviceKey = ((StreamOnDevice) entity).getDevice().getKey().getKey();
            History deviceHistory = historyDao.create(deviceKey, updated, ObjectType.DEVICE,
                HistoryAction.RELATIONSHIP);
            event.getSession().insertObject(deviceHistory);
        }
    }
}
