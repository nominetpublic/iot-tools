/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.database;

import static uk.org.nominet.iot.registry.database.DatabaseDriver.USERADMIN_USERNAME;
import static uk.org.nominet.iot.registry.database.DatabaseDriver.ALL_USERS_GROUP;
import static uk.org.nominet.iot.registry.model.RegistryProperty.PROPERTY_DB_CREATED;
import static uk.org.nominet.iot.registry.model.RegistryProperty.PROPERTY_DB_CREATED_BY;
import static uk.org.nominet.iot.registry.model.RegistryProperty.PROPVAL_SCHEMA_CREATOR;

import java.util.Date;
import java.util.Properties;

import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.RegistryPropertyDao;
import uk.org.nominet.iot.registry.dao.impl.DatabaseDaoFactory;
import uk.org.nominet.iot.registry.model.Group;
import uk.org.nominet.iot.registry.model.Privilege;
import uk.org.nominet.iot.registry.model.User;

/**
 *
 * SchemaDefinition
 *
 * Provides a main() function that sets up schema / creates DDL
 *
 * See also persistence.xml
 *
 */
public class SchemaCreator {

    final String ddlOutputFile = "create_tables.sql";

    final static String USAGE = "usage: java -cp <jar> " + SchemaCreator.class.getName() + " <DDL|create>";

    final static int USAGE_ERR = 1;
    final static int DB_ERR = 2;

    public static String DDL_CREATION = "eclipselink.ddl-generation";
    public static String DDL_OUTPUT_MODE = "eclipselink.ddl-generation.output-mode";
    public static String DDL_FILENAME = "eclipselink.create-ddl-jdbc-file-name";

    public static String DDL_OUTPUT_MODE_FILE = "sql-script";
    public static String DDL_OUTPUT_MODE_DATABASE = "database";
    public static String DDL_CREATE_TABLES = "create-tables";
    public static String DDL_DROP_AND_CREATE_TABLES = "drop-and-create-tables";

    public static void main(String[] args) {
        SchemaCreator schemaCreator = new SchemaCreator();
        schemaCreator.run(args);
    }

    void run(String[] args) {
        if (args.length != 1) {
            System.err.println(USAGE);
            System.exit(USAGE_ERR);
        }

        DaoFactoryManager.setFactory(new DatabaseDaoFactory());

        Properties eclipseLinkProps = null;
        if ("DDL".equals(args[0])) {
            eclipseLinkProps = propertiesForCreatingDDL(ddlOutputFile);
        } else if ("create".equals(args[0])) {
            eclipseLinkProps = propertiesForCreatingTables(true);
        } else {
            System.err.println(USAGE);
            System.exit(USAGE_ERR);
        }

        System.out.println("Creating IoT schema...");

        DatabaseDriver.setEclipseLinkProperties(eclipseLinkProps);

        // database config
        uk.org.nominet.iot.registry.database.ConfigFactory.loadConfig("config/db.properties");
        DatabaseDriver.setConnectionProperties(
            uk.org.nominet.iot.registry.database.ConfigFactory.getConfig().getJdbcProperties());

        DatabaseDriver.globalInitialise();

        if ("create".equals(args[0])) {
            setupDbInitialData();
        }

        System.out.println("schema created");

        DatabaseDriver.disposeEntityManagerFactory();

        System.out.println("database closed");
    }

    public void setupDbInitialData() {
        setDbCreatedDate();
        createPublicUser();
    }

    private void setDbCreatedDate() {
        try {
            DatabaseDriver.beginMutableTransaction();

            RegistryPropertyDao dao = DaoFactoryManager.getFactory().registryPropertyDao();
            dao.setProperty(PROPERTY_DB_CREATED, new Date().toString());
            dao.setProperty(PROPERTY_DB_CREATED_BY, PROPVAL_SCHEMA_CREATOR);

            DatabaseDriver.commitTransaction();
            System.out.println("creation date property stored");
        } catch (Exception e) {
            e.printStackTrace();
            DatabaseDriver.abortTransaction();
            System.exit(DB_ERR);
        }
    }

    private void createPublicUser() {
        try {
            DatabaseDriver.beginMutableTransaction();

            DaoFactory df = DaoFactoryManager.getFactory();

            // create "public" user
            User publicUser = df.userDao().create();
            df.userCredentialsDao().createNoLogon(DatabaseDriver.PUBLIC_USERNAME, publicUser);
            df.accountDao().create(publicUser);

            // create "user admin" user
            // & has password=username as bootstrap
            User adminUser = df.userDao().create();
            df.userCredentialsDao().createNoLogon(USERADMIN_USERNAME, adminUser);
            df.accountDao().create(adminUser);
            // admin needs USER_ADMIN privilege
            df.userPrivilegeDao().create(adminUser, Privilege.USER_ADMIN);

            System.out.println("'public', 'user_admin' users created");

            // create 'all_users' group
            Group all_users = df.groupDao().create(adminUser, ALL_USERS_GROUP);
            df.accountDao().createGroupAccount(all_users);
            System.out.println("'all_users' group created");

            DatabaseDriver.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
            DatabaseDriver.abortTransaction();
            System.exit(DB_ERR);
        }
    }

    /**
     * we need to choose whether we create tables in DB so that we can automatically
     * create them, without automatically breaking them :-) & we don't want to have
     * to hack about with persistence.xml e.g.
     * <property name="eclipselink.ddl-generation" value="create-tables" />
     * <property name="eclipselink.ddl-generation.output-mode" value="database" />
     * see http://eclipse.org/eclipselink/documentation/2.5/jpa/extensions/
     * p_ddl_generation.htm
     *
     */
    static Properties propertiesForCreatingDDL(String ddlFilename) {
        Properties props = new Properties();
        props.setProperty(DDL_CREATION, DDL_CREATE_TABLES);
        props.setProperty(DDL_FILENAME, ddlFilename);
        props.setProperty(DDL_OUTPUT_MODE, DDL_OUTPUT_MODE_FILE);
        return props;
    }

    public static Properties propertiesForCreatingTables(boolean dropTables) {
        Properties props = new Properties();
        props.setProperty(DDL_CREATION, dropTables ? DDL_DROP_AND_CREATE_TABLES : DDL_CREATE_TABLES);
        props.setProperty(DDL_OUTPUT_MODE, DDL_OUTPUT_MODE_DATABASE);
        return props;
    }

    static Properties propertiesForUsingExistingTables() {
        return new Properties(); // nothing
    }

}
