/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import java.sql.SQLException;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.postgresql.util.PGobject;

import uk.nominet.iot.utils.error.ProgramDefectException;

@Converter
public class JsonbStringConverter implements AttributeConverter<String, Object> {
    @Override
    public Object convertToDatabaseColumn(String jsonb) {
        PGobject object = new PGobject();
        object.setType("jsonb");
        try {
            if (jsonb == null) {
                object.setValue(null);
            } else {
                object.setValue(jsonb);
            }
        } catch (SQLException | RuntimeException e) {
            System.out.println("this will fail!");
            throw new IllegalArgumentException("Error when creating Postgres jsonb", e);
        }
        return object;
    }

    @Override
    public String convertToEntityAttribute(Object dbData) {
        // (PGobject has 2 fields: type+value, both Strings)
        if (dbData == null) {
            return null;
        } else if (dbData instanceof String) {
            return (String) dbData;
        } else if (dbData instanceof PGobject) {
            return ((PGobject) dbData).getValue();
        } else {
            throw new ProgramDefectException("got unexpected object for conversion: " + dbData.getClass().toString());
        }
    }
}
