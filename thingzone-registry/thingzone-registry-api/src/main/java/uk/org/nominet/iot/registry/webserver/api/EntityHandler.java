/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import uk.org.nominet.iot.registry.common.RegistryUtilities;
import uk.org.nominet.iot.registry.database.DatabaseTransaction;
import uk.org.nominet.iot.registry.database.SimpleDatabaseOperation;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.service.EntityService;
import uk.org.nominet.iot.registry.service.SearchMetaService;
import uk.org.nominet.iot.registry.service.SearchMetaService.SearchResult;
import uk.org.nominet.iot.registry.service.dto.DnsKeyDetailDto;
import uk.org.nominet.iot.registry.webserver.api.support.JsonErrorResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSingleResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSuccessResult;
import uk.org.nominet.iot.registry.webserver.api.support.PersistenceEffect;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;
import uk.org.nominet.iot.registry.webserver.api.support.SessionWrapper;

/**
 * The API for handling entities.
 */
@Path("/entity")
public class EntityHandler {
    private final static Logger log = LoggerFactory.getLogger(EntityHandler.class);

    @Context
    Request request;

    @Context
    private javax.servlet.http.HttpServletRequest hsr;

    private final EntityService entityService;
    private final SearchMetaService searchMetaService;

    /**
     * Instantiates a new entity handler.
     */
    public EntityHandler() {
        entityService = new EntityService();
        searchMetaService = new SearchMetaService();
    }

    /**
     * create an entity key.
     *
     * @param key the key
     * @param type the type
     * @param name the name
     * @return the new entity
     */
    @POST
    @Path("create_key")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object createKey(final @FormParam("key") String key,
                            final @FormParam("type") String type,
                            final @FormParam("name") String name) {
        final boolean externallyNamed = !Strings.isNullOrEmpty(key);
        final String entityType = Strings.isNullOrEmpty(type) ? null : type;
        final String entityName = Strings.isNullOrEmpty(name) ? null : name;

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[dns/create_key] key={}, type={}, name={}", externallyNamed ? key : "(unspecified)", type,
                    name);

                DNSkey created = entityService.createKey(spa, externallyNamed, key);
                if (entityType != null || entityName != null)
                    entityService.setValues(created, entityType, entityName);

                return new JsonSingleResult("key", created.getKey()).output();
            }
        });
    }

    /**
     * updates the entity content.
     *
     * @param key  the key to look up
     * @param type update the type - empty string to unset
     * @param name the name
     * @return success or failure
     */
    @PUT
    @Path("update")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object update(final @FormParam("key") String key,
                         final @FormParam("type") String type,
                         final @FormParam("name") String name) {
        if (Strings.isNullOrEmpty(key))
            return new JsonErrorResult("missing parameter \"key\"").asResponse(Status.BAD_REQUEST);
        if (type == null && name == null)
            return new JsonErrorResult("missing \"type\" or \"name\" parameter").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[entity/update] (key={}, type={})", key, type);

                entityService.setValues(spa, key, type, name);

                return new JsonSuccessResult();
            }
        });
    }

    /**
     * Lists all entities that match the given type.
     *
     * @param type the type to search for or null or empty for keys without type
     * @param detailOption the detail option
     * @param permissionsOption the permissions option
     * @param size the size
     * @param page the page
     * @return the entities
     */
    @GET
    @Path("list")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object list(final @QueryParam("type") String type,
                       final @QueryParam("detail") String detailOption,
                       final @QueryParam("showPermissions") Boolean permissionsOption,
                       final @DefaultValue("30") @QueryParam("size") int size,
                       final @DefaultValue("0") @QueryParam("page") int page) {
        final boolean withDetail = RegistryUtilities.DETAIL_FULL.equals(detailOption) ? true : false;
        final boolean withPermissions = permissionsOption != null && permissionsOption.booleanValue() ? true : false;

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[entity/list] (type={})", type);

                if (withDetail) {
                    List<Object> entities = new ArrayList<>();
                    for (DNSkey key : entityService.getByType(spa, type, size, page)) {
                        Optional<SearchResult> result = searchMetaService.identify(spa, key.getKey(), withDetail,
                            withPermissions);
                        if (result.isPresent()) {
                            entities.add(result.get().value);
                        }
                    }
                    return new JsonSingleResult("entities", entities).output();
                }
                List<DnsKeyDetailDto> entities = entityService.getByType(spa, type, size, page).stream()
                                                              .map(e -> DnsKeyDetailDto.basic(e))
                                                              .collect(Collectors.toList());
                return new JsonSingleResult("entities", entities).output();
            }
        });
    }

    /**
     * deletes an entity (but only if it has no dependents).
     *
     * @param entityKey the entity key
     * @return success or failure
     */
    @DELETE
    @Path("delete")
    @Produces(MediaType.APPLICATION_JSON)
    public Object delete(final @QueryParam("key") String entityKey) {
        if (Strings.isNullOrEmpty(entityKey))
            return new JsonErrorResult("missing param \"key\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[entity/delete] (key={})", entityKey);

                entityService.delete(spa, entityKey);
                return new JsonSuccessResult();
            }
        });
    }

    /**
     * Lists all entity keys that the user has admin or modify permissions for.
     *
     * @param type the type to search for or null for keys without type
     * @param name the name to filter by
     * @param permissionsOption the permissions option
     * @param username the username
     * @param groupname the groupname
     * @param size number of results to return, default 30
     * @param page page number of results
     * @return the entities
     */
    @GET
    @Path("list_admin")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object listAdmin(final @QueryParam("type") String type,
                            final @QueryParam("name") String name,
                            final @QueryParam("showPermissions") Boolean permissionsOption,
                            final @QueryParam("username") String username,
                            final @QueryParam("groupname") String groupname,
                            final @DefaultValue("30") @QueryParam("size") int size,
                            final @DefaultValue("0") @QueryParam("page") int page) {

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);
        final boolean withPermissions = permissionsOption != null && permissionsOption.booleanValue() ? true : false;
        if (size > 50) {
            return new JsonErrorResult("Size is limited to a maximum of 50 entities").asResponse(Status.BAD_REQUEST);
        }
        if (withPermissions) {
            if (Strings.isNullOrEmpty(username) && Strings.isNullOrEmpty(groupname))
                return new JsonErrorResult("select a user or group to return the permissions for").asResponse(
                    Status.BAD_REQUEST);
        }

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[entity/list] (type={}, name={})", type, name);

                List<Object> entities = entityService.listAdminEntities(spa, type, name, withPermissions,
                    username, groupname, size, page);
                return new JsonSingleResult("entities", entities).output();
            }
        });
    }

}
