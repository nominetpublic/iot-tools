/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service;

import static uk.org.nominet.iot.registry.model.Privilege.QUERY_STREAMS;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import uk.org.nominet.iot.registry.common.AuthException;
import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.TransformDao;
import uk.org.nominet.iot.registry.dao.TransformOutputStreamDao;
import uk.org.nominet.iot.registry.dao.TransformSourceDao;
import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.model.Privilege;
import uk.org.nominet.iot.registry.model.Transform;
import uk.org.nominet.iot.registry.model.TransformFunction;
import uk.org.nominet.iot.registry.model.TransformSource;
import uk.org.nominet.iot.registry.service.dto.DataStreamSummaryDto;
import uk.org.nominet.iot.registry.service.dto.DumpObjects;
import uk.org.nominet.iot.registry.service.dto.TransformDependentDto;
import uk.org.nominet.iot.registry.service.dto.TransformOutputStreamDto;
import uk.org.nominet.iot.registry.service.dto.TransformSourceDto;
import uk.org.nominet.iot.registry.service.dto.TransformSummaryDetailDto;
import uk.org.nominet.iot.registry.webserver.ConfigFactory;
import uk.org.nominet.iot.registry.webserver.KeyGenerationType;
import uk.org.nominet.iot.registry.webserver.api.support.EntityLoader;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;

/**
 * Transform Service
 */
public class TransformService {
    private final static Logger log = LoggerFactory.getLogger(TransformService.class);

    private TransformDao transformDao;
    private TransformSourceDao transformSourceDao;
    private TransformOutputStreamDao transformOutputStreamDao;

    private DNSdataService dnsDataService;
    private ACLService aclService;
    private UserPrivilegeService userPrivilegeService;
    private DataStreamService dataStreamService;

    public TransformService() {
        DaoFactory df = DaoFactoryManager.getFactory();
        transformDao = df.transformDao();
        transformSourceDao = df.transformSourceDao();
        transformOutputStreamDao = df.transformOutputStreamDao();

        dnsDataService = new DNSdataService();
        aclService = new ACLService();
        userPrivilegeService = new UserPrivilegeService();
        dataStreamService = new DataStreamService();
    }

    public List<String> listTransforms(SessionPermissionAssistant spa) {
        List<String> ts = new ArrayList<>();
        for (Transform transform : transformDao.getAllowed(spa.getRelevantAccounts(), Permission.MODIFY)) {
            ts.add(transform.getDataStream().getKey().getKey());
        }
        return ts;
    }

    public Transform getByOutputStream(DataStream dataStream) {
        return transformDao.getByOutputStream(dataStream);
    }

    public Transform getByOutputStreamKey(SessionPermissionAssistant spa, String outputStreamKey) {
        DataStream outputStream = dataStreamService.getDataStream(spa, outputStreamKey);
        return getByOutputStream(outputStream);
    }

    public boolean exists(SessionPermissionAssistant spa, String outputStreamKey) {
        if (!dataStreamService.exists(outputStreamKey)) {
            return false;
        }
        // we know something exists, but is it a transform or just a bare datastream?
        DataStream outputStream = dataStreamService.getDataStream(spa, outputStreamKey);
        return outputStream.getIsTransform();
    }

    public Transform createTransform(SessionPermissionAssistant spa,
                                     String name,
                                     String type,
                                     String functionName,
                                     String parameterValues,
                                     boolean externallyNamed,
                                     String transformKey,
                                     String runSchedule) {
        if (!userPrivilegeService.canCreate(spa, Privilege.CREATE_TRANSFORM)) {
            throw new AuthException(spa, "need privilege " + Privilege.CREATE_TRANSFORM);
        }

        TransformFunction transformFunction = EntityLoader.getTransformFunction(functionName);

        Transform transform;
        if (!externallyNamed) {
            // user didn't specify a key
            transform = transformDao.create(transformFunction, parameterValues, runSchedule);
        } else {
            // user did specify a key:
            if (ConfigFactory.getConfig().getKeyGenerationType() != KeyGenerationType.EXTERNAL) {
                throw new DataException("external key generation is not configured");
            }
            transform = transformDao.createKnownKey(transformKey, transformFunction, parameterValues, runSchedule);
        }

        DNSkey dnsKey = transform.getDataStream().getKey();
        if (!Strings.isNullOrEmpty(name)) {
            dnsKey.setName(name);
        }
        if (!Strings.isNullOrEmpty(type)) {
            dnsKey.setType(type);
        }

        // create & attach "owner" ACL
        Account account = spa.getUserAccount();
        aclService.setOwnerPrivileges(account, dnsKey);

        return transform;
    }

    public Transform getTransform(SessionPermissionAssistant spa, String transformKey) {
        return EntityLoader.getTransform(spa, transformKey, Permission.DISCOVER);
    }

    public TransformSummaryDetailDto getSummary(SessionPermissionAssistant spa, String transformKey) {
        Transform transform = EntityLoader.getTransform(spa, transformKey, Permission.MODIFY);
        DataStreamSummaryDto dataStreamSummary = dataStreamService.getDataStreamSummary(spa, transform.getDataStream());
        return TransformSummaryDetailDto.basic(transform, dataStreamSummary, true);
    }

    public TransformSummaryDetailDto getSummary(SessionPermissionAssistant spa, Transform transform) {
        DataStreamSummaryDto dataStreamSummary = dataStreamService.getDataStreamSummary(spa, transform.getDataStream());
        boolean canQueryStreams = userPrivilegeService.canQueryStreams(spa);
        if (canQueryStreams || aclService.isAllowed(spa, transform.getDataStream().getKey(), Permission.MODIFY)) {
            return TransformSummaryDetailDto.basic(transform, dataStreamSummary, true);
        }
        return TransformSummaryDetailDto.basic(transform, dataStreamSummary, false);
    }

    public TransformSummaryDetailDto getSummaryDetail(SessionPermissionAssistant spa, String transformKey,
                                                      boolean withPermissions) {
        Transform transform = EntityLoader.getTransform(spa, transformKey, Permission.MODIFY);
        return getSummaryDetail(spa, transform, withPermissions);
    }

    public TransformSummaryDetailDto getSummaryDetail(SessionPermissionAssistant spa, Transform transform,
                                                      boolean withPermissions) {
        boolean canQueryStreams = userPrivilegeService.canQueryStreams(spa);
        if (canQueryStreams || aclService.isAllowed(spa, transform.getDataStream().getKey(), Permission.MODIFY)) {
            DataStreamSummaryDto dataStreamSummaryDetail = dataStreamService.getDataStreamSummaryDetail(spa,
                transform.getDataStream(), withPermissions, true, true);
            // has MODIFY permission - return full details
            return TransformSummaryDetailDto.details(transform, dataStreamSummaryDetail,
                TransformSourceDto.toDto(transformSourceDao.getByTransform(transform)),
                TransformOutputStreamDto.toDto(transformOutputStreamDao.getByTransform(transform)),
                spa.getPublicUser());
        }
        DataStreamSummaryDto dataStreamSummaryDetail = dataStreamService.getDataStreamSummaryDetail(spa,
            transform.getDataStream(), withPermissions, false, false);
        return TransformSummaryDetailDto.basic(transform, dataStreamSummaryDetail, false);
    }

    public void updateParameters(SessionPermissionAssistant spa, String transformKey, String parameterValues) {
        Transform transform = EntityLoader.getTransform(spa, transformKey, Permission.MODIFY);
        transform.setParameterValues(parameterValues);
    }

    public void updateFunction(SessionPermissionAssistant spa, String transformKey, String functionName) {
        Transform transform = EntityLoader.getTransform(spa, transformKey, Permission.MODIFY);
        TransformFunction transformFunction = EntityLoader.getTransformFunction(functionName);
        transform.setFunction(transformFunction);
    }

    public void updateRunSchedule(SessionPermissionAssistant spa, String transformKey, String runSchedule) {
        Transform transform = EntityLoader.getTransform(spa, transformKey, Permission.MODIFY);
        transform.setRunSchedule(runSchedule);
    }

    public void createOrUpdateSource(SessionPermissionAssistant spa, String transformKey, String sourceStreamKey,
                                     String alias, Boolean trigger, String aggregationType, String aggregationSpec) {
        Transform transform = EntityLoader.getTransform(spa, transformKey, Permission.MODIFY);
        DataStream sourceStream = EntityLoader.getDataStream(spa, sourceStreamKey, Permission.MODIFY);

        if (transformOutputStreamDao.exists(transform, sourceStream)) {
            throw new DataException("Cannot use an outputStream as a source stream", Status.BAD_REQUEST);
        }

        transformSourceDao.createOrUpdate(transform, sourceStream, alias, trigger, aggregationType, aggregationSpec);
    }

    public void removeSource(SessionPermissionAssistant spa, String transformKey, String alias) {
        Transform transform = EntityLoader.getTransform(spa, transformKey, Permission.MODIFY);
        transformSourceDao.delete(transform, alias);
    }

    public void createOutputStream(SessionPermissionAssistant spa,
                                   String transformKey,
                                   String sourceStreamKey,
                                   String alias) {
        Transform transform = EntityLoader.getTransform(spa, transformKey, Permission.MODIFY);
        DataStream sourceStream = EntityLoader.getDataStream(spa, sourceStreamKey, Permission.MODIFY);

        if (transformSourceDao.exists(transform, sourceStream)) {
            throw new DataException("Cannot use a source stream as an outputStream", Status.BAD_REQUEST);
        }
        transformOutputStreamDao.create(transform, sourceStream, alias);
    }

    public void removeOutputStream(SessionPermissionAssistant spa, String transformKey, String alias) {
        Transform transform = EntityLoader.getTransform(spa, transformKey, Permission.MODIFY);
        transformOutputStreamDao.remove(transform, alias);
    }

    /**
     * delete - requires ADMIN permission
     */
    public void delete(SessionPermissionAssistant spa, String transformKey) {
        Transform transform = EntityLoader.getTransform(spa, transformKey, Permission.ADMIN);

        // get list of sources & remove them
        for (TransformSource ts : transformSourceDao.getByTransform(transform)) {
            transformSourceDao.delete(transform, ts.getAlias());
            log.info("disconnected transformSource \"{}\" ({})", ts.getAlias(), ts.getSourceStream().getKey().getKey());
        }

        // delete push/pull pubs
        dataStreamService.deletePullSubscription(transform.getDataStream());
        dataStreamService.deletePushSubscribers(spa, transform.getDataStream());
        dataStreamService.removeFromDevices(transform.getDataStream());

        // delete DNS records
        dnsDataService.removeAllRecords(transform.getDataStream().getKey());

        // delete transform itself
        transformDao.delete(transform);

        // & clean up ACLs
        aclService.removeForDelete(spa, transform.getDataStream().getKey());

        log.info("deleted transform {}", transformKey);
    }

    public DumpObjects.TransformWithPermissions getDump(SessionPermissionAssistant spa, Transform transform) {
        // retrieve objects for dump using permission checks & normal JPA
        DumpRetriever dumpRetriever = new DumpRetriever(new DefaultDumpRetrieverAccessor(spa));

        return dumpRetriever.getTransformSpecificDetails(transform.getDataStream(),
            dumpRetriever.getGenericStreamDetails(transform.getDataStream()), spa.getPublicUser());
    }

    public List<TransformDependentDto> listDependentsBySourceStream(SessionPermissionAssistant spa,
                                                                    String sourceStreamKey) {
        if (!userPrivilegeService.canQueryStreams(spa)) {
            throw new AuthException(spa, "need privilege " + QUERY_STREAMS);
        }
        DataStream sourceStream = EntityLoader.getDataStreamWithoutCheckingPermission(sourceStreamKey);
        return transformSourceDao.getBySourceStream(sourceStream)
                                 .stream()
                                 .map(ts -> new TransformDependentDto(
                                     ts,
                                     transformSourceDao.getByTransform(ts.getTransform()),
                                     transformOutputStreamDao.getByTransform(ts.getTransform())))
                                 .collect(Collectors.toList());
    }

    public Object listTransformsWithRunSchedule(SessionPermissionAssistant spa) {
        if (!userPrivilegeService.canQueryStreams(spa)) {
            throw new AuthException(spa, "need privilege " + QUERY_STREAMS);
        }
        return transformDao.getTransformsWithRunSchedule()
                           .stream()
                           .map(t -> TransformSummaryDetailDto.withFunction(t,
                               DataStreamSummaryDto.basic(t.getDataStream(), null, null),
                               TransformSourceDto.toDto(transformSourceDao.getByTransform(t)),
                               TransformOutputStreamDto.toDto(transformOutputStreamDao.getByTransform(t))))
                           .collect(Collectors.toList());
    }
}
