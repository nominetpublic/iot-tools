/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Customizer;

import com.fasterxml.jackson.annotation.JsonIgnore;

import uk.org.nominet.iot.registry.database.ColumnPosition;
import uk.org.nominet.iot.registry.database.ColumnPositionCustomizer;
import uk.org.nominet.iot.registry.database.DatabaseDriver;

@Entity
@Table(name = "history", schema = DatabaseDriver.SCHEMA_NAME)
@Customizer(ColumnPositionCustomizer.class)
public class History {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ColumnPosition(position = 1)
    @JsonIgnore
    private Long id;

    @ColumnPosition(position = 2)
    @Column(length = DatabaseDriver.DNSKEY_LENGTH, nullable = false)
    private String key;

    @Column(name = "object_type", columnDefinition = DatabaseDriver.SCHEMA_NAME + ".OBJECT_TYPE", nullable = false)
    @Convert(converter = ObjectType.ObjectTypeConverter.class)
    @ColumnPosition(position = 3)
    private ObjectType objectType;

    @Column(name = "action", columnDefinition = DatabaseDriver.SCHEMA_NAME + ".HISTORY_ACTION_TYPE", nullable = false)
    @Convert(converter = HistoryAction.HistoryActionConverter.class)
    @ColumnPosition(position = 4)
    private HistoryAction action;

    @Column(name = "account_id", columnDefinition = "integer")
    @ColumnPosition(position = 5)
    private Long accountId;

    @Column(name = "updated", updatable = false, nullable = false)
    @ColumnPosition(position = 6)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;

    public History() {}

    public Long getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public ObjectType getObjectType() {
        return objectType;
    }

    public void setObjectType(ObjectType objectType) {
        this.objectType = objectType;
    }

    public HistoryAction getAction() {
        return action;
    }

    public void setAction(HistoryAction action) {
        this.action = action;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(long userId) {
        this.accountId = userId;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Override
    public String toString() {
        return "History [key=" + key + ", objectType=" + objectType + ", action=" + action + ", updated=" + updated
            + "]";
    }

}
