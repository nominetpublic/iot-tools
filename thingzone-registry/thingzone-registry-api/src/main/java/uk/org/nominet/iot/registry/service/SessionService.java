/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service;

import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.SessionDao;
import uk.org.nominet.iot.registry.model.Session;

public class SessionService {
    private SessionDao sessionDao;

    public SessionService() {
        DaoFactory df = DaoFactoryManager.getFactory();
        sessionDao = df.sessionDao();
    }

    public Session open(String username, String remoteIp) {
        return sessionDao.open(username, remoteIp);
    }

    // get & check session, for normal use
    public Session getSession(String sessionKey, String remoteIp) {
        Session session = sessionDao.find(sessionKey);
        checkSourceAddress(session, remoteIp);
        sessionDao.refresh(session);
        return session;
    }

    public void closeSession(Session session) {
        sessionDao.close(session);
    }

    public int getTimeRemaining(Session session) {
        return sessionDao.getTimeRemaining(session);
    }

    private void checkSourceAddress(Session session, String srcAddr) {
        // No-op
        // we decided not to require that IP address stays constant for a session;
        // there are valid reasons for a users IP address to change

        /*
         * if (!session.getIpAddr().equals(srcAddr)) { log.warn("mismatched source IP {} for session {} (expected {})",
         * srcAddr, session.getSessionKey(), session.getIpAddr()); throw new
         * DataException("mismatched source IP for session"); }
         */
    }

    // get session as a reference (i.e. don't refresh it, or check source address)
    // (this is a privileged operation)
    public Session getSessionAsReference(String sessionKey) {
        return sessionDao.findByReference(sessionKey);
    }

}
