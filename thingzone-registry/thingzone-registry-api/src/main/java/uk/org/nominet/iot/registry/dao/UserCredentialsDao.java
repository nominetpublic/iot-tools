/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao;

import java.util.List;
import java.util.Map;

import uk.org.nominet.iot.registry.model.Group;
import uk.org.nominet.iot.registry.model.User;
import uk.org.nominet.iot.registry.model.UserCredentials;
import uk.org.nominet.iot.registry.model.UserToken;

public interface UserCredentialsDao extends JpaDao<UserCredentials> {

    UserCredentials create(String username, String password, User user, String email);

    void createNoLogon(String username, User user);

    boolean exists(String username);

    User authenticate(String username, String password); // NB returns User, not UserCredentials

    void updatePassword(String username, String oldPassword, String newPassword);

    void tokenUpdatePassword(String username, UserToken token, String newPassword);

    void initialisePassword(String username, String newPassword);

    boolean isPasswordInitialised(String username);

    UserCredentials get(String username);

    User getUserByUsername(String username);

    String getUsernameByUser(User user);

    Map<String, Boolean> listUsernamesByGroup(Group group);

    List<UserCredentials> getAll();

    List<String> getStandardUsers();
}
