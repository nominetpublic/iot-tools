/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.common;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import org.xbill.DNS.utils.base16;
import org.xbill.DNS.utils.base32;

import uk.nominet.iot.utils.error.ProgramDefectException;

public class UidGenerator {

    public final static int BITS_PER_BYTE = 8;
    public final static int BITS_PER_BASE32 = 5;
    public final static int BITS_PER_HEX = 4;

    public final static int UID_ENTROPY_BITS = 256;
    public final static int DEVICE_KEY_BITS = 130;

    /**
     * Generates a User UID, containing a large amount of entropy
     *
     * @return Hex encoded String containing UID_ENTROPY_BITS bits of entropy
     */
    public static String generateUserUid() {
        // get entropy
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[UID_ENTROPY_BITS / BITS_PER_BYTE];
        random.nextBytes(bytes);

        // convert to hex using dnsjava base16 (NB nonstandard naming for class
        // "base16")
        String hex = base16.toString(bytes);

        return hex;
    }

    /**
     * Generates a random device key
     *
     * @return Hex encoded String containing DEVICE_KEY_BITS bits of entropy
     */
    public static String generateRandomDeviceKey() {
        // get entropy
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[DEVICE_KEY_BITS / BITS_PER_BYTE + 1]; // round up not down
        random.nextBytes(bytes);

        // convert to base32
        base32 converter = new base32(base32.Alphabet.BASE32, false, true); // lower case
        String result = converter.toString(bytes);

        return result.substring(0, DEVICE_KEY_BITS / BITS_PER_BASE32);
    }

    /**
     * Generates a device key (base32) from device data, with entropy from User UID,
     * registry UID, device (and optional anti-collision
     *
     * @return base32 encoded String
     */
    public static String generateDeterministicDeviceKey(String registryUid,
                                                        String userUid,
                                                        String deviceString,
                                                        String antiCollision) {
        try {
            // don't need to worry about length extension, so digest is sufficient &
            // so don't need HMAC - also we'll check for & handle any collisions
            MessageDigest md = MessageDigest.getInstance("SHA-256");

            md.update(registryUid.getBytes());
            md.update(userUid.getBytes());
            md.update(deviceString.getBytes());
            md.update(antiCollision.getBytes());
            byte[] digestBytes = md.digest();

            // convert to base32
            base32 converter = new base32(base32.Alphabet.BASE32, false, true); // lower case
            String result = converter.toString(digestBytes);

            return result.substring(0, DEVICE_KEY_BITS / BITS_PER_BASE32);

        } catch (NoSuchAlgorithmException e) {
            throw new ProgramDefectException(e);
        }
    }
}
