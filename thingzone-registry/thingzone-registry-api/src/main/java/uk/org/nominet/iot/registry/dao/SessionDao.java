/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao;

import uk.org.nominet.iot.registry.model.Session;

public interface SessionDao extends JpaDao<Session> {
    Session open(String username, String srcAddr);

    boolean exists(String sessionKey);

    Session find(String sessionKey);

    Session findByReference(String sessionKey);

    void refresh(Session session);

    void close(Session session);

    void cleanOldSessions(int expiryPeriod);

    void detach(Session session);

    int getTimeRemaining(Session session);
}
