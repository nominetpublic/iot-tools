/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import java.util.Collections;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.AbstractJpaDao;
import uk.org.nominet.iot.registry.dao.DataStreamDao;
import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.EntityStatus;
import uk.org.nominet.iot.registry.model.Permission;

class DataStreamDaoImpl extends AbstractJpaDao<DataStream> implements DataStreamDao {
    private final static Logger log = LoggerFactory.getLogger(DataStreamDaoImpl.class);

    DataStreamDaoImpl() {
        super(DataStream.class);
    }

    @Override
    public DataStream create() {
        return createKnownKey(new DNSkeyDaoImpl().generateKey());
    }

    @Override
    public DataStream createKnownKey(String key) {
        DNSkey dnskey = new DNSkeyDaoImpl().createKnownKey(key);
        dnskey.setStatus(EntityStatus.DATASTREAM);
        DataStream dataStream = new DataStream();
        dataStream.setKey(dnskey);
        dataStream.setMetadata("{}"); // empty JSON
        dataStream.setIsTransform(false);
        createStream(dataStream); // (will throw if already exists)
        return dataStream;
    }

    DataStream createStream(DataStream dataStream) {
        // see if exists already
        if (exists(dataStream.getKey().getKey())) {
            throw new DataException("DNS key \"" + dataStream.getKey() + "\" exists already", Status.CONFLICT);
        }

        persist(dataStream);
        log.info("created data stream: {}", dataStream);
        return dataStream;
    }

    @Override
    public boolean exists(String key) {
        try {
            loadByKey(key);
            return true;
        } catch (PersistenceException e) {
            if (e instanceof NoResultException) {
                return false;
            } else {
                throw e;
            }
        }
    }

    @Override
    public DataStream getByName(String key) {
        return loadByKey(key);
    }

    private DataStream loadByKey(String key) {
        DNSkey dnsKey = new DNSkey(key);

        return loadByMatch("key", dnsKey);
    }

    @Override
    public void delete(DataStream datastream) {
        datastream.getKey().setStatus(EntityStatus.DELETED);
        deleteEntity(datastream);
    }

    @Override
    public List<DataStream> getAllowed(List<Account> accounts, List<Permission> perms) {
        return DaoPermissionRetrieval.forDataStream().getAllowed(getEntityManager(), accounts, perms);
    }

    @Override
    public List<DataStream> getAllowed(List<Account> accounts, Permission permission) {
        return getAllowed(accounts, Collections.singletonList(permission));
    }

}
