/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.dto;

import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.PushSubscriber;

public class PushSubscriberRow {
    private PushSubscriberId id;
    private DNSkey streamKey;
    private String uri;
    private String uriParams;
    private String headers;
    private String method;
    private String retrySequence;

    public PushSubscriberRow(PushSubscriberId id, DNSkey streamKey, String uri, String uriParams) {
        this.id = id;
        this.streamKey = streamKey;
        this.uri = uri;
        this.uriParams = uriParams;
    }

    public PushSubscriberRow(PushSubscriber ps) {
        this.id = new PushSubscriberId(ps.getId());
        this.streamKey = ps.getDataStream().getKey();
        this.uri = ps.getUri();
        this.uriParams = ps.getUriParams();
        this.headers = ps.getHeaders();
        this.method = ps.getMethod();
        this.retrySequence = ps.getRetrySequence();
    }

    public PushSubscriberId getId() {
        return id;
    }

    public DNSkey getStreamKey() {
        return streamKey;
    }

    public String getUri() {
        return uri;
    }

    public String getUriParams() {
        return uriParams;
    }

    public String getHeaders() {
        return headers;
    }

    public String getMethod() {
        return method;
    }

    public String getRetrySequence() {
        return retrySequence;
    }
}
