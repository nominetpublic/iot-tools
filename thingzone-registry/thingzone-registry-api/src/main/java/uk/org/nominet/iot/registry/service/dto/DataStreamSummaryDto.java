/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.dto;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.Permission;

public class DataStreamSummaryDto {

    @JsonUnwrapped
    DnsKeyDetailDto basicEntity;

    @JsonInclude(Include.NON_EMPTY)
    public String linkName;

    @JsonInclude(Include.NON_EMPTY)
    @JsonRawValue
    public String metadata;

    @JsonInclude(Include.NON_NULL)
    public Boolean isTransform;

    @JsonInclude(Include.NON_NULL)
    public PullSubscriptionSummaryDto pullSubscription;

    @JsonInclude(Include.NON_EMPTY)
    public List<PushSubscriberSummaryDto> pushSubscribers;

    @JsonInclude(Include.NON_EMPTY)
    public List<DeviceSummaryDetailDto> devices;

    private DataStreamSummaryDto(DNSkey dnsKey,
                                 String metadata,
                                 Boolean isTransform,
                                 List<DNSdataDto> dnsRecords,
                                 Map<String, Set<Permission>> userPermissions,
                                 Map<String, Set<Permission>> groupPermissions,
                                 String streamName,
                                 List<DeviceSummaryDetailDto> devices,
                                 Optional<PullSubscriptionSummaryDto> pullSubscriptionOption,
                                 List<PushSubscriberSummaryDto> pushSubscribers) {
        this.basicEntity = DnsKeyDetailDto.detail(dnsKey, dnsRecords, userPermissions, groupPermissions);
        this.linkName = streamName;
        this.metadata = metadata;
        this.isTransform = isTransform;
        this.devices = devices;

        if (pullSubscriptionOption.isPresent()) {
            this.pullSubscription = pullSubscriptionOption.get();
            this.pullSubscription.clearDataStream(); // erase back-reference
        } else {
            this.pullSubscription = null;
        }

        if (pushSubscribers == null || pushSubscribers.size() == 0) {
            this.pushSubscribers = null;
        } else {
            this.pushSubscribers = pushSubscribers;
            for (PushSubscriberSummaryDto ps : pushSubscribers) {
                ps.clearDataStream(); // erase back-reference
            }
        }
    }

    public DataStreamSummaryDto(DnsKeyDetailDto entity, String metadata, Boolean isTransform) {
        this.basicEntity = entity;
        this.metadata = metadata;
        this.isTransform = isTransform;
    }

    /**
     * Basic datastream view
     *
     * @param dataStream the basic datastream information
     * @param metadata this is separated to check for VIEW_METADATA permission
     * @param dnsRecords the DNS records for the key
     */
    public static DataStreamSummaryDto basic(DataStream dataStream,
                                             String metadata,
                                             List<DNSdataDto> dnsRecords) {
        return new DataStreamSummaryDto(dataStream.getKey(),
                                        metadata,
                                        dataStream.getIsTransform(),
                                        dnsRecords,
                                        null,
                                        null,
                                        null,
                                        null,
                                        Optional.empty(),
                                        null);
    }

    /**
     * Allows display of a linked datastream for a device at various detail levels
     *
     * @param dataStream the basic datastream information
     * @param metadata this is separated to check for VIEW_METADATA permission
     * @param dnsRecords the DNS records for the key
     * @param userPermissions the user permissions for the key - requires ADMIN
     * @param groupPermissions the group permissions for the key - requires ADMIN
     * @param streamName the stream name if this is linked to a device
     */
    public static DataStreamSummaryDto streamLink(DataStream dataStream,
                                                  String metadata,
                                                  List<DNSdataDto> dnsRecords,
                                                  Map<String, Set<Permission>> userPermissions,
                                                  Map<String, Set<Permission>> groupPermissions,
                                                  String streamName) {
        return new DataStreamSummaryDto(dataStream.getKey(),
                                        metadata,
                                        dataStream.getIsTransform(),
                                        dnsRecords,
                                        userPermissions,
                                        groupPermissions,
                                        streamName,
                                        null,
                                        Optional.empty(),
                                        null);
    }

    /**
     * Allows display of a linked datastream for a device at various detail levels
     *
     * @param dnsKey the entity key of the datastream
     * @param streamName the stream name if linked to a device
     */
    public static DataStreamSummaryDto basicStreamLink(DNSkey dnsKey, String streamName) {
        return new DataStreamSummaryDto(dnsKey, null, null, null, null, null, streamName, null, Optional.empty(), null);
    }

    /**
     * Show the datastream with options available, including subscriptions and linked devices
     *
     * @param dataStream the basic datastream information
     * @param metadata this is separated to check for VIEW_METADATA permission
     * @param dnsRecords the DNS records for the key
     * @param userPermissions the user permissions for the key - requires ADMIN
     * @param groupPermissions the group permissions for the key - requires ADMIN
     * @param streamName the stream name if this is linked to a device
     * @param devices the devices it is linked to
     * @param pullSubscriptionOption the pull subscription
     * @param pushSubscribers any push subscribers
     */
    public static DataStreamSummaryDto details(DataStream dataStream,
                                               String metadata,
                                               List<DNSdataDto> dnsRecords,
                                               Map<String, Set<Permission>> userPermissions,
                                               Map<String, Set<Permission>> groupPermissions,
                                               String streamName,
                                               List<DeviceSummaryDetailDto> devices,
                                               Optional<PullSubscriptionSummaryDto> pullSubscriptionOption,
                                               List<PushSubscriberSummaryDto> pushSubscribers) {
        return new DataStreamSummaryDto(dataStream.getKey(),
                                        metadata,
                                        dataStream.getIsTransform(),
                                        dnsRecords,
                                        userPermissions,
                                        groupPermissions,
                                        streamName,
                                        devices,
                                        pullSubscriptionOption,
                                        pushSubscribers);
    }
}
