/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import uk.org.nominet.iot.registry.database.DatabaseTransaction;
import uk.org.nominet.iot.registry.database.SimpleDatabaseOperation;
import uk.org.nominet.iot.registry.model.DNSdata;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.service.DNSdataService;
import uk.org.nominet.iot.registry.service.dto.DNSdataDto;
import uk.org.nominet.iot.registry.webserver.api.support.EntityLoader;
import uk.org.nominet.iot.registry.webserver.api.support.JsonErrorResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSingleResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSuccessResult;
import uk.org.nominet.iot.registry.webserver.api.support.PersistenceEffect;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;
import uk.org.nominet.iot.registry.webserver.api.support.SessionWrapper;

/**
 * The API handler for DNS records.
 */
@Path("/dns")
public class DNSdataHandler {
    private final static Logger log = LoggerFactory.getLogger(DNSdataHandler.class);

    @Context
    Request request;

    @Context
    private javax.servlet.http.HttpServletRequest hsr;

    DNSdataService dnsDataService;

    /**
     * Instantiates a new DNS data handler.
     */
    public DNSdataHandler() {
        dnsDataService = new DNSdataService();
    }

    /**
     * list DNS records for a given key.
     *
     * @param key the key
     * @return the dns records
     */
    @GET
    @Path("list_records")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object dnsListRecords(final @QueryParam("key") String key) {
        if (Strings.isNullOrEmpty(key)) {
            return new JsonErrorResult("missing param \"key\"").asResponse(Status.BAD_REQUEST);
        }

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[dns/list_records] (key={})", key);

                List<DNSdata> dnsDatas = dnsDataService.dnsDataListByKey(spa, key);

                return new JsonSingleResult("records", dnsDatas).output();
            }
        });
    }

    /**
     * list all DNS records.
     *
     * @param size the size
     * @param page the page
     * @return the DNS records
     */
    @GET
    @Path("list_all_records")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object dnsListAllRecords(final @DefaultValue("0") @QueryParam("size") int size,
                                    final @DefaultValue("1") @QueryParam("page") int page) {

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[dns/list_all_records] ()");

                List<DNSdata> dnsDatas = dnsDataService.dnsDataListAll(spa, size, page);

                @SuppressWarnings("unused")
                class RecordJson {
                    public String key;
                    public List<DNSdata> records;

                    RecordJson(String key) {
                        this.key = key;
                        this.records = new ArrayList<>();
                    }
                }
                Map<String, RecordJson> keysWithData = new HashMap<>();
                for (DNSdata dnsData : dnsDatas) {
                    String dnsKey = dnsData.getKey().getKey();
                    if (!keysWithData.containsKey(dnsKey)) {
                        keysWithData.put(dnsKey, new RecordJson(dnsKey));
                        log.info("created key={}", dnsKey);
                    }
                    RecordJson ld = keysWithData.get(dnsKey);
                    ld.records.add(dnsData);
                    log.info("added data={}", dnsData.getSubdomain());
                }

                return new JsonSingleResult("keys", keysWithData.values()).output();
            }
        });
    }

    /**
     * adds a DNS record.
     *
     * @param key the key
     * @param descriptionOption the description
     * @param subdomainOption the subdomain
     * @param ttlOption the ttl option
     * @param rrtype the rrtype
     * @param rdata the rdata
     * @return success or failure
     */
    @PUT
    @Path("add_record")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object add_record(final @FormParam("key") String key,
                             final @FormParam("description") String descriptionOption,
                             final @FormParam("subdomain") String subdomainOption,
                             final @FormParam("TTL") Integer ttlOption,
                             final @FormParam("RRTYPE") String rrtype,
                             final @FormParam("RDATA") String rdata) {
        if (Strings.isNullOrEmpty(key)) {
            return new JsonErrorResult("missing param \"key\"").asResponse(Status.BAD_REQUEST);
        }
        final String description = Strings.isNullOrEmpty(descriptionOption) ? "" : descriptionOption;
        final String subdomain = Strings.isNullOrEmpty(subdomainOption) ? "" : subdomainOption;
        final Integer ttl = (ttlOption == null) ? 600 : ttlOption;
        if (Strings.isNullOrEmpty(rrtype)) {
            return new JsonErrorResult("missing param \"rrtype\"").asResponse(Status.BAD_REQUEST);
        }
        if (Strings.isNullOrEmpty(rdata)) {
            return new JsonErrorResult("missing param \"rdata\"").asResponse(Status.BAD_REQUEST);
        }

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[dns/add_record] (key={} subdomain={} ttl={} rrtype={} rdata={})", key, subdomain, ttl,
                    rrtype, rdata);

                DNSkey dnsKey = EntityLoader.getDnsKey(spa, key, Permission.MODIFY);
                DNSdataDto ddr = new DNSdataDto(dnsKey, description, subdomain, ttl, rrtype, rdata);

                dnsDataService.addRecord(spa, ddr);
                return new JsonSuccessResult();
            }
        });
    }

    /**
     * removes a DNS record.
     *
     * @param key the key
     * @param subdomainOption the subdomain option
     * @param rrtype the rrtype
     * @param rdata the rdata
     * @return the object
     */
    @PUT
    @Path("remove_record")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object remove_record(final @FormParam("key") String key,
                                final @FormParam("subdomain") String subdomainOption,
                                final @FormParam("RRTYPE") String rrtype,
                                final @FormParam("RDATA") String rdata) {
        if (Strings.isNullOrEmpty(key)) {
            return new JsonErrorResult("missing param \"key\"").asResponse(Status.BAD_REQUEST);
        }
        final String subdomain = Strings.isNullOrEmpty(subdomainOption) ? "" : subdomainOption;
        if (Strings.isNullOrEmpty(rrtype)) {
            return new JsonErrorResult("missing param \"rrtype\"").asResponse(Status.BAD_REQUEST);
        }
        if (Strings.isNullOrEmpty(rdata)) {
            return new JsonErrorResult("missing param \"rdata\"").asResponse(Status.BAD_REQUEST);
        }

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[dns/remove_record] (key={} subdomain={} rrtype={} rdata={})", key, subdomain, rrtype, rdata);

                DNSkey dnsKey = EntityLoader.getDnsKey(spa, key, Permission.MODIFY);
                DNSdataDto ddr = new DNSdataDto(dnsKey, "", subdomain, 0, rrtype, rdata);

                dnsDataService.removeRecord(spa, ddr);

                return new JsonSuccessResult();
            }
        });
    }

}
