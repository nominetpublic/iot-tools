/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.Permission;

public class PermissionDto {
    @JsonInclude(Include.NON_NULL)
    public String key;

    public String username;
    public List<Permission> userPermissions;

    public PermissionDto(DNSkey key, String username, List<Permission> userPermissions) {
        this.key = key.getKey();
        this.username = username;
        this.userPermissions = userPermissions;
    }

    public PermissionDto(String username, List<Permission> userPermissions) {
        this.key = null;
        this.username = username;
        this.userPermissions = userPermissions;
    }
}
