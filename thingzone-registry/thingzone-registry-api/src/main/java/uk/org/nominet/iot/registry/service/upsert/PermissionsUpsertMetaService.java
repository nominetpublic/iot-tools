/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.upsert;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.service.PermissionService;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;
import uk.nominet.iot.utils.error.ProgramDefectException;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * PermissionsUpsertMetaService is a meta-service to help with upserts It helps upsert permissions, so is used for
 * Device/DataStream/Transform upsert
 */
public class PermissionsUpsertMetaService {
    private final static Logger log = LoggerFactory.getLogger(new Object() {}.getClass().getEnclosingClass());

    private final PermissionService permissionService = new PermissionService();

    public void upsertUserPermissions(SessionPermissionAssistant spa, String key, String permissionsAsJsonString) {
        Map<String, Set<Permission>> requestedPermissions = decodeFromJson(permissionsAsJsonString);
        Map<String, Set<Permission>> existingPermissions = permissionService.listUserPermissions(spa, key, null);
        // log.info("permissions existing ={}", existingPermissions);

        if (areSame(requestedPermissions, existingPermissions)) {
            log.info("Permissions already are already as requested: no need to update");
        } else {
            updateUserPermissions(spa, key, requestedPermissions, existingPermissions);
        }
    }

    private void updateUserPermissions(SessionPermissionAssistant spa,
                                       String key,
                                       Map<String, Set<Permission>> requested,
                                       Map<String, Set<Permission>> existing) {
        // update/create permissions for the rest
        // (uses setPermissions own difference calculator)
        requested.keySet().forEach(
            x -> permissionService.setUserPermissions(spa, key, x, new ArrayList<Permission>(requested.get(x))));

        // take out any users that are entirely removed
        existing.keySet().stream().filter(n -> !requested.containsKey(n))
                .forEach(x -> permissionService.setUserPermissions(spa, key, x, new ArrayList<>()));
    }

    public void upsertGroupPermissions(SessionPermissionAssistant spa, String key, String permissionsAsJsonString) {
        Map<String, Set<Permission>> requestedPermissions = decodeFromJson(permissionsAsJsonString);
        Map<String, Set<Permission>> existingPermissions = permissionService.listGroupPermissions(spa, key, null);

        if (areSame(requestedPermissions, existingPermissions)) {
            log.info("Permissions already are already as requested: no need to update");
        } else {
            updateGroupPermissions(spa, key, requestedPermissions, existingPermissions);
        }
    }

    private void updateGroupPermissions(SessionPermissionAssistant spa,
                                        String key,
                                        Map<String, Set<Permission>> requested,
                                        Map<String, Set<Permission>> existing) {
        // take out any users that are entirely removed
        existing.keySet().stream().filter(n -> !requested.containsKey(n))
                .forEach(x -> permissionService.setGroupPermissions(spa, key, x, new ArrayList<>()));

        // update/create permissions for the rest
        // (uses setPermissions own difference calculator)
        requested.keySet().forEach(
            x -> permissionService.setGroupPermissions(spa, key, x, new ArrayList<Permission>(requested.get(x))));
    }

    static Map<String, Set<Permission>> decodeFromJson(String permsJson) {
        ObjectMapper mapper = new ObjectMapper();
        MapType mapType = mapper.getTypeFactory().constructMapType(HashMap.class, String.class, List.class);
        try {
            Map<String, List<String>> decoded = mapper.readValue(permsJson, mapType);
            return decoded.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, x -> {
                List<String> perms = x.getValue();
                return perms.stream().map(Permission::fromString).collect(Collectors.toSet());
            }));
        } catch (IOException | ProgramDefectException e) {
            throw new DataException("bad JSON: " + e.getMessage());
        }
    }

    static boolean areSame(Map<String, Set<Permission>> lhs, Map<String, Set<Permission>> rhs) {
        if (lhs.size() != rhs.size())
            return false;
        if (!lhs.keySet().equals(rhs.keySet()))
            return false;
        for (String key : lhs.keySet()) {
            if (!((lhs.get(key))).equals(rhs.get(key)))
                return false;
        }
        return true;
    }
}
