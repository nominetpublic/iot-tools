/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response.Status;

import uk.org.nominet.iot.registry.common.AuthException;
import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.common.RegistryUtilities.Detail;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.TransformFunctionDao;
import uk.org.nominet.iot.registry.model.Privilege;
import uk.org.nominet.iot.registry.model.TransformFunction;
import uk.org.nominet.iot.registry.service.dto.TransformFunctionDto;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;

public class TransformFunctionService {
    private TransformFunctionDao transformFunctionDao;
    private UserPrivilegeService userPrivilegeService;

    public TransformFunctionService() {
        DaoFactory df = DaoFactoryManager.getFactory();
        transformFunctionDao = df.transformFunctionDao();
        userPrivilegeService = new UserPrivilegeService();
    }

    /**
     * List the transform functions.
     *
     * @param spa the current user session
     * @param detail the detail level to return
     * @return a list of functions
     */
    public List<TransformFunctionDto> listFunctions(SessionPermissionAssistant spa, Detail detail) {
        List<TransformFunction> allFunctions = transformFunctionDao.findAll();
        List<TransformFunctionDto> tfs = new ArrayList<TransformFunctionDto>();
        if (detail == Detail.SIMPLE) {
            for (TransformFunction tf : allFunctions) {
                tfs.add(new TransformFunctionDto(tf, false));
            }
        } else if (userPrivilegeService.canCreate(spa, Privilege.CREATE_TRANSFORMFUNCTION)) {
            for (TransformFunction tf : allFunctions) {
                tfs.add(new TransformFunctionDto(tf, true));
            }
        } else {
            for (TransformFunction tf : allFunctions) {
                if (tf.getOwner() == spa.getUser()) {
                    tfs.add(new TransformFunctionDto(tf, true));
                } else {
                    tfs.add(new TransformFunctionDto(tf, false));
                }
            }
        }
        return tfs;
    }

    /**
     * Creates a transform function.
     *
     * @param spa the current user session
     * @param name the name of the function
     * @param functionType the function type
     * @param functionContent the function content
     */
    public void createFunction(SessionPermissionAssistant spa,
                               String name,
                               String functionType,
                               String functionContent) {
        if (!userPrivilegeService.canCreate(spa, Privilege.CREATE_TRANSFORMFUNCTION)) {
            throw new AuthException(spa, "need privilege " + Privilege.CREATE_TRANSFORMFUNCTION);
        }
        transformFunctionDao.create(name, spa.getUser(), functionType, functionContent);
    }

    /**
     * Gets the function.
     *
     * @param spa the current user session
     * @param name the name
     * @return the function
     */
    public TransformFunctionDto getFunction(SessionPermissionAssistant spa, String name) {
        if (!transformFunctionDao.exists(name)) {
            throw new DataException("TransformFunction does not exist: " + name, Status.NOT_FOUND);
        }
        TransformFunction tf = transformFunctionDao.getByName(name);
        if (tf.getOwner() == spa.getUser() || userPrivilegeService.canCreate(spa, Privilege.CREATE_TRANSFORMFUNCTION)) {
            return new TransformFunctionDto(tf, true);
        }
        return new TransformFunctionDto(tf, false);
    }

    /**
     * Delete function.
     *
     * @param spa the current user session
     * @param name the name
     */
    public void deleteFunction(SessionPermissionAssistant spa, String name) {
        if (!transformFunctionDao.exists(name)) {
            throw new DataException("TransformFunction does not exist: " + name, Status.NOT_FOUND);
        }
        TransformFunction tf = transformFunctionDao.getByName(name);
        if (tf.getOwner() != spa.getUser()) {
            throw new AuthException(spa, "Not the function owner");
        }
        transformFunctionDao.delete(name);
    }

    /**
     * Update the transform function content and type
     *
     * @param spa the current user session
     * @param name the name
     * @param functionType the function type
     * @param functionContent the function content
     */
    public void modifyFunction(SessionPermissionAssistant spa,
                               String name,
                               String functionType,
                               String functionContent) {
        if (!transformFunctionDao.exists(name)) {
            throw new DataException("TransformFunction does not exist: " + name, Status.NOT_FOUND);
        }
        TransformFunction tf = transformFunctionDao.getByName(name);
        if (tf.getOwner() != spa.getUser()) {
            throw new AuthException(spa, "Not the function owner");
        }
        transformFunctionDao.updateFunction(name, functionType, functionContent);
    }

    /**
     * Update the name of the transform function
     *
     * @param spa the current user session
     * @param name the current name
     * @param updatedName the updated name
     */
    public void modifyFunctionName(SessionPermissionAssistant spa, String name, String updatedName) {
        if (!transformFunctionDao.exists(name)) {
            throw new DataException("TransformFunction does not exist: " + name, Status.NOT_FOUND);
        }

        TransformFunction tf = transformFunctionDao.getByName(name);
        if (tf.getOwner() != spa.getUser()) {
            throw new AuthException(spa, "Not the function owner");
        }

        // check there isn't one existing with the new name
        if (transformFunctionDao.exists(updatedName)) {
            throw new DataException("TransformFunction already exists with name=" + updatedName, Status.CONFLICT);
        }

        // safe to update
        transformFunctionDao.updateName(name, updatedName);
    }

}
