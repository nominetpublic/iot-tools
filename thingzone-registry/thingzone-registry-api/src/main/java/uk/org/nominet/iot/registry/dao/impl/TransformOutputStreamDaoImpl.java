/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.ws.rs.core.Response.Status;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.AbstractJpaDao;
import uk.org.nominet.iot.registry.dao.TransformOutputStreamDao;
import uk.org.nominet.iot.registry.dao.dto.TransformId;
import uk.org.nominet.iot.registry.dao.dto.TransformOutputStreamRow;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.Transform;
import uk.org.nominet.iot.registry.model.TransformOutputStream;
import uk.nominet.iot.utils.error.ProgramDefectException;

public class TransformOutputStreamDaoImpl extends AbstractJpaDao<TransformOutputStream>
    implements TransformOutputStreamDao {

    public TransformOutputStreamDaoImpl() {
        super(TransformOutputStream.class);
    }

    @Override
    public TransformOutputStream create(Transform transform, DataStream datastream, String alias) {
        if (exists(transform, alias)) {
            throw new DataException("A output stream with the alias \"" + alias + "\" has already been added.",
                                    Status.CONFLICT);
        }
        if (exists(transform, datastream)) {
            throw new DataException("This outputStream has already been added to the transform", Status.CONFLICT);
        }
        TransformOutputStream outputStream = new TransformOutputStream(alias, transform, datastream);
        persist(outputStream);
        flush();
        return outputStream;
    }

    @Override
    public boolean exists(Transform transform, String alias) {
        try {
            find(transform, alias);
            return true;
        } catch (PersistenceException e) {
            if (e instanceof NoResultException) {
                return false;
            } else {
                throw e;
            }
        }
    }

    @Override
    public boolean exists(Transform transform, DataStream datastream) {
        try {
            loadByMatch("transform", transform, "dataStream", datastream);
            return true;
        } catch (PersistenceException e) {
            if (e instanceof NoResultException) {
                return false;
            } else {
                throw e;
            }
        }
    }

    private TransformOutputStream find(Transform transform, String alias) {
        return loadByMatch("transform", transform, "alias", alias);
    }

    @Override
    public void remove(Transform transform, String alias) {
        TransformOutputStream outputStream = find(transform, alias);
        deleteEntity(outputStream);
    }

    @Override
    public List<TransformOutputStream> getByTransform(Transform transform) {
        return findByFilter("transform", transform);
    }

    @Override
    public List<TransformOutputStream> findByStream(DataStream stream) {
        return findByFilter("stream", stream);
    }

    @Override
    public List<TransformOutputStreamRow> unmanagedGetOutputStreams(TransformId id) {
        List<TransformOutputStreamRow> streams = new ArrayList<>();

        try {
            String query = "SELECT transform_id, datastream_id, alias "
                + "FROM thingzone.transform_outputstreams WHERE transform_id = ?";
            PreparedStatement st = getConnection().prepareStatement(query);
            st.setLong(1, id.getId());
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                streams.add(
                    new TransformOutputStreamRow(new TransformId(rs.getLong("transform_id")),
                                                 new DNSkey(rs.getString("datastream_id")),
                                                 rs.getString("alias")));
            }
            st.close();
        } catch (SQLException e) {
            throw new ProgramDefectException("JDBC error", e);
        }
        return streams;
    }

}
