/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import uk.org.nominet.iot.registry.common.RegistryUtilities;
import uk.org.nominet.iot.registry.common.RegistryUtilities.Detail;
import uk.org.nominet.iot.registry.database.DatabaseTransaction;
import uk.org.nominet.iot.registry.database.SimpleDatabaseOperation;
import uk.org.nominet.iot.registry.model.Device;
import uk.org.nominet.iot.registry.service.DeviceService;
import uk.org.nominet.iot.registry.service.EntityService;
import uk.org.nominet.iot.registry.service.dto.DeviceSummaryDetailDto;
import uk.org.nominet.iot.registry.service.upsert.DataStreamLinkUpsertMetaService;
import uk.org.nominet.iot.registry.service.upsert.DnsDataUpsertMetaService;
import uk.org.nominet.iot.registry.service.upsert.PermissionsUpsertMetaService;
import uk.org.nominet.iot.registry.webserver.api.support.JsonErrorResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSingleResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSuccessResult;
import uk.org.nominet.iot.registry.webserver.api.support.PersistenceEffect;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;
import uk.org.nominet.iot.registry.webserver.api.support.SessionWrapper;

/**
 * The API handler for devices.
 */
@Path("/device")
public class DeviceHandler {
    private final static Logger log = LoggerFactory.getLogger(DeviceHandler.class);

    @Context
    Request request;

    @Context
    private javax.servlet.http.HttpServletRequest hsr;

    private final DeviceService deviceService;
    private final EntityService entityService;

    /**
     * Instantiates a new device handler.
     */
    public DeviceHandler() {
        deviceService = new DeviceService();
        entityService = new EntityService();
    }

    /**
     * returns a list of device keys.
     *
     * @return the keys
     */
    @GET
    @Path("list")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object list() {
        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[device/list] ()");
                List<String> keys = deviceService.getVisibleTo(spa).stream().map(device -> device.getKey().getKey())
                                                 .collect(Collectors.toList());
                return new JsonSingleResult("devices", keys).output();
            }
        });
    }

    /**
     * creates a Device, owned by the current user
     *
     * @param deviceKey the device key
     * @param name the name
     * @param type the type
     * @return the new device key
     */
    @POST
    @Path("create")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object createNamed(final @FormParam("device") String deviceKey, final @FormParam("name") String name,
                              final @FormParam("type") String type) {
        final boolean externallyNamed = !Strings.isNullOrEmpty(deviceKey);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[device/create] (deviceKey={}, name={}, type={})",
                    externallyNamed ? deviceKey : "(unspecified)", name, type);
                // create a new device
                Device created = deviceService.create(spa, externallyNamed, deviceKey, name, type);
                log.info("created Device {}", created.getKey());

                return new JsonSingleResult("device", created.getKey().getKey()).output();
            }
        });
    }

    /**
     * returns Device contents, including metadata.
     *
     * @param deviceKey the device key
     * @param detailOption the detail level
     * @param permissionsOption whether to show permissions
     * @return the device
     */
    @GET
    @Path("get")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object get(final @QueryParam("device") String deviceKey, final @QueryParam("detail") String detailOption,
                      final @QueryParam("showPermissions") Boolean permissionsOption) {
        if (Strings.isNullOrEmpty(deviceKey))
            return new JsonErrorResult("missing param \"device\"").asResponse(Status.BAD_REQUEST);
        final Detail detail = RegistryUtilities.DETAIL_FULL.equals(detailOption) ? Detail.FULL : Detail.SIMPLE;
        final boolean withPermissions = permissionsOption != null && permissionsOption.booleanValue() ? true : false;

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[device/get] (device={}, detail={}, permissions={})", deviceKey, detail, withPermissions);

                if (detail == Detail.SIMPLE) {
                    Device device = deviceService.getDevice(spa, deviceKey);
                    return new JsonSingleResult("device", deviceService.getSimpleDeviceSummary(spa, device)).output();
                } else {
                    DeviceSummaryDetailDto deviceDetail
                        = deviceService.getDeviceSummaryDetail(spa, deviceKey, withPermissions);
                    return new JsonSingleResult("device", deviceDetail).output();
                }
            }
        });
    }

    /**
     * updates the Device
     *
     * @param deviceKey the device key
     * @param metadata the metadata
     * @param location the location
     * @param name the name
     * @param type the type
     * @return success or failure
     */
    @PUT
    @Path("update")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object update(final @FormDataParam("device") String deviceKey,
                         final @FormDataParam("metadata") String metadata,
                         final @FormDataParam("location") String location,
                         final @FormDataParam("name") String name,
                         final @FormDataParam("type") String type) {
        if (Strings.isNullOrEmpty(deviceKey))
            return new JsonErrorResult("missing form part \"device\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[device/update] (device={}, name={}, type={}, metadata={}, location={})", deviceKey, name,
                    type, metadata, location);

                if (type != null || name != null) {
                    entityService.setValues(spa, deviceKey, type, name);
                }

                if (!Strings.isNullOrEmpty(metadata)) {
                    deviceService.setMetadata(spa, deviceKey, metadata);
                }
                if (location != null) {
                    deviceService.setLocation(spa, deviceKey, location);
                }
                return new JsonSuccessResult();
            }
        });
    }

    /**
     * attaches a DataStream to a Device.
     *
     * @param deviceKey the device key
     * @param dataStreamKey the data stream key
     * @param name the name
     * @return success or failure
     */
    @PUT
    @Path("add_stream")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object add_stream(final @FormParam("device") String deviceKey,
                             final @FormParam("dataStream") String dataStreamKey,
                             @FormParam("name") String name) {
        if (Strings.isNullOrEmpty(deviceKey))
            return new JsonErrorResult("missing param \"device\"").asResponse(Status.BAD_REQUEST);
        if (Strings.isNullOrEmpty(dataStreamKey))
            return new JsonErrorResult("missing param \"dataStream\"").asResponse(Status.BAD_REQUEST);
        // allow absent name & set to empty string
        if (Strings.isNullOrEmpty(name))
            name = "";
        final String sodName = name;

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[device/add_stream] (device={}, dataStream={})", deviceKey, dataStreamKey);

                deviceService.addDataStream(spa, deviceKey, dataStreamKey, sodName);
                return new JsonSuccessResult();
            }
        });
    }

    /**
     * removes a DataStream from a Device.
     *
     * @param deviceKey the device key
     * @param dataStreamKey the data stream key
     * @return success or failure
     */
    @PUT
    @Path("remove_stream")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object remove_stream(final @FormParam("device") String deviceKey,
                                final @FormParam("dataStream") String dataStreamKey) {
        if (Strings.isNullOrEmpty(deviceKey))
            return new JsonErrorResult("missing param \"device\"").asResponse(Status.BAD_REQUEST);
        if (Strings.isNullOrEmpty(dataStreamKey))
            return new JsonErrorResult("missing param \"dataStream\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[device/remove_stream] (device={}, dataStream={})", deviceKey, dataStreamKey);

                deviceService.removeDataStream(spa, deviceKey, dataStreamKey);
                return new JsonSuccessResult();
            }
        });
    }

    /**
     * lists DataStreams attached to a Device.
     *
     * @param deviceKey the device key
     * @return the streams
     */
    @GET
    @Path("get_streams")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object get_streams(final @QueryParam("device") String deviceKey) {
        if (Strings.isNullOrEmpty(deviceKey)) {
            return new JsonErrorResult("missing param \"device\"").asResponse(Status.BAD_REQUEST);
        }

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[device/get_streams] (device={})", deviceKey);
                Device device = deviceService.getDevice(spa, deviceKey);
                List<String> streamKeys
                    = deviceService.getDataStreams(device).stream().map(datastream -> datastream.getKey().getKey())
                                   .collect(Collectors.toList());
                return new JsonSingleResult("dataStreams", streamKeys).output();
            }
        });
    }

    /**
     * deletes a Device (but not any datastreams etc).
     *
     * @param deviceKey the device key
     * @return success or failure
     */
    @DELETE
    @Path("delete")
    @Produces(MediaType.APPLICATION_JSON)
    public Object delete(final @QueryParam("device") String deviceKey) {
        if (Strings.isNullOrEmpty(deviceKey))
            return new JsonErrorResult("missing param \"device\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[device/delete] (device={})", deviceKey);

                deviceService.delete(spa, deviceKey);
                return new JsonSuccessResult();
            }
        });
    }

    /**
     * upserts i.e. creates/updates a Device
     *
     * multipart; receives JSON as text
     *
     * @param deviceKey the device key
     * @param metadata the metadata
     * @param location the location
     * @param dataStreamLinks the data stream links
     * @param userPermissions the user permissions
     * @param groupPermissions the group permissions
     * @param dnsRecords the dns records
     * @param type the type
     * @param name the name
     * @return the device
     */
    @PUT
    @Path("upsert")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Object upsert(final @FormDataParam("device") String deviceKey,
                         final @FormDataParam("metadata") String metadata,
                         final @FormDataParam("location") String location,
                         final @FormDataParam("dataStreams") String dataStreamLinks,
                         final @FormDataParam("userPermissions") String userPermissions,
                         final @FormDataParam("groupPermissions") String groupPermissions,
                         final @FormDataParam("dnsRecords") String dnsRecords, final @FormDataParam("type") String type,
                         final @FormDataParam("name") String name) {
        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        final boolean externallyNamed = !Strings.isNullOrEmpty(deviceKey);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                Device device = deviceService.upsert(spa, deviceKey, externallyNamed);
                String upsertKey = device.getKey().getKey();
                log.info("[device/upsert] (device={})", upsertKey);

                if (type != null || name != null) {
                    entityService.setValues(spa, upsertKey, type, name);
                }
                if (!Strings.isNullOrEmpty(metadata)) {
                    deviceService.setMetadata(spa, upsertKey, metadata);
                }
                if (location != null) {
                    deviceService.setLocation(spa, upsertKey, location);
                }
                if (!Strings.isNullOrEmpty(dataStreamLinks)) {
                    new DataStreamLinkUpsertMetaService().upsert(spa, upsertKey, dataStreamLinks);
                }
                if (!Strings.isNullOrEmpty(userPermissions)) {
                    new PermissionsUpsertMetaService().upsertUserPermissions(spa, upsertKey, userPermissions);
                }
                if (!Strings.isNullOrEmpty(groupPermissions)) {
                    new PermissionsUpsertMetaService().upsertGroupPermissions(spa, upsertKey, groupPermissions);
                }
                if (!Strings.isNullOrEmpty(dnsRecords)) {
                    new DnsDataUpsertMetaService().upsert(spa, upsertKey, dnsRecords);
                }
                DeviceSummaryDetailDto deviceDetail = deviceService.getDeviceSummaryDetail(spa, upsertKey, true);
                return new JsonSingleResult("device", deviceDetail).output();
            }
        });
    }

}
