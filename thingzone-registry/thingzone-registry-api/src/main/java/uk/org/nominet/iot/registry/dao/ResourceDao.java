/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao;

import java.util.List;

import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.model.Resource;

public interface ResourceDao extends JpaDao<Resource> {

    Resource createKnownKey(String key, String name, String type, String contentType, byte[] content);

    Resource find(String key, String name);

    List<Resource> listByKey(String key, List<Account> accounts, List<Permission> permissions);

    List<Resource> listByKey(String key, List<Account> accounts, Permission permission);

    List<Resource> getAllowed(List<Account> accounts, List<Permission> permissions);

    List<Resource> getAllowed(List<Account> accounts, Permission permission);

    List<Resource> listByType(List<Account> accounts, List<Permission> permissions, String type, String contentType);

    void delete(Resource resource);

    boolean exists(String key, String name);
}
