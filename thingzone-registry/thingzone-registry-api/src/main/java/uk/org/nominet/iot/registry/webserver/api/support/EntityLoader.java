/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api.support;

import javax.persistence.NoResultException;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import uk.org.nominet.iot.registry.common.AuthException;
import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.*;
import uk.org.nominet.iot.registry.model.*;

/**
 * assistant to load objects as required by handlers with error-handling
 */
public class EntityLoader {
    private final static Logger log = LoggerFactory.getLogger(EntityLoader.class);

    private static DeviceDao getDeviceDao() {
        return DaoFactoryManager.getFactory().deviceDao();
    }

    private static DataStreamDao getDataStreamDao() {
        return DaoFactoryManager.getFactory().dataStreamDao();
    }

    private static TransformFunctionDao getTransformFunctionDao() {
        return DaoFactoryManager.getFactory().transformFunctionDao();
    }

    private static TransformDao getTransformDao() {
        return DaoFactoryManager.getFactory().transformDao();
    }

    private static DNSkeyDao getDnsKeyDao() {
        return DaoFactoryManager.getFactory().dnsKeyDao();
    }

    private static ACLDao getACLDao() {
        return DaoFactoryManager.getFactory().aclDao();
    }

    private static UserPrivilegeDao getUserPrivilegeDao() {
        return DaoFactoryManager.getFactory().userPrivilegeDao();
    }

    private static boolean canQueryStreams(SessionPermissionAssistant spa) {
        return getUserPrivilegeDao().exists(spa.getUser(), Privilege.QUERY_STREAMS);
    }

    public static Device getDevice(SessionPermissionAssistant spa, String deviceKey, Permission permission) {
        Device device = getDeviceWithoutCheckingPermission(deviceKey);

        // check permission
        if (canQueryStreams(spa)) {
            return device;
        } else if (getACLDao().isAllowed(device.getKey(), spa.getRelevantAccounts(), permission)) {
            return device;
        } else {
            throw new AuthException(spa, device.getKey(), "need " + permission + " permission");
        }
    }

    public static Device getDeviceWithoutCheckingPermission(String deviceKey) {
        if (Strings.isNullOrEmpty(deviceKey))
            throw new DataException("missing Device selector");

        Device device;
        try {
            device = getDeviceDao().getByName(deviceKey);
            log.info("found Device {}", deviceKey);
            return device;
        } catch (NoResultException e) {
            throw new DataException("Device not found: " + deviceKey, Status.NOT_FOUND);
        }
    }

    public static DataStream getDataStream(SessionPermissionAssistant spa,
                                           String dataStreamKey,
                                           Permission permission) {
        DataStream dataStream = getDataStreamWithoutCheckingPermission(dataStreamKey);
        // check permission
        if (canQueryStreams(spa)) {
            return dataStream;
        } else if (getACLDao().isAllowed(dataStream.getKey(), spa.getRelevantAccounts(), permission)) {
            return dataStream;
        } else {
            throw new AuthException(spa, dataStream.getKey(), "need " + permission + " permission");
        }

    }

    public static DataStream getDataStreamWithoutCheckingPermission(String dataStreamKey) {
        if (Strings.isNullOrEmpty(dataStreamKey))
            throw new DataException("missing DataStream selector");

        DataStream dataStream;
        try {
            dataStream = getDataStreamDao().getByName(dataStreamKey);
            log.info("found DataStream {}", dataStreamKey);
            return dataStream;
        } catch (NoResultException e) {
            throw new DataException("DataStream not found: " + dataStreamKey, Status.NOT_FOUND);
        }
    }

    public static TransformFunction getTransformFunction(String name) {
        if (Strings.isNullOrEmpty(name))
            throw new DataException("missing Transform name selector");
        // (we allow empty owner)

        TransformFunction transformFunction;
        try {
            transformFunction = getTransformFunctionDao().getByName(name);
            log.info("found Transform Function {}", transformFunction);
            return transformFunction;
        } catch (NoResultException e) {
            throw new DataException("Transform Function not found with name: " + name, Status.NOT_FOUND);
        }
    }

    public static Transform getTransform(SessionPermissionAssistant spa, String transformKey, Permission permission) {
        Transform transform = getTransformWithoutCheckingPermission(transformKey);
        DNSkey key = transform.getDataStream().getKey();

        // check permission
        if (canQueryStreams(spa)) {
            return transform;
        } else if (getACLDao().isAllowed(key, spa.getRelevantAccounts(), permission)) {
            return transform;
        } else {
            throw new AuthException(spa, key, "need " + permission + " permission");
        }
    }

    private static Transform getTransformWithoutCheckingPermission(String transformKey) {
        if (Strings.isNullOrEmpty(transformKey))
            throw new DataException("missing Transform selector");

        // get underlying DataStream
        DataStream dataStream;
        try {
            dataStream = getDataStreamWithoutCheckingPermission(transformKey);
        } catch (DataException e) {
            throw new DataException("Transform not found: " + transformKey, Status.NOT_FOUND);
        }

        // upgrade to Transform
        Transform transform;
        try {
            transform = getTransformDao().getByOutputStream(dataStream);
            log.info("found Transform {}", transformKey);
            return transform;
        } catch (NoResultException e) {
            throw new DataException("Transform not found: " + transformKey, Status.NOT_FOUND);
        }
    }

    public static DNSkey getDnsKey(SessionPermissionAssistant spa,
                                   String key,
                                   Permission permission) {
        DNSkey dnsKey = getDnsKeyWithoutCheckingPermission(key);

        // check permission
        if (canQueryStreams(spa)) {
            return dnsKey;
        } else if (getACLDao().isAllowed(dnsKey, spa.getRelevantAccounts(), permission)) {
            return dnsKey;
        } else {
            throw new AuthException(spa, dnsKey, "need " + permission + " permission");
        }
    }

    public static DNSkey getDnsKeyWithoutCheckingPermission(String key) {
        if (Strings.isNullOrEmpty(key))
            throw new DataException("missing DNSkey selector");

        DNSkey dnsKey;
        try {
            dnsKey = getDnsKeyDao().getByKey(key);
            log.info("found DNSkey {}", dnsKey);
        } catch (NoResultException e) {
            throw new DataException("DNSkey not found: " + key, Status.NOT_FOUND);
        }
        return dnsKey;
    }

}
