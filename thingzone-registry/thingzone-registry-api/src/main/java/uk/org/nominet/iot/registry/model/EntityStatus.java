/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import java.sql.SQLException;
import javax.persistence.Converter;
import org.postgresql.util.PGobject;
import uk.nominet.iot.utils.error.ProgramDefectException;

public enum EntityStatus implements PersistableEnum<PGobject> {
    KEY, DEVICE, DATASTREAM, DELETED;

    public static boolean isValid(String s) {
        try {
            EntityStatus.valueOf(s);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    public static EntityStatus fromString(String s) {
        try {
            return EntityStatus.valueOf(s);
        } catch (IllegalArgumentException e) {
            throw new ProgramDefectException("invalid entity status: \'" + s + "\'");
        }
    }

    @Override
    public PGobject getValue() {
        PGobject object = new PGobject();
        object.setType("entity_status_type");
        try {
            object.setValue(this.toString());
        } catch (SQLException e) {
            throw new IllegalArgumentException("Error when creating PostgreSQL enum", e);
        }
        return object;
    }

    @Converter
    public static class EntityStatusConverter extends AbstractEnumConverter<EntityStatus, PGobject> {
        public EntityStatusConverter() {
            super(EntityStatus.class);
        }
    }
}
