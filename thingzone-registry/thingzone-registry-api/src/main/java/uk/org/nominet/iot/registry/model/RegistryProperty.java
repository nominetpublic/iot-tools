/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import uk.org.nominet.iot.registry.database.DatabaseDriver;

@Entity
@Table(name = "REGISTRY_PROPERTIES", schema = DatabaseDriver.SCHEMA_NAME)
public class RegistryProperty {

    public final static String PROPERTY_DB_CREATED = "db schema created";
    public final static String PROPERTY_DB_CREATED_BY = "db schema created by";
    public final static String PROPVAL_SCHEMA_CREATOR = "schema creator";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @SuppressWarnings("unused")
    private Long id;

    @Column(nullable = false, unique = true)
    private String property;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "LAST_UPDATED", columnDefinition = "TIMESTAMP", nullable = false)
    @SuppressWarnings("unused")
    private Date updatedTime;

    private String value;

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Registry Property [" + property + "=\"" + value + "\"]";
    }

}
