/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import org.eclipse.persistence.annotations.Customizer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonRawValue;

import uk.org.nominet.iot.registry.database.ColumnPosition;
import uk.org.nominet.iot.registry.database.ColumnPositionCustomizer;
import uk.org.nominet.iot.registry.database.DatabaseDriver;

import javax.persistence.*;

@Entity
@Table(name = "PushSubscribers", schema = DatabaseDriver.SCHEMA_NAME,
       uniqueConstraints = { @UniqueConstraint(name = "UQ_PushSubscribers", columnNames = { "streamKey, URI" }) })
@Customizer(ColumnPositionCustomizer.class)
public class PushSubscriber {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ColumnPosition(position = 1)
    @JsonIgnore
    private Long id;

    @JoinColumn(name = "streamKey", referencedColumnName = "DNSkey", nullable = false)
    @ColumnPosition(position = 2)
    private DataStream dataStream;

    @Column(name = "uri", nullable = false)
    @ColumnPosition(position = 3)
    private String uri;

    @Column(name = "uri_params", columnDefinition = "JSONB NOT NULL", nullable = false) // empty="{}"
    @Convert(converter = JsonbStringConverter.class) // our custom (to/from String) converter
    @ColumnPosition(position = 4)
    @JsonRawValue
    private String uriParams;

    @Column(name = "headers", columnDefinition = "JSONB", nullable = true) // empty="{}"
    @Convert(converter = JsonbStringConverter.class) // our custom (to/from String) converter
    @ColumnPosition(position = 5)
    @JsonRawValue
    private String headers;

    @Column(name = "method", nullable = true)
    @ColumnPosition(position = 6)
    private String method;

    @Column(name = "retry_sequence", nullable = true)
    @ColumnPosition(position = 7)
    private String retrySequence;

    PushSubscriber() {}

    public PushSubscriber(DataStream dataStream, String uri, String uriParams, String headers, String method,
                          String retrySequence) {
        this.dataStream = dataStream;
        this.uri = uri;
        this.uriParams = uriParams;
        this.setHeaders(headers);
        this.setMethod(method);
        this.setRetrySequence(retrySequence);
    }

    public Long getId() {
        return id;
    }

    public DataStream getDataStream() {
        return dataStream;
    }

    public String getUri() {
        return uri;
    }

    public void setUriParams(String uriParams) {
        this.uriParams = uriParams;
    }

    public String getUriParams() {
        return uriParams;
    }

    public void setHeaders(String headers) {
        this.headers = headers;
    }

    public String getHeaders() {
        return headers;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getMethod() {
        return method;
    }

    public void setRetrySequence(String retrySequence) {
        this.retrySequence = retrySequence;
    }

    public String getRetrySequence() {
        return retrySequence;
    }

    @Override
    public String toString() {
        return "PushSubscriber{" + "id=" + id + ", dataStream=" + dataStream + ", uri='" + uri + '\'' + ", uriParams='"
            + uriParams + '\'' + ", headers='" + headers + '\'' + ", method='" + method + '\'' + ", retrySequence='"
            + retrySequence + '\'' + '}';
    }

}
