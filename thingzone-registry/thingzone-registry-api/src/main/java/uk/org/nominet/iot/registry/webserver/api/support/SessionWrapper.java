/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api.support;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.SessionDao;
import uk.org.nominet.iot.registry.database.DatabaseTransaction;
import uk.org.nominet.iot.registry.database.SimpleDatabaseOperation;
import uk.org.nominet.iot.registry.model.*;
import uk.org.nominet.iot.registry.service.SessionService;
import uk.org.nominet.iot.registry.webserver.ConfigFactory;
import uk.org.nominet.iot.registry.webserver.api.SessionHandler;
import uk.nominet.iot.utils.error.ProgramDefectException;

import java.util.List;

/**
 * SessionWrapper wraps up the checking of a session in its own DB transaction (writes to DB to update session
 * freshness)
 */
public class SessionWrapper {
    private final static Logger log = LoggerFactory.getLogger(SessionWrapper.class);

    private String sessionKey;
    private String remoteIp;

    // we either get log-on session or an error blob
    private GenericSession session = null;
    private Object error = null;

    public SessionWrapper(HttpServletRequest hsr) {
        this(hsr, false);
    }

    // private constructor
    private SessionWrapper(HttpServletRequest hsr, boolean privilegedConnectionExpected) {

        String remoteIp = hsr.getRemoteAddr();

        if (privilegedConnectionExpected) {
            if (!isPrivilegedAddress(remoteIp)) {
                this.error = new JsonErrorResult("privileged connection required").asResponse(Status.FORBIDDEN);
                log.info("session refused: privileged connection required (connection from {})", remoteIp);
                return;
            }
        }

        this.sessionKey = hsr.getHeader(SessionHandler.SESSIONKEY_HEADER);
        this.remoteIp = remoteIp;

        connect();
    }

    // create session from internal / another thingzone process
    public static SessionWrapper createPrivilegedSession(HttpServletRequest hsr) {
        return new SessionWrapper(hsr, true);
    }

    public boolean failed() {
        return !succeeded();
    }

    private boolean succeeded() {
        return session != null;
    }

    public GenericSession getSession() {
        if (session == null)
            throw new ProgramDefectException("session not available");
        return session;
    }

    public Object getError() {
        if (error == null)
            throw new ProgramDefectException("no error stored");
        return error;
    }

    // is this an internal connection (e.g. from thingzone Bridge)?
    private final static String ANY_IP = "0.0.0.0";

    static boolean isPrivilegedAddress(String remoteIp) {
        List<String> privilegedClients = ConfigFactory.getConfig().getPrivilegedClients();
        for (String client : privilegedClients) {
            if (client.equals(remoteIp) || client.equals(ANY_IP)) {
                return true;
            }
        }
        return false;
    }

    boolean isPrivilegedAddress() {
        return isPrivilegedAddress(remoteIp);
    }

    private void connect() {
        if (session != null || error != null) {
            // shouldn't be re-trying this
            throw new ProgramDefectException("should only look up session once");
        }

        // parameter sanity
        if (Strings.isNullOrEmpty(sessionKey)) {
            // treat this as a public login
            connectAsPublicUser();
        } else {
            connectAsUser();
        }

    }

    private void connectAsPublicUser() {
        this.session = new PublicSession();
        log.info("public session created");
    }

    private void connectAsUser() {
        if (Strings.isNullOrEmpty(remoteIp)) {
            String msg = "failed to get remote IP for session check";
            this.error = new JsonErrorResult(msg).asResponse(Status.UNAUTHORIZED);
            log.info(msg);
            return;
        }

        // retrieve session, using a separate database transaction
        Object result = new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(null) {
            public Object execute() {

                SessionService sessionService = new SessionService();
                Session session = sessionService.getSession(sessionKey, remoteIp);

                // explicitly detach Session from EntityManager:
                // - strictly speaking this is unnecessary, because we could just keep hold
                // of the session after the transaction completes, but doing this makes it
                // clear that it's deliberate
                SessionDao sessionDao = DaoFactoryManager.getFactory().sessionDao();
                sessionDao.detach(session);

                return session;
            }
        });

        if (result.getClass() == Session.class) {
            Session session = (Session) result;
            // retrieved session OK
            this.session = session;
            log.info("SessionWrapper retrieved valid session {} for user {}", session.getSessionKey(),
                this.session.getUsername());
        } else {
            // failed to retrieve session: we actually got an error message
            log.info("SessionWrapper failed to find valid session");
            this.error = result;
        }
    }

}
