/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.ws.rs.core.Response.Status;

import com.google.common.collect.ImmutableList;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.AbstractJpaDao;
import uk.org.nominet.iot.registry.dao.GroupUserDao;
import uk.org.nominet.iot.registry.model.Group;
import uk.org.nominet.iot.registry.model.GroupUser;
import uk.org.nominet.iot.registry.model.User;

public class GroupUserDaoImpl extends AbstractJpaDao<GroupUser> implements GroupUserDao {

    GroupUserDaoImpl() {
        super(GroupUser.class);
    }

    @Override
    public GroupUser create(Group group, User user, boolean isAdmin) {
        if (exists(group, user)) {
            throw new DataException("User \"" + user.getId() + "\" has already been added to Group \"" + group.getName()
                + "\"", Status.CONFLICT);
        }
        GroupUser gu = new GroupUser(group, user, isAdmin);
        persist(gu);
        flush(); // insert into database
        return gu;
    }

    @Override
    public void remove(Group group, User user) {
        GroupUser gu = find(group, user);
        deleteEntity(gu);
    }

    @Override
    public boolean exists(Group group, User user) {
        try {
            find(group, user);
            return true;
        } catch (PersistenceException e) {
            if (e instanceof NoResultException) {
                return false;
            } else {
                throw e;
            }
        }
    }

    @Override
    public boolean isAdmin(Group group, User user) {
        try {
            GroupUser find = find(group, user);
            if (find.isAdmin()) {
                return true;
            }
            return false;
        } catch (PersistenceException e) {
            if (e instanceof NoResultException) {
                return false;
            } else {
                throw e;
            }
        }
    }

    @Override
    public void setAdmin(Group group, User user, boolean setAdmin) {
        try {
            GroupUser find = find(group, user);
            find.setAdmin(setAdmin);
        } catch (PersistenceException e) {
            throw e;
        }
    }

    @Override
    public List<Group> listGroupsByUser(User user) {
        List<GroupUser> groupUsers = findByFilter("user", user);
        List<Group> groups = new ArrayList<>();
        for (GroupUser groupUser : groupUsers) {
            groups.add(groupUser.getGroup());
        }
        return ImmutableList.copyOf(groups);
    }

    @Override
    public List<User> listUsersByGroup(Group group) {
        List<GroupUser> groupUsers = findByFilter("group", group);
        List<User> users = new ArrayList<>();
        for (GroupUser groupUser : groupUsers) {
            users.add(groupUser.getUser());
        }
        return ImmutableList.copyOf(users);
    }

    private GroupUser find(Group group, User user) {
        return loadByMatch("group", group, "user", user);
    }
}
