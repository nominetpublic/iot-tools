/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import java.util.List;
import java.util.Properties;

import uk.org.nominet.iot.registry.dao.AbstractJpaDao;
import uk.org.nominet.iot.registry.dao.RegistryPropertyDao;
import uk.org.nominet.iot.registry.model.RegistryProperty;

class RegistryPropertyDaoImpl extends AbstractJpaDao<RegistryProperty> implements RegistryPropertyDao {
    RegistryPropertyDaoImpl() {
        super(RegistryProperty.class);
    }

    @Override
    public Properties getProperties() {
        Properties props = new Properties();
        List<RegistryProperty> all = findAll();
        for (RegistryProperty rp : all) {
            props.setProperty(rp.getProperty(), rp.getValue());
        }
        return props;
    }

    @Override
    public void setProperty(String key, String value) {
        RegistryProperty rp = new RegistryProperty();
        rp.setProperty(key);
        rp.setValue(value);
        persist(rp);
    }

}
