/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service;

import static uk.org.nominet.iot.registry.database.DatabaseDriver.ALL_USERS_GROUP;
import java.util.List;

import javax.persistence.NoResultException;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import uk.org.nominet.iot.registry.common.AuthException;
import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.AccountDao;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.GroupDao;
import uk.org.nominet.iot.registry.dao.GroupUserDao;
import uk.org.nominet.iot.registry.dao.UserCredentialsDao;
import uk.org.nominet.iot.registry.dao.UserDao;
import uk.org.nominet.iot.registry.dao.UserTokenDao;
import uk.org.nominet.iot.registry.model.Group;
import uk.org.nominet.iot.registry.model.Privilege;
import uk.org.nominet.iot.registry.model.User;
import uk.org.nominet.iot.registry.model.UserCredentials;
import uk.org.nominet.iot.registry.model.UserToken;
import uk.org.nominet.iot.registry.model.UserTokenType;
import uk.org.nominet.iot.registry.service.dto.UserSummaryDto;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;

public class UserService {
    private final static Logger log = LoggerFactory.getLogger(UserService.class);

    private final Privilege PRIVILEGE_REQUIRED_FOR_CREATE = Privilege.CREATE_USER;

    private UserDao userDao;
    private UserCredentialsDao userCredentialsDao;
    private AccountDao accountDao;
    private UserTokenDao userTokenDao;
    private GroupDao groupDao;
    private GroupUserDao groupUserDao;
    private UserPrivilegeService userPrivilegeService;

    public UserService() {
        DaoFactory df = DaoFactoryManager.getFactory();
        userDao = df.userDao();
        userCredentialsDao = df.userCredentialsDao();
        accountDao = df.accountDao();
        userTokenDao = df.userTokenDao();
        groupDao = df.groupDao();
        groupUserDao = df.groupUserDao();
        userPrivilegeService = new UserPrivilegeService();
    }

    public User getUserByUsername(String username) {
        return userDao.getUserByUsername(username);
    }

    String getUsernameByUser(User user) {
        return userCredentialsDao.getUsernameByUser(user);
    }

    public UserSummaryDto getDetails(SessionPermissionAssistant spa, String username) {
        if (spa.getUsername().equals(username)) {
            username = null; // not really an attempt to view another users details
        }

        if (!Strings.isNullOrEmpty(username)) {
            // you can get details of all users, if you have USER_ADMIN privilege
            if (userPrivilegeService.hasUserAdminPrivilege(spa)) {
                if (!userCredentialsDao.exists(username)) {
                    throw new DataException("user does not exist: " + username, Status.NOT_FOUND);
                }
                User target = getUserByUsername(username);
                return new UserSummaryDto(username, target.getDetails(), target.getPreferences(),
                                          userPrivilegeService.getPrivileges(target));
            } else {
                // but no-one else can
                throw new AuthException(spa, "cannot view details of another user");
            }
        }
        return new UserSummaryDto(spa.getUsername(), spa.getUser().getDetails(), spa.getUser().getPreferences(),
                                  userPrivilegeService.getPrivileges(spa.getUser()));
    }

    public List<String> getUsernames(SessionPermissionAssistant spa) {
        // admin privs required
        if (!userPrivilegeService.hasUserAdminPrivilege(spa)) {
            throw new AuthException(spa, "need privilege " + Privilege.USER_ADMIN);
        } else {
            return userCredentialsDao.getStandardUsers();
        }
    }

    public void update(SessionPermissionAssistant spa,
                       String username,
                       String newUsername,
                       String details,
                       String preferences) {
        if (spa.getUsername().equals(username)) {
            username = null;
        }

        if (!Strings.isNullOrEmpty(username)) {
            if (userPrivilegeService.hasUserAdminPrivilege(spa)) {
                if (!userCredentialsDao.exists(username)) {
                    throw new DataException("user does not exist: " + username, Status.NOT_FOUND);
                }
                User target = getUserByUsername(username);
                if (!Strings.isNullOrEmpty(newUsername)) {
                    if (userCredentialsDao.exists(newUsername)) {
                        throw new DataException("A user with the updated username already exists", Status.CONFLICT);
                    }
                    UserCredentials userCredentials = userCredentialsDao.get(username);
                    userCredentials.setUsername(newUsername);
                }
                if (!Strings.isNullOrEmpty(details)) {
                    target.setDetails(details);
                }
                if (!Strings.isNullOrEmpty(preferences)) {
                    target.setPreferences(preferences);
                }
            } else {
                throw new AuthException(spa, "Requres USER_ADMIN privilege");
            }
        } else {
            User user = spa.getUser();
            if (!Strings.isNullOrEmpty(details)) {
                user.setDetails(details);
            }
            if (!Strings.isNullOrEmpty(preferences)) {
                user.setPreferences(preferences);
            }
        }
    }

    public void createUser(SessionPermissionAssistant spa,
                           String username,
                           String password,
                           String email,
                           List<Privilege> privileges) {
        // Does the user have the privilege to create another user?
        if (!userPrivilegeService.canCreate(spa, PRIVILEGE_REQUIRED_FOR_CREATE)) {
            throw new AuthException(spa, "need privilege " + PRIVILEGE_REQUIRED_FOR_CREATE);
        }

        // Does the user have the privilege to grant privileges to the user we're
        // creating?
        for (Privilege p : privileges) {
            if (!userPrivilegeService.canGrant(spa, p)) {
                throw new AuthException(spa, "need grants to give privilege " + p);
            }
        }

        if (userCredentialsDao.exists(username)) {
            throw new DataException("username already in use", Status.CONFLICT);
        }

        // create user
        User user = userDao.create();
        // create & attach credentials
        userCredentialsDao.create(username, password, user, email);
        // create & attach account
        accountDao.create(user);

        // add privileges
        userPrivilegeService.grantPrivileges(spa, user, privileges);

        Group group = groupDao.getByName(ALL_USERS_GROUP);
        groupUserDao.create(group, user, false);
    }

    public void setPrivileges(SessionPermissionAssistant spa, String username, List<Privilege> requiredPrivileges) {
        // admin privs required
        if (!userPrivilegeService.hasUserAdminPrivilege(spa)) {
            throw new AuthException(spa, "need privilege " + Privilege.USER_ADMIN);
        }

        // get current permissions
        User user = getUserByUsername(username);
        List<Privilege> currentPrivileges = userPrivilegeService.getPrivileges(user);

        // make the changes
        for (DifferenceCalculator<Privilege>.Difference diff : new DifferenceCalculator<Privilege>().calculate(
            requiredPrivileges, currentPrivileges)) {
            if (diff.difference == DifferenceCalculator.Type.ADD) {
                userPrivilegeService.adminAddPrivilege(user, diff.value);
                log.info("added privilege {} to {}", diff.value, username);
            } else {
                userPrivilegeService.adminRemovePrivilege(user, diff.value);
                log.info("removed privilege {} from {}", diff.value, username);
            }
        }
    }

    public void setPassword(String username, String token, String newPassword) {
        try {
            UserToken userToken = userTokenDao.get(token, UserTokenType.PASSWORD);
            if (!userTokenDao.valid(userToken)) {
                throw new DataException("Invalid Token", Status.FORBIDDEN);
            }
            userCredentialsDao.tokenUpdatePassword(username, userToken, newPassword);
        } catch (NoResultException e) {
            throw new DataException("Invalid Token", Status.FORBIDDEN);
        }
    }

    public String getToken(SessionPermissionAssistant spa,
                           String username,
                           UserTokenType tokenType,
                           long secondsExpiry) {
        if (!userPrivilegeService.hasUserAdminPrivilege(spa)) {
            throw new AuthException(spa, "need privilege " + Privilege.USER_ADMIN);
        }

        UserCredentials userCredentials = userCredentialsDao.get(username);
        UserToken newToken = userTokenDao.create(userCredentials, tokenType, secondsExpiry);
        return newToken.getToken();
    }

}
