/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.dto;

import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.Permission;

// raw DB row object
public class AclRow {
    private AclId id;
    private DNSkey key;
    private AccountId accountId;
    private Permission permission;

    public AclRow(AclId aclId, DNSkey key, AccountId accountId, Permission permission) {
        this.id = aclId;
        this.key = key;
        this.accountId = accountId;
        this.permission = permission;
    }

    public AclId getId() {
        return id;
    }

    public DNSkey getKey() {
        return key;
    }

    public AccountId getAccountId() {
        return accountId;
    }

    public Permission getPermission() {
        return permission;
    }
}
