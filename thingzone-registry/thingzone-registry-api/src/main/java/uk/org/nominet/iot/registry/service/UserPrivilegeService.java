/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service;

import java.util.List;

import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nominet.iot.utils.error.ProgramDefectException;
import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.UserPrivilegeDao;
import uk.org.nominet.iot.registry.model.Privilege;
import uk.org.nominet.iot.registry.model.User;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;

public class UserPrivilegeService {
    private final static Logger log = LoggerFactory.getLogger(UserPrivilegeService.class);

    private final UserPrivilegeDao userPrivilegeDao;

    public UserPrivilegeService() {
        DaoFactory df = DaoFactoryManager.getFactory();
        userPrivilegeDao = df.userPrivilegeDao();
    }

    boolean canCreate(SessionPermissionAssistant spa, Privilege privilege) {
        checkIsCreatePrivilege(privilege);

        // USER_ADMIN privilege enables all user/grant creation
        if (privilege == Privilege.CREATE_USER && hasUserAdminPrivilege(spa)) {
            return true;
        }

        return userPrivilegeDao.exists(spa.getUser(), privilege);
    }

    boolean canGrant(SessionPermissionAssistant spa, Privilege privilege) {
        if (hasUserAdminPrivilege(spa)) {
            return true; // admin can grant anything
        }

        // check grants
        switch (privilege) {
        // specific cases: can grant if user has the specific grant privilege
        case CREATE_USER:
            return userPrivilegeDao.exists(spa.getUser(), Privilege.GRANT_CREATE_USER);
        case CREATE_GROUP:
            return userPrivilegeDao.exists(spa.getUser(), Privilege.GRANT_CREATE_GROUP);
        case CREATE_DEVICE:
            return userPrivilegeDao.exists(spa.getUser(), Privilege.GRANT_CREATE_DEVICE);
        case CREATE_DATASTREAM:
            return userPrivilegeDao.exists(spa.getUser(), Privilege.GRANT_CREATE_DATASTREAM);
        case CREATE_TRANSFORM:
            return userPrivilegeDao.exists(spa.getUser(), Privilege.GRANT_CREATE_TRANSFORM);
        case CREATE_TRANSFORMFUNCTION:
            return userPrivilegeDao.exists(spa.getUser(), Privilege.GRANT_CREATE_TRANSFORMFUNCTION);
        case CREATE_DETECTOR:
            return userPrivilegeDao.exists(spa.getUser(), Privilege.GRANT_CREATE_DETECTOR);
        case RUN_DETECTOR:
            return userPrivilegeDao.exists(spa.getUser(), Privilege.GRANT_RUN_DETECTOR);
        case CREATE_RESOURCE:
            return userPrivilegeDao.exists(spa.getUser(), Privilege.GRANT_CREATE_RESOURCE);
        case CREATE_ENTITY:
            return userPrivilegeDao.exists(spa.getUser(), Privilege.GRANT_CREATE_ENTITY);

        // general cases: can grant if use has "grant all grants" privilege
        case GRANT_CREATE_USER: // fall-through
        case GRANT_CREATE_GROUP: // fall-through
        case GRANT_CREATE_DEVICE: // fall-through
        case GRANT_CREATE_DATASTREAM: // fall-through
        case GRANT_CREATE_TRANSFORM: // fall-through
        case GRANT_CREATE_TRANSFORMFUNCTION: // fall-through
        case GRANT_CREATE_DETECTOR: // fall-through
        case GRANT_RUN_DETECTOR: // fall-through
        case GRANT_CREATE_RESOURCE: // fall-through
        case GRANT_CREATE_ENTITY: // fall-through
            return userPrivilegeDao.exists(spa.getUser(), Privilege.GRANT_GRANTS);

        // otherwise, cannot grant
        default:
            return false;
        }
    }

    boolean canDump(SessionPermissionAssistant spa) {
        return userPrivilegeDao.exists(spa.getUser(), Privilege.DUMP_ALL);
    }

    public boolean canUseSession(SessionPermissionAssistant spa) {
        return userPrivilegeDao.exists(spa.getUser(), Privilege.USE_SESSION);
    }

    public boolean canQueryStreams(SessionPermissionAssistant spa) {
        return userPrivilegeDao.exists(spa.getUser(), Privilege.QUERY_STREAMS);
    }

    boolean hasUserAdminPrivilege(SessionPermissionAssistant spa) {
        if (userPrivilegeDao.exists(spa.getUser(), Privilege.USER_ADMIN)) {
            // require connection from IP whitelist
            if (!spa.isPrivilegedConnection()) {
                log.info("admin operation refused - (connection from non-privileged address)");
                throw new DataException("privileged connection required", Status.UNAUTHORIZED);
            }
            return true;
        } else {
            return false;
        }
    }

    void grantPrivileges(SessionPermissionAssistant spa, User recipient, List<Privilege> privileges) {
        for (Privilege privilege : privileges) {
            if (!canGrant(spa, privilege)) {
                throw new ProgramDefectException("grant not checked");
            }
            userPrivilegeDao.create(recipient, privilege);
        }
    }

    List<Privilege> getPrivileges(User user) {
        return userPrivilegeDao.get(user);
    }

    private void checkIsCreatePrivilege(Privilege privilege) {
        switch (privilege) {
        case CREATE_USER:
        case CREATE_GROUP:
        case CREATE_DEVICE:
        case CREATE_DATASTREAM:
        case CREATE_TRANSFORM:
        case CREATE_TRANSFORMFUNCTION:
        case CREATE_ENTITY:
        case CREATE_RESOURCE:
            return;
        default:
            throw new ProgramDefectException("Privilege " + privilege + " is not a type of create");
        }
    }

    void adminAddPrivilege(User recipient, Privilege privilege) {
        userPrivilegeDao.create(recipient, privilege);
    }

    void adminRemovePrivilege(User recipient, Privilege privilege) {
        userPrivilegeDao.remove(recipient, privilege);
    }

}
