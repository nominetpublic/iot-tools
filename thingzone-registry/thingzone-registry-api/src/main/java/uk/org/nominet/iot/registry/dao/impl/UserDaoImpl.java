/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.AbstractJpaDao;
import uk.org.nominet.iot.registry.dao.UserDao;
import uk.org.nominet.iot.registry.model.User;

class UserDaoImpl extends AbstractJpaDao<User> implements UserDao {
    private final static Logger log = LoggerFactory.getLogger(UserDaoImpl.class);

    UserDaoImpl() {
        super(User.class);
    }

    @Override
    public User create() {
        User user = new User("{}", "{}");
        persist(user);
        flush(); // get the Id populated
        log.info("created user with ID={}", user.getId());
        return user;
    }

    @Override
    public User getUserByUsername(String username) {
        checkTransaction();
        if (Strings.isNullOrEmpty(username))
            throw new DataException("No username specified");

        TypedQuery<User> query = getEntityManager().createNamedQuery("User.findByUsername", getEntityClass());
        query.setParameter("username", username);
        return query.getSingleResult();
    }
}
