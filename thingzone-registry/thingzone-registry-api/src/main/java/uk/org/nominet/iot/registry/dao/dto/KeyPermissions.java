/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.dto;

import java.util.Map;
import java.util.Set;

import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.Permission;

public class KeyPermissions {

    private DNSkey key;
    private Map<String, Set<Permission>> userPermissions;
    private Map<String, Set<Permission>> groupPermissions;

    public KeyPermissions(DNSkey key,
                          Map<String, Set<Permission>> userPermissions,
                          Map<String, Set<Permission>> groupPermissions) {
        this.key = key;
        this.userPermissions = userPermissions;
        this.groupPermissions = groupPermissions;
    }

    public DNSkey getKey() {
        return key;
    }

    public Map<String, Set<Permission>> getUserPermissions() {
        return userPermissions;
    }

    public Map<String, Set<Permission>> getGroupPermissions() {
        return groupPermissions;
    }

    @Override
    public String toString() {
        return "KeyPermissions [key=" + key + ", userPermissions=" + userPermissions
            + ", groupPermissions=" + groupPermissions + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((groupPermissions == null) ? 0 : groupPermissions.hashCode());
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        result = prime * result + ((userPermissions == null) ? 0 : userPermissions.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        KeyPermissions other = (KeyPermissions) obj;
        if (groupPermissions == null) {
            if (other.groupPermissions != null)
                return false;
        } else if (!groupPermissions.equals(other.groupPermissions))
            return false;
        if (key == null) {
            if (other.key != null)
                return false;
        } else if (!key.equals(other.key))
            return false;
        if (userPermissions == null) {
            if (other.userPermissions != null)
                return false;
        } else if (!userPermissions.equals(other.userPermissions))
            return false;
        return true;
    }
}
