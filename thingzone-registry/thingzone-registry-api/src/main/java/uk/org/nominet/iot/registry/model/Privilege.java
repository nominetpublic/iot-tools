/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import java.sql.SQLException;

import javax.persistence.Converter;

import org.postgresql.util.PGobject;

import uk.nominet.iot.utils.error.ProgramDefectException;

public enum Privilege implements PersistableEnum<PGobject> {
    // user creation
    CREATE_USER,
    CREATE_GROUP,

    // object creation
    CREATE_DEVICE,
    CREATE_DATASTREAM,
    CREATE_TRANSFORM,
    CREATE_TRANSFORMFUNCTION,
    CREATE_ENTITY,
    CREATE_RESOURCE,
    CREATE_DETECTOR,
    RUN_DETECTOR,

    // privileges that you can grant to a new user
    GRANT_CREATE_USER,
    GRANT_CREATE_GROUP,
    GRANT_CREATE_DEVICE,
    GRANT_CREATE_DATASTREAM,
    GRANT_CREATE_TRANSFORM,
    GRANT_CREATE_TRANSFORMFUNCTION,
    GRANT_CREATE_ENTITY,
    GRANT_CREATE_RESOURCE,
    GRANT_CREATE_DETECTOR,
    GRANT_RUN_DETECTOR,

    GRANT_GRANTS, // new user can be given grants

    // privileges that can only be granted by USER_ADMIN
    DUMP_ALL, // can dump the entire set of (devices/datastreams/transforms)
    USE_SESSION, // using the context of another users session, e.g. get the username, or the set
                    // of permissions on an object
    QUERY_STREAMS, // can read all stream metadata and transforms (i.e. implement the Bridge)

    // "superuser"
    USER_ADMIN, // can create users with any of the above privileges
    ;

    public static boolean isValid(String s) {
        try {
            Privilege.valueOf(s);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    public static Privilege fromString(String s) {
        try {
            return Privilege.valueOf(s);
        } catch (IllegalArgumentException e) {
            throw new ProgramDefectException("invalid User Privilege: \'" + s + "\'");
        }
    }

    @Override
    public PGobject getValue() {
        PGobject object = new PGobject();
        object.setType("privilege_type");
        try {
            object.setValue(this.toString());
        } catch (SQLException e) {
            System.out.println("this will fail!");
            throw new IllegalArgumentException("Error when creating PostgreSQL enum", e);
        }
        return object;
    }

    @Converter
    public static class PrivilegeConverter extends AbstractEnumConverter<Privilege, PGobject> {
        public PrivilegeConverter() {
            super(Privilege.class);
        }
    }
}
