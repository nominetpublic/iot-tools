/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import uk.org.nominet.iot.registry.common.RegistryUtilities;
import uk.org.nominet.iot.registry.common.RegistryUtilities.Detail;
import uk.org.nominet.iot.registry.database.DatabaseTransaction;
import uk.org.nominet.iot.registry.database.SimpleDatabaseOperation;
import uk.org.nominet.iot.registry.model.PullSubscription;
import uk.org.nominet.iot.registry.service.DataStreamService;
import uk.org.nominet.iot.registry.service.PullSubscriptionService;
import uk.org.nominet.iot.registry.service.dto.PullSubscriptionDetailDto;
import uk.org.nominet.iot.registry.service.dto.PullSubscriptionSummaryDto;
import uk.org.nominet.iot.registry.webserver.api.support.JsonErrorResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSingleResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSuccessResult;
import uk.org.nominet.iot.registry.webserver.api.support.PersistenceEffect;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;
import uk.org.nominet.iot.registry.webserver.api.support.SessionWrapper;

/**
 * API handler for pull subscriptions.
 */
@Path("/pull_subscription")
public class PullSubscriptionHandler {
    private final static Logger log = LoggerFactory.getLogger(PullSubscriptionHandler.class);

    @Context
    Request request;

    @Context
    private javax.servlet.http.HttpServletRequest hsr;

    PullSubscriptionService psService;
    DataStreamService dataStreamService;

    /**
     * Instantiates a new pull subscription handler.
     */
    public PullSubscriptionHandler() {
        psService = new PullSubscriptionService();
        dataStreamService = new DataStreamService();
    }

    /**
     * List all pull subscriptions.
     *
     * @param detailOption the detail option
     * @return the pull subscriptions
     */
    @GET
    @Path("list")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object list(final @QueryParam("detail") String detailOption) {
        final Detail detail = RegistryUtilities.DETAIL_FULL.equals(detailOption) ? Detail.FULL : Detail.SIMPLE;

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[pull_subscription/list]");

                List<PullSubscription> subs = psService.getVisible(spa);

                if (detail == Detail.SIMPLE) {
                    List<PullSubscriptionSummaryDto> summaries
                        = subs.stream().map(ps -> new PullSubscriptionSummaryDto(ps)).collect(Collectors.toList());
                    return new JsonSingleResult("pullSubscriptions", summaries).output();
                } else {
                    List<PullSubscriptionDetailDto> details
                        = subs.stream().map(ps -> new PullSubscriptionDetailDto(ps)).collect(Collectors.toList());
                    return new JsonSingleResult("pullSubscriptions", details).output();
                }
            }
        });
    }

    /**
     * Create or update a pull subscription on the datastream.
     *
     * @param dataStreamKey the data stream key
     * @param uri the uri
     * @param uriParams the uri params
     * @param interval the time in seconds between pull requests
     * @param aligned the aligned
     * @return success or failure
     */
    @PUT
    @Path("set")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object set(final @FormDataParam("dataStream") String dataStreamKey,
                      final @FormDataParam("uri") String uri,
                      final @FormDataParam("uriParams") String uriParams,
                      final @FormDataParam("interval") Integer interval,
                      final @FormDataParam("aligned") Integer aligned) {
        if (Strings.isNullOrEmpty(dataStreamKey))
            return new JsonErrorResult("missing form part \"dataStream\"").asResponse(Status.BAD_REQUEST);
        if (Strings.isNullOrEmpty(uri))
            return new JsonErrorResult("missing form part \"uri\"").asResponse(Status.BAD_REQUEST);
        if (Strings.isNullOrEmpty(uriParams))
            return new JsonErrorResult("missing form part \"uriParams\"").asResponse(Status.BAD_REQUEST);
        if (interval == null)
            return new JsonErrorResult("missing form part \"interval\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[pull_subscription/set] dataStream={} uri={} uriParams={} interval={} aligned={}",
                    dataStreamKey, uri, uriParams, interval, aligned);

                psService.set(spa, dataStreamKey, uri, uriParams, interval, aligned);
                return new JsonSuccessResult();
            }
        });
    }

    /**
     * Gets the pull subscription on the given datastream
     *
     * @param dataStreamKey the data stream key
     * @param detailOption the detail option
     * @return the pull subscription
     */
    @GET
    @Path("get")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object get(final @QueryParam("dataStream") String dataStreamKey,
                      final @QueryParam("detail") String detailOption) {
        if (Strings.isNullOrEmpty(dataStreamKey))
            return new JsonErrorResult("missing form part \"dataStream\"").asResponse(Status.BAD_REQUEST);
        final Detail detail = RegistryUtilities.DETAIL_FULL.equals(detailOption) ? Detail.FULL : Detail.SIMPLE;

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[pull_subscription/get] dataStream={}", dataStreamKey);

                Optional<PullSubscription> ps = psService.get(spa, dataStreamKey);

                if (!ps.isPresent()) {
                    return new JsonSuccessResult();
                }

                if (detail == Detail.SIMPLE) {
                    return new JsonSingleResult("pullSubscription", new PullSubscriptionSummaryDto(ps.get())).output();
                } else {
                    return new JsonSingleResult("pullSubscription", new PullSubscriptionDetailDto(ps.get())).output();
                }
            }
        });
    }

    /**
     * Delete the pull subscription on the given datastream
     *
     * @param dataStreamKey the data stream key
     * @return success or failure
     */
    @PUT
    @Path("delete")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object delete(final @FormParam("dataStream") String dataStreamKey) {
        if (Strings.isNullOrEmpty(dataStreamKey))
            return new JsonErrorResult("missing form part \"dataStream\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[pull_subscription/delete] dataStream={}", dataStreamKey);

                psService.delete(spa, dataStreamKey);

                return new JsonSuccessResult();
            }
        });
    }

}
