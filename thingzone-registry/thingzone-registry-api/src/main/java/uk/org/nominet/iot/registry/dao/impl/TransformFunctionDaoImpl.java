/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.AbstractJpaDao;
import uk.org.nominet.iot.registry.dao.TransformFunctionDao;
import uk.org.nominet.iot.registry.model.TransformFunction;
import uk.org.nominet.iot.registry.model.User;

public class TransformFunctionDaoImpl extends AbstractJpaDao<TransformFunction> implements TransformFunctionDao {
    private final static Logger log = LoggerFactory.getLogger(TransformFunctionDaoImpl.class);

    TransformFunctionDaoImpl() {
        super(TransformFunction.class);
    }

    @Override
    public TransformFunction create(String name, User owner, String functionType, String functionContent) {
        if (exists(name)) {
            if (owner == null) {
                throw new DataException("System TransformFunction \"" + name + "\" exists already", Status.CONFLICT);
            } else {
                throw new DataException("TransformFunction \"" + name + "\" exists already for user", Status.CONFLICT);
            }
        }

        TransformFunction tf = new TransformFunction(name, owner, functionType, functionContent);
        persist(tf);
        flush(); // get the Id populated
        log.info("created TransformFunction for name={}, owner={}, functionType={}, functionContent={}", name, owner,
            functionType, functionContent);
        return tf;
    }

    @Override
    public boolean exists(String name) {
        try {
            findByName(name);
            return true;
        } catch (PersistenceException e) {
            if (e instanceof NoResultException) {
                return false;
            } else {
                throw e;
            }
        }
    }

    void delete(TransformFunction transformFunction) {
        deleteEntity(transformFunction);
    }

    @Override
    public void delete(String name) {
        delete(findByName(name));
    }

    @Override
    public void updateName(String name, String updatedName) {
        TransformFunction tf = findByName(name);
        tf.setName(updatedName);
    }

    @Override
    public void updateFunction(String name, String functionType, String functionContent) {
        TransformFunction tf = findByName(name);
        tf.setFunctionType(functionType);
        tf.setFunctionContent(functionContent);
    }

    @Override
    public TransformFunction getByName(String name) {
        return findByName(name);
    }

    private TransformFunction findByName(String name) {
        return loadByMatch("name", name);
    }
}
