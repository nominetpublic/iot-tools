/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.NoResultException;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

import uk.org.nominet.iot.registry.common.AuthException;
import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.ResourceDao;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.EntityStatus;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.model.Privilege;
import uk.org.nominet.iot.registry.model.Resource;
import uk.org.nominet.iot.registry.service.dto.ResourceDetailDto;
import uk.org.nominet.iot.registry.service.dto.ResourceSummaryDto;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;

public class ResourceService {

    private final static Logger log = LoggerFactory.getLogger(EntityService.class);

    private final ResourceDao resourceDao;
    private final ACLService aclService;
    private final UserPrivilegeService userPrivilegeService;
    private final EntityService entityService;
    private final DNSkeyService dnsKeyService;

    private final Privilege PRIVILEGE_REQUIRED_FOR_CREATE = Privilege.CREATE_RESOURCE;

    public ResourceService() {
        DaoFactory df = DaoFactoryManager.getFactory();
        resourceDao = df.resourceDao();
        aclService = new ACLService();
        userPrivilegeService = new UserPrivilegeService();
        entityService = new EntityService();
        dnsKeyService = new DNSkeyService();
    }

    /**
     * Create a resource using an existing key
     *
     * @param spa the session
     * @param key the entity key
     * @param resourceName the name of the resource
     * @param resourceType the type of resource
     * @param contentType the content type of the resource
     * @param content the resource content
     * @return the newly created resource
     */
    private Resource create(SessionPermissionAssistant spa,
                            String key,
                            String resourceName,
                            String resourceType,
                            String contentType,
                            byte[] content) {
        if (!userPrivilegeService.canCreate(spa, PRIVILEGE_REQUIRED_FOR_CREATE)) {
            throw new AuthException(spa, "need privilege " + PRIVILEGE_REQUIRED_FOR_CREATE);
        }

        return resourceDao.createKnownKey(key, resourceName, resourceType, contentType, content);
    }

    /**
     * Update or create an entity
     *
     * @param spa the session
     * @param key the entity key to lookup - if null or empty then a new entity will be created
     * @param name the name for a created entity
     * @param type the type for a created entity
     * @param resourceName the name of the resource
     * @param resourceType the type of resource
     * @param contentType the content type of the resource
     * @param content the content of the resource
     * @return the new resource
     */
    public Resource upsert(SessionPermissionAssistant spa,
                           String key,
                           String name,
                           String type,
                           String resourceName,
                           String resourceType,
                           String contentType,
                           byte[] content) {
        Resource resource;
        // check for existing resource to update
        try {
            resource = resourceDao.find(key, resourceName);
            log.info("found Resource {}", resource);

            // check permission
            DNSkey dnsKey = resource.getKey();
            if (dnsKey.getStatus() == EntityStatus.DELETED) {
                throw new DataException("Key not found", Status.NOT_FOUND);
            }
            if (aclService.isAllowed(spa, dnsKey, Permission.MODIFY)) {
                // update the entity name if set
                if (!Strings.isNullOrEmpty(name)) {
                    entityService.setValues(dnsKey, null, name);
                }
                if (!Strings.isNullOrEmpty(resourceType)) {
                    resource.setType(resourceType);
                }
                resource.setContentType(contentType);
                resource.setContent(content);
                return resource;
            } else {
                throw new AuthException(spa, dnsKey, "need MODIFY permission");
            }
        } catch (NoResultException e) {
            log.info("no resource found for key={}, name={}", key, resourceName);
        }

        if (Strings.isNullOrEmpty(key)) {
            // if the key isn't specified then create a new entity and add the resource
            DNSkey entity = entityService.createKey(spa, false, null);
            entityService.setValues(entity, type, name);
            return this.create(spa, entity.getKey(), resourceName, resourceType, contentType, content);

        } else if (entityService.isAlive(key)) {
            DNSkey dnsKey = dnsKeyService.getDnsKey(spa, key);
            if (!aclService.isAllowed(spa, dnsKey, Permission.MODIFY)) {
                throw new AuthException(spa, dnsKey, "need MODIFY permission on an entity to add a resource");
            }
            // update the entity name if set
            if (!Strings.isNullOrEmpty(name)) {
                entityService.setValues(dnsKey, null, name);
            }
            // otherwise add the resource to an existing entity
            return this.create(spa, key, resourceName, resourceType, contentType, content);
        } else {
            throw new DataException("Key not found", Status.NOT_FOUND);
        }
    }

    public boolean checkJson(byte[] bytes) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            mapper.readTree(bytes);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    /**
     * Get the named resource on the key
     *
     * @param spa the session
     * @param key the entity key
     * @param resourceName the resource name
     */
    public Resource getResource(SessionPermissionAssistant spa, String key, String resourceName) {
        Resource resource;
        try {
            resource = resourceDao.find(key, resourceName);
            log.info("found Resource {}", resource);
        } catch (NoResultException e) {
            throw new DataException("Resource not found: " + key, Status.NOT_FOUND);
        }
        // check permission
        if (aclService.isAllowed(spa, resource.getKey(), Permission.DISCOVER)) {
            return resource;
        } else {
            throw new AuthException(spa, resource.getKey(), "need DISCOVER permission");
        }
    }

    /**
     * List all resources on keys visible to the user
     */
    public List<ResourceSummaryDto> listAll(SessionPermissionAssistant spa, String resourceType) {
        Stream<Resource> resources = resourceDao.getAllowed(spa.getRelevantAccounts(), Permission.DISCOVER).stream();
        if (!Strings.isNullOrEmpty(resourceType)) {
            resources = resources.filter(resource -> resourceType.equals(resource.getType()));
        }
        return resources.map(e -> new ResourceSummaryDto(e)).collect(Collectors.toList());
    }

    /**
     * List resources on keys visible to the user, filtered by type
     */
    public List<ResourceSummaryDto> listByType(SessionPermissionAssistant spa, String type, String resourceType) {
        Stream<Resource> resources
            = resourceDao.listByType(spa.getRelevantAccounts(), ImmutableList.of(Permission.DISCOVER), type, null)
                         .stream();
        if (!Strings.isNullOrEmpty(resourceType)) {
            resources = resources.filter(resource -> resourceType.equals(resource.getType()));
        }
        return resources.map(e -> new ResourceSummaryDto(e)).collect(Collectors.toList());
    }

    /**
     * List all resources on a single key that is visible to the user
     */
    public List<ResourceSummaryDto> listByKey(SessionPermissionAssistant spa, String key, String resourceType) {
        DNSkey dnsKey = dnsKeyService.getDnsKey(spa, key);

        Stream<Resource> resources
            = resourceDao.listByKey(dnsKey.getKey(), spa.getRelevantAccounts(), Permission.DISCOVER).stream();
        if (!Strings.isNullOrEmpty(resourceType)) {
            resources = resources.filter(resource -> resourceType.equals(resource.getType()));
        }
        return resources.map(e -> new ResourceSummaryDto(e)).collect(Collectors.toList());
    }

    /**
     * Remove a resource from an entity
     *
     * @param spa the session
     * @param key the entity key to remove the resource from
     * @param resourceName the name of the resource to remove
     */
    public void remove(SessionPermissionAssistant spa, String key, String resourceName) {
        Resource resource;
        try {
            resource = resourceDao.find(key, resourceName);
            log.info("found Resource {}", resource);

            // check permission
            if (aclService.isAllowed(spa, resource.getKey(), ImmutableList.of(Permission.MODIFY))) {
                resourceDao.delete(resource);
            } else {
                throw new AuthException(spa, resource.getKey(),
                                        "need MODIFY permission to remove a resource from an entity");
            }
        } catch (NoResultException e) {
            log.info("no resource found on key={}, resource name={}", key, resourceName);
        }
    }

    public List<ResourceDetailDto> getJsonByType(SessionPermissionAssistant spa, String type, String resourceType) {
        Stream<Resource> resources = resourceDao.listByType(spa.getRelevantAccounts(),
                                                            ImmutableList.of(Permission.DISCOVER),
                                                            type,
                                                            "application/json")
                                                .stream();
        if (!Strings.isNullOrEmpty(resourceType)) {
            resources = resources.filter(resource -> resourceType.equals(resource.getType()));
        }
        return resources.map(e -> {
            try {
                return new ResourceDetailDto(e);
            } catch (UnsupportedEncodingException e1) {
                throw new DataException("Resource found with invalid JSON: " + e.getKey().getKey(),
                                        Status.INTERNAL_SERVER_ERROR);
            }
        }).collect(Collectors.toList());
    }
}
