/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao;

import java.util.List;

import uk.org.nominet.iot.registry.database.DbObjectStreamer;
import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.Permission;

public interface DNSkeyDao extends JpaDao<DNSkey> {
    String generateKey();

    DNSkey create();

    DNSkey createKnownKey(String key);

    DNSkey getByKey(String key);

    void delete(DNSkey dnsKey); // no throw

    boolean exists(String key); // no throw

    boolean isAlive(String key);

    List<DNSkey> getAllowed(List<Account> accounts, List<Permission> perms, int size, int page);

    List<DNSkey> getAllowed(List<Account> accounts, Permission permission, int size, int page);

    DbObjectStreamer<DNSkey> streamAll();

    List<DNSkey> getAll();

    List<DNSkey> getByType(List<Account> accounts, String type, int size, int page);

    List<Object[]> listEntitiesRelated(List<Account> accounts,
                                       List<Permission> permissions,
                                       String type,
                                       String name,
                                       int size, int page);

    List<Object[]> listEntitiesRelatedWithPermissions(List<Account> accounts,
                                                      List<Permission> permissions,
                                                      String type,
                                                      String name,
                                                      Account account,
                                                      int size,
                                                      int page);
}
