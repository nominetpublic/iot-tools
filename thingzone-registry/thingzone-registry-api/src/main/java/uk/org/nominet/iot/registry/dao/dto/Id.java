/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.dto;

public class Id implements Comparable<Id> {
    private long id;

    public Id(long aclId) {
        this.id = aclId;
    }

    public long getId() {
        return id;
    }

    @Override
    public int compareTo(Id rhs) {
        if (this == rhs)
            return 0;
        return Long.valueOf(id).compareTo(rhs.getId());
    }

    @Override
    public int hashCode() {
        return 31 * (17 + (int) (id ^ (id >>> 32)));
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Id) {
            Id rhs = (Id) o;
            return id == rhs.id;
        }
        return false;
    }
}
