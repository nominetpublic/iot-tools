/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.upsert;

import java.io.IOException;
import java.util.HashMap;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.model.PullSubscription;
import uk.org.nominet.iot.registry.service.PullSubscriptionService;
import uk.org.nominet.iot.registry.service.dto.PullSubscriptionSummaryDto;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;
import uk.nominet.iot.utils.error.ProgramDefectException;

/**
 * PullSubscriptionUpsertMetaService is a meta-service to help with upserts It helps upsert Pull Subscriptions, so is
 * used for DataStream (but not Transform) upsert
 */
public class PullSubscriptionUpsertMetaService {

    private final PullSubscriptionService pullSubscriptionService = new PullSubscriptionService();

    public void upsert(SessionPermissionAssistant spa, String key, String pullSubscriptionAsJsonString) {
        Optional<PullSubscriptionSummaryDto> requestedPullSubscription
            = decodeFromJson(pullSubscriptionAsJsonString, key);
        Optional<PullSubscription> existingPullSubscription = pullSubscriptionService.get(spa, key);

        if (requestedPullSubscription.isPresent()) {
            pullSubscriptionService.set(spa, requestedPullSubscription.get());
        } else {
            // we probably got a blank object
            if (existingPullSubscription.isPresent()) {
                pullSubscriptionService.delete(spa, key);
            }
        }

    }

    /**
     * we need to provide something appropriate for Jackson to deserialise into and it won't deserialise object tree
     * into @JsonRawValue so we need to deserialise into a map, & then pack that back into a string
     */
    public static class ParamsDeserialiser {
        public String uri;
        public HashMap<String, Object> uriParams;
        public Integer interval;
        public Integer aligned;

        public ParamsDeserialiser() {}
    }

    static Optional<PullSubscriptionSummaryDto> decodeFromJson(String dnsJson, String parentKey) {
        try {
            ParamsDeserialiser decoded = new ObjectMapper().readValue(dnsJson, ParamsDeserialiser.class);

            // it's hard to be sure when we're using this kind of deserialisation, but ...
            // we'll infer an empty object if there's no "uri"
            if (decoded.uri == null) {
                return Optional.empty();
            } else if (decoded.uriParams == null) {
                throw new DataException("Pull subscription missing 'uriParams'");
            } else if (decoded.interval == null) {
                throw new DataException("Pull subscription missing 'interval'");
            }

            return Optional.of(new PullSubscriptionSummaryDto(parentKey,
                                                              decoded.uri,
                                                              new ObjectMapper().writeValueAsString(decoded.uriParams),
                                                              decoded.interval,
                                                              decoded.aligned));
        } catch (IOException | ProgramDefectException e) {
            throw new DataException("bad JSON: " + e.getMessage());
        }
    }

}
