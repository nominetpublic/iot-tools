/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.eclipse.persistence.annotations.Customizer;

import uk.org.nominet.iot.registry.database.ColumnPosition;
import uk.org.nominet.iot.registry.database.ColumnPositionCustomizer;
import uk.org.nominet.iot.registry.database.DatabaseDriver;

/**
 * Transform Sources
 */
@Entity
@Table(name = "TransformSources", schema = DatabaseDriver.SCHEMA_NAME,
       uniqueConstraints = { @UniqueConstraint(name = "UQ_TransformSources", columnNames = { "transformId, alias" }), })
@Customizer(ColumnPositionCustomizer.class)
public class TransformSource {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ColumnPosition(position = 1)
    private Long id;

    @JoinColumn(name = "transformId", referencedColumnName = "id", nullable = false)
    @ColumnPosition(position = 2)
    private Transform transform;

    @JoinColumn(name = "sourceStream", referencedColumnName = "DNSkey", nullable = false)
    @ColumnPosition(position = 3)
    private DataStream sourceStream;

    @Column(name = "trigger", nullable = false)
    @ColumnPosition(position = 5)
    private Boolean trigger;

    @Column(name = "alias", nullable = false)
    @ColumnPosition(position = 6)
    private String alias;

    @Column(name = "aggregationType", nullable = true)
    @ColumnPosition(position = 7)
    private String aggregationType;

    @Column(name = "aggregationSpec", nullable = true, columnDefinition = "character varying(1023)")
    @ColumnPosition(position = 8)
    private String aggregationSpec;

    TransformSource() {}

    public TransformSource(Transform transform,
                           DataStream sourceStream,
                           String alias,
                           Boolean trigger,
                           String aggregationType,
                           String aggregationSpec) {
        this.transform = transform;
        this.sourceStream = sourceStream;
        this.alias = alias;
        this.trigger = trigger;
        this.aggregationType = aggregationType;
        this.aggregationSpec = aggregationSpec;
    }

    @Override
    public String toString() {
        return "TransformSource{" + "id=" + id + ", transform=" + transform.getDataStream().getKey().getKey()
            + ", sourceStream=" + sourceStream.getKey().getKey() + ", trigger=" + trigger + ", alias='" + alias + '\''
            + ", aggregationType=" + aggregationType + ", aggregationSpec=" + aggregationSpec + '}';
    }

    public Long getId() {
        return id;
    }

    public Transform getTransform() {
        return transform;
    }

    public DataStream getSourceStream() {
        return sourceStream;
    }

    public void setSourceStream(DataStream sourceStream) {
        this.sourceStream = sourceStream;
    }

    public String getAlias() {
        return alias;
    }

    public Boolean getTrigger() {
        return trigger;
    }

    public void setTrigger(Boolean trigger) {
        this.trigger = trigger;
    }

    public String getAggregationType() {
        return aggregationType;
    }

    public void setAggregationType(String aggregationType) {
        this.aggregationType = aggregationType;
    }

    public String getAggregationSpec() {
        return aggregationSpec;
    }

    public void setAggregationSpec(String aggregationSpec) {
        this.aggregationSpec = aggregationSpec;
    }
}
