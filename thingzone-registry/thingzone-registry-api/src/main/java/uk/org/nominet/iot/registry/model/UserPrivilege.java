/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.eclipse.persistence.annotations.Customizer;
import uk.org.nominet.iot.registry.database.ColumnPosition;
import uk.org.nominet.iot.registry.database.ColumnPositionCustomizer;
import uk.org.nominet.iot.registry.database.DatabaseDriver;

import javax.persistence.*;

@Entity
@Table(name = "UserPrivileges", schema = DatabaseDriver.SCHEMA_NAME,
       uniqueConstraints = { @UniqueConstraint(name = "USERPRIVS_UQ", columnNames = { "user_id", "privilege" }) })
@Customizer(ColumnPositionCustomizer.class)
public class UserPrivilege {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ColumnPosition(position = 1)
    @JsonIgnore
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    @ColumnPosition(position = 4)
    private User user;

    @Column(name = "privilege", columnDefinition = DatabaseDriver.SCHEMA_NAME + ".PRIVILEGE_TYPE", nullable = false)
    @Convert(converter = Privilege.PrivilegeConverter.class)
    @ColumnPosition(position = 4)
    private Privilege privilege;

    UserPrivilege() {}

    public UserPrivilege(User user, Privilege privilege) {
        this.user = user;
        this.privilege = privilege;
    }

    public Long getId() {
        return id;
    }

    public Privilege getPrivilege() {
        return privilege;
    }

    @Override
    public String toString() {
        return "Privilege {" + "id=" + id + ", userId='" + user.getId() + '"' + ", privilege='" + privilege + '"' + '}';
    }
}
