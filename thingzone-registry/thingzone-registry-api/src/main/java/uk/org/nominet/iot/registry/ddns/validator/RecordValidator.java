/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.ddns.validator;

import com.google.common.base.Strings;
import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.common.UserKeyValidator;
import uk.org.nominet.iot.registry.service.dto.DNSdataDto;

public class RecordValidator {

    // throws DataException on error
    public static void validate(String rrtype, String rdata) {
        getValidator(rrtype).validate(rdata);
    }

    public static void validate(DNSdataDto ddr) {
        validateSubdomain(ddr.getSubdomain());
        validate(ddr.getRrtype(), ddr.getRdata());
    }

    static void validateSubdomain(String subdomain) {
        if (Strings.isNullOrEmpty(subdomain)) {
            return;
        }

        if (!UserKeyValidator.validate(subdomain)) {
            throw new DataException("invalid DNS subdomain \"" + subdomain + "\"");
        }
    }

    static AbstractRecordValidator getValidator(String rrtype) {
        switch (rrtype) {
        case "A":
            return new ARecordValidator();
        case "AAAA":
            return new AAAARecordValidator();
        case "CNAME":
            return new CNAMERecordValidator();
        case "URI":
            return new URIRecordValidator();
        default:
            throw new DataException("unhandled RRTYPE");
        }
    }

    public static int getType(String rrtype) {
        return getValidator(rrtype).expectedType();
    }

}
