/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import java.sql.SQLException;
import javax.persistence.Converter;
import org.postgresql.util.PGobject;
import uk.nominet.iot.utils.error.ProgramDefectException;

public enum Permission implements PersistableEnum<PGobject> {
    ADMIN, MODIFY, UPLOAD, CONSUME, ANNOTATE, DISCOVER, UPLOAD_EVENTS, CONSUME_EVENTS, VIEW_LOCATION, VIEW_METADATA;

    public static boolean isValid(String s) {
        try {
            Permission.valueOf(s);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    public static Permission fromString(String s) {
        try {
            return Permission.valueOf(s);
        } catch (IllegalArgumentException e) {
            throw new ProgramDefectException("invalid Permission: \'" + s + "\'");
        }
    }

    @Override
    public PGobject getValue() {
        PGobject object = new PGobject();
        object.setType("permission_type");
        try {
            object.setValue(this.toString());
        } catch (SQLException e) {
            System.out.println("this will fail!");
            throw new IllegalArgumentException("Error when creating PostgreSQL enum", e);
        }
        return object;
    }

    @Converter
    public static class PermissionConverter extends AbstractEnumConverter<Permission, PGobject> {
        public PermissionConverter() {
            super(Permission.class);
        }
    }
}
