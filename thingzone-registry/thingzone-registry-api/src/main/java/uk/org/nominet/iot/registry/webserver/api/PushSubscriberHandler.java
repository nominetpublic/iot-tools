/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.common.RegistryUtilities;
import uk.org.nominet.iot.registry.common.RegistryUtilities.Detail;
import uk.org.nominet.iot.registry.database.DatabaseTransaction;
import uk.org.nominet.iot.registry.database.SimpleDatabaseOperation;
import uk.org.nominet.iot.registry.model.PushSubscriber;
import uk.org.nominet.iot.registry.service.DataStreamService;
import uk.org.nominet.iot.registry.service.PushSubscriberService;
import uk.org.nominet.iot.registry.service.dto.PushSubscriberDetailDto;
import uk.org.nominet.iot.registry.service.dto.PushSubscriberSummaryDto;
import uk.org.nominet.iot.registry.webserver.api.support.JsonErrorResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSingleResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSuccessResult;
import uk.org.nominet.iot.registry.webserver.api.support.PersistenceEffect;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;
import uk.org.nominet.iot.registry.webserver.api.support.SessionWrapper;

/**
 * The API handler for Push Subscribers.
 */
@Path("/push_subscribers")
public class PushSubscriberHandler {
    private final static Logger log = LoggerFactory.getLogger(PushSubscriberHandler.class);

    @Context
    Request request;

    @Context
    private javax.servlet.http.HttpServletRequest hsr;

    PushSubscriberService psService;
    DataStreamService dataStreamService;

    /**
     * Instantiates a new push subscriber handler.
     */
    public PushSubscriberHandler() {
        psService = new PushSubscriberService();
        dataStreamService = new DataStreamService();
    }

    /**
     * List all push subscribers.
     *
     * @param detailOption the detail option
     * @return the push subscribers
     */
    @GET
    @Path("list")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object list(final @QueryParam("detail") String detailOption) {
        final Detail detail = RegistryUtilities.DETAIL_FULL.equals(detailOption) ? Detail.FULL : Detail.SIMPLE;

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[push_subscribers/list]");
                List<PushSubscriber> subs = psService.getVisible(spa);
                if (detail == Detail.SIMPLE) {
                    return new JsonSingleResult("pushSubscribers", PushSubscriberSummaryDto.asList(subs)).output();
                } else {
                    return new JsonSingleResult("pushSubscribers", PushSubscriberDetailDto.asList(subs)).output();
                }
            }
        });
    }

    /**
     * Create or update a push subscriber for the specified data stream key
     *
     * @param dataStreamKey the data stream key
     * @param uri the uri
     * @param uriParams the uri params
     * @param headers the headers
     * @param method the method
     * @param retrySequence the retry sequence
     * @return success or failure
     */
    @PUT
    @Path("set")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object set(final @FormDataParam("dataStream") String dataStreamKey,
                      final @FormDataParam("uri") String uri,
                      final @FormDataParam("uriParams") String uriParams,
                      final @FormDataParam("headers") String headers,
                      final @FormDataParam("method") String method,
                      final @FormDataParam("retrySequence") String retrySequence) {
        if (Strings.isNullOrEmpty(dataStreamKey))
            return new JsonErrorResult("missing form part \"dataStream\"").asResponse(Status.BAD_REQUEST);
        if (Strings.isNullOrEmpty(uri))
            return new JsonErrorResult("missing form part \"uri\"").asResponse(Status.BAD_REQUEST);
        if (Strings.isNullOrEmpty(uriParams))
            return new JsonErrorResult("missing form part \"uriParams\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info(
                    "[push_subscriber/set] dataStream={} uri={} uriParams={} headers={} method={} retrySequence={}",
                    dataStreamKey, uri, uriParams, headers, method, retrySequence);
                psService.set(spa, dataStreamKey, uri, uriParams, headers, method, retrySequence);
                return new JsonSuccessResult();
            }
        });
    }

    /**
     * Gets the push subscribers for the dataStream, can filter by URI
     *
     * @param dataStreamKey the data stream key
     * @param uriOption the uri
     * @param detailOption the detail option
     * @return the push subscribers
     */
    @GET
    @Path("get")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object get(final @QueryParam("dataStream") String dataStreamKey,
                      final @QueryParam("uri") String uriOption,
                      final @QueryParam("detail") String detailOption) {
        if (Strings.isNullOrEmpty(dataStreamKey))
            return new JsonErrorResult("missing form part \"dataStream\"").asResponse(Status.BAD_REQUEST);
        final Detail detail = RegistryUtilities.DETAIL_FULL.equals(detailOption) ? Detail.FULL : Detail.SIMPLE;

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[push_subscribers/get] dataStream={}", dataStreamKey);

                List<PushSubscriber> ps;
                if (Strings.isNullOrEmpty(uriOption)) {
                    // uri not specified; get all
                    ps = psService.getForStream(spa, dataStreamKey);
                } else {
                    // get the specified push subscription (or none)
                    ps = new ArrayList<>();
                    PushSubscriber p = psService.get(spa, dataStreamKey, uriOption);
                    if (p != null) {
                        ps.add(p);
                    }
                }
                if (ps == null) {
                    throw new DataException("Push subscriber not found", Status.NOT_FOUND);
                }
                if (detail == Detail.SIMPLE) {
                    return new JsonSingleResult("pushSubscribers", PushSubscriberSummaryDto.asList(ps)).output();
                } else {
                    return new JsonSingleResult("pushSubscribers", PushSubscriberDetailDto.asList(ps)).output();
                }
            }
        });
    }

    /**
     * Delete the push subscriber
     *
     * @param dataStreamKey the data stream key
     * @param uri the uri
     * @return success or failure
     */
    @PUT
    @Path("delete")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object delete(final @FormParam("dataStream") String dataStreamKey,
                         final @FormParam("uri") String uri) {
        if (Strings.isNullOrEmpty(dataStreamKey))
            return new JsonErrorResult("missing form param \"dataStream\"").asResponse(Status.BAD_REQUEST);
        if (Strings.isNullOrEmpty(uri))
            return new JsonErrorResult("missing form param \"uri\"").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[push_subscriber/delete] dataStream={}", dataStreamKey);

                psService.delete(spa, dataStreamKey, uri);
                return new JsonSuccessResult();
            }
        });
    }

}
