/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import java.util.Collections;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.AbstractJpaDao;
import uk.org.nominet.iot.registry.dao.DeviceDao;
import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.Device;
import uk.org.nominet.iot.registry.model.EntityStatus;
import uk.org.nominet.iot.registry.model.Permission;

class DeviceDaoImpl extends AbstractJpaDao<Device> implements DeviceDao {
    private final static Logger log = LoggerFactory.getLogger(DeviceDaoImpl.class);

    DeviceDaoImpl() {
        super(Device.class);
    }

    @Override
    public Device create() {
        return createKnownKey(new DNSkeyDaoImpl().generateKey());
    }

    @Override
    public Device createKnownKey(String key) {
        DNSkey dnskey = new DNSkeyDaoImpl().createKnownKey(key);
        dnskey.setStatus(EntityStatus.DEVICE);
        Device device = new Device();
        device.setKey(dnskey);
        device.setMetadata("{}"); // empty JSON
        create(device); // (will throw if already exists)
        return device;
    }

    Device create(Device device) {
        // see if exists already
        if (exists(device.getKey().getKey())) {
            throw new DataException("DNS key \"" + device.getKey() + "\" exists already", Status.CONFLICT);
        }

        persist(device);
        log.info("created device: {}", device);
        return device;
    }

    @Override
    public boolean exists(String key) {
        try {
            loadByKey(key);
            return true;
        } catch (PersistenceException e) {
            if (e instanceof NoResultException) {
                return false;
            } else {
                throw e;
            }
        }
    }

    @Override
    public Device getByName(String key) {
        return loadByKey(key);
    }

    private Device loadByKey(String key) {
        DNSkey dnsKey = new DNSkey(key);

        return loadByMatch("key", dnsKey);
    }

    @Override
    public void delete(Device device) {
        device.getKey().setStatus(EntityStatus.DELETED);
        deleteEntity(device);
    }

    @Override
    public List<Device> getAllowed(List<Account> accounts, List<Permission> perms) {
        return DaoPermissionRetrieval.forDevice().getAllowed(getEntityManager(), accounts, perms);
    }

    @Override
    public List<Device> getAllowed(List<Account> accounts, Permission permission) {
        return getAllowed(accounts, Collections.singletonList(permission));
    }

}
