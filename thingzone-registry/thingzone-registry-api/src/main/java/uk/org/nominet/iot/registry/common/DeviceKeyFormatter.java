/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DeviceKeyFormatter {
    public final static int RAW_LENGTH = 26;
    public final static int FORMATTED_LENGTH = 33;

    public static String format(String key) {
        String dnsFormatKey = "";
        for (int i = 0; i < 24; i += 4) {
            if (dnsFormatKey.length() > 0)
                dnsFormatKey += "-";
            dnsFormatKey += key.substring(i, i + 4);
        }
        dnsFormatKey += "." + key.substring(24, 25) + "." + key.substring(25, 26);
        return dnsFormatKey;
    }

    final static Pattern validKeyPattern = Pattern.compile(
        "^[a-z2-7]{4}-[a-z2-7]{4}-[a-z2-7]{4}-[a-z2-7]{4}-[a-z2-7]{4}-[a-z2-7]{4}\\.[a-z2-7]\\.[a-z2-7]$");

    public static boolean isValid(String key) {
        Matcher matcher = validKeyPattern.matcher(key);
        return matcher.matches();
    }

}
