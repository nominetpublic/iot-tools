/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import uk.org.nominet.iot.registry.common.RegistryUtilities;
import uk.org.nominet.iot.registry.common.RegistryUtilities.Detail;
import uk.org.nominet.iot.registry.database.DatabaseTransaction;
import uk.org.nominet.iot.registry.database.SimpleDatabaseOperation;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.Device;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.model.Transform;
import uk.org.nominet.iot.registry.service.DNSdataService;
import uk.org.nominet.iot.registry.service.DNSkeyService;
import uk.org.nominet.iot.registry.service.DataStreamService;
import uk.org.nominet.iot.registry.service.DeviceService;
import uk.org.nominet.iot.registry.service.PermissionService;
import uk.org.nominet.iot.registry.service.SearchMetaService;
import uk.org.nominet.iot.registry.service.SearchMetaService.SearchResult;
import uk.org.nominet.iot.registry.service.TransformFunctionService;
import uk.org.nominet.iot.registry.service.TransformService;
import uk.org.nominet.iot.registry.service.dto.DataStreamSummaryDto;
import uk.org.nominet.iot.registry.service.dto.DeviceSummaryDetailDto;
import uk.org.nominet.iot.registry.service.dto.DnsKeyDetailDto;
import uk.org.nominet.iot.registry.service.dto.TransformFunctionDto;
import uk.org.nominet.iot.registry.webserver.api.support.JsonErrorResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonFlattenedMapResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSingleResult;
import uk.org.nominet.iot.registry.webserver.api.support.PersistenceEffect;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;
import uk.org.nominet.iot.registry.webserver.api.support.SessionWrapper;

/**
 * API handler for search.
 */
@Path("/search")
public class SearchHandler {
    private final static Logger log = LoggerFactory.getLogger(SearchHandler.class);

    @Context
    Request request;

    @Context
    private javax.servlet.http.HttpServletRequest hsr;

    private final DeviceService deviceService;
    private final DataStreamService dataStreamService;
    private final DNSdataService dnsDataService;
    private final DNSkeyService dnsKeyService;
    private final TransformService transformService;
    private final TransformFunctionService transformFunctionService;
    private final SearchMetaService searchMetaService;

    /**
     * Instantiates a new search handler.
     */
    public SearchHandler() {
        deviceService = new DeviceService();
        dataStreamService = new DataStreamService();
        dnsDataService = new DNSdataService();
        dnsKeyService = new DNSkeyService();
        transformService = new TransformService();
        transformFunctionService = new TransformFunctionService();
        searchMetaService = new SearchMetaService();
    }


    /**
     * List all entities, devices, datastreams and transforms that are available based on the permission level
     *
     * @param permissionsList a list of comma separated permissions
     * @param detailOption the detail option
     * @param permissionsOption whether to display the permission info
     * @return the list of entities
     */
    @GET
    @Path("list")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object list(final @QueryParam("permissions") String permissionsList,
                       final @QueryParam("detail") String detailOption,
                       final @QueryParam("showPermissions") Boolean permissionsOption) {
        final boolean withDetail = RegistryUtilities.DETAIL_FULL.equals(detailOption) ? true : false;
        final boolean withPermissions = permissionsOption != null && permissionsOption.booleanValue() ? true : false;

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[search/list] (permissions={})", permissionsList);

                List<Permission> permissions = PermissionService.validatePermissions(permissionsList);
                if (permissions.size() == 0) {
                    permissions.add(Permission.DISCOVER); // default to DISCOVER if not set
                }

                // keep track of all the DNSkeys we see, so we can list
                // just the ones that haven't previously been referenced
                Set<DNSkey> dnsKeysReferenced = new HashSet<>();

                // generate list of device summaries
                List<DeviceSummaryDetailDto> deviceSummaries = new ArrayList<>();
                for (Device device : deviceService.getAllowed(spa, permissions)) {

                    // get child datastreams
                    List<DataStream> dataStreams = deviceService.getDataStreams(device);

                    // add device & its datastreams to list of DNSkeys already referenced
                    dnsKeysReferenced.add(device.getKey());
                    for (DataStream dataStream : dataStreams) {
                        dnsKeysReferenced.add(dataStream.getKey());
                    }

                    deviceSummaries.add(deviceService.getDeviceSummary(spa, device));
                }

                // generate list of DataStream & Transform summaries
                List<DataStreamSummaryDto> dataStreamSummaries = new ArrayList<>();
                List<Object> transformSummaries = new ArrayList<>();
                for (DataStream dataStream : dataStreamService.getAllowed(spa, permissions)) {

                    // add datastream to list of DNSkeys already referenced
                    dnsKeysReferenced.add(dataStream.getKey());

                    if (dataStream.getIsTransform()) {
                        // store as a Transform
                        Transform transform = transformService.getByOutputStream(dataStream);
                        if (withDetail) {
                            transformSummaries.add(transformService.getSummaryDetail(spa, transform, withPermissions));
                        } else {
                            transformSummaries.add(transformService.getSummary(spa, transform));
                        }
                    } else {
                        // store as a dataStream
                        if (withDetail) {
                            dataStreamSummaries.add(dataStreamService.getDataStreamSummaryDetail(spa, dataStream,
                                withPermissions, true, true));
                        } else {
                            dataStreamSummaries.add(dataStreamService.getDataStreamSummary(spa, dataStream));
                        }
                    }
                }

                // generate list of TransformFunction summaries
                List<TransformFunctionDto> transformFunctions = transformFunctionService.listFunctions(spa, Detail.SIMPLE);

                // find any DNSkeys that haven't already been referenced, and list them
                List<DnsKeyDetailDto> unattachedDnsKeyDetails = new ArrayList<>();
                for (DNSkey key : dnsKeyService.getAllowed(spa, permissions, 0, 1)) {
                    if (!dnsKeysReferenced.contains(key)) {
                        unattachedDnsKeyDetails.add(
                            DnsKeyDetailDto.withRecords(key, dnsDataService.dnsDataListByKey(spa, key)));
                    }
                }

                return new JsonFlattenedMapResult().put("devices", deviceSummaries)
                                                   .put("dataStreams", dataStreamSummaries)
                                                   .put("transforms", transformSummaries)
                                                   .put("transformFunctions", transformFunctions)
                                                   .put("keys", unattachedDnsKeyDetails).output();
            }
        });
    }

    /**
     * work out what kind of entity a specified DNSkey refers to, and return details.
     *
     * @param key the key
     * @param detailOption the detail option
     * @param permissionsOption the permissions option
     * @return the entity
     */
    @GET
    @Path("identify")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object identify(final @QueryParam("key") String key,
                           final @QueryParam("detail") String detailOption,
                           final @QueryParam("showPermissions") Boolean permissionsOption) {
        if (Strings.isNullOrEmpty(key))
            return new JsonErrorResult("missing query param \"key\"").asResponse(Status.BAD_REQUEST);
        final boolean withDetail = RegistryUtilities.DETAIL_FULL.equals(detailOption) ? true : false;
        final boolean withPermissions = permissionsOption != null && permissionsOption.booleanValue() ? true : false;

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("[search/identify] key={}", key);

                Optional<SearchResult> result = searchMetaService.identify(spa, key, withDetail, withPermissions);
                if (result.isPresent()) {
                    return new JsonSingleResult(result.get().description, result.get().value).output();
                } else {
                    return new JsonErrorResult("not recognised").asResponse(Status.NOT_FOUND);
                }
            }
        });
    }
}
