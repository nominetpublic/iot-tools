/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.database;

import java.io.IOException;

/**
 * Interface for database commands, so they can called by a common transaction
 * wrapper
 */
interface DatabaseOperation {
    /**
     * Executes the database operation - the caller sets appropriate handlers to
     * catch database exceptions - the implementor does DB operation, returns result
     * object (for conversion to JSON)
     */
    Object execute() throws IOException;

    /**
     * provides suitable context for error message; will be returned to user
     */
    Object getErrorContext();

    /**
     * returns true if operation is (or should be) read-only
     */
    boolean isReadOnly();

}
