/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.upsert;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.service.DataStreamService;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;
import uk.nominet.iot.utils.error.ProgramDefectException;

import static uk.org.nominet.iot.registry.service.dto.DumpObjects.DeviceStreamLink;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * DeviceLinkUpsertMetaService is a meta-service to help with upserts It helps upsert links to Devices, so is used by
 * DataStreams & Transforms
 */
public class DeviceLinkUpsertMetaService {
    private final static Logger log = LoggerFactory.getLogger(new Object() {}.getClass().getEnclosingClass());

    private final DataStreamService dataStreamService = new DataStreamService();

    public void upsert(SessionPermissionAssistant spa, String dataStreamKey, String deviceLinksAsJsonString) {
        List<DeviceStreamLink> requestedLinks = decodeFromJson(deviceLinksAsJsonString);

        DataStream dataStream = dataStreamService.getDataStream(spa, dataStreamKey);

        List<DeviceStreamLink> existingLinks
            = dataStreamService.getDevicesAttachedToStream(dataStream).stream()
                               .map(x -> new DeviceStreamLink(x.getDevice().getKey(), x.getName()))
                               .collect(Collectors.toList());
        // log.info("device links existing ={}", existingLinks);

        if (areSame(requestedLinks, existingLinks)) {
            log.info("Device links already are already as requested: no need to update");
        } else {
            updateDeviceLinks(spa, dataStreamKey, requestedLinks, existingLinks);
        }
    }

    private void updateDeviceLinks(SessionPermissionAssistant spa,
                                   String dataStreamKey,
                                   List<DeviceStreamLink> requestedLinks,
                                   List<DeviceStreamLink> existingLinks) {
        // first, delete any links that are entirely removed
        existingLinks.stream().filter(x -> requestedLinks.stream().noneMatch(y -> y.equals(x)))
                     .forEach(x -> dataStreamService.removeDevice(spa, x.key, dataStreamKey));

        // then update all the new/modified ones
        for (DeviceStreamLink requestedLink : requestedLinks) {
            if (existingLinks.contains(requestedLink)) {
                dataStreamService.removeDevice(spa, requestedLink.key, dataStreamKey);
            }
            dataStreamService.addDevice(spa, requestedLink.key, dataStreamKey, requestedLink.linkName);
        }
    }

    static List<DeviceStreamLink> decodeFromJson(String dnsJson) {
        try {
            return Arrays.asList(new ObjectMapper().readValue(dnsJson, DeviceStreamLink[].class));
        } catch (IOException | ProgramDefectException e) {
            throw new DataException("bad JSON: " + e.getMessage());
        }
    }

    static private boolean areSame(List<DeviceStreamLink> lhs, List<DeviceStreamLink> rhs) {
        if (lhs.size() != rhs.size())
            return false;
        for (DeviceStreamLink dl : lhs) {
            if (!rhs.contains(dl))
                return false;
        }
        return true;
    }
}
