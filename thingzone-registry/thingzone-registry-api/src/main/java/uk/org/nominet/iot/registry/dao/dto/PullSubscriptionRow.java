/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.dto;

import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.PullSubscription;

public class PullSubscriptionRow {
    private PullSubscriptionId id;
    private DNSkey streamKey;
    private String uri;
    private String uriParams;
    private Integer interval;
    private Integer aligned;

    public PullSubscriptionRow(PullSubscriptionId id,
                               DNSkey dataStreamKey,
                               String uri,
                               String uriParams,
                               Integer interval,
                               Integer aligned) {
        this.id = id;
        this.streamKey = dataStreamKey;
        this.uri = uri;
        this.uriParams = uriParams;
        this.interval = interval;
        this.aligned = aligned;
    }

    public PullSubscriptionRow(PullSubscription ps) {
        this.id = new PullSubscriptionId(ps.getId());
        this.streamKey = ps.getDataStream().getKey();
        this.uri = ps.getUri();
        this.uriParams = ps.getUriParams();
        this.interval = ps.getInterval();
        this.aligned = ps.getAligned();
    }

    public PullSubscriptionId getId() {
        return id;
    }

    public DNSkey getStreamKey() {
        return streamKey;
    }

    public String getUri() {
        return uri;
    }

    public String getUriParams() {
        return uriParams;
    }

    public Integer getInterval() {
        return interval;
    }

    public Integer getAligned() {
        return aligned;
    }
}
