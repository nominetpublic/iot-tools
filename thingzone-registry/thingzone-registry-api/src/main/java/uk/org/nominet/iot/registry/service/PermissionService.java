/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service;

import static uk.org.nominet.iot.registry.model.Privilege.USE_SESSION;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.NoResultException;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;

import uk.org.nominet.iot.registry.common.AuthException;
import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.ACLDao;
import uk.org.nominet.iot.registry.dao.AccountDao;
import uk.org.nominet.iot.registry.dao.DNSkeyDao;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.GroupDao;
import uk.org.nominet.iot.registry.dao.GroupUserDao;
import uk.org.nominet.iot.registry.dao.UserCredentialsDao;
import uk.org.nominet.iot.registry.dao.UserDao;
import uk.org.nominet.iot.registry.dao.dto.KeyPermissions;
import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.Group;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.model.Session;
import uk.org.nominet.iot.registry.model.User;
import uk.org.nominet.iot.registry.service.dto.DnsKeyDetailDto;
import uk.org.nominet.iot.registry.service.dto.PermissionDto;
import uk.org.nominet.iot.registry.webserver.api.support.EntityLoader;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;
import uk.org.nominet.iot.registry.webserver.api.support.UserIdentifier;
import uk.nominet.iot.utils.error.ProgramDefectException;

public class PermissionService {
    private final static Logger log = LoggerFactory.getLogger(PermissionService.class);

    private UserCredentialsDao userCredentialsDao;
    private UserDao userDao;
    private AccountDao accountDao;
    private ACLDao aclDao;
    private DNSkeyDao dnskeyDao;
    private GroupDao groupDao;
    private GroupUserDao groupUserDao;

    private SessionService sessionService;
    private UserPrivilegeService userPrivilegeService;

    public PermissionService() {
        DaoFactory df = DaoFactoryManager.getFactory();
        userCredentialsDao = df.userCredentialsDao();
        userDao = df.userDao();
        accountDao = df.accountDao();
        aclDao = df.aclDao();
        dnskeyDao = df.dnsKeyDao();
        groupDao = df.groupDao();
        groupUserDao = df.groupUserDao();

        sessionService = new SessionService();
        userPrivilegeService = new UserPrivilegeService();
    }

    /**
     * get permissions that apply to the user who's asking
     */
    public PermissionDto getPermissions(SessionPermissionAssistant spa, String key) {
        log.info("user {} getting permissions on {} for {}", spa.getUsername(), key);
        // can we see this object at all?
        DNSkey dnsKey = EntityLoader.getDnsKey(spa, key, Permission.DISCOVER);
        // get permissions
        List<Permission> perms = aclDao.get(dnsKey, spa.getRelevantAccounts());

        return new PermissionDto(dnsKey, spa.getUsername(), perms);
    }

    /**
     * List user permissions on key. Requires ADMIN permission or QUERY_STREAMS privilege.
     *
     * @param spa the current session
     * @param key the key
     * @param username the username to filter by or null
     * @return the map
     */
    public Map<String, Set<Permission>> listUserPermissions(SessionPermissionAssistant spa,
                                                            String key,
                                                            String username) {
        DNSkey dnsKey = EntityLoader.getDnsKeyWithoutCheckingPermission(key);
        log.info("user {} list permissions on {} for {}", spa.getUsername(), dnsKey, username);

        // does requesting user have admin privilege?
        List<Permission> reqPerms = aclDao.get(dnsKey, spa.getUserAccount());
        boolean canQueryStreams = userPrivilegeService.canQueryStreams(spa);
        if (canQueryStreams || reqPerms.contains(Permission.ADMIN)) {
            log.info("user has ADMIN rights on object");
        } else {
            log.info("auth error: user does not have ADMIN rights on object");
            throw new AuthException(spa, dnsKey, "need to own object");
        }

        return aclDao.listUserPermissions(dnsKey, username);
    }

    /**
     * List group permissions on key. Requires ADMIN permission or QUERY_STREAMS privilege.
     *
     * @param spa the current session
     * @param key the key
     * @param groupname filter by group name or null
     */
    public Map<String, Set<Permission>> listGroupPermissions(SessionPermissionAssistant spa,
                                                             String key,
                                                             String groupname) {
        DNSkey dnsKey = EntityLoader.getDnsKeyWithoutCheckingPermission(key);
        log.info("user {} list permissions on {} for {}", spa.getUsername(), dnsKey, groupname);
        // does requesting user have admin privilege?
        List<Permission> reqPerms = aclDao.get(dnsKey, spa.getUserAccount());
        boolean canQueryStreams = userPrivilegeService.canQueryStreams(spa);
        if (canQueryStreams || reqPerms.contains(Permission.ADMIN)) {
            log.info("user has ADMIN rights on object");
        } else {
            log.info("auth error: user does not have ADMIN rights on object");
            throw new AuthException(spa, dnsKey, "need to own object");
        }

        return aclDao.listGroupPermissions(dnsKey, groupname);
    }

    public KeyPermissions listAllPermissionsForKey(SessionPermissionAssistant spa, String key) {
        DNSkey dnsKey = EntityLoader.getDnsKeyWithoutCheckingPermission(key);
        log.info("user {} list permissions on {} for {}", spa.getUsername(), dnsKey);

        // does requesting user have admin privilege?
        List<Permission> reqPerms = aclDao.get(dnsKey, spa.getUserAccount());
        boolean canQueryStreams = userPrivilegeService.canQueryStreams(spa);
        if (canQueryStreams || reqPerms.contains(Permission.ADMIN)) {
            log.info("user has ADMIN rights on object");
        } else {
            log.info("auth error: user does not have ADMIN rights on object");
            throw new AuthException(spa, dnsKey, "need to own object");
        }

        return aclDao.listAllPermissions(dnsKey);
    }

    public Map<String, KeyPermissions> listAllPermissionsForAccount(SessionPermissionAssistant spa,
                                                                    String username,
                                                                    String groupname,
                                                                    int size,
                                                                    int page) {
        if (!Strings.isNullOrEmpty(username)) {
            try {
                Account account = this.accountDao.getByUsername(username);
                return aclDao.listPermissionsForAccount(account, username, size, page);
            } catch (NoResultException e) {
                throw new DataException("No user found with name '" + username + "'", Status.NOT_FOUND);
            }
        } else if (!Strings.isNullOrEmpty(groupname)) {
            try {
                Account account = this.accountDao.getByGroupname(groupname);
                return aclDao.listPermissionsForAccount(account, groupname, size, page);
            } catch (NoResultException e) {
                throw new DataException("No group found with name '" + groupname + "'", Status.NOT_FOUND);
            }
        }
        Map<String, KeyPermissions> keyMap = new HashMap<>();
        return keyMap;
    }

    /**
     * get all permissions for a given key
     */
    public DnsKeyDetailDto getPermissionsForKey(SessionPermissionAssistant spa, String key) {
        if (!userPrivilegeService.canUseSession(spa)) {
            throw new AuthException(spa, "need privilege " + USE_SESSION);
        }

        KeyPermissions permissions = getInternalPermissionsForKey(spa, key);
        return new DnsKeyDetailDto(key, null, null, null, permissions.getUserPermissions(),
                                   permissions.getGroupPermissions());
    }

    public static List<Permission> validatePermissions(String permissionsString) {
        // parse & validate array of permissions
        List<Permission> permissions = new ArrayList<>();

        if (Strings.isNullOrEmpty(permissionsString)) {
            return permissions; // empty list
        }

        for (String p : Splitter.on(CharMatcher.anyOf(", ")).trimResults().omitEmptyStrings()
                                .split(permissionsString)) {
            if (!Permission.isValid(p)) {
                throw new DataException("invalid permission type:\"" + p + "\"");
            }
            permissions.add(Permission.fromString(p));
        }
        return permissions;
    }

    public void setUserPermissions(SessionPermissionAssistant spa,
                                   String key,
                                   String usernameAffected,
                                   List<Permission> requiredPermissions) {
        // need admin privilege
        DNSkey dnsKey = EntityLoader.getDnsKey(spa, key, Permission.ADMIN);
        // work out account that we want to update
        Account affectedAccount;
        try {
            affectedAccount = accountDao.getByUsername(usernameAffected);
        } catch (NoResultException exception) {
            throw new DataException("User not found", Status.NOT_FOUND);
        }
        // get current permissions
        List<Permission> currentPermissions = aclDao.get(dnsKey, affectedAccount);

        // make the changes
        for (DifferenceCalculator<Permission>.Difference diff : new DifferenceCalculator<Permission>().calculate(
            requiredPermissions, currentPermissions)) {
            if (diff.difference == DifferenceCalculator.Type.ADD) {
                aclDao.create(dnsKey, affectedAccount, diff.value);
                log.info("added permission {} to {} for {}", diff.value, dnsKey.toString(), affectedAccount);
            } else {
                aclDao.remove(dnsKey, affectedAccount, diff.value);
                log.info("removed permission {} to {} for {}", diff.value, dnsKey.toString(), affectedAccount);
            }
        }
    }

    public void setGroupPermissions(SessionPermissionAssistant spa, String key, String groupAffected,
                                    List<Permission> requiredPermissions) {
        // need admin privilege
        DNSkey dnsKey = EntityLoader.getDnsKey(spa, key, Permission.ADMIN);
        // work out account that we want to update
        Account affectedAccount;
        try {
            affectedAccount = accountDao.getByGroupname(groupAffected);
        } catch (NoResultException exception) {
            throw new DataException("Group not found", Status.NOT_FOUND);
        }
        // get current permissions
        List<Permission> currentPermissions = aclDao.get(dnsKey, affectedAccount);

        // make the changes
        for (DifferenceCalculator<Permission>.Difference diff : new DifferenceCalculator<Permission>().calculate(
            requiredPermissions, currentPermissions)) {
            if (diff.difference == DifferenceCalculator.Type.ADD) {
                aclDao.create(dnsKey, affectedAccount, diff.value);
                log.info("added permission {} to {} for {}", diff.value, dnsKey.toString(), affectedAccount);
            } else {
                aclDao.remove(dnsKey, affectedAccount, diff.value);
                log.info("removed permission {} to {} for {}", diff.value, dnsKey.toString(), affectedAccount);
            }
        }
    }

    public void setUserPermissionsKeys(SessionPermissionAssistant spa,
                                       String keysString,
                                       String usernameAffected,
                                       List<Permission> requiredPermissions,
                                       boolean remove) {
        Account affectedAccount;
        try {
            affectedAccount = accountDao.getByUsername(usernameAffected);
        } catch (NoResultException exception) {
            throw new DataException("User not found", Status.NOT_FOUND);
        }
        // get current permissions
        List<DNSkey> keys = validateKeys(spa, keysString, Permission.ADMIN);
        for (DNSkey dnsKey : keys) {
            for (Permission permission : requiredPermissions) {
                if (remove) {
                    try {
                        aclDao.remove(dnsKey, affectedAccount, permission);
                        log.info("removed permission {} to {} for {}", permission, dnsKey.toString(), affectedAccount);
                    } catch (DataException exception) {
                        // permission did not already exist
                    }
                } else {
                    try {
                        aclDao.create(dnsKey, affectedAccount, permission);
                        log.info("added permission {} to {} for {}", permission, dnsKey.toString(), affectedAccount);
                    } catch (DataException exception) {
                        // permission already exists
                    }
                }
            }
        }
    }

    public void setGroupPermissionsKeys(SessionPermissionAssistant spa,
                                        String keysString,
                                        String groupAffected,
                                        List<Permission> requiredPermissions,
                                        boolean remove) {
        // work out account that we want to update
        Account affectedAccount = accountDao.getByGroupname(groupAffected);

        List<DNSkey> keys = validateKeys(spa, keysString, Permission.ADMIN);
        for (DNSkey dnsKey : keys) {
            for (Permission permission : requiredPermissions) {
                if (remove) {
                    try {
                        aclDao.remove(dnsKey, affectedAccount, permission);
                        log.info("removed permission {} to {} for {}", permission, dnsKey.toString(), affectedAccount);
                    } catch (DataException exception) {
                        // permission did not already exist
                    }
                } else {
                    try {
                        aclDao.create(dnsKey, affectedAccount, permission);
                        log.info("added permission {} to {} for {}", permission, dnsKey.toString(), affectedAccount);
                    } catch (DataException exception) {
                        // permission already exists
                    }
                }
            }
        }
    }

    public void checkPermissions(SessionPermissionAssistant spa,
                                 String keysString,
                                 String permissionsString,
                                 UserIdentifier userIdentifier) {
        if (!userPrivilegeService.canUseSession(spa)) {
            throw new AuthException(spa, "need privilege " + USE_SESSION);
        }

        List<Account> applicableAccounts = getRelevantAccountsForUserIdentifier(spa, userIdentifier);
        List<Permission> requiredPermissions = validatePermissions(permissionsString);
        List<DNSkey> keys = validateKeys(keysString);

        if (keys.size() == 0) {
            throw new DataException("no DNSkeys specified");
        }
        if (requiredPermissions.size() == 0) {
            throw new DataException("no permissions specified");
        }

        // look up permissions
        for (DNSkey key : keys) {
            List<Permission> allowedPermissions = aclDao.get(key, applicableAccounts);
            for (Permission permission : requiredPermissions) {
                if (!allowedPermissions.contains(permission)) {
                    throw new DataException("missing permission " + permission + " for key " + key.getKey(),
                                            Status.NOT_FOUND);
                }
            }
        }
        // no problems, i.e. successful check
    }

    /**
     * filters which keys have the requested permissions (for use by Bridge/Datastore)
     */
    public List<DNSkey> filterPermissions(SessionPermissionAssistant spa,
                                          String keysString,
                                          String permissionsString,
                                          UserIdentifier userIdentifier) {
        if (!userPrivilegeService.canUseSession(spa)) {
            throw new AuthException(spa, "need privilege " + USE_SESSION);
        }
        List<Account> applicableAccounts = getRelevantAccountsForUserIdentifier(spa, userIdentifier);

        log.info("user {} filtering permissions on {} for {}", spa.getUsername(), keysString, permissionsString);

        List<Permission> requiredPermissions = validatePermissions(permissionsString);
        List<DNSkey> keys = validateKeys(keysString);

        if (keys.size() == 0) {
            throw new DataException("no DNSkeys specified");
        }
        if (requiredPermissions.size() == 0) {
            throw new DataException("no permissions specified");
        }

        List<DNSkey> filtered = new ArrayList<>();
        for (DNSkey key : keys) {
            List<Permission> allowedPermissions = aclDao.get(key, applicableAccounts);

            boolean ok = true;
            for (Permission permission : requiredPermissions) {
                if (!allowedPermissions.contains(permission)) {
                    ok = false;
                    break;
                }
            }
            if (ok) {
                filtered.add(key);
            }
        }
        return filtered;
    }

    /**
     * get the set of accounts that are related to a given (i.e. someone else's) session
     */
    private List<Account> getRelevantAccountsForUserIdentifier(SessionPermissionAssistant spa,
                                                               UserIdentifier userIdentifier) {
        if (!userPrivilegeService.canUseSession(spa)) {
            throw new AuthException(spa, "need privilege " + USE_SESSION);
        }
        List<Account> applicableAccounts = new ArrayList<>();
        if (!userIdentifier.isPublicUser()) {
            // firstly get user
            User user;
            if (userIdentifier.hasSessionKey()) {
                // get the relevant user's session
                Session userSession = sessionService.getSessionAsReference(userIdentifier.getSessionKey());

                // get set of accounts to use, based on sessionKey
                user = userDao.getUserByUsername(userSession.getUsername());
            } else if (userIdentifier.hasUsername()) {
                final String username = userIdentifier.getUsername();

                // get set of accounts to use, based on sessionKey
                if (!userCredentialsDao.exists(username)) {
                    throw new DataException("user not found", Status.NOT_FOUND);
                }
                user = userDao.getUserByUsername(username);
            } else {
                throw new ProgramDefectException("");
            }

            Account userAccount = accountDao.getByOwner(user);
            // we looked up user account OK
            applicableAccounts.add(userAccount);
            // get group accounts that the user is a member of
            List<Account> groupAccounts = accountDao.listByGroups(groupUserDao.listGroupsByUser(user));
            applicableAccounts.addAll(groupAccounts);
        }
        // public account is always applicable
        applicableAccounts.add(spa.getPublicAccount());
        return applicableAccounts;
    }

    public Set<String> getInternalPermittedUsers(SessionPermissionAssistant spa,
                                                 String keyString,
                                                 String permissionsString) {
        if (!userPrivilegeService.canUseSession(spa)) {
            throw new AuthException(spa, "need privilege " + USE_SESSION);
        }

        List<Permission> requiredPermissions = validatePermissions(permissionsString);
        // check that the key exists and is not deleted
        if (!dnskeyDao.isAlive(keyString)) {
            throw new DataException("DNSkey '" + keyString + "' not found", Status.NOT_FOUND);
        }
        KeyPermissions internalPermissions = getInternalPermissionsForKey(spa, keyString);
        // filter for matching permissions
        Set<String> permittedUsers = new HashSet<>();
        Map<String, Set<Permission>> internalUserPermissionsForKey = internalPermissions.getUserPermissions();
        for (String username : internalUserPermissionsForKey.keySet()) {
            Set<Permission> accountPermissions = internalUserPermissionsForKey.get(username);
            if (accountPermissions.containsAll(requiredPermissions)) {
                permittedUsers.add(username);
            }
        }

        List<String> permittedGroupNames = new ArrayList<>();
        Map<String, Set<Permission>> internalGroupPermissionsForKey = internalPermissions.getGroupPermissions();
        for (String groupName : internalGroupPermissionsForKey.keySet()) {
            Set<Permission> accountPermissions = internalGroupPermissionsForKey.get(groupName);
            if (accountPermissions.containsAll(requiredPermissions)) {
                permittedGroupNames.add(groupName);
            }
        }
        if (!permittedGroupNames.isEmpty()) {
            List<Group> applicableGroups = groupDao.listGroupsByNames(permittedGroupNames);
            for (Group group : applicableGroups) {
                permittedUsers.addAll(userCredentialsDao.listUsernamesByGroup(group).keySet());
            }
        }

        return permittedUsers;
    }

    public KeyPermissions getInternalPermissionsForKey(SessionPermissionAssistant spa, String keyString) {
        if (!userPrivilegeService.canUseSession(spa)) {
            throw new AuthException(spa, "need privilege " + USE_SESSION);
        }
        // check that the key exists and is not deleted
        if (!dnskeyDao.isAlive(keyString)) {
            throw new DataException("DNSkey '" + keyString + "' not found", Status.NOT_FOUND);
        }
        return aclDao.listAllPermissions(new DNSkey(keyString));
    }

    /**
     * Convert comma or space separated string into list
     */
    private static List<String> convertKeysString(String keysString) {
        List<String> keys = new ArrayList<>();

        if (Strings.isNullOrEmpty(keysString)) {
            return keys; // empty list
        }

        for (String k : Splitter.on(CharMatcher.anyOf(", ")).trimResults().omitEmptyStrings().split(keysString)) {
            keys.add(k);
        }
        return keys;
    }

    private static List<DNSkey> validateKeys(String keysString) {
        List<DNSkey> dnsKeys = new ArrayList<>();

        for (String k : convertKeysString(keysString)) {
            dnsKeys.add(EntityLoader.getDnsKeyWithoutCheckingPermission(k));
        }
        return dnsKeys;
    }

    private static List<DNSkey> validateKeys(SessionPermissionAssistant spa, String keysString, Permission permission) {
        List<DNSkey> dnsKeys = new ArrayList<>();

        for (String k : convertKeysString(keysString)) {
            dnsKeys.add(EntityLoader.getDnsKey(spa, k, permission));
        }
        return dnsKeys;
    }

}
