/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service;

import uk.org.nominet.iot.registry.dao.dto.*;
import uk.org.nominet.iot.registry.model.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * DumpRetrieverAccessor provides database access for DumpRetriever
 *
 * This interface allows implementation of object retrieval for "dump" either
 * using normal (JPA, with permission check) access, or fast (JBDC / unmanaged
 * route)
 */
public interface DumpRetrieverAccessor {
    Map<String, Set<Permission>> getGroupPermissions(DNSkey key);

    Map<String, Set<Permission>> getUserPermissions(DNSkey key);

    List<DNSdata> listDnsDataByKey(DNSkey key);

    List<StreamOnDeviceRow> listStreamOnDeviceByStream(DNSkey key);

    List<StreamOnDeviceRow> listStreamOnDeviceByDevice(DNSkey key);

    List<PushSubscriberRow> listPushSubscribers(DataStream dataStream);

    Optional<PullSubscriptionRow> getPullSubscription(DataStream dataStream);

    DataStream getDataStream(String key);

    Transform getTransformByOutputStream(DataStream dataStream);

    List<TransformSourceRow> getTransformSources(Transform transform);

    List<TransformOutputStreamRow> getTransformOutputStreams(Transform transform);
}
