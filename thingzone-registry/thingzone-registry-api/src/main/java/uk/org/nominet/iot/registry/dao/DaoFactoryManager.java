/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao;

import uk.nominet.iot.utils.error.ProgramDefectException;

/**
 * simple generator for DaoFactory - to allow easy insertion of "test" Dao
 * factory
 */
public class DaoFactoryManager {

    private static DaoFactory daoFactory;

    public static void setFactory(DaoFactory daoFactory) {
        if (DaoFactoryManager.daoFactory != null) {
            if (DaoFactoryManager.daoFactory != daoFactory) {
                // change from one sort to another is unlikely to be a good thing
                throw new ProgramDefectException("change in daoFactory");
            }

        }
        DaoFactoryManager.daoFactory = daoFactory;
    }

    // for unit-test purposes...
    public static void clearFactory() {
        daoFactory = null;
    }

    public static DaoFactory getFactory() {
        return daoFactory;
    }

}
