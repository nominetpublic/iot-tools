/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.DataStreamDao;
import uk.org.nominet.iot.registry.dao.PullSubscriptionDao;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.model.PullSubscription;
import uk.org.nominet.iot.registry.service.dto.PullSubscriptionSummaryDto;
import uk.org.nominet.iot.registry.webserver.api.support.EntityLoader;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;

public class PullSubscriptionService {
    private final static Logger log = LoggerFactory.getLogger(PullSubscriptionService.class);

    PullSubscriptionDao dao;
    DataStreamDao dataStreamDao;
    ACLService aclService;

    public PullSubscriptionService() {
        dao = DaoFactoryManager.getFactory().pullSubscriptionDao();
        dataStreamDao = DaoFactoryManager.getFactory().dataStreamDao();
        aclService = new ACLService();
    }

    public List<PullSubscription> getVisible(SessionPermissionAssistant spa) {
        // only view if MODIFY allowed on DataStream
        return dao.getAllowed(spa.getRelevantAccounts(), Permission.MODIFY);
    }

    public void set(SessionPermissionAssistant spa, PullSubscriptionSummaryDto ps) {
        set(spa, ps.dataStream, ps.uri, ps.uriParams, ps.interval, ps.aligned);
    }

    // create or update
    public void set(SessionPermissionAssistant spa,
                    String dataStreamKey,
                    String uri,
                    String uriParams,
                    Integer interval,
                    Integer aligned) {
        // needs MODIFY permission
        DataStream dataStream = EntityLoader.getDataStream(spa, dataStreamKey, Permission.MODIFY);
        if (dao.exists(dataStream)) {
            PullSubscription ps = dao.getForStream(dataStream);
            ps.setUri(uri);
            ps.setUriParams(uriParams);
            ps.setInterval(interval);
            ps.setAligned(aligned);
        } else {
            dao.create(dataStream, uri, uriParams, interval, aligned);
        }
    }

    public Optional<PullSubscription> get(SessionPermissionAssistant spa, String dataStreamKey) {
        // needs MODIFY permission on DataStream
        DataStream dataStream = EntityLoader.getDataStream(spa, dataStreamKey, Permission.MODIFY);

        if (dao.exists(dataStream)) {
            return Optional.of(dao.getForStream(dataStream));
        } else {
            return Optional.empty();
        }
    }

    public void delete(SessionPermissionAssistant spa, String dataStreamKey) {
        // needs MODIFY permission
        DataStream dataStream = EntityLoader.getDataStream(spa, dataStreamKey, Permission.MODIFY);

        delete(dataStream);
    }

    public void delete(DataStream dataStream) {
        if (dao.exists(dataStream)) {
            log.info("removed pullSubscription from {}", dataStream.getKey().getKey());
            dao.delete(dataStream);
        } else {
            log.warn("no pullSubscription to remove from {}", dataStream.getKey().getKey());
        }
    }

}
