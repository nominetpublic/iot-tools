/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.database;

import javax.persistence.EntityExistsException;
import javax.persistence.PersistenceException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

import org.eclipse.persistence.exceptions.DatabaseException;
import org.postgresql.util.PSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.org.nominet.iot.registry.common.AuthException;
import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.webserver.api.support.JsonDataExceptionResult;

public class DatabaseTransaction {
    private final static Logger log = LoggerFactory.getLogger(DatabaseTransaction.class);

    public DatabaseTransaction() {
    }

    private final static String AUTH_ERRMSG = "authentication error";

    /**
     * Internal transaction wrapper Expected error responses return a Response
     * object with the appropriate HTTP status code Unexpected errors return
     * {@link WebApplicationException}
     */
    public Object executeInTransaction(DatabaseOperation operation) {
        try {
            log.info("creating transaction");
            if (operation.isReadOnly()) {
                DatabaseDriver.beginReadOnlyTransaction();
            } else {
                DatabaseDriver.beginMutableTransaction();
            }
            Object response = operation.execute();
            if (operation.isReadOnly()) {
                log.info("discarding transaction");
                DatabaseDriver.abortTransaction();
            } else {
                log.info("committing transaction");
                DatabaseDriver.commitTransaction();
            }
            return response;
        } catch (AuthException e) {
            // effectively a "user error" - respond to user, but WITHOUT detail
            DatabaseDriver.abortTransaction();
            log.info("AuthException: \"{}\"", e.formatDetails());
            if (true) {
                log.info("AuthException stack...", e);
            }
            return new JsonDataExceptionResult(AUTH_ERRMSG).asResponse(e.getStatus());
        } catch (DataException e) {
            // effectively a "user error" - respond to user
            DatabaseDriver.abortTransaction();
            log.info("DataException: \"{}\"", e.getMessage());
            return new JsonDataExceptionResult(e.formatWithCause()).asResponse(e.getStatus());
        } catch (EntityExistsException e) {
            logException("EntityExistsException", e);
            DatabaseDriver.abortTransaction();
            // re-throw something simple to Jetty to avoid log spam
            throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
        } catch (PersistenceException e) {
            try {
                handlePostgresException(e);
            } catch (DataException pe) {
                DatabaseDriver.abortTransaction();
                return new JsonDataExceptionResult(pe.getMessage()).asResponse(pe.getStatus());
            }
            logException("Unhandled PersistenceException", e);
            DatabaseDriver.abortTransaction();
            // re-throw something simple to Jetty to avoid log spam
            throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
        }
        // catch-all rollback
        catch (Throwable e) {
            logException("Exception", e);
            DatabaseDriver.abortTransaction();
            // re-throw something simple to Jetty to avoid log spam
            throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * detect & handler PostgreSQL exceptions so we can provide something more
     * helpful than 500 error
     */
    void handlePostgresException(Throwable e) throws DataException {
        // inspect to handle PostgreSQL / PSQLException
        if (e.getCause() != null) {
            Throwable cause = e.getCause();
            if (cause instanceof DatabaseException && cause.getCause() instanceof PSQLException) {
                PSQLException pe = (PSQLException) cause.getCause();
                switch (pe.getSQLState()) {

                case "22P02":
                    // "invalid_text_representation" - i.e. JSON
                    // provide full message back to user
                    log.info("bogus JSON rejected by database {}: returning error message to user", pe.getMessage());
                    throw new DataException(pe.getMessage());

                case "23503":
                    // foreign_key_violation
                    log.info("FK violation");
                    throw new DataException("child object exists", Status.CONFLICT);

                case "23505":
                    // foreign_key_violation
                    log.info("unique constraint violation");
                    throw new DataException("Duplicate value", Status.CONFLICT);

                default:
                    // something else; continue
                }
            }
        }
    }

    void logException(String description, Throwable e) {
        log.error(description, e);
    }

    void logException(String description, String message) {
        log.error(description, message);
    }

}
