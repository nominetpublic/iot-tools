/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.upsert;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.service.PushSubscriberService;
import uk.org.nominet.iot.registry.service.dto.PushSubscriberSummaryDto;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;
import uk.nominet.iot.utils.error.ProgramDefectException;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * PushSubscriberUpsertMetaService is a meta-service to help with upserts It helps upsert PushSubscribers, so is used
 * for DataStream/Transform upsert
 */
public class PushSubscriberUpsertMetaService {
    private final static Logger log = LoggerFactory.getLogger(new Object() {}.getClass().getEnclosingClass());

    private final PushSubscriberService pushSubscriberService = new PushSubscriberService();

    public void upsert(SessionPermissionAssistant spa, String key, String pushSubscribersAsJsonString) {
        List<PushSubscriberSummaryDto> requestedPushSubscribers = decodeFromJson(pushSubscribersAsJsonString, key);

        log.info("PushSubscribers received={}", pushSubscribersAsJsonString);
        log.info("PushSubscribers inferred={}", requestedPushSubscribers);
        List<PushSubscriberSummaryDto> existingPushSubscribers
            = pushSubscriberService.getForStream(spa, key).stream().map(PushSubscriberSummaryDto::new)
                                   .collect(Collectors.toList());
        log.info("PushSubscribers existing ={}", existingPushSubscribers);

        if (areSame(requestedPushSubscribers, existingPushSubscribers)) {
            log.info("DNS records are already as requested: no need to update");
        } else {
            updatePushSubscribers(spa, requestedPushSubscribers, existingPushSubscribers);
        }
    }

    private void updatePushSubscribers(SessionPermissionAssistant spa,
                                       List<PushSubscriberSummaryDto> requestedRecords,
                                       List<PushSubscriberSummaryDto> existingRecords) {
        // first, delete any PushSubscribers that are entirely removed
        existingRecords.stream().filter(ps -> requestedRecords.stream().noneMatch(x -> x.refersToSameDbEntity(ps)))
                       .forEach(ps -> pushSubscriberService.delete(spa, ps.dataStream, ps.uri));

        // then update all the new/modified ones
        requestedRecords.forEach(ps -> {
            if (existingRecords.contains(ps)) {
                pushSubscriberService.delete(spa, ps.dataStream, ps.uri);
            }
            pushSubscriberService.set(spa, ps);
        });
    }

    // we need to provide something appropriate for Jackson to deserialise into
    // and it won't deserialise object tree into @JsonRawValue
    // so we need to deserialise into a map, & then pack that back into a string
    public static class ParamsDeserialiser {
        public String uri;
        public HashMap<String, Object> uriParams;
        public HashMap<String, Object> headers;
        public String method;
        public String retrySequence;

        public ParamsDeserialiser() {}
    }

    static List<PushSubscriberSummaryDto> decodeFromJson(String dnsJson, String parentKey) {
        try {
            ParamsDeserialiser[] decoded = new ObjectMapper().readValue(dnsJson, ParamsDeserialiser[].class);
            return Arrays.stream(decoded).map(pd -> {
                try {
                    return new PushSubscriberSummaryDto(parentKey,
                                                        pd.uri,
                                                        new ObjectMapper().writeValueAsString(pd.uriParams),
                                                        new ObjectMapper().writeValueAsString(pd.headers),
                                                        pd.method,
                                                        pd.retrySequence);
                } catch (JsonProcessingException e) {
                    throw new DataException("bad JSON: " + e.getMessage());
                }
            }).collect(Collectors.toList());
        } catch (IOException | ProgramDefectException e) {
            throw new DataException("bad JSON: " + e.getMessage());
        }
    }

    static private boolean areSame(List<PushSubscriberSummaryDto> lhs, List<PushSubscriberSummaryDto> rhs) {
        if (lhs.size() != rhs.size())
            return false;
        for (PushSubscriberSummaryDto x : lhs) {
            if (rhs.stream().filter(x::equals).count() != 1)
                return false;
        }
        return true;
    }
}
