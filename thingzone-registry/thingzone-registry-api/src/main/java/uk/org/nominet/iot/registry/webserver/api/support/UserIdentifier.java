/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api.support;

import com.google.common.base.Strings;
import uk.nominet.iot.utils.error.ProgramDefectException;

/** A user can be specified by sessionKey or username */
public class UserIdentifier {
    private boolean isPublicUser = false;
    private String sessionKey = null;
    private String username = null;

    private enum UserIdentifierType {
        PUBLICUSER, SESSIONKEY, USERNAME
    }

    private UserIdentifierType userIdentifierType;

    public UserIdentifier(String sessionKey, String username) {
        if (Strings.isNullOrEmpty(sessionKey) && Strings.isNullOrEmpty(username)) {
            userIdentifierType = UserIdentifierType.PUBLICUSER;
            isPublicUser = true;
        } else if (!Strings.isNullOrEmpty(sessionKey)) {
            // if both are specified, use sessionKey
            userIdentifierType = UserIdentifierType.SESSIONKEY;
            this.sessionKey = sessionKey;
        } else if (!Strings.isNullOrEmpty(username)) {
            userIdentifierType = UserIdentifierType.USERNAME;
            this.username = username;
        }
    }

    public String getLogDescription() {
        switch (userIdentifierType) {
        case PUBLICUSER:
            return "(no user specified)";
        case SESSIONKEY:
            return "sessionkey=\"" + sessionKey + "\"";
        case USERNAME:
            return "username=\"" + username + "\"";
        default:
            throw new ProgramDefectException("");
        }
    }

    public boolean isPublicUser() {
        return isPublicUser;
    }

    public boolean hasUsername() {
        return userIdentifierType == UserIdentifierType.USERNAME;
    }

    public boolean hasSessionKey() {
        return userIdentifierType == UserIdentifierType.SESSIONKEY;
    }

    public String getUsername() {
        if (!hasUsername()) {
            throw new ProgramDefectException("no username stored");
        }
        return username;
    }

    public String getSessionKey() {
        if (!hasSessionKey()) {
            throw new ProgramDefectException("no sessionkey stored");
        }
        return sessionKey;
    }
}
