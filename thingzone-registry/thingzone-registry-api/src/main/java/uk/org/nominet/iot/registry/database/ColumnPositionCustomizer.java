/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.database;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.eclipse.persistence.config.DescriptorCustomizer;
import org.eclipse.persistence.descriptors.ClassDescriptor;
import org.eclipse.persistence.mappings.DatabaseMapping;

public class ColumnPositionCustomizer implements DescriptorCustomizer {
    @Override
    public void customize(ClassDescriptor descriptor) throws Exception {
        descriptor.setShouldOrderMappings(true);
        List<DatabaseMapping> mappings = descriptor.getMappings();
        addWeight(this.getClass(descriptor.getJavaClassName()), mappings);
    }

    private void addWeight(Class<?> cls, List<DatabaseMapping> mappings) {
        Map<String, Integer> fieldOrderMap = getColumnPositions(cls, null);
        for (DatabaseMapping mapping : mappings) {
            String key = mapping.getAttributeName();
            Object obj = fieldOrderMap.get(key);
            int weight = 1;
            if (obj != null) {
                weight = Integer.parseInt(obj.toString());
            }
            mapping.setWeight(weight);
        }
    }

    private Class<?> getClass(String javaFileName) throws ClassNotFoundException {
        Class<?> cls = null;
        if (javaFileName != null && !javaFileName.equals("")) {
            cls = Class.forName(javaFileName);
        }
        return cls;
    }

    private Map<String, Integer> getColumnPositions(Class<?> classFile, Map<String, Integer> columnOrder) {
        if (columnOrder == null) {
            columnOrder = new HashMap<>();
        }
        Field[] fields = classFile.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(ColumnPosition.class)) {
                ColumnPosition cp = field.getAnnotation(ColumnPosition.class);
                columnOrder.put(field.getName(), cp.position());
            }
        }
        if (classFile.getSuperclass() != null && classFile.getSuperclass() != Object.class) {
            this.getColumnPositions(classFile.getSuperclass(), columnOrder);
        }
        return columnOrder;
    }
}
