/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonRawValue;

import uk.org.nominet.iot.registry.model.PullSubscription;

public class PullSubscriptionDetailDto {
    @JsonInclude(Include.NON_NULL)
    public DataStreamSummaryDto dataStream;

    public String uri;

    @JsonRawValue
    public String uriParams;

    public Integer interval;

    public Integer aligned;

    public PullSubscriptionDetailDto(DataStreamSummaryDto dataStream,
                                     String uri,
                                     String uriParams,
                                     Integer interval,
                                     Integer aligned) {
        this.dataStream = dataStream;
        this.uri = uri;
        this.uriParams = uriParams;
        this.interval = interval;
        this.aligned = aligned;
    }

    public PullSubscriptionDetailDto(PullSubscription ps) {
        this.dataStream = DataStreamSummaryDto.basic(ps.getDataStream(), null, null);
        this.uri = ps.getUri();
        this.uriParams = ps.getUriParams();
        this.interval = ps.getInterval();
        this.aligned = ps.getAligned();
    }

    @Override
    public String toString() {
        return "PullSubscriptionSummaryDto{" + "dataStream='" + dataStream + '\'' + ", uri='" + uri + '\''
            + ", uriParams='" + uriParams + '\'' + ", interval=" + interval + ", aligned=" + aligned + '}';
    }
}
