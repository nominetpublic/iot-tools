/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import uk.org.nominet.iot.registry.database.DatabaseTransaction;
import uk.org.nominet.iot.registry.database.SimpleDatabaseOperation;
import uk.org.nominet.iot.registry.model.Session;
import uk.org.nominet.iot.registry.model.User;
import uk.org.nominet.iot.registry.service.SessionService;
import uk.org.nominet.iot.registry.service.UserAuthenticationService;
import uk.org.nominet.iot.registry.service.UserService;
import uk.org.nominet.iot.registry.service.dto.UserSummaryDto;
import uk.org.nominet.iot.registry.webserver.api.support.JsonErrorResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonFlattenedMapResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSuccessResult;

/**
 * API handler for the session actions
 */
@Path("/session")
public class SessionHandler {
    private final static Logger log = LoggerFactory.getLogger(SessionHandler.class);

    public static final String SESSIONKEY_HEADER = "X-FollyBridge-SessionKey";

    @Context
    Request request;

    @Context
    private javax.servlet.http.HttpServletRequest hsr;

    private SessionService sessionService;
    private UserAuthenticationService userAuthenticationService;

    public SessionHandler() {
        sessionService = new SessionService();
        userAuthenticationService = new UserAuthenticationService();
    }

    /**
     * Log in to the system
     * @param username
     * @param password
     * @return the session key and user details
     */
    @POST
    @Path("login")
    @Produces(MediaType.APPLICATION_JSON)
    public Object login(
            final @FormParam(value = "username") String username,
            final @FormParam(value = "password") String password)
    {
        if (Strings.isNullOrEmpty(username))
            return new JsonErrorResult("missing username").asResponse(Status.BAD_REQUEST);
        if (Strings.isNullOrEmpty(password))
            return new JsonErrorResult("missing password").asResponse(Status.BAD_REQUEST);

        final String remoteIp = hsr.getRemoteAddr();
        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("login: username={} password={} ip={}", username, "***", remoteIp);

                userAuthenticationService.authenticate(username, password);

                Session session = sessionService.open(username, remoteIp);
                log.info("getSessionKey={}", session.getSessionKey());

                JsonFlattenedMapResult result = new JsonFlattenedMapResult();
                result.put("sessionKey", session.getSessionKey());
                result.put("ttl", sessionService.getTimeRemaining(session));

                User user = new UserService().getUserByUsername(username);
                result.put("user", new UserSummaryDto(username, user.getDetails(), user.getPreferences(), null));

                return result.output();
            }
        });
    }

    /**
     * Update the time to live on the session key
     * @return the new time to live
     */
    @POST
    @Path("keepalive")
    @Produces(MediaType.APPLICATION_JSON)
    public Object keepalive() {
        final String remoteIp = hsr.getRemoteAddr();
        final String sessionKey = hsr.getHeader(SESSIONKEY_HEADER);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("keepalive: sessionKey={} ip={}", sessionKey, remoteIp);

                Session session = sessionService.getSession(sessionKey, remoteIp);

                int ttl = sessionService.getTimeRemaining(session);
                log.info(String.format("sessionKey ttl=%.3f", (float) ttl / 1000));

                JsonFlattenedMapResult result = new JsonFlattenedMapResult();
                result.put("ttl", ttl);

                return result.output();
            }
        });
    }

    /**
     * Logout the user for the session key in the header
     * @return success
     */
    @POST
    @Path("logout")
    @Produces(MediaType.APPLICATION_JSON)
    public Object logout() {
        final String remoteIp = hsr.getRemoteAddr();
        final String sessionKey = hsr.getHeader(SESSIONKEY_HEADER);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            public Object execute() {
                log.info("logout: session={} ip={}", sessionKey, remoteIp);

                Session session = sessionService.getSession(sessionKey, remoteIp);
                sessionService.closeSession(session);

                return new JsonSuccessResult().output();
            }
        });
    }

}
