/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.database;

import java.util.*;

import uk.nominet.iot.utils.config.ValidatingConfigProperties;

class DbConfigProperties extends ValidatingConfigProperties implements DbConfig {

    private final static String[] INTEGER_PROPERTY_NAMES = new String[] { "Port", };
    private final static String[] STRING_PROPERTY_NAMES = new String[] { "Hostname", "SID", "Username", "Password", };

    private final static String JDBC_USER = "javax.persistence.jdbc.user";
    private final static String JDBC_PASSWORD = "javax.persistence.jdbc.password";
    private final static String JDBC_URL = "javax.persistence.jdbc.url";

    DbConfigProperties(Properties properties) {
        // superclass will validate, calling our overridden methods
        super(properties);
    }

    @Override
    protected String[] getIntegerPropertyNames() {
        return INTEGER_PROPERTY_NAMES;
    }

    @Override
    protected String[] getStringPropertyNames() {
        return STRING_PROPERTY_NAMES;
    }

    /**
    * masks things like passwords when logging
    */
    @Override
    protected boolean isMaskedProperty(String propertyName) {
        return propertyName.equals("Password");
    }

    /** gets the properties, as javax.persistence.jdbc.xxx keys */
    @Override
    public Properties getJdbcProperties() {
        Properties props = new Properties();
        props.setProperty(JDBC_USER, getUsername());
        props.setProperty(JDBC_PASSWORD, getPassword());
        props.setProperty(JDBC_URL, String.format("jdbc:postgresql://%s:%d/%s", getHostname(), getPort(), getSid()));
        return props;
    }

    @Override
    public String getHostname() {
        return stringProps.get("Hostname");
    }

    @Override
    public int getPort() {
        return intProps.get("Port");
    }

    @Override
    public String getSid() {
        return stringProps.get("SID");
    }

    @Override
    public String getUsername() {
        return stringProps.get("Username");
    }

    @Override
    public String getPassword() {
        return stringProps.get("Password");
    }

}
