/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;

import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.common.DeviceKeyFormatter;
import uk.org.nominet.iot.registry.common.UidGenerator;
import uk.org.nominet.iot.registry.common.UserKeyValidator;
import uk.org.nominet.iot.registry.dao.AbstractJpaDao;
import uk.org.nominet.iot.registry.dao.DNSkeyDao;
import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.EntityStatus;
import uk.org.nominet.iot.registry.model.Permission;

class DNSkeyDaoImpl extends AbstractJpaDao<DNSkey> implements DNSkeyDao {
    private final static Logger log = LoggerFactory.getLogger(DNSkeyDaoImpl.class);

    DNSkeyDaoImpl() {
        super(DNSkey.class);
    }

    @Override
    public String generateKey() {
        String key;
        while (true) {
            key = UidGenerator.generateRandomDeviceKey(); // as a plain string
            key = DeviceKeyFormatter.format(key); // formatted as xxxx-xxxx-xxxx-xxxx-xxxx-xxxx.x.x
            if (!exists(key))
                break;
            // we have a duplicate; try again
            log.warn("deviceKey collision with key '{}'", key);
        }
        return key;
    }

    @Override
    public DNSkey create() {
        return createKnownKey(generateKey());
    }

    @Override
    public DNSkey createKnownKey(String key) {
        if (!UserKeyValidator.validate(key)) {
            throw new DataException("key '" + key + "' is not valid");
        }
        DNSkey dnsKey = new DNSkey(key);
        create(dnsKey); // (will throw if already exists)
        return dnsKey;
    }

    DNSkey create(DNSkey dnsKey) {
        // see if exists already
        if (exists(dnsKey.getKey())) {
            throw new DataException("DNS key '" + dnsKey.getKey() + "' exists already", Status.CONFLICT);
        }

        persist(dnsKey);
        log.info("created DNSkey {}", dnsKey.getKey());
        return dnsKey;
    }

    @Override
    public boolean exists(String key) {
        try {
            loadByKey(key);
            return true;
        } catch (PersistenceException e) {
            if (e instanceof NoResultException) {
                return false;
            } else {
                throw e;
            }
        }
    }

    @Override
    public boolean isAlive(String key) {
        try {
            DNSkey loadByKey = loadByKey(key);
            if (loadByKey.getStatus() == EntityStatus.DELETED) {
                return false;
            }
            return true;
        } catch (PersistenceException e) {
            if (e instanceof NoResultException) {
                return false;
            } else {
                throw e;
            }
        }
    }

    @Override
    public DNSkey getByKey(String key) {
        DNSkey dnsKey = loadByKey(key);
        if (dnsKey.getStatus() == EntityStatus.DELETED) {
            throw new DataException("This entity is deleted", Status.NOT_FOUND);
        }
        return dnsKey;
    }

    private DNSkey loadByKey(String key) {
        return loadByMatch("key", key);
    }

    @Override
    public void delete(DNSkey dnsKey) {
        // Do not actually remove the key, use EntityStatus for soft deletion
        DNSkey key = loadByKey(dnsKey.getKey());
        key.setStatus(EntityStatus.DELETED);
    }

    @Override
    public List<DNSkey> getAllowed(List<Account> accounts, Permission permission, int size, int page) {
        return getAllowed(accounts, ImmutableList.of(permission), size, page);
    }

    @Override
    public List<DNSkey> getAllowed(List<Account> accounts, List<Permission> permissions, int size, int page) {
        checkTransaction();

        TypedQuery<DNSkey> query = getEntityManager().createNamedQuery("DNSkeys.getAllowed", getEntityClass());
        query.setParameter("accounts", accounts);
        query.setParameter("perms", permissions);
        query.setParameter("status", EntityStatus.DELETED);
        if (size > 0) {
            query.setFirstResult(page * size);
            query.setMaxResults(size);
        }
        return query.getResultList();
    }

    @Override
    public List<DNSkey> getByType(List<Account> accounts, String type, int size, int page) {
        checkTransaction();

        TypedQuery<DNSkey> query = getEntityManager().createNamedQuery("DNSkeys.findByType", getEntityClass());
        query.setParameter("accounts", accounts);
        query.setParameter("perms", ImmutableList.of(Permission.DISCOVER));
        query.setParameter("type", type);
        if (size > 0) {
            query.setFirstResult(page * size);
            query.setMaxResults(size);
        }
        return query.getResultList();
    }

    /**
     * List entities and their related device and datastreams filtered by
     * permissions
     *
     * @param  permissions List of permissions to filter returned entities by
     * @param  size        of result
     * @param  page        number of results
     * @return             An list of object arrays with the rows containing DNSkey,
     *                     Device, DataStream objects in that order, Device and
     *                     DataStream objects can be null
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Object[]> listEntitiesRelated(List<Account> accounts, List<Permission> permissions, String type,
                                              String name, int size, int page) {
        checkTransaction();
        if (permissions.isEmpty()) {
            permissions = ImmutableList.of(Permission.DISCOVER);
        }

        // Not using a named query due to needing fields from multiple tables
        EntityManager em = getEntityManager();
        Query query = em.createQuery("SELECT k, k.device, k.dataStream "
            + "FROM ACL ac JOIN DNSkey k LEFT JOIN FETCH k.device "
            + "LEFT JOIN FETCH k.dataStream "
            + "WHERE ac.key = k AND ac.account IN :accounts "
            + "AND ac.permission IN :perms AND k.status <> :status "
            + "AND (:type is null OR k.entity_type = :type) "
            + "AND (:name is null OR k.name = :name) "
            + "ORDER BY k");
        query.setParameter("accounts", accounts);
        query.setParameter("perms", permissions);
        query.setParameter("status", EntityStatus.DELETED);
        query.setParameter("type", type);
        query.setParameter("name", name);
        if (size > 0) {
            query.setFirstResult(page * size);
            query.setMaxResults(size);
        }
        return query.getResultList();
    }

    /**
     * List entities and their related device and datastreams filtered by
     * permissions
     *
     * @param  permissions List of permissions to filter returned entities by
     * @param  type        entity type to filter by or null
     * @param  name        entity name to filter by or null
     * @param  account     account to filter permissions by
     * @param  ownerName   username or group name of account
     * @param  size        of result
     * @param  page        number of results
     * @return             An list of object arrays with the rows containing DNSkey,
     *                     Device, DataStream objects in that order Device and
     *                     DataStream objects can be null
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Object[]> listEntitiesRelatedWithPermissions(List<Account> accounts, List<Permission> permissions,
                                                             String type, String name, Account account, int size, int page) {
        checkTransaction();
        if (permissions.isEmpty()) {
            permissions = ImmutableList.of(Permission.DISCOVER);
        }

        List<String> accountParameters = new ArrayList<>();
        accounts.forEach(e -> accountParameters.add("?"));

        List<String> permissionParameters = new ArrayList<>();
        permissions.forEach(e -> permissionParameters.add("?"));

        String sql = "WITH permissions AS (SELECT ac.key, a.id AS account_id, "
                + "COUNT(ac.key) FILTER (WHERE ac.permission = 'ADMIN') AS ADMIN, "
                + "COUNT(ac.key) FILTER (WHERE ac.permission = 'UPLOAD') AS UPLOAD, "
                + "COUNT(ac.key) FILTER (WHERE ac.permission = 'CONSUME') AS CONSUME,"
                + "COUNT(ac.key) FILTER (WHERE ac.permission = 'MODIFY') AS MODIFY, "
                + "COUNT(ac.key) FILTER (WHERE ac.permission = 'ANNOTATE') AS ANNOTATE, "
                + "COUNT(ac.key) FILTER (WHERE ac.permission = 'DISCOVER') AS DISCOVER, "
                + "COUNT(ac.key) FILTER (WHERE ac.permission = 'UPLOAD_EVENTS') AS UPLOAD_EVENTS, "
                + "COUNT(ac.key) FILTER (WHERE ac.permission = 'CONSUME_EVENTS') AS CONSUME_EVENTS, "
                + "COUNT(ac.key) FILTER (WHERE ac.permission = 'VIEW_LOCATION') AS VIEW_LOCATION, "
                + "COUNT(ac.key) FILTER (WHERE ac.permission = 'VIEW_METADATA') AS VIEW_METADATA "
                + "FROM thingzone.accounts a LEFT JOIN thingzone.ACLs ac ON a.id = ac.account_id "
                + "GROUP BY a.id, ac.key) "
            + "SELECT k.DNSkey, k.entity_type, k.name, k.status, "
            + "d.DNSkey, d.metadata, d.location, ds.DNSkey, "
            + "ds.metadata, ds.isTransform, permissions.* "
            + "FROM thingzone.DNSkeys k "
            + "CROSS JOIN thingzone.accounts acc "
            + "JOIN thingzone.ACLs acl ON acl.key = k.DNSkey "
            + "LEFT JOIN thingzone.Devices d ON d.DNSkey = k.DNSkey "
            + "LEFT JOIN thingzone.DataStreams ds ON ds.DNSkey = k.DNSkey "
            + "LEFT JOIN permissions ON k.DNSkey = permissions.key AND permissions.account_id = acc.id "
            + "WHERE k.status <> 'DELETED' "
            + "AND (acc.id = ?) "
            + "AND ((? IS NULL) OR (k.entity_type = ?)) "
            + "AND ((? IS NULL) OR (k.name = ?)) "
            + "AND (acl.account_id IN (" + String.join(",", accountParameters) + ")) "
            + "AND (acl.permission IN (" + String.join(",", permissionParameters) + ")) "
            + "ORDER BY k.DNSkey LIMIT ? OFFSET ?";

        // Not using a named query due to needing fields from multiple tables
        EntityManager em = getEntityManager();
        Query query = em.createNativeQuery(sql);
        int index = 1;
        query.setParameter(index, account.getId());
        index++;
        query.setParameter(index, type);
        index++;
        query.setParameter(index, type);
        index++;
        query.setParameter(index, name);
        index++;
        query.setParameter(index, name);
        for(Account a :accounts) {
           index++;
           query.setParameter(index, a.getId());
        };
        for(Permission p : permissions) {
            index++;
            query.setParameter(index, p.getValue());
        }
        index++;
        query.setParameter(index, size);
        index++;
        query.setParameter(index, page * size);
        return query.getResultList();
    }
}
