/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import javax.persistence.*;

import org.eclipse.persistence.annotations.Customizer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import uk.org.nominet.iot.registry.database.ColumnPosition;
import uk.org.nominet.iot.registry.database.ColumnPositionCustomizer;
import uk.org.nominet.iot.registry.database.DatabaseDriver;
import uk.org.nominet.iot.registry.service.dto.DNSdataDto;

@Entity
@Table(name = "DNSdata", schema = DatabaseDriver.SCHEMA_NAME,
       uniqueConstraints = { @UniqueConstraint(name = "DNSdata_UQ_KEY",
                                               columnNames = { "DNSkey, subdomain, rrtype, rdata" }), })
@Customizer(ColumnPositionCustomizer.class)
@JsonPropertyOrder(value = { "subdomain", "ttl", "rrtype", "rdata" })
@NamedQueries({ @NamedQuery(name = "DNSdata.findByKey", query = "SELECT d FROM DNSdata d WHERE d.key = :key"), })
public class DNSdata {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ColumnPosition(position = 1)
    @JsonIgnore // don't serialise to user (simple for now: can upgrade to DTO if required)
    private Long id;

    @JoinColumn(name = "DNSkey", referencedColumnName = "DNSkey", nullable = false)
    @ColumnPosition(position = 2)
    @JsonIgnore // don't serialise to user (simple for now: can upgrade to DTO if required)
    private DNSkey key;

    @Lob
    @Column(name = "description", nullable = false)
    @ColumnPosition(position = 3)
    private String description;

    @Column(name = "subdomain", nullable = false)
    @ColumnPosition(position = 4)
    private String subdomain;

    @Column(name = "ttl", nullable = false)
    @ColumnPosition(position = 5)
    private Integer ttl;

    @Column(name = "rrtype", nullable = false)
    @ColumnPosition(position = 6)
    private String rrtype;

    @Lob
    @Column(name = "rdata", nullable = false)
    @ColumnPosition(position = 7)
    private String rdata;

    DNSdata() {}

    public DNSdata(DNSdataDto ddr) {
        this.key = ddr.getDnsKey();
        this.description = ddr.getDescription();
        this.subdomain = ddr.getSubdomain();
        this.ttl = ddr.getTtl();
        this.rrtype = ddr.getRrtype();
        this.rdata = ddr.getRdata();
    }

    @Override
    public String toString() {
        return "DNSdata{" + "key=" + key.toString() + ", description='" + description + '\'' + ", subdomain='"
            + subdomain + '\'' + ", ttl='" + ttl + '\'' + ", rrtype='" + rrtype + '\'' + ", rdata='" + rdata + '\''
            + '}';
    }

    public Long getId() {
        return id;
    }

    public DNSkey getKey() {
        return key;
    }

    public String getDescription() {
        return description;
    }

    public String getSubdomain() {
        return subdomain;
    }

    public Integer getTTL() {
        return ttl;
    }

    public String getRRTYPE() {
        return rrtype;
    }

    public String getRDATA() {
        return rdata;
    }

}
