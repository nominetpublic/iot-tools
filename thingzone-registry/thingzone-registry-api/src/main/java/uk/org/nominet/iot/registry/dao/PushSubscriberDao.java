/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao;

import java.util.List;

import uk.org.nominet.iot.registry.dao.dto.PushSubscriberRow;
import uk.org.nominet.iot.registry.model.*;

public interface PushSubscriberDao extends JpaDao<PushSubscriber> {

    PushSubscriber create(DataStream dataStream,
                          String uri,
                          String uriParams,
                          String headers,
                          String method,
                          String retrySequence);

    List<PushSubscriber> getForStream(DataStream dataStream);

    List<PushSubscriber> getAllowed(List<Account> accounts, List<Permission> perms);

    List<PushSubscriber> getAllowed(List<Account> accounts, Permission permission);

    List<PushSubscriber> getAll();

    PushSubscriber get(DataStream dataStream, String uri);

    boolean exists(DataStream dataStream, String uri); // no throw

    void delete(DataStream dataStream, String uri);

    // returns objects that aren't JPA-managed (for performance)
    List<PushSubscriberRow> unmanagedGetForStream(DataStream dataStream);
}
