/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;

import uk.org.nominet.iot.registry.database.ColumnPosition;
import uk.org.nominet.iot.registry.database.ColumnPositionCustomizer;
import uk.org.nominet.iot.registry.database.DatabaseDriver;

@Entity
@Table(name = "groups_users", schema = DatabaseDriver.SCHEMA_NAME)
@Customizer(ColumnPositionCustomizer.class)
@IdClass(GroupUserKey.class)
public class GroupUser {
    @Id
    @Column(name = "group_id")
    private Long groupId;

    @Id
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "admin", nullable = false)
    @ColumnPosition(position = 3)
    private boolean admin;

    @ManyToOne
    @PrimaryKeyJoinColumn(name = "group_id", referencedColumnName = "id")
    private Group group;

    @ManyToOne
    @PrimaryKeyJoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    public GroupUser() {}

    public GroupUser(Group group, User user, boolean admin) {
        this.group = group;
        this.groupId = group.getId();
        this.user = user;
        this.userId = user.getId();
        this.admin = admin;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
        this.groupId = group.getId();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user.getId();
    }
}
