/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

import uk.org.nominet.iot.registry.common.AuthException;
import uk.org.nominet.iot.registry.common.DataException;
import uk.org.nominet.iot.registry.dao.DaoFactory;
import uk.org.nominet.iot.registry.dao.DaoFactoryManager;
import uk.org.nominet.iot.registry.dao.DataStreamDao;
import uk.org.nominet.iot.registry.dao.StreamOnDeviceDao;
import uk.org.nominet.iot.registry.dao.dto.KeyPermissions;
import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.DNSdata;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.Device;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.model.Privilege;
import uk.org.nominet.iot.registry.model.PullSubscription;
import uk.org.nominet.iot.registry.model.PushSubscriber;
import uk.org.nominet.iot.registry.model.StreamOnDevice;
import uk.org.nominet.iot.registry.service.dto.DNSdataDto;
import uk.org.nominet.iot.registry.service.dto.DataStreamSummaryDto;
import uk.org.nominet.iot.registry.service.dto.DeviceSummaryDetailDto;
import uk.org.nominet.iot.registry.service.dto.DumpObjects.DataStreamWithPermissionsAndDeviceKeys;
import uk.org.nominet.iot.registry.service.dto.PullSubscriptionSummaryDto;
import uk.org.nominet.iot.registry.service.dto.PushSubscriberSummaryDto;
import uk.org.nominet.iot.registry.webserver.ConfigFactory;
import uk.org.nominet.iot.registry.webserver.KeyGenerationType;
import uk.org.nominet.iot.registry.webserver.api.support.EntityLoader;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;

/**
 * DataStream Service
 */
public class DataStreamService {
    private final static Logger log = LoggerFactory.getLogger(DataStreamService.class);

    private DataStreamDao dataStreamDao;

    private ACLService aclService;
    private UserPrivilegeService userPrivilegeService;
    private DNSdataService dnsDataService;
    private PullSubscriptionService pullSubscriptionService;
    private PushSubscriberService pushSubscriberService;
    private PermissionService permissionService;
    private StreamOnDeviceDao sodDao;

    private final Privilege PRIVILEGE_REQUIRED_FOR_CREATE = Privilege.CREATE_DATASTREAM;

    public DataStreamService() {
        DaoFactory df = DaoFactoryManager.getFactory();
        dataStreamDao = df.dataStreamDao();
        sodDao = df.streamOnDeviceDao();

        aclService = new ACLService();
        userPrivilegeService = new UserPrivilegeService();
        dnsDataService = new DNSdataService();
        pullSubscriptionService = new PullSubscriptionService();
        pushSubscriberService = new PushSubscriberService();
        permissionService = new PermissionService();
    }

    public boolean exists(String key) {
        return dataStreamDao.exists(key);
    }

    public DataStream create(SessionPermissionAssistant spa, boolean externallyNamed, String key, String name,
                             String type) {
        if (!userPrivilegeService.canCreate(spa, PRIVILEGE_REQUIRED_FOR_CREATE)) {
            throw new AuthException(spa, "need privilege " + PRIVILEGE_REQUIRED_FOR_CREATE);
        }

        DataStream dataStream;
        if (!externallyNamed) {
            // user didn't specify a key
            dataStream = dataStreamDao.create();
        } else {
            // user did specify a key:
            if (ConfigFactory.getConfig().getKeyGenerationType() != KeyGenerationType.EXTERNAL) {
                throw new DataException("external key generation is not configured");
            }
            dataStream = dataStreamDao.createKnownKey(key);
        }
        DNSkey dnsKey = dataStream.getKey();
        if (!Strings.isNullOrEmpty(name)) {
            dnsKey.setName(name);
        }
        if (!Strings.isNullOrEmpty(type)) {
            dnsKey.setType(type);
        }

        // create & attach "owner" ACL
        Account account = spa.getUserAccount();
        aclService.setOwnerPrivileges(account, dnsKey);

        return dataStream;
    }

    public DataStream upsert(SessionPermissionAssistant spa, String key, boolean externallyNamed) {
        if (dataStreamDao.exists(key)) {
            return getDataStream(spa, key);
        } else {
            // if name or type are set then they will be updated later as part of the upsert
            return create(spa, externallyNamed, key, null, null);
        }
    }

    public List<DataStream> getVisibleTo(SessionPermissionAssistant spa) {
        // get all datastreams visible to specified user (incl. those publicly visible)
        return dataStreamDao.getAllowed(spa.getRelevantAccounts(), Permission.DISCOVER);
    }

    public List<DataStream> getAllowed(SessionPermissionAssistant spa, List<Permission> perms) {
        return dataStreamDao.getAllowed(spa.getRelevantAccounts(), perms);
    }

    public DataStream getDataStream(SessionPermissionAssistant spa, String dataStreamKey) {
        return EntityLoader.getDataStream(spa, dataStreamKey, Permission.DISCOVER);
    }

    public DataStreamSummaryDto getDataStreamSummary(SessionPermissionAssistant spa, DataStream dataStream) {
        String metadata = null;
        boolean canQueryStreams = userPrivilegeService.canQueryStreams(spa);
        if (canQueryStreams || aclService.isAllowed(spa, dataStream.getKey(),
            ImmutableList.of(Permission.ADMIN, Permission.MODIFY, Permission.VIEW_METADATA))) {
            metadata = dataStream.getMetadata();
        }

        return DataStreamSummaryDto.basic(dataStream, metadata, null);
    }

    /**
     * Method that will return all possible data for the datastream
     *
     * @param spa the current user session
     * @param dataStream the datastream to display
     * @param showPermissions if permissions should be displayed
     * @param showDevices if any device links should be displayed
     * @param showSubscribers if any pull or push subscriptions should be displayed
     */
    public DataStreamSummaryDto getDataStreamSummaryDetail(SessionPermissionAssistant spa, DataStream dataStream,
                                                           boolean showPermissions, boolean showDevices,
                                                           boolean showSubscribers) {
        // get DNSdata per stream
        boolean canQueryStreams = userPrivilegeService.canQueryStreams(spa);
        DNSkey key = dataStream.getKey();
        List<DNSdata> dnsDatas = dnsDataService.dnsDataListByKey(spa, key);
        List<DNSdataDto> streamDns = null;
        if (dnsDatas != null) {
            streamDns = new ArrayList<>();
            for (DNSdata dnsData : dnsDataService.dnsDataListByKey(spa, key)) {
                streamDns.add(new DNSdataDto(dnsData));
            }
        }

        String metadata = null;
        if (canQueryStreams || aclService.isAllowed(spa, dataStream.getKey(),
            ImmutableList.of(Permission.ADMIN, Permission.MODIFY, Permission.VIEW_METADATA))) {
            metadata = dataStream.getMetadata();
        }

        KeyPermissions permissions = new KeyPermissions(key, null, null);
        if (showPermissions) {
            if (canQueryStreams || aclService.isAllowed(spa, key, Permission.ADMIN)) {
                permissions = permissionService.listAllPermissionsForKey(spa, key.getKey());
            }
        }

        List<DeviceSummaryDetailDto> streamDtos = new ArrayList<>();
        if (showDevices) {
            for (StreamOnDevice streamOnDevice : sodDao.findByDataStream(dataStream)) {
                Device device = streamOnDevice.getDevice();
                // only display streams that the user has permission to see
                DNSkey deviceKey = device.getKey();
                if (canQueryStreams || aclService.isAllowed(spa, deviceKey, Permission.DISCOVER)) {
                    List<DNSdataDto> deviceDns = DNSdataService.asDto(dnsDataService.dnsDataListByKey(spa, deviceKey));

                    KeyPermissions devicePermissions = new KeyPermissions(key, null, null);
                    if (showPermissions) {
                        if (aclService.isAllowed(spa, deviceKey, Permission.ADMIN)) {
                            devicePermissions = permissionService.listAllPermissionsForKey(spa, key.getKey());
                        }
                    }
                    String deviceMetadata = null;
                    if (canQueryStreams || aclService.isAllowed(spa, device.getKey(),
                        ImmutableList.of(Permission.ADMIN, Permission.MODIFY, Permission.VIEW_METADATA))) {
                        deviceMetadata = dataStream.getMetadata();
                    }

                    String deviceLocation = null;
                    if (canQueryStreams || aclService.isAllowed(spa, device.getKey(),
                        ImmutableList.of(Permission.ADMIN, Permission.MODIFY, Permission.VIEW_LOCATION))) {
                        deviceLocation = device.getLocation();
                    }

                    streamDtos.add(
                        new DeviceSummaryDetailDto(deviceKey, deviceMetadata, deviceLocation, null, deviceDns,
                                                   streamOnDevice.getName(), devicePermissions.getUserPermissions(),
                                                   devicePermissions.getGroupPermissions()));
                }
            }
        }

        if (showSubscribers) {
            // get Push/Pull
            Optional<PullSubscription> pullSubscriptionOption;
            List<PushSubscriber> pushSubscribers;

            if (canQueryStreams || aclService.isAllowed(spa, dataStream.getKey(), Permission.MODIFY)) {
                pullSubscriptionOption = pullSubscriptionService.get(spa, dataStream.getKey().getKey());
                pushSubscribers = pushSubscriberService.getForStream(spa, dataStream.getKey().getKey());
            } else {
                pullSubscriptionOption = Optional.empty();
                pushSubscribers = null;
            }
            return DataStreamSummaryDto.details(dataStream, metadata, streamDns, permissions.getUserPermissions(),
                permissions.getGroupPermissions(), null, streamDtos,
                PullSubscriptionSummaryDto.fromOptional(pullSubscriptionOption),
                PushSubscriberSummaryDto.asList(pushSubscribers));
        }

        return DataStreamSummaryDto.details(dataStream, metadata, streamDns, permissions.getUserPermissions(),
            permissions.getGroupPermissions(), null, streamDtos, Optional.empty(), null);
    }

    /**
     * set metadata requires MODIFY permission
     */
    public void setMetadata(SessionPermissionAssistant spa, String dataStreamKey, String metadata) {
        DataStream dataStream = EntityLoader.getDataStream(spa, dataStreamKey, Permission.MODIFY);
        dataStream.setMetadata(metadata);
    }

    /**
     * delete - requires ADMIN permission
     */
    public void delete(SessionPermissionAssistant spa, String dataStreamKey) {
        DataStream dataStream = EntityLoader.getDataStream(spa, dataStreamKey, Permission.ADMIN);

        // don't allow delete if actually a transform
        if (dataStream.getIsTransform()) {
            throw new DataException("cannot delete Transform via underlying DataStream");
        }

        // delete push/pull
        deletePullSubscription(dataStream);
        deletePushSubscribers(spa, dataStream);
        removeFromDevices(dataStream);

        // delete DNS records
        dnsDataService.removeAllRecords(dataStream.getKey());

        // delete DataStream itself
        dataStreamDao.delete(dataStream);

        // & remove ACLs
        aclService.removeForDelete(spa, dataStream.getKey());

        log.info("deleted dataStream {}", dataStreamKey);
    }

    void deletePullSubscription(DataStream dataStream) {
        pullSubscriptionService.delete(dataStream);
    }

    void deletePushSubscribers(SessionPermissionAssistant spa, DataStream dataStream) {
        for (PushSubscriber ps : pushSubscriberService.getForStream(spa, dataStream.getKey().getKey())) {
            pushSubscriberService.delete(dataStream, ps.getUri());
        }
    }

    void removeFromDevices(DataStream dataStream) {
        for (Device device : sodDao.getDevices(dataStream)) {
            sodDao.removeDataStream(device, dataStream);
        }
    }

    public DataStreamWithPermissionsAndDeviceKeys getDump(SessionPermissionAssistant spa, DataStream dataStream) {
        // retrieve objects for dump using permission checks & normal JPA
        DumpRetriever dumpRetriever = new DumpRetriever(new DefaultDumpRetrieverAccessor(spa));

        return dumpRetriever.getDataStreamSpecificDetails(dataStream,
            dumpRetriever.getGenericStreamDetails(dataStream));
    }

    public void addDevice(SessionPermissionAssistant spa, String deviceKey, String dataStreamKey, String sodName) {
        Device device = EntityLoader.getDevice(spa, deviceKey, Permission.DISCOVER);
        DataStream dataStream = EntityLoader.getDataStream(spa, dataStreamKey, Permission.MODIFY);

        sodDao.addDataStream(device, dataStream, sodName);
    }

    public void removeDevice(SessionPermissionAssistant spa, String deviceKey, String dataStreamKey) {
        // it's OK not to check permissions on a Device that we're removing from a datastream
        Device device = EntityLoader.getDeviceWithoutCheckingPermission(deviceKey);
        DataStream dataStream = EntityLoader.getDataStream(spa, dataStreamKey, Permission.MODIFY);

        sodDao.removeDataStream(device, dataStream);
    }

    public List<StreamOnDevice> getDevicesAttachedToStream(DataStream dataStream) {
        return sodDao.findByDataStream(dataStream);
    }
}
