/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.eclipse.persistence.annotations.Customizer;

import com.fasterxml.jackson.annotation.JsonIgnore;

import uk.org.nominet.iot.registry.database.ColumnPosition;
import uk.org.nominet.iot.registry.database.ColumnPositionCustomizer;
import uk.org.nominet.iot.registry.database.DatabaseDriver;

/**
 * Transform Functions
 */
@Entity
@Table(name = "TransformFunctions",
       schema = DatabaseDriver.SCHEMA_NAME,
       uniqueConstraints = { @UniqueConstraint(name = "UQ_TransformFunctions", columnNames = { "name, owner_id" }), })
@Customizer(ColumnPositionCustomizer.class)
public class TransformFunction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ColumnPosition(position = 1)
    private Long id;

    @Column(name = "name", nullable = false)
    @ColumnPosition(position = 2)
    private String name;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "functionContent", columnDefinition = "TEXT NOT NULL")
    @ColumnPosition(position = 3)
    private String functionContent;

    // account of user that created this function
    @JoinColumn(name = "owner_id", referencedColumnName = "id", nullable = false)
    @ColumnPosition(position = 4)
    private User owner;

    @Column(name = "functionType", nullable = false)
    @ColumnPosition(position = 5)
    private String functionType;

    public TransformFunction(String name, User owner, String functionType, String functionContent) {
        this.name = name;
        this.owner = owner;
        this.functionType = functionType;
        this.functionContent = functionContent;
    }

    TransformFunction() {
    }

    @Override
    public String toString() {
        return "TransformFunction{" + "id=" + id + ", name='" + name + '\'' + ", functionType='" + functionType + '\''
            + ", functionContent='" + functionContent + '\'' + ", owner=" + owner.toString() + '}';
    }

    @JsonIgnore
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getOwner() {
        return owner;
    }

    public String getFunctionType() {
        return functionType;
    }

    public void setFunctionType(String functionType) {
        this.functionType = functionType;
    }

    public String getFunctionContent() {
        return functionContent;
    }

    public void setFunctionContent(String functionContent) {
        this.functionContent = functionContent;
    }

}
