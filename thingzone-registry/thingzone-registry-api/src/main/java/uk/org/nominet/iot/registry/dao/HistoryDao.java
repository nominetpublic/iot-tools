/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao;

import java.util.Date;
import java.util.List;

import uk.org.nominet.iot.registry.database.DbObjectStreamer;
import uk.org.nominet.iot.registry.model.History;
import uk.org.nominet.iot.registry.model.HistoryAction;
import uk.org.nominet.iot.registry.model.ObjectType;

public interface HistoryDao extends JpaDao<History> {

    /**
     * Creates the object but does not persist it to the database
     */
    History create(String key, Date updated, ObjectType objectType, HistoryAction action);

    List<History> listByKey(String key);

    List<History> listByKeyAction(String key, HistoryAction action);

    DbObjectStreamer<History> streamFrom(Date date);

}
