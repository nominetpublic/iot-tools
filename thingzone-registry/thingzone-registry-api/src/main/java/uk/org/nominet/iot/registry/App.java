/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.org.nominet.iot.registry.database.DatabaseApp;
import uk.org.nominet.iot.registry.database.DatabaseDriver;
import uk.org.nominet.iot.registry.webserver.WebServer;
import uk.org.nominet.iot.registry.webserver.WebServerApp;

/**
 * App that both runs web server & dynamic updates which is convenient for
 * testing
 */
public class App {
    private final static Logger log = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        log.info("running Combined App...");

        try {
            App app = new App();
            app.run(args);
        } catch (Throwable e) {
            // we need exception info to be logged, not just printed to stderr
            log.error("exception thrown from main():", e);
        } finally {
            DatabaseDriver.globalDispose();
        }
    }

    void run(String args[]) throws Exception {
        DatabaseApp.initialiseDatabase();
        WebServerApp.initialiseWebServerConfig();

        // start web server (which never returns)
        WebServer.webMain();
    }

}
