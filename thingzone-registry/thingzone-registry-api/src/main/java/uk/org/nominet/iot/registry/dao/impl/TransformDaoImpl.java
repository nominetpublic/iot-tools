/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.impl;

import java.util.Collections;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.org.nominet.iot.registry.dao.AbstractJpaDao;
import uk.org.nominet.iot.registry.dao.TransformDao;
import uk.org.nominet.iot.registry.model.Account;
import uk.org.nominet.iot.registry.model.DataStream;
import uk.org.nominet.iot.registry.model.Permission;
import uk.org.nominet.iot.registry.model.Transform;
import uk.org.nominet.iot.registry.model.TransformFunction;

class TransformDaoImpl extends AbstractJpaDao<Transform> implements TransformDao {
    private final static Logger log = LoggerFactory.getLogger(TransformDaoImpl.class);

    TransformDaoImpl() {
        super(Transform.class);
    }

    @Override
    public Transform create(TransformFunction function, String parameterValues, String runSchedule) {
        DataStream outputStream = new DataStreamDaoImpl().create();
        outputStream.setIsTransform(true);
        return create(outputStream, function, parameterValues, runSchedule);
    }

    @Override
    public Transform createKnownKey(String key, TransformFunction function, String parameterValues,
                                    String runSchedule) {
        DataStream outputStream = new DataStreamDaoImpl().createKnownKey(key);
        outputStream.setIsTransform(true);
        return create(outputStream, function, parameterValues, runSchedule);
    }

    @Override
    public Transform create(DataStream outputStream, TransformFunction function, String parameterValues,
                            String runSchedule) {
        Transform transform = new Transform(outputStream, function, parameterValues, runSchedule);
        persist(transform);
        flush(); // get the Id populated
        log.info("created Transform for outputStream={}", outputStream.getKey());
        return transform;
    }

    @Override
    public Transform getById(Long id) {
        return findById(id);
    }

    @Override
    public Transform getByOutputStream(DataStream dataStream) {
        return loadByMatch("dataStream", dataStream);
    }

    @Override
    public boolean exists(DataStream dataStream) {
        try {
            getByOutputStream(dataStream);
            return true;
        } catch (PersistenceException e) {
            if (e instanceof NoResultException) {
                return false;
            } else {
                throw e;
            }
        }
    }

    @Override
    public void delete(Transform transform) {
        new DataStreamDaoImpl().delete(transform.getDataStream());
        deleteEntity(transform);
    }

    @Override
    public List<Transform> getAllowed(List<Account> accounts, Permission permission) {
        return getAllowed(accounts, Collections.singletonList(permission));
    }

    @Override
    public List<Transform> getAllowed(List<Account> accounts, List<Permission> perms) {
        checkTransaction();

        TypedQuery<Transform> query = getEntityManager().createNamedQuery("Transforms.getAllowed", getEntityClass());
        query.setParameter("accounts", accounts);
        query.setParameter("perms", perms);
        return query.getResultList();
    }

    @Override
    public List<Transform> getTransformsWithRunSchedule() {
        checkTransaction();

        TypedQuery<Transform> query = getEntityManager().createNamedQuery("Transforms.getWithRunSchedule",
            getEntityClass());
        return query.getResultList();
    }

}
