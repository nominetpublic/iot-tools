/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.database;

import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nominet.iot.utils.error.ProgramDefectException;

import static uk.org.nominet.iot.registry.database.SchemaCreator.DDL_OUTPUT_MODE;
import static uk.org.nominet.iot.registry.database.SchemaCreator.DDL_OUTPUT_MODE_DATABASE;

// see http://tomee.apache.org/jpa-concepts.html
// - the only clear documentation I found that really covers our use case
@SuppressWarnings("unused") // we need org.postgresql.Driver;
public class DatabaseDriver {
    private final static Logger log = LoggerFactory.getLogger(DatabaseDriver.class);

    // specially-named users
    final static public String PUBLIC_USERNAME = "public";
    final static public String USERADMIN_USERNAME = "user_admin";
    final static public String ALL_USERS_GROUP = "all_users";

    private static final String PERSISTENCE_UNIT_NAME = "iot_registry";
    public static final String SCHEMA_NAME = "thingzone";
//    public static final int URI_LENGTH = 1000;
    public static final int DNSKEY_LENGTH = 43; // with hyphens + dots

    // minimise DB rebuilds for unit testing
    private static boolean databaseRebuilt = false;

    private static boolean initialized = false;
    private static boolean initializing = false;

    public static boolean isDatabaseRebuilt() {
        return databaseRebuilt;
    }

    private static EntityManagerFactory factory = null;

    private static Properties eclipseLinkProps = new Properties();

    public static void setEclipseLinkProperties(Properties eclipseLinkProperties) {
        if (eclipseLinkProperties == null)
            DatabaseDriver.eclipseLinkProps = new Properties();
        else
            DatabaseDriver.eclipseLinkProps = eclipseLinkProperties;
    }

    private static Properties connectionProps = new Properties();

    public static void setConnectionProperties(Properties connectionProperties) {
        if (connectionProperties == null)
            DatabaseDriver.connectionProps = new Properties();
        else
            DatabaseDriver.connectionProps = connectionProperties;
    }

    synchronized public static void globalInitialise() {
        if (initialized) { // we should initialise exactly once!
            throw new ProgramDefectException("DB already initialised");
        }
        log.info("initialising database");

        Properties props = new Properties();

        // set EclipseLink props (if any)
        props.putAll(eclipseLinkProps);
        props.putAll(connectionProps);

        // links with <persistence-unit> specified in persistence.xml
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME, props);

        // remember that we just created DB, so JUnit tests don't need to recreate
        if (DDL_OUTPUT_MODE_DATABASE.equals(props.get(DDL_OUTPUT_MODE))) {
            databaseRebuilt = true;
        }

        initialized = true;
    }

    synchronized public static boolean isInitialized() {
        return initialized;
    }

    public static void globalDispose() {
        disposeEntityManagerFactory();
    }

    static void disposeEntityManagerFactory() {
        if (factory != null) {
            factory.close();
            factory = null;
        }
        initialized = false;
    }

    /**
     * We manage transaction lifetime, because we have clear entry/exit points so
     * it's straightforward to do so, and it gives us more control Transactions are
     * attached to thread via EntityManager
     */
    private static ThreadLocal<EntityManager> entityManagerHolder = new ThreadLocal<EntityManager>() {
        @Override
        protected EntityManager initialValue() {
            if (!isInitialized()) {
                throw new ProgramDefectException("DB not yet initialised");
            }

            // create empty holder; we'll populate on first use
            return null;
        }
    };

    public static EntityManager getEntityManager() {
        if (!initialized)
            throw new ProgramDefectException("DB not yet initialised");

        if (entityManagerHolder.get() == null)
            throw new ProgramDefectException("no entity manager");

        return entityManagerHolder.get();
    }

    private static void disposeEntityManager() {
        if (!initialized)
            throw new ProgramDefectException("DB not yet initialised");

        if (!haveEntityManager())
            throw new ProgramDefectException("no entity manager attached to thread");

        entityManagerHolder.get().close();
        entityManagerHolder.set(null);
    }

    private static EntityTransaction getEntityTransaction() {
        if (!isInitialized()) {
            throw new ProgramDefectException("DB not yet initialised");
        }

        if (entityManagerHolder.get() == null) {
            entityManagerHolder.set(factory.createEntityManager());
        }

        return entityManagerHolder.get().getTransaction();
    }

    private static boolean haveEntityManager() {
        if (!isInitialized()) {
            throw new ProgramDefectException("DB not yet initialised");
        }

        return (entityManagerHolder.get() != null);
    }

    public static void beginMutableTransaction() {
        log.info("beginTransaction");
        beginTransaction();
    }

    static void beginReadOnlyTransaction() {
        log.info("beginReadOnlyTransaction");
        beginTransaction();
        getEntityTransaction().setRollbackOnly();
    }

    private static void beginTransaction() {
        if (haveEntityManager()) {
            throw new ProgramDefectException("transaction already attached to thread");
        }
        getEntityTransaction().begin();
    }

    public static void abortTransaction() {
        log.info("abortTransaction");
        if (!haveEntityManager()) {
            throw new ProgramDefectException("no transaction attached to thread");
        }

        try {
            getEntityTransaction().rollback();
        } catch (IllegalStateException e) {
            // transaction was already closed by a previous error, so let's just let it lie
            // ("... I would have let it lie ... " etc)
        } catch (Throwable e) {
            // didn't expect this
            log.error("Exception during transaction rollback", e);
        }

        // but make sure we've dumped the broken stuff from our thread
        disposeEntityManager();
    }

    static void commitTransaction() {
        log.info("commitTransaction");
        if (!haveEntityManager()) {
            throw new ProgramDefectException("no transaction attached to thread");
        }

        getEntityTransaction().commit();
        disposeEntityManager();
    }

}
