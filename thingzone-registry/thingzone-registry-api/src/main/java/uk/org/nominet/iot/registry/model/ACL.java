/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.eclipse.persistence.annotations.Customizer;

import com.fasterxml.jackson.annotation.JsonIgnore;

import uk.org.nominet.iot.registry.database.ColumnPosition;
import uk.org.nominet.iot.registry.database.ColumnPositionCustomizer;
import uk.org.nominet.iot.registry.database.DatabaseDriver;
import uk.org.nominet.iot.registry.database.HistoryListener;

/**
 * called "ACLs" (Access Control Lists) because we need to reserve the term
 * "Permission" for individual permissions
 */
@Entity
@EntityListeners(HistoryListener.class)
@Table(name = "ACLs", schema = DatabaseDriver.SCHEMA_NAME,
       uniqueConstraints = { @UniqueConstraint(name = "ACLS_UQ", columnNames = { "key", "account_id", "permission" }) })
@NamedQueries({ @NamedQuery(name = "ACL.keyIsAllowed",
                            query = "SELECT ac FROM ACL ac WHERE ac.account in :accounts AND ac.permission in :perms"
                                + " AND ac.key = :key") })
@Customizer(ColumnPositionCustomizer.class)
public class ACL {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ColumnPosition(position = 1)
    @JsonIgnore
    private Long id;

    @ManyToOne()
    @JoinColumn(name = "key", referencedColumnName = "DNSkey", nullable = false)
    @ColumnPosition(position = 2)
    private DNSkey key;

    @JoinColumn(name = "account_id", referencedColumnName = "id", nullable = false)
    @ColumnPosition(position = 3)
    private Account account;

    // the PostgreSQL enum has to be created separately
    // e.g. CREATE TYPE thingzone.permission_type AS ENUM('foo', 'bar', 'baz');
    // see "create_types.sql"
    @Column(name = "permission", columnDefinition = DatabaseDriver.SCHEMA_NAME + ".PERMISSION_TYPE", nullable = false)
    @Convert(converter = Permission.PermissionConverter.class) // our custom converter
    @ColumnPosition(position = 4)
    private Permission permission;

    ACL() {}

    public ACL(DNSkey key, Account account) {
        this.key = key;
        this.account = account;
    }

    public Long getId() {
        return id;
    }

    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    public Permission getPermission() {
        return permission;
    }

    public Account getAccount() {
        return account;
    }

    public DNSkey getKey() {
        return key;
    }

    @Override
    public String toString() {
        return "ACL{" + "id=" + id + ", DNSkey='" + key + '\'' + ", account='" + account + '\'' + ", permission='"
            + permission + '\'' + '}';
    }
}
