/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Customizer;

import uk.org.nominet.iot.registry.database.ColumnPosition;
import uk.org.nominet.iot.registry.database.ColumnPositionCustomizer;
import uk.org.nominet.iot.registry.database.DatabaseDriver;

@Entity
@IdClass(ResourceKey.class)
@Table(name = "Resources", schema = DatabaseDriver.SCHEMA_NAME)
@NamedQueries({ @NamedQuery(name = "Resources.getAllowed",
                            query = "SELECT rs FROM Resource rs, ACL ac WHERE ac.account in :accounts AND ac.permission in :perms"
                                + " AND rs.key = ac.key"),
                @NamedQuery(name = "Resources.listByKey",
                            query = "SELECT rs FROM Resource rs, ACL ac WHERE ac.account in :accounts AND ac.permission in :perms"
                                + " AND rs.key = ac.key AND rs.key = :key"),
                @NamedQuery(name = "Resources.listByEntityType",
                            query = "SELECT rs FROM Resource rs, ACL ac, DNSkey dk WHERE ac.account in :accounts"
                                + " AND ac.permission in :perms AND rs.key = dk AND dk = ac.key AND dk.entity_type = :type")
                })
@Customizer(ColumnPositionCustomizer.class)
public class Resource {
    @Id
    @JoinColumn(name = "key", referencedColumnName = "DNSkey", nullable = false)
    DNSkey key;

    @Id
    @Column(name = "name", nullable = false)
    String name;

    @Column(name = "content_type", nullable = false)
    @ColumnPosition(position = 3)
    private String contentType;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "content", columnDefinition = "bytea NOT NULL")
    @ColumnPosition(position = 4)
    private byte[] content;

    @Column(name = "type", nullable = true)
    @ColumnPosition(position = 5)
    String type;

    public Resource() {
    }

    public Resource(DNSkey key, String name, String type, String contentType, byte[] content) {
        super();
        this.key = key;
        this.name = name;
        this.type = type;
        this.contentType = contentType;
        this.content = content;
    }

    public DNSkey getKey() {
        return key;
    }

    public void setKey(DNSkey key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Resource [key=" + key.getKey() + ", name=" + name + ", content_type=" + contentType + "]";
    }

}
