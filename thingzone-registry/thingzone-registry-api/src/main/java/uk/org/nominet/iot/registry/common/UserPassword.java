/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.common;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.xbill.DNS.utils.base64;

import uk.nominet.iot.utils.error.ProgramDefectException;

public class UserPassword {
    public static String hashPassword(String plaintext) {
        if (plaintext == null)
            plaintext = "";
        try {
            // hash as SHA-256
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(plaintext.getBytes());
            byte[] digestBytes = md.digest();

            // format as base-64
            String result = base64.toString(digestBytes);
            return result;
        } catch (NoSuchAlgorithmException e) {
            throw new ProgramDefectException(e);
        }
    }

    public static boolean checkPassword(String plaintext, String hash) {
        if (hash == null || plaintext == null)
            return false;
        String hashedPlaintext = hashPassword(plaintext);
        return hash.equals(hashedPlaintext);
    }
}
