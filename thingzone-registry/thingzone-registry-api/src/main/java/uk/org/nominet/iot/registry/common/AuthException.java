/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.common;

import javax.ws.rs.core.Response.Status;

import com.google.common.base.Strings;
import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;

public class AuthException extends DataException {
    private static final long serialVersionUID = -5205286324372633334L;

    private String username;
    private String dnsKey;

    public AuthException(SessionPermissionAssistant spa, String message) {
        this(spa, null, message);
    }

    public AuthException(SessionPermissionAssistant spa, DNSkey key, String message) {
        super(message, Status.FORBIDDEN);
        // strip to avoid taking objects outside DB transaction boundary
        if (spa != null) {
            this.username = spa.getUsername();
        }
        if (key != null) {
            this.dnsKey = key.toString();
        }

        // in err handler: avoid surprises
        if (this.username == null) {
            this.username = "?";
        }
    }

    public String formatDetails() {
        StringBuilder sb = new StringBuilder();
        sb.append(getMessage()).append(": username=\"").append(username).append("\"");
        if (!Strings.isNullOrEmpty(dnsKey)) {
            sb.append(", key=").append(dnsKey);
        }
        return sb.toString();
    }

    public String getUsername() {
        return username;
    }

    public String getDnsKey() {
        return dnsKey;
    }
}
