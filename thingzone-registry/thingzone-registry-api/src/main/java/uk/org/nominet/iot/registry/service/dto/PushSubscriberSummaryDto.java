/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonRawValue;

import uk.org.nominet.iot.registry.dao.dto.PushSubscriberRow;
import uk.org.nominet.iot.registry.model.PushSubscriber;

public class PushSubscriberSummaryDto {
    @JsonInclude(Include.NON_NULL)
    public String dataStream;

    @JsonInclude(Include.NON_NULL)
    public String uri;

    @JsonRawValue
    public String uriParams;

    @JsonInclude(Include.NON_NULL)
    @JsonRawValue
    public String headers;

    @JsonInclude(Include.NON_NULL)
    public String method;

    @JsonInclude(Include.NON_NULL)
    public String retrySequence;

    public PushSubscriberSummaryDto(String dataStream,
                                    String uri,
                                    String uriParams,
                                    String headers,
                                    String method,
                                    String retrySequence) {
        this.dataStream = dataStream;
        this.uri = uri;
        this.uriParams = uriParams;
        this.headers = headers;
        this.method = method;
        this.retrySequence = retrySequence;
    }

    public PushSubscriberSummaryDto(PushSubscriber ps) {
        this.dataStream = ps.getDataStream().getKey().getKey();
        this.uri = ps.getUri();
        this.uriParams = ps.getUriParams();
        this.headers = ps.getHeaders();
        this.method = ps.getMethod();
        this.retrySequence = ps.getRetrySequence();
    }

    public PushSubscriberSummaryDto(PushSubscriberRow ps) {
        this.dataStream = ps.getStreamKey().getKey();
        this.uri = ps.getUri();
        this.uriParams = ps.getUriParams();
        this.headers = ps.getHeaders();
        this.method = ps.getMethod();
        this.retrySequence = ps.getRetrySequence();
    }

    public static List<PushSubscriberSummaryDto> asList(List<PushSubscriber> subs) {
        if (subs == null)
            return null;
        List<PushSubscriberSummaryDto> summaries
            = subs.stream().map(ps -> new PushSubscriberSummaryDto(ps)).collect(Collectors.toList());
        return summaries;
    }

    public static List<PushSubscriberSummaryDto> asListFromRow(List<PushSubscriberRow> subs) {
        if (subs == null)
            return null;
        List<PushSubscriberSummaryDto> summaries = new ArrayList<>();
        for (PushSubscriberRow sub : subs) {
            summaries.add(new PushSubscriberSummaryDto(sub));
        }
        return summaries;
    }

    public boolean refersToSameDbEntity(PushSubscriberSummaryDto rhs) {
        return Objects.equals(dataStream, rhs.dataStream) && Objects.equals(uri, rhs.uri);
        // ... but not comparing uriParams!
    }

    // allow removal of back-reference when serialising as a child of a DataStream
    void clearDataStream() {
        this.dataStream = null;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dataStream == null) ? 0 : dataStream.hashCode());
        result = prime * result + ((headers == null) ? 0 : headers.hashCode());
        result = prime * result + ((method == null) ? 0 : method.hashCode());
        result = prime * result + ((retrySequence == null) ? 0 : retrySequence.hashCode());
        result = prime * result + ((uri == null) ? 0 : uri.hashCode());
        result = prime * result + ((uriParams == null) ? 0 : uriParams.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PushSubscriberSummaryDto other = (PushSubscriberSummaryDto) obj;
        if (dataStream == null) {
            if (other.dataStream != null)
                return false;
        } else if (!dataStream.equals(other.dataStream))
            return false;
        if (headers == null) {
            if (other.headers != null)
                return false;
        } else if (!headers.equals(other.headers))
            return false;
        if (method == null) {
            if (other.method != null)
                return false;
        } else if (!method.equals(other.method))
            return false;
        if (retrySequence == null) {
            if (other.retrySequence != null)
                return false;
        } else if (!retrySequence.equals(other.retrySequence))
            return false;
        if (uri == null) {
            if (other.uri != null)
                return false;
        } else if (!uri.equals(other.uri))
            return false;
        if (uriParams == null) {
            if (other.uriParams != null)
                return false;
        } else if (!uriParams.equals(other.uriParams))
            return false;
        return true;
    }

}
