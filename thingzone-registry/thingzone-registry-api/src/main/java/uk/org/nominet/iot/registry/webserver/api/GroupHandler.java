/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver.api;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import uk.org.nominet.iot.registry.database.DatabaseTransaction;
import uk.org.nominet.iot.registry.database.SimpleDatabaseOperation;
import uk.org.nominet.iot.registry.service.GroupService;
import uk.org.nominet.iot.registry.service.dto.GroupUserDto;
import uk.org.nominet.iot.registry.webserver.api.support.JsonErrorResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSingleResult;
import uk.org.nominet.iot.registry.webserver.api.support.JsonSuccessResult;
import uk.org.nominet.iot.registry.webserver.api.support.PersistenceEffect;
import uk.org.nominet.iot.registry.webserver.api.support.SessionPermissionAssistant;
import uk.org.nominet.iot.registry.webserver.api.support.SessionWrapper;

/**
 * The Class GroupHandler.
 */
@Path("/groups")
public class GroupHandler {
    private final static Logger log = LoggerFactory.getLogger(GroupHandler.class);

    @Context
    Request request;

    @Context
    private javax.servlet.http.HttpServletRequest hsr;

    GroupService groupService;

    public GroupHandler() {
        groupService = new GroupService();
    }

    /**
     * List groups.
     * @param ownedOption If 'owned' is true, then will return groups that the
     *        logged in user created
     * @param allOption If 'all' is true, then will return groups in the system,
     *        requires USER_ADMIN
     * @return an array of group names
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object list(final @QueryParam("owned") Boolean ownedOption,
                       final @QueryParam("all") Boolean allOption) {
        final boolean showOwned = ownedOption != null && ownedOption.booleanValue() ? true : false;
        final boolean showAll = allOption != null && allOption.booleanValue() ? true : false;

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            @Override
            public Object execute() {
                log.info("[groups] owned={}", ownedOption);
                List<String> groupnames = groupService.getGroupNames(spa, showOwned, showAll);
                return new JsonSingleResult("groups", groupnames).output();
            }
        });
    }

    /**
     * Create a group.
     * @param name the name
     * @return success or failure
     */
    @POST
    @Path("/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object createGroup(final @PathParam("name") String name) {
        if (Strings.isNullOrEmpty(name))
            return new JsonErrorResult("missing name").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            @Override
            public Object execute() {
                log.info("[groups/{}]", name);
                groupService.createGroup(spa, name);
                return new JsonSuccessResult();
            }
        });
    }

    /**
     * Update a group.
     * @param groupName the existing group name
     * @param newName the new name
     * @return success or failure
     */
    @PUT
    @Path("/{name}/update")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object update(final @PathParam("name") String groupName,
                         final @FormParam("updatedName") String newName) {
        if (Strings.isNullOrEmpty(newName))
            return new JsonErrorResult("no replacement group name").asResponse(Status.BAD_REQUEST);

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            @Override
            public Object execute() {
                log.info("[groups/{}/update] newName = {}", groupName, newName);
                groupService.updateGroup(spa, groupName, newName);
                return new JsonSuccessResult();
            }
        });
    }

    /**
     * List users in the group.
     * @param groupName the name of the group to list
     * @return list of users and their group admin status
     */
    @GET
    @Path("/{name}/users")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.READ)
    public Object listUsers(final @PathParam("name") String groupName) {

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            @Override
            public Object execute() {
                log.info("[groups/{}/users]", groupName);

                Map<String, Boolean> groupUsers = groupService.listUsers(spa, groupName);
                List<GroupUserDto> users = groupUsers.entrySet().stream()
                                                     .map(e -> new GroupUserDto(e.getKey(), e.getValue()))
                                                     .collect(Collectors.toList());
                return new JsonSingleResult("users", users).output();
            }
        });
    }

    /**
     * Add users to the group.
     *
     * @param groupName the group to add users to
     * @param usernames a comma separated list of usernames
     * @return success or failure
     */
    @PUT
    @Path("/{name}/users/add")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object addUsers(final @PathParam("name") String groupName,
                           final @FormParam("usernames") String usernames) {

        if (Strings.isNullOrEmpty(usernames)) {
            return new JsonErrorResult("no usernames specified").asResponse(Status.BAD_REQUEST);
        }

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            @Override
            public Object execute() {
                log.info("[groups/{}/users/add] users = {}", groupName, usernames);
                groupService.addUsers(spa, groupName, usernames);
                return new JsonSuccessResult();
            }
        });
    }

    /**
     * Remove users from the group.
     *
     * @param groupName the group to remove users from
     * @param usernames string of comma separated user names
     * @return success or failure
     */
    @PUT
    @Path("/{name}/users/remove")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object removeUsers(final @PathParam("name") String groupName,
                              final @FormParam("usernames") String usernames) {
        if (Strings.isNullOrEmpty(usernames)) {
            return new JsonErrorResult("no usernames specified").asResponse(Status.BAD_REQUEST);
        }

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            @Override
            public Object execute() {
                log.info("[groups/{}/users/remove] users = {}", groupName, usernames);
                groupService.removeUsers(spa, groupName, usernames);
                return new JsonSuccessResult();
            }
        });
    }

    /**
     * Set admin status of a user in the group.
     *
     * @param groupName the group the user is in
     * @param username the username
     * @param setAdmin the admin status to set
     * @return success or failure
     */
    @PUT
    @Path("/{name}/admin")
    @Produces(MediaType.APPLICATION_JSON)
    @PersistenceEffect(type = PersistenceEffect.Effect.MODIFY)
    public Object setAdmin(final @PathParam("name") String groupName,
                           final @FormParam("username") String username,
                           final @FormParam("admin") Boolean setAdmin) {
        if (Strings.isNullOrEmpty(username)) {
            return new JsonErrorResult("missing param \"username\"").asResponse(Status.BAD_REQUEST);
        }
        if (setAdmin == null) {
            return new JsonErrorResult("missing param \"admin\"").asResponse(Status.BAD_REQUEST);
        }

        final SessionWrapper sw = new SessionWrapper(hsr);
        if (sw.failed())
            return sw.getError();
        final SessionPermissionAssistant spa = new SessionPermissionAssistant(sw);

        return new DatabaseTransaction().executeInTransaction(new SimpleDatabaseOperation(hsr) {
            @Override
            public Object execute() {
                log.info("[groups/{}/admin] username = {}, admin = {}", groupName, username, setAdmin);
                groupService.setAdmin(spa, groupName, username, setAdmin);
                return new JsonSuccessResult();
            }
        });
    }

}
