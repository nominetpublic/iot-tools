/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.database;

import javax.servlet.http.HttpServletRequest;

public abstract class SimpleDatabaseOperation implements DatabaseOperation {

    HttpServletRequest hsr;
    boolean readOnly;

    public SimpleDatabaseOperation(HttpServletRequest hsr, boolean isReadOnly) {
        this.hsr = hsr;
        this.readOnly = isReadOnly;
    }

    public SimpleDatabaseOperation(HttpServletRequest hsr) {
        this(hsr, false); // default to true for backwards compatibilty
    }

    static class RequestErrorContext {
        public String method;
        public String contextPath;
        public String pathInfo;
        public String characterEncoding;
        public Integer contentLength;
        public String contentType;
        public String protocol;
        // full headers (as a map?)
        // all query params?
        // all post params?

        static public RequestErrorContext create(HttpServletRequest hsr) {
            RequestErrorContext rec = new RequestErrorContext();
            rec.method = hsr.getMethod();
            rec.contextPath = hsr.getContextPath();
            rec.pathInfo = hsr.getPathInfo();
            rec.characterEncoding = hsr.getCharacterEncoding();
            rec.contentLength = hsr.getContentLength();
            rec.contentType = hsr.getContentType();
            rec.protocol = hsr.getProtocol();
            return rec;
        }
    }

    /**
     * returns an object suitable for JSON serialisation, providing error
     * information & context
     */
    @Override
    public Object getErrorContext() {
        if (hsr != null)
            return RequestErrorContext.create(hsr);
        else
            return null;
    }

    /**
     * returns true if operation is (or should be) read-only
     */
    @Override
    public boolean isReadOnly() {
        return readOnly;
    }

}
