/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.dao.dto;

import uk.org.nominet.iot.registry.model.DNSkey;
import uk.org.nominet.iot.registry.model.TransformOutputStream;

public class TransformOutputStreamRow {
    private TransformId transformId;
    private DNSkey streamKey;
    private String alias;

    public TransformOutputStreamRow(TransformId transformId, DNSkey streamKey, String alias) {
        this.transformId = transformId;
        this.streamKey = streamKey;
        this.alias = alias;
    }

    public TransformOutputStreamRow(TransformOutputStream transformOutputStream) {
        this.streamKey = transformOutputStream.getDatastream().getKey();
        this.alias = transformOutputStream.getAlias();
    }

    public TransformId getTransformId() {
        return transformId;
    }

    public DNSkey getStreamKey() {
        return streamKey;
    }

    public String getAlias() {
        return alias;
    }
}
