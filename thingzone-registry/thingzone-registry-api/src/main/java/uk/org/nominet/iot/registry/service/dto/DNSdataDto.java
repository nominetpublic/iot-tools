/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import uk.org.nominet.iot.registry.model.DNSdata;
import uk.org.nominet.iot.registry.model.DNSkey;

import java.util.Objects;

public class DNSdataDto {
    @JsonIgnore
    private DNSkey dnsKey;

    private String description;
    private String subdomain;
    private Integer ttl;
    private String rrtype;
    private String rdata;

    public DNSdataDto() {}

    public DNSdataDto(DNSkey dnsKey, String description, String subdomain, Integer ttl, String rrtype, String rdata) {
        this.dnsKey = dnsKey;
        this.description = description;
        this.subdomain = subdomain;
        this.ttl = ttl;
        this.rrtype = rrtype;
        this.rdata = rdata;
    }

    public DNSdataDto(DNSdata dnsData) {
        this(dnsData.getKey(), dnsData.getDescription(), dnsData.getSubdomain(), dnsData.getTTL(), dnsData.getRRTYPE(),
             dnsData.getRDATA());
    }

    public DNSkey getDnsKey() {
        return dnsKey;
    }

    public String getDescription() {
        return description;
    }

    public String getSubdomain() {
        return subdomain;
    }

    public Integer getTtl() {
        return ttl;
    }

    public String getRrtype() {
        return rrtype;
    }

    public String getRdata() {
        return rdata;
    }

    public static DNSdataDto withDnsKey(DNSdataDto dto, DNSkey key) {
        return new DNSdataDto(key, dto.getDescription(), dto.getSubdomain(), dto.getTtl(), dto.getRrtype(),
                              dto.getRdata());
    }

    public boolean contentsEqual(DNSdata rhs) {
        return Objects.equals(rhs.getKey(), getDnsKey()) && Objects.equals(rhs.getDescription(), getDescription())
            && Objects.equals(rhs.getRDATA(), getRdata()) && Objects.equals(rhs.getRRTYPE(), getRrtype())
            && Objects.equals(rhs.getSubdomain(), getSubdomain()) && Objects.equals(rhs.getTTL(), getTtl());
    }

    public boolean contentsEqual(DNSdataDto rhs) {
        return Objects.equals(rhs.getDnsKey(), getDnsKey()) && Objects.equals(rhs.getDescription(), getDescription())
            && Objects.equals(rhs.getRdata(), getRdata()) && Objects.equals(rhs.getRrtype(), getRrtype())
            && Objects.equals(rhs.getSubdomain(), getSubdomain()) && Objects.equals(rhs.getTtl(), getTtl());
    }

    // items are unique over key/subdomain/rrtype/rdata
    public boolean refersToSameDbEntity(DNSdataDto rhs) {
        return Objects.equals(rhs.getDnsKey(), getDnsKey()) && Objects.equals(rhs.getRdata(), getRdata())
            && Objects.equals(rhs.getRrtype(), getRrtype()) && Objects.equals(rhs.getSubdomain(), getSubdomain());
    }

    @Override
    public String toString() {
        return "DNSdataDto{" + "key=" + dnsKey.toString() + ", description='" + description + '\'' + ", subdomain='"
            + subdomain + '\'' + ", ttl='" + ttl + '\'' + ", rrtype='" + rrtype + '\'' + ", rdata='" + rdata + '\''
            + '}';
    }

}
