/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.webserver;

public enum KeyGenerationType {

    INTERNAL {
        @Override
        public String toString() {
            return INTERNAL_TEXT;
        }
    },
    EXTERNAL {
        @Override
        public String toString() {
            return EXTERNAL_TEXT;
        }
    };

    final static String INTERNAL_TEXT = "internal";
    final static String EXTERNAL_TEXT = "external";

    public static boolean isValid(String s) {
        if (s == null || s.isEmpty())
            return false;

        try {
            fromString(s);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    public static KeyGenerationType fromString(String s) {
        if (s == null || s.isEmpty())
            throw new IllegalArgumentException("KeyGenerationType cannot be empty");

        switch (s.toLowerCase()) { // we allow lower case
        case INTERNAL_TEXT:
            return INTERNAL;
        case EXTERNAL_TEXT:
            return EXTERNAL;
        default:
            throw new IllegalArgumentException("not a valid KeyGenerationType ");
        }
    }

}
