#!/bin/bash
# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

if [ "$RUNNING_IN_DOCKER" == "" ]; then
	make
	RESULT=$?
	if [ $RESULT -ne 0 ]; then
		echo "make failed"
		exit $RESULT
	fi
fi

if [ "$EXTERNAL_KEY_GENERATION" != "" ]; then
 sed -i -e "s/KeyGeneration\=internal/KeyGeneration=external/g" /thingzone-registry/config/web.properties
fi

if [ "$DOCKER_PRIVILEGED_IP" != "" ]; then
 sed -i -e "s/PrivilegedClients\=127\.0\.0\.1/PrivilegedClients=$DOCKER_PRIVILEGED_IP/g" /thingzone-registry/config/web.properties
fi

JAR=./target/thingzone-registry-api-1.0-SNAPSHOT-jar-with-dependencies.jar
CLASSPATH=logback.xml:$JAR

if [ "$FOLLYBRIDGE_LOGBACK_CONFIG" == "" ]; then
	FOLLYBRIDGE_LOGBACK_CONFIG=logback.xml
fi
LOGBACK=-Dlogback.configurationFile=$FOLLYBRIDGE_LOGBACK_CONFIG

echo LOGBACK=$LOGBACK

IPV4_ONLY=-Djava.net.preferIPv4Stack=true

JMX_OPTS="-Dcom.sun.management.jmxremote -Djava.rmi.server.hostname=localhost -Dcom.sun.management.jmxremote.port=1099 -Dcom.sun.management.jmxremote.rmi.port=1099 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.local.only=false"

#java -javaagent:$JAR -cp $CLASSPATH $LOGBACK $JMX_OPTS $IPV4_ONLY uk.org.nominet.iot.registry.webserver.WebServerApp
java -cp $CLASSPATH $LOGBACK $JMX_OPTS $IPV4_ONLY uk.org.nominet.iot.registry.webserver.WebServerApp

