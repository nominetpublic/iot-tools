#!/bin/bash
# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#export PGPASSWORD='e2e_registry_rwx'; psql -lqt -U e2e_registry_rw -h postgres | cut -d \| -f 1 | grep -qw e2e_registry
set -e

cmd="$@"

until PGPASSWORD=$POSTGRES_PASSWORD psql -lqt -U e2e_registry_rw -h postgres | cut -d \| -f 1 | grep -qw e2e_registry; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "Postgres is up - executing command"

exec $cmd
