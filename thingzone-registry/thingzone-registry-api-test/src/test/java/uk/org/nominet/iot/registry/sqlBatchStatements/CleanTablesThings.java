/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.sqlBatchStatements;

import java.util.ArrayList;
import java.util.List;

public class CleanTablesThings {
    public static List<String> statements = new ArrayList<>();

    static {
        statements.add("DELETE FROM thingzone.PushSubscribers;");
        statements.add("DELETE FROM thingzone.PullSubscriptions;");
        statements.add("DELETE FROM thingzone.TransformSources;");
        statements.add("DELETE FROM thingzone.transform_outputstreams;");
        statements.add("DELETE FROM thingzone.Transforms;");
        statements.add("DELETE FROM thingzone.TransformFunctions;");
        statements.add("DELETE FROM thingzone.StreamsOnDevices;");
        statements.add("DELETE FROM thingzone.DataStreams;");
        statements.add("DELETE FROM thingzone.Devices;");
        statements.add("DELETE FROM thingzone.Resources;");
        statements.add("DELETE FROM thingzone.DNSdata;");
        statements.add("DELETE FROM thingzone.ZoneFileChangesTransactions;");
        statements.add("DELETE FROM thingzone.ZoneFileChanges;");
        statements.add("DELETE FROM thingzone.ACLs;");
        statements.add("DELETE FROM thingzone.DNSkeys;");
        statements.add("DELETE FROM thingzone.Sessions;");
        statements.add("DELETE FROM thingzone.History;");
        statements.add("DELETE FROM thingzone.groups_users;");
        statements.add("DELETE FROM thingzone.user_tokens;");
    }
}
