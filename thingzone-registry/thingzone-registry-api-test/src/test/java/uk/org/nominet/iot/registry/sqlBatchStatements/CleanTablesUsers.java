/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry.sqlBatchStatements;

import java.util.ArrayList;
import java.util.List;

public class CleanTablesUsers {

    public static List<String> statements = new ArrayList<>();
    
    static {
        statements.add("DELETE FROM thingzone.UserPrivileges where user_id NOT IN " +
                       "   (SELECT u.id  " +
                       "   FROM thingzone.user_credentials uc, thingzone.users u " +
                       "   WHERE uc.username IN ('user_admin') AND  uc.user_id=u.id);");
        statements.add("DELETE FROM thingzone.Accounts where owner_id NOT IN " +
                       "      (SELECT u.id " +
                       "         FROM thingzone.user_credentials uc, thingzone.users u " +
                       "         WHERE uc.username IN ('public', 'user_admin') AND  uc.user_id=u.id) " +
                       "   OR group_id NOT IN  " +
                       "      (SELECT g.id " +
                       "         FROM thingzone.groups g " +
                       "         WHERE g.name = 'all_users');");
        statements.add("DELETE FROM thingzone.groups WHERE name <> 'all_users';");
        statements.add("DELETE FROM thingzone.User_Credentials WHERE username NOT IN ('public', 'user_admin');");
        statements.add("DELETE FROM thingzone.Users where id NOT IN " +
                       "   (SELECT user_id FROM thingzone.user_credentials);");
    }
}
