/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry;

import org.junit.ClassRule;
import uk.nominet.iot.test.docker.compose.ThingzoneComposeRuntimeContainer;
import uk.nominet.iot.test.docker.compose.ThingzoneEnvironment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import static uk.nominet.iot.test.docker.compose.ThingzoneEnvironment.SERVICE_POSTGRES;
import static uk.nominet.iot.test.docker.compose.ThingzoneEnvironment.SERVICE_REGISTRY;

public class RegistryAPITestBase {
    public static boolean RUN_WITH_DOCKER = true;

    @ClassRule
    public static ThingzoneComposeRuntimeContainer environment = (RUN_WITH_DOCKER) ? new ThingzoneEnvironment().withServices(
            SERVICE_POSTGRES).withServices(SERVICE_REGISTRY).getContainer() : null;

    public void updateDB(List<String> statements) throws SQLException {
        String url = (RUN_WITH_DOCKER) ? String.format("jdbc:postgresql://%s:%d/e2e_registry",
                                   environment.getServiceHost(SERVICE_POSTGRES),
                                   environment.getServicePort(SERVICE_POSTGRES)) :
                     "jdbc:postgresql://localhost:5432/e2e_registry";

        String user = "e2e_registry_rw";
        String password = "e2e_registry_rwx";


        Connection con = DriverManager.getConnection(url, user, password);
        Statement st = con.createStatement();

        for (String statement : statements) {
            st.addBatch(statement);
        }

        st.executeBatch();
    }
}
