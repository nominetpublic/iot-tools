/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry;

import org.junit.*;
import org.junit.rules.Timeout;
import uk.org.nominet.iot.registry.sqlBatchStatements.CleanTablesThings;
import uk.org.nominet.iot.registry.sqlBatchStatements.CleanTablesUsers;
import uk.org.nominet.iot.registry.sqlBatchStatements.ResetAdminUserPassword;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;


import static org.junit.Assert.*;
import static uk.nominet.iot.test.docker.compose.ThingzoneEnvironment.*;

public class APIPerlTest extends RegistryAPITestBase {


    @Rule
    public Timeout globalTimeout = Timeout.seconds(20);

    public Process runPerlTestUnit(String unitName) throws IOException, InterruptedException {
        String registryURL = (RUN_WITH_DOCKER) ? environment.getServiceHost(SERVICE_REGISTRY) : "localhost";
        int registryPort = (RUN_WITH_DOCKER) ? environment.getServicePort(SERVICE_REGISTRY) : 8081;
        final ProcessBuilder processBuilder = new ProcessBuilder("perl",
                                                    String.format("./%s.pl", unitName),
                                                                 registryURL,
                                                                 ""+registryPort);
        processBuilder.directory( new File("api_test"));
        processBuilder.redirectInput(ProcessBuilder.Redirect.INHERIT);
        processBuilder.redirectOutput(ProcessBuilder.Redirect.INHERIT);
        processBuilder.redirectError(ProcessBuilder.Redirect.INHERIT);

        Process process = processBuilder.start();
        process.waitFor();
        return process;
    }



    @Before
    public void setUp() throws Exception {
        updateDB(CleanTablesThings.statements);
        updateDB(CleanTablesUsers.statements);
    }

    @Test
    public void test_session() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_session");
        assertEquals(0, testProcess.exitValue());
    }


    @Test
    public void test_user() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_user");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_permission() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_permission");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_privileges() throws IOException, InterruptedException, SQLException {
        updateDB(ResetAdminUserPassword.statements);
        Process testProcess = runPerlTestUnit("test_privileges");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_dataStream() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_dataStream");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_dataStream_permission() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_dataStream_permission");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_dataStream_upsert() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_dataStream_upsert");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_publicuser() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_publicuser");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_device() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_device");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_device_permission() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_device_permission");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_device_upsert() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_device_upsert");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_streamOnDevice() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_streamOnDevice");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_streamOnDevice_permission() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_streamOnDevice_permission");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_transforms() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_transforms");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_transforms_permission() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_transforms_permission");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_transform_upsert() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_transform_upsert");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_dnsdata() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_dnsdata");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_dnsdata_permission() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_dnsdata_permission");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_pull_subscriptions() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_pull_subscriptions");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_pull_subscriptions_permission() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_pull_subscriptions_permission");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_push_subscribers_permission() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_push_subscribers_permission");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_details() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_details");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_details_permission() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_details_permission");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_dataStream_extkey() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_dataStream_extkey");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_device_extkey() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_device_extkey");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_dump() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_dump");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_internal() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_internal");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_internal_transform() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_internal_transform");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_entity() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_entity");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_resource() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_resource");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_group() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_group");
        assertEquals(0, testProcess.exitValue());
    }

    @Test
    public void test_group_permission() throws IOException, InterruptedException {
        Process testProcess = runPerlTestUnit("test_group_permission");
        assertEquals(0, testProcess.exitValue());
    }

}
