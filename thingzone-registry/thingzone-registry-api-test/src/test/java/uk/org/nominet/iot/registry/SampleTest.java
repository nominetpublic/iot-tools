/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.nominet.iot.registry;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.specification.RequestSpecification;
import org.junit.*;
import uk.org.nominet.iot.registry.sqlBatchStatements.CleanTablesThings;
import uk.org.nominet.iot.registry.sqlBatchStatements.CleanTablesUsers;


import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static uk.nominet.iot.test.docker.compose.ThingzoneEnvironment.SERVICE_REGISTRY;

@Ignore
public class SampleTest extends RegistryAPITestBase {
    private static RequestSpecification spec;

    @BeforeClass
    public static void setUpClass() throws Exception {
        String registryURL = (RUN_WITH_DOCKER) ? environment.getServiceHost(SERVICE_REGISTRY) : "localhost";
        int registryPort = (RUN_WITH_DOCKER) ? environment.getServicePort(SERVICE_REGISTRY) : 8081;

        spec = new RequestSpecBuilder()
                       .setBaseUri(String.format("http://%s:%d/api",
                                                 registryURL,
                                                 registryPort))
                       .addFilter(new ResponseLoggingFilter())//log request and response for better debugging. You can also only log if a requests fails.
                       .addFilter(new RequestLoggingFilter())
                       .build();
    }

    @Before
    public void setUp() throws Exception {
        updateDB(CleanTablesThings.statements);
        updateDB(CleanTablesUsers.statements);
    }

    @Test
    public void canGetLoginSession() {
        given()
                .spec(spec)
                .header("Content-Type", "application/x-www-form-urlencoded")
                .header("Accept", "*")
                .formParam("username", "user_admin")
                .formParam("password", "user_admin")
                .when()
                .post("/session/login")
                .then()
                .statusCode(200)
                .header("Content-Type", "application/json")
                .body("result", equalTo("ok"));
                //.body("sessionKey", equalTo(2));
    }

}
