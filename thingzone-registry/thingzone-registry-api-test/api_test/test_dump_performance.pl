# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use Data::Dumper;
use POSIX qw(strftime);
use DateTime;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;

my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";

(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

my $json;
my $sessionKey_userAdmin=ThingzoneLogin::login_admin_user() or die;

#============================================================================
# test prerequisites

sleep(2);
my $all_changes = strftime "%Y-%m-%dT%H:%M:%S%z", localtime;

# test user 1
my $unique = time();
my $username1 = "user_device_dump_1_$unique";
my $password1 = "password_$unique";
my $username2 = "user_device_dump_2_$unique";
my $password2 = "password_$unique";

# create 2x users
$json = HttpHelper::post (
    desc  => "Create user 1 ($username1)",
    path  => '/user/create',
    params=> {username=>$username1, password=>$password1, privileges=>"CREATE_DEVICE, CREATE_DATASTREAM, CREATE_TRANSFORM"},
    sessionKey=>$sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "Create user 2 ($username2)",
    path  => '/user/create',
    params=> {username=>$username2, password=>$password2, privileges=>"CREATE_DEVICE, CREATE_DATASTREAM, CREATE_TRANSFORM"},
    sessionKey=>$sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# create users for dump
$json = HttpHelper::post (
    desc  => "Create user dumper",
    path  => '/user/create',
    params=> {username=>'dumper', password=>'dumper', privileges=>"DUMP_ALL"},
    sessionKey=>$sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# log in 2x users
$json = HttpHelper::post (
    desc  => "Log in as user 1 ($username1)",
    path  => '/session/login',
    params=> {username=>$username1, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user1=$$json{sessionKey};


$json = HttpHelper::post (
    desc  => "Log in as user 2 ($username2)",
    path  => '/session/login',
    params=> {username=>$username2, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user2=$$json{sessionKey};


$json = HttpHelper::post (
    desc  => "Log in as user 'dumper'",
    path  => '/session/login',
    params=> {username=>'dumper', password=>'dumper'}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_dumper=$$json{sessionKey};

#============================================================================

my @datastreams = ();
for (my $i=0; $i <= 50000; $i++) {
    $json = HttpHelper::post (
        desc  => "create datastream $i",
        path  => "/datastream/create",
        params=>{},
        sessionKey=>$sessionKey_user1
    );
    HttpHelper::fail("fail") unless $$json{result} eq 'ok';
    push @datastreams, $$json{dataStream};
}

my @devices = ();
for (my $i=0; $i <= 50000; $i++) {
    $json = HttpHelper::post (
        desc  => "create device $i",
        path  => "/device/create",
        params=> {},
        sessionKey=>$sessionKey_user1
    );
    HttpHelper::fail("fail") unless $$json{result} eq 'ok';
    push @devices, $$json{device};
}


#=============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

