#!/usr/bin/perl -w
use strict;

package HttpHelper;


# helper functions that sent HTTP request, and decode JSON response

sub init($);        # base URL
sub set_header($$); # key, value

sub put($@); # path, params
#sub post($$);	# path, params
sub get($@); # path, params


#########################################

use LWP::UserAgent; # RPM perl-libwww-perl
use JSON;           # RPM perl-JSON
use Data::Dumper;
use HTTP::Request::Common qw(PUT POST);

our $ua;
our $base_url;
our $testClass;
our $current_test_description;

sub init($) {
    $base_url = shift or die;
    $ua = LWP::UserAgent->new();
}

sub set_testClass($) {
    $testClass = shift or die;
}

sub set_header($$) {
    my $key = shift or die;
    my $value = shift or die;
    $ua->default_header($key => $value);
}


my $DIVIDER = "\n--------------------------------------------------------------\n";


#########################################
sub startTest($) {
    $current_test_description = shift or die;
    print "\n##teamcity[testStarted name='$HttpHelper::testClass.$current_test_description' captureStandardOutput='true']\n";
}

sub endTest {
    print "\n##teamcity[testFinished name='$HttpHelper::testClass.$current_test_description']\n";
}

sub fail($) {
    print "\n##teamcity[testFailed  name='$HttpHelper::testClass.$current_test_description' message='$_[0]']\n"
}

sub out($) {
	my $output = $_[0];
	print "\n##teamcity[testStdOut name='$HttpHelper::testClass.$current_test_description' out='$output \\n']\n"
}

sub err($) {
    print "\n##teamcity[testStdErr name='$HttpHelper::testClass.$current_test_description' out='$_[0]']\n"
}

sub post {
    # can receive either:
    #  (old style):
    #    path (scalar) + params (hash)
    # or
    #  (new style):
    #    hash containing "desc", "path", "params" (optionally, "sessionKey")

    my $test_description;
    my $path;
    my $params;
    my $sessionKey;

    my %detect = @_;
    if (defined $detect{'desc'} && defined $detect{'path'} && defined $detect{'params'}) {
        $test_description = $detect{'desc'};
        $path = $detect{'path'} or die;
        $params = $detect{'params'} or die;
        $sessionKey = $detect{'sessionKey'}; # or undef
    }
    else {
        $path = shift or die;
        $params = shift or die;
        $test_description = $path;
    }
    my $previous_sessionKey = $ua->default_header('X-FollyBridge-SessionKey');
    if ($sessionKey) {
        $ua->default_header('X-FollyBridge-SessionKey' => $sessionKey);
    }

    print $DIVIDER;
    print "Test: $test_description\n" if $test_description;
    startTest($test_description);

    my $response = just_post($path, $params);

    $ua->default_header('X-FollyBridge-SessionKey' => $previous_sessionKey);
    die $response->status_line if $response->code eq '500';
    print $response->status_line . "\n" unless $response->is_success;

    print "response content-type: \"" . $response->header('content_type') . "\n";

    print "response: \"" . $response->decoded_content . "\"\n";
    out "response: \"" . $response->decoded_content . "\"";

    endTest;
    my $json = decode_json_response($response->decoded_content, $response->code) or die "failed to decode";
    return $json;
}

sub just_post($@) {
    my $path = shift or die;
    my $params = shift or die;

    print "POST request: \"$path\"\n";
    return $ua->post("${base_url}$path", $params);
}


sub put($@) {
    # can receive either:
    #  (old style):
    #    path (scalar) + params (hash)
    # or
    #  (new style):
    #    hash containing "desc", "path", "params" (optionally, "sessionKey")

    my $test_description;
    my $path;
    my $params;
    my $sessionKey;

    my %detect = @_;
    if (defined $detect{'desc'} && defined $detect{'path'} && defined $detect{'params'}) {
        $test_description = $detect{'desc'};
        $path = $detect{'path'} or die;
        $params = $detect{'params'} or die;
        $sessionKey = $detect{'sessionKey'}; # or undef
    }
    else {
        $path = shift or die;
        $params = shift or die;
        $test_description = $path;
    }
    my $previous_sessionKey = $ua->default_header('X-FollyBridge-SessionKey');
    if ($sessionKey) {
        $ua->default_header('X-FollyBridge-SessionKey' => $sessionKey);
    }

    print $DIVIDER;
    print "Test: $test_description\n" if $test_description;
	startTest($test_description);
    print "PUT request: \"$path\"\n";

    # this is a hack but the only way I could make it work
    my $req = POST("${base_url}$path", 'Content' => $params);
    $req->method('PUT');
    my $response = $ua->request($req);
    $ua->default_header('X-FollyBridge-SessionKey' => $previous_sessionKey);


    #	print Dumper($response);
    die $response->status_line if $response->code eq '500';
    print $response->status_line . "\n" unless $response->is_success;

    print "response content-type: \"" . $response->header('content_type') . "\n";
    print "response: \"" . $response->decoded_content . "\"\n";
    out "response: \"" . $response->decoded_content . "\"";

	endTest;
    my $json = decode_json_response($response->decoded_content, $response->code) or die "failed to decode";
    return $json;
}


sub get($@) {
    # can receive either:
    #  (old style):
    #    path (scalar) + params (hash)
    # or
    #  (new style):
    #    hash containing "desc", "path", "params" (optionally, "sessionKey")

    my $test_description;
    my $path;
    my $params;
    my $sessionKey;
    my $print_raw_json;
    my $return_response;

    my %detect = @_;
    if (defined $detect{'desc'} && defined $detect{'path'} && defined $detect{'params'}) {
        $test_description = $detect{'desc'};
        $path = $detect{'path'} or die;
        $params = $detect{'params'} or die;
        $sessionKey = $detect{'sessionKey'};           # or undef
        $print_raw_json = $detect{'print_raw_json'};   # or undef
        $return_response = $detect{'return_response'}; # or undef
    }
    else {
        $path = shift or die;
        $params = shift or die;
        $test_description = $path;
    }
    my $previous_sessionKey = $ua->default_header('X-FollyBridge-SessionKey');
    if ($sessionKey) {
        $ua->default_header('X-FollyBridge-SessionKey' => $sessionKey);
    }

    print $DIVIDER;
    print "Test: $test_description\n" if $test_description;
	startTest($test_description);
    print "GET request: \"$path\"\n";
    my $url = URI->new("${base_url}$path");
    $url->query_form(%$params);

    print $url->as_string . "\n";

    my $response = $ua->get($url);
    $ua->default_header('X-FollyBridge-SessionKey' => $previous_sessionKey);

    #	print Dumper($response);
    die $response->status_line if $response->code eq '500';
    print $response->status_line . "\n" unless $response->is_success;
    print "response Content-Type: \"" . $response->header('Content-Type') . "\"\n";

    if ($print_raw_json) {
        print "raw response ==>\n";
        print $response->decoded_content;
        out $response->decoded_content;
        print "\n";
    }
    if ($return_response) {
		endTest;
        return $response;
    }

	endTest;
    return decode_json_response($response->decoded_content, $response->code);
}

sub delete($@) {
    # can receive either:
    #  (old style):
    #    path (scalar) + params (hash)
    # or
    #  (new style):
    #    hash containing "desc", "path", "params" (optionally, "sessionKey")

    my $test_description;
    my $path;
    my $params;
    my $sessionKey;

    my %detect = @_;
    if (defined $detect{'desc'} && defined $detect{'path'} && defined $detect{'params'}) {
        $test_description = $detect{'desc'};
        $path = $detect{'path'} or die;
        $params = $detect{'params'} or die;
        $sessionKey = $detect{'sessionKey'}; # or undef
    }
    else {
        $path = shift or die;
        $params = shift or die;
        $test_description = $path;
    }
    my $previous_sessionKey = $ua->default_header('X-FollyBridge-SessionKey');
    if ($sessionKey) {
        $ua->default_header('X-FollyBridge-SessionKey' => $sessionKey);
    }

    # this is a hack but the only way I could make it work

    print $DIVIDER;
    print "Test: $test_description\n" if $test_description;
	startTest($test_description);
    print "DELETE request: \"$path\"\n";
    my $url = URI->new("${base_url}$path");
    $url->query_form(%$params);
    #	print $url->as_string."\n";

    my $response = $ua->delete($url);
    $ua->default_header('X-FollyBridge-SessionKey' => $previous_sessionKey);

    #	print Dumper($response);
    die $response->status_line if $response->code eq '500';
    print $response->status_line . "\n" unless $response->is_success;
    print "response Content-Type: \"" . $response->header('Content-Type') . "\"\n";
    out "response Content-Type: \"" . $response->header('Content-Type') . "\"";

	endTest;
    return decode_json_response($response->decoded_content, $response->code);
}


sub decode_json_response {
    my $raw = shift or die;
    my $status = shift or die;

    my $decoded_json;
    eval {$decoded_json = decode_json($raw)};
    if ($@) {
        die "failed to decode JSON:\n" . $raw;
    }
    if (ref($decoded_json) eq 'HASH') {
        $$decoded_json{status_code} = $status;
    }
    print "response JSON=" . to_json($decoded_json, { utf8 => 1, pretty => 1 }) . "\n";

    return $decoded_json;
}


sub put_multipart {
    # can receive either:
    #  (old style):
    #    path (scalar) + parts (array, each part is an HTTP::Message)
    # or
    #  (new style):
    #    hash containing "desc", "path", "parts" (optionally, "sessionKey")

    my $test_description;
    my $path;
    my $parts;
    my $sessionKey;

    my %detect = @_;
    if (defined $detect{'desc'} && defined $detect{'path'} && defined $detect{'parts'}) {
        $test_description = $detect{'desc'};
        $path = $detect{'path'} or die;
        $parts = $detect{'parts'} or die;
        $sessionKey = $detect{'sessionKey'}; # or undef
    }
    else {
        $path = shift or die;
        $parts = shift or die;
        $test_description = $path;
    }
    my $previous_sessionKey = $ua->default_header('X-FollyBridge-SessionKey');
    if ($sessionKey) {
        $ua->default_header('X-FollyBridge-SessionKey' => $sessionKey);
    }

    print $DIVIDER;
    print "Test: $test_description\n" if $test_description;
	startTest($test_description);
    print "multipart PUT: \"$path\"\n";
    my $req = HTTP::Request->new(PUT => "${base_url}$path");
    $req->content_type('multipart/form-data');

    for my $part (@$parts) {
        $req->add_part($part);
    }

    if (!defined $detect{'hide_content'}) {
        print "Request:\n" . $req->as_string . "\n";
    }

    my $response = $ua->request($req);
    $ua->default_header('X-FollyBridge-SessionKey' => $previous_sessionKey);

    die $response->status_line if $response->code eq '500';
    print $response->status_line . "\n" unless $response->is_success;

    print "response content-type: \"" . $response->header('content_type') . "\"\n";
	endTest;
    return decode_json_response($response->decoded_content, $response->code);
}

sub post_multipart {
    # can receive either:
    #  (old style):
    #    path (scalar) + parts (array, each part is an HTTP::Message)
    # or
    #  (new style):
    #    hash containing "desc", "path", "parts" (optionally, "sessionKey")

    my $test_description;
    my $path;
    my $parts;
    my $sessionKey;

    my %detect = @_;
    if (defined $detect{'desc'} && defined $detect{'path'} && defined $detect{'parts'}) {
        $test_description = $detect{'desc'};
        $path = $detect{'path'} or die;
        $parts = $detect{'parts'} or die;
        $sessionKey = $detect{'sessionKey'}; # or undef
    }
    else {
        $path = shift or die;
        $parts = shift or die;
        $test_description = $path;
    }
    my $previous_sessionKey = $ua->default_header('X-FollyBridge-SessionKey');
    if ($sessionKey) {
        $ua->default_header('X-FollyBridge-SessionKey' => $sessionKey);
    }

    print $DIVIDER;
    print "Test: $test_description\n" if $test_description;
	startTest($test_description);
    print "\nmultipart POST: \"$path\"\n";
    my $req = HTTP::Request->new(POST => "${base_url}$path");
    $req->content_type('multipart/form-data');

    for my $part (@$parts) {
        $req->add_part($part);
    }

    if (!defined $detect{'hide_content'}) {
        print "Request:\n" . $req->as_string . "\n";
    }

    my $response = $ua->request($req);
    $ua->default_header('X-FollyBridge-SessionKey' => $previous_sessionKey);

    die $response->status_line if $response->code eq '500';
    print $response->status_line . "\n" unless $response->is_success;

    print "response content-type: \"" . $response->header('content_type') . "\"\n";
	endTest;
    return decode_json_response($response->decoded_content, $response->code);
}


sub create_multiparts {
    my @part_specs = @_;
    my @message_parts;

    for my $part (@part_specs) {
        my $part_name = ${$part}{name};
        my $part_value = ${$part}{value};
        my $part_content_type = ${$part}{content_type};
        $part_content_type = 'text/plain; charset=UTF-8' unless $part_content_type; # default

        print "name=$part_name\n";
        if (!defined ${$part}{hide_content}) {
            print "value=$part_value\n";
        }
        print "content_type=$part_content_type\n";

        push @message_parts, HTTP::Message->new([
            'Content-Type'        => "$part_content_type",
            'Content-Disposition' => "form-data; name=\"$part_name\"",
        ], $part_value);
    }
    return \@message_parts;
}

1;

