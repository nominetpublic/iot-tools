# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;


my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";

(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

my $json;
# login
my %login1 = ThingzoneLogin::login_section('transforms');
my $sessionKey_user1 = $login1{sessionKey} or die;

#============================================================================
# Transform Functions
my $unique = time();
my $function_name = "function_$unique";
my $owner_name = "owner_$unique";

# create a transform function
$json = HttpHelper::put_multipart ("/transform/create_function", HttpHelper::create_multiparts(
    {name=>'name', value=>$function_name},
    {name=>'functionType', value=>'javascript'},
    {name=>'functionContent', value=>"console.log('hello world')"})
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# get the TransformFunction we just created
$json = HttpHelper::get ("/transform/get_function", {'name' => $function_name});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

my $function2_name = "function2_$unique";
$json = HttpHelper::put_multipart ("/transform/create_function", HttpHelper::create_multiparts(
    {name=>'name', value=>$function2_name},
    {name=>'functionType', value=>'javascript'},
    {name=>'functionContent', value=>"console.log('hello world')"})
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# cannot create pre-existing function
$json = HttpHelper::put_multipart ("/transform/create_function", HttpHelper::create_multiparts(
    {name=>'name', value=>$function2_name},
    {name=>'functionType', value=>'javascript'},
    {name=>'functionContent', value=>"console.log('hello world')"})
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /exists already for user/;
HttpHelper::fail("fail") unless $$json{status_code} eq '409';

$json = HttpHelper::put_multipart (
    desc => "user 1 creates function",
    path => "/transform/create_function",
    parts=> HttpHelper::create_multiparts(
        {name=>'name', value=>'testfn'},
        {name=>'functionType', value=>'javascript'},
        {name=>'functionContent', value=>"console.log('test')"}),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# delete one that doesn't match
$json = HttpHelper::delete ("/transform/delete_function", {'name'=>'who me?', "foo" => "bar"});
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

# delete
$json = HttpHelper::delete ("/transform/delete_function", {'name'=>$function2_name});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::get ("/transform/list_functions", {});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{transformFunctions}} == 2;


#============================================================================

# create a transform
$json = HttpHelper::post_multipart ("/transform/create", HttpHelper::create_multiparts(
    {name=>'functionName', value=>$function_name},
    {name=>'parameterValues', content_type=>'application/json', value=>q#{}#},
    {name=>'runSchedule', value=>"not today"},
    ));
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $transform=$$json{transform};
HttpHelper::fail("fail") unless $transform;

$json = HttpHelper::get(
    desc  => "get the Transform we just created.",
    path  => "/transform/get",
    params=> {'transform' => $transform, 'detail' => 'full'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{transform};
HttpHelper::fail("fail") unless $$json{transform}{key};
HttpHelper::fail("fail") unless $$json{transform}{key} eq $transform;
HttpHelper::fail("fail") unless defined $$json{transform}{runSchedule};
HttpHelper::fail("fail") unless $$json{transform}{runSchedule} eq "not today";

# get a non-existent Transform
$json = HttpHelper::get ("/transform/get", {'transform' => 'abde.etc'});
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /Transform not found/;
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

# list Transforms
$json = HttpHelper::get ("/transform/list", {});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


#============================================================================
# Transform Parameters

#
$json = HttpHelper::put_multipart ("/transform/update_parameters", HttpHelper::create_multiparts(
    {name=>'transform', value=>$transform},
    {name=>'parameterValues', content_type=>'application/json', value=>q#{"value":"foo"}#},
    ));
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put_multipart ("/transform/update_parameters", HttpHelper::create_multiparts(
    {name=>'transform', value=>$transform},
    {name=>'parameterValues', content_type=>'application/json', value=>q#{"value":"overwritten"}#},
    ));
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# test with invalid input
$json = HttpHelper::put_multipart ("/transform/update_parameters", HttpHelper::create_multiparts(
    {name=>'transform', value=>$transform},
    {name=>'parameterValues', content_type=>'application/json', value=>q#{{ bogus json#},
    ));
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /invalid input syntax for type json/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';


#============================================================================
# Transform Sources

$json = HttpHelper::post ("/datastream/create", {});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream1=$$json{dataStream};

$json = HttpHelper::post ("/datastream/create", {});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream2=$$json{dataStream};


# add a source stream (no trigger)
$json = HttpHelper::put ("/transform/update_source", {'transform' => $transform, 'stream' => $dataStream1, 'alias' => 'a42'});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# add another source stream
$json = HttpHelper::put ("/transform/update_source", {'transform' => $transform, 'stream' => $dataStream1, 'alias' => 'a43'}, "trigger" => "true");
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# overwrite first source stream
$json = HttpHelper::put ("/transform/update_source", {'transform' => $transform, 'stream' => $dataStream1, 'alias' => 'a42'}, "trigger" => "true");
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc  => "expect 2 sources",
    path  => "/transform/get",
    params=> {'transform' => $transform, 'detail'=>'full'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{transform}{sources}} == 2;

# remove source
$json = HttpHelper::put ("/transform/update_source", {'transform' => $transform, 'alias' => 'a43'});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc  => "expect 1 source",
    path  => "/transform/get",
    params=> {'transform' => $transform, 'detail' => 'full'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{transform}{sources}} == 1;

$json = HttpHelper::put (
    desc  => "set aggregation values",
    path  => "/transform/update_source",
    params => { 'transform' => $transform, 'stream' => $dataStream1, 'alias' => 'a42', 'trigger' => 'true', 'aggregationType' => 'type', 'aggregationSpec' => 'spec' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc   => "check update streams works with aggregation data",
    path   => "/transform/get",
    params => { 'transform' => $transform, 'detail' => 'full' },
    sessionKey=>$sessionKey_user1);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{transform}{sources}} == 1;
HttpHelper::fail("fail") unless $$json{transform}{sources}[0]{aggregationType} eq "type";
HttpHelper::fail("fail") unless $$json{transform}{sources}[0]{aggregationSpec} eq "spec";

$json = HttpHelper::put (
    desc  => "set incorrect aggregation values",
    path  => "/transform/update_source",
    params => { 'transform' => $transform, 'stream' => $dataStream1, 'alias' => 'a42', 'trigger' => 'true', 'aggregationType' => '', 'aggregationSpec' => 'spec' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /must be set/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put (
    desc  => "aggregation values empty",
    path  => "/transform/update_source",
    params => { 'transform' => $transform, 'stream' => $dataStream1, 'alias' => 'a42', 'trigger' => 'true', 'aggregationType' => '', 'aggregationSpec' => ''  },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /are empty/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put (
    desc  => "unset the aggregation values",
    path  => "/transform/update_source",
    params => { 'transform' => $transform, 'stream' => $dataStream1, 'alias' => 'a42', 'trigger' => 'true' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc   => "check aggregation data has been removed",
    path   => "/transform/get",
    params => { 'transform' => $transform, 'detail' => 'full' },
    sessionKey=>$sessionKey_user1);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{transform}{sources}} == 1;
HttpHelper::fail("fail") if (defined $$json{transform}{sources}[0]{aggregation});

#============================================================================
# update outputStream

$json = HttpHelper::put (
    desc  => "update outputStream with a stream that is already a source",
    path  => "/transform/update_outputStream",
    params => { 'transform' => $transform, 'stream' => $dataStream1, 'alias' => 'stream1' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put (
    desc  => "remove source",
    path  => "/transform/update_source",
    params => { 'transform' => $transform, 'alias' => 'a42'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc  => "add outputStream",
    path  => "/transform/update_outputStream",
    params => { 'transform' => $transform, 'stream' => $dataStream1, 'alias' => 'stream1' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc  => "add duplicate outputStream",
    path  => "/transform/update_outputStream",
    params => { 'transform' => $transform, 'stream' => $dataStream1, 'alias' => 'stream2' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '409';

$json = HttpHelper::put (
    desc  => "update source with stream that is already an output",
    path  => "/transform/update_source",
    params => { 'transform' => $transform, 'stream' => $dataStream1, 'alias' => 'a42', 'trigger' => 'true' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put (
    desc  => "remove outputStream",
    path  => "/transform/update_outputStream",
    params => { 'transform' => $transform, 'alias' => 'stream1'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

#============================================================================
# update function - set a different function

# set public function
$json = HttpHelper::put ("/transform/update_function", {'transform' => $transform, 'functionName'=>'testfn'});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# create another function
my $function3_name = "function3_$unique";
$json = HttpHelper::put_multipart ("/transform/create_function", HttpHelper::create_multiparts(
    {name=>'name', value=>$function3_name},
    {name=>'owner', value=>$owner_name},
    {name=>'functionType', value=>'javascript'},
    {name=>'functionContent', value=>"console.log('hello world')"})
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# set the new function
$json = HttpHelper::put ("/transform/update_function", {'transform' => $transform, 'functionName'=>$function3_name});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# check
$json = HttpHelper::get (
    desc   => "check new function",
    path   => "/transform/get",
    params => { 'transform' => $transform, 'detail' => 'full' },
    sessionKey=>$sessionKey_user1);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{transform}{function}{name} eq $function3_name;

#============================================================================
# update runSchedule

$json = HttpHelper::put (
    desc  => "set runSchedule",
    path  => "/transform/update_runSchedule",
    params=> { transform => "$transform", runSchedule => 'twice nightly' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# read back
$json = HttpHelper::get (
    desc   => "get transform",
    path   => "/transform/get",
    params => { 'transform' => $transform },
    sessionKey=>$sessionKey_user1);
HttpHelper::fail("fail") unless defined $$json{transform}{runSchedule};
HttpHelper::fail("fail") unless $$json{transform}{runSchedule} eq "twice nightly";


$json = HttpHelper::put (
    desc  => "clear runSchedule",
    path  => "/transform/update_runSchedule",
    params=> { transform => "$transform" },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# read back
$json = HttpHelper::get (
    desc   => "get transform",
    path   => "/transform/get",
    params => { 'transform' => $transform },
    sessionKey=>$sessionKey_user1);
HttpHelper::fail("fail") unless defined $$json{transform}{runSchedule};
HttpHelper::fail("fail") unless $$json{transform}{runSchedule} eq "";


#============================================================================
# modify/rename function

# modify function
$json = HttpHelper::put_multipart (
    desc => "Modify existing function - update name and content",
    path => "/transform/modify_function",
    parts=> HttpHelper::create_multiparts(
        {name=>'name', value=>$function_name},
        {name=>'owner', value=>$owner_name},
        {name=>'functionType', value=>'updatedType'},
        {name=>'functionContent', value=>"updatedContent"},
        {name=>'updatedName', value=>$function2_name}),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc => "Check function contents were modified",
    path => "/transform/get_function",
    params=>  {'name' => $function2_name},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{function}{functionType} eq 'updatedType';
HttpHelper::fail("fail") unless $$json{function}{functionContent} eq 'updatedContent';
HttpHelper::fail("fail") unless $$json{function}{name} eq $function2_name;

# failed rename if already exists (using current name to simplify test)
$json = HttpHelper::put_multipart ("/transform/modify_function", HttpHelper::create_multiparts(
    {name=>'name', value=>$function2_name},
    {name=>'owner', value=>$owner_name},
    {name=>'updatedName', value=>$function2_name})
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '409';

$json = HttpHelper::put_multipart (
    desc => "Modify function fails if there is nothing to set",
    path => "/transform/modify_function",
    parts=> HttpHelper::create_multiparts(
        {name=>'name', value=>$function_name},
        {name=>'owner', value=>$owner_name}),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put_multipart (
    desc => "Modify function fails if function type and function content are not both set",
    path => "/transform/modify_function",
    parts=> HttpHelper::create_multiparts(
        {name=>'name', value=>$function_name},
        {name=>'owner', value=>$owner_name},
        {name=>'functionType', value=>'updatedType'}),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

#============================================================================
# FK checks

# in use...
$json = HttpHelper::get (
    desc   => "check in use",
    path   => "/transform/get",
    params => { 'transform' => $transform, 'detail' => 'full' },
    sessionKey=>$sessionKey_user1);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{transform}{function}{name} eq $function3_name;

$json = HttpHelper::delete ("/transform/delete_function", {'name'=>$function3_name, 'owner'=>$owner_name});
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /child object exists/;
HttpHelper::fail("fail") unless $$json{status_code} eq '409';


# check
$json = HttpHelper::get ("/transform/get", {'transform' => $transform});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


#============================================================================
# deletion

$json = HttpHelper::delete (
    desc  => "user1 cannot delete transform as if datastream",
    path  => "/datastream/delete",
    params => {dataStream=>$transform},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /underlying DataStream/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::delete (
    desc  => "user1 deletes transform",
    path  => "/transform/delete",
    params => {transform=>$transform},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# should have gone
$json = HttpHelper::get (
    desc  => "deleted transform no longer exists",
    path  => "/transform/get",
    params => {transform=>$transform},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ 'not found';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

$json = HttpHelper::delete (
    desc  => "user1 deletes deleted transform",
    path  => "/transform/delete",
    params => {transform=>$transform},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ 'not found';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';


$json = HttpHelper::post_multipart (
    desc  => "externally generated test",
    path  => "/transform/create",
    parts=>HttpHelper::create_multiparts(
        {name=>'functionName', value=>$function2_name},
        {name=>'parameterValues', content_type=>'application/json', value=>q#{}#},
        {name=>'transform', value=>"i.want.a.microbit"},
        ),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $transformExtkey=$$json{transform};
HttpHelper::fail("fail") unless $transformExtkey;

# get the Transform we just created
$json = HttpHelper::get ("/transform/get", {'transform' => $transformExtkey});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


#============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

