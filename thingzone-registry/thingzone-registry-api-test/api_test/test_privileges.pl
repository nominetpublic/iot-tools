# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;



# connect
my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";
(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

my $json;


# a unique user that we expect not to exist
my $unique = time();
my $username1 = "user_priv_1_$unique";
my $password1 = "password_$unique";
my $username2 = "user_priv_2_$unique";
my $password2 = "password_$unique";
my $username3 = "user_priv_3_$unique";
my $password3 = "password_$unique";


#------------------------------------------

$json = HttpHelper::post (
    desc  => "user_admin can set initial password (bootstrapping)",
    path  => '/session/login',
    params=> {username=>'user_admin', password=>'user_admin'}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_userAdmin=$$json{sessionKey} or die;



$json = HttpHelper::post (
    desc  => "user 'public' cannot set initial password (bootstrapping)",
    path  => '/session/login',
    params=> {username=>'public', password=>'public'}
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /authentication error/;
HttpHelper::fail("fail") unless $$json{status_code} eq '401';

#------------------------------------------

$json = HttpHelper::post (
    desc  => "user_admin can create user1 without privileges",
    path  => '/user/create',
    params=> {username=>$username1, password=>$password1},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "user1 can log in",
    path  => '/session/login',
    params=> {username=>$username1, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user1=$$json{sessionKey} or die;


$json = HttpHelper::post (
    desc  => "user1 cannot create other users",
    path  => '/user/create',
    params=> {username=>$username1, password=>$password1},
    sessionKey=> $sessionKey_user1,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::get (
    desc  => "user_admin can get details of user1",
    path  => '/user/get',
    params=> {username=>$username1},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{user};
HttpHelper::fail("fail") unless $$json{user}{username} eq $username1;
HttpHelper::fail("fail") unless $$json{user}{privileges};
HttpHelper::fail("fail") unless scalar(@{$$json{user}{privileges}}) == 0;

#------------------------------------------


$json = HttpHelper::post (
    desc  => "user_admin can create user2 with CREATE_USER privilege",
    path  => '/user/create',
    params=> {username=>$username2, password=>$password2, privileges=>"CREATE_USER"},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc  => "user_admin can get details of user2",
    path  => '/user/get',
    params=> {username=>$username2},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{user};
HttpHelper::fail("fail") unless $$json{user}{username} eq $username2;
HttpHelper::fail("fail") unless $$json{user}{privileges};
HttpHelper::fail("fail") unless scalar(@{$$json{user}{privileges}}) == 1;
HttpHelper::fail("fail") unless grep {/^CREATE_USER$/} @{$$json{user}{privileges}};


$json = HttpHelper::post (
    desc  => "user2 can log in",
    path  => '/session/login',
    params=> {username=>$username2, password=>$password2},
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user2=$$json{sessionKey} or die;


$json = HttpHelper::post (
    desc  => "user2 can create another user (user3)",
    path  => '/user/create',
    params=> {username=>$username3, password=>$password3},
    sessionKey=> $sessionKey_user2,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc  => "user2 cannot get details of another another user (user3)",
    path  => '/user/get',
    params=> {username=>$username3},
    sessionKey=> $sessionKey_user2,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::post (
    desc  => "user3 can log in",
    path  => '/session/login',
    params=> {username=>$username3, password=>$password3},
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user3=$$json{sessionKey} or die;


$json = HttpHelper::post (
    desc  => "user3 cannot create another user",
    path  => '/user/create',
    params=> {username=>'foo', password=>'foo'},
    sessionKey=> $sessionKey_user3,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::put (
    desc  => "user_admin can revoke privilege from user2",
    path  => '/user/set_privileges',
    params=> {username=>$username2, , privileges=>"CREATE_TRANSFORMFUNCTION"},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::post (
    desc  => "user2 can no longer create another user",
    path  => '/user/create',
    params=> {username=>'foo', password=>'foo'},
    sessionKey=> $sessionKey_user2,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::get (
    desc  => "user_admin re-checks permissions of user2",
    path  => '/user/get',
    params=> {username=>$username2},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{user};
HttpHelper::fail("fail") unless $$json{user}{username} eq $username2;
HttpHelper::fail("fail") unless $$json{user}{privileges};
HttpHelper::fail("fail") unless scalar(@{$$json{user}{privileges}}) == 1;
HttpHelper::fail("fail") unless grep {/^CREATE_TRANSFORMFUNCTION$/} @{$$json{user}{privileges}};



#------------------------------------------
# GRANT_CREATE_USER etc

my $username_grant1 = "user_priv_grant1_$unique";
my $username_grant2 = "user_priv_grant2_$unique";
my $username_grant3 = "user_priv_grant3_$unique";
my $password_grantx = "password_$unique";

$json = HttpHelper::post (
    desc  => "user_admin can create user_grant1 with CREATE_USER, GRANT_CREATE_USER privilege",
    path  => '/user/create',
    params=> {username=>$username_grant1, password=>$password_grantx, privileges=>"CREATE_USER, GRANT_CREATE_USER"},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "user_grant1 can log in",
    path  => '/session/login',
    params=> {username=>$username_grant1, password=>$password_grantx},
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_usergrant1=$$json{sessionKey} or die;


$json = HttpHelper::post (
    desc  => "user_grant1 cannot give another user GRANT_CREATE_USER",
    path  => '/user/create',
    params=> {username=>$username_grant2, password=>$password_grantx, privileges=>"CREATE_USER, GRANT_CREATE_USER"},
    sessionKey=> $sessionKey_usergrant1,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /authentication/;
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::post (
    desc  => "user_grant1 can create another user (user_grant2) & grant them CREATE_USER",
    path  => '/user/create',
    params=> {username=>$username_grant2, password=>$password_grantx, privileges=>"CREATE_USER"},
    sessionKey=> $sessionKey_usergrant1,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "user_grant2 can log in",
    path  => '/session/login',
    params=> {username=>$username_grant2, password=>$password_grantx},
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_usergrant2=$$json{sessionKey} or die;

$json = HttpHelper::post (
    desc  => "user_grant2 cannot give another user CREATE_USER",
    path  => '/user/create',
    params=> {username=>$username_grant3, password=>$password_grantx, privileges=>"CREATE_USER"},
    sessionKey=> $sessionKey_usergrant2,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /authentication/;
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::post (
    desc  => "user_grant2 can create another user (user_grant3)",
    path  => '/user/create',
    params=> {username=>$username_grant3, password=>$password_grantx},
    sessionKey=> $sessionKey_usergrant2,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "user_grant3 can log in",
    path  => '/session/login',
    params=> {username=>$username_grant3, password=>$password_grantx},
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_usergrant3=$$json{sessionKey} or die;


$json = HttpHelper::post (
    desc  => "user_grant3 cannot create another user",
    path  => '/user/create',
    params=> {username=>'foo', password=>$password_grantx},
    sessionKey=> $sessionKey_usergrant3,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /authentication/;
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

#------------------------------------------
# GRANT_CREATE_DEVICE etc

my $username_grant4 = "user_priv_grant4_$unique";
my $username_grant5 = "user_priv_grant5_$unique";

$json = HttpHelper::post (
    desc  => "user_admin can create user_grant4 with CREATE_USER, GRANT_CREATE_DEVICE, GRANT_CREATE_TRANSFORM privilege",
    path  => '/user/create',
    params=> {username=>$username_grant4, password=>$password_grantx, privileges=>"CREATE_USER, GRANT_CREATE_DEVICE, GRANT_CREATE_TRANSFORM"},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "user_grant4 can log in",
    path  => '/session/login',
    params=> {username=>$username_grant4, password=>$password_grantx},
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_usergrant4=$$json{sessionKey} or die;


$json = HttpHelper::post (
    desc  => "user_grant4 cannot give another user GRANT_CREATE_DEVICE",
    path  => '/user/create',
    params=> {username=>$username_grant5, password=>$password_grantx, privileges=>"GRANT_CREATE_DEVICE"},
    sessionKey=> $sessionKey_usergrant4,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /authentication/;
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::post (
    desc  => "user_grant4 cannot give another user CREATE_TRANSFORMFUNCTION",
    path  => '/user/create',
    params=> {username=>$username_grant5, password=>$password_grantx, privileges=>"CREATE_TRANSFORMFUNCTION"},
    sessionKey=> $sessionKey_usergrant4,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /authentication/;
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::post (
    desc  => "user_grant4 can create another user (user_grant5) & grant them CREATE_DEVICE, CREATE_TRANSFORM",
    path  => '/user/create',
    params=> {username=>$username_grant5, password=>$password_grantx, privileges=>"CREATE_DEVICE, CREATE_TRANSFORM"},
    sessionKey=> $sessionKey_usergrant4,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


#------------------------------------------
# GRANT_GRANTS

my $username_gg1 = "user_priv_gg1_$unique";
my $username_gg2 = "user_priv_gg2_$unique";

$json = HttpHelper::post (
    desc  => "user_admin can create user_priv_gg1 with CREATE_USER, GRANT_GRANTS privilege",
    path  => '/user/create',
    params=> {username=>$username_gg1, password=>$password_grantx, privileges=>"CREATE_USER, GRANT_GRANTS"},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "user_priv_gg1 can log in",
    path  => '/session/login',
    params=> {username=>$username_gg1, password=>$password_grantx},
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user_gg1=$$json{sessionKey} or die;


$json = HttpHelper::post (
    desc  => "user_priv_gg1 cannot give another user GRANT_GRANTS",
    path  => '/user/create',
    params=> {username=>$username_gg2, password=>$password_grantx, privileges=>"GRANT_GRANTS"},
    sessionKey=> $sessionKey_user_gg1,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /authentication/;
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::post (
    desc  => "user_priv_gg can can create a user & grant anything",
    path  => '/user/create',
    params=> {username=>$username_gg2, password=>$password_grantx, privileges=>"GRANT_CREATE_TRANSFORMFUNCTION"},
    sessionKey=> $sessionKey_user_gg1,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';



#------------------------------------------

# device creation

$json = HttpHelper::put (
    desc  => "user_admin grants CREATE_DEVICE to user1",
    path  => '/user/set_privileges',
    params=> {username=>$username1, , privileges=>"CREATE_DEVICE"},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "user1 can now create a device",
    path  => "/device/create",
    params=> {},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{device};

$json = HttpHelper::put (
    desc  => "user_admin revokes CREATE_DEVICE from user1",
    path  => '/user/set_privileges',
    params=> {username=>$username1, , privileges=>""},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "user1 cannot create a device",
    path  => "/device/create",
    params=> {},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

#------------------------------------------

# datastream creation

$json = HttpHelper::put (
    desc  => "user_admin grants CREATE_DATASTREAM to user1",
    path  => '/user/set_privileges',
    params=> {username=>$username1, , privileges=>"CREATE_DATASTREAM"},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "user1 can now create a datastream",
    path  => "/datastream/create",
    params=> {},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream};

$json = HttpHelper::put (
    desc  => "user_admin revokes CREATE_DATASTREAM from user1",
    path  => '/user/set_privileges',
    params=> {username=>$username1, , privileges=>""},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "user1 cannot create a datastream",
    path  => "/datastream/create",
    params=> {},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

#------------------------------------------

# transform function creation

$json = HttpHelper::put (
    desc  => "user_admin grants CREATE_TRANSFORMFUNCTION to user1",
    path  => '/user/set_privileges',
    params=> {username=>$username1, , privileges=>"CREATE_TRANSFORMFUNCTION"},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put_multipart (
    desc => "user 1 can now create a transform function",
    path => "/transform/create_function",
    parts=> HttpHelper::create_multiparts(
        {name=>'name', value=>'function1'},
        {name=>'functionType', value=>'javascript'},
        {name=>'functionContent', value=>"console.log('hello world')"}),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::put (
    desc  => "user_admin revokes CREATE_TRANSFORMFUNCTION from user1",
    path  => '/user/set_privileges',
    params=> {username=>$username1, , privileges=>""},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put_multipart (
    desc => "user 1 cannot create a transform function",
    path => "/transform/create_function",
    parts=> HttpHelper::create_multiparts(
        {name=>'name', value=>'function2'},
        {name=>'functionContent', content_type=>'application/json', value=>q#{"stringVal":"forty two"}#},
        {name=>'functionType', value=>'json'}),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

#------------------------------------------

# transform creation

$json = HttpHelper::put (
    desc  => "user_admin grants CREATE_TRANSFORM to user1",
    path  => '/user/set_privileges',
    params=> {username=>$username1, , privileges=>"CREATE_TRANSFORM"},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post_multipart (
    desc  => "user1 can now create a transform",
    path => "/transform/create",
    parts => HttpHelper::create_multiparts(
        {name=>'functionName', value=>'function1'},
        {name=>'functionIsPublic', value=>'false'},
        {name=>'parameterValues', content_type=>'application/json', value=>q#{}#},
        ),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $transform1=$$json{transform} or die;


$json = HttpHelper::put (
    desc  => "user_admin revokes CREATE_TRANSFORM from user1",
    path  => '/user/set_privileges',
    params=> {username=>$username1, , privileges=>""},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post_multipart (
    desc  => "user1 cannot create a transform",
    path => "/transform/create",
    parts => HttpHelper::create_multiparts(
        {name=>'functionName', value=>'function1'},
        {name=>'parameterValues', content_type=>'application/json', value=>q#{}#},
        ),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

#------------------------------------------

# detectors

# just check that the new names exist
$json = HttpHelper::put (
    desc  => "user_admin grants all DETECTOR privs to user1",
    path  => '/user/set_privileges',
    params=> {username=>$username1, privileges=>"CREATE_DETECTOR, RUN_DETECTOR, GRANT_CREATE_DETECTOR, GRANT_RUN_DETECTOR"},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

my $username_detector1 = "user_priv_detector1_$unique";
my $username_detector2 = "user_priv_detector2_$unique";
my $username_detector3 = "user_priv_detector3_$unique";

$json = HttpHelper::post (
    desc  => "user_admin can create detector1 with CREATE_USER privilege",
    path  => '/user/create',
    params=> {username=>$username_detector1, password=>$password_grantx, privileges=>"CREATE_USER"},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "user detector1 can log in",
    path  => '/session/login',
    params=> {username=>$username_detector1, password=>$password_grantx},
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user_detector1=$$json{sessionKey} or die;

$json = HttpHelper::post (
    desc  => "user detector1 cannot create a user with CREATE_DETECTOR privilege",
    path  => '/user/create',
    params=> {username=>$username_detector2, password=>$password_grantx, privileges=>"CREATE_DETECTOR"},
    sessionKey=> $sessionKey_user_detector1,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /authentication error/;
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::post (
    desc  => "user detector1 cannot create a user with RUN_DETECTOR privilege",
    path  => '/user/create',
    params=> {username=>$username_detector2, password=>$password_grantx, privileges=>"RUN_DETECTOR"},
    sessionKey=> $sessionKey_user_detector1,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /authentication error/;
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::put (
    desc  => "user_admin gives GRANT_CREATE_DETECTOR to user detector1",
    path  => '/user/set_privileges',
    params=> {username=>$username_detector1, privileges=>"CREATE_USER, GRANT_CREATE_DETECTOR"},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "user detector1 cannot create a user with RUN_DETECTOR privilege",
    path  => '/user/create',
    params=> {username=>$username_detector2, password=>$password_grantx, privileges=>"RUN_DETECTOR"},
    sessionKey=> $sessionKey_user_detector1,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /authentication error/;
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::post (
    desc  => "user detector1 can now create a user with CREATE_DETECTOR privilege",
    path  => '/user/create',
    params=> {username=>$username_detector2, password=>$password_grantx, privileges=>"CREATE_DETECTOR"},
    sessionKey=> $sessionKey_user_detector1,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::put (
    desc  => "user_admin gives GRANT_RUN_DETECTOR to user detector1",
    path  => '/user/set_privileges',
    params=> {username=>$username_detector1, privileges=>"CREATE_USER, GRANT_RUN_DETECTOR"},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "user detector1 cannot create a user with CREATE_DETECTOR privilege",
    path  => '/user/create',
    params=> {username=>$username_detector3, password=>$password_grantx, privileges=>"CREATE_DETECTOR"},
    sessionKey=> $sessionKey_user_detector1,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /authentication error/;
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::post (
    desc  => "user detector1 can now create a user with RUN_DETECTOR privilege",
    path  => '/user/create',
    params=> {username=>$username_detector3, password=>$password_grantx, privileges=>"RUN_DETECTOR"},
    sessionKey=> $sessionKey_user_detector1,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

#=============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

