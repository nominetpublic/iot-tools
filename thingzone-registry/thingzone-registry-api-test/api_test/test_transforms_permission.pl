# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use Data::Dumper;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;


my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";

(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

my $json;
#============================================================================
# test prerequisites

my $sessionKey_userAdmin=ThingzoneLogin::login_admin_user() or die;

# test user 1
my $unique = time();
my $username1 = "user_device_perm_1_$unique";
my $password1 = "password_$unique";
my $username2 = "user_device_perm_2_$unique";
my $password2 = "password_$unique";

# create 2x users
$json = HttpHelper::post (
    desc  => "Create user 1 ($username1)",
    path  => '/user/create',
    params=> {username=>$username1, password=>$password1, privileges=>"CREATE_DEVICE, CREATE_DATASTREAM, CREATE_TRANSFORM, CREATE_TRANSFORMFUNCTION"},
    sessionKey=>$sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "Create user 2 ($username2)",
    path  => '/user/create',
    params=> {username=>$username2, password=>$password2, privileges=>"CREATE_DEVICE, CREATE_DATASTREAM, CREATE_TRANSFORM"},
    sessionKey=>$sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# log in 2x users
$json = HttpHelper::post (
    desc  => "Log in as user 1 ($username1)",
    path  => '/session/login',
    params=> {username=>$username1, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user1=$$json{sessionKey};


$json = HttpHelper::post (
    desc  => "Log in as user 2 ($username2)",
    path  => '/session/login',
    params=> {username=>$username2, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user2=$$json{sessionKey};

#============================================================================
# functions


$json = HttpHelper::put_multipart (
    desc => "user 1 creates function",
    path => "/transform/create_function",
    parts=> HttpHelper::create_multiparts(
        {name=>'name', value=>'function1'},
        {name=>'functionType', value=>'javascript'},
        {name=>'functionContent', value=>"console.log('hello world')"}),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put_multipart (
    desc => "user 1 creates function",
    path => "/transform/create_function",
    parts=> HttpHelper::create_multiparts(
        {name=>'name', value=>'testfn'},
        {name=>'functionType', value=>'javascript'},
        {name=>'functionContent', value=>"console.log('test')"}),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc  => "user 1 can view their function",
    path  => "/transform/get_function",
    params=> {'name' => 'function1'},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{function}{name};
HttpHelper::fail("fail") unless $$json{function}{functionContent};
HttpHelper::fail("fail") unless $$json{function}{functionType};

$json = HttpHelper::get (
    desc  => "user 2 can only see function names",
    path  => "/transform/get_function",
    params=> {'name' => 'function1'},
    sessionKey=>$sessionKey_user2
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{function}{name};
HttpHelper::fail("fail") if $$json{function}{functionContent};
HttpHelper::fail("fail") if $$json{function}{functionType};

$json = HttpHelper::get (
    desc  => "user 1 can see own function in list",
    path  => "/transform/list_functions",
    params=> {'detail'=>'full'},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{transformFunctions}} == 2;
HttpHelper::fail("fail") unless ${$$json{transformFunctions}}[0]{functionType};
HttpHelper::fail("fail") unless ${$$json{transformFunctions}}[0]{functionContent};

$json = HttpHelper::get (
    desc  => "can't see functionContent without detail = full",
    path  => "/transform/list_functions",
    params=> {},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{transformFunctions}} == 2;
HttpHelper::fail("fail") if ${$$json{transformFunctions}}[0]{functionType};
HttpHelper::fail("fail") if ${$$json{transformFunctions}}[0]{functionContent};

$json = HttpHelper::get (
    desc  => "user 2 can't see function even with detail full",
    path  => "/transform/list_functions",
    params=> {'detail'=>'full'},
    sessionKey=>$sessionKey_user2
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{transformFunctions}} == 2;
HttpHelper::fail("fail") if ${$$json{transformFunctions}}[0]{functionType};
HttpHelper::fail("fail") if ${$$json{transformFunctions}}[0]{functionContent};

# modify function
$json = HttpHelper::put_multipart (
    desc  => "user 1 can modify own function",
    path  => "/transform/modify_function",
    parts => HttpHelper::create_multiparts(
        {name=>'name', value=>'function1'},
        {name=>'functionType', value=>'javascript'},
        {name=>'functionContent', value=>"console.log('hello world')"}),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put_multipart (
    desc  => "user 2 cannot modify user1\'s function",
    path  => "/transform/modify_function",
    parts => HttpHelper::create_multiparts(
        {name=>'name', value=>'function1'},
        {name=>'updatedName', value=>'function1_updated'}),
    sessionKey=>$sessionKey_user2
    );
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::delete (
    desc  => "user 2 cannot delete user1\'s function",
    path  => "/transform/delete_function",
    params=> {'name' => 'function1'},
    sessionKey=>$sessionKey_user2
    );
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::delete (
    desc  => "user 1 can delete their function",
    path  => "/transform/delete_function",
    params=> {'name' => 'function1'},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc  => "the function is deleted",
    path  => "/transform/get_function",
    params=> {'name' => 'function1'},
    sessionKey=>$sessionKey_user2
    );
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

$json = HttpHelper::put_multipart (
    desc => "user 1 creates function",
    path => "/transform/create_function",
    parts=> HttpHelper::create_multiparts(
        {name=>'name', value=>'function1'},
        {name=>'functionType', value=>'javascript'},
        {name=>'functionContent', value=>"console.log('hello world')"}),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put_multipart (
    desc  => "user 1 cannot create function with duplicate name",
    path => "/transform/create_function",
    parts=> HttpHelper::create_multiparts(
        {name=>'name', value=>'function1'},
        {name=>'functionType', value=>'javascript'},
        {name=>'functionContent', value=>"console.log('hello world')"}),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /exists already for user/;
HttpHelper::fail("fail") unless $$json{status_code} eq '409';

$json = HttpHelper::put_multipart (
    desc  => "user 2 can't create a function",
    path => "/transform/create_function",
    parts=> HttpHelper::create_multiparts(
        {name=>'name', value=>'function1'},
        {name=>'functionType', value=>'javascript'},
        {name=>'functionContent', value=>"console.log('hello world')"}),
    sessionKey=>$sessionKey_user2
    );
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

#============================================================================
# transform creation, visibility

$json = HttpHelper::post_multipart (
    desc => "user 1 creates transform",
    path => "/transform/create",
    parts => HttpHelper::create_multiparts(
        {name=>'functionName', value=>'testfn'},
        {name=>'parameterValues', content_type=>'application/json', value=>q#{}#},
     ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $transform1=$$json{transform} or die;

$json = HttpHelper::get (
    desc => "user1 can get own transform",
    path => "/transform/get",
    params=> {'transform' => $transform1},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc => "user1's transform appears in list",
    path => "/transform/list",
    params=> {},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{transforms}} == 1;

$json = HttpHelper::get (
    desc => "user2 cannot get user1\'s transform",
    path => "/transform/get",
    params=> {'transform' => $transform1},
    sessionKey=>$sessionKey_user2
    );
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::get (
    desc => "user2's transform does not appear in user1\'s list",
    path => "/transform/list",
    params=> {},
    sessionKey=>$sessionKey_user2
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{transforms}} == 0;

# transform modification: function
$json = HttpHelper::put (
    desc  => "user1 can modify his transform (function)",
    path  => "/transform/update_function",
    params=> {'transform' => $transform1, 'functionName'=>'function1'},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc  => "user2 cannot modify user1\'s transform (function)",
    path  => "/transform/update_function",
    params=> {'transform' => $transform1, 'functionName'=>'testfn'},
    sessionKey=>$sessionKey_user2
    );
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /authentication error/;
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

# transform modification: params
$json = HttpHelper::put_multipart (
    desc => "user1 can modify his transform (params)",
    path => "/transform/update_parameters",
    parts => HttpHelper::create_multiparts(
        {name=>'transform', value=>$transform1},
        {name=>'parameterValues', content_type=>'application/json', value=>q#{"foo":"bar"}#},
        ),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put_multipart (
    desc => "user2 cannot modify user1\'s transform (params)",
    path => "/transform/update_parameters",
    parts => HttpHelper::create_multiparts(
        {name=>'transform', value=>$transform1},
        {name=>'parameterValues', content_type=>'application/json', value=>q#{"foo":"bar"}#},
        ),
    sessionKey=>$sessionKey_user2
    );
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /authentication error/;
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

# transform modification: sources
$json = HttpHelper::post (
    desc => "create datastream for source",
    path => "/datastream/create",
    params => {},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream1=$$json{dataStream};

$json = HttpHelper::put (
    desc => "user1 can attach datastream source",
    path => "/transform/update_source",
    params => {'transform' => $transform1, 'stream' => $dataStream1, 'alias' => 'a42'},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc => "user2 cannot attach datastream source",
    path => "/transform/update_source",
    params => {'transform' => $transform1, 'stream' => $dataStream1, 'alias' => 'a43'},
    sessionKey=>$sessionKey_user2
    );
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

# need CONSUME permission to attach source
$json = HttpHelper::post (
    desc => "user2 creates datastream for source",
    path => "/datastream/create",
    params => {},
    sessionKey=>$sessionKey_user2
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream2=$$json{dataStream};

$json = HttpHelper::put (
    desc => "user1 cannot attach user2\'s source (no perms)",
    path => "/transform/update_source",
    params => {'transform' => $transform1, 'stream' => $dataStream2, 'alias' => 'b42'},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /authentication error/;+
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::put (
    desc  => "user 2 grants MODIFY permission on source datastream",
    path  => "/permission/set",
    params => {'key'=>$dataStream2, 'username'=>$username1, 'permissions'=>'MODIFY, DISCOVER'},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc => "user1 can now attach user2\'s source (MODIFY perm)",
    path => "/transform/update_source",
    params => {'transform' => $transform1, 'stream' => $dataStream2, 'alias' => 'b42'},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


#============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

