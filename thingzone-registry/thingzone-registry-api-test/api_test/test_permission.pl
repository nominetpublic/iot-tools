# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM = basename $0;

my $server = shift @ARGV;
my $port   = shift @ARGV;
$server = 'localhost' unless $server;
$port   = '8081'      unless $port;

# connect
my $base_url = "http://$server:$port/api";
print "connecting to $base_url...\n";
( my $TC_TEST_CLASS = basename($0) ) =~ s/\.[^.]+$//;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

my $json;

$json = HttpHelper::post(
    desc   => "user_admin sets initial password (bootstrapping)",
    path   => '/session/login',
    params => { username => 'user_admin', password => 'user_admin' }
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_userAdmin = $$json{sessionKey} or die;

# test user 1
my $unique    = time();
my $username1 = "user_permission_1_$unique";
my $password1 = "password_$unique";
my $username2 = "user_permission_2_$unique";
my $password2 = "password_$unique";
my $group1    = "group_1_$unique";

# create 2x users
$json = HttpHelper::post(
    desc   => "Create user 1 ($username1)",
    path   => '/user/create',
    params => {
        username   => $username1,
        password   => $password1,
        privileges => "CREATE_DEVICE, CREATE_DATASTREAM, CREATE_GROUP"
    },
    sessionKey => $sessionKey_userAdmin
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post(
    desc   => "Create user 2 ($username2)",
    path   => '/user/create',
    params => {
        username   => $username2,
        password   => $password2,
        privileges => "CREATE_DEVICE, CREATE_DATASTREAM"
    },
    sessionKey => $sessionKey_userAdmin
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post(
    desc   => "Create user 'sessionContext'",
    path   => '/user/create',
    params => {
        username   => 'sessionContext',
        password   => 'sessionContext',
        privileges => "USE_SESSION"
    },
    sessionKey => $sessionKey_userAdmin
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# log in 2x users
$json = HttpHelper::post(
    desc   => "Log in as user 1 ($username1)",
    path   => '/session/login',
    params => { username => $username1, password => $password1 }
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user1 = $$json{sessionKey};

$json = HttpHelper::post(
    desc       => "create group 1",
    path       => "/groups/$group1",
    params     => {},
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post(
    desc   => "Log in as user 2 ($username2)",
    path   => '/session/login',
    params => { username => $username2, password => $password1 }
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user2 = $$json{sessionKey};

$json = HttpHelper::post(
    desc   => "Log in as user 'sessionContext'",
    path   => '/session/login',
    params => { username => 'sessionContext', password => 'sessionContext' }
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_sessionContext = $$json{sessionKey};

# each user creates a datastream
$json = HttpHelper::post(
    desc       => "create datastream for user 1",
    path       => "/datastream/create",
    params     => {},
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream1 = $$json{dataStream};

$json = HttpHelper::post(
    desc       => "create datastream for user 2",
    path       => "/datastream/create",
    params     => {},
    sessionKey => $sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream2 = $$json{dataStream};

# each user can get permissions for their own datastream
my $TRUE  = 1;
my $FALSE = 0;

$json = HttpHelper::get(
    desc       => "user 1 can get permissions on own datastream",
    path       => "/permission/get",
    params     => { 'key' => $dataStream1 },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put(
    desc =>
      "user 1 can update permissions to allow user 2, for user1's datastream",
    path   => "/permission/set",
    params => {
        'key'         => $dataStream1,
        'username'    => $username2,
        'permissions' => 'CONSUME, DISCOVER'
    },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put(
    desc   => "user 1 cannot set permissions on 2 user2's datastream",
    path   => "/permission/set",
    params => {
        'key'         => $dataStream2,
        'username'    => $username1,
        'permissions' => 'CONSUME, DISCOVER'
    },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} eq 'authentication error';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::get(
    desc => "user 2 can get permissions for themselves, for user1's datastream",
    path => "/permission/get",
    params     => { 'key' => $dataStream1 },
    sessionKey => $sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{ $$json{permissions}{userPermissions} } == 2;
HttpHelper::fail("fail")
  unless grep { /^CONSUME$/ } @{ $$json{permissions}{userPermissions} };
HttpHelper::fail("fail")
  unless grep { /^DISCOVER$/ } @{ $$json{permissions}{userPermissions} };

$json = HttpHelper::put(
    desc   => "user 1 set DISCOVER permission for public user",
    path   => "/permission/set",
    params => {
        'key'         => $dataStream1,
        'username'    => 'public',
        'permissions' => 'DISCOVER'
    },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put(
    desc   => "user 1 set DISCOVER permission for group1",
    path   => "/permission/set",
    params => {
        'key'         => $dataStream1,
        'group'       => $group1,
        'permissions' => 'DISCOVER'
    },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put(
    desc   => "user 1 set DISCOVER permission for all_users group",
    path   => "/permission/set",
    params => {
        'key'         => $dataStream1,
        'group'       => 'all_users',
        'permissions' => 'DISCOVER'
    },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

#----------------------------------------------------------------------------
# permission/list

$json = HttpHelper::get(
    desc =>
"user 1 cannot list permissions on datastream 2 as this requires ADMIN permission",
    path       => "/permission/list",
    params     => { 'key' => $dataStream2 },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} eq 'authentication error';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::get(
    desc =>
"user 2 cannot list permissions on datastream 1 as this requires ADMIN permission",
    path       => "/permission/list",
    params     => { 'key' => $dataStream1 },
    sessionKey => $sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} eq 'authentication error';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::get(
    desc       => "user 1 lists all permissions on datastream 1",
    path       => "/permission/list",
    params     => { 'key' => $dataStream1 },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail")
  unless scalar( keys %{ $$json{userPermissions} } ) == 3;   # both users+public

$json = HttpHelper::get(
    desc       => "user 2 lists all permissions on datastream 2",
    path       => "/permission/list",
    params     => { 'key' => $dataStream2 },
    sessionKey => $sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail")
  unless scalar( keys %{ $$json{userPermissions} } ) == 1;    # just 1 user

$json = HttpHelper::get(
    desc       => "user 1 lists user 2's permissions, on datastream 1",
    path       => "/permission/list",
    params     => { 'key' => $dataStream1, 'username' => $username2 },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail")
  unless scalar( keys %{ $$json{userPermissions} } ) == 1;    # user2 is found
HttpHelper::fail("fail")
  unless $$json{userPermissions}{$username2};                 # user2 is found
HttpHelper::fail("fail")
  unless @{ $$json{userPermissions}{$username2} } == 2;    # CONSUME & DISCOVER

$json = HttpHelper::get(
    desc       => "user 2 lists user 1's permissions, on datastream 2",
    path       => "/permission/list",
    params     => { 'key' => $dataStream2, 'username' => $username1 },
    sessionKey => $sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail")
  unless scalar( keys %{ $$json{userPermissions} } ) == 1;    # user1 is found
HttpHelper::fail("fail")
  unless $$json{userPermissions}{$username1};                 # user1 is found
HttpHelper::fail("fail")
  unless @{ $$json{userPermissions}{$username1} } == 0;       # none

$json = HttpHelper::get(
    desc       => "user 2 asks about nonexistent user on datastream 2",
    path       => "/permission/list",
    params     => { 'key' => $dataStream2, 'username' => 'nemo' },
    sessionKey => $sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail")
  unless scalar( keys %{ $$json{userPermissions} } ) == 1;    # bogus user
HttpHelper::fail("fail")
  unless @{ $$json{userPermissions}{nemo} } == 0;             # no permissions!

$json = HttpHelper::put(
    desc => "user 1 revokes all permissions on datastream 1 for user 2",
    path => "/permission/set",
    params =>
      { 'key' => $dataStream1, 'username' => $username2, 'permissions' => '' },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc   => "user 1 checks permissions removed on datastream 1 for user 2",
    path   => "/permission/list",
    params => { 'key' => $dataStream1, 'username' => $username2 },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail")
  unless @{ $$json{userPermissions}{$username2} } == 0;    # no permissions!

$json = HttpHelper::put(
    desc   => "server handles bogus permissions",
    path   => "/permission/set",
    params => {
        'key'         => $dataStream1,
        'username'    => $username2,
        'permissions' => 'loadofnonsense'
    },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /invalid/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put(
    desc   => "user 1 can grant permission to \'public\' user on datastream 1",
    path   => "/permission/set",
    params => {
        'key'         => $dataStream1,
        'username'    => 'public',
        'permissions' => 'DISCOVER'
    },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc       => "user 1 reviews permissions on datastream 1",
    path       => "/permission/list",
    params     => { 'key' => $dataStream1 },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail")
  unless scalar( keys %{ $$json{userPermissions} } ) == 2;    # 2 users

#------------------------------------------
# events & alerts

$json = HttpHelper::put(
    desc   => "user 1 adds the 4 event/alert permissions for user 2 on datastream 1",
    path   => "/permission/set",
    params => {
        'key'         => $dataStream1,
        'username'    => $username2,
        'permissions' => 'CONSUME, DISCOVER, UPLOAD_EVENTS, CONSUME_EVENTS'
    },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc       => "user 2 reviews permissions on datastream 1",
    path       => "/permission/get",
    params     => { 'key' => $dataStream1 },
    sessionKey => $sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{ $$json{permissions}{userPermissions} } == 4;

$json = HttpHelper::get(
    desc       => "user 2 reviews permissions on datastream 2",
    path       => "/permission/get",
    params     => { 'key' => $dataStream2 },
    sessionKey => $sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{ $$json{permissions}{userPermissions} } == 6;

#------------------------------------------
# Set permissions for multiple keys

$json = HttpHelper::put(
    desc   => "user 1 fails because not ADMIN on datastream2",
    path   => "/permission/set_keys",
    params => {
        'keys'        => "$dataStream1, $dataStream2",
        'username'    => $username2,
        'permissions' => 'CONSUME, DISCOVER, UPLOAD_EVENTS, CONSUME_EVENTS'
    },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::put(
    desc   => "user 2 gives admin to user 1",
    path   => "/permission/set",
    params => {
        'key'         => $dataStream2,
        'username'    => $username1,
        'permissions' => 'ADMIN'
    },
    sessionKey => $sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put(
    desc   => "user 1 sets permissions on datastream 1 and 2",
    path   => "/permission/set_keys",
    params => {
        'keys'        => "$dataStream1, $dataStream2",
        'username'    => $username2,
        'permissions' => 'VIEW_LOCATION, VIEW_METADATA'
    },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc       => "user 2 reviews permissions on datastream 1 after multikey add",
    path       => "/permission/get",
    params     => { 'key' => $dataStream1 },
    sessionKey => $sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{ $$json{permissions}{userPermissions} } == 6;

$json = HttpHelper::get(
    desc       => "user 2 reviews permissions on datastream 2 after multikey add, permissions appended to existing",
    path       => "/permission/get",
    params     => { 'key' => $dataStream2 },
    sessionKey => $sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{ $$json{permissions}{userPermissions} } == 8;

$json = HttpHelper::put(
    desc   => "user 1 removes permissions on datastream 1 and 2",
    path   => "/permission/set_keys",
    params => {
        'keys'        => "$dataStream1, $dataStream2",
        'username'    => $username2,
        'remove'      => 'true',
        'permissions' => 'CONSUME'
    },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc       => "user 2 checks removal of permission on datastream 1",
    path       => "/permission/get",
    params     => { 'key' => $dataStream1 },
    sessionKey => $sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{ $$json{permissions}{userPermissions} } == 5;

$json = HttpHelper::get(
    desc       => "user 2 checks removal of permission on datastream 2",
    path       => "/permission/get",
    params     => { 'key' => $dataStream2 },
    sessionKey => $sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{ $$json{permissions}{userPermissions} } == 7;

$json = HttpHelper::put(
    desc   => "user 1 removes permissions on datastream 1 and 2 for non existent user",
    path   => "/permission/set_keys",
    params => {
        'keys'        => "$dataStream1, $dataStream2",
        'username'    => 'nonexistentUsername',
        'remove'      => 'true',
        'permissions' => 'DISCOVER'
    },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

$json = HttpHelper::put(
    desc   => "user 1 removes permissions on datastream 1 and 2 for non existent key",
    path   => "/permission/set_keys",
    params => {
        'keys'        => "nonexistent, $dataStream2",
        'username'    =>  $username2,
        'remove'      => 'true',
        'permissions' => 'DISCOVER'
    },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

#------------------------------------------
# List admin
$json = HttpHelper::get(
    desc       => "List all entities with admin permission user 2",
    path       => "/entity/list_admin",
    params     => {},
    sessionKey => $sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar( @{ $$json{entities} } ) == 1;
HttpHelper::fail("fail") unless $$json{entities}[0]{key} = $dataStream2;

$json = HttpHelper::get(
    desc       => "List all entities  with admin permission user 1",
    path       => "/entity/list_admin",
    params     => {},
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar( @{ $$json{entities} } ) == 2;
HttpHelper::fail("fail") unless $$json{entities}[0]{key} = $dataStream1;

#=============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

