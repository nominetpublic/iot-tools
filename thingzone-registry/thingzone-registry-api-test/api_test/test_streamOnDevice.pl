# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;

# connect
my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";
(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

# login
ThingzoneLogin::login_section('streamOnDevice');

my $json;

$json = HttpHelper::post ("/device/create", {});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $device=$$json{device};


# fail if device not exist
$json = HttpHelper::get ("/device/get_streams", {'device' => 'nonexistent'});
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

# create some dataStreams
$json = HttpHelper::post ("/datastream/create", {});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream1=$$json{dataStream};

$json = HttpHelper::post ("/datastream/create", {});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream2=$$json{dataStream};

# list before adding
$json = HttpHelper::get ("/device/get_streams", {'device' => $device});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# add one
$json = HttpHelper::put ("/device/add_stream", {'device' => $device, 'dataStream' => $dataStream1, 'name' => 's1'});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# add another
$json = HttpHelper::put ("/device/add_stream", {'device' => $device, 'dataStream' => $dataStream2});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# check
$json = HttpHelper::get ("/device/get_streams", {'device' => $device});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar(@{$$json{dataStreams}}) == 2;

# re-add first with different name
$json = HttpHelper::put ("/device/add_stream", {'device' => $device, 'dataStream' => $dataStream1, 'name' => 's1_bis'});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# remove one
$json = HttpHelper::put ("/device/remove_stream", {'device' => $device, 'dataStream' => $dataStream2});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# remove when not present
$json = HttpHelper::put ("/device/remove_stream", {'device' => $device, 'dataStream' => $dataStream2});
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

# expect 1 left
$json = HttpHelper::get ("/device/get_streams", {'device' => $device});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar(@{$$json{dataStreams}}) == 1;

# fail for bogus device
$json = HttpHelper::put ("/device/add_stream", {'device' => 'bogus', 'dataStream' => $dataStream1, 'name' => 'xxx'});
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /Device not found/;
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

$json = HttpHelper::put ("/device/remove_stream", {'device' => 'bogus', 'dataStream' => $dataStream1});
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /Device not found/;
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

# fail for bogus dataStream
$json = HttpHelper::put ("/device/add_stream", {'device' => $device, 'dataStream' => 'bogus', 'name' => 'xxx'});
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

HttpHelper::fail("fail") unless $$json{reason} =~ /DataStream not found/;
$json = HttpHelper::put ("/device/remove_stream", {'device' => $device, 'dataStream' => 'bogus'});
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~  /DataStream not found/;
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

#============================================================================
# deletion

$json = HttpHelper::delete ("/datastream/delete", {dataStream=>$dataStream1});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# should have gone
$json = HttpHelper::get ("/datastream/get", {dataStream=>$dataStream1});
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ 'not found';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

#=============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

