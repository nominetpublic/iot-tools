# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;

my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";
(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

# login
my $json;
my %login1 = ThingzoneLogin::login_section("device_extkey");
my $sessionKey_user1 = $login1{sessionKey} or die;

#============================================================================
# Device with externally generated name

$json = HttpHelper::post (
    desc  => "create device with externally generated name",
    path  => "/device/create",
    params => {device=>"foo.bar.baz.microbit"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $device=$$json{device};

$json = HttpHelper::post (
    desc  => "can't create device with bogus name",
    path  => "/device/create",
    params => {device=>"I'm bogus, me!"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ 'not valid';
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::get ("/device/list", {});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
die "newly created device not found"
    unless (grep { /$device/ } @{$$json{devices}}) == 1;


# update

$json = HttpHelper::put_multipart ("/device/update", HttpHelper::create_multiparts(
    {name=>'device', value=>$device},
    {name=>'metadata', content_type=>'application/json', value=>q#{"stringVal":"forty two","intVal":42, "test":"something else"}#},
    ));
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# deletion
$json = HttpHelper::delete (
    desc  => "user1 deletes device",
    path  => "/device/delete",
    params => {device=>$device},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# should have gone
$json = HttpHelper::get (
    desc  => "deleted device no longer exists",
    path  => "/device/get",
    params => {device=>$device},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ 'not found';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

#============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

