# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use Data::Dumper;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;



# connect
my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";
(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");


my $json;
# login
my $sessionKey_userAdmin=ThingzoneLogin::login_admin_user() or die;

# test user 1
my $unique = time();
my $username1 = "user_device_perm_1_$unique";
my $password1 = "password_$unique";
my $username_bridge = "bridge_user_$unique";
my $password_bridge = "bridge_user_password_$unique";

# create user
$json = HttpHelper::post (
    desc  => "Create user 1 ($username1)",
    path  => '/user/create',
    params=> {username=>$username1, password=>$password1, privileges=>"CREATE_DEVICE, CREATE_DATASTREAM, CREATE_TRANSFORM, CREATE_TRANSFORMFUNCTION"},
    sessionKey=>$sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# create "bridge" user
$json = HttpHelper::post (
    desc  => "Create bridge user ($username_bridge)",
    path  => '/user/create',
    params=> {username=>$username_bridge, password=>$password_bridge, privileges=>"QUERY_STREAMS"},
    sessionKey=>$sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# log in users
$json = HttpHelper::post (
    desc  => "Log in as user 1 ($username1)",
    path  => '/session/login',
    params=> {username=>$username1, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user1=$$json{sessionKey};

$json = HttpHelper::post (
    desc  => "Log in as bridge user ($username_bridge)",
    path  => '/session/login',
    params=> {username=>$username_bridge, password=>$password_bridge}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_bridge=$$json{sessionKey};

#============================================================================
# functions

$json = HttpHelper::put_multipart (
    desc => "user 1 creates function",
    path => "/transform/create_function",
    parts=> HttpHelper::create_multiparts(
        {name=>'name', value=>'function1'},
        {name=>'functionType', value=>'javascript'},
        {name=>'functionContent', value=>"console.log('hello world')"}),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put_multipart (
    desc => "user 1 creates function",
    path => "/transform/create_function",
    parts=> HttpHelper::create_multiparts(
        {name=>'name', value=>'testfn'},
        {name=>'functionType', value=>'javascript'},
        {name=>'functionContent', value=>"console.log('test')"}),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

#============================================================================
# transform creation, visibility

$json = HttpHelper::post_multipart (
    desc => "user 1 creates transform",
    path => "/transform/create",
    parts => HttpHelper::create_multiparts(
        {name=>'functionName', value=>'testfn'},
        {name=>'parameterValues', content_type=>'application/json', value=>q#{"parameter1": "foo", "parameter2": 42}#},
        ),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $transform1=$$json{transform} or die;

$json = HttpHelper::post_multipart (
    desc => "user 1 creates transform 2",
    path => "/transform/create",
    parts => HttpHelper::create_multiparts(
        {name=>'functionName', value=>'function1'},
        {name=>'functionIsPublic', value=>'false'},
        {name=>'parameterValues', content_type=>'application/json', value=>q#{"marsupial": "numbat"}#},
        ),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $transform2=$$json{transform} or die;

#============================================================================
# transform modification: sources

$json = HttpHelper::post (
    desc => "create datastream1 for source",
    path => "/datastream/create",
    params => {},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream1=$$json{dataStream};

$json = HttpHelper::post (
    desc => "create datastream2 for source",
    path => "/datastream/create",
    params => {},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream2=$$json{dataStream};

$json = HttpHelper::post (
    desc => "create datastream3 for source",
    path => "/datastream/create",
    params => {},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream3=$$json{dataStream};

$json = HttpHelper::put (
    desc => "attaches datastream1 as source (to transform1)",
    path => "/transform/update_source",
    params => {
        'transform' => $transform1,
        'stream' => $dataStream1,
        'alias' => 'Banksy',
        'trigger' => 'false',
        },
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc => "attaches datastream2 as source (to transform1)",
    path => "/transform/update_source",
    params => {
        'transform' => $transform1,
        'stream' => $dataStream2,
        'alias' => 'Testy',
        'trigger' => 'true',
        },
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc => "attaches datastream2 as source (to transform2)",
    path => "/transform/update_source",
    params => {
        'transform' => $transform2,
        'stream' => $dataStream2,
        'alias' => 'Billy the Kid',
        'trigger' => 'false',
        },
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc => "attaches datastream3 as outputStream to transform1",
    path => "/transform/update_outputStream",
    params => { 'transform' => $transform1,
                'stream' => $dataStream3,
                'alias' => 'output' },
    sessionKey=>$sessionKey_user1);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

#============================================================================
# verify dataset

$json = HttpHelper::get (
    desc => "user1 reads transform1",
    path => "/transform/get",
    params=> {'transform' => $transform1},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc => "user1 reads transform2",
    path => "/transform/get",
    params=> {'transform' => $transform2},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

#============================================================================
# GET /internal/transforms_dependent_on_stream

$json = HttpHelper::get (
    desc => "bridge user (internal API) calls transforms_dependent_on_stream on dataStream1",
    path => "/internal/transforms_dependent_on_stream",
    params=> {'key' => $dataStream1},
    sessionKey=>$sessionKey_bridge
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{dependent_transforms};

HttpHelper::fail("fail") unless @{$$json{dependent_transforms}} == 1;
HttpHelper::fail("fail") unless defined ${$$json{dependent_transforms}}[0]{alias};
HttpHelper::fail("fail") unless ${$$json{dependent_transforms}}[0]{alias} eq "Banksy";

HttpHelper::fail("fail") unless defined ${$$json{dependent_transforms}}[0]{trigger};
HttpHelper::fail("fail") unless         ${$$json{dependent_transforms}}[0]{trigger} == 0;
HttpHelper::fail("fail") unless defined ${$$json{dependent_transforms}}[0]{transform}{key};
HttpHelper::fail("fail") unless         ${$$json{dependent_transforms}}[0]{transform}{key} eq $transform1;
HttpHelper::fail("fail") unless defined ${$$json{dependent_transforms}}[0]{transform}{sources};
HttpHelper::fail("fail") unless         @{${$$json{dependent_transforms}}[0]{transform}{sources}} == 2;
HttpHelper::fail("fail") unless defined ${$$json{dependent_transforms}}[0]{transform}{outputStreams};
HttpHelper::fail("fail") unless         @{${$$json{dependent_transforms}}[0]{transform}{outputStreams}} == 1;
HttpHelper::fail("fail") unless defined ${$$json{dependent_transforms}}[0]{transform}{function};
HttpHelper::fail("fail") unless defined ${$$json{dependent_transforms}}[0]{transform}{function}{name};
HttpHelper::fail("fail") unless defined ${$$json{dependent_transforms}}[0]{transform}{function}{functionType};
HttpHelper::fail("fail") unless defined ${$$json{dependent_transforms}}[0]{transform}{function}{functionContent};
HttpHelper::fail("fail") unless defined ${$$json{dependent_transforms}}[0]{transform}{parameterValues};

$json = HttpHelper::get (
    desc => "bridge user (internal API) calls transforms_dependent_on_stream on dataStream2",
    path => "/internal/transforms_dependent_on_stream",
    params=> {'key' => $dataStream2},
    sessionKey=>$sessionKey_bridge
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{dependent_transforms};
HttpHelper::fail("fail") unless @{$$json{dependent_transforms}} == 2;

$json = HttpHelper::get (
    desc => "bridge user (internal API) calls transforms_dependent_on_stream on dataStream3",
    path => "/internal/transforms_dependent_on_stream",
    params=> {'key' => $dataStream3},
    sessionKey=>$sessionKey_bridge
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{dependent_transforms};
HttpHelper::fail("fail") unless @{$$json{dependent_transforms}} == 0;

$json = HttpHelper::get (
    desc => "bridge user (internal API) calls transforms_dependent_on_stream on transform2",
    path => "/internal/transforms_dependent_on_stream",
    params=> {'key' => $transform2},
    sessionKey=>$sessionKey_bridge
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{dependent_transforms};
HttpHelper::fail("fail") unless @{$$json{dependent_transforms}} == 0;

#============================================================================
# permission checks...

$json = HttpHelper::get (
    desc => "normal user cannot call internal API (no QUERY_STREAMS privilege)",
    path => "/internal/transforms_dependent_on_stream",
    params=> {'key' => $dataStream1},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /authentication/;
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

# restore
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

#============================================================================
# filter for non-empty runSchedule...

$json = HttpHelper::get (
    desc => "normal user cannot use transforms_with_runSchedule",
    path => "/internal/transforms_with_runSchedule",
    params=> {},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /authentication/;
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::get (
    desc => "bridge user searches for transforms with runSchedule (none to be found)",
    path => "/internal/transforms_with_runSchedule",
    params=> {},
    sessionKey=>$sessionKey_bridge
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{transforms};
HttpHelper::fail("fail") unless @{$$json{transforms}} == 0;

$json = HttpHelper::put (
    desc  => "update runSchedule",
    path  => "/transform/update_runSchedule",
    params=> { transform => "$transform1", runSchedule => 'twice, knightly' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc => "bridge user searches for transforms with runSchedule (one found)",
    path => "/internal/transforms_with_runSchedule",
    params=> {},
    sessionKey=>$sessionKey_bridge
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{transforms};
HttpHelper::fail("fail") unless @{$$json{transforms}} == 1;
HttpHelper::fail("fail") unless $$json{transforms}[0]{key} eq $transform1;
HttpHelper::fail("fail") unless $$json{transforms}[0]{runSchedule} eq 'twice, knightly';

$json = HttpHelper::put (
    desc  => "update runSchedule",
    path  => "/transform/update_runSchedule",
    params=> { transform => "$transform2", runSchedule => 'all year' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc => "bridge user searches for transforms with runSchedule (two found)",
    path => "/internal/transforms_with_runSchedule",
    params=> {},
    sessionKey=>$sessionKey_bridge
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{transforms};
HttpHelper::fail("fail") unless @{$$json{transforms}} == 2;


#============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

