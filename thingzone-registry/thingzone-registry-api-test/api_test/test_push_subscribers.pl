# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use Data::Dumper;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;


my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";

(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

my $json;

# login
ThingzoneLogin::login_section('pullSubscribers');
#============================================================================

$json = HttpHelper::get ("/push_subscribers/list", {});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

my %login1 = ThingzoneLogin::login_section("dataStream");
my $sessionKey_user1 = $login1{sessionKey} or die;

# get with bogus dataStream (will fail)
$json = HttpHelper::get ("/push_subscribers/get", {dataStream=>"wot"});
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

# create a dataStream
$json = HttpHelper::post(desc  => "create datastream_name with entity name and type",
    path  => "/datastream/create",
    params => { "name"=>"datastream name", "type"=>"camera"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream=$$json{dataStream};

# create a second dataStream
$json = HttpHelper::post(desc  => "create a second datastream",
    path  => "/datastream/create",
    params => { "name"=>"second", "type"=>"magnetic"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream2=$$json{dataStream};


# get when not yet created
$json = HttpHelper::get ("/push_subscribers/get", {dataStream=>$dataStream, uri=>'wot'});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# set push subscriber on dataStream
$json = HttpHelper::put_multipart (
    desc  => "Add first push subscriber",
    path  => "/push_subscribers/set",
    parts => HttpHelper::create_multiparts(
        {name=>'dataStream', value=>$dataStream},
        {name=>'uri', value=>'http://example.com'},
        {name=>'uriParams', content_type=>'application/json', value=>q#{"httpHeaders":{"SessionHeaderId": "1234"} }#}),
    sessionKey=>$sessionKey_user1);

$json = HttpHelper::get(
    desc  => "check new push subscriber",
    path  => "/push_subscribers/get",
    params=> {'dataStream' => $dataStream},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{pushSubscribers}[0]{uriParams}{httpHeaders}{SessionHeaderId} eq '1234';
HttpHelper::fail("fail") if defined $$json{pushSubscribers}[0]{headers};
HttpHelper::fail("fail") unless @{$$json{pushSubscribers}} == 1;

$json = HttpHelper::put_multipart (
    desc  => "Update push subscriber",
    path  =>"/push_subscribers/set",
    parts => HttpHelper::create_multiparts(
    {name=>'dataStream', value=>$dataStream},
    {name=>'uri', value=>'http://example.com'},
    {name=>'uriParams', content_type=>'application/json', value=>q#{"foo":"bar"}#},
    {name=>'headers', content_type=>'application/json', value=>q#{"httpHeaders":{"SessionHeaderId": "AAAA"} }#},
    {name=>'method', value=>'POST'},
    {name=>'retrySequence', value=>'5m'},
    ));
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc  => "check updates were added",
    path  => "/push_subscribers/get",
    params=> {'dataStream' => $dataStream},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{pushSubscribers}[0]{uriParams}{foo} eq 'bar';
HttpHelper::fail("fail") unless defined $$json{pushSubscribers}[0]{headers};
HttpHelper::fail("fail") unless defined $$json{pushSubscribers}[0]{retrySequence};
HttpHelper::fail("fail") unless $$json{pushSubscribers}[0]{method} eq 'POST';
HttpHelper::fail("fail") unless @{$$json{pushSubscribers}} == 1;

$json = HttpHelper::put_multipart (
    desc  => "Add second push subscriber",
    path  => "/push_subscribers/set",
    parts => HttpHelper::create_multiparts(
        {name=>'dataStream', value=>$dataStream},
        {name=>'uri', value=>'http://another-example.com'},
        {name=>'uriParams', content_type=>'application/json', value=>q#{}#},
        {name=>'headers', content_type=>'application/json', value=>q#{"httpHeaders":{"SessionHeaderId": "1234"} }#},
    ),
    sessionKey=>$sessionKey_user1);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# now expect 2
$json = HttpHelper::get(
    desc  => "check updates were added",
    path  => "/push_subscribers/get",
    params=> {'dataStream' => $dataStream},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{pushSubscribers}} == 2;
HttpHelper::fail("fail") unless $$json{pushSubscribers}[0]{dataStream} eq $dataStream;

$json = HttpHelper::get(
    desc  => "get with details",
    path  => "/push_subscribers/get",
    params=> {'dataStream' => $dataStream, detail=>"full"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{pushSubscribers}} == 2;
HttpHelper::fail("fail") unless $$json{pushSubscribers}[0]{dataStream}{type} eq 'camera';

$json = HttpHelper::put_multipart (
    desc  => "Add subscriber to 2nd datastream",
    path  =>"/push_subscribers/set",
    parts => HttpHelper::create_multiparts(
        {name=>'dataStream', value=>$dataStream2},
        {name=>'uri', value=>'http://example.com'},
        {name=>'uriParams', content_type=>'application/json', value=>q#{"second":2}#},
        {name=>'headers', content_type=>'application/json', value=>q#{"httpHeaders":{"SessionHeaderId": 324} }#},
        {name=>'method', value=>'PUT'},
        {name=>'retrySequence', value=>'53m'},
    ));
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc  => "list with details",
    path  => "/push_subscribers/list",
    params=> { detail => 'full' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{pushSubscribers}} == 3;

$json = HttpHelper::put(
    desc  => "remove second datastream",
    path  => "/push_subscribers/delete",
    params=> {dataStream=>$dataStream, uri=>'http://another-example.com'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc  => "check that the second push subscriber has been deleted",
    path  => "/push_subscribers/get",
    params=> {'dataStream' => $dataStream},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{pushSubscribers}} == 1;

$json = HttpHelper::get(
    desc  => "check that the second push subscriber has been deleted",
    path  => "/push_subscribers/list",
    params=> {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{pushSubscribers}} == 2;


#============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

