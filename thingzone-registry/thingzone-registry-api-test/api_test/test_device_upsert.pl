# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use Data::Dumper;
use JSON;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;

# connect
my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";
(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

my $json;
my $sessionKey_userAdmin=ThingzoneLogin::login_admin_user() or die;
#=========================================================================

# user creation
my $unique = time();
my $username1 = "user_device_upsert_1_$unique";
my $password1 = "password_$unique";
my $username_dumper = "user_dumper_$unique";
my $password_dumper = "password_dumper_$unique";
my $group1 = "group_upsert_1_$unique";

$json = HttpHelper::post (
    desc  => "Create user ($username1)",
    path  => '/user/create',
    params=> {username=>$username1, password=>$password1, privileges=>"CREATE_GROUP, CREATE_DEVICE, CREATE_DATASTREAM"},
    sessionKey=>$sessionKey_userAdmin
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "Create user 'dumper'",
    path  => '/user/create',
    params=> {username=>$username_dumper, password=>$password_dumper, privileges=>"DUMP_ALL"},
    sessionKey=>$sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# log in users
$json = HttpHelper::post (
    desc  => "Log in as user ($username1)",
    path  => '/session/login',
    params=> {username=>$username1, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user1=$$json{sessionKey};

$json = HttpHelper::post (
    desc  => "Log in as user 'dumper'",
    path  => '/session/login',
    params=> {username=>$username_dumper, password=>$password_dumper}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_dumper=$$json{sessionKey};

# create group
$json = HttpHelper::post(
    desc  => "create group 1",
    path  => "/groups/$group1",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put(
    desc  => "add users to group 1",
    path  => "/groups/$group1/users/add",
    params => { 'usernames' => "$username1,$username_dumper" },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

#=========================================================================
# create a initial device + all stuff

$json = HttpHelper::post (
    desc  => "create device",
    path  => "/device/create",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $device1=$$json{device} or die;

$json = HttpHelper::put_multipart (
    desc  => "add metadata",
    path  => "/device/update",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device1},
        {name=>'metadata', content_type=>'application/json', value=>q#{"here be metadata":"so meta"}#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# set permissions
$json = HttpHelper::put (
    desc  => "user 1 adds some privileges",
    path  => "/permission/set",
    params => {'key'=>$device1, 'username'=>'public', 'permissions'=>'DISCOVER'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


#=============================================================================
# verify / read


$json = HttpHelper::get (
    desc  => "read back initial data",
    path  => "/device/get",
    params => {"device" => $device1},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::get (
    desc  => "dump should return all data for user 'dumper'",
    path  => "/dump/all",
    params=> {},
    sessionKey=>$sessionKey_dumper,
    print_raw_json => 1
    );
HttpHelper::fail("fail") unless ref $json eq 'ARRAY';    # special JSON response for dump, so it's streamable


#=============================================================================
# upsert

# upsert as existing device (no changes)

$json = HttpHelper::put_multipart (
    desc  => "upsert (no changes)",
    path  => "/device/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device1}
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{device};
HttpHelper::fail("fail") unless $$json{device}{key} eq $device1;
HttpHelper::fail("fail") unless $$json{device}{metadata};
HttpHelper::fail("fail") unless $$json{device}{metadata}{'here be metadata'};
HttpHelper::fail("fail") unless $$json{device}{metadata}{'here be metadata'} eq 'so meta';
HttpHelper::fail("fail") if $$json{device}{dnsRecords};
HttpHelper::fail("fail") if $$json{device}{dataStreams};
HttpHelper::fail("fail") unless $$json{device}{userPermissions};
HttpHelper::fail("fail") unless $$json{device}{userPermissions}{public};
HttpHelper::fail("fail") unless @{$$json{device}{userPermissions}{public}} eq 1;
HttpHelper::fail("fail") unless $$json{device}{userPermissions}{$username1};
HttpHelper::fail("fail") unless @{$$json{device}{userPermissions}{$username1}} eq 6;


# upsert as a new device
my $device2 = "device2-$unique";

$json = HttpHelper::put_multipart (
    desc  => "upsert (create new device $device2)",
    path  => "/device/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device2}
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{device};
HttpHelper::fail("fail") unless $$json{device}{key} eq $device2;

# upsert as a new device with autogenerated key
$json = HttpHelper::put_multipart (
    desc  => "upsert (create new device no key provided)",
    path  => "/device/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>'metadata', content_type=>'application/json', value=>q#{"metadata":"meta meta data"}#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{device};
HttpHelper::fail("fail") unless $$json{device}{metadata}{'metadata'};
HttpHelper::fail("fail") unless $$json{device}{metadata}{'metadata'} eq 'meta meta data';

#-----------------------------------------------------------------------------
# upsert metadata

$json = HttpHelper::put_multipart (
    desc  => "upsert & change metadata",
    path  => "/device/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device1},
        {name=>'metadata', content_type=>'application/json', value=>q#{"different metadata":"meta meta data"}#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{device}{key} eq $device1;
HttpHelper::fail("fail") unless $$json{device}{metadata};
HttpHelper::fail("fail") if $$json{device}{metadata}{'here be metadata'};
HttpHelper::fail("fail") unless $$json{device}{metadata}{'different metadata'};
HttpHelper::fail("fail") unless $$json{device}{metadata}{'different metadata'} eq 'meta meta data';

#-----------------------------------------------------------------------------
# upsert DNS records
# (bare-bones test here: full tests for dataStreams, which share implementation)


$json = HttpHelper::put (
    desc  => "add DNS record",
    path  => "/dns/add_record",
    params=> {'key'=>$device1, 'description'=>'dns1', 'subdomain'=>'nautilus', 'RRTYPE'=>"A", 'RDATA'=>"1.2.3.4"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
$json = HttpHelper::put (
    desc  => "add DNS record",
    path  => "/dns/add_record",
    params=> {'key'=>$device1, 'description'=>'dns2', 'subdomain'=>'gup-b', 'RRTYPE'=>"CNAME", 'RDATA'=>"octo.nauts"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::put_multipart (
    desc  => "upsert & change DNS records",
    path  => "/device/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device1},
        {name=>'dnsRecords', content_type=>'application/json', value=>
"["
. q# { "rrtype" : "CNAME","subdomain" : "gup-b","ttl" : 6000, "description" : "dns2", "rdata" : "octo.nauts" } #    # modified
.','
. q# { "ttl" : 600, "description" : "dns3", "rdata" : "2.3.4.5", "rrtype" : "A", "subdomain" : "triton" } # # new
. "]" },
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{device}{dnsRecords}} == 2;

for my $dns (@{$$json{device}{dnsRecords}}) {
#    print Dumper $dns;
    if ($$dns{description} eq 'dns3') {
        HttpHelper::fail("fail") unless $$dns{subdomain} eq 'triton';
        HttpHelper::fail("fail") unless $$dns{rrtype} eq 'A';
        HttpHelper::fail("fail") unless $$dns{rdata} eq '2.3.4.5';
        HttpHelper::fail("fail") unless $$dns{ttl} eq 600;
    } elsif ($$dns{description} eq 'dns2') {
        HttpHelper::fail("fail") unless $$dns{subdomain} eq 'gup-b';
        HttpHelper::fail("fail") unless $$dns{rrtype} eq 'CNAME';
        HttpHelper::fail("fail") unless $$dns{rdata} eq 'octo.nauts';
        HttpHelper::fail("fail") unless $$dns{ttl} eq 6000;
    } else {
        HttpHelper::fail("DNS record desc=".$$dns{description}. " unexpected");
    }
}


#-----------------------------------------------------------------------------
# upsert datastream links

# (hard-code the datastream names, to simplify JSON param creation)
$json = HttpHelper::post (
    desc  => "create datastream1",
    path  => "/datastream/create",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream1=$$json{dataStream} or die;

$json = HttpHelper::post (
    desc  => "create datastream2",
    path  => "/datastream/create",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream2=$$json{dataStream} or die;

$json = HttpHelper::post (
    desc  => "create datastream3",
    path  => "/datastream/create",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream3=$$json{dataStream} or die;

$json = HttpHelper::put (
    desc  => "add stream 1 to device 1",
    path  => "/device/add_stream",
    params=> {'device' => $device1, 'dataStream' => $dataStream1, 'name' => 'd1_s1'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc  => "add stream 2 to device 1",
    path  => "/device/add_stream",
    params=> {'device' => $device1, 'dataStream' => $dataStream2, 'name' => 'd1_s2'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

my @stream_data = ({ "key" => $dataStream1, "linkName" => "d1_s1" }, { "key" => $dataStream2, "linkName" => "d1_s2" });
my $stream_json = encode_json \@stream_data;
$json = HttpHelper::put_multipart (
    desc  => "upsert Device & set link to datastream1&2 (no change)",
    path  => "/device/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device1},
        {name=>'dataStreams', content_type=>'application/json', value=> $stream_json },
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

@stream_data = ({ "key" => $dataStream1, "linkName" => "renamed" }, { "key" => $dataStream3, "linkName" => "d1_s3" });
$stream_json = encode_json \@stream_data;
$json = HttpHelper::put_multipart (
    desc  => "upsert Device & make changes",
    path  => "/device/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device1},
        {name=>'dataStreams', content_type=>'application/json', value=> $stream_json},
    ),
    sessionKey=>$sessionKey_user1
);

HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{device}{dataStreams};
die unless @{$$json{device}{dataStreams}} == 2;
for my $dataStream (@{$$json{device}{dataStreams}}) {
    print Dumper $dataStream;
    if ($$dataStream{key} eq $dataStream1) {
        die unless $$dataStream{linkName} eq 'renamed';
    } elsif ($$dataStream{key} eq $dataStream3) {
        die unless $$dataStream{linkName} eq 'd1_s3';
    } else {
        die "dataStream key=".$$dataStream{key}. " unexpected";
    }
}


#-----------------------------------------------------------------------------
# upsert entity type

$json = HttpHelper::put_multipart (
    desc  => "upsert Device & update entity type",
    path  => "/device/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device1},
        {name=>"type", value=>"updated_type" }),
    sessionKey=>$sessionKey_user1
);

HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{device}{type} eq 'updated_type';

$json = HttpHelper::put_multipart (
    desc  => "upsert Device & unset entity type",
    path  => "/device/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device1},
        {name=>"type", value=>"" }),
    sessionKey=>$sessionKey_user1
);

HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") if (defined $$json{device}{type});


#-----------------------------------------------------------------------------
# upsert entity name

$json = HttpHelper::put_multipart (
    desc  => "upsert Device & update entity type",
    path  => "/device/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device1},
        {name=>"name", value=>"updated_name" }),
    sessionKey=>$sessionKey_user1
);

HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{device}{name} eq "updated_name";

$json = HttpHelper::put_multipart (
    desc  => "upsert Device & unset entity type",
    path  => "/device/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device1},
        {name=>"name", value=>"" }),
    sessionKey=>$sessionKey_user1
);

HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") if (defined $$json{device}{name});

#-----------------------------------------------------------------------------
# upsert location

$json = HttpHelper::put_multipart (
    desc  => "upsert Device & set location",
    path  => "/device/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device1},
        {name=>"location", content_type=>'application/json', value=>q#{"latitude":"51.507351", "longitude":"-0.127758"}#},
    ),
    sessionKey=>$sessionKey_user1
);

HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{device}{location};
HttpHelper::fail("fail") unless $$json{device}{location}{latitude} eq "51.507351";

$json = HttpHelper::put_multipart (
    desc  => "upsert Device & unset location",
    path  => "/device/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device1},
        {name=>"location", value=>"" }),
    sessionKey=>$sessionKey_user1
);

HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") if $$json{device}{location};
HttpHelper::fail("fail") if $$json{device}{location}{latitude};

#=============================================================================
# upsert permissions

$json = HttpHelper::put_multipart (
    desc  => "upsert & change permissions (bogus json)",
    path  => "/device/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device1},
        {name=>'userPermissions', content_type=>'application/json', value=> "invalid json"},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /bad JSON/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put_multipart (
    desc  => "upsert & change permissions (json in wrong form)",
    path  => "/device/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device1},
        {name=>'userPermissions', content_type=>'application/json', value=> q#{ "foo": false }#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /bad JSON/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put_multipart (
    desc  => "upsert & change permissions (with invalid user)",
    path  => "/device/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device1},
        {name=>'userPermissions', content_type=>'application/json', value=>
"{ \"notUser\" : [\"DISCOVER\"], \"$username1\" : [\"ADMIN\",\"UPLOAD\",\"CONSUME\",\"MODIFY\",\"ANNOTATE\",\"DISCOVER\"] }"},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /User not found/;
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

$json = HttpHelper::put_multipart (
    desc  => "upsert & change permissions (to be the same)",
    path  => "/device/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device1},
        {name=>'userPrmissions', content_type=>'application/json', value=>
"{ \"public\" : [\"DISCOVER\"], \"$username1\" : [\"ADMIN\",\"UPLOAD\",\"CONSUME\",\"MODIFY\",\"ANNOTATE\",\"DISCOVER\"] }"
            },
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{device}{key} eq $device1;
HttpHelper::fail("fail") unless $$json{device}{metadata};
HttpHelper::fail("fail") unless $$json{device}{userPermissions}{public};
HttpHelper::fail("fail") unless @{$$json{device}{userPermissions}{public}} == 1;
HttpHelper::fail("fail") unless $$json{device}{userPermissions}{$username1};
HttpHelper::fail("fail") unless @{$$json{device}{userPermissions}{$username1}} == 6;

#=============================================================================
# upsert group permissions

$json = HttpHelper::put_multipart (
    desc  => "upsert & change group permissions (bogus json)",
    path  => "/device/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device1},
        {name=>'groupPermissions', content_type=>'application/json', value=> "invalid json"},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /bad JSON/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put_multipart (
    desc  => "upsert & change group permissions (json in wrong form)",
    path  => "/device/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device1},
        {name=>'groupPermissions', content_type=>'application/json', value=> q#{ "foo": false }#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /bad JSON/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put_multipart (
    desc  => "upsert & change group permissions (json with invalid permission)",
    path  => "/device/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device1},
        {name=>'groupPermissions', content_type=>'application/json', value=> q#{ "public" : ["NOT_A_VALID_PERMISSION"] }#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /bad JSON/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put_multipart (
    desc  => "upsert & change permissions (json with invalid group)",
    path  => "/device/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device1},
        {name=>'groupPermissions', content_type=>'application/json', value=> "{ \"notGroup\" : [\"DISCOVER\"] }" },
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /Group not found/;
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

$json = HttpHelper::put_multipart (
    desc  => "upsert & change permissions (to add group 1)",
    path  => "/device/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device1},
        {name=>'groupPermissions', content_type=>'application/json', value=>
"{ \"$group1\" : [\"ADMIN\",\"UPLOAD\",\"CONSUME\",\"MODIFY\",\"ANNOTATE\",\"DISCOVER\"] }" },
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{device}{key} eq $device1;
HttpHelper::fail("fail") unless $$json{device}{metadata};
HttpHelper::fail("fail") unless $$json{device}{groupPermissions}{$group1};
HttpHelper::fail("fail") unless @{$$json{device}{groupPermissions}{$group1}} == 6;

#=============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

