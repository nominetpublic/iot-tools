# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;


my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;

my $base_url="http://$server:$port/api";

# get non-localhost IP
chomp (my $ip_address=`hostname -I`) or die;
(my $ip = $ip_address) =~ s/([\w\.]+).*/$1/;
my $unpriv_url="http://$ip:$port/api";

my $json;

print "connecting to $unpriv_url...\n";
HttpHelper::init($unpriv_url);
$json = HttpHelper::post (
    desc  => "user_admin can connect from non-privileged IP",
    path  => '/session/login',
    params=> {username=>'user_admin', password=>'user_admin'}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_userAdmin_unpriv=$$json{sessionKey} or die;


print "connecting to $base_url...\n";
use File::Basename;
(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");
$json = HttpHelper::post (
    desc  => "user_admin can connect from privileged IP",
    path  => '/session/login',
    params=> {username=>'user_admin', password=>'user_admin'}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_userAdmin_priv=$$json{sessionKey} or die;

#=============================================================================
# DUMP_ALL

print "connecting to $base_url...\n";

HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

# create user for dump
$json = HttpHelper::post (
    desc  => "Create user dumper",
    path  => '/user/create',
    params=> {username=>'dumper', password=>'dumper', privileges=>"DUMP_ALL"},
    sessionKey=>$sessionKey_userAdmin_priv,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

print "connecting to $unpriv_url...\n";
HttpHelper::init($unpriv_url);

$json = HttpHelper::post (
    desc  => "Log in as user 'dumper'",
    path  => '/session/login',
    params=> {username=>'dumper', password=>'dumper'}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_dumper=$$json{sessionKey};

HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

$json = HttpHelper::post (
    desc  => "Log in as user 'dumper'",
    path  => '/session/login',
    params=> {username=>'dumper', password=>'dumper'}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
$sessionKey_dumper=$$json{sessionKey};


$json = HttpHelper::get (
    desc  => "dump should succeed from privileged IP",
    path  => "/dump/all",
    params=>{},
    sessionKey=>$sessionKey_dumper,
    );
die unless ref $json eq 'ARRAY';

#============================================================================
# USE_SESSION

HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

$json = HttpHelper::post (
    desc  => "Create user 'sessionContext'",
    path  => '/user/create',
    params=> {username=>'sessionContext', password=>'sessionContext', privileges=>"USE_SESSION"},
    sessionKey=>$sessionKey_userAdmin_priv
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

print "connecting to $unpriv_url...\n";
HttpHelper::init($unpriv_url);

$json = HttpHelper::post (
    desc  => "Log in as user 'sessionContext' from non-privileged IP",
    path  => '/session/login',
    params=> {username=>'sessionContext', password=>'sessionContext'}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_sessionContext_unpriv=$$json{sessionKey} or die;

$json = HttpHelper::get (
    desc  => "get_username should now succeed (any address accepted)",
    path  => "/internal/get_username",
    params => {'sessionKey'=>$sessionKey_userAdmin_priv},
    sessionKey=>$sessionKey_sessionContext_unpriv,
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

$json = HttpHelper::post (
    desc  => "Log in as user 'sessionContext' from non-privileged IP",
    path  => '/session/login',
    params=> {username=>'sessionContext', password=>'sessionContext'}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

my $sessionKey_sessionContext_priv=$$json{sessionKey} or die;


$json = HttpHelper::get (
    desc  => "get_username should succeeed from privileged IP",
    path  => "/internal/get_username",
    params => {'sessionKey'=>$sessionKey_userAdmin_priv},
    sessionKey=>$sessionKey_sessionContext_priv,
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


#=============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

