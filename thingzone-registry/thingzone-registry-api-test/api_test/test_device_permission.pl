# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;

# connect
my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";
(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

my $json;
my $sessionKey_userAdmin=ThingzoneLogin::login_admin_user() or die;
#-------------------------------------------

# test user 1
my $unique = time();
my $username1 = "user_device_perm_1_$unique";
my $password1 = "password_$unique";
my $username2 = "user_device_perm_2_$unique";
my $password2 = "password_$unique";

# create 2x users (via user_admin's session)
$json = HttpHelper::post (
    desc  => "Create user 1 ($username1)",
    path  => '/user/create',
    params=> {username=>$username1, password=>$password1, privileges=>"CREATE_DEVICE"},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "Create user 2 ($username2)",
    path  => '/user/create',
    params=> {username=>$username2, password=>$password2, privileges=>"CREATE_DEVICE"},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# log in 2x users
$json = HttpHelper::post (
    desc  => "Log in as user 1 ($username1)",
    path  => '/session/login',
    params=> {username=>$username1, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user1=$$json{sessionKey};


$json = HttpHelper::post (
    desc  => "Log in as user 2 ($username2)",
    path  => '/session/login',
    params=> {username=>$username2, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user2=$$json{sessionKey};

#-------------------------------------------
# each user creates a device

$json = HttpHelper::post (
    desc  => "create device for user 1",
    path  => "/device/create",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $device1=$$json{device};

$json = HttpHelper::post (
    desc  => "create device for user 2",
    path  => "/device/create",
    params => {},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $device2=$$json{device};


# each user can get permissions for their own device

my $TRUE=1;
my $FALSE=0;

$json = HttpHelper::put (
    desc  => "user 1 can update privileges to allow user 2, for user1's device",
    path  => "/permission/set",
    params => {'key'=>$device1, 'username'=>$username2, 'permissions'=>'CONSUME, DISCOVER'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::get (
    desc  => "user 1 cannot discover user2's device",
    path  => "/device/list",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
die unless @{$$json{devices}} == 1;


$json = HttpHelper::get (
    desc  => "user 2 can discover user1's device (due to DISCOVER permission)",
    path  => "/device/list",
    params => {},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{devices}} == 2;

#=============================================================================


$json = HttpHelper::get (
    desc  => "user 2 can get details on user1's device (DISCOVER permission)",
    path  => "/device/get",
    params => {"device" => $device1},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc  => "user 1 cannot get details on user2's device",
    path  => "/device/get",
    params => {"device" => $device2},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

#=============================================================================
#=== Check Permission.VIEW_LOCATION

$json = HttpHelper::put_multipart (
    desc  => "set location",
    path  => "/device/update",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device1},
        {name=>'location', content_type=>'application/json', value=>q#{"latitude":"51.507351", "longitude":"-0.127758"}#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc  => "user 1 can see location",
    path  => "/device/get",
    params => {"device" => $device1},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{device}{location};
HttpHelper::fail("fail") unless $$json{device}{location}{latitude} eq "51.507351";

$json = HttpHelper::get (
    desc  => "user 2 can't see location",
    path  => "/device/get",
    params => {"device" => $device1},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
die if $$json{device}{location};

$json = HttpHelper::put (
    desc  => "user 1 can update privileges to allow user 2 to view location on device1",
    path  => "/permission/set",
    params => {'key'=>$device1, 'username'=>$username2, 'permissions'=>'CONSUME, DISCOVER, VIEW_LOCATION'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc  => "user 2 can see location",
    path  => "/device/get",
    params => {"device" => $device1},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") if $$json{device}{metadata};
HttpHelper::fail("fail") unless $$json{device}{location};
HttpHelper::fail("fail") unless $$json{device}{location}{latitude} eq "51.507351";

#=============================================================================
#=== Check Permission.VIEW_METADATA

$json = HttpHelper::put_multipart (
    desc  => "se metadata",
    path  => "/device/update",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device1},
        {name=>'metadata', content_type=>'application/json', value=>q#{"meta":"data", "foo":"bar"}#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc  => "user 1 can see metadata",
    path  => "/device/get",
    params => {"device" => $device1},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{device}{metadata};
HttpHelper::fail("fail") unless $$json{device}{metadata}{meta} eq "data";


$json = HttpHelper::get (
    desc  => "user 2 doesn't have permission to see metadata",
    path  => "/device/get",
    params => {"device" => $device1},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") if $$json{device}{metadata};

$json = HttpHelper::put (
    desc  => "user 1 can update privileges to allow user 2 to view location on device1",
    path  => "/permission/set",
    params => {'key'=>$device1, 'username'=>$username2, 'permissions'=>'CONSUME, DISCOVER, VIEW_METADATA'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc  => "user 2 can see metadata",
    path  => "/device/get",
    params => {"device" => $device1},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") if $$json{device}{location};
HttpHelper::fail("fail") unless $$json{device}{metadata};
HttpHelper::fail("fail") unless $$json{device}{metadata}{meta} eq "data";

#=============================================================================

$json = HttpHelper::put_multipart (
    desc  => "user 2 can NOT modify user1's device (no MODIFY permission)",
    path  => "/device/update",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device1},
        {name=>'metadata', content_type=>'application/json', value=>q#{"thing":"yes"}#},
    ),
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::put_multipart (
    desc  => "user 1 can NOT modify user2's device (no MODIFY permission)",
    path  => "/device/update",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device2},
        {name=>'metadata', content_type=>'application/json', value=>q#{"thing":"yes"}#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

# set permission
$json = HttpHelper::put (
    desc  => "user 1 updates privileges to give user 2 MODIFY permission",
    path  => "/permission/set",
    params => {'key'=>$device1, 'username'=>$username2, 'permissions'=>'CONSUME, MODIFY, DISCOVER'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::put_multipart (
    desc  => "user 2 can modify user1's device (MODIFY permission)",
    path  => "/device/update",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device1},
        {name=>'metadata', content_type=>'application/json', value=>q#{"thing":"yes"}#},
    ),
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


#============================================================================
# deletion

$json = HttpHelper::delete (
    desc  => "user2 cannot delete user1's device",
    path  => "/device/delete",
    params => {device=>$device1},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::put (
    desc  => "user1 grants ADMIN to user2",
    path  => "/permission/set",
    params => {'key'=>$device1, 'username'=>$username2, 'permissions'=>'ADMIN'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::delete (
    desc  => "user2 can now delete user1's device, based on ADMIN priv",
    path  => "/device/delete",
    params => {device=>$device1},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


#=============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

