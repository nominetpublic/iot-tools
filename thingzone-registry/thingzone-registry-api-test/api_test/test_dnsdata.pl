# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;

# connect
my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";
(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

my $json;

# login
my %login1 = ThingzoneLogin::login_section('details');
my $sessionKey_user1 = $login1{sessionKey} or die;

#============================================================================
# DNSkeys

$json = HttpHelper::post(
    desc  => 'user1 creates key',
    path  => '/entity/create_key',
    params=>{},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $key=$$json{key} or die;

$json = HttpHelper::get(
    desc  => 'user can see records',
    path  => '/dns/list_records',
    params=>{key=>$key},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# add some records
$json = HttpHelper::put(
    desc  => 'user1 adds DNS record',
    path  => '/dns/add_record',
    params=> {key=>$key, subdomain=>"latest", TTL=>42, RRTYPE=>"A", RDATA=>"1.2.3.4"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put(
    desc  => 'user1 adds DNS record',
    path  => '/dns/add_record',
    params=> {key=>$key, RRTYPE=>"A", RDATA=>"1.2.3.5"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put(
    desc  => 'user1 adds DNS record',
    path  => '/dns/add_record',
    params=> {key=>$key, description=>"an AAAA record", RRTYPE=>"AAAA", RDATA=>"dead::beef"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put(
    desc  => 'user1 adds DNS record',
    path  => '/dns/add_record',
    params=> {key=>$key, description=>"a URI record", RRTYPE=>"URI", RDATA=>"10 10 http://example.com"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put(
    desc  => 'user1 adds DNS record',
    path  => '/dns/add_record',
    params=> {key=>$key, subdomain=>"cname", description=>"a CNAME record", RRTYPE=>"CNAME", RDATA=>"google.com."},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# expect error on dupe
$json = HttpHelper::put(
    desc  => 'user1 adds duplicate DNS record',
    path  => '/dns/add_record',
    params=> {key=>$key, RRTYPE=>"URI", RDATA=>"10 10 http://example.com"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '409';

# expect error on bogus records
$json = HttpHelper::put(
    desc  => 'user1 adds invalid DNS record',
    path  => '/dns/add_record',
    params=> {key=>$key, RRTYPE=>"A", RDATA=>"notta ipv4"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put(
    desc  => 'user1 adds invalid DNS record',
    path  => '/dns/add_record',
    params=> {key=>$key, RRTYPE=>"WTF", RDATA=>"not a real record type"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put(
    desc  => 'user1 adds invalid DNS record',
    path  => '/dns/add_record',
    params=> {key=>$key, RRTYPE=>"A", RDATA=>"deaf::beef"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put(
    desc  => 'user1 adds invalid DNS record',
    path  => '/dns/add_record',
    params=> {key=>$key, RRTYPE=>"AAAA", RDATA=>"1.2.3.4"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put(
    desc  => 'user1 adds invalid DNS record',
    path  => '/dns/add_record',
    params=> {key=>$key, subdomain=>"...", TTL=>42, RRTYPE=>"A", RDATA=>"1.2.3.4"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put(
    desc  => 'user1 adds invalid DNS record',
    path  => '/dns/add_record',
    params=> {key=>$key, subdomain=>"-", TTL=>42, RRTYPE=>"A", RDATA=>"1.2.3.4"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

# expect error on non-existent key
$json = HttpHelper::put(
    desc  => 'user1 adds DNS record to non-existant key',
    path  => '/dns/add_record',
    params=> {key=>'yk7k-3qzb-aaaa-5nh6-hwjs-io3f.u.a', RRTYPE=>"A", RDATA=>"1.2.3.255"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

# let's see what we got
$json = HttpHelper::get(
    desc  => 'get records',
    path  => '/dns/list_records',
    params=>{key=>$key},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
# sample domain must exist
HttpHelper::fail("fail") unless (grep { /example.com/ } map {$$_{'rdata'}} @{$$json{records}}) == 1;

# remove record
$json = HttpHelper::put(
    desc  => 'user1 removes record',
    path  => '/dns/remove_record',
    params=> {key=>$key, RRTYPE=>"URI", RDATA=>"10 10 http://example.com"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# remove record that doesn't exist
$json = HttpHelper::put(
    desc  => 'user1 removes record that has already been removed',
    path  => '/dns/remove_record',
    params=> {key=>$key, RRTYPE=>"URI", RDATA=>"10 10 http://example.com"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

# should now have gone
$json = HttpHelper::get(
    desc  => 'get records',
    path  => '/dns/list_records',
    params=>{key=>$key},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{records}} eq 4;
# sample domain must exist
HttpHelper::fail("fail") unless (grep { /example.com/ } map {$$_{'rdata'}} @{$$json{records}}) == 0;

# sample test record
$json = HttpHelper::put(
    desc  => 'user1 adds invalid DNS record',
    path  => '/dns/add_record',
    params=> {key=>$key, RRTYPE=>"URI", RDATA=>"10 10 http://tools.ietf.org/html/draft-faltstrom-uri"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# let's see the whole world
$json = HttpHelper::get(
    desc  => 'get all records',
    path  => '/dns/list_all_records',
    params=>{key=>$key},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

#===================================================================
# DNSdata on other objects

$json = HttpHelper::post(
    desc  => "create a datastream",
    path  => "/datastream/create",
    params=> {},
    sessionKey=>$sessionKey_user1,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream=$$json{dataStream};

$json = HttpHelper::put(
    desc  => "add record to DataStream",
    path  => "/dns/add_record",
    params => {key=>$dataStream, RRTYPE=>"A", RDATA=>"42.42.42.42"},
    sessionKey=>$sessionKey_user1,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc  => "get records for datastream",
    path  => "/dns/list_records",
    params=> {key=>$dataStream},
    sessionKey=>$sessionKey_user1,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
# sample domain must exist
die unless @{$$json{records}} == 1;

$json = HttpHelper::delete(
    desc  => "delete the datastream",
    path  => "/datastream/delete",
    params=> {dataStream=>$dataStream},
    sessionKey=>$sessionKey_user1,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

#===================================================================
# externally-generated keys

$json = HttpHelper::post(
    desc  => "externally generated key allowed if configured",
    path  => "/entity/create_key",
    params=> {'key'=>'test.microbit'},
    sessionKey=>$sessionKey_user1,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
$key=$$json{key} or die;

#============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

