# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use Data::Dumper;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;

# connect
my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";
(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");


my $json = HttpHelper::post (
    desc  => "user_admin sets initial password (bootstrapping)",
    path  => '/session/login',
    params=> {username=>'user_admin', password=>'user_admin'}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_userAdmin=$$json{sessionKey} or die;


# test user 1
my $unique = time();
my $username1 = "user_permission_1_$unique";
my $password1 = "password_$unique";
my $username2 = "user_permission_2_$unique";
my $password2 = "password_$unique";
my $username_bridge = "bridge_user_$unique";
my $password_bridge = "bridge_user_password_$unique";
my $group1 = "group_1_$unique";
my $group2 = "group_2_$unique";

# create 2x users
$json = HttpHelper::post (
    desc  => "Create user 1 ($username1)",
    path  => '/user/create',
    params=> {username=>$username1, password=>$password1, privileges=>"CREATE_GROUP, CREATE_DEVICE, CREATE_DATASTREAM, CREATE_TRANSFORM, CREATE_RESOURCE, CREATE_TRANSFORMFUNCTION"},
    sessionKey=>$sessionKey_userAdmin
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "Create user 2 ($username2)",
    path  => '/user/create',
    params=> {username=>$username2, password=>$password2, privileges=>"CREATE_DEVICE, CREATE_DATASTREAM"},
    sessionKey=>$sessionKey_userAdmin
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "Create user 'sessionContext'",
    path  => '/user/create',
    params=> {username=>'sessionContext', password=>'sessionContext', privileges=>"USE_SESSION"},
    sessionKey=>$sessionKey_userAdmin
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# create "bridge" user
$json = HttpHelper::post (
    desc  => "Create bridge user ($username_bridge)",
    path  => '/user/create',
    params=> {username=>$username_bridge, password=>$password_bridge, privileges=>"QUERY_STREAMS"},
    sessionKey=>$sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# log in 2x users
$json = HttpHelper::post (
    desc  => "Log in as user 1 ($username1)",
    path  => '/session/login',
    params=> {username=>$username1, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user1=$$json{sessionKey};

$json = HttpHelper::post (
    desc  => "Log in as user 2 ($username2)",
    path  => '/session/login',
    params=> {username=>$username2, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user2=$$json{sessionKey};

$json = HttpHelper::post (
    desc  => "Log in as user 'sessionContext'",
    path  => '/session/login',
    params=> {username=>'sessionContext', password=>'sessionContext'}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_sessionContext=$$json{sessionKey};

$json = HttpHelper::post (
    desc  => "Log in as bridge user ($username_bridge)",
    path  => '/session/login',
    params=> {username=>$username_bridge, password=>$password_bridge}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_bridge=$$json{sessionKey};

$json = HttpHelper::post(
    desc  => "create group 1",
    path  => "/groups/$group1",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post(
    desc  => "create group 2",
    path  => "/groups/$group2",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put(
    desc  => "add users to group 1",
    path  => "/groups/$group1/users/add",
    params => { 'usernames' => "$username1,$username2" },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# each user creates a datastream
$json = HttpHelper::post (
    desc  => "create datastream for user 1",
    path  => "/datastream/create",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream1=$$json{dataStream};

$json = HttpHelper::post (
    desc  => "create datastream for user 2",
    path  => "/datastream/create",
    params => {},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream2=$$json{dataStream};

$json = HttpHelper::put (
    desc  => "user 2 set VIEW_METADATA permission on dataStream 2 for group 1",
    path  => "/permission/set",
    params => {'key'=>$dataStream2, 'group'=>$group1, 'permissions'=>'VIEW_METADATA'},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc  => "user 1 updates permissions to allow user 2, for user1's datastream",
    path  => "/permission/set",
    params => {'key'=>$dataStream1, 'username'=>$username2, 'permissions'=>'CONSUME, DISCOVER'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc  => "user 1 set DISCOVER permission for public user",
    path  => "/permission/set",
    params => {'key'=>$dataStream1, 'username'=>'public', 'permissions'=>'DISCOVER'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# create a function
$json = HttpHelper::put_multipart (
    desc => "user 1 creates function",
    path => "/transform/create_function",
    parts=> HttpHelper::create_multiparts(
        {name=>'name', value=>'testfn'},
        {name=>'functionType', value=>'javascript'},
        {name=>'functionContent', value=>"console.log('test')"}),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::post_multipart (
    desc => "user 1 creates transform",
    path => "/transform/create",
    parts => HttpHelper::create_multiparts(
        {name=>'functionName', value=>'testfn'},
        {name=>'parameterValues', content_type=>'application/json', value=>q#{"parameter1": "foo", "parameter2": 42}#},
        {name=>'runSchedule', value=>'sometimes'},
        ),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $transform1=$$json{transform} or die;

#----------------------------------------------------------------------------
# internal/permission_check

$json = HttpHelper::get (
    desc  => "user1 cannot call permission_check (missing USE_SESSION privilege)",
    path  => "/internal/permission_check",
    params => {
        'keys'=>"$dataStream1, $dataStream2",
        'permissions'=>'DISCOVER, CONSUME',
        'sessionKey'=>$sessionKey_user2,
        },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /authentication error/;
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::get (
    desc  => "user 'sessionContext' returns an error for invalid session",
    path  => "/internal/permission_check",
    params => {
        'keys'=>"$dataStream1, $dataStream2",
        'permissions'=>'DISCOVER, CONSUME',
        'sessionKey'=>'notavalidsession',
        },
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /not found/;
HttpHelper::fail("fail") unless $$json{reason} =~ /reference session/;
HttpHelper::fail("fail") unless $$json{status_code} eq '401';

$json = HttpHelper::get (
    desc  => "user 'sessionContext' returns an error for invalid username",
    path  => "/internal/permission_check",
    params => {
        'keys'=>"$dataStream1, $dataStream2",
        'permissions'=>'DISCOVER, CONSUME',
        'username'=>'notavaliduser',
        },
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /not found/;
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

$json = HttpHelper::get (
    desc  => "user 'sessionContext' checks permissions that exist (DISCOVER+CONSUME) for user2",
    path  => "/internal/permission_check",
    params => {
        'keys'=>"$dataStream1, $dataStream2",
        'permissions'=>'DISCOVER, CONSUME',
        'sessionKey'=>$sessionKey_user2,
        },
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc  => "user 'sessionContext' checks permissions that exist (DISCOVER+CONSUME) for user2 (via username)",
    path  => "/internal/permission_check",
    params => {
        'keys'=>"$dataStream1, $dataStream2",
        'permissions'=>'DISCOVER, CONSUME',
        'username'=>$username2,
        },
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::get (
    desc  => "user 'sessionContext' checks permissions that exist (DISCOVER+CONSUME+ADMIN) for user1",
    path  => "/internal/permission_check",
    params => {
        'keys'=>"$dataStream1",
        'permissions'=>'DISCOVER, CONSUME, ADMIN',
        'sessionKey'=>$sessionKey_user1,
        },
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc  => "user 'sessionContext' checks permissions that exist (VIEW_METADATA) for user2 via group 1",
    path  => "/internal/permission_check",
    params => {
        'keys'=>"$dataStream2",
        'permissions'=>'VIEW_METADATA',
        'sessionKey'=>$sessionKey_user2,
        },
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc  => "user 'sessionContext' checks permissions that don't exist (missing DISCOVER on stream 2) for user1",
    path  => "/internal/permission_check",
    params => {
        'keys'=>"$dataStream1, $dataStream2",
        'permissions'=>'DISCOVER',
        'sessionKey'=>$sessionKey_user1,
        },
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

$json = HttpHelper::get (
    desc  => "user 'sessionContext' checks permissions that don't exist (missing ADMIN on both stream 1) for user2",
    path  => "/internal/permission_check",
    params => {
        'keys'=>"$dataStream1, $dataStream2",
        'permissions'=>'DISCOVER, CONSUME, ADMIN',
        'sessionKey'=>$sessionKey_user2,
        },
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

$json = HttpHelper::get (
    desc  => "user 'sessionContext' checks DISCOVER permission for public user",
    path  => "/internal/permission_check",
    params => {
        'keys'=>"$dataStream1",
        'permissions'=>'DISCOVER',
#        'sessionKey' absent
        },
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::get (
    desc  => "user 'sessionContext' checks (absent) ADMIN permission for public user",
    path  => "/internal/permission_check",
    params => {
        'keys'=>"$dataStream1",
        'permissions'=>'ADMIN',
#        'sessionKey' absent
        },
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /missing permission ADMIN/;
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

$json = HttpHelper::get (
    desc  => "user 'sessionContext' checks (absent) ADMIN permission for public user (as username)",
    path  => "/internal/permission_check",
    params => {
        'keys'=>"$dataStream1",
        'permissions'=>'ADMIN',
        'username'=>'public',
        },
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /missing permission ADMIN/;
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

#----------------------------------------------------------------------------
# internal/permission_filter

$json = HttpHelper::get (
    desc  => "user 'sessionContext' filters permissions that exist (DISCOVER+CONSUME on both streams) for user2",
    path  => "/internal/permission_filter",
    params => {
        'keys'=>"$dataStream1, $dataStream2",
        'permissions'=>'DISCOVER, CONSUME',
        'sessionKey'=>$sessionKey_user2,
        },
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{DNSkeys}} == 2;
HttpHelper::fail("fail") unless grep {/^$dataStream1$/} @{$$json{DNSkeys}};
HttpHelper::fail("fail") unless grep {/^$dataStream2$/} @{$$json{DNSkeys}};


$json = HttpHelper::get (
    desc  => "user 'sessionContext' filters permissions, looking for DISCOVER+CONSUME for user1",
    path  => "/internal/permission_filter",
    params => {
        'keys'=>"$dataStream1, $dataStream2",
        'permissions'=>'DISCOVER, CONSUME',
        'sessionKey'=>$sessionKey_user1,
        },
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{DNSkeys}} == 1;
HttpHelper::fail("fail") unless grep {/^$dataStream1$/} @{$$json{DNSkeys}};    # just own stream


$json = HttpHelper::get (
    desc  => "user 'sessionContext' filters permissions, looking for DISCOVER+CONSUME for user1 (by username)",
    path  => "/internal/permission_filter",
    params => {
        'keys'=>"$dataStream1, $dataStream2",
        'permissions'=>'DISCOVER, CONSUME',
        'username'=>$username1,
        },
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{DNSkeys}} == 1;
HttpHelper::fail("fail") unless grep {/^$dataStream1$/} @{$$json{DNSkeys}};    # just own stream


$json = HttpHelper::get (
    desc  => "user 'sessionContext' filters permissions, looking for DISCOVER for public user",
    path  => "/internal/permission_filter",
    params => {
        'keys'=>"$dataStream1, $dataStream2",
        'permissions'=>'DISCOVER',
#        'sessionKey' absent
        },
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{DNSkeys}} == 1;
HttpHelper::fail("fail") unless grep {/^$dataStream1$/} @{$$json{DNSkeys}};    # just single


$json = HttpHelper::get (
    desc  => "user 'sessionContext' filters permissions, looking for ADMIN for public user",
    path  => "/internal/permission_filter",
    params => {
        'keys'=>"$dataStream1, $dataStream2",
        'permissions'=>'ADMIN',
#        'sessionKey' absent
        },
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{DNSkeys}} == 0;


#----------------------------------------------------------------------------
# internal/permitted_users

$json = HttpHelper::get (
    desc  => "list users with DISCOVER on datastream1 (includes public)",
    path  => "/internal/permitted_users",
    params => {
        'key'=>"$dataStream1",
        'permissions'=>'DISCOVER'
        },
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{users}} == 3;
HttpHelper::fail("fail") unless grep {/^public$/} @{$$json{users}};
HttpHelper::fail("fail") unless grep {/^$username1$/} @{$$json{users}};
HttpHelper::fail("fail") unless grep {/^$username2$/} @{$$json{users}};


$json = HttpHelper::get (
    desc  => "list users with CONSUME & DISCOVER & ADMIN on datastream1",
    path  => "/internal/permitted_users",
    params => {
        'key'=>"$dataStream1",
        'permissions'=>'CONSUME, DISCOVER, ADMIN'
        },
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{users}} == 1;
HttpHelper::fail("fail") unless grep {/^$username1$/} @{$$json{users}};

$json = HttpHelper::get (
    desc  => "list users with VIEW_METADATA on datastream 2",
    path  => "/internal/permitted_users",
    params => {
        'key'=>"$dataStream2",
        'permissions'=>'VIEW_METADATA'
        },
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{users}} == 2;
HttpHelper::fail("fail") unless grep {/^$username1$/} @{$$json{users}};
HttpHelper::fail("fail") unless grep {/^$username2$/} @{$$json{users}};

$json = HttpHelper::get (
    desc  => "negative test: nonexistent datastream",
    path  => "/internal/permitted_users",
    params => {
        'key'=>"nonsense",
        'permissions'=>'DISCOVER'
        },
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

$json = HttpHelper::get (
    desc  => "negative test: bogus permission",
    path  => "/internal/permitted_users",
    params => {
        'key'=>"$dataStream1",
        'permissions'=>'DISCOVER, nonsense'
        },
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::get (
    desc  => "fails without USE_SESSION privilege",
    path  => "/internal/permitted_users",
    params => {
        'key'=>"$dataStream1",
        'permissions'=>'DISCOVER'
        },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /authentication/;
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

#----------------------------------------------------------------------------
# internal/get_username

$json = HttpHelper::get (
    desc  => "user 'sessionContext' can look up user2's sessionKey",
    path  => "/internal/get_username",
    params => {
        'sessionKey'=>$sessionKey_user2,
        },
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{username} eq $username2;
HttpHelper::fail("fail") unless $$json{ttl};


$json = HttpHelper::get (
    desc  => "user 'sessionContext' can look up a bogus sessionKey",
    path  => "/internal/get_username",
    params => {
        'sessionKey'=>'iambogus',
        },
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /not found/;
HttpHelper::fail("fail") unless $$json{status_code} eq '401';

$json = HttpHelper::get (
    desc  => "user2 cannot look up a sessionKey",
    path  => "/internal/get_username",
    params => {'sessionKey'=>$sessionKey_user1},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /authentication/;
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

#----------------------------------------------------------------------------
# internal/permissions_for_key

$json = HttpHelper::get (
    desc  => "user1 cannot call permissions_for_key (missing USE_SESSION privilege)",
    path  => "/internal/permissions_for_key",
    params => {
        'key'=>"$dataStream1",
        },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /authentication error/;
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::get (
    desc  => "user with USE_SESSION can get permissions for specified key",
    path  => "/internal/permissions_for_key",
    params => {
        'key'=>"$dataStream1",
        },
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar(keys %{$$json{permissions}{userPermissions}}) == 3;
HttpHelper::fail("fail") unless @{$$json{permissions}{userPermissions}{$username1}} == 6;
HttpHelper::fail("fail") unless @{$$json{permissions}{userPermissions}{$username2}} == 2;

$json = HttpHelper::get (
    desc  => "user with USE_SESSION can get permissions for specified key",
    path  => "/internal/permissions_for_key",
    params => {
        'key'=>"$dataStream2",
        },
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar(keys %{$$json{permissions}{userPermissions}}) == 1;
HttpHelper::fail("fail") unless @{$$json{permissions}{userPermissions}{$username2}} == 6;
HttpHelper::fail("fail") unless $$json{permissions}{groupPermissions}{$group1};
HttpHelper::fail("fail") unless scalar(keys %{$$json{permissions}{groupPermissions}}) == 1;
HttpHelper::fail("fail") unless @{$$json{permissions}{groupPermissions}{$group1}} == 1;

#----------------------------------------------------------------------------
# internal/user_groups

$json = HttpHelper::get (
    desc  => "get groups for user 1 session key",
    path  => "/internal/user_groups",
    params => {'sessionKey'=>$sessionKey_user1},
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{groups}} == 2;
HttpHelper::fail("fail") unless $$json{groups}[0] eq 'all_users';
HttpHelper::fail("fail") unless $$json{groups}[1] eq $group1;

$json = HttpHelper::put(
    desc  => "add users to group 2",
    path  => "/groups/$group2/users/add",
    params => { 'usernames' => "$username1" },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc  => "get groups for user 1",
    path  => "/internal/user_groups",
    params => {'username'=>$username1},
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{groups}} == 3;

$json = HttpHelper::get (
    desc  => "get groups for user 2",
    path  => "/internal/user_groups",
    params => {'username'=>$username2},
    sessionKey=>$sessionKey_sessionContext
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{groups}} == 2;
HttpHelper::fail("fail") unless $$json{groups}[0] eq 'all_users';
HttpHelper::fail("fail") unless $$json{groups}[1] eq $group1;


#----------------------------------------------------------------------------
# internal/resource

$json = HttpHelper::put_multipart(
    desc => "create a json resource on datastream1",
    path => "/resource/upsert",
    parts=> HttpHelper::create_multiparts(
        {name=>'key', value=>$dataStream1},
        {name=>'resource_name', value=>'data'},
        {name=>'content', value=>q#{"key":"value"}#, content_type=>'application/json', }),
    hide_content=>1,
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc  => "Get the json resource",
    path  => "/internal/resource/$dataStream1/data",
    params => {},
    sessionKey=>$sessionKey_bridge
);
HttpHelper::fail("fail") unless $$json{status_code} eq '200';
HttpHelper::fail("fail") unless $$json{key} eq 'value';

$json = HttpHelper::get(
    desc  => "Get unset resource",
    path  => "/internal/resource/$dataStream1/nothing",
    params => {},
    sessionKey=>$sessionKey_bridge
);
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

$json = HttpHelper::get(
    desc  => "Get the json resource without correct permissions",
    path  => "/internal/resource/$dataStream1/data",
    params => {},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

#----------------------------------------------------------------------------
# internal/push_subscribers

# set push subscriber on dataStream
$json = HttpHelper::put_multipart (
    desc  => "Add first push subscriber",
    path  => "/push_subscribers/set",
    parts => HttpHelper::create_multiparts(
        {name=>'dataStream', value=>$dataStream1},
        {name=>'uri', value=>'http://example.com'},
        {name=>'uriParams', content_type=>'application/json', value=>q#{"httpHeaders":{"SessionHeaderId": "1234"} }#}),
    sessionKey=>$sessionKey_user1);

$json = HttpHelper::get(
    desc  => "check new push subscriber",
    path  => "/push_subscribers/get",
    params=> {'dataStream' => $dataStream1},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{pushSubscribers}[0]{uriParams}{httpHeaders}{SessionHeaderId} eq '1234';
HttpHelper::fail("fail") if defined $$json{pushSubscribers}[0]{headers};
HttpHelper::fail("fail") unless @{$$json{pushSubscribers}} == 1;

$json = HttpHelper::get(
    desc  => "list push subscribers for bridge",
    path  => "/internal/push_subscribers",
    params=> {},
    sessionKey=>$sessionKey_bridge
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{pushSubscribers}} == 1;
HttpHelper::fail("fail") unless $$json{pushSubscribers}[0]{dataStream}{key} eq $dataStream1;

$json = HttpHelper::get(
    desc  => "Auth error for non query_streams users",
    path  => "/internal/push_subscribers",
    params=> {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} eq 'authentication error';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

#----------------------------------------------------------------------------
# internal/identify

$json = HttpHelper::get (
    desc  => "user can identify their own datastream (normal API)",
    path  => "/search/identify",
    params=> {'key' => $dataStream1},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc  => "user cannot use /internal API",
    path  => "/internal/identify",
    params=> {'key' => $dataStream1},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} eq 'authentication error';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::get (
    desc  => "bridge user can use /internal/identify",
    path  => "/internal/identify",
    params=> {'key' => $dataStream1},
    sessionKey=>$sessionKey_bridge
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream};
HttpHelper::fail("fail") unless $$json{dataStream}{key};

$json = HttpHelper::put (
    desc  => "user 1 set DISCOVER permission for public user",
    path  => "/permission/set",
    params => {'key'=>$dataStream1, 'username'=>'public', 'permissions'=>''},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc  => "bridge user can still use /internal/identify",
    path  => "/internal/identify",
    params=> {'key' => $dataStream1},
    sessionKey=>$sessionKey_bridge
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream};
HttpHelper::fail("fail") unless $$json{dataStream}{key};

# test device internal identify
$json = HttpHelper::post (
    desc  => "create device for user 1",
    path  => "/device/create",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $device1=$$json{device} or die;

$json = HttpHelper::put (
    desc  => "add stream to device",
    path  => "/device/add_stream",
    params => {'device' => $device1, 'dataStream' => $dataStream1, 'name' => 'd1_s1'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc  => "internal identify device",
    path  => "/internal/identify",
    params=> {'key' => $device1},
    sessionKey=>$sessionKey_bridge
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{'device'};
HttpHelper::fail("fail") unless @{$$json{device}{dataStreams}} == 1;

$json = HttpHelper::get (
    desc  => "internal identify device with full detail",
    path  => "/internal/identify",
    params=> {'key' => $device1, 'detail' => 'full', 'showPermissions'=>'true'},
    sessionKey=>$sessionKey_bridge
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{'device'};
HttpHelper::fail("fail") unless $$json{device}{dataStreams}[0]{key} eq $dataStream1;

$json = HttpHelper::get (
    desc  => "Get datastream details",
    path  => "/internal/identify",
    params=> {'key' => $dataStream1, 'detail' => 'full', 'showPermissions'=>'true'},
    sessionKey=>$sessionKey_bridge
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{'dataStream'};
HttpHelper::fail("fail") unless $$json{dataStream}{devices}[0]{key} eq $device1;

$json = HttpHelper::get (
    desc  => "user can identify their own transform (normal API)",
    path  => "/internal/identify",
    params=> {'key' => $transform1, 'detail'=>'full', 'showPermissions'=>'true'},
    sessionKey=>$sessionKey_bridge
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

#=============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

