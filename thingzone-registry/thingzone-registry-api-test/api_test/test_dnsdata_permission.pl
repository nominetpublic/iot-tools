# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $TRUE=1;
my $FALSE=0;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;

# connect
my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";
(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

my $json;
my $sessionKey_userAdmin=ThingzoneLogin::login_admin_user() or die;


# test user 1
my $unique = time();
my $username1 = "user_device_perm_1_$unique";
my $password1 = "password_$unique";
my $username2 = "user_device_perm_2_$unique";
my $password2 = "password_$unique";

# create 2x users
$json = HttpHelper::post (
    desc  => "Create user 1 ($username1)",
    path  => '/user/create',
    params=> {username=>$username1, password=>$password1, privileges=>"CREATE_ENTITY"},
    sessionKey=>$sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::post (
    desc  => "Create user 2 ($username2)",
    path  => '/user/create',
    params=> {username=>$username2, password=>$password2, privileges=>"CREATE_ENTITY"},
    sessionKey=>$sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# log in 2x users
$json = HttpHelper::post (
    desc  => "Log in as user 1 ($username1)",
    path  => '/session/login',
    params=> {username=>$username1, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user1=$$json{sessionKey};


$json = HttpHelper::post (
    desc  => "Log in as user 2 ($username2)",
    path  => '/session/login',
    params=> {username=>$username2, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user2=$$json{sessionKey};


$json = HttpHelper::post (
    desc  => 'user1 creates key',
    path  => '/entity/create_key',
    params=>{},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $key1=$$json{key} or die;


$json = HttpHelper::post (
    desc  => 'user2 creates key',
    path  => '/entity/create_key',
    params=>{},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $key2=$$json{key} or die;


$json = HttpHelper::put (
    desc  => 'user1 cannot add record to user2s key',
    path  => '/dns/add_record',
    params=>{key=>$key2, subdomain=>"latest", TTL=>42, RRTYPE=>"A", RDATA=>"1.2.3.4"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} eq 'authentication error';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::put (
    desc  => 'user1 cannot remove record from user2s key',
    path  => '/dns/remove_record',
    params=>{key=>$key2, subdomain=>"latest", RRTYPE=>"A", RDATA=>"1.2.3.4"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} eq 'authentication error';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';


$json = HttpHelper::put (
    desc  => 'user1 adds DNS record',
    path  => '/dns/add_record',
    params=>{key=>$key1, subdomain=>"latest", TTL=>42, RRTYPE=>"A", RDATA=>"1.2.3.4"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc  => 'user2 adds DNS record',
    path  => '/dns/add_record',
    params=>{key=>$key2, subdomain=>"latest", TTL=>42, RRTYPE=>"A", RDATA=>"1.2.3.4"},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::get (
    desc  => 'user1 can see own records',
    path  => '/dns/list_records',
    params=>{key=>$key1},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc  => 'user1 cannot see user2s records',
    path  => '/dns/list_records',
    params=>{key=>$key2},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} eq 'authentication error';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::put (
    desc  => "user 1 updates privileges to allow user 2, for user1's key",
    path  => "/permission/set",
    params => {'key'=>$key1, 'username'=>$username2, 'permissions'=>'DISCOVER'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::get (
    desc  => 'user2 can now see user1s records',
    path  => '/dns/list_records',
    params=>{key=>$key1},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::get (
    desc  => 'user1 only sees own records in list-all',
    path  => '/dns/list_all_records',
    params=>{},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{keys}} == 1;


$json = HttpHelper::get (
    desc  => 'user2 also sees user1 records in list-all',
    path  => '/dns/list_all_records',
    params=>{},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{keys}} == 2;


#=============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

