# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;


# connect
my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";
(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");


my $json = HttpHelper::post (
    desc  => "user_admin can set initial password (bootstrapping)",
    path  => '/session/login',
    params=> {username=>'user_admin', password=>'user_admin'}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_userAdmin=$$json{sessionKey} or die;

# create a user
my $unique = time();
my $username = "user_$unique";
my $password = "password_$unique";

$json = HttpHelper::post (
    desc  => "create user",
    path  => '/user/create',
    params=> {username=>$username, password=>$password},
    sessionKey=>$sessionKey_userAdmin
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# login
$json = HttpHelper::post (
    desc  => "log in",
    path  => '/session/login',
    params=>{username=>$username, password=>$password},
    sessionKey => undef);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{sessionKey};
HttpHelper::fail("fail") unless $$json{ttl};
my $sessionKey=$$json{sessionKey};


###############################################

print "sleep 2\n";
sleep 2;

$json = HttpHelper::post (
    desc  => "keepalive",
    path  => "/session/keepalive",
    params => {},
    sessionKey => $sessionKey
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{ttl};

###############################################
# optional 5-second time test
# NB set SessionTimeout=5000 in web.properties

my $TEST_5_SECOND_TIMEOUT = 0;
#$TEST_5_SECOND_TIMEOUT = 1;
if ($TEST_5_SECOND_TIMEOUT) {
    print "sleep 4\n";
    sleep 4;

    $json = HttpHelper::post (
        desc  => "keepalive after 4s",
        path  => "/session/keepalive",
        params => {},
        sessionKey => $sessionKey
        );
    HttpHelper::fail("fail") unless $$json{result} eq 'ok';

    print "sleep 4\n";
    sleep 4;

    $json = HttpHelper::get (
        desc  => "misc request after 4s",
        path  => "/device/list",
        params => {},
        sessionKey => $sessionKey
        );
    HttpHelper::fail("fail") unless $$json{result} eq 'ok';

    print "sleep 4\n";
    sleep 4;

    $json = HttpHelper::post (
        desc  => "keepalive after 4s",
        path  => "/session/keepalive",
        params => {},
        sessionKey => $sessionKey
        );
    HttpHelper::fail("fail") unless $$json{result} eq 'ok';

    print "sleep 6\n";
    sleep 6;

    # should fail
    $json = HttpHelper::post (
        desc  => "keepalive after 5s should fail if SessionTimeout=5000",
        path  => "/session/keepalive",
        params => {},
        sessionKey => $sessionKey
        );
    HttpHelper::fail("fail") unless $$json{result} eq 'fail';
    HttpHelper::fail("fail") unless $$json{reason} eq 'session has expired';
    HttpHelper::fail("fail") unless $$json{status_code} eq '401';

    # log back in again
    $json = HttpHelper::post (
        desc  => "log back in again",
        path  => '/session/login',
        params=> {username=>$username, password=>$password},
        sessionKey => undef
        );
    HttpHelper::fail("fail") unless $$json{result} eq 'ok';
    $sessionKey=$$json{sessionKey};
}


$json = HttpHelper::post (
    desc  => "logout",
    path  => "/session/logout",
    params => {},
    sessionKey => $sessionKey
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::post (
    desc  => "logged-out session keepalive should fail",
    path  => "/session/keepalive",
    params =>{},
    sessionKey => $sessionKey
    );
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '401';

$json = HttpHelper::post (
    desc  => "nonsense session keepalive should fail",
    path  => "/session/keepalive",
    params =>{},
    sessionKey => 'nonsense-keepalive'
    );
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '401';

$json = HttpHelper::get (
    desc  => "public operation succeeds with absent sessionKey",
    path  => "/user/get",
    params => {},
    sessionKey=>undef
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
die unless defined $$json{user};
HttpHelper::fail("fail") unless $$json{user}{username} eq 'public';


$json = HttpHelper::get (
    desc  => "public operation fails with bogus sessionKey",
    path  => "/user/get",
    params => {},
    sessionKey=>'nonsense-user-get'
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '401';

# multiple logins

$json = HttpHelper::post (
    desc  => "log in",
    path  => '/session/login',
    params=> {username=>$username, password=>$password},
    sessionKey => undef
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey1=$$json{sessionKey};

$json = HttpHelper::post (
    desc  => "duplicate login as same user",
    path  => '/session/login',
    params=> {username=>$username, password=>$password},
    sessionKey => undef
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey2=$$json{sessionKey};


$json = HttpHelper::post (
    desc  => "session 1 is valid",
    path  => "/session/keepalive",
    params =>{},
    sessionKey => $sessionKey1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "session 2 is valid",
    path  => "/session/keepalive",
    params =>{},
    sessionKey => $sessionKey2
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


#============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

