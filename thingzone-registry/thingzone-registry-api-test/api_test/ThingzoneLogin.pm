#!/usr/bin/perl -w
use strict;

package ThingzoneLogin;


use HttpHelper;


# create a user for test section, and login
# returns username/password/sessionKey
sub login_section {
	my $section = shift or die;

	# create a user
	my $unique = time();
	my $username = "user_${section}_$unique";
	my $password = "password_$unique";

	my $json = HttpHelper::post (
		desc  => "ThingzoneLogin: user_admin sets initial password (bootstrapping)",
		path  => '/session/login',
		params=> {username=>'user_admin', password=>'user_admin'}
	);
	HttpHelper::fail("fail") unless $$json{result} eq 'ok';
	my $sessionKey_userAdmin=$$json{sessionKey} or die;

	$json = HttpHelper::post (
		desc  => "ThingzoneLogin: create user $username",
		path  => '/user/create',
		params=> {username=>$username, password=>$password, privileges=>"CREATE_DEVICE, CREATE_DATASTREAM, CREATE_TRANSFORM, CREATE_TRANSFORMFUNCTION, CREATE_RESOURCE, CREATE_ENTITY"},
		sessionKey=>$sessionKey_userAdmin
	);
	HttpHelper::fail("fail") unless $$json{result} eq 'ok';

	$json = HttpHelper::post ('/session/login', {username=>$username, password=>$password});
	HttpHelper::fail("fail") unless $$json{result} eq 'ok';
	my $sessionKey=$$json{sessionKey};

	# store sessionKey in headers
	HttpHelper::set_header('X-FollyBridge-SessionKey', $sessionKey);

	return (username=>$username, password=>$password, sessionKey=>$sessionKey);
}


sub login_admin_user {
	my $json = HttpHelper::post (
		desc  => "user_admin sets initial password (bootstrapping)",
		path  => '/session/login',
		params=> {username=>'user_admin', password=>'user_admin'}
	);
	HttpHelper::fail("fail") unless $$json{result} eq 'ok';
	my $sessionKey_userAdmin=$$json{sessionKey} or die;
	return $sessionKey_userAdmin;
}


1;
