# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;

# connect
my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";
(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

# login
my %login1 = ThingzoneLogin::login_section('device');
my $sessionKey_user1 = $login1{sessionKey} or die;
my $json;


$json = HttpHelper::get ("/device/list", {});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::post ("/device/create", {});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $device=$$json{device};


$json = HttpHelper::get ("/device/list", {});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
die "newly created device not found"
    unless (grep { /$device/ } @{$$json{devices}}) == 1;


$json = HttpHelper::get ("/device/get", {"device" => $device});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

#============================================================================
# create with name and type

$json = HttpHelper::post(desc  => "create device_name with entity name",
    path  => "/device/create",
    params => { "name"=>"key name"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $device_name=$$json{device} or die;


$json = HttpHelper::get(
    desc  => "check that device_name has a name",
    path  => "/search/identify",
    params=> {'key' => $device_name},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{device}{name} eq 'key name';


$json = HttpHelper::post(desc  => "create device_name_type with entity name and type",
    path  => "/device/create",
    params => { "name"=>"device_name_type", "type"=>"camera"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $device_name_type=$$json{device} or die;


$json = HttpHelper::get(desc  => "Check device_name_type created properly",
    path  => "/device/get",
    params => { "device"=>$device_name_type},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{device}{name} eq "device_name_type";
HttpHelper::fail("fail") unless $$json{device}{type} eq "camera";

#==========================================================================
# updating

$json = HttpHelper::put_multipart ("/device/update", HttpHelper::create_multiparts(
    {name=>'device', value=>$device},
    {name=>'metadata', content_type=>'application/json', value=>q#{"stringVal":"forty three","intVal":43, "test":"something different"}#},
    ));
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get ("/device/get", {"device" => $device});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{device}{metadata}{stringVal} eq "forty three";


# helpful error message with bogus JSON
$json = HttpHelper::put_multipart ("/device/update", HttpHelper::create_multiparts(
    {name=>'device', value=>$device},
    {name=>'metadata', content_type=>'application/json', value=>q#{" this is bogus json! [[}}#},
    ));
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /invalid input syntax for type json/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';


$json = HttpHelper::put_multipart(
    desc  => "update device name",
    path  => "/device/update",
    parts => HttpHelper::create_multiparts(
        {name=>'device', value=>$device},
        {name=>'name', value=>"device name"}),
    sessionKey=>$sessionKey_user1);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::get (
    desc  => "name has been updated",
    path  => "/device/get",
    params => {"device" => $device},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{device}{name} eq "device name";


$json = HttpHelper::put_multipart(
    desc  => "update device type",
    path  => "/device/update",
    parts => HttpHelper::create_multiparts(
        {name=>'device', value=>$device},
        {name=>'type', value=>"device type"}),
    sessionKey=>$sessionKey_user1);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::get (
    desc  => "type has been updated",
    path  => "/device/get",
    params => {"device" => $device},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{device}{name} eq "device name";
HttpHelper::fail("fail") unless $$json{device}{type} eq "device type";


$json = HttpHelper::put_multipart(
    desc  => "helpful error message for bogus JSON",
    path  => "/device/update",
    parts => HttpHelper::create_multiparts(
        {name=>'device', value=>$device},
        {name=>'location', content_type=>'application/json', value=>q#{" this is bogus json! [[}}#}),
    sessionKey=>$sessionKey_user1);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /invalid input syntax for type json/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';


$json = HttpHelper::put_multipart (
    desc  => "set location",
    path  => "/device/update",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device},
        {name=>'location', content_type=>'application/json', value=>q#{"latitude":"51.507351", "longitude":"-0.127758"}#}),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::get (
    desc  => "location has been updated",
    path  => "/device/get",
    params => {"device" => $device},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{device}{location};
HttpHelper::fail("fail") unless $$json{device}{location}{latitude} eq "51.507351";


$json = HttpHelper::put_multipart (
    desc  => "unset location",
    path  => "/device/update",
    parts => HttpHelper::create_multiparts(
        {name=>"device", value => $device},
        {name=>'location', content_type=>'application/json', value=>""}
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
die if $$json{device}{location};


$json = HttpHelper::get (
    desc  => "location is no longer set",
    path  => "/device/get",
    params => {"device" => $device},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
die if $$json{device}{location};


# check permissions...
my %login2 = ThingzoneLogin::login_section('device2');

$json = HttpHelper::get (
    desc  => "Another user cannot see anothers user devices",
    path  => '/device/list',
    params=> {},
    sessionKey=>$login2{sessionKey}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
die unless @{$$json{devices}} == 0;


$json = HttpHelper::put (
    desc  => "user 1 updates privileges to device",
    path  => "/permission/set",
    params => {'key'=>$device, 'username'=>$login2{username}, 'permissions'=>'CONSUME, DISCOVER'},
    sessionKey=>$login1{sessionKey}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::get (
    desc  => "Another user can now see first users device",
    path  => '/device/list',
    params=> {},
    sessionKey=>$login2{sessionKey}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
die unless @{$$json{devices}} == 1;

#============================================================================
# deletion

$json = HttpHelper::delete (
    desc  => "user1 deletes device",
    path  => "/device/delete",
    params => {device=>$device},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

#============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

