# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use Data::Dumper;
use POSIX qw(strftime);
use DateTime;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;


my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";

(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

my $json;
my $sessionKey_userAdmin=ThingzoneLogin::login_admin_user() or die;

#============================================================================
# test prerequisites

sleep(2);
my $all_changes = strftime "%Y-%m-%dT%H:%M:%S%z", localtime;

# test user 1
my $unique = time();
my $username1 = "user_device_dump_1_$unique";
my $password1 = "password_$unique";
my $username2 = "user_device_dump_2_$unique";
my $password2 = "password_$unique";
my $group1 = "group_dump_1_$unique";

# create 2x users
$json = HttpHelper::post (
    desc  => "Create user 1 ($username1)",
    path  => '/user/create',
    params=> {username=>$username1, password=>$password1, privileges=>"CREATE_GROUP, CREATE_DEVICE, CREATE_DATASTREAM, CREATE_TRANSFORM, CREATE_TRANSFORMFUNCTION"},
    sessionKey=>$sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "Create user 2 ($username2)",
    path  => '/user/create',
    params=> {username=>$username2, password=>$password2, privileges=>"CREATE_DEVICE, CREATE_DATASTREAM, CREATE_TRANSFORM, CREATE_TRANSFORMFUNCTION"},
    sessionKey=>$sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# create users for dump
$json = HttpHelper::post (
    desc  => "Create user dumper",
    path  => '/user/create',
    params=> {username=>'dumper', password=>'dumper', privileges=>"DUMP_ALL"},
    sessionKey=>$sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# log in 2x users
$json = HttpHelper::post (
    desc  => "Log in as user 1 ($username1)",
    path  => '/session/login',
    params=> {username=>$username1, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user1=$$json{sessionKey};


$json = HttpHelper::post (
    desc  => "Log in as user 2 ($username2)",
    path  => '/session/login',
    params=> {username=>$username2, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user2=$$json{sessionKey};


$json = HttpHelper::post (
    desc  => "Log in as user 'dumper'",
    path  => '/session/login',
    params=> {username=>'dumper', password=>'dumper'}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_dumper=$$json{sessionKey};

$json = HttpHelper::post(
    desc  => "create group 1",
    path  => "/groups/$group1",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put(
    desc  => "add users to group 1",
    path  => "/groups/$group1/users/add",
    params => { 'usernames' => "$username1,$username2" },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


#============================================================================

$json = HttpHelper::post (
    desc  => "create datastream",
    path  => "/datastream/create",
    params=>{},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream1=$$json{dataStream};

$json = HttpHelper::post (
    desc  => "create datastream",
    path  => "/datastream/create",
    params=>{},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream2=$$json{dataStream};

$json = HttpHelper::post (
    desc  => "create datastream",
    path  => "/datastream/create",
    params=>{},
    sessionKey=>$sessionKey_user2
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream3=$$json{dataStream};

# grant permissions...
$json = HttpHelper::put (
    desc  => "user 1 grants DISCOVER on datastream 1 to public",
    path  => "/permission/set",
    params => {'key'=>$dataStream1, 'username'=>'public', 'permissions'=>'DISCOVER'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc  => "user 1 grants DISCOVER on datastream 1 to group 1",
    path  => "/permission/set",
    params => {'key'=>$dataStream1, 'group'=>$group1, 'permissions'=>'DISCOVER'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::put (
    desc  => "user 1 grants DISCOVER on datastream 2 to public",
    path  => "/permission/set",
    params => {'key'=>$dataStream2, 'username'=>'public', 'permissions'=>'DISCOVER'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc  => "user 1 grants all on datastream 2 to user2",
    path  => "/permission/set",
    params => {'key'=>$dataStream2, 'username'=>$username2, 'permissions'=>'ADMIN,UPLOAD,CONSUME,MODIFY,ANNOTATE,DISCOVER'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put_multipart (
    desc  => "add push subscriber",
    path  => "/push_subscribers/set",
    parts => HttpHelper::create_multiparts(
        {name=>'dataStream', value=>$dataStream1},
        {name=>'uri', value=>'http://transform-push.example.com'},
        {name=>'uriParams', content_type=>'application/json', value=>q#{"httpHeaders":{"SessionHeaderId": "asda"} }#},
        ),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::put_multipart (
    desc  => "add pull subscriber",
    path  => "/pull_subscription/set",
    parts => HttpHelper::create_multiparts(
        {name=>'dataStream', value=>$dataStream1},
        {name=>'uri', value=>'http://example.com'},
        {name=>'uriParams', content_type=>'application/json', value=>q#{"httpHeaders":{"SessionHeaderId": "1234"} }#},
        {name=>'interval', value=>42},
        ),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc  => "add DNS record to datastream",
    path  => "/dns/add_record",
    params=> {key=>$dataStream1, description=>'dns on dataStream', RRTYPE=>"A", RDATA=>"1.2.3.4"},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::post (
    desc  => "create device",
    path  => "/device/create",
    params=> {},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $device1=$$json{device};

$json = HttpHelper::post (
    desc  => "create device",
    path  => "/device/create",
    params=> {},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $device2=$$json{device};

$json = HttpHelper::post (
    desc  => "create device",
    path  => "/device/create",
    params=> {},
    sessionKey=>$sessionKey_user2
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $device3=$$json{device};


$json = HttpHelper::put (
    desc  => "add stream to device",
    path  => "/device/add_stream",
    params=>{'device' => $device1, 'dataStream' => $dataStream1, 'name' => 's1'},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::put (
    desc  => "add DNS record to device",
    path  => "/dns/add_record",
    params=> {key=>$device1, description=>'dns on device', RRTYPE=>"A", RDATA=>"1.2.3.4", subdomain=>"arse"},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# create a function
$json = HttpHelper::put_multipart (
    desc => "user 1 creates function",
    path => "/transform/create_function",
    parts=> HttpHelper::create_multiparts(
        {name=>'name', value=>'testfn'},
        {name=>'functionType', value=>'javascript'},
        {name=>'functionContent', value=>"console.log('test')"}),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# create a transform
$json = HttpHelper::post_multipart (
    desc => "create transform",
    path => "/transform/create",
    parts => HttpHelper::create_multiparts(
        {name=>'functionName', value=>'testfn'},
        {name=>'functionIsPublic', value=>'true'},
        {name=>'parameterValues', content_type=>'application/json', value=>q#{"value":"foo"}#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $transform1=$$json{transform};
die unless $transform1;

# add a transform source
$json = HttpHelper::put (
    desc => "add source to transform",
    path => "/transform/update_source",
    params => {'transform' => $transform1, 'stream' => $dataStream1, 'alias' => 'my favourite stream'},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc => "add outputStream to transform",
    path => "/transform/update_outputStream",
    params => {'transform' => $transform1, 'stream' => $dataStream2, 'alias' => 'output2'},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# set push subscriber on transform's datastream
$json = HttpHelper::put_multipart (
    desc  => "add push subscriber",
    path  => "/push_subscribers/set",
    parts => HttpHelper::create_multiparts(
        {name=>'dataStream', value=>$transform1},
        {name=>'uri', value=>'http://transform-push.example.com'},
        {name=>'uriParams', content_type=>'application/json', value=>q#{"httpHeaders":{"SessionHeaderId": "asda"} }#},
        ),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::put (
    desc  => "add transform to device",
    path  => "/device/add_stream",
    params=>{'device' => $device1, 'dataStream' => $transform1, 'name' => 't1'},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

#============================================================================
# dump tests

# should refuse sessionKey
$json = HttpHelper::get (
    desc  => "dump should fail if requested by user1",
    path  => "/dump/all",
    params=>{},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless ref $json eq 'HASH';    # normal JSON response
HttpHelper::fail("fail") unless $$json{result} eq 'fail';

$json = HttpHelper::get (
    desc  => "dump should return all data for user 'dumper'",
    path  => "/dump/all",
    params=> {},
    sessionKey=>$sessionKey_dumper,
    print_raw_json => 1
    );
HttpHelper::fail("fail") unless ref $json eq 'ARRAY';    # special JSON response for dump, so it's streamable


# NB! this json is in different format, i.e. array of objects

#print Dumper $json;
#for my $obj (@$json) {
#    print Dumper($obj)."\n";
#}

# extract & put into hash key -> object for easier checking...
my %lookfor = (
    'dataStream' => [
        $dataStream1,
        $dataStream2,
        $dataStream3,
    ],
    'device' => [
        $device1,
        $device2,
        $device3,
    ],
    'transform' => [
        $transform1,
    ],
    );
my $result;
my %found;
for my $obj (@$json) {
    if (defined $$obj{result}) {
        $result = $$obj{result};
    }

    for my $lookfor_type (keys %lookfor) {
        for my $lookfor_key(@{$lookfor{$lookfor_type}}) {
#            print "looking for $lookfor_key under $lookfor_type\n";
            if ((defined $$obj{$lookfor_type}{key} && $$obj{$lookfor_type}{key} eq $lookfor_key)
            || (defined $$obj{$lookfor_type}{outputStream}{key} && $$obj{$lookfor_type}{outputStream}{key} eq $lookfor_key)) {
                $found{$lookfor_key}{$lookfor_type} = $$obj{$lookfor_type};
                if (defined $$obj{userPermissions}) {
                    $found{$lookfor_key}{userPermissions} = $$obj{userPermissions};
                }
                if (defined $$obj{devices}) {
                    $found{$lookfor_key}{devices} = $$obj{devices};
                }
            }
        }
    }
}
HttpHelper::fail("fail") unless $result eq 'ok';

#use Data::Printer;
#print STDERR "found="; p(%found);

print "checking datastream $dataStream1\n";
HttpHelper::fail("fail") unless $found{$dataStream1};
HttpHelper::fail("fail") unless $found{$dataStream1}{dataStream}{key};
HttpHelper::fail("fail") unless $found{$dataStream1}{dataStream}{dnsRecords};
HttpHelper::fail("fail") unless scalar(@{$found{$dataStream1}{dataStream}{dnsRecords}}) == 1;
HttpHelper::fail("fail") unless $found{$dataStream1}{dataStream}{userPermissions};
HttpHelper::fail("fail") unless $found{$dataStream1}{dataStream}{userPermissions}{public};
HttpHelper::fail("fail") unless $found{$dataStream1}{dataStream}{userPermissions}{$username1};
HttpHelper::fail("fail") unless scalar(keys %{$found{$dataStream1}{dataStream}{userPermissions}}) == 2;
HttpHelper::fail("fail") unless scalar(@{$found{$dataStream1}{dataStream}{devices}}) == 1;
HttpHelper::fail("fail") unless ${$found{$dataStream1}{dataStream}{devices}}[0]{key} eq $device1;
HttpHelper::fail("fail") unless ${$found{$dataStream1}{dataStream}{devices}}[0]{linkName} eq 's1';

print "checking datastream $dataStream2\n";
HttpHelper::fail("fail") unless $found{$dataStream2};
HttpHelper::fail("fail") unless $found{$dataStream2}{dataStream}{key};
HttpHelper::fail("fail") unless $found{$dataStream2}{dataStream}{userPermissions};
HttpHelper::fail("fail") unless $found{$dataStream2}{dataStream}{userPermissions}{public};
HttpHelper::fail("fail") unless $found{$dataStream2}{dataStream}{userPermissions}{$username1};
HttpHelper::fail("fail") unless $found{$dataStream2}{dataStream}{userPermissions}{$username2};
HttpHelper::fail("fail") unless scalar(keys %{$found{$dataStream2}{dataStream}{userPermissions}}) == 3;

HttpHelper::fail("fail") unless scalar(@{$found{$dataStream2}{dataStream}{userPermissions}{public}}) == 1;
HttpHelper::fail("fail") unless scalar(@{$found{$dataStream2}{dataStream}{userPermissions}{$username1}}) == 6;
HttpHelper::fail("fail") unless scalar(@{$found{$dataStream2}{dataStream}{userPermissions}{$username2}}) == 6;

print "checking datastream $dataStream3\n";
HttpHelper::fail("fail") unless $found{$dataStream3};
HttpHelper::fail("fail") unless $found{$dataStream3}{dataStream}{key};
HttpHelper::fail("fail") unless $found{$dataStream3}{dataStream}{userPermissions};
HttpHelper::fail("fail") unless $found{$dataStream3}{dataStream}{userPermissions}{$username2};
HttpHelper::fail("fail") unless scalar(keys %{$found{$dataStream3}{dataStream}{userPermissions}}) == 1;

print "checking device $device1\n";
HttpHelper::fail("fail") unless $found{$device1};
HttpHelper::fail("fail") unless $found{$device1}{device}{key};
HttpHelper::fail("fail") unless $found{$device1}{device}{userPermissions};
HttpHelper::fail("fail") unless $found{$device1}{device}{userPermissions}{$username1};
HttpHelper::fail("fail") unless $found{$device1}{device}{dnsRecords};
HttpHelper::fail("fail") unless scalar(@{$found{$device1}{device}{dnsRecords}}) == 1;

print "checking device $device2\n";
HttpHelper::fail("fail") unless $found{$device2};
HttpHelper::fail("fail") unless $found{$device2}{device}{key};
HttpHelper::fail("fail") unless $found{$device2}{device}{userPermissions};
HttpHelper::fail("fail") unless $found{$device2}{device}{userPermissions}{$username1};
HttpHelper::fail("fail") if $found{$device2}{device}{dnsRecords};

print "checking device $device2\n";
HttpHelper::fail("fail") unless $found{$device3};
HttpHelper::fail("fail") unless $found{$device3}{device}{key};
HttpHelper::fail("fail") unless $found{$device3}{device}{userPermissions};
HttpHelper::fail("fail") unless $found{$device3}{device}{userPermissions}{$username2};
HttpHelper::fail("fail") if $found{$device3}{device}{dnsRecords};

print "checking transform $transform1\n";
HttpHelper::fail("fail") unless $found{$transform1};
HttpHelper::fail("fail") unless $found{$transform1}{transform}{key};
HttpHelper::fail("fail") unless $found{$transform1}{transform}{userPermissions};
HttpHelper::fail("fail") unless $found{$transform1}{transform}{userPermissions}{$username1};
HttpHelper::fail("fail") if $found{$transform1}{transform}{dnsRecords};
HttpHelper::fail("fail") unless scalar(@{$found{$transform1}{transform}{userPermissions}{$username1}}) == 6;
HttpHelper::fail("fail") unless scalar(@{$found{$transform1}{transform}{devices}}) == 1;
HttpHelper::fail("fail") unless ${$found{$transform1}{transform}{devices}}[0]{key} eq $device1;
HttpHelper::fail("fail") unless ${$found{$transform1}{transform}{devices}}[0]{linkName} eq 't1';
HttpHelper::fail("fail") unless scalar(@{$found{$transform1}{transform}{sources}}) == 1;
HttpHelper::fail("fail") unless ${$found{$transform1}{transform}{sources}}[0]{source} eq $dataStream1;
HttpHelper::fail("fail") unless ${$found{$transform1}{transform}{sources}}[0]{alias} eq 'my favourite stream';
HttpHelper::fail("fail") unless scalar(@{$found{$transform1}{transform}{outputStreams}}) == 1;
HttpHelper::fail("fail") unless ${$found{$transform1}{transform}{outputStreams}}[0]{key} eq $dataStream2;
HttpHelper::fail("fail") unless ${$found{$transform1}{transform}{outputStreams}}[0]{alias} eq 'output2';

#============================================================================

$json = HttpHelper::get (
    desc  => "dump should fail if requested by user1",
    path  => "/dump/all",
    params=>{},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless ref $json eq 'HASH';    # normal JSON response
HttpHelper::fail("fail") unless $$json{result} eq 'fail';

$json = HttpHelper::get (
    desc  => "get datastream properties including parent devices",
    path  => "/dump/datastream",
    params => {'dataStream'=>$dataStream1},
    sessionKey=>$sessionKey_dumper
);
HttpHelper::fail("fail") unless ref $json eq 'HASH';    # normal JSON response
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream};
HttpHelper::fail("fail") unless $$json{dataStream}{devices};
HttpHelper::fail("fail") unless $$json{dataStream}{userPermissions};
HttpHelper::fail("fail") unless $$json{dataStream}{key} eq $dataStream1;
HttpHelper::fail("fail") unless scalar(@{$$json{dataStream}{devices}}) == 1;
HttpHelper::fail("fail") unless $$json{dataStream}{devices}[0]{key} eq $device1;
HttpHelper::fail("fail") unless keys %{$$json{dataStream}{userPermissions}} == 2;
HttpHelper::fail("fail") unless $$json{dataStream}{userPermissions}{public};
HttpHelper::fail("fail") unless $$json{dataStream}{userPermissions}{$username1};
HttpHelper::fail("fail") unless scalar(@{$$json{dataStream}{userPermissions}{public}}) == 1;
HttpHelper::fail("fail") unless scalar(@{$$json{dataStream}{userPermissions}{$username1}}) == 6;

$json = HttpHelper::post (
    desc  => "create datastream (private)",
    path  => "/datastream/create",
    params=>{},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStreamPrivate=$$json{dataStream};

$json = HttpHelper::get (
    desc  => "get datastream properties of private stream",
    path  => "/dump/datastream",
    params => {'dataStream'=>$dataStreamPrivate},
    sessionKey=>$sessionKey_dumper
);
HttpHelper::fail("fail") unless ref $json eq 'HASH';    # normal JSON response
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

#============================================================================
#== Dump changes

$json = HttpHelper::delete (
    desc  => "user2 deletes device3",
    path  => "/device/delete",
    params => {device=>$device3},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::delete (
    desc  => "user2 deletes datastream3",
    path  => "/datastream/delete",
    params => {dataStream=>$dataStream3},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc  => "dump the most recent changes",
    path  => "/dump/changes/not_a_date",
    params=>{},
    sessionKey=>$sessionKey_dumper
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';

$json = HttpHelper::get (
    desc  => "dump the most recent changes",
    path  => "/dump/changes/$all_changes",
    params=>{},
    sessionKey=>$sessionKey_dumper
);
HttpHelper::fail("fail") unless ref $json eq 'ARRAY';
HttpHelper::fail("fail") unless scalar(@{$json}) == 9;

#============================================================================
#== Testing incremental changes

$json = HttpHelper::post (
    desc  => "create datastream",
    path  => "/datastream/create",
    params=>{},
    sessionKey=>$sessionKey_user2
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
$dataStream3=$$json{dataStream};

$json = HttpHelper::post (
    desc  => "create device",
    path  => "/device/create",
    params=> {},
    sessionKey=>$sessionKey_user2
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
$device3=$$json{device};
# make sure that previous changes are not included in the next dump
sleep(2);

my $changes_date = strftime "%Y-%m-%dT%H:%M:%S%z", localtime;
my $dt   = DateTime->now;
$dt->set_time_zone( 'America/Los_Angeles' );
my $timezone_date = $dt->strftime("%Y-%m-%dT%H:%M:%S%z");

# set permission
$json = HttpHelper::put (
    desc  => "user 1 updates privileges to give user 2 MODIFY permission on device1",
    path  => "/permission/set",
    params => {'key'=>$device1, 'username'=>$username2, 'permissions'=>'CONSUME, MODIFY, DISCOVER'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc  => "user 1 updates privileges to give group 1 MODIFY permission on device 2",
    path  => "/permission/set",
    params => {'key'=>$device1, 'group'=>$group1, 'permissions'=>'CONSUME, MODIFY, DISCOVER'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc  => "user 1 updates privileges to remove MODIFY permission from group 1 on device 2",
    path  => "/permission/set",
    params => {'key'=>$device1, 'group'=>$group1, 'permissions'=>''},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc  => "user 1 updates privileges to give group 1 MODIFY permission on transform 1",
    path  => "/permission/set",
    params => {'key'=>$transform1, 'group'=>$group1, 'permissions'=>'CONSUME, MODIFY, DISCOVER'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc  => "add stream to device",
    path  => "/device/add_stream",
    params=>{'device' => $device1, 'dataStream' => $dataStream2, 'name' => 's2'},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc  => "user 1 updates privileges on device1 to remove user2",
    path  => "/permission/set",
    params => {'key'=>$device1, 'username'=>$username2, 'permissions'=>''},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put_multipart (
    desc  => "upsert transform",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'functionName', value=>'testfn'},
        {name=>'parameterValues', content_type=>'application/json', value=>q#{"parameter1": "foo", "parameter2": 42}#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put(
    desc  => "update entity type",
    path  => "/entity/update",
    params => { 'key' => $device2, 'type' => 'new_type' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc  => "dump the most recent changes using a date in a different timezone",
    path  => "/dump/changes/$timezone_date",
    params=>{},
    sessionKey=>$sessionKey_dumper
);
HttpHelper::fail("fail") unless ref $json eq 'ARRAY';
HttpHelper::fail("fail") unless scalar(@{$json}) == 5;

$json = HttpHelper::get (
    desc  => "dump the most recent changes",
    path  => "/dump/changes/$changes_date",
    params=>{},
    sessionKey=>$sessionKey_dumper
);
HttpHelper::fail("fail") unless ref $json eq 'ARRAY';
HttpHelper::fail("fail") unless scalar(@{$json}) == 5;

my @keys = ($device1, $device2, $transform1, $dataStream2);
my %changes;
for my $obj (@$json) {
    if (defined $$obj{result}) {
        $result = $$obj{result};
        next;
    }
    foreach my $entity_key (@keys) {
        if ($$obj{key} eq $entity_key) {
            $changes{$entity_key} = $obj;
        }
    }
}

print "checking device $device1\n";
HttpHelper::fail("fail") unless $changes{$device1};
HttpHelper::fail("fail") unless $changes{$device1}{entityType} eq "DEVICE";
HttpHelper::fail("fail") unless $changes{$device1}{device}{key} eq $device1;
HttpHelper::fail("fail") unless $changes{$device1}{device}{userPermissions};
HttpHelper::fail("fail") unless $changes{$device1}{device}{userPermissions}{$username1};
HttpHelper::fail("fail") unless (grep { /$username2/ } @{$changes{$device1}{removedUserPermissions}});
HttpHelper::fail("fail") if $changes{$device1}{device}{userPermissions}{$username2};
HttpHelper::fail("fail") unless (grep { /$group1/ } @{$changes{$device1}{removedGroupPermissions}});
HttpHelper::fail("fail") if $changes{$device1}{device}{groupPermissions}{$group1};
HttpHelper::fail("fail") unless $changes{$device1}{device}{dnsRecords};
HttpHelper::fail("fail") unless scalar(@{$changes{$device1}{device}{dnsRecords}}) == 1;

print "checking device $device2\n";
HttpHelper::fail("fail") unless $changes{$device2};
HttpHelper::fail("fail") unless $changes{$device2}{entityType} eq "DEVICE";
HttpHelper::fail("fail") unless $changes{$device2}{device}{key} eq $device2;
HttpHelper::fail("fail") if $changes{$device2}{removedPermissions};
HttpHelper::fail("fail") if $changes{$device2}{device}{dnsRecords};

print "checking datastream $dataStream2\n";
HttpHelper::fail("fail") unless $changes{$dataStream2};
HttpHelper::fail("fail") unless $changes{$dataStream2}{entityType} eq "DATASTREAM";
HttpHelper::fail("fail") unless $changes{$dataStream2}{dataStream}{key} eq $dataStream2;
HttpHelper::fail("fail") unless $changes{$dataStream2}{dataStream}{userPermissions};
HttpHelper::fail("fail") unless $changes{$dataStream2}{dataStream}{userPermissions}{public};
HttpHelper::fail("fail") unless $changes{$dataStream2}{dataStream}{userPermissions}{$username1};
HttpHelper::fail("fail") unless $changes{$dataStream2}{dataStream}{userPermissions}{$username2};
HttpHelper::fail("fail") unless scalar(keys %{$changes{$dataStream2}{dataStream}{userPermissions}}) == 3;

HttpHelper::fail("fail") unless scalar(@{$changes{$dataStream2}{dataStream}{userPermissions}{public}}) == 1;
HttpHelper::fail("fail") unless scalar(@{$changes{$dataStream2}{dataStream}{userPermissions}{$username1}}) == 6;
HttpHelper::fail("fail") unless scalar(@{$changes{$dataStream2}{dataStream}{userPermissions}{$username2}}) == 6;

print "checking transform transform1\n";
HttpHelper::fail("fail") unless $changes{$transform1};
HttpHelper::fail("fail") unless $changes{$transform1}{entityType} eq "DATASTREAM";
HttpHelper::fail("fail") unless $changes{$transform1}{transform}{key} eq $transform1;
HttpHelper::fail("fail") unless $changes{$transform1}{transform}{userPermissions};
HttpHelper::fail("fail") unless $changes{$transform1}{transform}{userPermissions}{$username1};
HttpHelper::fail("fail") if $changes{$transform1}{transform}{dnsRecords};
HttpHelper::fail("fail") unless scalar(@{$changes{$transform1}{transform}{userPermissions}{$username1}}) == 6;
HttpHelper::fail("fail") unless scalar(@{$changes{$transform1}{transform}{groupPermissions}{$group1}}) == 3;
HttpHelper::fail("fail") unless scalar(@{$changes{$transform1}{transform}{devices}}) == 1;
HttpHelper::fail("fail") unless $changes{$transform1}{transform}{parameterValues}{parameter1} eq "foo";
HttpHelper::fail("fail") unless ${$changes{$transform1}{transform}{devices}}[0]{key} eq $device1;
HttpHelper::fail("fail") unless ${$changes{$transform1}{transform}{devices}}[0]{linkName} eq 't1';
HttpHelper::fail("fail") unless scalar(@{$changes{$transform1}{transform}{sources}}) == 1;
HttpHelper::fail("fail") unless ${$changes{$transform1}{transform}{sources}}[0]{source} eq $dataStream1;
HttpHelper::fail("fail") unless ${$changes{$transform1}{transform}{sources}}[0]{alias} eq 'my favourite stream';
HttpHelper::fail("fail") unless scalar(@{$changes{$transform1}{transform}{outputStreams}}) == 1;
HttpHelper::fail("fail") unless ${$changes{$transform1}{transform}{outputStreams}}[0]{key} eq $dataStream2;
HttpHelper::fail("fail") unless ${$changes{$transform1}{transform}{outputStreams}}[0]{alias} eq 'output2';

#============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

