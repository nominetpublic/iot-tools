# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use Data::Dumper;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;

# connect
my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";

(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

my $json;

my $sessionKey_userAdmin=ThingzoneLogin::login_admin_user() or die;

#=========================================================================

# user creation
my $unique = time();
my $username1 = "user_stream_upsert_1_$unique";
my $password1 = "password_$unique";
my $username_dumper = "user_dumper_$unique";
my $password_dumper = "password_dumper_$unique";
my $group1 = "group_upsert_1_$unique";

$json = HttpHelper::post (
    desc  => "Create user ($username1)",
    path  => '/user/create',
    params=> {username=>$username1, password=>$password1, privileges=>"CREATE_GROUP, CREATE_DEVICE, CREATE_DATASTREAM"},
    sessionKey=>$sessionKey_userAdmin
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "Create user 'dumper'",
    path  => '/user/create',
    params=> {username=>$username_dumper, password=>$password_dumper, privileges=>"DUMP_ALL"},
    sessionKey=>$sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# log in users
$json = HttpHelper::post (
    desc  => "Log in as user ($username1)",
    path  => '/session/login',
    params=> {username=>$username1, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user1=$$json{sessionKey};

$json = HttpHelper::post (
    desc  => "Log in as user 'dumper'",
    path  => '/session/login',
    params=> {username=>$username_dumper, password=>$password_dumper}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_dumper=$$json{sessionKey};

# create group
$json = HttpHelper::post(
    desc  => "create group 1",
    path  => "/groups/$group1",
    params => { 'name' => $group1 },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put(
    desc  => "add users to group 1",
    path  => "/groups/$group1/users/add",
    params => { 'usernames' => "$username1,$username_dumper" },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

#=========================================================================
# create a initial datastream + all stuff

$json = HttpHelper::post (
    desc  => "create datastream",
    path  => "/datastream/create",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream1=$$json{dataStream};


$json = HttpHelper::put_multipart (
    desc  => "add metadata",
    path  => "/datastream/update",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>'metadata', content_type=>'application/json', value=>q#{"here be metadata":"so meta"}#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# set permissions
$json = HttpHelper::put (
    desc  => "user 1 adds some privileges",
    path  => "/permission/set",
    params => {'key'=>$dataStream1, 'username'=>'public', 'permissions'=>'DISCOVER'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';



#=============================================================================
# verify / read


$json = HttpHelper::get (
    desc  => "read back initial data",
    path  => "/datastream/get",
    params => {"dataStream" => $dataStream1},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::get (
    desc  => "dump should return all data for user 'dumper'",
    path  => "/dump/all",
    params=> {},
    sessionKey=>$sessionKey_dumper,
    print_raw_json => 1
    );
die unless ref $json eq 'ARRAY';    # special JSON response for dump, so it's streamable



#=============================================================================
# upsert

# upsert as existing datastream (no changes)

$json = HttpHelper::put_multipart (
    desc  => "upsert (no changes)",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1}
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream};
HttpHelper::fail("fail") unless $$json{dataStream}{key} eq $dataStream1;
HttpHelper::fail("fail") unless $$json{dataStream}{metadata};
HttpHelper::fail("fail") unless $$json{dataStream}{metadata}{'here be metadata'};
HttpHelper::fail("fail") unless $$json{dataStream}{metadata}{'here be metadata'} eq 'so meta';
die if $$json{dataStream}{dnsRecords};
die if $$json{devices};
HttpHelper::fail("fail") unless $$json{dataStream}{userPermissions};
HttpHelper::fail("fail") unless $$json{dataStream}{userPermissions}{public};
die unless @{$$json{dataStream}{userPermissions}{public}} eq 1;
HttpHelper::fail("fail") unless $$json{dataStream}{userPermissions}{$username1};
die unless @{$$json{dataStream}{userPermissions}{$username1}} eq 6;

# upsert as a new datastream

my $dataStream2 = "datastream2-$unique";

$json = HttpHelper::put_multipart (
    desc  => "upsert (create new datastream $dataStream2)",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream2}
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream};
HttpHelper::fail("fail") unless $$json{dataStream}{key} eq $dataStream2;

# upsert as a new datastream with autogenerated key
$json = HttpHelper::put_multipart (
    desc  => "upsert (create new datastream no key provided)",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>'metadata', content_type=>'application/json', value=>q#{"metadata":"meta meta data"}#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream};
HttpHelper::fail("fail") unless $$json{dataStream}{metadata}{'metadata'};
HttpHelper::fail("fail") unless $$json{dataStream}{metadata}{'metadata'} eq 'meta meta data';

#=============================================================================
# upsert and modify...

#-----------------------------------------------------------------------------
# upsert metadata

$json = HttpHelper::put_multipart (
    desc  => "upsert & change metadata",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>'metadata', content_type=>'application/json', value=>q#{"different metadata":"meta meta data"}#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream}{key} eq $dataStream1;
HttpHelper::fail("fail") unless $$json{dataStream}{metadata};
die if $$json{dataStream}{metadata}{'here be metadata'};
HttpHelper::fail("fail") unless $$json{dataStream}{metadata}{'different metadata'};
HttpHelper::fail("fail") unless $$json{dataStream}{metadata}{'different metadata'} eq 'meta meta data';


#-----------------------------------------------------------------------------
# upsert permissions

$json = HttpHelper::put_multipart (
    desc  => "upsert & change permissions (bogus json)",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>'userPermissions', content_type=>'application/json', value=> "invalid json"},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /bad JSON/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put_multipart (
    desc  => "upsert & change permissions (json in wrong form)",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>'userPermissions', content_type=>'application/json', value=> q#{ "foo": false }#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /bad JSON/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put_multipart (
    desc  => "upsert & change permissions (with invalid user)",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>'userPermissions', content_type=>'application/json', value=>
"{ \"notUser\" : [\"DISCOVER\"], \"$username1\" : [\"ADMIN\",\"UPLOAD\",\"CONSUME\",\"MODIFY\",\"ANNOTATE\",\"DISCOVER\"] }"},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /User not found/;
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

$json = HttpHelper::put_multipart (
    desc  => "upsert & change permissions (to be the same)",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>'userPermissions', content_type=>'application/json', value=>
"{ \"public\" : [\"DISCOVER\"], \"$username1\" : [\"ADMIN\",\"UPLOAD\",\"CONSUME\",\"MODIFY\",\"ANNOTATE\",\"DISCOVER\"] }"
            },
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream}{key} eq $dataStream1;
HttpHelper::fail("fail") unless $$json{dataStream}{metadata};
HttpHelper::fail("fail") unless $$json{dataStream}{userPermissions}{public};
die unless @{$$json{dataStream}{userPermissions}{public}} == 1;
HttpHelper::fail("fail") unless $$json{dataStream}{userPermissions}{$username1};
die unless @{$$json{dataStream}{userPermissions}{$username1}} == 6;


#-----------------------------------------------------------------------------
# upsert group permissions

$json = HttpHelper::put_multipart (
    desc  => "upsert & change group permissions (bogus json)",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>'groupPermissions', content_type=>'application/json', value=> "invalid json"},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /bad JSON/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put_multipart (
    desc  => "upsert & change group permissions (json in wrong form)",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>'groupPermissions', content_type=>'application/json', value=> q#{ "foo": false }#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /bad JSON/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put_multipart (
    desc  => "upsert & change group permissions (json with invalid permission)",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>'groupPermissions', content_type=>'application/json', value=> q#{ "public" : ["NOT_A_VALID_PERMISSION"] }#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /bad JSON/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put_multipart (
    desc  => "upsert & change permissions (json with invalid group)",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>'groupPermissions', content_type=>'application/json', value=> "{ \"notGroup\" : [\"DISCOVER\"] }" },
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /Group not found/;
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

$json = HttpHelper::put_multipart (
    desc  => "upsert & change permissions (to add group 1)",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>'groupPermissions', content_type=>'application/json', value=>
"{ \"$group1\" : [\"ADMIN\",\"UPLOAD\",\"CONSUME\",\"MODIFY\",\"ANNOTATE\",\"DISCOVER\"] }" },
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream}{key} eq $dataStream1;
HttpHelper::fail("fail") unless $$json{dataStream}{metadata};
HttpHelper::fail("fail") unless $$json{dataStream}{groupPermissions}{$group1};
die unless @{$$json{dataStream}{groupPermissions}{$group1}} == 6;

#-----------------------------------------------------------------------------
# upsert DNS records



$json = HttpHelper::put (
    desc  => "add DNS record",
    path  => "/dns/add_record",
    params=> {'key'=>$dataStream1, 'description'=>'dns1', 'subdomain'=>'nautilus', 'RRTYPE'=>"A", 'RDATA'=>"1.2.3.4"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
$json = HttpHelper::put (
    desc  => "add DNS record",
    path  => "/dns/add_record",
    params=> {'key'=>$dataStream1, 'description'=>'dns2', 'subdomain'=>'gup-b', 'RRTYPE'=>"CNAME", 'RDATA'=>"octo.nauts"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::get (
    desc  => "dump object",
    path  => "/dump/datastream",
    params=> {'dataStream'=>$dataStream1},
    sessionKey=>$sessionKey_dumper
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
die unless @{$$json{dataStream}{dnsRecords}} == 2;


$json = HttpHelper::put_multipart (
    desc  => "upsert & change DNS records (to same as before)",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>'dnsRecords', content_type=>'application/json', value=>
"["
. q# { "rrtype" : "CNAME","subdomain" : "gup-b","ttl" : 600, "description" : "dns2", "rdata" : "octo.nauts" } #
.','
. q# { "ttl" : 600, "description" : "dns1", "rdata" : "1.2.3.4", "rrtype" : "A", "subdomain" : "nautilus" } #
. "]" },
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
die unless @{$$json{dataStream}{dnsRecords}} == 2;

for my $dns (@{$$json{dataStream}{dnsRecords}}) {
    print Dumper $dns;
    if ($$dns{description} eq 'dns1') {
        die unless $$dns{subdomain} eq 'nautilus';
        die unless $$dns{rrtype} eq 'A';
        die unless $$dns{rdata} eq '1.2.3.4';
        die unless $$dns{ttl} eq 600;
    } elsif ($$dns{description} eq 'dns2') {
        die unless $$dns{subdomain} eq 'gup-b';
        die unless $$dns{rrtype} eq 'CNAME';
        die unless $$dns{rdata} eq 'octo.nauts';
        die unless $$dns{ttl} eq 600;
    } else {
        die "DNS record desc=".$$dns{description}. " unexpected";
    }
}



$json = HttpHelper::put_multipart (
    desc  => "upsert & change DNS records",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>'dnsRecords', content_type=>'application/json', value=>
"["
. q# { "rrtype" : "CNAME","subdomain" : "gup-b","ttl" : 6000, "description" : "dns2", "rdata" : "octo.nauts" } #    # modified
.','
. q# { "ttl" : 600, "description" : "dns3", "rdata" : "2.3.4.5", "rrtype" : "A", "subdomain" : "triton" } # # new
. "]" },
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
die unless @{$$json{dataStream}{dnsRecords}} == 2;

for my $dns (@{$$json{dataStream}{dnsRecords}}) {
#    print Dumper $dns;
    if ($$dns{description} eq 'dns3') {
        die unless $$dns{subdomain} eq 'triton';
        die unless $$dns{rrtype} eq 'A';
        die unless $$dns{rdata} eq '2.3.4.5';
        die unless $$dns{ttl} eq 600;
    } elsif ($$dns{description} eq 'dns2') {
        die unless $$dns{subdomain} eq 'gup-b';
        die unless $$dns{rrtype} eq 'CNAME';
        die unless $$dns{rdata} eq 'octo.nauts';
        die unless $$dns{ttl} eq 6000;
    } else {
        die "DNS record desc=".$$dns{description}. " unexpected";
    }
}

$json = HttpHelper::put_multipart (
    desc  => "upsert DataStream dnsRecords with invalid json is refused",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>'dnsRecords', content_type=>'application/json', value=> "invalid json"},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /bad JSON/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put_multipart (
    desc  => "upsert DataStream dnsRecords with failing validation is refused",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>'dnsRecords', content_type=>'application/json', value=>
q# [{ "rrtype" : "UNSUPPORTED","subdomain" : "gup-b","ttl" : 600, "description" : "dns2", "rdata" : "octo.nauts" }] # },
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /unhandled RRTYPE/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

#-----------------------------------------------------------------------------
# upsert device links

# (hard-code the device names, to simplify JSON param creation)
$json = HttpHelper::post (
    desc  => "create device1",
    path  => "/device/create",
    params=> {'device' => 'device1' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "create device2",
    path  => "/device/create",
    params=> {'device' => 'device2' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "create device3",
    path  => "/device/create",
    params=> {'device' => 'device3' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc  => "add stream 1 to device 1",
    path  => "/device/add_stream",
    params=> {'device' => 'device1', 'dataStream' => $dataStream1, 'name' => 'd1_s1'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc  => "add stream 1 to device 2",
    path  => "/device/add_stream",
    params=> {'device' => 'device2', 'dataStream' => $dataStream1, 'name' => 'd2_s1'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::get (
    desc  => "dump object",
    path  => "/dump/datastream",
    params=> {'dataStream'=>$dataStream1},
    sessionKey=>$sessionKey_dumper
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream}{devices};
die unless @{$$json{dataStream}{devices}} == 2;
for my $device (@{$$json{dataStream}{devices}}) {
    print Dumper $device;
    if ($$device{key} eq 'device1') {
        die unless $$device{linkName} eq 'd1_s1';
    } elsif ($$device{key} eq 'device2') {
        die unless $$device{linkName} eq 'd2_s1';
    } else {
        die "device key=".$$device{key}. " unexpected";
    }
}


$json = HttpHelper::put_multipart (
    desc  => "upsert DataStream & set link to device1&2 (no change)",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>'devices', content_type=>'application/json', value=>
q# [ { "key" : "device1", "linkName" : "d1_s1" }, { "key" : "device2", "linkName" : "d2_s1" } ] # },
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::put_multipart (
    desc  => "upsert DataStream & make changes",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>'devices', content_type=>'application/json', value=>
"["
. q# { "key" : "device1", "linkName" : "renamed" } #    # renamed
. ","
. q# { "key" : "device3", "linkName" : "d3_s1" } #  # new
. "]" },
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream}{devices};
die unless @{$$json{dataStream}{devices}} == 2;
for my $device (@{$$json{dataStream}{devices}}) {
    print Dumper $device;
    if ($$device{key} eq 'device1') {
        die unless $$device{linkName} eq 'renamed';
    } elsif ($$device{key} eq 'device3') {
        die unless $$device{linkName} eq 'd3_s1';
    } else {
        die "device key=".$$device{key}. " unexpected";
    }
}


#-----------------------------------------------------------------------------
# upsert push subscribers

$json = HttpHelper::put_multipart (
    desc  => "add push subscriber to datastream",
    path  => "/push_subscribers/set",
    parts => HttpHelper::create_multiparts(
        {name=>'dataStream', value=>$dataStream1},
        {name=>'uri', value=>'http://example.com'},
        {name=>'uriParams', content_type=>'application/json', value=>q#{"httpHeaders":{"SessionHeaderId": "1234"} }#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::put_multipart (
    desc  => "upsert DataStream & leave push subscriber alone",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream}{pushSubscribers};
die unless @{$$json{dataStream}{pushSubscribers}} == 1;
die unless ${$$json{dataStream}{pushSubscribers}}[0]{uri} eq "http://example.com";


$json = HttpHelper::put_multipart (
    desc  => "upsert DataStream & add push subscriber",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>'pushSubscribers', content_type=>'application/json', value=>
q# [ { "uri" : "https://en.wikipedia.org/", "method":"PUT", "uriParams" : {},
    "headers":{"SessionHeaderId": "1234"}, "retrySequence" : "1m 5m 1d" }] # },
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream}{pushSubscribers};
die unless @{$$json{dataStream}{pushSubscribers}} == 1;
die unless ${$$json{dataStream}{pushSubscribers}}[0]{uri} eq "https://en.wikipedia.org/";
die unless ${$$json{dataStream}{pushSubscribers}}[0]{retrySequence} eq "1m 5m 1d";

$json = HttpHelper::put_multipart (
    desc  => "upsert DataStream & modify push subscribers",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>'pushSubscribers', content_type=>'application/json', value=>
q# [ { "uri" : "https://en.wikipedia.org/", "method":"PUT", "uriParams" : { "test" : "value" },
    "headers":{"SessionHeaderId": "5678"}, "retrySequence" : "1m 5m 1d 1d 1d" }] # },
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream}{pushSubscribers};
die unless @{$$json{dataStream}{pushSubscribers}} == 1;
die unless ${$$json{dataStream}{pushSubscribers}}[0]{uri} eq "https://en.wikipedia.org/";
die unless ${$$json{dataStream}{pushSubscribers}}[0]{retrySequence} eq "1m 5m 1d 1d 1d";


$json = HttpHelper::put_multipart (
    desc  => "upsert DataStream & modify push subscribers",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>'pushSubscribers', content_type=>'application/json', value=>
q# [{"uri":"https://en.wikipedia.org/","method":"PUT","retrySequence":"1m 5m 1d 1d",
    "uriParams" : { "test" : "value" },"headers":{"content-type":"application/json"}}] # },
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream}{pushSubscribers};
die unless @{$$json{dataStream}{pushSubscribers}} == 1;
die unless ${$$json{dataStream}{pushSubscribers}}[0]{uri} eq "https://en.wikipedia.org/";
die unless ${$$json{dataStream}{pushSubscribers}}[0]{retrySequence} eq "1m 5m 1d 1d";


#-----------------------------------------------------------------------------
# upsert pull subscription

$json = HttpHelper::put_multipart (
    desc  => "add pull subscription to datastream",
    path  => "/pull_subscription/set",
    parts => HttpHelper::create_multiparts(
        {name=>'dataStream', value=>$dataStream1},
        {name=>'uri', value=>'http://example.com'},
        {name=>'uriParams', content_type=>'application/json', value=>q#{"httpHeaders":{"SessionHeaderId": "1234"} }#},
        {name=>'interval', value=>42},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::put_multipart (
    desc  => "upsert DataStream & leave pull subscription alone",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream}{pullSubscription};
HttpHelper::fail("fail") unless $$json{dataStream}{pullSubscription}{uri} eq 'http://example.com';
HttpHelper::fail("fail") unless $$json{dataStream}{pullSubscription}{uriParams};
HttpHelper::fail("fail") unless $$json{dataStream}{pullSubscription}{uriParams}{httpHeaders};
HttpHelper::fail("fail") unless $$json{dataStream}{pullSubscription}{uriParams}{httpHeaders}{SessionHeaderId};
HttpHelper::fail("fail") unless $$json{dataStream}{pullSubscription}{uriParams}{httpHeaders}{SessionHeaderId} == 1234;
HttpHelper::fail("fail") unless $$json{dataStream}{pullSubscription}{interval} == 42;

$json = HttpHelper::put_multipart (
    desc  => "upsert DataStream & modify pull subscription",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>'pullSubscription', content_type=>'application/json', value=>
q# { "uri" : "https://en.wikipedia.org/", "uriParams" : {}, "interval" : 44, "aligned": 5 } # },
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream}{pullSubscription};
HttpHelper::fail("fail") unless $$json{dataStream}{pullSubscription}{uri} eq "https://en.wikipedia.org/";
HttpHelper::fail("fail") unless $$json{dataStream}{pullSubscription}{uriParams};
HttpHelper::fail("fail") unless $$json{dataStream}{pullSubscription}{interval} == 44;
HttpHelper::fail("fail") unless $$json{dataStream}{pullSubscription}{aligned} == 5;

$json = HttpHelper::put_multipart (
    desc  => "upsert DataStream & supply partial object",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>'pullSubscription', content_type=>'application/json', value=>
q# { "uri" : "https://en.wikipedia.org/", "interval" : 69 } # },
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /missing 'uriParams'/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';


#-----------------------------------------------------------------------------
# upsert entity type

$json = HttpHelper::put_multipart (
    desc  => "upsert DataStream & update entity type",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>"type", value=>"updated_type" }),
    sessionKey=>$sessionKey_user1
);

HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream}{type} eq 'updated_type';

$json = HttpHelper::put_multipart (
    desc  => "upsert DataStream & unset entity type",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>"type", value=>"" }),
    sessionKey=>$sessionKey_user1
);

HttpHelper::fail("fail") unless $$json{result} eq 'ok';
die if (defined $$json{dataStream}{type});

#=============================================================================
# upsert entity name


$json = HttpHelper::put_multipart (
    desc  => "upsert DataStream & update entity type",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>"name", value=>"updated_name" }),
    sessionKey=>$sessionKey_user1
);

HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream}{name} eq 'updated_name';

$json = HttpHelper::put_multipart (
    desc  => "upsert DataStream & unset entity type",
    path  => "/datastream/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>"name", value=>"" }),
    sessionKey=>$sessionKey_user1
);

HttpHelper::fail("fail") unless $$json{result} eq 'ok';
die if (defined $$json{dataStream}{name});

#=============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

