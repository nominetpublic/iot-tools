# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use Data::Dumper;
use JSON;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;


# connect
my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";
(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

my $json;
my $sessionKey_userAdmin=ThingzoneLogin::login_admin_user() or die;
#=========================================================================

# user creation
my $unique = time();
my $username1 = "user_stream_upsert_1_$unique";
my $password1 = "password_$unique";
my $username_dumper = "user_dumper_$unique";
my $password_dumper = "password_dumper_$unique";
my $group1 = "group_upsert_1_$unique";

$json = HttpHelper::post (
    desc  => "Create user ($username1)",
    path  => '/user/create',
    params=> {username=>$username1, password=>$password1, privileges=>
        "CREATE_GROUP, CREATE_DEVICE, CREATE_DATASTREAM, CREATE_TRANSFORM, CREATE_TRANSFORMFUNCTION"},
    sessionKey=>$sessionKey_userAdmin
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "Create user 'dumper'",
    path  => '/user/create',
    params=> {username=>$username_dumper, password=>$password_dumper, privileges=>"DUMP_ALL"},
    sessionKey=>$sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# log in users
$json = HttpHelper::post (
    desc  => "Log in as user ($username1)",
    path  => '/session/login',
    params=> {username=>$username1, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user1=$$json{sessionKey};

$json = HttpHelper::post (
    desc  => "Log in as user 'dumper'",
    path  => '/session/login',
    params=> {username=>$username_dumper, password=>$password_dumper}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_dumper=$$json{sessionKey};

# create group
$json = HttpHelper::post(
    desc  => "create group 1",
    path  => "/groups/$group1",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put(
    desc  => "add users to group 1",
    path  => "/groups/$group1/users/add",
    params => { 'usernames' => "$username1,$username_dumper" },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

#============================================================================
# functions
$json = HttpHelper::put_multipart (
    desc => "user 1 creates function",
    path => "/transform/create_function",
    parts=> HttpHelper::create_multiparts(
        {name=>'name', value=>'function1'},
        {name=>'functionType', value=>'javascript'},
        {name=>'functionContent', value=>"console.log('hello world')"}),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put_multipart (
    desc => "user 1 creates function",
    path => "/transform/create_function",
    parts=> HttpHelper::create_multiparts(
        {name=>'name', value=>'testfn'},
        {name=>'functionType', value=>'javascript'},
        {name=>'functionContent', value=>"console.log('test')"}),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

#============================================================================
# transform creation, visibility

$json = HttpHelper::post_multipart (
    desc => "user 1 creates transform",
    path => "/transform/create",
    parts => HttpHelper::create_multiparts(
        {name=>'functionName', value=>'testfn'},
        {name=>'parameterValues', content_type=>'application/json', value=>q#{"parameter1": "foo", "parameter2": 42}#},
        {name=>'runSchedule', value=>'sometimes'},
        ),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $transform1=$$json{transform} or die;

$json = HttpHelper::put_multipart (
    desc  => "add metadata",
    path  => "/datastream/update",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $transform1},
        {name=>'metadata', content_type=>'application/json', value=>q#{"here be metadata":"meta datastream"}#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# set permissions
$json = HttpHelper::put (
    desc  => "user 1 adds some privileges",
    path  => "/permission/set",
    params => {'key'=>$transform1, 'username'=>'public', 'permissions'=>'DISCOVER'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# transform modification: sources
$json = HttpHelper::post (
    desc => "create datastream1 for source",
    path => "/datastream/create",
    params => {},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream1=$$json{dataStream};

$json = HttpHelper::post (
    desc => "create datastream2 for source",
    path => "/datastream/create",
    params => {},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream2=$$json{dataStream};

$json = HttpHelper::post (
    desc => "create datastream3 for source",
    path => "/datastream/create",
    params => {},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream3=$$json{dataStream};

$json = HttpHelper::put (
    desc => "attaches datastream1 as source (to transform1)",
    path => "/transform/update_source",
    params => {
        'transform' => $transform1,
        'stream' => $dataStream1,
        'alias' => 'Banksy',
        'trigger' => 'false',
        'aggregationType' => 'type',
        'aggregationSpec' => 'spec'
        },
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc => "attaches datastream2 as source (to transform1)",
    path => "/transform/update_source",
    params => {
        'transform' => $transform1,
        'stream' => $dataStream2,
        'alias' => 'Mr Test',
        'trigger' => 'true',
        },
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

#============================================================================
# verify dataset

$json = HttpHelper::get (
    desc => "user1 reads transform1",
    path => "/transform/get",
    params=> {'transform' => $transform1},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::get (
    desc  => "dump should return all data for user 'dumper'",
    path  => "/dump/all",
    params=> {},
    sessionKey=>$sessionKey_dumper,
    print_raw_json => 1
    );
HttpHelper::fail("fail") unless ref $json eq 'ARRAY';    # special JSON response for dump, so it's streamable

#============================================================================
# upsert with no changes

$json = HttpHelper::put_multipart (
    desc  => "upsert (no changes, minimal params)",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1}
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{transform};
HttpHelper::fail("fail") unless $$json{transform}{key} eq $transform1;
HttpHelper::fail("fail") unless $$json{transform}{metadata};
HttpHelper::fail("fail") unless $$json{transform}{metadata}{'here be metadata'};
HttpHelper::fail("fail") unless $$json{transform}{metadata}{'here be metadata'} eq 'meta datastream';
HttpHelper::fail("fail") unless $$json{transform}{parameterValues};
HttpHelper::fail("fail") unless $$json{transform}{parameterValues}{parameter1};
HttpHelper::fail("fail") unless $$json{transform}{parameterValues}{parameter1} eq "foo";
HttpHelper::fail("fail") unless $$json{transform}{parameterValues}{parameter2};
HttpHelper::fail("fail") unless $$json{transform}{parameterValues}{parameter2} == 42;
HttpHelper::fail("fail") unless $$json{transform}{runSchedule} eq "sometimes";
HttpHelper::fail("fail") if $$json{transform}{dnsRecords};
HttpHelper::fail("fail") if $$json{devices};
HttpHelper::fail("fail") unless $$json{transform}{sources};
HttpHelper::fail("fail") unless @{$$json{transform}{sources}} eq 2;
HttpHelper::fail("fail") unless $$json{transform}{userPermissions};
HttpHelper::fail("fail") unless $$json{transform}{userPermissions}{public};
HttpHelper::fail("fail") unless @{$$json{transform}{userPermissions}{public}} eq 1;
HttpHelper::fail("fail") unless $$json{transform}{userPermissions}{$username1};
HttpHelper::fail("fail") unless @{$$json{transform}{userPermissions}{$username1}} eq 6;

$json = HttpHelper::put_multipart (
    desc  => "upsert (no changes, max params)",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'functionName', value=>'testfn'},
        {name=>'functionisPublic', value=>'true'},
        {name=>'metadata', content_type=>'application/json', value=>q#{"here be metadata":"so meta"}#},
        {name=>'parameterValues', content_type=>'application/json', value=>q#{"parameter1": "foo", "parameter2": 42}#},
        {name=>'runSchedule', value=>'sometimes'},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{transform};
HttpHelper::fail("fail") unless $$json{transform}{key} eq $transform1;
HttpHelper::fail("fail") unless $$json{transform}{metadata};
HttpHelper::fail("fail") unless $$json{transform}{metadata}{'here be metadata'};
HttpHelper::fail("fail") unless $$json{transform}{metadata}{'here be metadata'} eq 'so meta';
HttpHelper::fail("fail") unless $$json{transform}{parameterValues};
HttpHelper::fail("fail") unless $$json{transform}{parameterValues}{parameter1};
HttpHelper::fail("fail") unless $$json{transform}{parameterValues}{parameter1} eq "foo";
HttpHelper::fail("fail") unless $$json{transform}{parameterValues}{parameter2};
HttpHelper::fail("fail") unless $$json{transform}{parameterValues}{parameter2} == 42;
HttpHelper::fail("fail") unless $$json{transform}{runSchedule} eq "sometimes";
HttpHelper::fail("fail") if $$json{transform}{dnsRecords};
HttpHelper::fail("fail") if $$json{devices};
HttpHelper::fail("fail") unless $$json{transform}{sources};
HttpHelper::fail("fail") unless @{$$json{transform}{sources}} eq 2;

HttpHelper::fail("fail") unless $$json{transform}{userPermissions};
HttpHelper::fail("fail") unless $$json{transform}{userPermissions}{public};
HttpHelper::fail("fail") unless @{$$json{transform}{userPermissions}{public}} eq 1;
HttpHelper::fail("fail") unless $$json{transform}{userPermissions}{$username1};
HttpHelper::fail("fail") unless @{$$json{transform}{userPermissions}{$username1}} eq 6;

#============================================================================
# upsert to create new entity

my $transform2 = "upserted2-$unique";

$json = HttpHelper::put_multipart (
    desc  => "upsert creation is refused if functionName is omitted",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform2},
        {name=>'parameterValues', content_type=>'application/json', value=>q#{"myname": "upserted2"}#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /functionName is needed/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put_multipart (
    desc  => "upsert creates a new transform",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform2},
        {name=>'functionName', value=>'testfn'},
        {name=>'parameterValues', content_type=>'application/json', value=>q#{"myname": "upserted2"}#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{transform};
HttpHelper::fail("fail") unless $$json{transform}{key} eq $transform2;
HttpHelper::fail("fail") unless $$json{transform}{metadata};
HttpHelper::fail("fail") unless $$json{transform}{parameterValues};
HttpHelper::fail("fail") unless $$json{transform}{parameterValues}{myname};
HttpHelper::fail("fail") unless $$json{transform}{parameterValues}{myname} eq "upserted2";
HttpHelper::fail("fail") if $$json{transform}{dnsRecords};
HttpHelper::fail("fail") if $$json{devices};
HttpHelper::fail("fail") if $$json{transform}{sources};
HttpHelper::fail("fail") unless $$json{transform}{userPermissions};
HttpHelper::fail("fail") if     $$json{transform}{userPermissions}{public};
HttpHelper::fail("fail") unless $$json{transform}{userPermissions}{$username1};
HttpHelper::fail("fail") unless @{$$json{transform}{userPermissions}{$username1}} eq 6;

#-----------------------------------------------------------------------------
# upsert DNS records
# (bare-bones test here: full tests for dataStreams, which share implementation)

$json = HttpHelper::put (
    desc  => "add DNS record",
    path  => "/dns/add_record",
    params=> {'key'=>$transform1, 'description'=>'dns1', 'subdomain'=>'nautilus', 'RRTYPE'=>"A", 'RDATA'=>"1.2.3.4"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
$json = HttpHelper::put (
    desc  => "add DNS record",
    path  => "/dns/add_record",
    params=> {'key'=>$transform1, 'description'=>'dns2', 'subdomain'=>'gup-b', 'RRTYPE'=>"CNAME", 'RDATA'=>"octo.nauts"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put_multipart (
    desc  => "upsert & change DNS records",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'dnsRecords', content_type=>'application/json', value=>
"["
. q# { "rrtype" : "CNAME","subdomain" : "gup-b","ttl" : 6000, "description" : "dns2", "rdata" : "octo.nauts" } #    # modified
.','
. q# { "ttl" : 600, "description" : "dns3", "rdata" : "2.3.4.5", "rrtype" : "A", "subdomain" : "triton" } # # new
. "]" },
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{transform}{dnsRecords}} == 2;

for my $dns (@{$$json{transform}{dnsRecords}}) {
#    print Dumper $dns;
    if ($$dns{description} eq 'dns3') {
        HttpHelper::fail("fail") unless $$dns{subdomain} eq 'triton';
        HttpHelper::fail("fail") unless $$dns{rrtype} eq 'A';
        HttpHelper::fail("fail") unless $$dns{rdata} eq '2.3.4.5';
        HttpHelper::fail("fail") unless $$dns{ttl} eq 600;
    } elsif ($$dns{description} eq 'dns2') {
        HttpHelper::fail("fail") unless $$dns{subdomain} eq 'gup-b';
        HttpHelper::fail("fail") unless $$dns{rrtype} eq 'CNAME';
        HttpHelper::fail("fail") unless $$dns{rdata} eq 'octo.nauts';
        HttpHelper::fail("fail") unless $$dns{ttl} eq 6000;
    } else {
        HttpHelper::fail("DNS record desc=".$$dns{description}. " unexpected");
    }
}

#-----------------------------------------------------------------------------
# upsert device links

$json = HttpHelper::post (
    desc  => "create device",
    path  => "/device/create",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $device1=$$json{device} or die;

$json = HttpHelper::post (
    desc  => "create device 2",
    path  => "/device/create",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $device2=$$json{device} or die;

$json = HttpHelper::put (
    desc  => "add transform1 1 to device 1",
    path  => "/device/add_stream",
    params=> {'device' => $device1, 'dataStream' => $transform1, 'name' => 'd1_t1'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

my @stream_data = ({ "key" => $device1, "linkName" => "d1_t1" });
my $stream_json = encode_json \@stream_data;

$json = HttpHelper::put_multipart (
    desc  => "upsert transform & set link to device1 (no change)",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'devices', content_type=>'application/json', value=> $stream_json },
    ),
    sessionKey=>$sessionKey_user1
);

HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{transform}{devices};
HttpHelper::fail("fail") unless @{$$json{transform}{devices}} == 1;
HttpHelper::fail("fail") unless ${$$json{transform}{devices}}[0]{linkName} eq 'd1_t1';
HttpHelper::fail("fail") unless ${$$json{transform}{devices}}[0]{key} eq $device1;

@stream_data = ({ "key" => $device2, "linkName" => "d2_t1" });
$stream_json = encode_json \@stream_data;

$json = HttpHelper::put_multipart (
    desc  => "upsert transform and change device links & make changes",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'devices', content_type=>'application/json', value=> $stream_json}
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{transform}{devices};
HttpHelper::fail("fail") unless @{$$json{transform}{devices}} == 1;
HttpHelper::fail("fail") unless ${$$json{transform}{devices}}[0]{linkName} eq 'd2_t1';
HttpHelper::fail("fail") unless ${$$json{transform}{devices}}[0]{key} eq $device2;

#-----------------------------------------------------------------------------
# upsert push subscribers

$json = HttpHelper::put_multipart (
    desc  => "add push subscriber to datastream",
    path  => "/push_subscribers/set",
    parts => HttpHelper::create_multiparts(
        {name=>'dataStream', value=>$transform1},
        {name=>'uri', value=>'http://example.com'},
        {name=>'uriParams', content_type=>'application/json', value=>q#{"httpHeaders":{"SessionHeaderId": "1234"} }#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put_multipart (
    desc  => "upsert transform & leave push subscriber alone",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{transform}{pushSubscribers};
HttpHelper::fail("fail") unless @{$$json{transform}{pushSubscribers}} == 1;
HttpHelper::fail("fail") unless ${$$json{transform}{pushSubscribers}}[0]{uri} eq "http://example.com";

$json = HttpHelper::put_multipart (
    desc  => "upsert transform & modify push subscribers",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'pushSubscribers', content_type=>'application/json', value=>
q# [ { "uri" : "https://en.wikipedia.org/", "uriParams" : {} }] # },
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{transform}{pushSubscribers};
HttpHelper::fail("fail") unless @{$$json{transform}{pushSubscribers}} == 1;
HttpHelper::fail("fail") unless ${$$json{transform}{pushSubscribers}}[0]{uri} eq "https://en.wikipedia.org/";

#-----------------------------------------------------------------------------
# upsert runSchedule option

$json = HttpHelper::put_multipart (
    desc  => "upsert transform & modify runSchedule option",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'runSchedule', value=> "not any more" },
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{transform}{runSchedule};
HttpHelper::fail("fail") unless $$json{transform}{runSchedule} eq "not any more";

$json = HttpHelper::put_multipart (
    desc  => "upsert transform & remove runSchedule option",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'runSchedule', value=> "" },
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{transform}{runSchedule};
HttpHelper::fail("fail") unless $$json{transform}{runSchedule} eq "";

#=============================================================================
# upsert entity type

$json = HttpHelper::put_multipart (
    desc  => "upsert transform  & update entity type",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>"type", value=>"updated_type" }),
    sessionKey=>$sessionKey_user1
);

HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{transform}{type} eq 'updated_type';

$json = HttpHelper::put_multipart (
    desc  => "upsert transform  & unset entity type",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>"type", value=>"" }),
    sessionKey=>$sessionKey_user1
);

HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") if (defined $$json{transform}{type});

#=============================================================================
# upsert entity name

$json = HttpHelper::put_multipart (
    desc  => "upsert transform  & update entity name",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>"name", value=>"updated name" }),
    sessionKey=>$sessionKey_user1
);

HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{transform}{name} eq 'updated name';

$json = HttpHelper::put_multipart (
    desc  => "upsert transform  & unset entity type",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>"name", value=>"" }),
    sessionKey=>$sessionKey_user1
);

HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") if (defined $$json{transform}{name});

#=============================================================================
# upsert
$json = HttpHelper::put_multipart (
    desc  => "upsert & change permissions (bogus json)",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'userPermissions', content_type=>'application/json', value=> "invalid json"},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /bad JSON/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put_multipart (
    desc  => "upsert & change permissions (json in wrong form)",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'userPermissions', content_type=>'application/json', value=> q#{ "foo": false }#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /bad JSON/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put_multipart (
    desc  => "upsert & change permissions (with invalid user)",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'userPermissions', content_type=>'application/json', value=>
"{ \"notUser\" : [\"DISCOVER\"], \"$username1\" : [\"ADMIN\",\"UPLOAD\",\"CONSUME\",\"MODIFY\",\"ANNOTATE\",\"DISCOVER\"] }"},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /User not found/;
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

$json = HttpHelper::put_multipart (
    desc  => "upsert & change permissions (to be the same)",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'userPermissions', content_type=>'application/json', value=>
"{ \"public\" : [\"DISCOVER\"], \"$username1\" : [\"ADMIN\",\"UPLOAD\",\"CONSUME\",\"MODIFY\",\"ANNOTATE\",\"DISCOVER\"] }"
            },
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{transform}{key} eq $transform1;
HttpHelper::fail("fail") unless $$json{transform}{metadata};
HttpHelper::fail("fail") unless $$json{transform}{userPermissions}{public};
HttpHelper::fail("fail") unless @{$$json{transform}{userPermissions}{public}} == 1;
HttpHelper::fail("fail") unless $$json{transform}{userPermissions}{$username1};
HttpHelper::fail("fail") unless @{$$json{transform}{userPermissions}{$username1}} == 6;

#=============================================================================
# upsert group permissions

$json = HttpHelper::put_multipart (
    desc  => "upsert & change group permissions (bogus json)",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'groupPermissions', content_type=>'application/json', value=> "invalid json"},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /bad JSON/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put_multipart (
    desc  => "upsert & change group permissions (json in wrong form)",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'groupPermissions', content_type=>'application/json', value=> q#{ "foo": false }#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /bad JSON/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put_multipart (
    desc  => "upsert & change group permissions (json with invalid permission)",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'groupPermissions', content_type=>'application/json', value=> q#{ "public" : ["NOT_A_VALID_PERMISSION"] }#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /bad JSON/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put_multipart (
    desc  => "upsert & change permissions (json with invalid group)",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'groupPermissions', content_type=>'application/json', value=> "{ \"notGroup\" : [\"DISCOVER\"] }" },
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /Group not found/;
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

$json = HttpHelper::put_multipart (
    desc  => "upsert & change permissions (to add group 1)",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'groupPermissions', content_type=>'application/json', value=>
"{ \"$group1\" : [\"ADMIN\",\"UPLOAD\",\"CONSUME\",\"MODIFY\",\"ANNOTATE\",\"DISCOVER\"] }" },
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{transform}{key} eq $transform1;
HttpHelper::fail("fail") unless $$json{transform}{metadata};
HttpHelper::fail("fail") unless $$json{transform}{groupPermissions}{$group1};
HttpHelper::fail("fail") unless @{$$json{transform}{groupPermissions}{$group1}} == 6;

#=============================================================================
# upsert output streams

$json = HttpHelper::put_multipart (
    desc  => "add output streams using upsert - incorrect json",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'outputStreams', content_type=>'application/json', value=> "invalid json"},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /bad JSON/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::post (
    desc => "create datastreamOut1 for output streams",
    path => "/datastream/create",
    params => {},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStreamOut1=$$json{dataStream};

$json = HttpHelper::post (
    desc => "create datastreamOut2 for output streams",
    path => "/datastream/create",
    params => {},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStreamOut2=$$json{dataStream};

my @outputStream_data = ({ "key" => $dataStreamOut1, "alias" => "stream1" });
my $outputStream = encode_json \@outputStream_data;

$json = HttpHelper::put_multipart (
    desc  => "upsert output streams",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'outputStreams', content_type=>'application/json', value=> $outputStream},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{transform}{key} eq $transform1;
HttpHelper::fail("fail") unless $$json{transform}{outputStreams};
HttpHelper::fail("fail") unless @{$$json{transform}{outputStreams}} == 1;

my @duplicate_outputStream_data = ({ "key" => $dataStreamOut1, "alias" => "stream1" },
                                { "key" => $dataStreamOut1, "alias" => "stream2" });
my $duplicate_outputStream = encode_json \@duplicate_outputStream_data;

$json = HttpHelper::put_multipart (
    desc  => "upsert outputStream with duplicate stream",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'outputStreams', content_type=>'application/json', value=> $duplicate_outputStream},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '409';

my @duplicate_alias_data = ({ "key" => $dataStreamOut1, "alias" => "stream1" },
                            { "key" => $dataStreamOut2, "alias" => "stream1" });
my $duplicate_alias = encode_json \@duplicate_alias_data;

$json = HttpHelper::put_multipart (
    desc  => "upsert outputstream with duplicate alias",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'outputStreams', content_type=>'application/json', value=> $duplicate_alias},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '409';

my @sources_output_data = ({
        'source' => $dataStreamOut1,
        'alias' => 'Test',
        'trigger' => 'false',
        'aggregation' => {
            'aggregationType' => 'type',
            'aggregationSpec' => 'spec' }
        });
my $sources_outputStream = encode_json \@sources_output_data;

$json = HttpHelper::put_multipart (
    desc  => "duplicate source stream",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'sources', content_type=>'application/json', value=> $sources_outputStream},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put_multipart (
    desc  => "upsert empty outputstream",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'outputStreams', content_type=>'application/json', value=> q#[]#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{transform}{key} eq $transform1;
HttpHelper::fail("fail") if $$json{transform}{outputStreams};

#=============================================================================
# upsert sources

$json = HttpHelper::put_multipart (
    desc  => "add sources using upsert - incorrect json",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'sources', content_type=>'application/json', value=> "invalid json"},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /bad JSON/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put_multipart (
    desc  => "upsert empty sources",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'sources', content_type=>'application/json', value=> q#[]#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{transform}{key} eq $transform1;
HttpHelper::fail("fail") if $$json{transform}{sources};

my @sources_data = ({
        'source' => $dataStream1,
        'alias' => 'Test',
        'trigger' => 'false',
        'aggregationType' => 'type',
        'aggregationSpec' => 'spec'
});
my $sources = encode_json \@sources_data;

$json = HttpHelper::put_multipart (
    desc  => "upsert sources",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'sources', content_type=>'application/json', value=> $sources},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{transform}{key} eq $transform1;
HttpHelper::fail("fail") unless $$json{transform}{sources};
HttpHelper::fail("fail") unless @{$$json{transform}{sources}} == 1;
HttpHelper::fail("fail") unless ${$$json{transform}{sources}}[0]{alias} eq "Test";

my @source_outputStream_data = ({ "key" => $dataStream1, "alias" => "duplicateStream" });
my $source_outputStream = encode_json \@source_outputStream_data;

$json = HttpHelper::put_multipart (
    desc  => "upsert sources",
    path  => "/transform/upsert",
    parts => HttpHelper::create_multiparts(
        {name=>"transform", value => $transform1},
        {name=>'outputStreams', content_type=>'application/json', value=> $source_outputStream},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

#=============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

