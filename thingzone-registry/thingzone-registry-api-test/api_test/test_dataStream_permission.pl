# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;

# connect
my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";
(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

my $json;

my $sessionKey_userAdmin=ThingzoneLogin::login_admin_user() or die;

# test user 1
my $unique = time();
my $username1 = "user_stream_perm_1_$unique";
my $password1 = "password_$unique";
my $username2 = "user_stream_perm_2_$unique";
my $password2 = "password_$unique";

# create 2x users
$json = HttpHelper::post (
    desc  => "Create user 1 ($username1)",
    path  => '/user/create',
    params=> {username=>$username1, password=>$password1, privileges=>"CREATE_DEVICE, CREATE_DATASTREAM"},
    sessionKey=>$sessionKey_userAdmin
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "Create user 2 ($username2)",
    path  => '/user/create',
    params=> {username=>$username2, password=>$password2, privileges=>"CREATE_DEVICE, CREATE_DATASTREAM"},
    sessionKey=>$sessionKey_userAdmin
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# log in 2x users
$json = HttpHelper::post (
    desc  => "Log in as user 1 ($username1)",
    path  => '/session/login',
    params=> {username=>$username1, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user1=$$json{sessionKey};


$json = HttpHelper::post (
    desc  => "Log in as user 2 ($username2)",
    path  => '/session/login',
    params=> {username=>$username2, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user2=$$json{sessionKey};


# each user creates a datastream

$json = HttpHelper::post (
    desc  => "create datastream for user 1",
    path  => "/datastream/create",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream1=$$json{dataStream};

$json = HttpHelper::post (
    desc  => "create datastream for user 2",
    path  => "/datastream/create",
    params => {},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream2=$$json{dataStream};


# each user can get permissions for their own datastream

my $TRUE=1;
my $FALSE=0;

$json = HttpHelper::put (
    desc  => "user 1 can update privileges to allow user 2, for user1's datastream",
    path  => "/permission/set",
    params => {'key'=>$dataStream1, 'username'=>$username2, 'permissions'=>'CONSUME, DISCOVER'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';



$json = HttpHelper::get (
    desc  => "user 1 cannot discover user2's datastream",
    path  => "/datastream/list",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
die unless @{$$json{dataStreams}} == 1;



$json = HttpHelper::get (
    desc  => "user 2 can discover user1's datastream (due to DISCOVER permission)",
    path  => "/datastream/list",
    params => {},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
die unless @{$$json{dataStreams}} == 2;

#=============================================================================


$json = HttpHelper::get (
    desc  => "user 2 can get details on user1's datastream (DISCOVER permission)",
    path  => "/datastream/get",
    params => {"dataStream" => $dataStream1},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::get (
    desc  => "user 1 cannot get details on user2's datastream",
    path  => "/datastream/get",
    params => {"dataStream" => $dataStream2},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

#=============================================================================

$json = HttpHelper::put_multipart (
    desc  => "user 2 can NOT modify user1's datastream (no MODIFY permission)",
    path  => "/datastream/update",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>'metadata', content_type=>'application/json', value=>q#{"thing":"yes"}#},
    ),
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::put_multipart (
    desc  => "user 1 can NOT modify user2's datastream (no MODIFY permission)",
    path  => "/datastream/update",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream2},
        {name=>'metadata', content_type=>'application/json', value=>q#{"thing":"yes"}#},
    ),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

# set permission
$json = HttpHelper::put (
    desc  => "user 1 updates privileges to give user 2 MODIFY permission",
    path  => "/permission/set",
    params => {'key'=>$dataStream1, 'username'=>$username2, 'permissions'=>'CONSUME, MODIFY, DISCOVER'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::put_multipart (
    desc  => "user 2 can modify user1's datastream (MODIFY permission)",
    path  => "/datastream/update",
    parts => HttpHelper::create_multiparts(
        {name=>"dataStream", value => $dataStream1},
        {name=>'metadata', content_type=>'application/json', value=>q#{"thing":"yes"}#},
    ),
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


#=============================================================================
# 'public' users

$json = HttpHelper::put (
    desc  => "user revokes all privs to user 2",
    path  => "/permission/set",
    params => {'key'=>$dataStream1, 'username'=>$username2, 'permissions'=>''},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::get (
    desc  => "user 2 can no longer get user1's datastream",
    path  => "/datastream/get",
    params => {"dataStream" => $dataStream1},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} eq 'authentication error';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::put (
    desc  => "user grants to 'public' user",
    path  => "/permission/set",
    params => {'key'=>$dataStream1, 'username'=>'public', 'permissions'=>'DISCOVER'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::get (
    desc  => "user1 only sees 1 datastream, even though 'public' access has been granted",
    path  => "/datastream/list",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
die unless @{$$json{dataStreams}} == 1;


#============================================================================
# deletion

$json = HttpHelper::delete (
    desc  => "user2 cannot delete user1's datastream",
    path  => "/datastream/delete",
    params => {dataStream=>$dataStream1},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';


$json = HttpHelper::put (
    desc  => "user1 grants ADMIN,MODIFY to user2",
    path  => "/permission/set",
    params => {'key'=>$dataStream1, 'username'=>$username2, 'permissions'=>'ADMIN,MODIFY'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::delete (
    desc  => "user2 can now delete user1's datastream, based on ADMIN priv",
    path  => "/datastream/delete",
    params => {dataStream=>$dataStream1},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


#=============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

