# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;

my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";

(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

my $json;

# login
my %login1 = ThingzoneLogin::login_section('details');
my $sessionKey_user1 = $login1{sessionKey} or die;


# create DataStream

$json = HttpHelper::post ("/datastream/create", {});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream=$$json{dataStream};

# metadata for the datastream
$json = HttpHelper::put_multipart ("/datastream/update", HttpHelper::create_multiparts(
    {name=>'dataStream', value=>$dataStream},
    {name=>'metadata', content_type=>'application/json', value=>q#{"stringVal":"sixty nine","intVal":69, "test":"a number"}#},
    ));
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::put ("/dns/add_record", {key=>$dataStream, description=>'dns on dataStream', RRTYPE=>"A", RDATA=>"1.2.3.4"});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# get details
$json = HttpHelper::get ("/datastream/get", {'dataStream' => $dataStream, 'detail'=>'full'});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream}{metadata}{stringVal} eq "sixty nine";
HttpHelper::fail("fail") unless $$json{dataStream}{dnsRecords}[0]{description} eq "dns on dataStream";


# pull subscription for the datastream
$json = HttpHelper::put_multipart ("/pull_subscription/set", HttpHelper::create_multiparts(
    {name=>'dataStream', value=>$dataStream},
    {name=>'uri', value=>'http://example.com'},
    {name=>'uriParams', content_type=>'application/json', value=>q#{"httpHeaders":{"SessionHeaderId": "1234"} }#},
    {name=>'interval', value=>42},
    ));
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# set push subscriber on dataStream
$json = HttpHelper::put_multipart ("/push_subscribers/set", HttpHelper::create_multiparts(
    {name=>'dataStream', value=>$dataStream},
    {name=>'uri', value=>'http://example.com'},
    {name=>'uriParams', content_type=>'application/json', value=>q#{"httpHeaders":{"SessionHeaderId": "1234"} }#},
    ));
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# create Device
$json = HttpHelper::post ("/device/create", {});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $device=$$json{device};
$json = HttpHelper::put_multipart ("/device/update", HttpHelper::create_multiparts(
    {name=>'device', value=>$device},
    {name=>'metadata', content_type=>'application/json', value=>q#{"stringVal":"forty three","intVal":43, "test":"something different"}#},
    ));
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put ("/device/add_stream", {'device' => $device, 'dataStream' => $dataStream, 'name' => 's1'});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::put ("/dns/add_record", {key=>$device, description=>'dns on device', RRTYPE=>"A", RDATA=>"1.2.3.255"});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# simple get device
$json = HttpHelper::get ("/device/get", {"device" => $device});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{device}{metadata}{stringVal} eq "forty three";

# get device with details
$json = HttpHelper::get ("/device/get", {'device' => $device, 'detail'=>'full'});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{device}{metadata}{stringVal} eq "forty three";
HttpHelper::fail("fail") unless $$json{device}{dnsRecords}[0]{description} eq "dns on device";
HttpHelper::fail("fail") unless $$json{device}{dataStreams}[0]{dnsRecords}[0]{description} eq "dns on dataStream";
HttpHelper::fail("fail") unless $$json{device}{dataStreams}[0]{metadata}{stringVal} eq "sixty nine";


# create a transform function
my $unique = time();
my $function_name = "function_$unique";
my $owner_name = "owner_$unique";
$json = HttpHelper::put_multipart ("/transform/create_function", HttpHelper::create_multiparts(
    {name=>'name', value=>$function_name},
    {name=>'owner', value=>$owner_name},
    {name=>'functionType', value=>'javascript'},
    {name=>'functionContent', value=>"console.log('hello world')"})
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# create a transform
$json = HttpHelper::post_multipart ("/transform/create", HttpHelper::create_multiparts(
    {name=>'functionName', value=>$function_name},
    {name=>'functionIsPublic', value=>'false'},
    {name=>'parameterValues', content_type=>'application/json', value=>q#{"value":"foo"}#},
    ));
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $transform=$$json{transform};
die unless $transform;


# add a stream
$json = HttpHelper::put ("/transform/update_source", {'transform' => $transform, 'stream' => $dataStream, 'alias' => 'my favourite stream'});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# set push subscriber on transform's datastream
$json = HttpHelper::put_multipart ("/push_subscribers/set", HttpHelper::create_multiparts(
    {name=>'dataStream', value=>$transform},
    {name=>'uri', value=>'http://transform-push.example.com'},
    {name=>'uriParams', content_type=>'application/json', value=>q#{"httpHeaders":{"SessionHeaderId": "asda"} }#},
    ));
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# create a key
$json = HttpHelper::post ("/entity/create_key", {});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dnskey=$$json{key} or die;


# add some records
$json = HttpHelper::put ("/dns/add_record", {key=>$dnskey, subdomain=>"latest", TTL=>42, RRTYPE=>"A", RDATA=>"1.2.3.4"});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# (another datastream: tests serialisation of "Simple" datastream in search/list
$json = HttpHelper::post ("/datastream/create", {});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream2=$$json{dataStream};


# test search/list by going through & checking all references are satisfied
$json = HttpHelper::get ("/search/list", {});

my %references;
my %full_objects;

for my $datastream (@{$$json{dataStreams}}) {
    my $key = $$datastream{key} or die;
    print "DataStream: key=$key\n";
    $full_objects{$key} = 'datastream';
}
for my $device (@{$$json{devices}}) {
    my $key = $$device{key} or die;
    print "Device: key=$key\n";

    $full_objects{$key} = 'device';

    for my $device_datastream (@{$$device{dataStreams}}) {
        print "\tdatastream: key=$$device_datastream{key}\n";
        $references{$key} = 'datastream';
        # check reference to DataStream exists
        die unless $full_objects{$$device_datastream{key}};
        print "DataStream reference checked OK\n";
    }
}
for my $dnskey (@{$$json{keys}}) {
    my $key = $$dnskey{key} or die;
    print "bare DNSkey=$key\n";
    $full_objects{$key} = 'key';
}

for my $transform (@{$$json{transforms}}) {
    my $key = $$transform{key} or die;
    print "Transform output stream key=$key\n";

    $full_objects{$key} = 'transform';

    for my $sourcestream(@{$$transform{sources}}) {
        my $sourcestream_key = $$sourcestream{source};
        print "\ttransform source stream: key=$sourcestream_key\n";

        # check reference to source DataStream exists
        die unless $full_objects{$sourcestream_key};
        print "Transform DataSource reference checked OK\n";
    }
}


#===================================================================
# test "identify"

$json = HttpHelper::get ("/search/identify", {'key' => $device});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
die unless defined $$json{'device'};

$json = HttpHelper::get (
    desc  => "device with full details",
    path  => "/search/identify",
    params=> {'key' => $device, 'detail' => 'full'},
    sessionKey=>$sessionKey_user1,
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{'device'};
HttpHelper::fail("fail") unless $$json{device}{dataStreams}[0]{key} eq $dataStream;

$json = HttpHelper::get ("/search/identify", {'key' => $dataStream});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{'dataStream'};

$json = HttpHelper::get ("/search/identify", {'key' => $transform});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{'transform'};

$json = HttpHelper::get ("/search/identify", {'key' => $dnskey});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{'dnsKey'};

$json = HttpHelper::get ("/search/identify", {'key' => 'bogus'});
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';


#===================================================================
# test delete
# (while we've got a full set of data)

$json = HttpHelper::delete (
    desc  => "cannot delete device which has datastreams",
    path  => "/device/delete",
    params => {device=>$device},
    sessionKey=>$sessionKey_user1,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} eq 'child object exists';
HttpHelper::fail("fail") unless $$json{status_code} eq '409';

$json = HttpHelper::get (
    desc  => "device still exists",
    path  => "/search/identify",
    params=> {'key' => $device},
    sessionKey=>$sessionKey_user1,
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{'device'};


$json = HttpHelper::delete (
    desc  => "cannot delete datastream while attached to device",
    path  => "/datastream/delete",
    params => {dataStream=>$dataStream},
    sessionKey=>$sessionKey_user1,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} eq 'child object exists';
HttpHelper::fail("fail") unless $$json{status_code} eq '409';


$json = HttpHelper::put (
    desc  => "detach datastream",
    path  => "/device/remove_stream",
    params => {device=>$device, dataStream=>$dataStream},
    sessionKey=>$sessionKey_user1,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::delete (
    desc  => "cannot delete datastream while it is source for transform",
    path  => "/datastream/delete",
    params => {dataStream=>$dataStream},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} eq 'child object exists';
HttpHelper::fail("fail") unless $$json{status_code} eq '409';


$json = HttpHelper::get (
    desc  => "datastream still exists",
    path  => "/search/identify",
    params=> {'key' => $dataStream},
    sessionKey=>$sessionKey_user1,
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{'dataStream'};


$json = HttpHelper::delete (
    desc  => "delete transform, disconnecting source datastream",
    path  => "/transform/delete",
    params => {transform=>$transform},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::get (
    desc  => "datastream still exists",
    path  => "/search/identify",
    params=> {'key' => $dataStream},
    sessionKey=>$sessionKey_user1,
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{'dataStream'};


$json = HttpHelper::get (
    desc  => "transform has been deleted",
    path  => "/transform/get",
    params=> {'transform' => $transform},
    sessionKey=>$sessionKey_user1,
    );
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';


$json = HttpHelper::delete (
    desc  => "can now delete datastream",
    path  => "/datastream/delete",
    params => {dataStream=>$dataStream},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::get (
    desc  => "datastream has been deleted",
    path  => "/datastream/get",
    params => {dataStream=>$dataStream},
    sessionKey=>$sessionKey_user1,
    );
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

$json = HttpHelper::delete (
    desc  => "can now delete device",
    path  => "/device/delete",
    params => {device=>$device},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::get (
    desc  => "device has been deleted",
    path  => "/device/get",
    params => {device=>$device},
    sessionKey=>$sessionKey_user1,
    );
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

#===================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;


