# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;


my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";

(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

my $json;
my $sessionKey_userAdmin=ThingzoneLogin::login_admin_user() or die;


#============================================================================
# test prerequisites

# test user 1
my $unique = time();
my $username1 = "user_device_perm_1_$unique";
my $password1 = "password_$unique";
my $username2 = "user_device_perm_2_$unique";
my $password2 = "password_$unique";

# create 2x users
$json = HttpHelper::post (
    desc  => "Create user 1 ($username1)",
    path  => '/user/create',
    params=> {username=>$username1, password=>$password1, privileges=>"CREATE_DEVICE, CREATE_DATASTREAM"},
    sessionKey=>$sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "Create user 2 ($username2)",
    path  => '/user/create',
    params=> {username=>$username2, password=>$password2, privileges=>"CREATE_DEVICE, CREATE_DATASTREAM"},
    sessionKey=>$sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# log in 2x users
$json = HttpHelper::post (
    desc  => "Log in as user 1 ($username1)",
    path  => '/session/login',
    params=> {username=>$username1, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user1=$$json{sessionKey};


$json = HttpHelper::post (
    desc  => "Log in as user 2 ($username2)",
    path  => '/session/login',
    params=> {username=>$username2, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user2=$$json{sessionKey};


$json = HttpHelper::post (
    desc => "create datastream for test",
    path => "/datastream/create",
    params => {},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream1=$$json{dataStream};


#============================================================================
# creation

my $create_parts = HttpHelper::create_multiparts(
        {name=>'dataStream', value=>$dataStream1},
        {name=>'uri', value=>'http://example.com'},
        {name=>'uriParams', content_type=>'application/json', value=>q#{"httpHeaders":{"SessionHeaderId": "1234"} }#},
        {name=>'interval', value=>42},
    );


$json = HttpHelper::put_multipart (
    desc  => "user2 cannot create pull subscription for user1's datastream",
    path  => "/pull_subscription/set",
    parts => $create_parts,
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} eq 'authentication error';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::put_multipart (
    desc  => "user1 can create pull subscription",
    path  => "/pull_subscription/set",
    parts => $create_parts,
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

#=============================================================================
# list

$json = HttpHelper::get (
    desc => "user1 can list",
    path =>"/pull_subscription/list",
    params =>{},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{pullSubscriptions}} == 1;
HttpHelper::fail("fail") unless $$json{pullSubscriptions}[0]{dataStream} eq $dataStream1;


$json = HttpHelper::get (
    desc => "user2 cannot see in list",
    path =>"/pull_subscription/list",
    params =>{},
    sessionKey=>$sessionKey_user2
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{pullSubscriptions}} == 0;


#=============================================================================
# get

$json = HttpHelper::get (
    desc => "user1 can get",
    path =>"/pull_subscription/get",
    params => {dataStream=>$dataStream1},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{pullSubscription};
HttpHelper::fail("fail") unless $$json{pullSubscription}{dataStream} eq $dataStream1;


$json = HttpHelper::get (
    desc => "user2 cannot get",
    path =>"/pull_subscription/get",
    params => {dataStream=>$dataStream1},
    sessionKey=>$sessionKey_user2
    );
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} eq 'authentication error';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

#=============================================================================
# removal

$json = HttpHelper::put (
    desc => "user2 cannot delete pull subscription",
    path => "/pull_subscription/delete",
    params => {dataStream=>$dataStream1},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} eq 'authentication error';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::put (
    desc => "user1 can delete pull subscription",
    path => "/pull_subscription/delete",
    params => {dataStream=>$dataStream1},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


#============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

