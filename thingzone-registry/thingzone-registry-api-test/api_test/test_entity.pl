#!/usr/bin/perl -w
# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

use strict;
use warnings FATAL => 'all';
use File::Basename;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;

# connect
my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";
(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

# login
my %login1 = ThingzoneLogin::login_section('entity');
my $sessionKey_user1 = $login1{sessionKey} or die;

my $json;

$json = HttpHelper::get(
    desc  => "cget user details",
    path  => "/user/get",
    params=> {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $username1 = $$json{user}{username};

#============================================================================
# Create key

$json = HttpHelper::post(desc  => "create key",
    path  => "/entity/create_key",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $key=$$json{key} or die;

$json = HttpHelper::post(desc  => "create key 2 with name and type",
    path  => "/entity/create_key",
    params => { "name"=>"key name", "type"=>"thing"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $key2=$$json{key} or die;

$json = HttpHelper::get(
    desc  => "check that key 2 was created with a name and type",
    path  => "/search/identify",
    params=> {'key' => $key2},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dnsKey}{name} eq 'key name';
HttpHelper::fail("fail") unless $$json{dnsKey}{type} eq 'thing';

#============================================================================
# Update entity

$json = HttpHelper::put(
    desc  => "update entity type",
    path  => "/entity/update",
    params => { 'key' => $key, 'type' => 'new_type' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc  => "Check type has been updated",
    path  => "/search/identify",
    params=> {'key' => $key},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dnsKey}{type} eq 'new_type';

$json = HttpHelper::put(
    desc  => "unset entity type",
    path  => "/entity/update",
    params => { 'key' => $key, 'type' => '' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc  => "Check type has been updated",
    path  => "/search/identify",
    params=> {'key' => $key},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") if (defined $$json{dnsKey}{type});

$json = HttpHelper::put(
    desc  => "unset entity type",
    path  => "/entity/update",
    params => { 'key' => $key },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put(
    desc  => "update entity type name",
    path  => "/entity/update",
    params => { 'key' => $key, 'name' => 'name' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc  => "Check name has been updated",
    path  => "/search/identify",
    params=> {'key' => $key},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dnsKey}{name} eq 'name';

$json = HttpHelper::put(
    desc  => "update entity name and type",
    path  => "/entity/update",
    params => { 'key' => $key, 'name' => 'updated name', 'type' => 'updated type' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc  => "Check name and type have been updated",
    path  => "/search/identify",
    params=> {'key' => $key},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dnsKey}{name} eq 'updated name';
HttpHelper::fail("fail") unless $$json{dnsKey}{type} eq 'updated type';

$json = HttpHelper::put(
    desc  => "unset entity name and type",
    path  => "/entity/update",
    params => { 'key' => $key, 'name' => '', 'type' => '' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc  => "Check name and type have been unset",
    path  => "/search/identify",
    params=> {'key' => $key},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") if $$json{dnsKey}{name};
HttpHelper::fail("fail") if $$json{dnsKey}{type};

#============================================================================
# List by type

$json = HttpHelper::post("/entity/create_key", {});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $key1=$$json{key};

$json = HttpHelper::put(
    desc  => "update entity type",
    path  => "/entity/update",
    params => { 'key' => $key1, 'type' => 'camera' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "create device for user 1",
    path  => "/device/create",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $device1=$$json{device};

$json = HttpHelper::post (
    desc  => "create device for user 1",
    path  => "/device/create",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $device2=$$json{device};

$json = HttpHelper::get(
    desc  => "List all entities",
    path  => "/entity/list",
    params=> {'detail' => 'full'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar(@{$$json{entities}}) == 5;

$json = HttpHelper::put(
    desc  => "update entity type",
    path  => "/entity/update",
    params => { 'key' => $device1, 'type' => 'camera' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc  => "List by type camera with full details",
    path  => "/entity/list",
    params=> {'type' => 'camera', 'detail' => 'full'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar(@{$$json{entities}}) == 2;

$json = HttpHelper::put(
    desc  => "update entity type",
    path  => "/entity/update",
    params => { 'key' => $key1, 'type' => '' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc  => "Check updating the type changes the returned list",
    path  => "/entity/list",
    params=> {'type' => 'camera'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar(@{$$json{entities}}) == 1;
HttpHelper::fail("fail") unless $$json{entities}[0]{key} eq $device1;

$json = HttpHelper::get(
    desc  => "List entities with an empty type",
    path  => "/entity/list",
    params=> {'detail' => 'full', 'type' => '' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar(@{$$json{entities}}) == 3;

$json = HttpHelper::post(desc  => "create datastream_name with entity name and type",
    path  => "/datastream/create",
    params => { "name"=>"datastream name", "type"=>"camera"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $datastream_name_type=$$json{dataStream};

$json = HttpHelper::get(
    desc  => "List all entities with admin permission ",
    path  => "/entity/list_admin",
    params=> {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar(@{$$json{entities}}) == 6;

$json = HttpHelper::get(
    desc  => "List all entities with admin permission limit 3",
    path  => "/entity/list_admin",
    params=> { size => 3 },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar(@{$$json{entities}}) == 3;

$json = HttpHelper::get(
    desc  => "List all entities with admin permission page 2",
    path  => "/entity/list_admin",
    params=> { size => 5, page => 1 },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail not ok") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar(@{$$json{entities}}) == 1;

$json = HttpHelper::get(
    desc  => "List all entities with admin permission type camera",
    path  => "/entity/list_admin",
    params=> { "type" => "camera" },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar(@{$$json{entities}}) == 2;

$json = HttpHelper::get(
    desc  => "List all entities with permissions for all_users group",
    path  => "/entity/list_admin",
    params=> { "showPermissions" => "true",  groupname=> 'all_users' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar(@{$$json{entities}}) == 6;
HttpHelper::fail("fail") unless $$json{entities}[0]{groupPermissions}{all_users} = [];
HttpHelper::fail("fail") if $$json{entities}[0]{userPermissions};

$json = HttpHelper::get(
    desc  => "List all entities with permissions for user1",
    path  => "/entity/list_admin",
    params=> { "showPermissions" => "true", username=> $username1 },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar(@{$$json{entities}}) == 6;
HttpHelper::fail("fail") unless @{$$json{entities}[0]{userPermissions}{$username1}} = 6;
HttpHelper::fail("fail") if $$json{entities}[0]{groupPermissions};

$json = HttpHelper::get(
    desc  => "List all entities with permissions for user1 limit size",
    path  => "/entity/list_admin",
    params=> { "showPermissions" => "true", username=> $username1, size => 1 },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar(@{$$json{entities}}) == 1;

$json = HttpHelper::get(
    desc  => "Fail when listing entities with permissions for non existent user",
    path  => "/entity/list_admin",
    params=> { "showPermissions" => "true", username=> 'non existent', size => 5 },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

#============================================================================
# Delete

$json = HttpHelper::delete(
    desc  => "delete key1",
    path  => "/entity/delete",
    params => { 'key' => $key1 },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc  => "Check key1 is deleted",
    path  => "/search/identify",
    params=> {'key' => $key1},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

$json = HttpHelper::delete(
    desc  => "Fail to delete a device key",
    path  => "/entity/delete",
    params => { 'key' => $device1 },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

#============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

