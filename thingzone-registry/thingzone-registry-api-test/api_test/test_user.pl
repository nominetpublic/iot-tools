# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;

# connect
my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";
(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

my $json;

# a unique user that we expect not to exist
my $unique = time();
my $username1 = "user_user_1_$unique";
my $password1 = "password_$unique";

$json = HttpHelper::post (
    desc  => "cannot log in as unknown user",
    path  => '/session/login',
    params=> {username=>$username1, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '401';

$json = HttpHelper::post (
    desc  => "cannot create a user unless logged in",
    path  => '/user/create',
    params=> {username=>$username1, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::post (
    desc  => "user_admin can set initial password (bootstrapping)",
    path  => '/session/login',
    params=> {username=>'user_admin', password=>'user_admin'}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_userAdmin=$$json{sessionKey} or die;

#=======================================================================
# create user
$json = HttpHelper::post (
    desc  => "user_admin can create a new user",
    path  => '/user/create',
    params=> {username=>$username1, password=>$password1},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "new user can't log in with wrong password",
    path  => '/session/login',
    params=> {username=>$username1, password=>'wrong password'}
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '401';

$json = HttpHelper::post (
    desc  => "User 1 can log in",
    path  => '/session/login',
    params=> {username=>$username1, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey=$$json{sessionKey};

$json = HttpHelper::get (
    desc  => "logged in user can get own details",
    path  => '/user/get',
    params=> {},
    sessionKey=> $sessionKey,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{user};
HttpHelper::fail("fail") unless $$json{user}{username} eq $username1;

$json = HttpHelper::post (
    desc  => "can't create a user that already exists",
    path  => '/user/create',
    params=> {username=>$username1, password=>$password1},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '409';

$json = HttpHelper::get (
    desc  => "get 'public' user details if not logged in (i.e. no session key supplied)",
    path  => '/user/get',
    params=> {},
    sessionKey=> undef,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{user};
HttpHelper::fail("fail") unless $$json{user}{username} eq 'public';


# create an additional user
my $username2 = "user_user_2_$unique";
my $password2 = "password_$unique";

$json = HttpHelper::post (
    desc  => "create another user",
    path  => '/user/create',
    params=> {username=>$username2, password=>$password2},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

#=======================================================================
# create user with password token

my $username3 = "user_user_3_$unique";
my $password3 = "password3_$unique";

$json = HttpHelper::post (
    desc  => "create another user",
    path  => '/user/create',
    params=> {username=>$username3 },
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{token};
my $creation_token = $$json{token};

$json = HttpHelper::post (
    desc  => "User 3 can't log in without password",
    path  => '/session/login',
    params=> {username=>$username1, password=>""}
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put(
    desc  => "user 3 can set their password",
    path  => '/user/set_password',
    params=> {username=>$username3, token=>$creation_token, newPassword=>$password3},
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "User 3 can log in now their password is set",
    path  => '/session/login',
    params=> {username=>$username3, password=>$password3}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

#=======================================================================
# change password

$json = HttpHelper::put (
    desc  => "user cannot change password if wrong current one supplied",
    path  => '/user/change_password',
    params=> {existingPassword=>'wrong', newPassword=>'irrelevant'},
    sessionKey=> $sessionKey,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '401';

$json = HttpHelper::put (
    desc  => "user cannot change password if not logged in",
    path  => '/user/change_password',
    params=> {existingPassword=>'wrong', newPassword=>'irrelevant'},
    sessionKey=> undef,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '401';

$json = HttpHelper::put (
    desc  => "user cannot set short password",
    path  => '/user/change_password',
    params=> {existingPassword=>$password1, newPassword=>'foo'},
    sessionKey=> $sessionKey,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ /invalid password/;
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

my $new_password = "this is a valid password!";
$json = HttpHelper::put (
    desc  => "user can update password",
    path  => '/user/change_password',
    params=> {existingPassword=>$password1, newPassword=>$new_password},
    sessionKey=> $sessionKey,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::post (
    desc  => "user logs out",
    path  => '/session/logout',
    params=> {},
    sessionKey=> $sessionKey,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::post (
    desc  => "user cannot log in with original password",
    path  => '/session/login',
    params=> {username=>$username1, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '401';

$json = HttpHelper::post (
    desc  => "user can log in with modified password",
    path  => '/session/login',
    params=> {username=>$username1, password=>$new_password}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
$sessionKey=$$json{sessionKey};


$json = HttpHelper::get (
    desc  => "user_admin can get details of other users",
    path  => '/user/get',
    params=> {username=>$username1},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{user};
HttpHelper::fail("fail") unless $$json{user}{username} eq $username1;


$json = HttpHelper::get (
    desc  => "user_admin cannot get details of nonexistent user",
    path  => '/user/get',
    params=> {username=>'idontexist'},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

$json = HttpHelper::get (
    desc  => "user1 cannot get details of other users",
    path  => '/user/get',
    params=> {username=>'user_admin'},
    sessionKey=> $sessionKey,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

#=======================================================================
# user password tokens
my $token_password = "setpasswordwithtoken";

$json = HttpHelper::get(
    desc  => "request a password reset token for username1",
    path  => '/user/request_token',
    params=> {username=>$username1},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $token = $$json{token};


$json = HttpHelper::get(
    desc  => "request a password reset token for username1",
    path  => '/user/request_token',
    params=> {username=>$username1},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $token2 = $$json{token};

$json = HttpHelper::get(
    desc  => "request a password reset token for username2 with 0 expiry",
    path  => '/user/request_token',
    params=> {username=>$username2, expiry=>0},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $token3 = $$json{token};

$json = HttpHelper::get(
    desc  => "request a password reset token without the correct privilege",
    path  => '/user/request_token',
    params=> {username=>$username2, expiry=>0},
    sessionKey=> $sessionKey,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::put(
    desc  => "update a password using a token code - fails due to duplicate user token auto expiry",
    path  => '/user/set_password',
    params=> {username=>$username1, token=>$token, newPassword=>$token_password},
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::put(
    desc  => "update a password using a token code",
    path  => '/user/set_password',
    params=> {username=>$username1, token=>$token2, newPassword=>$token_password},
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post(
    desc  => "user can log in with modified password",
    path  => '/session/login',
    params=> {username=>$username1, password=>$token_password}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
$sessionKey=$$json{sessionKey};
$json = HttpHelper::put(
    desc  => "can't use a used token",
    path  => '/user/set_password',
    params=> {username=>$username1, token=>$token, newPassword=>$token_password},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::put(
    desc  => "can't use an expired token",
    path  => '/user/set_password',
    params=> {username=>$username1, token=>$token, newPassword=>$token_password},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

#=======================================================================
# List users

# create user
$json = HttpHelper::get (
    desc  => "list names",
    path  => '/user/list_names',
    params=> {},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{users}} == 5;

$json = HttpHelper::put (
    desc  => "add dump all and use session to user2",
    path  => '/user/set_privileges',
    params=> {username=>$username2, privileges=>"DUMP_ALL, USE_SESSION"},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# create user
$json = HttpHelper::get (
    desc  => "list names",
    path  => '/user/list_names',
    params=> {},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{users}} == 4;

$json = HttpHelper::put (
    desc  => "add all privileges to user1",
    path  => '/user/set_privileges',
    params=> {username=>$username1, privileges=>"CREATE_USER, CREATE_GROUP, CREATE_ENTITY, CREATE_DEVICE, CREATE_DATASTREAM,
    CREATE_TRANSFORM, CREATE_TRANSFORMFUNCTION, CREATE_RESOURCE, CREATE_DETECTOR, RUN_DETECTOR"},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# create user
$json = HttpHelper::get (
    desc  => "list names",
    path  => '/user/list_names',
    params=> {},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{users}} == 4;

$json = HttpHelper::get (
    desc  => "non admin user can't see list",
    path  => '/user/list_names',
    params=> {},
    sessionKey=> $sessionKey,
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

#=======================================================================
# user properties & preferences

$json = HttpHelper::put_multipart (
    desc  => "user can update details and preferences",
    path  => "/user/update",
    parts => HttpHelper::create_multiparts(
        {name=>'details', content_type=>'application/json', value=>q#{"key1":"value1"}#},
        {name=>'preferences', content_type=>'application/json', value=>q#{"key2":"value2"}#},
    ),
    sessionKey=>$sessionKey
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::get (
    desc  => "user can read back details and preferences",
    path  => '/user/get',
    params=> {},
    sessionKey=> $sessionKey,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless defined $$json{user};
HttpHelper::fail("fail") unless defined $$json{user}{username};
HttpHelper::fail("fail") unless $$json{user}{username} eq $username1;
HttpHelper::fail("fail") unless defined $$json{user}{details};
HttpHelper::fail("fail") unless defined $$json{user}{details}{key1};
HttpHelper::fail("fail") unless $$json{user}{details}{key1} eq "value1";
HttpHelper::fail("fail") unless defined $$json{user}{preferences};
HttpHelper::fail("fail") unless defined $$json{user}{preferences}{key2};
HttpHelper::fail("fail") unless $$json{user}{preferences}{key2} eq "value2";


$json = HttpHelper::post (
    desc  => "user logs out",
    path  => '/session/logout',
    params=> {},
    sessionKey=> $sessionKey,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
$sessionKey=$$json{sessionKey};

$json = HttpHelper::post (
    desc  => "user logs in and gets details/prefs in response",
    path  => '/session/login',
    params=> {username=>$username1, password=>$token_password}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{user}{details}{key1} eq "value1";
HttpHelper::fail("fail") unless $$json{user}{preferences}{key2} eq "value2";

#=======================================================================
# update

$json = HttpHelper::put_multipart (
    desc  => "user can't update other users",
    path  => "/user/update",
    parts => HttpHelper::create_multiparts(
        {name=>'username', value => $username2},
        {name=>'details', content_type=>'application/json', value=>q#{"key1":"value1"}#},
        {name=>'preferences', content_type=>'application/json', value=>q#{"key2":"value2"}#},
    ),
    sessionKey=>$sessionKey
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::put_multipart (
    desc  => "user can't update their own username",
    path  => "/user/update",
    parts => HttpHelper::create_multiparts(
        {name=>'updatedName', value => $username2},
        {name=>'details', content_type=>'application/json', value=>q#{"key1":"value1"}#},
        {name=>'preferences', content_type=>'application/json', value=>q#{"key2":"value2"}#},
    ),
    sessionKey=>$sessionKey
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::put_multipart (
    desc  => "user admin specifies non existent username",
    path  => "/user/update",
    parts => HttpHelper::create_multiparts(
        {name=>'username', value => 'not a username'},
        {name=>'updatedName', value => 'newUsername'},
        {name=>'details', content_type=>'application/json', value=>q#{"key1":"value1"}#},
        {name=>'preferences', content_type=>'application/json', value=>q#{"key2":"value2"}#},
    ),
    sessionKey=>$sessionKey_userAdmin
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

$json = HttpHelper::put_multipart (
    desc  => "user admin specifies an existing username",
    path  => "/user/update",
    parts => HttpHelper::create_multiparts(
        {name=>'username', value => $username1},
        {name=>'updatedName', value => $username2},
        {name=>'details', content_type=>'application/json', value=>q#{"key1":"value1"}#},
        {name=>'preferences', content_type=>'application/json', value=>q#{"key2":"value2"}#},
    ),
    sessionKey=>$sessionKey_userAdmin
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '409';

$json = HttpHelper::put_multipart (
    desc  => "user admin updates the username for user1",
    path  => "/user/update",
    parts => HttpHelper::create_multiparts(
        {name=>'username', value => $username1},
        {name=>'updatedName', value => 'newUsername1'},
    ),
    sessionKey=>$sessionKey_userAdmin
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc  => "user_admin gets details of renamed user",
    path  => '/user/get',
    params=> {username=>'newUsername1'},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

#============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

