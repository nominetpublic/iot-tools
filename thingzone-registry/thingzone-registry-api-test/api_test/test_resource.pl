# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;

# connect
my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";
(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

# login
my %login1 = ThingzoneLogin::login_section('resource');
my $sessionKey_user1 = $login1{sessionKey} or die;

my $json;
#============================================================================
# Create resources

$json = HttpHelper::post("/device/create", {});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $device=$$json{device};

my $numbat;
open(my $fh1, '<', 'data/Numbat.jpg') or die "cannot open file data/Numbat.jpg";
{
    binmode($fh1);
    local $/;
    $numbat = <$fh1>;
}
close($fh1);

$json = HttpHelper::put_multipart(
    desc => "create a jpg resource",
    path => "/resource/upsert",
    parts=> HttpHelper::create_multiparts(
        {name=>'key', value=>$device},
        {name=>'resource_name', value=>'numbat'},
        {name=>'resource_type', value=>'image'},
        {name=>'content', value=>$numbat, content_type=>'image/jpeg', hide_content=>1}),
    hide_content=>1,
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $resource1=$$json{resource};

my $sun;
open(my $fh2, '<', 'data/sun.svg') or die "cannot open file data/sun.svg";
{
    binmode($fh2);
    local $/;
    $sun = <$fh2>;
}
close($fh2);

$json = HttpHelper::put_multipart(
    desc => "create a svg resource",
    path => "/resource/upsert",
    parts=> HttpHelper::create_multiparts(
        {name=>'key', value=>$device},
        {name=>'resource_name', value=>'sun'},
        {name=>'content', value=>$sun, content_type=>'image/svg+xml', hide_content=>1}),
    hide_content=>1,
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $resource2=$$json{resource};

$json = HttpHelper::post("/entity/create_key", {});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $key=$$json{key};

$json = HttpHelper::put_multipart(
    desc => "create a text resource",
    path => "/resource/upsert",
    parts=> HttpHelper::create_multiparts(
        {name=>'key', value=>$key},
        {name=>'resource_name', value=>'text'},
        {name=>'resource_type', value=>'config'},
        {name=>'content', value=>"randomtext", content_type=>'text/plain', }),
    hide_content=>1,
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $resource3=$$json{resource};

$json = HttpHelper::put_multipart(
    desc => "Create data_source json resource",
    path => "/resource/upsert",
    parts=> HttpHelper::create_multiparts(
        {name=>'type', value=>'data_source'},
        {name=>'name', value=>'json data'},
        {name=>'resource_type', value=>'config'},
        {name=>'resource_name', value=>'data'},
        {name=>'content', value=>q#{"key":"value"}#, content_type=>'application/json', }),
    hide_content=>1,
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{resource}{name} eq 'json data';
my $jsonresource1=$$json{resource};

$json = HttpHelper::put_multipart(
    desc => "Update the name of the resource entity",
    path => "/resource/upsert",
    parts=> HttpHelper::create_multiparts(
        {name=>'key', value=>$$jsonresource1{key}},
        {name=>'name', value=>'json updated'},
        {name=>'resource_name', value=>'data'},
        {name=>'content', value=>q#{"key":"value"}#, content_type=>'application/json', }),
    hide_content=>1,
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{resource}{key} eq $$jsonresource1{key};
HttpHelper::fail("fail") unless $$json{resource}{name} eq 'json updated';

$json = HttpHelper::put_multipart(
    desc => "Create 2nd data_source json resource",
    path => "/resource/upsert",
    parts=> HttpHelper::create_multiparts(
        {name=>'type', value=>'data_source'},
        {name=>'resource_name', value=>'data'},
        {name=>'content', value=>q#{"otherkey":"othervalue"}#, content_type=>'application/json', }),
    hide_content=>1,
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $jsonresource2=$$json{resource};

$json = HttpHelper::put_multipart(
    desc => "Create saved_search json resource",
    path => "/resource/upsert",
    parts=> HttpHelper::create_multiparts(
        {name=>'type', value=>'saved_search'},
        {name=>'resource_name', value=>'data'},
        {name=>'content', value=>q#{"search":"term"}#, content_type=>'application/json', }),
    hide_content=>1,
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $jsonresource3=$$json{resource};

$json = HttpHelper::put_multipart(
    desc => "Invalid json",
    path => "/resource/upsert",
    parts=> HttpHelper::create_multiparts(
        {name=>'type', value=>'data_source'},
        {name=>'resource_name', value=>'data'},
        {name=>'content', value=>q#{"otherkey":}#, content_type=>'application/json', }),
    hide_content=>1,
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put_multipart(
    desc => "upsert content is required",
    path => "/resource/upsert",
    parts=> HttpHelper::create_multiparts(
        {name=>'key', value=>$jsonresource3},
        {name=>'resource_name', value=>'data'}),
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

#============================================================================
# get resource

my $response = HttpHelper::get(
    desc =>"Get the sun resource",
    path =>"/resource/$device/sun",
    params => {},
    return_response=>1,
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $response->header('Content-Type') eq 'image/svg+xml';

$response = HttpHelper::get(
    desc  => "Get the numbat resource",
    path  => "/resource/$device/numbat",
    params => {},
    return_response=>1,
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $response->header('Content-Type') eq 'image/jpeg';

$json = HttpHelper::get(
    desc  => "Error not found",
    path  => "/resource/$device/nothing",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

#============================================================================
# list resources

$json = HttpHelper::get(
    desc  => "List all resources",
    path  => "/resource/list",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar(@{$$json{resources}}) == 6;

$json = HttpHelper::get(
    desc  => "List all filtered by type",
    path  => "/resource/list",
    params => {type=>'data_source'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar(@{$$json{resources}}) == 2;

$json = HttpHelper::get(
    desc  => "List all filtered by resource type",
    path  => "/resource/list",
    params => {resource_type=>'config'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar(@{$$json{resources}}) == 2;

$json = HttpHelper::get(
    desc  => "List all filtered by entity type and resource type",
    path  => "/resource/list",
    params => {type=>'data_source', resource_type=>'config'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar(@{$$json{resources}}) == 1;

$json = HttpHelper::get(
    desc  => "List all resources on device",
    path  => "/resource/list/$device",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar(@{$$json{resources}}) == 2;

$json = HttpHelper::get(
    desc  => "List json resources for the 'data_source' type",
    path  => "/resource/json",
    params=> {type=>'data_source'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar(@{$$json{resources}}) == 2;

$json = HttpHelper::get(
    desc  => "List json resources for the 'saved_search' type",
    path  => "/resource/json",
    params=> {type=>'saved_search'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar(@{$$json{resources}}) == 1;

$json = HttpHelper::get(
    desc  => "List json resources for the '' type",
    path  => "/resource/json",
    params=> {type=>''},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar(@{$$json{resources}}) == 0;

$json = HttpHelper::get(
    desc  => "List json resources with no type set",
    path  => "/resource/json",
    params=> {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::get(
    desc  => "List json resources with resourceType 'config'",
    path  => "/resource/json",
    params=> {type=>'data_source', resource_type=>'config'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar(@{$$json{resources}}) == 1;

#============================================================================
# delete resources

$json = HttpHelper::delete(
    desc  => "Delete the sun",
    path  => "/resource/$device/sun",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc  => "List all resources on device",
    path  => "/resource/list/$device",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar(@{$$json{resources}}) == 1;

$json = HttpHelper::delete (
    desc  => "user1 deletes device",
    path  => "/device/delete",
    params => {device=>$device},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put_multipart(
    desc => "create a svg resource on a deleted key",
    path => "/resource/upsert",
    parts=> HttpHelper::create_multiparts(
        {name=>'key', value=>$device},
        {name=>'resource_name', value=>'sun'},
        {name=>'content', value=>$sun, content_type=>'image/svg+xml', hide_content=>1}),
    hide_content=>1,
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

#============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

