# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM = basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;

# connect
my $base_url = "http://$server:$port/api";
print "connecting to $base_url...\n";
(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

my $json;
my $sessionKey_userAdmin = ThingzoneLogin::login_admin_user() or die;

#============================================================================
# Create group
my $unique    = time();
my $group1    = "group_1_$unique";
my $group2    = "group_2_$unique";
my $username1 = "user_device_perm_1_$unique";
my $password1 = "password_$unique";
my $username2 = "user_device_perm_2_$unique";
my $password2 = "password_$unique";

# create 2x users (via user_admin's session)
$json = HttpHelper::post(
    desc   => "Create user 1 ($username1)",
    path   => '/user/create',
    params => {
        username   => $username1,
        password   => $password1,
        privileges => "CREATE_GROUP, CREATE_DEVICE, CREATE_DATASTREAM"
    },
    sessionKey => $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post(
    desc   => "Create user 2 ($username2)",
    path   => '/user/create',
    params => {
        username   => $username2,
        password   => $password2,
        privileges => "CREATE_GROUP, CREATE_DEVICE, CREATE_DATASTREAM"
    },
    sessionKey => $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# log in 2x users
$json = HttpHelper::post(
    desc   => "Log in as user 1 ($username1)",
    path   => '/session/login',
    params => { username => $username1, password => $password1 }
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user1 = $$json{sessionKey};

$json = HttpHelper::post(
    desc   => "Log in as user 2 ($username2)",
    path   => '/session/login',
    params => { username => $username2, password => $password1 }
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user2 = $$json{sessionKey};

#============================================================================
# create groups

$json = HttpHelper::post(
    desc       => "create group 1",
    path       => "/groups/$group1",
    params     => {},
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post(
    desc       => "create group 2",
    path       => "/groups/$group2",
    params     => {},
    sessionKey => $sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put(
    desc       => "add users to group 1",
    path       => "/groups/$group1/users/add",
    params     => { 'usernames' => "$username1,$username2" },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

#============================================================================
# create datastreams

$json = HttpHelper::post(
    desc       => "create datastream 1 for user 1",
    path       => "/datastream/create",
    params     => {},
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream1 = $$json{dataStream};

$json = HttpHelper::post(
    desc       => "create datastream 2 for user 2",
    path       => "/datastream/create",
    params     => {},
    sessionKey => $sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream2 = $$json{dataStream};

#============================================================================
# set permissions

$json = HttpHelper::put(
    desc =>
"user 1 can update permissions to allow group 1 to see user1's datastream",
    path   => "/permission/set",
    params => {
        'key'         => $dataStream1,
        'group'       => $group1,
        'permissions' => 'CONSUME, DISCOVER'
    },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc       => "user 2 can see their permissions on user 1's datastream",
    path       => "/permission/get",
    params     => { 'key' => $dataStream1, 'username' => $username2 },
    sessionKey => $sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{ $$json{permissions}{userPermissions} } == 2;
HttpHelper::fail("fail") unless grep { /^CONSUME$/ } @{ $$json{permissions}{userPermissions} };
HttpHelper::fail("fail") unless grep { /^DISCOVER$/ } @{ $$json{permissions}{userPermissions} };

$json = HttpHelper::get(
    desc       => "user 1 can see that group 1 has permissions on datastream 1",
    path       => "/permission/list",
    params     => { 'key' => $dataStream1, 'group' => $group1 },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar( keys %{ $$json{groupPermissions} } ) == 1;
HttpHelper::fail("fail") unless grep { /^CONSUME$/ } @{ $$json{groupPermissions}{$group1} };
HttpHelper::fail("fail") unless grep { /^DISCOVER$/ } @{ $$json{groupPermissions}{$group1} };

$json = HttpHelper::put(
    desc =>
"user 1 can update permissions to allow group 2 to see user 1's datastream",
    path   => "/permission/set",
    params => {
        'key'         => $dataStream1,
        'group'       => $group2,
        'permissions' => 'CONSUME, DISCOVER, VIEW_METADATA'
    },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc =>
    "user 1 can see that group 1 and group 2 has permissions on datastream 1",
    path       => "/permission/list",
    params     => { 'key' => $dataStream1 },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless scalar( keys %{ $$json{userPermissions} } ) == 1;
HttpHelper::fail("fail") unless scalar( keys %{ $$json{groupPermissions} } ) == 2;
HttpHelper::fail("fail") unless grep { /^CONSUME$/ } @{ $$json{groupPermissions}{$group1} };
HttpHelper::fail("fail") unless grep { /^DISCOVER$/ } @{ $$json{groupPermissions}{$group1} };
HttpHelper::fail("fail") unless grep { /^CONSUME$/ } @{ $$json{groupPermissions}{$group2} };
HttpHelper::fail("fail") unless grep { /^DISCOVER$/ } @{ $$json{groupPermissions}{$group2} };
HttpHelper::fail("fail") unless grep { /^VIEW_METADATA$/ } @{ $$json{groupPermissions}{$group2} };

$json = HttpHelper::get(
    desc       => "user1 can specify only group 1 to show permissions for",
    path       => "/permission/list",
    params     => { 'key' => $dataStream1, 'group' => $group1 },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") if $$json{userPermissions};
HttpHelper::fail("fail") unless scalar( keys %{ $$json{groupPermissions} } ) == 1;
HttpHelper::fail("fail") unless grep { /^CONSUME$/ } @{ $$json{groupPermissions}{$group1} };
HttpHelper::fail("fail") unless grep { /^DISCOVER$/ } @{ $$json{groupPermissions}{$group1} };

$json = HttpHelper::get(
    desc       => "user1 can specify only username 1 to see permissions for",
    path       => "/permission/list",
    params     => { 'key' => $dataStream1, 'username' => $username1 },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") if $$json{groupPermissions};
HttpHelper::fail("fail") unless scalar( keys %{ $$json{userPermissions} } ) == 1;
HttpHelper::fail("fail") unless grep { /^CONSUME$/ } @{ $$json{userPermissions}{$username1} };
HttpHelper::fail("fail") unless grep { /^DISCOVER$/ } @{ $$json{userPermissions}{$username1} };

$json = HttpHelper::get(
    desc =>
"user1 can't individually specify both a user and a group to show permissions for",
    path => "/permission/list",
    params =>
    { 'key' => $dataStream1, 'username' => $username1, 'group' => $group1 },
    sessionKey => $sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

#=============================================================================

print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

