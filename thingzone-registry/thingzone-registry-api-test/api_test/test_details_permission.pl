# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use Data::Dumper;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;

my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";

(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

my $json;
my $sessionKey_userAdmin=ThingzoneLogin::login_admin_user() or die;

#============================================================================
# test prerequisites

# test user 1
my $unique = time();
my $username1 = "user_device_perm_1_$unique";
my $password1 = "password_$unique";
my $username2 = "user_device_perm_2_$unique";
my $password2 = "password_$unique";

# create 2x users
$json = HttpHelper::post (
    desc  => "Create user 1 ($username1)",
    path  => '/user/create',
    params=> {username=>$username1, password=>$password1, privileges=>"CREATE_DEVICE, CREATE_DATASTREAM, CREATE_TRANSFORM, CREATE_TRANSFORMFUNCTION, CREATE_ENTITY"},
    sessionKey=>$sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "Create user 2 ($username2)",
    path  => '/user/create',
    params=> {username=>$username2, password=>$password2, privileges=>"CREATE_DEVICE, CREATE_DATASTREAM, CREATE_TRANSFORM, CREATE_TRANSFORMFUNCTION, CREATE_ENTITY"},
    sessionKey=>$sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# log in 2x users
$json = HttpHelper::post (
    desc  => "Log in as user 1 ($username1)",
    path  => '/session/login',
    params=> {username=>$username1, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user1=$$json{sessionKey};


$json = HttpHelper::post (
    desc  => "Log in as user 2 ($username2)",
    path  => '/session/login',
    params=> {username=>$username2, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user2=$$json{sessionKey};

#============================================================================

$json = HttpHelper::put_multipart (
    desc => "user 1 creates function",
    path => "/transform/create_function",
    parts=> HttpHelper::create_multiparts(
        {name=>'name', value=>'testfn'},
        {name=>'functionType', value=>'javascript'},
        {name=>'functionContent', value=>"console.log('test')"}),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::post (
    desc  => "create datastream",
    path  => "/datastream/create",
    params=>{},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream1=$$json{dataStream};


$json = HttpHelper::put_multipart (
    desc  => "add push subscriber",
    path  => "/push_subscribers/set",
    parts => HttpHelper::create_multiparts(
        {name=>'dataStream', value=>$dataStream1},
        {name=>'uri', value=>'http://transform-push.example.com'},
        {name=>'uriParams', content_type=>'application/json', value=>q#{"httpHeaders":{"SessionHeaderId": "asda"} }#},
        ),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::put_multipart (
    desc  => "add pull subscriber",
    path  => "/pull_subscription/set",
    parts => HttpHelper::create_multiparts(
        {name=>'dataStream', value=>$dataStream1},
        {name=>'uri', value=>'http://example.com'},
        {name=>'uriParams', content_type=>'application/json', value=>q#{"httpHeaders":{"SessionHeaderId": "1234"} }#},
        {name=>'interval', value=>42},
        ),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc  => "add DNS record to datastream",
    path  => "/dns/add_record",
    params=> {key=>$dataStream1, description=>'dns on dataStream', RRTYPE=>"A", RDATA=>"1.2.3.4"},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::post (
    desc  => "create device",
    path  => "/device/create",
    params=> {},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $device1=$$json{device};


$json = HttpHelper::put (
    desc  => "add stream to device",
    path  => "/device/add_stream",
    params=>{'device' => $device1, 'dataStream' => $dataStream1, 'name' => 's1'},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc  => "add DNS to device",
    path  => "/dns/add_record",
    params=> {key=>$device1, description=>'dns on device', RRTYPE=>"A", RDATA=>"1.2.3.255"},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';




# create a transform


$json = HttpHelper::post_multipart (
    desc => "user 1 creates transform",
    path => "/transform/create",
    parts => HttpHelper::create_multiparts(
        {name=>'functionName', value=>'testfn'},
        {name=>'parameterValues', content_type=>'application/json', value=>q#{"foo":42}#},
        ),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $transform1=$$json{transform} or die;


# add a stream

$json = HttpHelper::put (
    desc => "user1 can attach datastream source",
    path => "/transform/update_source",
    params => {'transform' => $transform1, 'stream' => $dataStream1, 'alias' => 'a42'},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';



# create a key
$json = HttpHelper::post (
    desc  => "create a key",
    path  => "/entity/create_key",
    params=> {},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dnskey1=$$json{key} or die;

# add some records
$json = HttpHelper::put (
    desc  => "add DNS record to DNSkey",
    path  => "/dns/add_record",
    params=> {key=>$dnskey1, description=>'dns on DNSkey', RRTYPE=>"A", RDATA=>"1.2.3.4"},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc  => "add DNS record to transform",
    path  => "/dns/add_record",
    params=> {key=>$transform1, description=>'dns on transform', RRTYPE=>"A", RDATA=>"1.2.3.4"},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


#=============================================================================

my %stuff = (
    'device' => $device1,
    'dataStream' => $dataStream1,
    'transform' => $transform1,
    'dnsKey' => $dnskey1,
    );

# user1 can identify own stuff
while (my ($desc, $object) = each %stuff) {
    $json = HttpHelper::get (
        desc  => "user1 can identity own $desc",
        path  => "/search/identify",
        params=> {'key' => $object, 'detail' => 'full'},
        sessionKey=>$sessionKey_user1
        );
    HttpHelper::fail("fail") unless $$json{result} eq 'ok';
    die unless (defined $$json{$desc}{dnsRecords} || defined $$json{$desc}{outputStream}{dnsRecords});
}


# user2 cannot identify anything
while (my ($desc, $object) = each %stuff) {
    $json = HttpHelper::get (
        desc  => "user2 cannot identify $desc owned by user1",
        path  => "/search/identify",
        params=> {'key' => $object},
        sessionKey=>$sessionKey_user2
        );
    HttpHelper::fail("fail") unless $$json{result} eq 'fail';
    HttpHelper::fail("fail") unless $$json{reason} eq 'authentication error';
    HttpHelper::fail("fail") unless $$json{status_code} eq '403';
}

# grant DISCOVER permission
while (my ($desc, $object) = each %stuff) {
    $json = HttpHelper::put (
        desc  => "user 1 grants DISCOVER permission on $desc",
        path  => "/permission/set",
        params => {'key'=>$object, 'username'=>$username2, 'permissions'=>'DISCOVER'},
        sessionKey=>$sessionKey_user1
    );
    HttpHelper::fail("fail") unless $$json{result} eq 'ok';
}

# user2 can now identify
while (my ($desc, $object) = each %stuff) {
    $json = HttpHelper::get (
        desc  => "user2 can identify $desc owned by user1, based on DISCOVER",
        path  => "/search/identify",
        params=> {'key' => $object},
        sessionKey=>$sessionKey_user2
        );
    HttpHelper::fail("fail") unless $$json{result} eq 'ok';
    die if (defined $$json{$desc}{dnsRecords} || defined $$json{$desc}{outputStream}{dnsRecords});    # shouldn't get DNSrecords for DISCOVER
}

# grant MODIFY, CONSUME permission
while (my ($desc, $object) = each %stuff) {
    $json = HttpHelper::put (
        desc  => "user 1 grants DISCOVER,MODIFY,CONSUME permission on $desc",
        path  => "/permission/set",
        params => {'key'=>$object, 'username'=>$username2, 'permissions'=>'DISCOVER,MODIFY,CONSUME'},
        sessionKey=>$sessionKey_user1
    );
    HttpHelper::fail("fail") unless $$json{result} eq 'ok';
}

# user2 can now see full details
while (my ($desc, $object) = each %stuff) {
    $json = HttpHelper::get (
        desc  => "user2 can identify $desc owned by user1, based on DISCOVER+MODIFY+CONSUME",
        path  => "/search/identify",
        params=> {'key' => $object, 'detail' => 'full' },
        sessionKey=>$sessionKey_user2
        );
    HttpHelper::fail("fail") unless $$json{result} eq 'ok';
    die unless (defined $$json{$desc}{dnsRecords} || defined $$json{$desc}{outputStream}{dnsRecords});
}

#============================================================================
# search/list tests

$json = HttpHelper::put_multipart (
    desc => "user 1 creates transform function",
    path => "/transform/create_function",
    parts=> HttpHelper::create_multiparts(
        {name=>'name', value=>'function1'},
        {name=>'functionType', value=>'javascript'},
        {name=>'functionContent', value=>"console.log('hello world')"}),
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# revoke permissions on user2
while (my ($desc, $object) = each %stuff) {
    $json = HttpHelper::put (
        desc  => "user 1 revokes permissions on $desc",
        path  => "/permission/set",
        params => {'key'=>$object, 'username'=>$username2, 'permissions'=>''},
        sessionKey=>$sessionKey_user1
    );
    HttpHelper::fail("fail") unless $$json{result} eq 'ok';
}

$json = HttpHelper::get (
    desc  => "user1 lists all his stuff",
    path  => "/search/list",
    params=> {},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{keys}} == 1;
HttpHelper::fail("fail") unless @{$$json{transforms}} == 1;
HttpHelper::fail("fail") unless @{$$json{dataStreams}} == 1;
HttpHelper::fail("fail") unless @{$$json{devices}} == 1;
HttpHelper::fail("fail") unless @{$$json{transformFunctions}} == 2;


$json = HttpHelper::get (
    desc  => "user1 owns all his stuff",
    path  => "/search/list",
    params=> {permissions=>'ADMIN'},
    sessionKey=>$sessionKey_user1
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{keys}} == 1;
HttpHelper::fail("fail") unless @{$$json{transforms}} == 1;
HttpHelper::fail("fail") unless @{$$json{dataStreams}} == 1;
HttpHelper::fail("fail") unless @{$$json{devices}} == 1;
HttpHelper::fail("fail") unless @{$$json{transformFunctions}} == 2;


$json = HttpHelper::get (
    desc  => "user2 can see nothing",
    path  => "/search/list",
    params=> {},
        sessionKey=>$sessionKey_user2
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{keys}} == 0;
HttpHelper::fail("fail") unless @{$$json{transforms}} == 0;
HttpHelper::fail("fail") unless @{$$json{dataStreams}} == 0;
HttpHelper::fail("fail") unless @{$$json{devices}} == 0;
HttpHelper::fail("fail") unless @{$$json{transformFunctions}} == 2;

while (my ($desc, $object) = each %stuff) {
    $json = HttpHelper::put (
        desc  => "user 1 grants DISCOVER permission on $desc to user2",
        path  => "/permission/set",
        params => {'key'=>$object, 'username'=>$username2, 'permissions'=>'DISCOVER'},
        sessionKey=>$sessionKey_user1
    );
    HttpHelper::fail("fail") unless $$json{result} eq 'ok';
}
$json = HttpHelper::get (
    desc  => "user2 can see summary",
    path  => "/search/list",
    params=> {},
        sessionKey=>$sessionKey_user2
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{keys}} == 1;
HttpHelper::fail("fail") if defined $$json{keys}[0]{dnsRecords};
HttpHelper::fail("fail") unless @{$$json{transforms}} == 1;
HttpHelper::fail("fail") if defined $$json{transforms}[0]{sources};
HttpHelper::fail("fail") if defined $$json{transforms}[0]{function}{functionContent};
HttpHelper::fail("fail") if defined $$json{transforms}[0]{parameterValues};
HttpHelper::fail("fail") if defined $$json{transforms}[0]{outputStream}{dnsRecords};
HttpHelper::fail("fail") unless @{$$json{dataStreams}} == 1;
HttpHelper::fail("fail") if defined $$json{dataStreams}[0]{dnsRecords};
HttpHelper::fail("fail") unless @{$$json{devices}} == 1;
HttpHelper::fail("fail") if defined $$json{devices}[0]{dnsRecords};
HttpHelper::fail("fail") unless @{$$json{transformFunctions}} == 2;


while (my ($desc, $object) = each %stuff) {
    $json = HttpHelper::put (
        desc  => "user 1 grants DISCOVER,MODIFY permission on $desc to user2",
        path  => "/permission/set",
        params => {'key'=>$object, 'username'=>$username2, 'permissions'=>'DISCOVER,MODIFY'},
        sessionKey=>$sessionKey_user1
    );
    HttpHelper::fail("fail") unless $$json{result} eq 'ok';
}
$json = HttpHelper::get (
    desc  => "user2 can now see full details",
    path  => "/search/list",
    params=> { 'detail' => 'full' },
        sessionKey=>$sessionKey_user2
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{keys}} == 1;
HttpHelper::fail("fail") unless defined $$json{keys}[0]{dnsRecords};
HttpHelper::fail("fail") unless @{$$json{transforms}} == 1;
HttpHelper::fail("fail") unless defined $$json{transforms}[0]{sources};
HttpHelper::fail("fail") unless defined $$json{transforms}[0]{function};
HttpHelper::fail("fail") unless defined $$json{transforms}[0]{parameterValues};
HttpHelper::fail("fail") unless defined $$json{transforms}[0]{dnsRecords};
HttpHelper::fail("fail") unless @{$$json{dataStreams}} == 1;
HttpHelper::fail("fail") unless defined $$json{dataStreams}[0]{dnsRecords};
HttpHelper::fail("fail") unless @{$$json{devices}} == 1;
HttpHelper::fail("fail") unless defined $$json{devices}[0]{dnsRecords};
HttpHelper::fail("fail") unless @{$$json{transformFunctions}} == 2;


$json = HttpHelper::get (
    desc  => "but user2 is still admin on none of them",
    path  => "/search/list",
    params=> {permissions=>'ADMIN'},
    sessionKey=>$sessionKey_user2
    );
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{keys}} == 0;
HttpHelper::fail("fail") unless @{$$json{transforms}} == 0;
HttpHelper::fail("fail") unless @{$$json{dataStreams}} == 0;
HttpHelper::fail("fail") unless @{$$json{devices}} == 0;
HttpHelper::fail("fail") unless @{$$json{transformFunctions}} == 2;

#============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

