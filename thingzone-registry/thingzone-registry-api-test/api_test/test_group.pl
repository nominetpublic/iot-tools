# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;

# connect
my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";
(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

my $json;
my $sessionKey_userAdmin=ThingzoneLogin::login_admin_user() or die;

#============================================================================
# Create group
my $unique = time();
my $group1 = "group_1_$unique";
my $group2 = "group_2_$unique";
my $username1 = "user_device_perm_1_$unique";
my $password1 = "password_$unique";
my $username2 = "user_device_perm_2_$unique";
my $password2 = "password_$unique";

# create 2x users (via user_admin's session)
$json = HttpHelper::post (
    desc  => "Create user 1 ($username1)",
    path  => '/user/create',
    params=> {username=>$username1, password=>$password1, privileges=>"CREATE_GROUP"},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "Create user 2 ($username2)",
    path  => '/user/create',
    params=> {username=>$username2, password=>$password2, privileges=>"CREATE_GROUP"},
    sessionKey=> $sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# log in 2x users
$json = HttpHelper::post (
    desc  => "Log in as user 1 ($username1)",
    path  => '/session/login',
    params=> {username=>$username1, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user1=$$json{sessionKey};

$json = HttpHelper::post (
    desc  => "Log in as user 2 ($username2)",
    path  => '/session/login',
    params=> {username=>$username2, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user2=$$json{sessionKey};

#============================================================================
# create

$json = HttpHelper::post(
    desc  => "create group 1",
    path  => "/groups/$group1",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post(
    desc  => "create group 2",
    path  => "/groups/$group2",
    params => {},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post(
    desc  => "create duplicate group 2",
    path  => "/groups/$group2",
    params => {},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '409';

$json = HttpHelper::get(
    desc  => "see if user 1 can see owned group",
    path  => "/groups",
    params => { 'owned' => 'true' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{groups}} eq 1;
HttpHelper::fail("fail") unless $$json{groups}[0] eq $group1;

$json = HttpHelper::get(
    desc  => "check user 1 can't see group they are not a member of",
    path  => "/groups",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{groups}} eq 1;
HttpHelper::fail("fail") unless $$json{groups}[0] eq 'all_users';

$json = HttpHelper::get(
    desc  => "check user admin can see all groups",
    path  => "/groups",
    params => { 'all' => 'true' },
    sessionKey=>$sessionKey_userAdmin
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{groups}} eq 3;
HttpHelper::fail("fail") unless $$json{groups}[0] eq 'all_users';

$json = HttpHelper::get(
    desc  => "check non user admin can't see all groups",
    path  => "/groups",
    params => { 'all' => 'true' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

#============================================================================
# add users

$json = HttpHelper::put(
    desc  => "add users to group 1",
    path  => "/groups/$group1/users/add",
    params => { 'usernames' => "$username1,$username2" },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc  => "check user 2 has been added to group 1",
    path  => "/groups",
    params => {},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{groups}} eq 2;
HttpHelper::fail("fail") unless $$json{groups}[0] eq 'all_users';
HttpHelper::fail("fail") unless $$json{groups}[1] eq $group1;

$json = HttpHelper::put(
    desc  => "check user 1 can't add people to group 2",
    path  => "/groups/$group2/users/add",
    params => { 'usernames' => "$username1,$username2" },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::put(
    desc  => "add users to group that doesn't exist",
    path  => "/groups/notGroup/users/add",
    params => {  'usernames' => "$username1,$username2" },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

$json = HttpHelper::put(
    desc  => "add non existent users to group 2",
    path  => "/groups/$group2/users/add",
    params => {  'usernames' => "$username1,notUsername" },
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '400';

$json = HttpHelper::put(
    desc  => "add users to group 2",
    path  => "/groups/$group2/users/add",
    params => { 'usernames' => "$username1,$username2" },
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc  => "check user 1 is a member of group 1 and group 2",
    path  => "/groups",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{groups}} eq 3;

#============================================================================
# list users

$json = HttpHelper::get(
    desc  => "check user 2 and 2 are in group 2",
    path  => "/groups/$group2/users",
    params => {},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{users}[0]{username} eq $username1;
HttpHelper::fail("fail") unless $$json{users}[1]{username} eq $username2;

$json = HttpHelper::get(
    desc  => "check user 1 is in group 1",
    path  => "/groups/$group1/users",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{users}[0]{username} eq $username1;

$json = HttpHelper::get(
    desc  => "make sure admin is required to list users in group",
    path  => "/groups/$group1/users",
    params => {},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::get(
    desc  => "list users on non existent group",
    path  => "/groups/notGroup/users",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

#============================================================================
# toggle admin

$json = HttpHelper::put(
    desc  => "check user 2 can't add people to group 1",
    path  => "/groups/$group1/users/add",
    params => { 'usernames' => "$username1,$username2" },
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::put(
    desc  => "make user 2 admin for group 1",
    path  => "/groups/$group1/admin",
    params => { 'username' => "$username2", 'admin' => 'true' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put(
    desc  => "check user 2 can now add people to group 1",
    path  => "/groups/$group1/users/add",
    params => { 'usernames' => "$username1,$username2" },
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc  => "check user 2 is admin in group 1",
    path  => "/groups/$group1/users",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{users}[1]{username} eq $username2;
HttpHelper::fail("fail") unless $$json{users}[1]{admin};

$json = HttpHelper::put(
    desc  => "remove admin from user 2 for group 1",
    path  => "/groups/$group1/admin",
    params => { 'username' => "$username2", 'admin' => 'false' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put(
    desc  => "check user 2 can't add people to group 1",
    path  => "/groups/$group1/users/add",
    params => { 'usernames' => "$username1,$username2" },
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::get(
    desc  => "check user 2 is admin in group 1",
    path  => "/groups/$group1/users",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{users}[1]{username} eq $username2;
HttpHelper::fail("fail") unless !$$json{users}[1]{admin};

#============================================================================
# remove users

$json = HttpHelper::put(
    desc  => "remove user 2 from group 1",
    path  => "/groups/$group1/users/remove",
    params => { 'usernames' => "$username2" },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc  => "check user 2 has been removed from group 1",
    path  => "/groups",
    params => {},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{groups}} eq 2;
HttpHelper::fail("fail") unless $$json{groups}[0] eq 'all_users';
HttpHelper::fail("fail") unless $$json{groups}[1] eq $group2;

$json = HttpHelper::get(
    desc  => "check user 1 is still in group 1",
    path  => "/groups",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{groups}} eq 3;

$json = HttpHelper::get(
    desc  => "check user 1 is still in group 1",
    path  => "/groups/$group1/users",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{users}} eq 1;
HttpHelper::fail("fail") unless $$json{users}[0]{username} eq $username1;

#============================================================================
# update group name
$json = HttpHelper::put(
    desc  => "Update group name - not owner",
    path  => "/groups/$group1/update",
    params => { 'updatedName' => 'newName' },
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::put(
    desc  => "Update group name - group name already exists",
    path  => "/groups/$group1/update",
    params => { 'updatedName' => $group2 },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '409';

$json = HttpHelper::put(
    desc  => "Update group name",
    path  => "/groups/$group1/update",
    params => { 'updatedName' => 'newGroup1' },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get(
    desc  => "check group 1 has been successfully renamed",
    path  => "/groups/newGroup1/users",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{users}} eq 1;
HttpHelper::fail("fail") unless $$json{users}[0]{username} eq $username1;

#============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

#########################################

