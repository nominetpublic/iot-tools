# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;


my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";

(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

my $json;

# login
ThingzoneLogin::login_section('pullSubscriptions');
#============================================================================

$json = HttpHelper::get ("/pull_subscription/list", {});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# get with bogus dataStream (will fail)
$json = HttpHelper::get ("/pull_subscription/get", {dataStream=>"wot"});
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

# create a dataStream
$json = HttpHelper::post ("/datastream/create", {});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream=$$json{dataStream};


# get when not yet created
$json = HttpHelper::get ("/pull_subscription/get", {dataStream=>$dataStream});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# set pull subscription on dataStream
$json = HttpHelper::put_multipart ("/pull_subscription/set", HttpHelper::create_multiparts(
    {name=>'dataStream', value=>$dataStream},
    {name=>'uri', value=>'http://example.com'},
    {name=>'uriParams', content_type=>'application/json', value=>q#{"httpHeaders":{"SessionHeaderId": "1234"} }#},
    {name=>'interval', value=>42},
    ));
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# modify it
$json = HttpHelper::put_multipart ("/pull_subscription/set", HttpHelper::create_multiparts(
    {name=>'dataStream', value=>$dataStream},
    {name=>'uri', value=>'http://different-example.com'},
    {name=>'uriParams', content_type=>'application/json', value=>q#{"httpHeaders":{"SessionHeaderId": "1234"} }#},
    {name=>'interval', value=>42},
    ));
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# check it exists (& was modified)
$json = HttpHelper::get ("/pull_subscription/get", {dataStream=>$dataStream});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{pullSubscription}{uri} eq "http://different-example.com";

# remove
$json = HttpHelper::put ("/pull_subscription/delete", {dataStream=>$dataStream});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# check it's gone
$json = HttpHelper::get ("/pull_subscription/get", {dataStream=>$dataStream});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless !defined $$json{pullSubscription};;


$json = HttpHelper::get ("/pull_subscription/list", {detail=>'full'});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


#============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

