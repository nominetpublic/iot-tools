# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $PROGRAM=basename $0;

my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;


my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";
(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

# login
my $json;

my %login1 = ThingzoneLogin::login_section("dataStream");
my $sessionKey_user1 = $login1{sessionKey} or die;

$json = HttpHelper::get ("/datastream/list", {});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post ("/datastream/create", {});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream=$$json{dataStream};


$json = HttpHelper::get ("/datastream/list", {});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

die "newly created datastream not found"
    unless (grep { /$dataStream/ } @{$$json{dataStreams}}) == 1;

$json = HttpHelper::get ("/datastream/get", {"dataStream" => $dataStream});
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

#============================================================================
# create with name and type

$json = HttpHelper::post(desc  => "create datastream_name with entity name",
    path  => "/datastream/create",
    params => { "name"=>"key name"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $datastream_name=$$json{dataStream} or die;

$json = HttpHelper::get(
    desc  => "check that datastream_name has a name",
    path  => "/datastream/get",
    params=> {'dataStream' => $datastream_name, 'detail' => 'full'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream}{name} eq 'key name';

$json = HttpHelper::post(desc  => "create datastream_type with entity type",
    path  => "/datastream/create",
    params => { "type"=>"entity type"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $datastream_type=$$json{dataStream} or die;

$json = HttpHelper::get(
    desc  => "check that datastream_type has a type",
    path  => "/datastream/get",
    params=> {'dataStream' => $datastream_type},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream}{type} eq 'entity type';

$json = HttpHelper::post(desc  => "create datastream_name with entity name and type",
    path  => "/datastream/create",
    params => { "name"=>"datastream name", "type"=>"camera"},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $datastream_name_type=$$json{dataStream} or die;

$json = HttpHelper::get(
    desc  => "check that datastream_name_type has a name and a type",
    path  => "/datastream/get",
    params=> {'dataStream' => $datastream_name_type},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream}{name} eq 'datastream name';
HttpHelper::fail("fail") unless $$json{dataStream}{type} eq 'camera';

#============================================================================
# updating

$json = HttpHelper::put_multipart(
    desc  => "update datastream name",
    path  => "/datastream/update",
    parts => HttpHelper::create_multiparts(
        {name=>'dataStream', value=>$dataStream},
        {name=>'name', value=>"name1"},
        {name=>'type', value=>"type1"}),
    sessionKey=>$sessionKey_user1);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc  => "name has been updated",
    path  => "/datastream/get",
    params => {"dataStream" => $dataStream},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream}{name} eq "name1";
HttpHelper::fail("fail") unless $$json{dataStream}{type} eq "type1";

$json = HttpHelper::put_multipart ("/datastream/update", HttpHelper::create_multiparts(
    {name=>'dataStream', value=>$dataStream},
    {name=>'metadata', content_type=>'application/json', value=>q#{"stringVal":"forty two","intVal":42, "test":"something else"}#},
    ));
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc  => "name has been updated",
    path  => "/datastream/get",
    params => {"dataStream" => $dataStream},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{dataStream}{name} eq "name1";
HttpHelper::fail("fail") unless $$json{dataStream}{type} eq "type1";
HttpHelper::fail("fail") unless $$json{dataStream}{metadata}{stringVal} eq "forty two";

#============================================================================
# deletion

$json = HttpHelper::delete (
    desc  => "user1 deletes datastream",
    path  => "/datastream/delete",
    params => {dataStream=>$dataStream},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# should have gone
$json = HttpHelper::get (
    desc  => "deleted datastream no longer exists",
    path  => "/datastream/get",
    params => {dataStream=>$dataStream},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} =~ 'not found';
HttpHelper::fail("fail") unless $$json{status_code} eq '404';

#============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

