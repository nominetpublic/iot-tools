# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use File::Basename;
use lib '.';
use HttpHelper;
use ThingzoneLogin;

my $TRUE=1;
my $FALSE=0;

my $PROGRAM=basename $0;


my $server=shift @ARGV;
my $port=shift @ARGV;
$server='localhost' unless $server;
$port='8081' unless $port;

# connect
my $base_url="http://$server:$port/api";
print "connecting to $base_url...\n";
(my $TC_TEST_CLASS = basename($0)) =~ s/\.[^.]+$//;;
print "\n##teamcity[testSuiteStarted name='APIPerlTest.$TC_TEST_CLASS']\n";
HttpHelper::init($base_url);
HttpHelper::set_testClass("AIPerlTest.$TC_TEST_CLASS ");

my $json;
my $sessionKey_userAdmin=ThingzoneLogin::login_admin_user() or die;

# test user 1
my $unique = time();
my $username1 = "user_sod_perm_1_$unique";
my $password1 = "password_$unique";
my $username2 = "user_sod_perm_2_$unique";
my $password2 = "password_$unique";
my $group1 = "group_sod_1_$unique";

# create 2x users
$json = HttpHelper::post (
    desc  => "Create user 1 ($username1)",
    path  => '/user/create',
    params=> {username=>$username1, password=>$password1, privileges=>"CREATE_GROUP, CREATE_DEVICE, CREATE_DATASTREAM"},
    sessionKey=>$sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::post (
    desc  => "Create user 2 ($username2)",
    path  => '/user/create',
    params=> {username=>$username2, password=>$password2, privileges=>"CREATE_DEVICE, CREATE_DATASTREAM"},
    sessionKey=>$sessionKey_userAdmin,
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


# log in 2x users
$json = HttpHelper::post (
    desc  => "Log in as user 1 ($username1)",
    path  => '/session/login',
    params=> {username=>$username1, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user1=$$json{sessionKey};


$json = HttpHelper::post (
    desc  => "Log in as user 2 ($username2)",
    path  => '/session/login',
    params=> {username=>$username2, password=>$password1}
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $sessionKey_user2=$$json{sessionKey};

# create group
$json = HttpHelper::post(
    desc  => "create group 1",
    path  => "/groups/$group1",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put(
    desc  => "add users to group 1",
    path  => "/groups/$group1/users/add",
    params => { 'usernames' => "$username1,$username2" },
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# each user creates a device & datastream

$json = HttpHelper::post (
    desc  => "create device for user 1",
    path  => "/device/create",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $device1=$$json{device} or die;

$json = HttpHelper::post (
    desc  => "create device for user 2",
    path  => "/device/create",
    params => {},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $device2=$$json{device} or die;


$json = HttpHelper::post (
    desc  => "create datastream for user 1",
    path  => "/datastream/create",
    params => {},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream1=$$json{dataStream} or die;

$json = HttpHelper::post (
    desc  => "create datastream for user 2",
    path  => "/datastream/create",
    params => {},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
my $dataStream2=$$json{dataStream} or die;

# add streams to devices

$json = HttpHelper::put (
    desc  => "add stream 1 to device 1",
    path  => "/device/add_stream",
    params => {'device' => $device1, 'dataStream' => $dataStream1, 'name' => 'd1_s1'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc  => "add stream 2 to device 2",
    path  => "/device/add_stream",
    params => {'device' => $device2, 'dataStream' => $dataStream2, 'name' => 'd2_s2'},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

#============================================================================


$json = HttpHelper::get (
    desc  => "get own device",
    path  => "/device/get",
    params => {'device' => $device1},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc  => "can get full details on own device/stream",
    path  => "/device/get",
    params => {'device' => $device1, 'detail'=>'full'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") if $$json{device}{permissions};
HttpHelper::fail("fail") unless $$json{device}{dataStreams}[0]{key} eq $dataStream1;
HttpHelper::fail("fail") if $$json{device}{dataStreams}[0]{permissions};


$json = HttpHelper::get (
    desc  => "cannot get full details on other users device/stream",
    path  => "/device/get",
    params => {'device' => $device2, 'detail'=>'full'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} eq 'authentication error';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::put (
    desc  => "user 1 updates privileges to allow user 2, for user1's device",
    path  => "/permission/set",
    params => {'key'=>$device1, 'username'=>$username2, 'permissions'=>'CONSUME, DISCOVER'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::get (
    desc  => "user2 can now get details on other users device, but not stream",
    path  => "/device/get",
    params => {'device' => $device1, 'detail'=>'full'},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") if $$json{device}{dataStreams};


$json = HttpHelper::put (
    desc  => "user 1 updates privileges to allow user 2, for user1's stream",
    path  => "/permission/set",
    params => {'key'=>$dataStream1, 'username'=>$username2, 'permissions'=>'CONSUME, DISCOVER'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';


$json = HttpHelper::get (
    desc  => "user2 can now get details on other users device, AND also stream",
    path  => "/device/get",
    params => {'device' => $device1, 'detail'=>'full'},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless @{$$json{device}{dataStreams}} == 1;
HttpHelper::fail("fail") unless $$json{device}{dataStreams}[0]{key} eq $dataStream1;


$json = HttpHelper::put (
    desc  => "user1 cannot add user2's stream to his device",
    path  => "/device/add_stream",
    params => {'device' => $device1, 'dataStream' => $dataStream2, 'name' => 'xxx'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'fail';
HttpHelper::fail("fail") unless $$json{reason} eq 'authentication error';
HttpHelper::fail("fail") unless $$json{status_code} eq '403';

$json = HttpHelper::put (
    desc  => "user2 can add user1's stream to his device",
    path  => "/device/add_stream",
    params => {'device' => $device2, 'dataStream' => $dataStream1, 'name' => 'xxx'},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::put (
    desc  => "user 1 sets permissions on device 1 to group 1",
    path  => "/permission/set",
    params => {'key'=>$device1, 'group'=>$group1, 'permissions'=>'CONSUME, DISCOVER'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

# Test device/get with permissions
$json = HttpHelper::get (
    desc  => "can get full details with user permissions on own device/stream",
    path  => "/device/get",
    params => {'device' => $device1, 'detail'=>'full', 'showPermissions'=>'true'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{device}{dataStreams}[0]{key} eq $dataStream1;
HttpHelper::fail("fail") unless $$json{device}{userPermissions};
HttpHelper::fail("fail") unless $$json{device}{groupPermissions};
HttpHelper::fail("fail") unless @{$$json{device}{groupPermissions}{$group1}} == 2;
HttpHelper::fail("fail") unless $$json{device}{dataStreams}[0]{userPermissions};


$json = HttpHelper::put (
    desc  => "user 1 updates privileges to allow user 2, for user1's stream",
    path  => "/permission/set",
    params => {'key'=>$dataStream1, 'username'=>$username2, 'permissions'=>'ADMIN, MODIFY, CONSUME, DISCOVER'},
    sessionKey=>$sessionKey_user1
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';

$json = HttpHelper::get (
    desc  => "check that users can see permissions data on streams they have ADMIN access to",
    path  => "/device/get",
    params => {'device' => $device1, 'detail'=>'full', 'showPermissions'=>'true'},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{device}{dataStreams}[0]{key} eq $dataStream1;
HttpHelper::fail("fail") if $$json{device}{userPermissions};
HttpHelper::fail("fail") unless $$json{device}{dataStreams}[0]{userPermissions};
HttpHelper::fail("fail") unless keys %{$$json{device}{dataStreams}[0]{userPermissions}} == 2;

$json = HttpHelper::get (
    desc  => "check that users can see permissions data on streams they have ADMIN access to",
    path  => "/device/get",
    params => {'device' => $device1, 'detail'=>'full', 'showPermissions'=>'true'},
    sessionKey=>$sessionKey_user2
);
HttpHelper::fail("fail") unless $$json{result} eq 'ok';
HttpHelper::fail("fail") unless $$json{device}{dataStreams}[0]{key} eq $dataStream1;
HttpHelper::fail("fail") if $$json{device}{userPermissions};
HttpHelper::fail("fail") unless $$json{device}{dataStreams}[0]{userPermissions};
HttpHelper::fail("fail") unless keys %{$$json{device}{dataStreams}[0]{userPermissions}} == 2;


#=============================================================================
print "\n##teamcity[testSuiteFinished name='APIPerlTest.$TC_TEST_CLASS']\n";
exit 0;

