#!/bin/bash
# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


# this is intended to be a thorough deletion script:
# - drops & recreates everything necessary for e2e testing

# drop all non-production databases & users
psql -a -U postgres -c "DROP DATABASE IF EXISTS e2e_registry;" || exit 1
psql -a -U postgres -c "DROP DATABASE IF EXISTS e2e_bridge;" || exit 1

psql -a -U postgres -c "DROP DATABASE IF EXISTS e2e_registry_rw;" || exit 1
psql -a -U postgres -c "DROP ROLE IF EXISTS e2e_registry_rw;" || exit 1

psql -a -U postgres -c "DROP DATABASE IF EXISTS e2e_bridge_ro;" || exit 1
psql -a -U postgres -c "DROP ROLE IF EXISTS e2e_bridge_ro;" || exit 1

# create Registry user
psql -a -U postgres -c "CREATE USER e2e_registry_rw WITH LOGIN PASSWORD 'e2e_registry_rwx';" || exit 1

# create DBs (separate for bridge vs registry e2e tests)
psql -a -U postgres -c "CREATE DATABASE e2e_registry OWNER = e2e_registry_rw;" || exit 1
psql -a -U postgres -c "CREATE DATABASE e2e_bridge OWNER = e2e_registry_rw;" || exit 1

# create thingzone schema in both DBs
#psql -a -d e2e_registry -U e2e_registry_rw -c "CREATE SCHEMA thingzone;" || exit 1
#psql -a -d e2e_bridge -U e2e_registry_rw -c "CREATE SCHEMA thingzone;" || exit 1

# create Bridge user
psql -a -U postgres -c "CREATE USER e2e_bridge_ro WITH LOGIN PASSWORD 'e2e_bridge_rox';" || exit 1

# create ENUM types (which don't get in the normal DDL)
#if [ -x create_types.sh ]; then
#	./create_types.sh || exit 1
#else 
#	if [ -x ../registry/db/create_types.sh ]; then
#		(cd ../registry/db && ./create_types.sh) || exit 1
#	fi
#fi

