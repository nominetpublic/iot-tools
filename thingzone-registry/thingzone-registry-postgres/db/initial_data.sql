--
-- PostgreSQL database dump
--

-- Dumped from database version xxx
-- Dumped by pg_dump version xxx

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: users; Type: TABLE DATA; Schema: thingzone; Owner: e2e_registry_rw
--

COPY thingzone.users (id, preferences, details) FROM stdin;
1	{}	{}
2	{}	{}
\.


--
-- Data for Name: groups; Type: TABLE DATA; Schema: thingzone; Owner: e2e_registry_rw
--

COPY thingzone.groups (id, name, owner_id) FROM stdin;
1	all_users	2
\.


--
-- Data for Name: accounts; Type: TABLE DATA; Schema: thingzone; Owner: e2e_registry_rw
--

COPY thingzone.accounts (id, owner_id, group_id) FROM stdin;
1	1	\N
2	2	\N
3	\N	1
\.


--
-- Data for Name: dnskeys; Type: TABLE DATA; Schema: thingzone; Owner: e2e_registry_rw
--

COPY thingzone.dnskeys (dnskey, entity_type, name, status) FROM stdin;
\.


--
-- Data for Name: acls; Type: TABLE DATA; Schema: thingzone; Owner: e2e_registry_rw
--

COPY thingzone.acls (id, key, account_id, permission) FROM stdin;
\.


--
-- Data for Name: datastreams; Type: TABLE DATA; Schema: thingzone; Owner: e2e_registry_rw
--

COPY thingzone.datastreams (dnskey, metadata, istransform) FROM stdin;
\.


--
-- Data for Name: devices; Type: TABLE DATA; Schema: thingzone; Owner: e2e_registry_rw
--

COPY thingzone.devices (dnskey, metadata, location) FROM stdin;
\.


--
-- Data for Name: dnsdata; Type: TABLE DATA; Schema: thingzone; Owner: e2e_registry_rw
--

COPY thingzone.dnsdata (id, dnskey, description, subdomain, ttl, rrtype, rdata) FROM stdin;
\.


--
-- Data for Name: groups_users; Type: TABLE DATA; Schema: thingzone; Owner: e2e_registry_rw
--

COPY thingzone.groups_users (group_id, user_id, admin) FROM stdin;
\.


--
-- Data for Name: history; Type: TABLE DATA; Schema: thingzone; Owner: e2e_registry_rw
--

COPY thingzone.history (id, key, object_type, action, account_id, updated) FROM stdin;
\.


--
-- Data for Name: pullsubscriptions; Type: TABLE DATA; Schema: thingzone; Owner: e2e_registry_rw
--

COPY thingzone.pullsubscriptions (id, streamkey, uri, uri_params, "interval", aligned, last_updated) FROM stdin;
\.


--
-- Data for Name: pushsubscribers; Type: TABLE DATA; Schema: thingzone; Owner: e2e_registry_rw
--

COPY thingzone.pushsubscribers (id, streamkey, uri, uri_params, headers, method, retry_sequence) FROM stdin;
\.


--
-- Data for Name: registry_properties; Type: TABLE DATA; Schema: thingzone; Owner: e2e_registry_rw
--

COPY thingzone.registry_properties (id, property, last_updated, value) FROM stdin;
1	db schema created	\N	Wed Mar 11 10:30:09 GMT 2020
2	db schema created by	\N	schema creator
\.


--
-- Data for Name: resources; Type: TABLE DATA; Schema: thingzone; Owner: e2e_registry_rw
--

COPY thingzone.resources (name, key, content_type, content, type) FROM stdin;
\.


--
-- Data for Name: sessions; Type: TABLE DATA; Schema: thingzone; Owner: e2e_registry_rw
--

COPY thingzone.sessions (sessionkey, username, ipaddr, created, refreshed, closed) FROM stdin;
\.


--
-- Data for Name: streamsondevices; Type: TABLE DATA; Schema: thingzone; Owner: e2e_registry_rw
--

COPY thingzone.streamsondevices (id, datastreamkey, devicekey, name) FROM stdin;
\.


--
-- Data for Name: transformfunctions; Type: TABLE DATA; Schema: thingzone; Owner: e2e_registry_rw
--

COPY thingzone.transformfunctions (id, name, functioncontent, owner_id, functiontype) FROM stdin;
\.


--
-- Data for Name: transforms; Type: TABLE DATA; Schema: thingzone; Owner: e2e_registry_rw
--

COPY thingzone.transforms (id, outputkey, functionid, parameter_values, multiplexer, runschedule) FROM stdin;
\.


--
-- Data for Name: transform_outputstreams; Type: TABLE DATA; Schema: thingzone; Owner: e2e_registry_rw
--

COPY thingzone.transform_outputstreams (transform_id, alias, datastream_id) FROM stdin;
\.


--
-- Data for Name: transformsources; Type: TABLE DATA; Schema: thingzone; Owner: e2e_registry_rw
--

COPY thingzone.transformsources (id, transformid, sourcestream, trigger, alias, aggregationtype, aggregationspec) FROM stdin;
\.


--
-- Data for Name: user_credentials; Type: TABLE DATA; Schema: thingzone; Owner: e2e_registry_rw
--

COPY thingzone.user_credentials (id, username, pw_hash, user_id, email, email_validated) FROM stdin;
1	public		1		f
2	user_admin		2		f
\.


--
-- Data for Name: user_tokens; Type: TABLE DATA; Schema: thingzone; Owner: e2e_registry_rw
--

COPY thingzone.user_tokens (token, user_credentials_id, token_type, created, expiry, used) FROM stdin;
\.


--
-- Data for Name: userprivileges; Type: TABLE DATA; Schema: thingzone; Owner: e2e_registry_rw
--

COPY thingzone.userprivileges (id, privilege, user_id) FROM stdin;
1	USER_ADMIN	2
\.


--
-- Data for Name: zonefilechanges; Type: TABLE DATA; Schema: thingzone; Owner: e2e_registry_rw
--

COPY thingzone.zonefilechanges (id, transaction, operation, rrname, ttl, rrtype, rdata, "timestamp", serial) FROM stdin;
\.


--
-- Data for Name: zonefilechangestransactions; Type: TABLE DATA; Schema: thingzone; Owner: e2e_registry_rw
--

COPY thingzone.zonefilechangestransactions (id) FROM stdin;
\.


--
-- Name: accounts_id_seq; Type: SEQUENCE SET; Schema: thingzone; Owner: e2e_registry_rw
--

SELECT pg_catalog.setval('thingzone.accounts_id_seq', 3, true);


--
-- Name: acls_id_seq; Type: SEQUENCE SET; Schema: thingzone; Owner: e2e_registry_rw
--

SELECT pg_catalog.setval('thingzone.acls_id_seq', 1, false);


--
-- Name: dnsdata_id_seq; Type: SEQUENCE SET; Schema: thingzone; Owner: e2e_registry_rw
--

SELECT pg_catalog.setval('thingzone.dnsdata_id_seq', 1, false);


--
-- Name: groups_id_seq; Type: SEQUENCE SET; Schema: thingzone; Owner: e2e_registry_rw
--

SELECT pg_catalog.setval('thingzone.groups_id_seq', 1, true);


--
-- Name: history_id_seq; Type: SEQUENCE SET; Schema: thingzone; Owner: e2e_registry_rw
--

SELECT pg_catalog.setval('thingzone.history_id_seq', 1, false);


--
-- Name: pullsubscriptions_id_seq; Type: SEQUENCE SET; Schema: thingzone; Owner: e2e_registry_rw
--

SELECT pg_catalog.setval('thingzone.pullsubscriptions_id_seq', 1, false);


--
-- Name: pushsubscribers_id_seq; Type: SEQUENCE SET; Schema: thingzone; Owner: e2e_registry_rw
--

SELECT pg_catalog.setval('thingzone.pushsubscribers_id_seq', 1, false);


--
-- Name: registry_properties_id_seq; Type: SEQUENCE SET; Schema: thingzone; Owner: e2e_registry_rw
--

SELECT pg_catalog.setval('thingzone.registry_properties_id_seq', 2, true);


--
-- Name: streamsondevices_id_seq; Type: SEQUENCE SET; Schema: thingzone; Owner: e2e_registry_rw
--

SELECT pg_catalog.setval('thingzone.streamsondevices_id_seq', 1, false);


--
-- Name: transformfunctions_id_seq; Type: SEQUENCE SET; Schema: thingzone; Owner: e2e_registry_rw
--

SELECT pg_catalog.setval('thingzone.transformfunctions_id_seq', 1, false);


--
-- Name: transforms_id_seq; Type: SEQUENCE SET; Schema: thingzone; Owner: e2e_registry_rw
--

SELECT pg_catalog.setval('thingzone.transforms_id_seq', 1, false);


--
-- Name: transformsources_id_seq; Type: SEQUENCE SET; Schema: thingzone; Owner: e2e_registry_rw
--

SELECT pg_catalog.setval('thingzone.transformsources_id_seq', 1, false);


--
-- Name: user_credentials_id_seq; Type: SEQUENCE SET; Schema: thingzone; Owner: e2e_registry_rw
--

SELECT pg_catalog.setval('thingzone.user_credentials_id_seq', 2, true);


--
-- Name: userprivileges_id_seq; Type: SEQUENCE SET; Schema: thingzone; Owner: e2e_registry_rw
--

SELECT pg_catalog.setval('thingzone.userprivileges_id_seq', 1, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: thingzone; Owner: e2e_registry_rw
--

SELECT pg_catalog.setval('thingzone.users_id_seq', 2, true);


--
-- Name: zonefilechanges_id_seq; Type: SEQUENCE SET; Schema: thingzone; Owner: e2e_registry_rw
--

SELECT pg_catalog.setval('thingzone.zonefilechanges_id_seq', 1, false);


--
-- Name: zonefilechangestransactions_id_seq; Type: SEQUENCE SET; Schema: thingzone; Owner: e2e_registry_rw
--

SELECT pg_catalog.setval('thingzone.zonefilechangestransactions_id_seq', 1, false);


--
-- PostgreSQL database dump complete
--

