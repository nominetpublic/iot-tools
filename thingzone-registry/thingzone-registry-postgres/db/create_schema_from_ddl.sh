#!/bin/bash
# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


psql -a -d e2e_registry -U e2e_registry_rw -c "DROP SCHEMA IF EXISTS thingzone CASCADE;" || exit 1

psql -a -d e2e_registry -U postgres -a --set ON_ERROR_STOP=on -f ddl_full_create.sql || exit 1

psql -a -d e2e_registry -U e2e_registry_rw -a --set ON_ERROR_STOP=on -f initial_data.sql || exit 1



