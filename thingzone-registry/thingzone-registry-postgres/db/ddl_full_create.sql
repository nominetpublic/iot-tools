--
-- PostgreSQL database dump
--

-- Dumped from database version xxx
-- Dumped by pg_dump version xxx

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: thingzone; Type: SCHEMA; Schema: -; Owner: e2e_registry_rw
--

CREATE SCHEMA thingzone;


ALTER SCHEMA thingzone OWNER TO e2e_registry_rw;

--
-- Name: entity_status_type; Type: TYPE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TYPE thingzone.entity_status_type AS ENUM (
    'KEY',
    'DEVICE',
    'DATASTREAM',
    'DELETED'
);


ALTER TYPE thingzone.entity_status_type OWNER TO e2e_registry_rw;

--
-- Name: history_action_type; Type: TYPE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TYPE thingzone.history_action_type AS ENUM (
    'CREATED',
    'MODIFIED',
    'DELETED',
    'PERMISSION_CREATED',
    'PERMISSION_REMOVED',
    'RELATIONSHIP'
);


ALTER TYPE thingzone.history_action_type OWNER TO e2e_registry_rw;

--
-- Name: object_type; Type: TYPE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TYPE thingzone.object_type AS ENUM (
    'ENTITY',
    'DEVICE',
    'DATASTREAM',
    'TRANSFORM'
);


ALTER TYPE thingzone.object_type OWNER TO e2e_registry_rw;

--
-- Name: permission_type; Type: TYPE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TYPE thingzone.permission_type AS ENUM (
    'ADMIN',
    'UPLOAD',
    'CONSUME',
    'MODIFY',
    'ANNOTATE',
    'DISCOVER',
    'UPLOAD_EVENTS',
    'UPLOAD_ALERTS',
    'CONSUME_EVENTS',
    'CONSUME_ALERTS',
    'VIEW_LOCATION',
    'VIEW_METADATA'
);


ALTER TYPE thingzone.permission_type OWNER TO e2e_registry_rw;

--
-- Name: privilege_type; Type: TYPE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TYPE thingzone.privilege_type AS ENUM (
    'CREATE_USER',
    'CREATE_GROUP',
    'CREATE_DEVICE',
    'CREATE_DATASTREAM',
    'CREATE_TRANSFORM',
    'CREATE_TRANSFORMFUNCTION',
    'CREATE_ENTITY',
    'CREATE_RESOURCE',
    'GRANT_CREATE_USER',
    'GRANT_CREATE_GROUP',
    'GRANT_CREATE_DEVICE',
    'GRANT_CREATE_DATASTREAM',
    'GRANT_CREATE_TRANSFORM',
    'GRANT_CREATE_TRANSFORMFUNCTION',
    'GRANT_CREATE_ENTITY',
    'GRANT_CREATE_RESOURCE',
    'GRANT_GRANTS',
    'DUMP_ALL',
    'USE_SESSION',
    'USER_ADMIN',
    'CREATE_DETECTOR',
    'RUN_DETECTOR',
    'GRANT_CREATE_DETECTOR',
    'GRANT_RUN_DETECTOR',
    'QUERY_STREAMS',
    'CREATE_PUBLIC_TRANSFORMFUNCTION',
    'GRANT_CREATE_PUBLIC_TRANSFORMFUNCTION'
);


ALTER TYPE thingzone.privilege_type OWNER TO e2e_registry_rw;

--
-- Name: user_token_type; Type: TYPE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TYPE thingzone.user_token_type AS ENUM (
    'PASSWORD',
    'EMAIL'
);


ALTER TYPE thingzone.user_token_type OWNER TO e2e_registry_rw;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: accounts; Type: TABLE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TABLE thingzone.accounts (
    id integer NOT NULL,
    owner_id bigint,
    group_id bigint
);


ALTER TABLE thingzone.accounts OWNER TO e2e_registry_rw;

--
-- Name: accounts_id_seq; Type: SEQUENCE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE SEQUENCE thingzone.accounts_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE thingzone.accounts_id_seq OWNER TO e2e_registry_rw;

--
-- Name: accounts_id_seq; Type: SEQUENCE OWNED BY; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER SEQUENCE thingzone.accounts_id_seq OWNED BY thingzone.accounts.id;


--
-- Name: acls; Type: TABLE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TABLE thingzone.acls (
    id integer NOT NULL,
    key character varying(43) NOT NULL,
    account_id bigint NOT NULL,
    permission thingzone.permission_type
);


ALTER TABLE thingzone.acls OWNER TO e2e_registry_rw;

--
-- Name: acls_id_seq; Type: SEQUENCE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE SEQUENCE thingzone.acls_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE thingzone.acls_id_seq OWNER TO e2e_registry_rw;

--
-- Name: acls_id_seq; Type: SEQUENCE OWNED BY; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER SEQUENCE thingzone.acls_id_seq OWNED BY thingzone.acls.id;


--
-- Name: datastreams; Type: TABLE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TABLE thingzone.datastreams (
    dnskey character varying(43) NOT NULL,
    metadata jsonb,
    istransform boolean NOT NULL
);


ALTER TABLE thingzone.datastreams OWNER TO e2e_registry_rw;

--
-- Name: devices; Type: TABLE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TABLE thingzone.devices (
    dnskey character varying(43) NOT NULL,
    metadata jsonb,
    location jsonb
);


ALTER TABLE thingzone.devices OWNER TO e2e_registry_rw;

--
-- Name: dnsdata; Type: TABLE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TABLE thingzone.dnsdata (
    id integer NOT NULL,
    dnskey character varying(43) NOT NULL,
    description text NOT NULL,
    subdomain character varying(255) NOT NULL,
    ttl integer NOT NULL,
    rrtype character varying(255) NOT NULL,
    rdata text NOT NULL
);


ALTER TABLE thingzone.dnsdata OWNER TO e2e_registry_rw;

--
-- Name: dnsdata_id_seq; Type: SEQUENCE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE SEQUENCE thingzone.dnsdata_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE thingzone.dnsdata_id_seq OWNER TO e2e_registry_rw;

--
-- Name: dnsdata_id_seq; Type: SEQUENCE OWNED BY; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER SEQUENCE thingzone.dnsdata_id_seq OWNED BY thingzone.dnsdata.id;


--
-- Name: dnskeys; Type: TABLE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TABLE thingzone.dnskeys (
    dnskey character varying(43) NOT NULL,
    entity_type character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    status thingzone.entity_status_type
);


ALTER TABLE thingzone.dnskeys OWNER TO e2e_registry_rw;

--
-- Name: groups; Type: TABLE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TABLE thingzone.groups (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    owner_id bigint NOT NULL
);


ALTER TABLE thingzone.groups OWNER TO e2e_registry_rw;

--
-- Name: groups_id_seq; Type: SEQUENCE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE SEQUENCE thingzone.groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE thingzone.groups_id_seq OWNER TO e2e_registry_rw;

--
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER SEQUENCE thingzone.groups_id_seq OWNED BY thingzone.groups.id;


--
-- Name: groups_users; Type: TABLE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TABLE thingzone.groups_users (
    group_id bigint NOT NULL,
    user_id bigint NOT NULL,
    admin boolean NOT NULL
);


ALTER TABLE thingzone.groups_users OWNER TO e2e_registry_rw;

--
-- Name: history; Type: TABLE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TABLE thingzone.history (
    id integer NOT NULL,
    key character varying(43) NOT NULL,
    object_type thingzone.object_type,
    action thingzone.history_action_type,
    account_id integer,
    updated timestamp without time zone NOT NULL
);


ALTER TABLE thingzone.history OWNER TO e2e_registry_rw;

--
-- Name: history_id_seq; Type: SEQUENCE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE SEQUENCE thingzone.history_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE thingzone.history_id_seq OWNER TO e2e_registry_rw;

--
-- Name: history_id_seq; Type: SEQUENCE OWNED BY; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER SEQUENCE thingzone.history_id_seq OWNED BY thingzone.history.id;


--
-- Name: pullsubscriptions; Type: TABLE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TABLE thingzone.pullsubscriptions (
    id integer NOT NULL,
    streamkey character varying(43) NOT NULL,
    uri character varying(255) NOT NULL,
    uri_params jsonb NOT NULL,
    "interval" integer NOT NULL,
    aligned integer,
    last_updated timestamp without time zone NOT NULL
);


ALTER TABLE thingzone.pullsubscriptions OWNER TO e2e_registry_rw;

--
-- Name: pullsubscriptions_id_seq; Type: SEQUENCE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE SEQUENCE thingzone.pullsubscriptions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE thingzone.pullsubscriptions_id_seq OWNER TO e2e_registry_rw;

--
-- Name: pullsubscriptions_id_seq; Type: SEQUENCE OWNED BY; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER SEQUENCE thingzone.pullsubscriptions_id_seq OWNED BY thingzone.pullsubscriptions.id;


--
-- Name: pushsubscribers; Type: TABLE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TABLE thingzone.pushsubscribers (
    id integer NOT NULL,
    streamkey character varying(43) NOT NULL,
    uri character varying(255) NOT NULL,
    uri_params jsonb NOT NULL,
    headers jsonb,
    method character varying(255),
    retry_sequence character varying(255)
);


ALTER TABLE thingzone.pushsubscribers OWNER TO e2e_registry_rw;

--
-- Name: pushsubscribers_id_seq; Type: SEQUENCE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE SEQUENCE thingzone.pushsubscribers_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE thingzone.pushsubscribers_id_seq OWNER TO e2e_registry_rw;

--
-- Name: pushsubscribers_id_seq; Type: SEQUENCE OWNED BY; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER SEQUENCE thingzone.pushsubscribers_id_seq OWNED BY thingzone.pushsubscribers.id;


--
-- Name: registry_properties; Type: TABLE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TABLE thingzone.registry_properties (
    id integer NOT NULL,
    property character varying(255) NOT NULL,
    last_updated timestamp without time zone,
    value character varying(255)
);


ALTER TABLE thingzone.registry_properties OWNER TO e2e_registry_rw;

--
-- Name: registry_properties_id_seq; Type: SEQUENCE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE SEQUENCE thingzone.registry_properties_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE thingzone.registry_properties_id_seq OWNER TO e2e_registry_rw;

--
-- Name: registry_properties_id_seq; Type: SEQUENCE OWNED BY; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER SEQUENCE thingzone.registry_properties_id_seq OWNED BY thingzone.registry_properties.id;


--
-- Name: resources; Type: TABLE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TABLE thingzone.resources (
    name character varying(255) NOT NULL,
    key character varying(43) NOT NULL,
    content_type character varying(255) NOT NULL,
    content bytea NOT NULL,
    type character varying(255)
);


ALTER TABLE thingzone.resources OWNER TO e2e_registry_rw;

--
-- Name: sessions; Type: TABLE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TABLE thingzone.sessions (
    sessionkey character varying(255) NOT NULL,
    username character varying(255) NOT NULL,
    ipaddr character varying(255) NOT NULL,
    created timestamp without time zone NOT NULL,
    refreshed timestamp without time zone NOT NULL,
    closed boolean NOT NULL
);


ALTER TABLE thingzone.sessions OWNER TO e2e_registry_rw;

--
-- Name: streamsondevices; Type: TABLE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TABLE thingzone.streamsondevices (
    id integer NOT NULL,
    datastreamkey character varying(43) NOT NULL,
    devicekey character varying(43) NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE thingzone.streamsondevices OWNER TO e2e_registry_rw;

--
-- Name: streamsondevices_id_seq; Type: SEQUENCE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE SEQUENCE thingzone.streamsondevices_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE thingzone.streamsondevices_id_seq OWNER TO e2e_registry_rw;

--
-- Name: streamsondevices_id_seq; Type: SEQUENCE OWNED BY; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER SEQUENCE thingzone.streamsondevices_id_seq OWNED BY thingzone.streamsondevices.id;


--
-- Name: transform_outputstreams; Type: TABLE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TABLE thingzone.transform_outputstreams (
    transform_id bigint NOT NULL,
    alias character varying(255) NOT NULL,
    datastream_id character varying(43) NOT NULL
);


ALTER TABLE thingzone.transform_outputstreams OWNER TO e2e_registry_rw;

--
-- Name: transformfunctions; Type: TABLE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TABLE thingzone.transformfunctions (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    functioncontent text NOT NULL,
    owner_id bigint NOT NULL,
    functiontype character varying(255) NOT NULL
);


ALTER TABLE thingzone.transformfunctions OWNER TO e2e_registry_rw;

--
-- Name: transformfunctions_id_seq; Type: SEQUENCE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE SEQUENCE thingzone.transformfunctions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE thingzone.transformfunctions_id_seq OWNER TO e2e_registry_rw;

--
-- Name: transformfunctions_id_seq; Type: SEQUENCE OWNED BY; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER SEQUENCE thingzone.transformfunctions_id_seq OWNED BY thingzone.transformfunctions.id;


--
-- Name: transforms; Type: TABLE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TABLE thingzone.transforms (
    id integer NOT NULL,
    outputkey character varying(43) NOT NULL,
    functionid bigint NOT NULL,
    parameter_values jsonb NOT NULL,
    multiplexer boolean DEFAULT false NOT NULL,
    runschedule character varying(255) DEFAULT ''::character varying
);


ALTER TABLE thingzone.transforms OWNER TO e2e_registry_rw;

--
-- Name: transforms_id_seq; Type: SEQUENCE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE SEQUENCE thingzone.transforms_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE thingzone.transforms_id_seq OWNER TO e2e_registry_rw;

--
-- Name: transforms_id_seq; Type: SEQUENCE OWNED BY; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER SEQUENCE thingzone.transforms_id_seq OWNED BY thingzone.transforms.id;


--
-- Name: transformsources; Type: TABLE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TABLE thingzone.transformsources (
    id integer NOT NULL,
    transformid bigint NOT NULL,
    sourcestream character varying(43) NOT NULL,
    trigger boolean NOT NULL,
    alias character varying(255) NOT NULL,
    aggregationtype character varying(255),
    aggregationspec character varying(1023)
);


ALTER TABLE thingzone.transformsources OWNER TO e2e_registry_rw;

--
-- Name: transformsources_id_seq; Type: SEQUENCE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE SEQUENCE thingzone.transformsources_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE thingzone.transformsources_id_seq OWNER TO e2e_registry_rw;

--
-- Name: transformsources_id_seq; Type: SEQUENCE OWNED BY; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER SEQUENCE thingzone.transformsources_id_seq OWNED BY thingzone.transformsources.id;


--
-- Name: user_credentials; Type: TABLE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TABLE thingzone.user_credentials (
    id integer NOT NULL,
    username character varying(255) NOT NULL,
    pw_hash character varying(255) NOT NULL,
    user_id bigint NOT NULL,
    email character varying(255) DEFAULT ''::character varying NOT NULL,
    email_validated boolean DEFAULT false NOT NULL
);


ALTER TABLE thingzone.user_credentials OWNER TO e2e_registry_rw;

--
-- Name: user_credentials_id_seq; Type: SEQUENCE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE SEQUENCE thingzone.user_credentials_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE thingzone.user_credentials_id_seq OWNER TO e2e_registry_rw;

--
-- Name: user_credentials_id_seq; Type: SEQUENCE OWNED BY; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER SEQUENCE thingzone.user_credentials_id_seq OWNED BY thingzone.user_credentials.id;


--
-- Name: user_tokens; Type: TABLE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TABLE thingzone.user_tokens (
    token character varying(64) NOT NULL,
    user_credentials_id bigint NOT NULL,
    token_type thingzone.user_token_type,
    created timestamp without time zone NOT NULL,
    expiry timestamp without time zone NOT NULL,
    used timestamp without time zone
);


ALTER TABLE thingzone.user_tokens OWNER TO e2e_registry_rw;

--
-- Name: userprivileges; Type: TABLE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TABLE thingzone.userprivileges (
    id integer NOT NULL,
    privilege thingzone.privilege_type,
    user_id bigint NOT NULL
);


ALTER TABLE thingzone.userprivileges OWNER TO e2e_registry_rw;

--
-- Name: userprivileges_id_seq; Type: SEQUENCE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE SEQUENCE thingzone.userprivileges_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE thingzone.userprivileges_id_seq OWNER TO e2e_registry_rw;

--
-- Name: userprivileges_id_seq; Type: SEQUENCE OWNED BY; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER SEQUENCE thingzone.userprivileges_id_seq OWNED BY thingzone.userprivileges.id;


--
-- Name: users; Type: TABLE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TABLE thingzone.users (
    id integer NOT NULL,
    preferences jsonb,
    details jsonb
);


ALTER TABLE thingzone.users OWNER TO e2e_registry_rw;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE SEQUENCE thingzone.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE thingzone.users_id_seq OWNER TO e2e_registry_rw;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER SEQUENCE thingzone.users_id_seq OWNED BY thingzone.users.id;


--
-- Name: zonefilechanges; Type: TABLE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TABLE thingzone.zonefilechanges (
    id integer NOT NULL,
    transaction bigint NOT NULL,
    operation integer NOT NULL,
    rrname text NOT NULL,
    ttl integer NOT NULL,
    rrtype character varying(255) NOT NULL,
    rdata text NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    serial bigint
);


ALTER TABLE thingzone.zonefilechanges OWNER TO e2e_registry_rw;

--
-- Name: zonefilechanges_id_seq; Type: SEQUENCE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE SEQUENCE thingzone.zonefilechanges_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE thingzone.zonefilechanges_id_seq OWNER TO e2e_registry_rw;

--
-- Name: zonefilechanges_id_seq; Type: SEQUENCE OWNED BY; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER SEQUENCE thingzone.zonefilechanges_id_seq OWNED BY thingzone.zonefilechanges.id;


--
-- Name: zonefilechangestransactions; Type: TABLE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE TABLE thingzone.zonefilechangestransactions (
    id integer NOT NULL
);


ALTER TABLE thingzone.zonefilechangestransactions OWNER TO e2e_registry_rw;

--
-- Name: zonefilechangestransactions_id_seq; Type: SEQUENCE; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE SEQUENCE thingzone.zonefilechangestransactions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE thingzone.zonefilechangestransactions_id_seq OWNER TO e2e_registry_rw;

--
-- Name: zonefilechangestransactions_id_seq; Type: SEQUENCE OWNED BY; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER SEQUENCE thingzone.zonefilechangestransactions_id_seq OWNED BY thingzone.zonefilechangestransactions.id;


--
-- Name: accounts id; Type: DEFAULT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.accounts ALTER COLUMN id SET DEFAULT nextval('thingzone.accounts_id_seq'::regclass);


--
-- Name: acls id; Type: DEFAULT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.acls ALTER COLUMN id SET DEFAULT nextval('thingzone.acls_id_seq'::regclass);


--
-- Name: dnsdata id; Type: DEFAULT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.dnsdata ALTER COLUMN id SET DEFAULT nextval('thingzone.dnsdata_id_seq'::regclass);


--
-- Name: groups id; Type: DEFAULT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.groups ALTER COLUMN id SET DEFAULT nextval('thingzone.groups_id_seq'::regclass);


--
-- Name: history id; Type: DEFAULT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.history ALTER COLUMN id SET DEFAULT nextval('thingzone.history_id_seq'::regclass);


--
-- Name: pullsubscriptions id; Type: DEFAULT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.pullsubscriptions ALTER COLUMN id SET DEFAULT nextval('thingzone.pullsubscriptions_id_seq'::regclass);


--
-- Name: pushsubscribers id; Type: DEFAULT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.pushsubscribers ALTER COLUMN id SET DEFAULT nextval('thingzone.pushsubscribers_id_seq'::regclass);


--
-- Name: registry_properties id; Type: DEFAULT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.registry_properties ALTER COLUMN id SET DEFAULT nextval('thingzone.registry_properties_id_seq'::regclass);


--
-- Name: streamsondevices id; Type: DEFAULT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.streamsondevices ALTER COLUMN id SET DEFAULT nextval('thingzone.streamsondevices_id_seq'::regclass);


--
-- Name: transformfunctions id; Type: DEFAULT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.transformfunctions ALTER COLUMN id SET DEFAULT nextval('thingzone.transformfunctions_id_seq'::regclass);


--
-- Name: transforms id; Type: DEFAULT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.transforms ALTER COLUMN id SET DEFAULT nextval('thingzone.transforms_id_seq'::regclass);


--
-- Name: transformsources id; Type: DEFAULT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.transformsources ALTER COLUMN id SET DEFAULT nextval('thingzone.transformsources_id_seq'::regclass);


--
-- Name: user_credentials id; Type: DEFAULT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.user_credentials ALTER COLUMN id SET DEFAULT nextval('thingzone.user_credentials_id_seq'::regclass);


--
-- Name: userprivileges id; Type: DEFAULT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.userprivileges ALTER COLUMN id SET DEFAULT nextval('thingzone.userprivileges_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.users ALTER COLUMN id SET DEFAULT nextval('thingzone.users_id_seq'::regclass);


--
-- Name: zonefilechanges id; Type: DEFAULT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.zonefilechanges ALTER COLUMN id SET DEFAULT nextval('thingzone.zonefilechanges_id_seq'::regclass);


--
-- Name: zonefilechangestransactions id; Type: DEFAULT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.zonefilechangestransactions ALTER COLUMN id SET DEFAULT nextval('thingzone.zonefilechangestransactions_id_seq'::regclass);


--
-- Name: accounts accounts_pkey; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.accounts
    ADD CONSTRAINT accounts_pkey PRIMARY KEY (id);


--
-- Name: acls acls_pkey; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.acls
    ADD CONSTRAINT acls_pkey PRIMARY KEY (id);


--
-- Name: acls acls_uq; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.acls
    ADD CONSTRAINT acls_uq UNIQUE (key, account_id, permission);


--
-- Name: datastreams datastreams_pkey; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.datastreams
    ADD CONSTRAINT datastreams_pkey PRIMARY KEY (dnskey);


--
-- Name: datastreams datastreams_uq_key; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.datastreams
    ADD CONSTRAINT datastreams_uq_key UNIQUE (dnskey);


--
-- Name: devices device_uq_key; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.devices
    ADD CONSTRAINT device_uq_key UNIQUE (dnskey);


--
-- Name: devices devices_pkey; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.devices
    ADD CONSTRAINT devices_pkey PRIMARY KEY (dnskey);


--
-- Name: dnsdata dnsdata_pkey; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.dnsdata
    ADD CONSTRAINT dnsdata_pkey PRIMARY KEY (id);


--
-- Name: dnsdata dnsdata_uq_key; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.dnsdata
    ADD CONSTRAINT dnsdata_uq_key UNIQUE (dnskey, subdomain, rrtype, rdata);


--
-- Name: dnskeys dnskeys_pkey; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.dnskeys
    ADD CONSTRAINT dnskeys_pkey PRIMARY KEY (dnskey);


--
-- Name: dnskeys dnskeys_uq_key; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.dnskeys
    ADD CONSTRAINT dnskeys_uq_key UNIQUE (dnskey);


--
-- Name: groups groups_pkey; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- Name: groups_users groups_users_pkey; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.groups_users
    ADD CONSTRAINT groups_users_pkey PRIMARY KEY (group_id, user_id);


--
-- Name: history history_pkey; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.history
    ADD CONSTRAINT history_pkey PRIMARY KEY (id);


--
-- Name: pullsubscriptions pullsubscriptions_pkey; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.pullsubscriptions
    ADD CONSTRAINT pullsubscriptions_pkey PRIMARY KEY (id);


--
-- Name: pushsubscribers pushsubscribers_pkey; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.pushsubscribers
    ADD CONSTRAINT pushsubscribers_pkey PRIMARY KEY (id);


--
-- Name: registry_properties registry_properties_pkey; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.registry_properties
    ADD CONSTRAINT registry_properties_pkey PRIMARY KEY (id);


--
-- Name: registry_properties registry_properties_property_key; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.registry_properties
    ADD CONSTRAINT registry_properties_property_key UNIQUE (property);


--
-- Name: resources resources_pkey; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.resources
    ADD CONSTRAINT resources_pkey PRIMARY KEY (name, key);


--
-- Name: sessions sessions_pkey; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.sessions
    ADD CONSTRAINT sessions_pkey PRIMARY KEY (sessionkey);


--
-- Name: streamsondevices streamsondevices_pkey; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.streamsondevices
    ADD CONSTRAINT streamsondevices_pkey PRIMARY KEY (id);


--
-- Name: transform_outputstreams transform_outputstreams_pkey; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.transform_outputstreams
    ADD CONSTRAINT transform_outputstreams_pkey PRIMARY KEY (transform_id, alias);


--
-- Name: transformfunctions transformfunctions_pkey; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.transformfunctions
    ADD CONSTRAINT transformfunctions_pkey PRIMARY KEY (id);


--
-- Name: transforms transforms_pkey; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.transforms
    ADD CONSTRAINT transforms_pkey PRIMARY KEY (id);


--
-- Name: transformsources transformsources_pkey; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.transformsources
    ADD CONSTRAINT transformsources_pkey PRIMARY KEY (id);


--
-- Name: accounts uq_account_group; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.accounts
    ADD CONSTRAINT uq_account_group UNIQUE (group_id);


--
-- Name: accounts uq_account_owner; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.accounts
    ADD CONSTRAINT uq_account_owner UNIQUE (owner_id);


--
-- Name: groups uq_group_name; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.groups
    ADD CONSTRAINT uq_group_name UNIQUE (name);


--
-- Name: pullsubscriptions uq_pullsubscriptions; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.pullsubscriptions
    ADD CONSTRAINT uq_pullsubscriptions UNIQUE (streamkey);


--
-- Name: pushsubscribers uq_pushsubscribers; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.pushsubscribers
    ADD CONSTRAINT uq_pushsubscribers UNIQUE (streamkey, uri);


--
-- Name: sessions uq_session; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.sessions
    ADD CONSTRAINT uq_session UNIQUE (sessionkey);


--
-- Name: streamsondevices uq_sod; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.streamsondevices
    ADD CONSTRAINT uq_sod UNIQUE (devicekey, datastreamkey);


--
-- Name: transform_outputstreams uq_transform_outputstream_datastream; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.transform_outputstreams
    ADD CONSTRAINT uq_transform_outputstream_datastream UNIQUE (transform_id, datastream_id);


--
-- Name: transformfunctions uq_transformfunctions; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.transformfunctions
    ADD CONSTRAINT uq_transformfunctions UNIQUE (name, owner_id);


--
-- Name: transformsources uq_transformsources; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.transformsources
    ADD CONSTRAINT uq_transformsources UNIQUE (transformid, alias);


--
-- Name: user_credentials uq_user_credentials_username; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.user_credentials
    ADD CONSTRAINT uq_user_credentials_username UNIQUE (username);


--
-- Name: user_credentials user_credentials_pkey; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.user_credentials
    ADD CONSTRAINT user_credentials_pkey PRIMARY KEY (id);


--
-- Name: user_tokens user_tokens_pkey; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.user_tokens
    ADD CONSTRAINT user_tokens_pkey PRIMARY KEY (token);


--
-- Name: userprivileges userprivileges_pkey; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.userprivileges
    ADD CONSTRAINT userprivileges_pkey PRIMARY KEY (id);


--
-- Name: userprivileges userprivs_uq; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.userprivileges
    ADD CONSTRAINT userprivs_uq UNIQUE (user_id, privilege);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: zonefilechanges zonefilechanges_pkey; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.zonefilechanges
    ADD CONSTRAINT zonefilechanges_pkey PRIMARY KEY (id);


--
-- Name: zonefilechangestransactions zonefilechangestransactions_pkey; Type: CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.zonefilechangestransactions
    ADD CONSTRAINT zonefilechangestransactions_pkey PRIMARY KEY (id);


--
-- Name: acls_key_idx; Type: INDEX; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE INDEX acls_key_idx ON thingzone.acls USING btree (key);


--
-- Name: dnskeys_type_idx; Type: INDEX; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE INDEX dnskeys_type_idx ON thingzone.dnskeys USING btree (entity_type);


--
-- Name: history_key_idx; Type: INDEX; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE INDEX history_key_idx ON thingzone.history USING btree (key);


--
-- Name: history_updated_idx; Type: INDEX; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE INDEX history_updated_idx ON thingzone.history USING btree (updated);


--
-- Name: pushsubscribers_streamkey_idx; Type: INDEX; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE INDEX pushsubscribers_streamkey_idx ON thingzone.pushsubscribers USING btree (streamkey);


--
-- Name: streamsondevices_datastreamkey_idx; Type: INDEX; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE INDEX streamsondevices_datastreamkey_idx ON thingzone.streamsondevices USING btree (datastreamkey);


--
-- Name: transforms_outputkey_idx; Type: INDEX; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE INDEX transforms_outputkey_idx ON thingzone.transforms USING btree (outputkey);


--
-- Name: transforms_runschedule_idx; Type: INDEX; Schema: thingzone; Owner: e2e_registry_rw
--

CREATE INDEX transforms_runschedule_idx ON thingzone.transforms USING btree (runschedule);


--
-- Name: accounts fk_accounts_group_id; Type: FK CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.accounts
    ADD CONSTRAINT fk_accounts_group_id FOREIGN KEY (group_id) REFERENCES thingzone.groups(id);


--
-- Name: accounts fk_accounts_owner_id; Type: FK CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.accounts
    ADD CONSTRAINT fk_accounts_owner_id FOREIGN KEY (owner_id) REFERENCES thingzone.users(id);


--
-- Name: acls fk_acls_account_id; Type: FK CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.acls
    ADD CONSTRAINT fk_acls_account_id FOREIGN KEY (account_id) REFERENCES thingzone.accounts(id);


--
-- Name: acls fk_acls_key; Type: FK CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.acls
    ADD CONSTRAINT fk_acls_key FOREIGN KEY (key) REFERENCES thingzone.dnskeys(dnskey);


--
-- Name: datastreams fk_datastreams_dnskey; Type: FK CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.datastreams
    ADD CONSTRAINT fk_datastreams_dnskey FOREIGN KEY (dnskey) REFERENCES thingzone.dnskeys(dnskey);


--
-- Name: devices fk_devices_dnskey; Type: FK CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.devices
    ADD CONSTRAINT fk_devices_dnskey FOREIGN KEY (dnskey) REFERENCES thingzone.dnskeys(dnskey);


--
-- Name: dnsdata fk_dnsdata_dnskey; Type: FK CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.dnsdata
    ADD CONSTRAINT fk_dnsdata_dnskey FOREIGN KEY (dnskey) REFERENCES thingzone.dnskeys(dnskey);


--
-- Name: groups fk_groups_owner_id; Type: FK CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.groups
    ADD CONSTRAINT fk_groups_owner_id FOREIGN KEY (owner_id) REFERENCES thingzone.users(id);


--
-- Name: groups_users fk_groups_users_group_id; Type: FK CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.groups_users
    ADD CONSTRAINT fk_groups_users_group_id FOREIGN KEY (group_id) REFERENCES thingzone.groups(id);


--
-- Name: groups_users fk_groups_users_user_id; Type: FK CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.groups_users
    ADD CONSTRAINT fk_groups_users_user_id FOREIGN KEY (user_id) REFERENCES thingzone.users(id);


--
-- Name: pullsubscriptions fk_pullsubscriptions_streamkey; Type: FK CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.pullsubscriptions
    ADD CONSTRAINT fk_pullsubscriptions_streamkey FOREIGN KEY (streamkey) REFERENCES thingzone.datastreams(dnskey);


--
-- Name: pushsubscribers fk_pushsubscribers_streamkey; Type: FK CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.pushsubscribers
    ADD CONSTRAINT fk_pushsubscribers_streamkey FOREIGN KEY (streamkey) REFERENCES thingzone.datastreams(dnskey);


--
-- Name: resources fk_resources_key; Type: FK CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.resources
    ADD CONSTRAINT fk_resources_key FOREIGN KEY (key) REFERENCES thingzone.dnskeys(dnskey);


--
-- Name: streamsondevices fk_streamsondevices_datastreamkey; Type: FK CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.streamsondevices
    ADD CONSTRAINT fk_streamsondevices_datastreamkey FOREIGN KEY (datastreamkey) REFERENCES thingzone.datastreams(dnskey);


--
-- Name: streamsondevices fk_streamsondevices_devicekey; Type: FK CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.streamsondevices
    ADD CONSTRAINT fk_streamsondevices_devicekey FOREIGN KEY (devicekey) REFERENCES thingzone.devices(dnskey);


--
-- Name: transform_outputstreams fk_transform_outputstreams_datastream_id; Type: FK CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.transform_outputstreams
    ADD CONSTRAINT fk_transform_outputstreams_datastream_id FOREIGN KEY (datastream_id) REFERENCES thingzone.datastreams(dnskey);


--
-- Name: transform_outputstreams fk_transform_outputstreams_transform_id; Type: FK CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.transform_outputstreams
    ADD CONSTRAINT fk_transform_outputstreams_transform_id FOREIGN KEY (transform_id) REFERENCES thingzone.transforms(id);


--
-- Name: transformfunctions fk_transformfunctions_owner_id; Type: FK CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.transformfunctions
    ADD CONSTRAINT fk_transformfunctions_owner_id FOREIGN KEY (owner_id) REFERENCES thingzone.users(id);


--
-- Name: transforms fk_transforms_functionid; Type: FK CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.transforms
    ADD CONSTRAINT fk_transforms_functionid FOREIGN KEY (functionid) REFERENCES thingzone.transformfunctions(id);


--
-- Name: transforms fk_transforms_outputkey; Type: FK CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.transforms
    ADD CONSTRAINT fk_transforms_outputkey FOREIGN KEY (outputkey) REFERENCES thingzone.datastreams(dnskey);


--
-- Name: transformsources fk_transformsources_sourcestream; Type: FK CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.transformsources
    ADD CONSTRAINT fk_transformsources_sourcestream FOREIGN KEY (sourcestream) REFERENCES thingzone.datastreams(dnskey);


--
-- Name: transformsources fk_transformsources_transformid; Type: FK CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.transformsources
    ADD CONSTRAINT fk_transformsources_transformid FOREIGN KEY (transformid) REFERENCES thingzone.transforms(id);


--
-- Name: user_credentials fk_user_credentials_user_id; Type: FK CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.user_credentials
    ADD CONSTRAINT fk_user_credentials_user_id FOREIGN KEY (user_id) REFERENCES thingzone.users(id);


--
-- Name: user_tokens fk_user_tokens_user_credentials_id; Type: FK CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.user_tokens
    ADD CONSTRAINT fk_user_tokens_user_credentials_id FOREIGN KEY (user_credentials_id) REFERENCES thingzone.user_credentials(id);


--
-- Name: userprivileges fk_userprivileges_user_id; Type: FK CONSTRAINT; Schema: thingzone; Owner: e2e_registry_rw
--

ALTER TABLE ONLY thingzone.userprivileges
    ADD CONSTRAINT fk_userprivileges_user_id FOREIGN KEY (user_id) REFERENCES thingzone.users(id);


--
-- PostgreSQL database dump complete
--

