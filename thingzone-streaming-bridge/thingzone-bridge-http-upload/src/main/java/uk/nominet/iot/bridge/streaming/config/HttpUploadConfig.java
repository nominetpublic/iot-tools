/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.config;

import org.apache.kafka.common.config.AbstractConfig;
import org.apache.kafka.common.config.ConfigDef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.nominet.iot.config.GenericConstant;
import uk.nominet.iot.config.ProvsvrConstant;
import uk.nominet.iot.config.RegistryConstant;

import java.util.Map;

public class HttpUploadConfig extends AbstractConfig {
    private static final Logger LOG = LoggerFactory.getLogger(HttpUploadConfig.class);

    public static ConfigDef baseConfigDef() {
        return new ConfigDef()
                .define(HttpUploadConstant.HTTP_UPLOAD_HOST,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Http Upload host")
                .define(HttpUploadConstant.HTTP_UPLOAD_PORT,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Http Upload Port")
                .define(HttpUploadConstant.BOOTSTRAP_SERVERS_CONFIG,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Bootstrap servers")
                .define(HttpUploadConstant.OUTPUT_TOPIC,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Output topic")
                .define(RegistryConstant.REGISTRY_SERVER_SCHEME,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Registry Server Scheme")
                .define(RegistryConstant.REGISTRY_SERVER_HOST,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Registry Host")
                .define(RegistryConstant.REGISTRY_SERVER_PATH,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Registry Path")
                .define(RegistryConstant.REGISTRY_SERVER_PORT,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Registry Port")
                .define(RegistryConstant.REGISTRY_SERVER_USERNAME,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Registry Username")
                .define(RegistryConstant.REGISTRY_SERVER_PASSWORD,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Registry Password")
                .define(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Registry Max Attempts")
                .define(GenericConstant.GENERATE_ACTIVITY_PROVENANCE,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Generate provenance for activity")
                .define(GenericConstant.GENERATE_AGENT_PROVENANCE,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Generate provenance agent")
                .define(GenericConstant.APPLICATION_PREFIX,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.LOW,
                        "Application Prefix")
                .define(ProvsvrConstant.PROVSVR_SERVER_SCHEME,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.LOW,
                        "Prov Server Scheme")
                .define(ProvsvrConstant.PROVSVR_SERVER_HOST,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.LOW,
                        "Prov Server Host")
                .define(ProvsvrConstant.PROVSVR_SERVER_PORT,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.LOW,
                        "Prov Server Port")
                .define(ProvsvrConstant.PROVSVR_SERVER_PATH,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.LOW,
                        "Prov Server Path");
    }

    public static final ConfigDef CONFIG_DEF = baseConfigDef();

    /**
     * Constructor
     *
     * @param props	properties map for configuration
     */
    public HttpUploadConfig(Map<String, String> props) {
        super(CONFIG_DEF, props);
        LOG.info("Initialize Http Upload service configuration");
    }
}
