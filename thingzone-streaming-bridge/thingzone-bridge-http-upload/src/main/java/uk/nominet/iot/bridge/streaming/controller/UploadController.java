/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.controller;

import com.google.gson.JsonObject;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.openprovenance.prov.model.Attribute;
import org.openprovenance.prov.model.QualifiedName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import spark.Service;
import uk.nominet.iot.annotations.ProvActivity;
import uk.nominet.iot.annotations.ProvAttribute;
import uk.nominet.iot.annotations.ProvId;
import uk.nominet.iot.bridge.streaming.config.HttpUploadConfig;
import uk.nominet.iot.bridge.streaming.config.HttpUploadConstant;
import uk.nominet.iot.bridge.streaming.error.PayloadBuildingException;
import uk.nominet.iot.bridge.streaming.utils.PayloadBuilder;
import uk.nominet.iot.common.Taxonomy;
import uk.nominet.iot.common.ThingzoneType;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.error.ProvenanceRecordViolationException;
import uk.nominet.iot.http.APIController;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.ConnectorPayload;
import uk.nominet.iot.model.api.ExceptionHandler;
import uk.nominet.iot.model.api.ResponseBuilder;
import uk.nominet.iot.provenance.ProvenanceManager;
import uk.nominet.iot.provenance.SoftwareAgentContext;
import uk.nominet.iot.provenance.ThingzoneActivity;

import java.time.Instant;
import java.util.Optional;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@ProvActivity(type = ThingzoneType.process)
public class UploadController implements APIController {
    @ProvAttribute(attribute = Attribute.AttributeKind.PROV_LABEL)
    protected String name = "Payload Upload";


    private static final Logger LOG = LoggerFactory.getLogger(UploadController.class);
    static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
    private KafkaProducer<String, String> producer;
    private String outputTopic;
    private SoftwareAgentContext softwareAgentContext;

    public UploadController(HttpUploadConfig config, SoftwareAgentContext softwareAgentContext) {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
                  config.getString(HttpUploadConstant.BOOTSTRAP_SERVERS_CONFIG));
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "ThingzoneHttpUpload");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                  StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                  StringSerializer.class.getName());
        producer = new KafkaProducer<>(props);

        outputTopic = config.getString(HttpUploadConstant.OUTPUT_TOPIC);

        this.softwareAgentContext = softwareAgentContext;
    }

    private String upload(Request req, Response res) {
        System.out.println("Uploading");

        String stream = req.params(":stream");
        String[] keys = {stream};
        String[] permissions = {Taxonomy.PERMISSION_UPLOAD};
        String session = req.headers("X-Introspective-Session-Key");

        try {
            RegistryAPIDriver.getSession().internal.permissionCheck(keys, permissions, Optional.of(session), Optional.empty());
        } catch (Exception e) {
            LOG.error("Permission error", e);
            return ExceptionHandler.handle(res, e, "Permission error");
        }

        ConnectorPayload payload = null;
        try {
            payload = PayloadBuilder.fromRequest(req);
        } catch (PayloadBuildingException e) {
            LOG.error("Could not build payload", e);
            return ExceptionHandler.handle(res, e, "Could not build payload");
        }


        //Record Provenance
        if (softwareAgentContext != null) {
            try {
                ProvenanceManager pm = softwareAgentContext.pm;
                QualifiedName processId = pm.qn(pm.getDefaultPrefix(), "Upload"+Instant.now().toEpochMilli());
                ThingzoneActivity ac = new ThingzoneActivity(pm, this, processId);
                ac.start();
                ac.generated(payload, "connectorPayload", Instant.now());
                ac.associatedWith(pm.qn(pm.getDefaultPrefix(), UUID.randomUUID().toString()),
                                  softwareAgentContext.getAgent().getId());

                ac.associatedWith(pm.qn(pm.getDefaultPrefix(), UUID.randomUUID().toString()),
                                  pm.qn(pm.getDefaultPrefix(),"Nominet" ));
                ac.stop();
                ac.commitToProvenanceRecord();
            } catch (Exception e) {
                // The process is doing something invalid
                // rollback?
                return ExceptionHandler.handle(res, e, "Could not assert provenance of activity" + e.getMessage());
            }
        }


        final ProducerRecord<String, String> record;
        try {
            record = new ProducerRecord<>(outputTopic, payload.getId().toString(),
                                          jsonStringSerialiser.writeObject(payload));
        } catch (Exception e) {
            LOG.error("Could not serialise payload", e);
            return ExceptionHandler.handle(res, e, "Could not serialise payload");
        }

        RecordMetadata metadata = null;

        try {
            metadata = producer.send(record).get();
        } catch (InterruptedException e) {
            LOG.error("Kafka error", e);
            return ExceptionHandler.handle(res, e);
        } catch (ExecutionException e) {
            LOG.error("Kafka error", e);
            return ExceptionHandler.handle(res, e);
        }


        JsonObject response = new ResponseBuilder(true)
                                      .addProperty("id", payload.getId().toString())
                                      .addProperty("partition", metadata.partition())
                                      .addProperty("offset", metadata.offset())
                                      .build();


        return response.toString();
    }

    @Override
    public void publishAPI(Service http, String path) {
        http.path(path, () -> {
            http.post("/:stream", (req, res) -> upload(req, res));
        });
    }

}
