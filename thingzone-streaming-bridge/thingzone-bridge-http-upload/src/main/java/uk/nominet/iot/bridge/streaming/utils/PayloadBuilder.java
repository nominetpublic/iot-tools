/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.utils;

import com.google.common.io.ByteStreams;
import spark.Request;
import uk.nominet.iot.bridge.streaming.HttpUploadApp;
import uk.nominet.iot.bridge.streaming.error.PayloadBuildingException;
import uk.nominet.iot.bridge.utils.ConnectorVersion;
import uk.nominet.iot.model.ConnectorPayload;
import uk.nominet.iot.model.Source;
import uk.nominet.iot.model.ThingzoneConnectorContext;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletException;
import javax.servlet.http.Part;
import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PayloadBuilder {

    public static ConnectorPayload fromRequest(Request req) throws PayloadBuildingException {
        String contentType = req.contentType().split(";")[0];

        ConnectorPayload payload = new ConnectorPayload();
        payload.setId(UUID.randomUUID());
        payload.setReceivedAt(Instant.now());

        Source source = new Source();
        source.setId(req.ip());
        source.setEndpoint(req.ip());

        payload.setSource(source);

        ThingzoneConnectorContext connectorContext = new ThingzoneConnectorContext();

        String taskname = String.format("%s: %s %s", HttpUploadApp.class.getName(), req.requestMethod(), req.uri());
        connectorContext.setTaskName(taskname);
        connectorContext.setTaskVersion(ConnectorVersion.getVersion());

        payload.setConnectorContext(connectorContext);
        payload.setRegistryUser(req.attribute("introspected_username"));
        payload.setContentType(contentType);
        payload.setKey(req.params(":stream"));

        Map<String, Object> metadata = new HashMap<>();

        Map<String, Object> headers = new HashMap<>();
        req.headers().forEach(header -> {
            if (!header.equals("X-Introspective-Session-Key")) {
                headers.put(header, req.headers(header));
            }
        });

        metadata.put("headers", headers);
        metadata.put("params", req.queryMap().toMap());
        metadata.put("user", req.attribute("introspected_username"));

        payload.setMetadata(metadata);

        switch (contentType) {
            case "multipart/form-data":
                MultipartConfigElement multipartConfigElement = new MultipartConfigElement("/tmp");
                req.raw().setAttribute("org.eclipse.jetty.multipartConfig", multipartConfigElement);
                try {
                    Part file = req.raw().getPart("file");
                    if (file.getContentType() != null) payload.setContentType(file.getContentType());
                    payload.setData(ByteStreams.toByteArray(file.getInputStream()));
                } catch (IOException e) {
                    throw new PayloadBuildingException(e);
                } catch (ServletException e) {
                    throw new PayloadBuildingException(e);
                }
                break;
            default:
                if (req.bodyAsBytes() == null) throw new PayloadBuildingException("Request body had no data");
                payload.setData(req.bodyAsBytes());
                break;
        }

        return payload;
    }
}
