/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming;

import static spark.Service.ignite;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Properties;

import org.apache.kafka.common.config.ConfigException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;

import spark.Service;
import uk.nominet.iot.bridge.Version;
import uk.nominet.iot.bridge.config.ConfigurationLoader;
import uk.nominet.iot.bridge.streaming.config.HttpUploadConfig;
import uk.nominet.iot.bridge.streaming.config.HttpUploadConstant;
import uk.nominet.iot.bridge.streaming.controller.UploadController;
import uk.nominet.iot.config.*;
import uk.nominet.iot.driver.ProvsvrDriver;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.http.CORSEnabler;
import uk.nominet.iot.http.ThingzoneRegistrySessionKeyIntrospector;
import uk.nominet.iot.provenance.SoftwareAgentContext;
import uk.nominet.iot.provenance.ProvenanceManager;


public class HttpUploadApp {
    private static final Logger LOG = LoggerFactory.getLogger(HttpUploadApp.class);
    private static HttpUploadConfig config;
    public static final int API_VERSION = 1;
    private static SoftwareAgentContext softwareAgentContext;

    public static void main(String[] args) throws Exception {
        LOG.info("Http Upload service");

        if (args.length != 1) {
            System.out.println("Usage: HttpUploadApp configFile");
            System.exit(1);
        }

        Properties configProp = ConfigurationLoader.fromFileName(args[0]);
        try {
            config = new HttpUploadConfig(Maps.fromProperties(configProp));
        } catch (ConfigException e) {
            LOG.error(e.getMessage());
            throw new Exception("Couldn't start HttpUploadApp due to config error", e);
        }

        if (!RegistryAPIDriver.isInitialized()) {
            RegistryDriverConfig registryConfig = new RegistryDriverConfig();
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_SCHEME,
                    config.getString(RegistryConstant.REGISTRY_SERVER_SCHEME));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST,
                    config.getString(RegistryConstant.REGISTRY_SERVER_HOST));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT,
                    config.getString(RegistryConstant.REGISTRY_SERVER_PORT));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_USERNAME,
                    config.getString(RegistryConstant.REGISTRY_SERVER_USERNAME));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD,
                    config.getString(RegistryConstant.REGISTRY_SERVER_PASSWORD));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS,
                    config.getString(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS));
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }

        if (Boolean.parseBoolean(config.getString(GenericConstant.GENERATE_ACTIVITY_PROVENANCE))
            || Boolean.parseBoolean(config.getString(GenericConstant.GENERATE_AGENT_PROVENANCE))) {
            if (!ProvsvrDriver.isInitialized()) {
                ProvsvrDriverConfig provsvrConfig = new ProvsvrDriverConfig();
                provsvrConfig.addProperty(ProvsvrConstant.PROVSVR_SERVER_SCHEME,
                                          config.getString(ProvsvrConstant.PROVSVR_SERVER_SCHEME));
                provsvrConfig.addProperty(ProvsvrConstant.PROVSVR_SERVER_HOST,
                                          config.getString(ProvsvrConstant.PROVSVR_SERVER_HOST));
                provsvrConfig.addProperty(ProvsvrConstant.PROVSVR_SERVER_PORT,
                                          config.getString(ProvsvrConstant.PROVSVR_SERVER_PORT));
                ProvsvrDriver.setConnectionProperties(provsvrConfig);
                ProvsvrDriver.globalInitialise();
            }

            ProvenanceManager provenanceManager = new ProvenanceManager(config.getString(GenericConstant.APPLICATION_PREFIX));
            softwareAgentContext = new SoftwareAgentContext(provenanceManager);

            softwareAgentContext.register(HttpUploadApp.class, Version.getVersion(), Instant.now());
        }



        //Initialise http server
        Service http = null;

        http = ignite()
                .port(Integer.parseInt(configProp.getProperty(HttpUploadConstant.HTTP_UPLOAD_PORT)))
                .ipAddress(configProp.getProperty(HttpUploadConstant.HTTP_UPLOAD_HOST))
                .threadPool(20);

        // Attach security components
        new CORSEnabler("*",
                "GET,PUT,POST,DELETE,OPTIONS",
                "Content-Type,Authorization,X-Requested-With," +
                        "Content-Length,Accept,Origin,X-Introspective-Session-Key")
                .attachTo(http);

        new ThingzoneRegistrySessionKeyIntrospector(
                false,
                null,
                new ArrayList<>())
                .attachTo(http);

        http.exception(Exception.class, (e, req, res) -> {
            LOG.error(e.getMessage(), e);
            LOG.debug("Request: " + req.toString());
            LOG.debug("Response: " + res.toString());
            e.printStackTrace();
        });

        // Instantiate controllers
        new UploadController(config, softwareAgentContext).publishAPI(http, String.format("/api/v%d/upload", API_VERSION));
    }

}
