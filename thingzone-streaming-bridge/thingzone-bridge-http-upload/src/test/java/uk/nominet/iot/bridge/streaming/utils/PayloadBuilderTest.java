/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import spark.QueryParamsMap;
import spark.Request;
import uk.nominet.iot.bridge.streaming.error.PayloadBuildingException;
import uk.nominet.iot.model.ConnectorPayload;

@Ignore
public class PayloadBuilderTest {
    HttpServletRequest servletRequest;
    HttpSession httpSession;
    Request request;

    @Before
    public void setup() {
        servletRequest = mock(HttpServletRequest.class);
        httpSession = mock(HttpSession.class);
        request = mock(Request.class);

        when(request.contentType()).thenReturn("application/json");
        when(request.ip()).thenReturn("127.0.0.1");
        when(request.requestMethod()).thenReturn("POST");
        when(request.uri()).thenReturn("/upload");
        when(request.attribute("introspected_username")).thenReturn("test");
        when(request.params(":stream")).thenReturn("device.stream1");

        Set<String> headers = new HashSet<>();
        headers.add("foo");
        headers.add("X-Introspective-Session-Key");

        when(request.headers()).thenReturn(headers);
        when(request.headers("foo")).thenReturn("bar");
        when(request.headers("X-Introspective-Session-Key")).thenReturn("123456789");

        QueryParamsMap queryMap = mock(QueryParamsMap.class);

        Map<String,String[]> map = new HashMap<>();
        String[] p1 = {"aa", "bb"};
        map.put("p1", p1);

        when(queryMap.toMap()).thenReturn(map);
        when(request.queryMap()).thenReturn(queryMap);

        String data = "123456789abcdefghi";
        when(request.bodyAsBytes()).thenReturn(data.getBytes());
        when(request.raw()).thenReturn(servletRequest);
    }

    @Test
    public void completePayload() throws IOException, ServletException, PayloadBuildingException {
        ConnectorPayload payload = PayloadBuilder.fromRequest(request);
        assertNotNull(payload.getId());
        assertNotNull(payload.getReceivedAt());
        assertEquals("127.0.0.1", payload.getSource().getEndpoint());
        assertEquals("127.0.0.1", payload.getSource().getId());
        assertNull(payload.getSource().getMetadata());
        assertEquals("uk.nominet.iot.bridge.streaming.HttpUploadApp: POST /upload",
                payload.getConnectorContext().getTaskName());
       // assertEquals("1.0-SNAPSHOT", payload.getConnectorContext().getTaskVersion());
        assertEquals("test", payload.getRegistryUser());
        assertEquals("application/json", payload.getContentType());
        assertEquals("device.stream1", payload.getKey());
        assertNotNull(payload.getMetadata().get("headers"));
        assertNotNull(payload.getMetadata().get("params"));
        assertNotNull(payload.getData());
        assertEquals(18, payload.getData().length);
    }

    @Test(expected = PayloadBuildingException.class)
    public void requestHasNoData() throws PayloadBuildingException {
        byte[] data = null;
        when(request.bodyAsBytes()).thenReturn(data);

        ConnectorPayload payload = PayloadBuilder.fromRequest(request);
    }
}
