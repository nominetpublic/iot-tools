/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.KStream;
import org.openprovenance.prov.model.Attribute;
import org.openprovenance.prov.model.QualifiedName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.nominet.iot.annotations.ProvActivity;
import uk.nominet.iot.annotations.ProvAttribute;
import uk.nominet.iot.bridge.Version;
import uk.nominet.iot.bridge.streaming.config.TransformOutputDispatcherConfig;
import uk.nominet.iot.bridge.streaming.config.TransformOutputDispatcherConstant;
import uk.nominet.iot.common.ThingzoneType;
import uk.nominet.iot.config.GenericConstant;
import uk.nominet.iot.config.ProvsvrConstant;
import uk.nominet.iot.config.ProvsvrDriverConfig;
import uk.nominet.iot.driver.ProvsvrDriver;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;
import uk.nominet.iot.model.registry.RegistryTransform;
import uk.nominet.iot.model.script.ExecutionTaskOutput;
import uk.nominet.iot.provenance.ProvenanceManager;
import uk.nominet.iot.provenance.SoftwareAgentContext;
import uk.nominet.iot.provenance.ThingzoneActivity;

import java.time.Instant;
import java.util.*;

@ProvActivity(type = ThingzoneType.process)
public class TransformOutputDispatcherProcess implements KafkaStreamingProcess {

    private static final Logger LOG = LoggerFactory.getLogger(TransformOutputDispatcherProcess.class);
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    private KafkaStreams streams;
    private final Topology topology;

    @ProvAttribute(attribute = Attribute.AttributeKind.PROV_LABEL)
    protected String name = "Transform Dispatcher";


    private String streamKeyFromAlias(RegistryTransform transfrom, String alias) throws Exception {
        Optional<String> streamKey = transfrom.getOutputStreams()
                                              .stream()
                                              .filter(outputStream -> outputStream.getAlias().equals(alias))
                                              .map(outputStream -> outputStream.getKey())
                                              .findFirst();
        if (streamKey.isPresent()) {
            return streamKey.get();
        } else {
            throw new Exception("Alias not found");
        }
    }

    public TransformOutputDispatcherProcess(TransformOutputDispatcherConfig config) throws Exception {

        LOG.info("Initialising Transform Output Dispatcher");
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG,
                  config.getString(TransformOutputDispatcherConstant.APPLICATION_STATE_ID_CONFIG));
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG,
                  config.getString(TransformOutputDispatcherConstant.BOOTSTRAP_SERVERS_CONFIG));
        props.put(StreamsConfig.EXACTLY_ONCE, true);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());


        // Initialise provenance service
        SoftwareAgentContext softwareAgentContext = null;
        if (Boolean.parseBoolean(config.getString(GenericConstant.GENERATE_ACTIVITY_PROVENANCE))
            || Boolean.parseBoolean(config.getString(GenericConstant.GENERATE_AGENT_PROVENANCE))) {
            if (!ProvsvrDriver.isInitialized()) {
                ProvsvrDriverConfig provsvrConfig = new ProvsvrDriverConfig();
                provsvrConfig.addProperty(ProvsvrConstant.PROVSVR_SERVER_SCHEME,
                                          config.getString(ProvsvrConstant.PROVSVR_SERVER_SCHEME));
                provsvrConfig.addProperty(ProvsvrConstant.PROVSVR_SERVER_HOST,
                                          config.getString(ProvsvrConstant.PROVSVR_SERVER_HOST));
                provsvrConfig.addProperty(ProvsvrConstant.PROVSVR_SERVER_PORT,
                                          config.getString(ProvsvrConstant.PROVSVR_SERVER_PORT));
                ProvsvrDriver.setConnectionProperties(provsvrConfig);
                ProvsvrDriver.globalInitialise();
            }

            ProvenanceManager provenanceManager = new ProvenanceManager(config.getString(GenericConstant.APPLICATION_PREFIX));
            softwareAgentContext = new SoftwareAgentContext(provenanceManager);

            softwareAgentContext.register(TransformOutputDispatcherProcess.class, Version.getVersion(), Instant.now());
        }


        final StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> inputStream = builder.stream(config.getString(TransformOutputDispatcherConstant.INPUT_TOPIC));

        SoftwareAgentContext finalSoftwareAgentContext = softwareAgentContext;
        inputStream.flatMap((key, output) -> {

            System.out.println("Processing: " + output);

            // List of values to return with the flat map
            List<KeyValue<String, String>> out = new ArrayList<>();

            //List of timeseries points to return in the flat map
            List<AbstractTimeseriesPoint> timeseriesPoints = new ArrayList<>();

            ExecutionTaskOutput taskOutput = null;

            // Decode output
            try {
                taskOutput = jsonStringSerialiser.readObject(output, ExecutionTaskOutput.class);
            } catch (Exception e) {
                e.printStackTrace();
                LOG.error(e.getMessage());
                return new ArrayList<KeyValue<String, String>>();
            }

            //Get transform object from
            RegistryTransform transfrom = null;
            try {
                transfrom = (RegistryTransform) RegistryAPIDriver.getSession()
                                                        .internal
                                                        .identify(taskOutput.getTransformKey(),
                                                                  true,
                                                                  true);
            } catch (Exception e) {
                e.printStackTrace();
                LOG.error(e.getMessage());
                return new ArrayList<KeyValue<String, String>>();
            }


            if (transfrom == null) {
                LOG.error("Could not get transfrom for with key" + taskOutput.getTransformKey());
                return new ArrayList<KeyValue<String, String>>();
            }


            //Add transform output to timeseries list
            if (taskOutput.getTransformOutput() != null) {
                AbstractTimeseriesPoint transformOutput = taskOutput.getTransformOutput();
                transformOutput.setStreamKey(transfrom.getKey());
                if (transformOutput.getTimestamp() != null) {
                    transformOutput.setTimestamp(transformOutput.getTimestamp());
                } else {
                    transformOutput.setTimestamp(Instant.now());
                }
                if (transformOutput.getId() == null) transformOutput.setId(UUID.randomUUID());
                timeseriesPoints.add(transformOutput);
            }


            //Add stream outputs to timeseries list
            if (taskOutput.getStreamOutputs() != null) {
                //timeseriesPoints.addAll(
                final RegistryTransform finalTransfrom = transfrom;
                taskOutput.getStreamOutputs().forEach((alias, point) -> {

                    if (point != null) {
                        System.out.println("Alias: " + alias);

                        try {
                            point.setStreamKey(streamKeyFromAlias(finalTransfrom, alias));
                            if (point.getId() == null)
                                point.setId(UUID.randomUUID());
                            if (point.getTimestamp() != null) {
                                point.setTimestamp(point.getTimestamp());
                            } else {
                                point.setTimestamp(Instant.now());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            LOG.error(e.getMessage());
                        }

                        timeseriesPoints.add(point);
                    }
                });

            }


            if (finalSoftwareAgentContext != null) {
                try {
                    // Thread.sleep(1000);
                    ProvenanceManager pm = finalSoftwareAgentContext.pm;
                    QualifiedName processId = pm.qn(pm.getDefaultPrefix(), "Mapping" + Instant.now().toEpochMilli());
                    ThingzoneActivity ac = new ThingzoneActivity(pm, this, processId);
                    ac.start();

                    for (UUID uuid : taskOutput.getInputs()) {
                        ac.used(pm.qn(finalSoftwareAgentContext.pm.getDefaultPrefix(), UUID.randomUUID().toString()),
                                pm.qn(finalSoftwareAgentContext.pm.getDefaultPrefix(), uuid.toString()),
                                Instant.now());
                    }


                    for (AbstractTimeseriesPoint point : timeseriesPoints) {
                        ac.generated(point, "transform_output", Instant.now());
                    }

                    ac.associatedWith(pm.qn(pm.getDefaultPrefix(), UUID.randomUUID().toString()),
                                      finalSoftwareAgentContext.getAgent().getId());

                    ac.associatedWith(pm.qn(pm.getDefaultPrefix(), UUID.randomUUID().toString()),
                                      pm.qn(pm.getDefaultPrefix(), "Nominet"));


                    ac.stop();
                    ac.commitToProvenanceRecord();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            //Serialise the timeseries and add it to the output list
            try {
                out.add(new KeyValue<>(key, jsonStringSerialiser.writeObject(timeseriesPoints)));
            } catch (Exception e) {
                e.printStackTrace();
                LOG.error(e.getMessage());

            }

            return out;
        }).to(config.getString(TransformOutputDispatcherConstant.OUTPUT_TOPIC));

        topology = builder.build();
        streams = new KafkaStreams(topology, props);
    }

    @Override
    public KafkaStreams getStreams() {
        return streams;
    }

    @Override
    public Topology getTopology() {
        return topology;
    }
}
