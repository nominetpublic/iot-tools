/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming;

import com.google.common.collect.Maps;
import org.apache.kafka.common.config.ConfigException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.nominet.iot.bridge.config.ConfigurationLoader;
import uk.nominet.iot.bridge.streaming.config.TransformOutputDispatcherConfig;
import uk.nominet.iot.config.ElasticsearchConstant;
import uk.nominet.iot.config.ElasticsearchDriverConfig;
import uk.nominet.iot.config.RegistryConstant;
import uk.nominet.iot.config.RegistryDriverConfig;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.driver.RegistryAPIDriver;

import java.util.Properties;

public class TransformOutputDispatcherApp {
    private static final Logger LOG = LoggerFactory.getLogger(TransformOutputDispatcherApp.class);

    public static void main(String[] args) throws Exception {
        LOG.info("Start Transform Output Dispatcher service");

        if (args.length != 1) {
            System.out.println("Usage: TransformOutputDispatcherApp configFile");
            System.exit(1);
        }

        Properties configProp = ConfigurationLoader.fromFileName(args[0]);

        TransformOutputDispatcherConfig config;
        try {
            config = new TransformOutputDispatcherConfig(Maps.fromProperties(configProp));
        } catch (ConfigException e) {
            LOG.error(e.getMessage());
            throw new Exception("Couldn't start Transform Output Dispatcher due to config error", e);
        }

        if (!RegistryAPIDriver.isInitialized()){
            RegistryDriverConfig registryConfig = new RegistryDriverConfig();
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_SCHEME,
                                       config.getString(RegistryConstant.REGISTRY_SERVER_SCHEME));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST,
                                       config.getString(RegistryConstant.REGISTRY_SERVER_HOST));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT,
                                       config.getString(RegistryConstant.REGISTRY_SERVER_PORT));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_USERNAME,
                                       config.getString(RegistryConstant.REGISTRY_SERVER_USERNAME));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD,
                                       config.getString(RegistryConstant.REGISTRY_SERVER_PASSWORD));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS,
                                       config.getString(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS));
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }

        if (!ElasticsearchDriver.isInitialized()) {
            ElasticsearchDriverConfig elasticsearchConfig = new ElasticsearchDriverConfig();
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_SCHEME,
                                            config.getString(ElasticsearchConstant.ELASTICSEARCH_SERVER_SCHEME));
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_HOST,
                                            config.getString(ElasticsearchConstant.ELASTICSEARCH_SERVER_HOST));
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_PORT,
                                            config.getString(ElasticsearchConstant.ELASTICSEARCH_SERVER_PORT));
            ElasticsearchDriver.setConnectionProperties(elasticsearchConfig);
            ElasticsearchDriver.globalInitialise();
        }

        // Initialise and run the state builder streaming process
//        TransformWorkerProcess transformWorkerProcess = new TransformWorkerProcess(config);
//        KafkaStreamingProcessExecutor transformWorkerProcessExecutor =
//                new KafkaStreamingProcessExecutor(transformWorkerProcess);
//        transformWorkerProcessExecutor.start();


        TransformOutputDispatcherProcess transformOutputDispatcherProcess = new TransformOutputDispatcherProcess(config);
        KafkaStreamingProcessExecutor transformOutputProcessExecutor =
                new KafkaStreamingProcessExecutor(transformOutputDispatcherProcess);
        transformOutputProcessExecutor.start();

    }
}
