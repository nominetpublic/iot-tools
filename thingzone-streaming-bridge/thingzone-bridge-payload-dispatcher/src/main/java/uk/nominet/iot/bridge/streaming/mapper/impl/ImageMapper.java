/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.mapper.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.*;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.tiff.TiffField;
import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;
import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.nominet.iot.bridge.streaming.config.PayloadDispatcherConfig;
import uk.nominet.iot.bridge.streaming.dto.PayloadMapperOutput;
import uk.nominet.iot.bridge.streaming.mapper.PayloadMapper;
import uk.nominet.iot.config.ClassifierConstant;
import uk.nominet.iot.driver.ImagesvrDriver;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.ConnectorPayload;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;
import uk.nominet.iot.model.core.Classification;
import uk.nominet.iot.model.registry.RegistryDataStream;
import uk.nominet.iot.model.timeseries.Image;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.*;

public class ImageMapper extends PayloadMapper {
    private static final Logger LOG = LoggerFactory.getLogger(ImageMapper.class);
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    public ImageMapper(PayloadDispatcherConfig config) {
        super(config);
    }

    private static final DateTimeFormatter FMT = new DateTimeFormatterBuilder()
            .appendPattern("yyyy:MM:dd HH:mm:ss")
            .toFormatter()
            .withZone(ZoneId.systemDefault());

    @Override
    public boolean canDecodePayload(ConnectorPayload payload) {
        return payload.getContentType().startsWith("image/");
    }

    public Map<String, Object> getMetadataFromImage(byte[] imageData) throws IOException, ImageReadException {
        final ImageMetadata metadata = Imaging.getMetadata(imageData);

        Map<String, Object> metadataProperties = new HashMap<>();

        if (metadata instanceof JpegImageMetadata) {
            final JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;
            TiffImageMetadata exif = jpegMetadata.getExif();

            for (TiffField field : exif.getAllFields()) {
                try {
                    metadataProperties.put(field.getTagName(), field.getStringValue());
                } catch (Exception e) {
                    System.out.println("No string exif");
                }
            }
        }

        return metadataProperties;
    }


    @Override
    public PayloadMapperOutput flatMapToTimeseries(ConnectorPayload payload) {
        if (!canDecodePayload(payload)) {
            return new PayloadMapperOutput();
        }

        PayloadMapperOutput output = new PayloadMapperOutput();
        List<AbstractTimeseriesPoint> timeseries = new ArrayList<>();


        try {
            String reference = ImagesvrDriver.getAPIClient().upload.fromBytes(payload.getData());

            Map<String, Object> metadataProperties = new HashMap<>();
            metadataProperties.putAll(getMetadataFromImage(payload.getData()));
            metadataProperties.putAll(payload.getMetadata());



            Image imagePoint = new Image();
            imagePoint.setId(UUID.randomUUID());
            imagePoint.setStreamKey(payload.getKey());
            imagePoint.setMetadata(metadataProperties);
            imagePoint.setTimestamp(payload.getReceivedAt());
            imagePoint.setImageRef(reference);

            if (metadataProperties != null && metadataProperties.containsKey("DateTime")) {
                Instant instant = FMT.parse(metadataProperties.get("DateTime").toString(), Instant::from);
                imagePoint.setTimestamp(instant);
            }

            imagePoint.getMetadata().put("receivedAt", payload.getReceivedAt());

            // Get datastream information from the registry
            String streamKey = payload.getKey();
            RegistryDataStream stream =
                    (RegistryDataStream) RegistryAPIDriver
                                                 .getSession()
                                                 .internal
                                                 .identify(streamKey, true, false);


            if (stream.getMetadata().containsKey("classifier")) {
                URIBuilder urlBuilder = new URIBuilder();
                urlBuilder.setScheme(config.getString(ClassifierConstant.CLASSIFIER_SERVER_SCHEME));
                urlBuilder.setHost(config.getString(ClassifierConstant.CLASSIFIER_SERVER_HOST));
                urlBuilder.setPath(config.getString(ClassifierConstant.CLASSIFIER_SERVER_PATH));
                urlBuilder.setPort(Integer.parseInt(config.getString(ClassifierConstant.CLASSIFIER_SERVER_PORT)));

                HttpConnectionManager httpConnectionManager = new MultiThreadedHttpConnectionManager();
                HttpClient httpClient = new HttpClient(httpConnectionManager);

                PostMethod postMethod = new PostMethod(urlBuilder.build().toString());

                ByteArrayPartSource byteArrayPartSource = new ByteArrayPartSource("image", payload.getData());
                FilePart filePart = new FilePart("image", byteArrayPartSource);
                StringPart dataStreamPart =
                        new StringPart("dataStream", jsonStringSerialiser.writeObject(stream));
                Part[] parts = {filePart, dataStreamPart};
                try {
                    MultipartRequestEntity multipartRequestEntity =
                            new MultipartRequestEntity(parts, postMethod.getParams());
                    postMethod.setRequestEntity(multipartRequestEntity);
                    httpClient.executeMethod(postMethod);

                    JsonElement classificationResponse =
                            new Gson().fromJson(postMethod.getResponseBodyAsString(), JsonElement.class);
                    if (classificationResponse.isJsonArray()) {
                        JsonArray classificationJson = classificationResponse.getAsJsonArray();
                        System.out.println(classificationJson.toString());
                        List<Classification> classifications = jsonStringSerialiser
                                .getDefaultSerialiser()
                                .fromJson(classificationJson, new TypeToken<List<Classification>>() {
                                }.getType());
                        classifications.forEach((u) -> u.setTimestamp(Instant.now()));
                        imagePoint.setClassifications(classifications);
                    } else {
                        LOG.error("classification failed for "+ payload.getKey() + " Not found Image Desciption, " +
                                "or problem in dataStream format" + classificationResponse);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            timeseries.add(imagePoint);

        } catch (Exception e) {
            e.printStackTrace();
            LOG.error(e.getMessage(), e);
        }

        output.setTimeseries(timeseries);
        return output;
    }

}
