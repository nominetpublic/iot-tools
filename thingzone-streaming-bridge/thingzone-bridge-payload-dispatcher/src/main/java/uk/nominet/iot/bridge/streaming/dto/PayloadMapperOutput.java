/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.dto;

import uk.nominet.iot.model.core.AbstractTimeseriesPoint;

import java.util.List;

public class PayloadMapperOutput {
    private List<AbstractTimeseriesPoint> timeseries;
    private Error error;

    public PayloadMapperOutput() {
    }

    public List<AbstractTimeseriesPoint> getTimeseries() {
        return timeseries;
    }

    public PayloadMapperOutput setTimeseries(List<AbstractTimeseriesPoint> timeseries) {
        this.timeseries = timeseries;
        return this;
    }

    public Error getError() {
        return error;
    }

    public PayloadMapperOutput setError(Error error) {
        this.error = error;
        return this;
    }

    @Override
    public String toString() {
        return "PayloadMapperOutput{" +
                "timeseries=" + timeseries +
                ", error=" + error +
                '}';
    }
}
