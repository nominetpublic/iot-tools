/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming;

import java.lang.reflect.InvocationTargetException;
import java.time.Instant;
import java.util.Properties;
import java.util.UUID;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;
import org.openprovenance.prov.model.Attribute;
import org.openprovenance.prov.model.QualifiedName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nominet.iot.annotations.ProvActivity;
import uk.nominet.iot.annotations.ProvAttribute;
import uk.nominet.iot.bridge.Version;
import uk.nominet.iot.bridge.streaming.config.PayloadDispatcherConfig;
import uk.nominet.iot.bridge.streaming.config.PayloadDispatcherConstant;
import uk.nominet.iot.bridge.streaming.dto.MappedPayload;
import uk.nominet.iot.bridge.streaming.dto.PayloadMapperOutput;
import uk.nominet.iot.bridge.streaming.mapper.PayloadMapperEngine;
import uk.nominet.iot.bridge.streaming.mapper.impl.*;
import uk.nominet.iot.common.ThingzoneType;
import uk.nominet.iot.config.*;
import uk.nominet.iot.driver.ImagesvrDriver;
import uk.nominet.iot.driver.ProvsvrDriver;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.ConnectorPayload;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;
import uk.nominet.iot.provenance.ProvenanceManager;
import uk.nominet.iot.provenance.SoftwareAgentContext;
import uk.nominet.iot.provenance.ThingzoneActivity;

@ProvActivity(type = ThingzoneType.process)
public class PayloadDispatcherProcess implements KafkaStreamingProcess {
    private static final Logger LOG = LoggerFactory.getLogger(PayloadDispatcherProcess.class);
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    @ProvAttribute(attribute = Attribute.AttributeKind.PROV_LABEL)
    protected String name = "Payload Dispatcher";

    private Properties props;
    private final KafkaStreams streams;
    private final Topology topology;
    private PayloadMapperEngine mapperEngine;


    public PayloadDispatcherProcess(PayloadDispatcherConfig config)
            throws Exception {
        props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG,
                config.getString(PayloadDispatcherConstant.APPLICATION_ID_CONFIG));
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG,
                config.getString(PayloadDispatcherConstant.BOOTSTRAP_SERVERS_CONFIG));
        props.put(StreamsConfig.EXACTLY_ONCE, true);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());


        if (!RegistryAPIDriver.isInitialized()) {
            RegistryDriverConfig registryConfig = new RegistryDriverConfig();
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_SCHEME,
                                       config.getString(RegistryConstant.REGISTRY_SERVER_SCHEME));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST,
                                       config.getString(RegistryConstant.REGISTRY_SERVER_HOST));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT,
                                       config.getString(RegistryConstant.REGISTRY_SERVER_PORT));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_USERNAME,
                                       config.getString(RegistryConstant.REGISTRY_SERVER_USERNAME));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD,
                                       config.getString(RegistryConstant.REGISTRY_SERVER_PASSWORD));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS,
                                       config.getString(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS));
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }


        if (!ImagesvrDriver.isInitialized()) {
            ImagesvrDriverConfig imagesvrDriverConfig = new ImagesvrDriverConfig();
            imagesvrDriverConfig.addProperty(ImageServerConstant.IMAGESERVER_SERVER_HOST,
                                             config.getString(ImageServerConstant.IMAGESERVER_SERVER_HOST));
            imagesvrDriverConfig.addProperty(ImageServerConstant.IMAGESERVER_SERVER_SCHEME,
                                             config.getString(ImageServerConstant.IMAGESERVER_SERVER_SCHEME));
            imagesvrDriverConfig.addProperty(ImageServerConstant.IMAGESERVER_SERVER_PORT,
                                             config.getString(ImageServerConstant.IMAGESERVER_SERVER_PORT));
            ImagesvrDriver.setConnectionProperties(imagesvrDriverConfig);
            ImagesvrDriver.globalInitialise();
        }

        mapperEngine = new PayloadMapperEngine(config);


        try {
            mapperEngine.attachMapper(ImageMapper.class.getName());
            mapperEngine.attachMapper(GenericUploadMapper.class.getName());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }


        // Initialise provenance service
        SoftwareAgentContext softwareAgentContext = null;
        if (Boolean.parseBoolean(config.getString(GenericConstant.GENERATE_ACTIVITY_PROVENANCE))
            || Boolean.parseBoolean(config.getString(GenericConstant.GENERATE_AGENT_PROVENANCE))) {
            if (!ProvsvrDriver.isInitialized()) {
                ProvsvrDriverConfig provsvrConfig = new ProvsvrDriverConfig();
                provsvrConfig.addProperty(ProvsvrConstant.PROVSVR_SERVER_SCHEME,
                                          config.getString(ProvsvrConstant.PROVSVR_SERVER_SCHEME));
                provsvrConfig.addProperty(ProvsvrConstant.PROVSVR_SERVER_HOST,
                                          config.getString(ProvsvrConstant.PROVSVR_SERVER_HOST));
                provsvrConfig.addProperty(ProvsvrConstant.PROVSVR_SERVER_PORT,
                                          config.getString(ProvsvrConstant.PROVSVR_SERVER_PORT));
                ProvsvrDriver.setConnectionProperties(provsvrConfig);
                ProvsvrDriver.globalInitialise();
            }

            ProvenanceManager provenanceManager = new ProvenanceManager(config.getString(GenericConstant.APPLICATION_PREFIX));
            softwareAgentContext = new SoftwareAgentContext(provenanceManager);

            softwareAgentContext.register(PayloadDispatcherApp.class, Version.getVersion(), Instant.now());
        }



        final StreamsBuilder builder = new StreamsBuilder();

        KStream<String, String> inputStream = builder.stream(config.getString(PayloadDispatcherConstant.INPUT_TOPIC));

        // Map payloads to timeseries
        KStream<String, MappedPayload> mappedPayloads = inputStream.mapValues(
                (value) -> {
                    ConnectorPayload payload = null;
                    PayloadMapperOutput output;

                    try {
                        // Deserialise message from queue to Connector Payload Object
                        payload = jsonStringSerialiser.readObject(value, ConnectorPayload.class);

                        LOG.info("Processing payload from source: " + payload.getSource());
                        LOG.trace(payload.toString());
                        // Map payload to timeseries
                        output = mapperEngine.flatMapToTimeseries(payload);
                    } catch (Exception e) {
                        LOG.error(e.getMessage());
                        output = new PayloadMapperOutput().setError(new Error(e));
                    }

                    MappedPayload mappedPayload = new MappedPayload();
                    mappedPayload.setPayload(payload);
                    mappedPayload.setOutput(output);
                    return mappedPayload;
                }
        );

        // Push correctly mapped payloads to processing queue
        SoftwareAgentContext finalSoftwareAgentContext = softwareAgentContext;
        mappedPayloads
                .filter((key, mappedPayload) ->
                        (mappedPayload.getOutput().getError() == null &&
                                mappedPayload.getOutput().getTimeseries().size() > 0))
                .peek((key, payload) -> {
                    if (finalSoftwareAgentContext != null) {
                        try {
                           // Thread.sleep(1000);
                            ProvenanceManager pm = finalSoftwareAgentContext.pm;
                            QualifiedName processId = pm.qn(pm.getDefaultPrefix(), "Mapping" + Instant.now().toEpochMilli());
                            ThingzoneActivity ac = new ThingzoneActivity(pm, this, processId);
                            ac.start();

                            ac.used(pm.qn(finalSoftwareAgentContext.pm.getDefaultPrefix(),
                                          payload.getPayload().getId().toString()),
                                    payload.getPayload(), Instant.now());

                            for (AbstractTimeseriesPoint point : payload.getOutput().getTimeseries()) {
                                ac.generated(point, "mapped_output", Instant.now());
                            }

                            ac.associatedWith(pm.qn(pm.getDefaultPrefix(), UUID.randomUUID().toString()),
                                              finalSoftwareAgentContext.getAgent().getId());

                            ac.associatedWith(pm.qn(pm.getDefaultPrefix(), UUID.randomUUID().toString()),
                                              pm.qn(pm.getDefaultPrefix(),"Nominet" ));
                            ac.stop();
                            ac.commitToProvenanceRecord();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .map((key, mappedPayload) -> {
                    try {
                        return new KeyValue<>(key, jsonStringSerialiser.writeObject(mappedPayload.getOutput().getTimeseries()));
                    } catch (Exception e) {
                        e.printStackTrace();
                        LOG.error(e.getMessage());
                        return null;
                    }
                })
                .to(config.getString(PayloadDispatcherConstant.OUTPUT_TOPIC));

        // Push payload with mappings error to deadletter queue
        mappedPayloads
                .filter((key, mappedPayload) -> (mappedPayload.getOutput().getError() != null))
                .mapValues((mappedPayload) -> {
                    try {
                        return jsonStringSerialiser.writeObject(mappedPayload);
                    } catch (Exception e) {
                        LOG.error(e.getMessage());
                        return null;
                    }
                })
                .to(config.getString(PayloadDispatcherConstant.DEADLETTER_TOPIC));

        topology = builder.build();
        streams = new KafkaStreams(topology, props);
    }

    public KafkaStreams getStreams() {
        return streams;
    }

    public Topology getTopology() {
        return topology;
    }
}
