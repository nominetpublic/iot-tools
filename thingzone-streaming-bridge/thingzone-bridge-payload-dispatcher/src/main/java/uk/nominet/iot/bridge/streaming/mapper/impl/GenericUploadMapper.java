/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.mapper.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.LoadingCache;

import uk.nominet.iot.bridge.streaming.config.PayloadDispatcherConfig;
import uk.nominet.iot.bridge.streaming.dto.PayloadMapperOutput;
import uk.nominet.iot.bridge.streaming.mapper.PayloadMapper;
import uk.nominet.iot.model.ConnectorPayload;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;
import uk.nominet.iot.model.timeseries.Base64DataPoint;
import uk.nominet.iot.registry.RegistryAPISession;
import uk.nominet.iot.utils.TimeseriesUtils;

public class GenericUploadMapper extends PayloadMapper {
    private static final Logger LOG = LoggerFactory.getLogger(GenericUploadMapper.class);
    private static LoadingCache<String, Object> deviceCache;
    private static RegistryAPISession api;

    public GenericUploadMapper(PayloadDispatcherConfig config) {
        super(config);
    }

    @Override
    public boolean canDecodePayload(ConnectorPayload payload) {
        boolean canDecode =
                payload.getConnectorContext().getTaskName().startsWith("uk.nominet.iot.bridge.streaming.HttpUploadApp");
        return canDecode;
    }

    @Override
    public PayloadMapperOutput flatMapToTimeseries(ConnectorPayload payload) {
        if (!canDecodePayload(payload)) {
            return new PayloadMapperOutput();
        }

        PayloadMapperOutput output = new PayloadMapperOutput();
        List<AbstractTimeseriesPoint> timeseries = new ArrayList<>();

        AbstractTimeseriesPoint dataPoint = null;

        if (payload.getContentType().equals("application/json")) {

            String decodedString = new String(payload.getData());

            AbstractTimeseriesPoint point = null;

            try {
                point = TimeseriesUtils.decodeFromJsonString(decodedString);
                dataPoint = point;
            } catch (Exception e) {
                LOG.error(e.getMessage());
                e.printStackTrace();
                dataPoint = buildBase64DataPoint(payload);
            }

        } else {
            dataPoint = buildBase64DataPoint(payload);
        }


        if (dataPoint.getId() == null) dataPoint.setId(UUID.randomUUID());
        if (dataPoint.getTimestamp() == null) dataPoint.setTimestamp(payload.getReceivedAt());
        dataPoint.setTimestamp(dataPoint.getTimestamp()); //Compute month partition property

        dataPoint.setStreamKey(payload.getKey());
        if (dataPoint.getMetadata() == null) dataPoint.setMetadata(payload.getMetadata());

        if (dataPoint != null) timeseries.add(dataPoint);


        output.setTimeseries(timeseries);
        return output;
    }

    private Base64DataPoint buildBase64DataPoint(ConnectorPayload payload) {
        Base64DataPoint dataPoint = new Base64DataPoint();
        dataPoint.setContentType(payload.getContentType());
        dataPoint.setMetadata(payload.getMetadata());
        dataPoint.setBase64Data(payload.getData());
        return dataPoint;
    }

}
