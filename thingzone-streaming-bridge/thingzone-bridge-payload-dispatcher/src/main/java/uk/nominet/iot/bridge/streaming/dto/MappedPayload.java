/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.dto;

import uk.nominet.iot.model.ConnectorPayload;

public class MappedPayload {
        ConnectorPayload payload;
        PayloadMapperOutput output;

        public ConnectorPayload getPayload() {
                return payload;
        }

        public void setPayload(ConnectorPayload payload) {
                this.payload = payload;
        }

        public PayloadMapperOutput getOutput() {
                return output;
        }

        public void setOutput(PayloadMapperOutput output) {
                this.output = output;
        }

}
