/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.config;

public class PayloadDispatcherConstant {
    public static final String APPLICATION_ID_CONFIG = "bridge.dispatcher.applicationId";
    public static final String BOOTSTRAP_SERVERS_CONFIG = "bridge.dispatcher.bootstrapServers";
    public static final String INPUT_TOPIC = "bridge.dispatcher.inputTopic";
    public static final String OUTPUT_TOPIC = "bridge.dispatcher.outputTopic";
    public static final String DEADLETTER_TOPIC = "bridge.dispatcher.deadletterTopic";
    public static final String PAYLOAD_MAPPERS = "bridge.dispatcher.payloadMappers";
    public static final String STORE_RAW_INPUT_MESSAGES = "bridge.dispatcher.storeRawMessages";
}
