/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.mapper;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nominet.iot.bridge.streaming.config.PayloadDispatcherConfig;
import uk.nominet.iot.bridge.streaming.dto.PayloadMapperOutput;
import uk.nominet.iot.model.ConnectorPayload;

public class PayloadMapperEngine {
    private static final Logger LOG = LoggerFactory.getLogger(PayloadMapperEngine.class);
    private HashMap<String, PayloadMapper> mappers = new HashMap<>();

    PayloadDispatcherConfig config;

    public PayloadMapperEngine(PayloadDispatcherConfig config) {
        this.config = config;
    }

    public void attachMapper(String payloadMapperClassName)
            throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Class<PayloadMapper> mapperClass = (Class<PayloadMapper>) Class.forName(payloadMapperClassName);

        Class[] types = {PayloadDispatcherConfig.class};
        Constructor constructor = mapperClass.getConstructor(types);

        Object[] parameters = {config};

        PayloadMapper mapper = (PayloadMapper)constructor.newInstance(parameters);
        mappers.put(payloadMapperClassName, mapper);
    }

    public PayloadMapperOutput flatMapToTimeseries(ConnectorPayload payload) {
        for (PayloadMapper mapper : mappers.values()) {
            PayloadMapperOutput output = mapper.flatMapToTimeseries(payload);
            if ( output.getTimeseries() != null || output.getError() != null) {
                return output;
            }
        }

        //catch all
        PayloadMapperOutput flatMappedOutput = new PayloadMapperOutput();
        flatMappedOutput.setError(new Error("Could not apply a payload mapper"));
        return flatMappedOutput;
    }

}
