/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.mapper.impl;

import org.junit.Test;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

import static org.junit.Assert.*;

public class ImageMapperTest {

    @Test
    public void exifDatatimeConverts() {
        final DateTimeFormatter FMT = new DateTimeFormatterBuilder()
                .appendPattern("yyyy:MM:dd HH:mm:ss")
                .toFormatter()
                .withZone(ZoneId.systemDefault());
        Instant instant = FMT.parse("2020:03:30 16:24:19", Instant::from);
        System.out.println(instant);


    }
}
