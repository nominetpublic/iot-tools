/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.mapper;

import uk.nominet.iot.bridge.streaming.config.PayloadDispatcherConfig;
import uk.nominet.iot.bridge.streaming.dto.PayloadMapperOutput;
import uk.nominet.iot.decoder.Decode;
import uk.nominet.iot.model.ConnectorPayload;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;
import uk.nominet.iot.model.timeseries.MetricDouble;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TestMapperJson extends PayloadMapper {

    public TestMapperJson(PayloadDispatcherConfig config) {
        super(config);
    }

    @Override
    public boolean canDecodePayload(ConnectorPayload payload) {
        return payload.getConnectorContext().getTaskName().equals("test.connector.Task") &&
                payload.getContentType().equals("application/json");
    }

    @Override
    public PayloadMapperOutput flatMapToTimeseries(ConnectorPayload payload) {
        if (!canDecodePayload(payload)) {
            return new PayloadMapperOutput();
        }

        PayloadMapperOutput output = new PayloadMapperOutput();

        JSONObject json = null;
        try {
            json = Decode
                    .forContentType(payload.getContentType())
                    .fromBytes(payload.getData())
                    .toJsonObject();
        } catch (Exception e) {
            output.setError(new Error(e));
            return output;
        }

        List<AbstractTimeseriesPoint> timeseries = new ArrayList<>();

        JSONArray values =  json.getJSONArray("values");

        for (int i = 0; i < values.length(); i++) {
            MetricDouble point = new MetricDouble();
            point.setId(UUID.randomUUID());
            point.setStreamKey("stream-" + i);
            point.setValue(values.getDouble(i));
            point.setTimestamp(payload.getReceivedAt());
            timeseries.add(point);
        }

        output.setTimeseries(timeseries);

        return output;
    }
}
