/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.mapper;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static junit.framework.TestCase.assertTrue;

import java.time.Instant;

import org.junit.Before;
import org.junit.Test;

import uk.nominet.iot.bridge.streaming.dto.PayloadMapperOutput;
import uk.nominet.iot.model.ConnectorPayload;
import uk.nominet.iot.model.ThingzoneConnectorContext;
import uk.nominet.iot.model.timeseries.MetricDouble;

public class PayloadMapperEngineTest {
    PayloadMapperEngine engine;

    @Before
    public void setUp() throws Exception {
        engine = new PayloadMapperEngine(null);
        engine.attachMapper(TestMapperString.class.getName());
        engine.attachMapper(TestMapperJson.class.getName());
    }

    @Test
    public void payloadStringGetMapped() throws Exception {
        ConnectorPayload payload = new ConnectorPayload();
        payload.setContentType("text/plain");
        ThingzoneConnectorContext connectorContext = new ThingzoneConnectorContext();
        connectorContext.setTaskVersion("1.0");
        connectorContext.setTaskName("test.connector.Task");
        payload.setConnectorContext(connectorContext);
        payload.setData("1,2,3".getBytes());
        payload.setReceivedAt(Instant.now());

        PayloadMapperOutput output = engine.flatMapToTimeseries(payload);

        assertNull(output.getError());
        assertNotNull(output.getTimeseries());
        assertEquals(3, output.getTimeseries().size());

        output.getTimeseries().forEach(point -> {
            assertEquals(MetricDouble.class, point.getClass());
            assertTrue(((MetricDouble)point).getValue() > 0);
        });
    }

    @Test
    public void payloadJsonGetMapped() throws Exception {
        ConnectorPayload payload = new ConnectorPayload();
        payload.setContentType("application/json");
        ThingzoneConnectorContext connectorContext = new ThingzoneConnectorContext();
        connectorContext.setTaskVersion("1.0");
        connectorContext.setTaskName("test.connector.Task");
        payload.setConnectorContext(connectorContext);
        payload.setData("{values: [1,2,3]}".getBytes());
        payload.setReceivedAt(Instant.now());

        PayloadMapperOutput output = engine.flatMapToTimeseries(payload);

        assertNull(output.getError());
        assertNotNull(output.getTimeseries());
        assertEquals(3, output.getTimeseries().size());

        output.getTimeseries().forEach(point -> {
            assertEquals(MetricDouble.class, point.getClass());
            assertTrue(((MetricDouble)point).getValue() > 0);
        });
    }

    @Test
    public void payloadDontMatchMappers() throws Exception {
        ConnectorPayload payload = new ConnectorPayload();
        payload.setContentType("random/content");
        ThingzoneConnectorContext connectorContext = new ThingzoneConnectorContext();
        connectorContext.setTaskVersion("1.0");
        connectorContext.setTaskName("test.connector.Task");
        payload.setConnectorContext(connectorContext);
        payload.setData("1,2,3".getBytes());
        payload.setReceivedAt(Instant.now());

        PayloadMapperOutput output = engine.flatMapToTimeseries(payload);

        assertNull(output.getTimeseries());
        assertEquals("Could not apply a payload mapper", output.getError().getMessage());
    }
}
