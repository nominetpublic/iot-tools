/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.kafka;

import com.google.common.collect.EvictingQueue;
import com.google.common.collect.Lists;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import spark.Request;
import spark.Response;
import spark.Service;
import uk.nominet.iot.http.CORSEnabler;
import uk.nominet.iot.json.JsonStringSerialiser;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static spark.Service.ignite;

public class QueueObserver {
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
    public static final int MAX_MESSAGES_PER_TOPIC = 30;
    private Map<String, EvictingQueue<ConsumerRecord<String, String>>> messages;
    private List<String> topics;
    private Service http;


    public QueueObserver(List<String> topics) {
        this.messages = new HashMap<>();
        this.topics = topics;
    }


    public void runKafkaClient(String bootstrapServers) {

        new Thread(() -> {
            Properties props = new Properties();
            props.put("bootstrap.servers", bootstrapServers);
            props.put("group.id", "observerClient");
            props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
            props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

            KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
            consumer.subscribe(topics);

            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(100);
                for (ConsumerRecord<String, String> record : records) {
                    try {
                        String topic = record.topic();

                        if (!messages.containsKey(topic)) {
                            messages.put(topic, EvictingQueue.create(MAX_MESSAGES_PER_TOPIC));
                        }
                        messages.get(topic).add(record);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        }).start();

    }

    public void runQueueMonitoring( String bootstrapServers) {
        new Thread(() -> {

            AdminClient adminClient;
            Map<String, Long> topicEnds = new HashMap<>();
            Map<String, Long> offsetsEnds = new HashMap<>();

            Properties props = new Properties();
            props.put("bootstrap.servers", bootstrapServers);
            props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
            props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

            Map<String, List<PartitionInfo>> topics;

            KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(props);

            while (!Thread.currentThread().isInterrupted()) {
                topics = consumer.listTopics();

                topics.keySet().forEach(topic -> {
                    if (!topic.equals("__consumer_offsets")) {

                        List<TopicPartition> partitions = consumer.partitionsFor(topic).stream()
                                                                  .map(p -> new TopicPartition(topic, p.partition()))
                                                                  .collect(Collectors.toList());
                        consumer.assign(partitions);
                        consumer.seekToEnd(Collections.emptySet());

                        Map<TopicPartition, Long> endPartitions = partitions.stream()
                                                                            .collect(Collectors.toMap(Function.identity(), consumer::position));
                        consumer.seekToBeginning(Collections.emptySet());
                        //long sum = partitions.stream().mapToLong(p -> endPartitions.get(p) - consumer.position(p)).sum();
                        long maxEndPartition = partitions.stream().mapToLong(p -> endPartitions.get(p)).max().orElse(0);

                        topicEnds.put(topic, maxEndPartition);
                    }
                });



                topicEnds.forEach((key, value) -> System.out.println("topic:" + key + "  value: " + value));
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }).start();
    }

    public void runAPI() {
        http = ignite()
                       .port(8087)
                       .ipAddress("0.0.0.0")
                       .threadPool(20);

        http.staticFileLocation("/www");

        // Attach security components
        new CORSEnabler("*",
                        "GET,PUT,POST,DELETE,OPTIONS",
                        "Content-Type,Authorization,X-Requested-With," +
                        "Content-Length,Accept,Origin,X-Introspective-Session-Key")
                .attachTo(http);

        http.before("/*", (request, response) -> response.type("application/json"));

        http.get("/topics", (req, res) -> getTopics(req, res));
        http.get("/topics/:topic", (req, res) -> getTopicMessages(req, res));
    }

    private String getTopicMessages(Request req, Response res) {

        JsonArray response = new JsonArray();
        String topic = req.params(":topic");
        if (messages.containsKey(topic)) {
            EvictingQueue<ConsumerRecord<String, String>> records = messages.get(topic);

            List<JsonObject> msg = records.stream()
                                          .map(record -> {
                                              JsonObject item = new JsonObject();
                                              item.addProperty("offset", record.offset());
                                              item.addProperty("partition", record.partition());
                                              item.addProperty("key", record.key());
                                              item.addProperty("timestamp", record.timestamp());
                                              item.addProperty("value", record.value());
                                              return item;
                                          }).collect(Collectors.toList());

            response = jsonStringSerialiser.getDefaultSerialiser().toJsonTree(Lists.reverse(msg)).getAsJsonArray();

        }

        return response.toString();
    }

    private String getTopics(Request req, Response res) {
        JsonObject response = new JsonObject();

        for (Map.Entry<String, EvictingQueue<ConsumerRecord<String, String>>> entry : messages.entrySet()) {
            String topic = entry.getKey();
            EvictingQueue<ConsumerRecord<String, String>> queue = entry.getValue();

            response.addProperty(topic, queue.size());
        }

        return response.toString();
    }

    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            System.out.println("Usage: QueueObserver  bootstrapServers topic1,topic2,topic3,topic4");
            System.exit(1);
        }

        String bootstrapServers = args[0];
        String[] topics = args[1].split(",");

        System.out.println("--------------------------------------------------------------------------------");
        System.out.println(" STARTING KAFKA QUEUE OBSERVER");
        System.out.println("--------------------------------------------------------------------------------");
        System.out.println("Bootstrap servers: " + bootstrapServers);
        System.out.println("Topics: " + topics);
        System.out.println("--------------------------------------------------------------------------------");

        QueueObserver queueObserver = new QueueObserver(Arrays.asList(topics));

        queueObserver.runKafkaClient(bootstrapServers);
        queueObserver.runQueueMonitoring(bootstrapServers);
        queueObserver.runAPI();
    }
}
