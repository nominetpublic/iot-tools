/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming;

import io.searchbox.client.JestClient;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.error.DataStorageException;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.manager.registry.*;
import uk.nominet.iot.model.registry.RegistryDataStream;
import uk.nominet.iot.model.registry.RegistryDevice;
import uk.nominet.iot.model.registry.RegistryEntity;
import uk.nominet.iot.model.registry.RegistryTransform;
import java.util.ArrayList;

public class StreamBatchProcessor {
    private JestClient esClient;
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
    private RegistryDeviceManager deviceManager;
    private RegistryDataStreamManager streamManager;
    private RegistryTransformManager transformManager;

    public StreamBatchProcessor() throws DataStorageException {
        this.esClient = ElasticsearchDriver.getClient();
        this.deviceManager = RegistryDeviceManager.createManager();
        this.streamManager = RegistryDataStreamManager.createManager();
        this.transformManager = RegistryTransformManager.createManager();
    }

    public void processBatch(ArrayList<RegistryEntity> batch) throws Exception {
        for (RegistryEntity item : batch) {
            if (item instanceof RegistryDevice) {
                RegistryDevice device = (RegistryDevice) item;

                RegistryDeviceHandler deviceItem = deviceManager.device(device.getKey());
                deviceItem.index(device);
            } else if (item instanceof RegistryDataStream) {
                try {
                    RegistryDataStream stream = (RegistryDataStream) item;
                    RegistryDataStreamHandler dataStreamItem = streamManager.stream(stream.getKey());
                    dataStreamItem.index();
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            } else if (item instanceof RegistryTransform) {
                try {
                    RegistryTransform transform = (RegistryTransform) item;
                    RegistryTransformHandler transformItem = transformManager.transform(transform.getKey());
                    transformItem.index();
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
