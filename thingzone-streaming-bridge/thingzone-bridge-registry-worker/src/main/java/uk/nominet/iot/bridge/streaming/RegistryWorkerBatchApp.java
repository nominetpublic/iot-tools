/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.ConfigurationConverter;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.BasicConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;

import uk.nominet.iot.bridge.streaming.config.RegistryWorkerConfig;
import uk.nominet.iot.bridge.streaming.config.RegistryWorkerConstant;
import uk.nominet.iot.config.ElasticsearchDriverConfig;
import uk.nominet.iot.config.RegistryDriverConfig;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.model.registry.RegistryEntity;
import uk.nominet.iot.registry.stream.StreamBatchHandler;

public class RegistryWorkerBatchApp {
    private static final Logger LOG = LoggerFactory.getLogger(RegistryWorkerBatchApp.class);
    private static final int MAX_WAITING_CYCLES = 10;
    private RegistryWorkerConfig config;

    public RegistryWorkerBatchApp(InputStream configInputStream) throws ConfigurationException, IOException {

        Parameters params = new Parameters();
        BasicConfigurationBuilder<PropertiesConfiguration> builder = new BasicConfigurationBuilder<>(PropertiesConfiguration.class).configure(params.basic());

        Configuration config = builder.getConfiguration();
        ((PropertiesConfiguration) config).read(new InputStreamReader(configInputStream));

        this.config = new RegistryWorkerConfig(Maps.fromProperties(ConfigurationConverter.getProperties(config)));

        RegistryDriverConfig registryConfig;
        ElasticsearchDriverConfig elasticsearchConfig;

        if (!RegistryAPIDriver.isInitialized()) {
            registryConfig = new RegistryDriverConfig();
            registryConfig.append(config);
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }

        if (!ElasticsearchDriver.isInitialized()) {
            elasticsearchConfig = new ElasticsearchDriverConfig();
            elasticsearchConfig.append(config);
            ElasticsearchDriver.setConnectionProperties(elasticsearchConfig);
            ElasticsearchDriver.globalInitialise();
        }
    }

    public void run() throws Exception {
        int waitingCycles = 0;
        int bufferSize = config.getInt(RegistryWorkerConstant.REGISTRY_DUMP_BUFFER);
        System.out.println("Buffer size: " + bufferSize);
        StreamBatchHandler dumpAllBatches = RegistryAPIDriver.getSession().dump.all(bufferSize);
        StreamBatchProcessor streamBatchProcessor = new StreamBatchProcessor();

        Queue<ArrayList<RegistryEntity>> batchJobsQueue = new ConcurrentLinkedQueue<>();

        new Thread(() -> {
            try {
                dumpAllBatches.forEachBatch((batch, sequence) -> {
                    LOG.debug("Processing batch " + (sequence++) + " size: " + batch.size());
                    batchJobsQueue.offer(batch);
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();

        while (true) {
            if (batchJobsQueue.isEmpty()) {
                if (waitingCycles >= MAX_WAITING_CYCLES)
                    break;

                System.out.println("Waiting two seconds");
                waitingCycles++;
                Thread.sleep(2000);
                continue;
            }
            ArrayList<RegistryEntity> batch = batchJobsQueue.remove();

            if (batch != null)
                streamBatchProcessor.processBatch(batch);
        }
    }

    public static void main(String[] args) throws Exception {
        LOG.info("Start Storage Worker Streaming service");

        if (args.length != 1) {
            System.out.println("Usage: RegistryWorkerBatchApp configFile");
            System.exit(1);
        }

        RegistryWorkerBatchApp registryWorkerBatchApp = new RegistryWorkerBatchApp(new FileInputStream(args[0]));
        registryWorkerBatchApp.run();
    }
}
