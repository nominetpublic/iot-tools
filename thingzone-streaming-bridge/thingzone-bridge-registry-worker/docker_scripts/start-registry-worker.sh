#!/usr/bin/env bash
# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

CLASSPATH=/thingzone/thingzone-bridge-registry-worker.jar
JAVA_OPTIONS="-Dlog4j.configuration=file:/thingzone/log4j.xml"
JAVA_CLASS=uk.nominet.iot.bridge.streaming.RegistryWorkerBatchApp
CONFIG_FILE=/thingzone/config/thingzone-bridge-registry-worker.properties

# clean logs
rm -f /thingzone/logs/registry-worker.log

sleep 20

while :
do
    /usr/bin/java $JAVA_OPTIONS -cp ${CLASSPATH} $JAVA_CLASS $CONFIG_FILE
	sleep 180
done
