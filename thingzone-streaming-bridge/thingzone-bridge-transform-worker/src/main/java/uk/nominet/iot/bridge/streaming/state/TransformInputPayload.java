/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.state;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.Instant;

public class TransformInputPayload implements Serializable {
    private String streamKey;
    private Instant timestamp;
    private boolean pending;
    private JsonObject value;

    public TransformInputPayload(String streamKey, Instant timestamp) {
        this.streamKey = streamKey;
        this.timestamp = timestamp;
        pending = true;
    }

    public TransformInputPayload(String streamKey, Instant timestamp, JsonObject value) {
        this.streamKey = streamKey;
        this.timestamp = timestamp;
        this.value = value;
        this.pending = false;
    }

    public void updateValue(Instant timestamp, JsonObject value) {
        if (timestamp.isAfter(this.timestamp) || timestamp.equals(this.timestamp)) {
            this.timestamp = timestamp;
            this.value = value;
            this.pending = false;
        }
    }

    public void updateValue(TransformInputPayload payload) {
        updateValue(payload.getTimestamp(), payload.getValue());
    }

    public boolean isPending() {
        return pending;
    }

    public void setPending(boolean pending) {
        this.pending = pending;
    }

    public JsonObject getValue() {
        return value;
    }

    public Instant getTimestamp() {
        return timestamp;
    }


    private void readObject(ObjectInputStream aInputStream) throws ClassNotFoundException, IOException
    {
        streamKey = aInputStream.readUTF();
        timestamp = Instant.ofEpochMilli(aInputStream.readLong());
        pending = aInputStream.readBoolean();
        value = new Gson().fromJson(aInputStream.readUTF(), JsonObject.class);
    }

    private void writeObject(ObjectOutputStream aOutputStream) throws IOException
    {
        aOutputStream.writeUTF(streamKey);
        aOutputStream.writeLong(timestamp.toEpochMilli());
        aOutputStream.writeBoolean(pending);
        aOutputStream.writeUTF(value.toString());
    }


    @Override
    public String toString() {
        return "TransformInputPayload{" +
                "timestamp=" + timestamp +
                ", pending=" + pending +
                ", value=" + ((value == null) ? "null" : value.toString()) +
                '}';
    }
}
