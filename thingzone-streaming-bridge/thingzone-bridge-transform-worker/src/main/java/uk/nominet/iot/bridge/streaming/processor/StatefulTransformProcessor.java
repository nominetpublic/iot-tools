/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.processor;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import uk.nominet.iot.bridge.streaming.state.PendingTransform;
import uk.nominet.iot.bridge.streaming.state.StateStore;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;
import uk.nominet.iot.model.registry.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StatefulTransformProcessor {
    private static final Logger LOG = LoggerFactory.getLogger(StatefulTransformProcessor.class);
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    private long windowSize;
    private TemporalUnit temporalUnit;

    private StateStore stateStore;

    public StatefulTransformProcessor(long windowSize, TemporalUnit temporalUnit) {
        this.windowSize = windowSize;
        this.temporalUnit = temporalUnit;
        this.stateStore = new StateStore(windowSize, temporalUnit);
    }


    /**
     * Process a list of timeseries points
     * @param points a json array of timeseries points
     * @return a list of transforms awaiting execution
     */
    public synchronized List<PendingTransform> processTimeseriesPoints(JsonArray points) throws Exception {
        Instant triggerTimestap = null;

        List<RegistryDependentTransform> triggeredTransforms = new ArrayList<>();

        for (JsonElement point : points) {
            // Deserialise point to abstract timeseries point
            AbstractTimeseriesPoint abstractTimeseriesPoint =
                    jsonStringSerialiser.readObject(point.toString(), AbstractTimeseriesPoint.class);
            String streamKey = abstractTimeseriesPoint.getStreamKey();

            // Check if any transform depends on the timeseries point
            List<RegistryDependentTransform> dependents =
                    RegistryAPIDriver.getSession()
                            .internal
                            .transformDependentOnStream(streamKey);


            // Extract list of triggers streams from the dependent transforms
            List<RegistryDependentTransform> triggers =
                    dependents.stream().filter(RegistryDependentTransform::isTrigger).collect(Collectors.toList());

            if (triggers.size() > 0) {
                triggeredTransforms.addAll(triggers);
                triggerTimestap = abstractTimeseriesPoint.getTimestamp();
            }

            if (dependents.size() > 0) {
                stateStore.addPayload(streamKey,
                                      abstractTimeseriesPoint.getTimestamp(),
                                      point.getAsJsonObject());
            }
        }


        for (RegistryDependentTransform triggeredTransform : triggeredTransforms) {

            List<RegistryTransformSource> sources = triggeredTransform
                                                            .getTransform()
                                                            .getSources();
            RegistryTransformFunction function = triggeredTransform.getTransform().getFunction();

            List<String> outputAliases = null;
            if (triggeredTransform.getTransform().getOutputStreams() != null) {
                outputAliases = triggeredTransform.getTransform()
                                                  .getOutputStreams()
                                                  .stream()
                                                  .map(RegistryTransformOutputStream::getAlias)
                                                  .collect(Collectors.toList());
            }

            stateStore.addTransform(
                    triggeredTransform.getTransform().getKey(),
                    function,
                    triggerTimestap,
                    sources,
                    triggeredTransform.getTransform().getParameterValues(),
                    outputAliases);
        }

        return stateStore.getTransformsReadyForExecution();

    }


    public synchronized void transformExecutionPromise(PendingTransform transform) throws Exception {
        List<RegistryDependentTransform> dependents = RegistryAPIDriver
                                                                    .getSession()
                                                                    .internal
                                                                    .transformDependentOnStream(transform.getTransformKey());

        boolean hasDependants = dependents.size() > 0;
        stateStore.transformExecutionPromise(transform, hasDependants);
    }

    public synchronized StateStore getStateStore() {
        return stateStore;
    }
}
