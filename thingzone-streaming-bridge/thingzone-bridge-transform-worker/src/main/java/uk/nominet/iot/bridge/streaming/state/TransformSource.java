/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.state;

import java.io.Serializable;

public class TransformSource implements Serializable {
    private String alias;
    private String streamKey;
    private TransformInputPayload payload;
    private String aggregationType;
    private String aggregationSpec;

    public TransformSource(String alias, String streamKey, TransformInputPayload payload, String aggregationType, String aggregationSpec) {
        this.alias = alias;
        this.streamKey = streamKey;
        this.payload = payload;
        this.aggregationType = aggregationType;
        this.aggregationSpec = aggregationSpec;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public TransformInputPayload getPayload() {
        return payload;
    }

    public void setPayload(TransformInputPayload payload) {
        this.payload = payload;
    }

    public String getAggregationType() {
        return aggregationType;
    }

    public void setAggregationType(String aggregationType) {
        this.aggregationType = aggregationType;
    }

    public String getAggregationSpec() {
        return aggregationSpec;
    }

    public void setAggregationSpec(String aggregationSpec) {
        this.aggregationSpec = aggregationSpec;
    }

    public String getStreamKey() {
        return streamKey;
    }

    public void setStreamKey(String streamKey) {
        this.streamKey = streamKey;
    }

    @Override
    public String toString() {
        return "TransformSource{" +
               "alias='" + alias + '\'' +
               ", payload=" + payload +
               '}';
    }
}
