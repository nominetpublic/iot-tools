/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming;

import com.google.common.collect.Maps;
import uk.nominet.iot.bridge.config.ConfigurationLoader;
import uk.nominet.iot.bridge.streaming.config.TransformWorkerConfig;
import uk.nominet.iot.bridge.streaming.processor.StatefulTransformProcessor;
import uk.nominet.iot.bridge.streaming.scheduler.ScheduledTransformExecutor;
import uk.nominet.iot.config.*;
import uk.nominet.iot.driver.CassandraDriver;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.driver.RegistryAPIDriver;
import org.apache.kafka.common.config.ConfigException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.temporal.ChronoUnit;
import java.util.Properties;

public class TransformWorkerApp {
    private static final Logger LOG = LoggerFactory.getLogger(TransformWorkerApp.class);

    public static void main(String[] args) throws Exception {
        LOG.info("Start Storage Worker Streaming service");

        if (args.length != 1) {
            System.out.println("Usage: StorageWorkerApp configFile");
            System.exit(1);
        }

        Properties configProp = ConfigurationLoader.fromFileName(args[0]);

        TransformWorkerConfig config;
        try {
            config = new TransformWorkerConfig(Maps.fromProperties(configProp));
        } catch (ConfigException e) {
            LOG.error(e.getMessage());
            throw new Exception("Couldn't start Storage Worker due to config error", e);
        }

        if (!CassandraDriver.isInitialized()) {
            CassandraDriverConfig cassandraConfig = new CassandraDriverConfig();
            cassandraConfig.addProperty(CassandraConstant.CASSANDRA_SERVER_HOST,
                                        config.getString(CassandraConstant.CASSANDRA_SERVER_HOST));
            cassandraConfig.addProperty(CassandraConstant.CASSANDRA_SERVER_PORT,
                                        config.getString(CassandraConstant.CASSANDRA_SERVER_PORT));
            CassandraDriver.setConnectionProperties(cassandraConfig);
            CassandraDriver.globalInitialise();
        }

        if (!RegistryAPIDriver.isInitialized()){
            RegistryDriverConfig registryConfig = new RegistryDriverConfig();
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_SCHEME,
                    config.getString(RegistryConstant.REGISTRY_SERVER_SCHEME));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST,
                    config.getString(RegistryConstant.REGISTRY_SERVER_HOST));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT,
                    config.getString(RegistryConstant.REGISTRY_SERVER_PORT));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_USERNAME,
                    config.getString(RegistryConstant.REGISTRY_SERVER_USERNAME));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD,
                    config.getString(RegistryConstant.REGISTRY_SERVER_PASSWORD));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS,
                    config.getString(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS));
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }

        if (!ElasticsearchDriver.isInitialized()) {
            ElasticsearchDriverConfig elasticsearchConfig = new ElasticsearchDriverConfig();
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_SCHEME,
                    config.getString(ElasticsearchConstant.ELASTICSEARCH_SERVER_SCHEME));
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_HOST,
                    config.getString(ElasticsearchConstant.ELASTICSEARCH_SERVER_HOST));
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_PORT,
                    config.getString(ElasticsearchConstant.ELASTICSEARCH_SERVER_PORT));
            ElasticsearchDriver.setConnectionProperties(elasticsearchConfig);
            ElasticsearchDriver.globalInitialise();
        }


        // The transform processor contains the global state of the transform process
        StatefulTransformProcessor transformProcessor = new StatefulTransformProcessor(1, ChronoUnit.DAYS);


        ScheduledTransformExecutor scheduledTransformExecutor = new ScheduledTransformExecutor(config).start();

        // Initialise and run the state builder streaming process
        TransformWorkerProcess transformWorkerProcess = new TransformWorkerProcess(config, transformProcessor);
        KafkaStreamingProcessExecutor transformWorkerProcessExecutor =
                new KafkaStreamingProcessExecutor(transformWorkerProcess);
        transformWorkerProcessExecutor.start();

    }
}
