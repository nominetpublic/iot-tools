/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.state;

import java.io.Serializable;
import java.time.Instant;
import java.util.TreeMap;


public class TransformState implements Serializable {
    private String transformKey;
    private TreeMap<Instant, PendingTransform> transforms;

    public TransformState(String transformKey) {
        this.transformKey = transformKey;
        transforms = new TreeMap<>();
    }

    public void addTransform(Instant triggerTimestamp, PendingTransform transform) {
        if (!transforms.containsKey(triggerTimestamp)) {
            transforms.put(triggerTimestamp, transform);
        }
    }

    public TreeMap<Instant, PendingTransform> getTransforms() {
        return transforms;
    }

    @Override
    public String toString() {
        return "TransformState{" +
                "transformKey='" + transformKey + '\'' +
                ", transforms=" + transforms +
                '}';
    }
}
