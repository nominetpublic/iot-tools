/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.scheduler;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import uk.nominet.iot.bridge.streaming.aggregation.StreamDataAggregator;
import uk.nominet.iot.bridge.streaming.config.TransformWorkerConfig;
import uk.nominet.iot.bridge.streaming.config.TransformWorkerConstant;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.api.ExceptionHandler;
import uk.nominet.iot.model.registry.RegistryTransform;
import uk.nominet.iot.model.registry.RegistryTransformSource;
import uk.nominet.iot.model.script.ExecutableScript;
import uk.nominet.iot.model.script.ExecutionTask;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class RunnableTransforms implements Runnable {
    private static Map<String, Instant> latestInvocations = new HashMap<>();
    static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    private KafkaProducer<String, String> producer;
    private TransformWorkerConfig config;
    private String outputTopic;

    public RunnableTransforms(TransformWorkerConfig config) {
        this.config = config;

        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
                  config.getString(TransformWorkerConstant.BOOTSTRAP_SERVERS_CONFIG));
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "ThingzoneHttpUpload");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                  StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                  StringSerializer.class.getName());
        producer = new KafkaProducer<>(props);

        outputTopic = config.getString(TransformWorkerConstant.OUTPUT_TOPIC);

    }


    @Override
    public void run() {
        List<RegistryTransform> scheduledTransforms = null;
        try {
            scheduledTransforms = RegistryAPIDriver.getSession().internal.transformWithRunSchedule();
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (RegistryTransform transform : scheduledTransforms) {
            try {
                if (!latestInvocations.containsKey(transform.getKey())) {
                    latestInvocations.put(transform.getKey(), Instant.now());
                    runTransform(transform);
                } else {

                    Duration duration = Duration.parse(transform.getRunSchedule());
                    Instant now = Instant.now();
                    Instant last = latestInvocations.get(transform.getKey());
                    long difference = Duration.between(last, now).getSeconds();

                    if (difference >= duration.getSeconds()) {
                        runTransform(transform);
                        latestInvocations.put(transform.getKey(), Instant.now());
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    /**
     * @param transform
     */
    private void runTransform(RegistryTransform transform) throws Exception {
        List<String> outputAliases = null;
        if (transform.getOutputStreams() != null) {
            outputAliases = transform.getOutputStreams()
                                     .stream()
                                     .map(output -> output.getAlias())
                                     .collect(Collectors.toList());
        }

        Map<String, Object> sources = new HashMap<>();

        for (RegistryTransformSource source : transform.getSources()) {
            if (source.getAggregationSpec() != null && source.getAggregationType() != null) {
                StreamDataAggregator streamDataAggregator = new StreamDataAggregator(source.getSource());
                sources.put(source.getAlias(),
                            streamDataAggregator.aggregate(source.getAggregationType(), source.getAggregationSpec()));
            } else {
                throw new Exception("Scheduled transfroms must use inputs with aggregation specs");
            }
        }


        ExecutionTask task = new ExecutionTask();
        task.setTransformKey(transform.getKey());
        task.setParameters(transform.getParameterValues());
        task.setOutputAliases(outputAliases);
        ExecutableScript script = new ExecutableScript();
        script.setName(transform.getFunction().getName());
        script.setType(transform.getFunction().getFunctionType());
        script.setCode(transform.getFunction().getFunctionContent());
        task.setScript(script);
        task.setSources(sources);


        final ProducerRecord<String, String> record = new ProducerRecord<>(outputTopic, task.getTransformKey(),
                                          jsonStringSerialiser.writeObject(task));

        RecordMetadata metadata = producer.send(record).get();

//        System.out.println("--------------------------------------------------------------------------------");
//        System.out.println(task);
//        System.out.println("--------------------------------------------------------------------------------");
//        System.out.println(metadata);
//        System.out.println("--------------------------------------------------------------------------------");


    }
}
