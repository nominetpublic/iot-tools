/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.aggregation;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import org.scijava.parse.ExpressionParser;
import org.scijava.parse.SyntaxTree;
import uk.nominet.iot.error.DataStorageException;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.manager.ThingzoneTimeseriesHandler;
import uk.nominet.iot.manager.ThingzoneTimeseriesManager;

import java.time.Instant;

public class StreamDataAggregator {
    private ThingzoneTimeseriesHandler thingzoneTimeseriesHandler;
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    public StreamDataAggregator(String datsStream) throws Exception {
        ThingzoneTimeseriesManager timeseriesManager = ThingzoneTimeseriesManager.createManager();
        thingzoneTimeseriesHandler = timeseriesManager.timeseries(datsStream);
    }

    /**
     *
     * @param type
     * @param spec
     * @return
     * @throws DataStorageException
     */
    public JsonElement aggregate(String type, String spec) throws DataStorageException {
        if (type.equals("basic")) {
            SyntaxTree syntaxTree = new ExpressionParser().parseTree(spec);

            if (syntaxTree.token().getClass() != org.scijava.parse.Function.class) {
                throw new RuntimeException("Aggregation expression should be a function");
            }

            String functionName = syntaxTree.child(0).token().toString();

            switch (functionName) {
                case "lastN":
                    return lastN((Integer) syntaxTree.child(1).child(0).token());
                case "lastT":
                    return lastT((String) syntaxTree.child(1).child(0).token());
                default:
                    throw new RuntimeException("Aggregation function not recognised: " + functionName);
            }
        } else  throw new RuntimeException("Aggregation type not recognised: " + type);

    }

    /**
     *
     * @param timestamp
     * @return
     * @throws DataStorageException
     */
    private JsonElement lastT(String timestamp) throws DataStorageException {
        System.out.println("Calling lastT with parameter " + timestamp);
        Instant from = Instant.parse(timestamp);

        JsonArray points = jsonStringSerialiser.getDefaultSerialiser().toJsonTree(thingzoneTimeseriesHandler.find(from, Instant.now())).getAsJsonArray();
        return (points.size() == 1) ? points.get(0) : points;
    }

    /**
     *
     * @param number
     * @return
     * @throws DataStorageException
     */
    private JsonElement lastN(int number) throws DataStorageException {
        JsonArray points = jsonStringSerialiser.getDefaultSerialiser().toJsonTree(thingzoneTimeseriesHandler.findLatestN(number)).getAsJsonArray();
        return (points.size() == 1) ? points.get(0) : points;
    }

}
