/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming;

import uk.nominet.iot.bridge.streaming.config.TransformWorkerConfig;
import uk.nominet.iot.bridge.streaming.config.TransformWorkerConstant;
import uk.nominet.iot.bridge.streaming.processor.StatefulTransformProcessor;
import uk.nominet.iot.bridge.streaming.task.DataStreamTransformTask;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class TransformWorkerProcess implements KafkaStreamingProcess {
    private static final Logger LOG = LoggerFactory.getLogger(TransformWorkerProcess.class);

    private  KafkaStreams streams;
    private final Topology topology;

    TransformWorkerProcess(TransformWorkerConfig config, StatefulTransformProcessor transformProcessor) {

        LOG.info("Initialising Transform Worker");
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG,
                config.getString(TransformWorkerConstant.APPLICATION_STATE_ID_CONFIG));
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG,
                config.getString(TransformWorkerConstant.BOOTSTRAP_SERVERS_CONFIG));
        props.put(StreamsConfig.EXACTLY_ONCE, true);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 2000); // This can create downstream issues if too short


        final StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> inputStream = builder.stream(config.getString(TransformWorkerConstant.INPUT_TOPIC));


        // Attach Transforn task and publish results to output topic
        DataStreamTransformTask dataStreamTransformTask = new DataStreamTransformTask(transformProcessor);
        dataStreamTransformTask.attachTo(inputStream)
                .to(config.getString(TransformWorkerConstant.OUTPUT_TOPIC));


        topology = builder.build();
        streams = new KafkaStreams(topology, props);

    }

    @Override
    public KafkaStreams getStreams() {
        return streams;
    }

    @Override
    public Topology getTopology() {
        return topology;
    }
}
