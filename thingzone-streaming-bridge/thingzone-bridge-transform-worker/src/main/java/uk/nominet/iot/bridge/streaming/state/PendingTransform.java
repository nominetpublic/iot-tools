/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.state;

import uk.nominet.iot.model.registry.RegistryTransformFunction;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PendingTransform implements Serializable {
    private String transformKey;
    private RegistryTransformFunction function;
    private Instant triggerTime;
    private Map<String, TransformSource> inputs;
    private Map<String, Object> parameters;
    private List<String> outputAliases;

    public PendingTransform(
            String transformKey,
            RegistryTransformFunction function,
            Instant triggerTime,
            HashMap<String, TransformSource> inputs,
            Map<String, Object> parameters,
            List<String> outputAliases) {
        this.transformKey = transformKey;
        this.function = function;
        this.triggerTime = triggerTime;
        this.inputs = inputs;
        this.parameters = parameters;
        this.outputAliases = outputAliases;
    }

    public Map<String, TransformSource> getInputs() {
        return inputs;
    }

    public void setInputs(Map<String, TransformSource> inputs) {
        this.inputs = inputs;
    }

    /**
     * @param payloadsWindow
     */
    public void update(HashMap<String, TransformInputPayloadWindow> payloadsWindow) {
        inputs.keySet().forEach(key -> {
            if (payloadsWindow.containsKey(key)) {
                TransformInputPayload payload = payloadsWindow.get(key).getLatest(triggerTime);
                if (payload != null) {
                    if (inputs.get(key).getPayload() == null) {
                        TransformSource source = inputs.get(key);
                        source.setPayload(payload);
                        //inputs.put(key, payload);
                    }
                    else inputs.get(key).getPayload().updateValue(payload);
                }
            }
        });
    }

    /**
     * @return true if the trabsform is ready to be executed
     */
    public boolean isReady() {
        if (inputs != null) {
            return inputs
                    .entrySet()
                    .stream()
                    .filter(input -> input == null ||
                                     input.getValue().getPayload() == null ||
                                     input.getValue().getPayload().isPending())
                    .count() == 0;
        }

        return false;
    }

    public String getTransformKey() {
        return transformKey;
    }

    public Instant getTriggerTime() {
        return triggerTime;
    }

    public void setTriggerTime(Instant triggerTime) {
        this.triggerTime = triggerTime;
    }

    public RegistryTransformFunction getFunction() {
        return function;
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public void setParameters(HashMap<String, Object> parameters) {
        this.parameters = parameters;
    }

    public void setTransformKey(String transformKey) {
        this.transformKey = transformKey;
    }

    public void setFunction(RegistryTransformFunction function) {
        this.function = function;
    }


    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }


    public List<String> getOutputAliases() {
        return outputAliases;
    }

    public void setOutputAliases(List<String> outputAliases) {
        this.outputAliases = outputAliases;
    }

    @Override
    public String toString() {
        return "PendingTransform{" +
                "transformKey='" + transformKey + '\'' +
                ", function=" + function +
                ", triggerTime=" + triggerTime +
                ", inputs=" + inputs +
                ", parameters=" + parameters +
                '}';
    }
}
