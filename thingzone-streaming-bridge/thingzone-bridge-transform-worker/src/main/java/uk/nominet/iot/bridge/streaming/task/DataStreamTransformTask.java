/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.task;

import java.util.*;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import uk.nominet.iot.bridge.streaming.aggregation.StreamDataAggregator;
import uk.nominet.iot.bridge.streaming.processor.StatefulTransformProcessor;
import uk.nominet.iot.bridge.streaming.state.PendingTransform;
import uk.nominet.iot.bridge.streaming.state.TransformSource;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.script.ExecutableScript;
import uk.nominet.iot.model.script.ExecutionTask;

public class DataStreamTransformTask {
    private static final Logger LOG = LoggerFactory.getLogger(DataStreamTransformTask.class);
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    private StatefulTransformProcessor transformProcessor;

    public DataStreamTransformTask(StatefulTransformProcessor transformProcessor) {
        this.transformProcessor = transformProcessor;
    }

    public KStream<String, String> attachTo(KStream<String, String> inputStream) {
        return inputStream.flatMap((key, points) -> {
            List<KeyValue<String, String>> transforms = new ArrayList<>();

            JsonParser jsonParser = new JsonParser();
            JsonElement messages = jsonParser.parse(points);

            if (!messages.isJsonArray())
                return null;

            JsonArray timeseriesMessages = messages.getAsJsonArray();

            List<PendingTransform> pendingTransforms = null;
            try {
                pendingTransforms = transformProcessor.processTimeseriesPoints(timeseriesMessages);
            } catch (Exception e) {
                LOG.error(e.getMessage());
                e.printStackTrace();
            }

            if (pendingTransforms != null) {
                for (PendingTransform transform : pendingTransforms) {
                    try {
                        Map<String, Object> sources = new HashMap<>();

                        for (Map.Entry<String, TransformSource> transformSourceEntry : transform.getInputs().entrySet()) {

                            if (transformSourceEntry.getValue().getAggregationSpec() != null &&
                                transformSourceEntry.getValue().getAggregationType() != null) {
                                StreamDataAggregator streamDataAggregator =
                                        new StreamDataAggregator(transformSourceEntry.getValue().getStreamKey());
                                sources.put(transformSourceEntry.getValue().getAlias(),
                                            streamDataAggregator.aggregate(transformSourceEntry.getValue().getAggregationType(),
                                                                           transformSourceEntry.getValue().getAggregationSpec()));
                            } else {
                                sources.put(transformSourceEntry.getValue().getAlias(),
                                            transformSourceEntry.getValue().getPayload().getValue());
                            }

                        }

                        ExecutionTask task = new ExecutionTask();
                        task.setTransformKey(transform.getTransformKey());
                        task.setParameters(transform.getParameters());
                        task.setOutputAliases(transform.getOutputAliases());
                        ExecutableScript script = new ExecutableScript();
                        script.setName(transform.getFunction().getName());
                        script.setType(transform.getFunction().getFunctionType());
                        script.setCode(transform.getFunction().getFunctionContent());
                        task.setScript(script);
                        task.setSources(sources);

                        KeyValue<String, String> tr = new KeyValue<>(UUID.randomUUID().toString(),
                                                                     jsonStringSerialiser.writeObject(task));
                        transforms.add(tr);
                        transformProcessor.transformExecutionPromise(transform);

                    } catch (Exception e) {
                        LOG.error(e.getMessage());
                    }
                }
            }

            return transforms;
        });
    }
}
