/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.state;

import java.io.Serializable;
import java.time.Instant;
import java.time.temporal.TemporalUnit;
import java.util.Map;
import java.util.TreeMap;

public class TransformInputPayloadWindow implements Serializable {
    private TreeMap<Instant, TransformInputPayload> payloads;

    private long windowSize;
    private TemporalUnit temporalUnit;

    public TransformInputPayloadWindow(long windowSize, TemporalUnit temporalUnit) {
        this.windowSize = windowSize;
        this.temporalUnit = temporalUnit;
        payloads = new TreeMap<>();
    }

    public void append(TransformInputPayload payload) {
        payloads.put(payload.getTimestamp(), payload);
        purge();
    }

    public int size(){
        return payloads.size();
    }

    public TransformInputPayload getLatest(Instant toTime) {

        return (payloads.floorEntry(toTime) == null) ? null : payloads.floorEntry(toTime).getValue();
    }

    public TreeMap<Instant, TransformInputPayload> getPayloads() {
        return payloads;
    }

    public Map.Entry<Instant, TransformInputPayload> last() {
        return payloads.lastEntry();
    }

    private void purge() {

        if (payloads.size() > 0) {
            //Get latest time
            Instant latestTime = payloads.lastKey();

            //Get time(key) closest to the end of the window
            Instant windowEndTime = payloads.ceilingKey(latestTime.minus(windowSize, temporalUnit));

            //Split tree at window end time
            payloads = new TreeMap<>(payloads.tailMap(windowEndTime));
        }
    }
}
