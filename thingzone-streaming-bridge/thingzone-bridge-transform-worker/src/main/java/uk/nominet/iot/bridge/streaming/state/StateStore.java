/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.state;

import com.google.gson.JsonObject;
import uk.nominet.iot.bridge.streaming.error.TransformException;
import uk.nominet.iot.model.registry.RegistryTransformFunction;
import uk.nominet.iot.model.registry.RegistryTransformSource;

import java.io.*;
import java.time.Instant;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StateStore implements Serializable {
    private HashMap<String, TransformState> pendingTransforms;
    private HashMap<String, TransformInputPayloadWindow> payloadsWindow;

    private long windowSize;
    private TemporalUnit temporalUnit;


    /**
     * Create a new state store instance
     * @param windowSize
     * @param temporalUnit
     */
    public StateStore(long windowSize, TemporalUnit temporalUnit) {
        this.windowSize = windowSize;
        this.temporalUnit = temporalUnit;
        pendingTransforms = new HashMap<>();
        payloadsWindow = new HashMap<>();
    }

    /**
     * Add a payload to the store
     * @param streamKey data stream key
     * @param timestamp timestamp
     * @param payload   json object containing payload
     */
    public void addPayload(String streamKey, Instant timestamp, JsonObject payload) {
        if (timestamp.isAfter(Instant.now().minus(windowSize, temporalUnit))) {
            if (!payloadsWindow.containsKey(streamKey))
                payloadsWindow.put(streamKey, new TransformInputPayloadWindow(windowSize, temporalUnit));

            TransformInputPayload inputPayload = new TransformInputPayload(streamKey, timestamp, payload);
            payloadsWindow.get(streamKey).append(inputPayload);
        }
    }

    /**
     * Add a pending payload, this is a promise that a payload will be available in the future
     * @param streamKey data stream key
     * @param timestamp timestamp
     */
    public void addPendingPayload(String streamKey, Instant timestamp) {
        if (!payloadsWindow.containsKey(streamKey))
            payloadsWindow.put(streamKey, new TransformInputPayloadWindow(windowSize, temporalUnit));

        TransformInputPayload inputPayload = new TransformInputPayload(streamKey, timestamp);
        payloadsWindow.get(streamKey).append(inputPayload);
    }

    /**
     * Gets the nearest payload to a given time
     * @param streamKey data stream key
     * @param toTime    timestamp
     * @return
     */
    public TransformInputPayload getNearestPayload(String streamKey, Instant toTime) {
        if (!payloadsWindow.containsKey(streamKey))
            return null;
        return payloadsWindow.get(streamKey).getLatest(toTime);
    }

    /**
     * Add transform to the store
     * @param transformKey     transform key
     * @param triggerTimestamp timestamp of when the transform was triggered
     * @param parameterValues  parameters
     * @param outputAliases    output aliases
     */
    public PendingTransform addTransform(String transformKey,
                                         RegistryTransformFunction function,
                                         Instant triggerTimestamp,
                                         List<RegistryTransformSource> sources,
                                         Map<String, Object> parameterValues,
                                         List<String> outputAliases) throws TransformException {

        if (triggerTimestamp.isBefore(Instant.now().minus(windowSize, temporalUnit)))
            throw new TransformException("Trigger outside time window range");

        HashMap<String, TransformSource> inputs = new HashMap<>();
        for (RegistryTransformSource source : sources) {

            TransformSource transformSource =
                    new TransformSource(source.getAlias(),
                                        source.getSource(),
                                        getNearestPayload(source.getSource(), triggerTimestamp),
                                        source.getAggregationType(),
                                        source.getAggregationSpec());

            inputs.put(source.getSource(), transformSource);
        }

        PendingTransform transform = new PendingTransform(
                transformKey,
                function,
                triggerTimestamp,
                inputs,
                parameterValues,
                outputAliases
        );

        if (!pendingTransforms.containsKey(transformKey)) {
            TransformState transformState = new TransformState(transformKey);

            transformState.addTransform(triggerTimestamp, transform);
            pendingTransforms.put(transformKey, transformState);

        } else {
            pendingTransforms
                    .get(transformKey)
                    .addTransform(triggerTimestamp, transform);
        }

        return transform;
    }

    /**
     * Get transform that are ready for execution
     * @return list of pending transforms
     */
    public List<PendingTransform> getTransformsReadyForExecution() {
        List<PendingTransform> readyList = new ArrayList<>();

        pendingTransforms.forEach((key, transformState) -> {
            transformState.getTransforms().forEach((triggerTime, transform) -> {
                //Update transform inputs
                transform.update(payloadsWindow);
                if (transform.isReady()) {
                    readyList.add(transform);
                }
            });
        });

        return readyList;
    }


    /**
     * Register a promise that a transform will be processed in the near future and the state store should
     * expect input payloads
     * @param transform transform pending execution
     */
    public void transformExecutionPromise(PendingTransform transform, boolean hasDependants) throws TransformException {
        String transformKey = transform.getTransformKey();
        Instant transformTrigger = transform.getTriggerTime();

        if (!transform.isReady())
            throw new TransformException(String.format("Transform key %s with trigger %s is not ready",
                                                       transformKey, transformTrigger));

        if (!pendingTransforms.containsKey(transformKey))
            throw new TransformException(String.format("Transform key %s not in state", transformKey));

        if (!pendingTransforms.get(transformKey).getTransforms().containsKey(transformTrigger)) {
            throw new TransformException(String.format("Transform key %s with trigger %s not in state",
                                                       transformKey, transformTrigger));
        }

        if (hasDependants) {
            //Add a pending payload into the state
            addPendingPayload(transformKey, transformTrigger);
        }

        //Remove transform from list
        pendingTransforms.get(transformKey).getTransforms().remove(transformTrigger);
    }

    /**
     * Save the state store to a file
     * @param file file path
     */
    public void toFile(File file) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
        oos.writeObject(this);
        oos.close();
    }

    /**
     * Load the store for a file
     * @param file file path
     * @return State store object
     */
    public static StateStore fromFile(File file) throws IOException, ClassNotFoundException {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            return (StateStore) ois.readObject();
        }
    }

    @Override
    public String toString() {
        return "StateStore{" +
               "pendingTransforms=" + pendingTransforms +
               ", payloadsWindow=" + payloadsWindow +
               ", windowSize=" + windowSize +
               ", temporalUnit=" + temporalUnit +
               '}';
    }

    public HashMap<String, TransformState> getPendingTransforms() {
        return pendingTransforms;
    }

    public HashMap<String, TransformInputPayloadWindow> getPayloadsWindow() {
        return payloadsWindow;
    }

    public void printStatus() {
        System.out.println(String.format("Window size: %d", windowSize));
        System.out.println(String.format("Time Unit: %s", temporalUnit.toString()));

        System.out.println("--------------------------------------------------------------------------------");
        System.out.println("Payloads Windows");
        System.out.println("--------------------------------------------------------------------------------");
        payloadsWindow.forEach((key, value) -> {
            System.out.println(String.format("key: %s, size: %d", key, value.size()));
        });
        System.out.println("--------------------------------------------------------------------------------");
        System.out.println("");
        System.out.println("");
        System.out.println("--------------------------------------------------------------------------------");
        System.out.println("Pending Transforms");
        System.out.println("--------------------------------------------------------------------------------");
        pendingTransforms.forEach((key, value) -> {
            System.out.println(String.format("key: %s, size: %s", key, value.toString()));
        });
        System.out.println("--------------------------------------------------------------------------------");
    }


}
