/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.state;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.junit.Before;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.junit.Assert.*;

public class TransformInputPayloadWindowTest {

    TransformInputPayloadWindow payloadWindow;
    Instant currentTime = Instant.now();

    @Before
    public void setUp() throws Exception {
        payloadWindow = new TransformInputPayloadWindow(30, ChronoUnit.MINUTES);

        //Fill 40 minutes of data
        for (int i = 40; i > 0; i--) {

            TransformInputPayload payload = new TransformInputPayload(
                    "stream1",
                    currentTime.minus(i, ChronoUnit.MINUTES),
                    new Gson().fromJson("{\"data\":\"some data"+i+"\"}", JsonObject.class)
            );

            payloadWindow.append(payload);

        }
    }

    @Test
    public void checkSizeOfInitialWindow() {
        assertEquals(31, payloadWindow.size());
    }

    @Test
    public void getLatestBeforeWindow() {
        TransformInputPayload payload = payloadWindow.getLatest(
                currentTime.minus(35, ChronoUnit.MINUTES)
        );
        assertNull(payload);

    }

    @Test
    public void getLatestAtWindowEdge() {
        Instant queryTime = currentTime.minus(30, ChronoUnit.MINUTES);
        TransformInputPayload payload = payloadWindow.getLatest(queryTime);
        assertEquals(queryTime, payload.getTimestamp());
    }

    @Test
    public void getNearestPayload() {
        Instant queryTime = currentTime.minus(190, ChronoUnit.SECONDS);
        TransformInputPayload payload = payloadWindow.getLatest(queryTime);
        assertTrue(payload.getTimestamp().isBefore(queryTime));
        assertTrue(Duration.between(payload.getTimestamp(), queryTime).toMillis() < 60000);
    }


    @Test
    public void addPayloadsToWondow() throws UnsupportedEncodingException {
        for (int i = 1; i < 10; i++) {

            TransformInputPayload payload = new TransformInputPayload(
                    "stream1",
                    currentTime.plus(2 * i, ChronoUnit.MINUTES),
                    new Gson().fromJson("{\"data\":\"some data\"}", JsonObject.class)
            );

            payloadWindow.append(payload);

        }
        assertEquals(21, payloadWindow.size());
    }
}
