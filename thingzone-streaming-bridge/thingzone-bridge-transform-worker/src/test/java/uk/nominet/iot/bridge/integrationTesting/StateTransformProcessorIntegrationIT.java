/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.integrationTesting;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import uk.nominet.iot.config.*;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.timeseries.MetricInt;
import uk.nominet.iot.test.docker.compose.ThingzoneComposeRuntimeContainer;
import uk.nominet.iot.test.docker.compose.ThingzoneEnvironment;
import uk.nominet.iot.tools.BootstrapRegistryData;
import org.apache.commons.io.IOUtils;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


@RunWith(Suite.class)
@Suite.SuiteClasses({
        SimpleTransformITSuite.class,
        TransformWithDependenciesITSuite.class
})
public class StateTransformProcessorIntegrationIT {
    public static final String DOCKER_COMPOSE_TEST_TEST_DATA_CONFIG_JSON = "../docker_compose_test/test_data_config.json";
    public static final String DOCKER_COMPOSE_TEST_TEST_DATA_CSV = "../docker_compose_test/test_data.csv";
    public static RegistryDriverConfig registryConfig;
    public static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    @ClassRule
    public static ThingzoneComposeRuntimeContainer environment =
            new ThingzoneEnvironment("../docker_compose_test/docker-compose.yaml")
                    .withServices(
                            ThingzoneEnvironment.SERVICE_REGISTRY
                    )
                    .getContainer();



    @BeforeClass
    public static void setUpClass() throws Exception {
        if (!RegistryAPIDriver.isInitialized()) {
            registryConfig = new RegistryDriverConfig();
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_SCHEME, "http");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST,
                    environment.getServiceHost(ThingzoneEnvironment.SERVICE_REGISTRY));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT,
                    environment.getServicePort(ThingzoneEnvironment.SERVICE_REGISTRY));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_USERNAME, "user_admin");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD, "user_admin");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS, 3);
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }


        InputStream jobInputStream = new FileInputStream(DOCKER_COMPOSE_TEST_TEST_DATA_CONFIG_JSON);
        File deviceListCSVFile = new File(DOCKER_COMPOSE_TEST_TEST_DATA_CSV);
        String jsonTxt = IOUtils.toString(jobInputStream, "UTF-8");
        JsonObject job = new Gson().fromJson(jsonTxt, JsonObject.class);

        BootstrapRegistryData bootstrapRegistryData = new BootstrapRegistryData(
                job,
                deviceListCSVFile,
                environment.getServicePort(ThingzoneEnvironment.SERVICE_REGISTRY)
        );
        bootstrapRegistryData.createRegistryUsers();
        bootstrapRegistryData.createFunctions();
        bootstrapRegistryData.createDevices();

        RegistryAPIDriver.globalDispose();

        if (!RegistryAPIDriver.isInitialized()) {
            RegistryDriverConfig registryConfig = new RegistryDriverConfig();
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_SCHEME, "http");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST,
                    environment.getServiceHost("registry", 8081));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT,
                    environment.getServicePort("registry", 8081));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_USERNAME, "bridge_agent");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD, "bridge_agent");
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS, 3);
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }
    }

    public static JsonObject generateTimeseriesPoint(Instant timestamp, String streamKey) throws Exception {
        MetricInt point = new MetricInt();
        point.setValue((int)(Math.random() * 10));
        point.setTimestamp(timestamp);
        point.setId(UUID.randomUUID());
        Map<String,Object> metadata = new HashMap<>();
        metadata.put("foo", "bar");
        point.setMetadata(metadata);
        point.setStreamKey(streamKey);

        String json = jsonStringSerialiser.writeObject(point);

        return new Gson().fromJson(json, JsonObject.class);

    }

}
