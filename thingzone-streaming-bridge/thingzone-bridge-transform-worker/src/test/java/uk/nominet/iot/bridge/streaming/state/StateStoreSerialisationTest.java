/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.state;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import uk.nominet.iot.model.registry.RegistryTransformFunction;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class StateStoreSerialisationTest extends StateStoreBase {
    static StateStore store;
    static Instant currentTime = Instant.now();

    @BeforeClass
    public static void setUpClass() throws Exception {
        RegistryTransformFunction function = new RegistryTransformFunction();
        function.setName("function1");
        function.setPublic(false);
        function.setFunctionType("javascript");
        function.setFunctionContent("var x = 1 + 1;");
        store = new StateStore(30, ChronoUnit.MINUTES);

        for (int i = 10000; i > 0; i--) {
            int streamIndex = (i%3);
            Instant payloadTime = currentTime.minus(i * 100, ChronoUnit.MILLIS);
            store.addPayload("stream" + streamIndex,
                    payloadTime,
                    new Gson().fromJson("{\"data\":\"some data\"}", JsonObject.class));

            if (streamIndex == 0 && payloadTime.isAfter(currentTime.minus(20, ChronoUnit.MINUTES))) {
                Map<String, Object> parameters = new HashMap<>();
                parameters.put("foo", 123);
                parameters.put("bar", "foo");

                List<String> outputAliases = ImmutableList.of("o1", "o2");

                store.addTransform(
                        "stream" + streamIndex,
                        function,
                        payloadTime,
                        ImmutableList.of(stream1, stream2),
                        parameters,
                        outputAliases);
            }
        }
    }

    @Test
    public void getTransformsReady() {
        List<PendingTransform> readyList =  store.getTransformsReadyForExecution();
        assertEquals(3332, readyList.size());
    }

    @Test
    public void canSaveStateToFile() throws IOException {
        File testFile = new File("/tmp/testStateStore");
        store.toFile(testFile);
    }

    @Test
    public void canSaveStateToFileAndReadItBack() throws IOException, ClassNotFoundException {
        File testFile = new File("/tmp/testStateStore");
        store.toFile(testFile);

        StateStore storeFromDisk = StateStore.fromFile(testFile);
        List<PendingTransform> readyList =  store.getTransformsReadyForExecution();

        readyList.forEach(pendingTransform ->
            pendingTransform.getInputs()
                            .forEach((key, value) -> assertEquals("{\"data\":\"some data\"}",
                                                                  value.getPayload().getValue().toString())));
        assertEquals(3, storeFromDisk.getPayloadsWindow().size());
        assertEquals(3332, readyList.size());
        assertEquals(3332, readyList.size());
    }
}
