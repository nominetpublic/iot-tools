/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.state;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import uk.nominet.iot.bridge.streaming.error.TransformException;
import uk.nominet.iot.model.registry.RegistryTransformFunction;
import org.junit.Before;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import static org.junit.Assert.*;

public class StateStoreTest extends StateStoreBase {
    StateStore store;
    Instant currentTime = Instant.now();
    RegistryTransformFunction function;

    @Before
    public void setup() throws Exception {
        store = new StateStore(30, ChronoUnit.MINUTES);

        function = new RegistryTransformFunction();
        function.setName("function1");
        function.setPublic(false);
        function.setFunctionType("javascript");
        function.setFunctionContent("var x = 1 + 1;");
    }

    @Test
    public void transformInputAppearsLater() throws UnsupportedEncodingException, TransformException {
        Instant triggerTime = currentTime.minus(15, ChronoUnit.MINUTES);
        store.addPayload(stream1.getSource(),
                triggerTime,
                new Gson().fromJson("{\"data\":\"some data\"}", JsonObject.class)
        );

        store.addPayload(stream2.getSource(),
                triggerTime.plus(1, ChronoUnit.MINUTES),
                new Gson().fromJson("{\"data\":\"some data\"}", JsonObject.class)
        );

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("foo", 123);
        parameters.put("bar", "foo");

        List<String> outputAliases = ImmutableList.of("o1", "o2");
        store.addTransform(transform1.getSource(), function, triggerTime, ImmutableList.of(stream1, stream2), parameters, outputAliases);

        assertEquals(0, store.getTransformsReadyForExecution().size());

        store.addPayload(stream2.getSource(),
                triggerTime.minus(2, ChronoUnit.MINUTES),
                new Gson().fromJson("{\"data\":\"some data\"}", JsonObject.class)
        );

        assertEquals(1, store.getTransformsReadyForExecution().size());

    }

    @Test(expected = TransformException.class)
    public void triggerTransformOutsideWindowPeriod() throws TransformException {
        Instant triggerTime = currentTime.minus(35, ChronoUnit.MINUTES);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("foo", 123);
        parameters.put("bar", "foo");
        List<String> outputAliases = ImmutableList.of("o1", "o2");
        store.addTransform("transform", function, triggerTime, ImmutableList.of(stream1, stream2), parameters, outputAliases);
    }


    @Test(expected = TransformException.class)
    public void promiseExecutionNoKey() throws TransformException {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("foo", 123);
        parameters.put("bar", "foo");
        PendingTransform transform = new PendingTransform("transfromABC", function, Instant.now(), null, parameters, null);
        store.transformExecutionPromise(transform, true);
    }

    @Test(expected = TransformException.class)
    public void promiseExecutionNoTrigger() throws TransformException {
        Instant triggerTime = currentTime.minus(15, ChronoUnit.MINUTES);

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("foo", 123);
        parameters.put("bar", "foo");

        List<String> outputAliases = ImmutableList.of("o1", "o2");
        PendingTransform transform = store.addTransform("transform", function, triggerTime, ImmutableList.of(stream1, stream2), parameters, outputAliases);

        transform.setTriggerTime(currentTime);

        store.transformExecutionPromise(transform, true);
    }
}

