/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.state;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import uk.nominet.iot.bridge.streaming.error.TransformException;
import uk.nominet.iot.model.registry.RegistryTransformFunction;
import org.junit.Before;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class StateStoreTransfromPromiseTest extends StateStoreBase {
    StateStore store;
    Instant currentTime = Instant.now();
    Instant triggerTime1 = currentTime.minus(15, ChronoUnit.MINUTES);
    Instant triggerTime2 = currentTime.minus(14, ChronoUnit.MINUTES);

    @Before
    public void setup() throws Exception {

        RegistryTransformFunction function = new RegistryTransformFunction();
        function.setName("function1");
        function.setPublic(false);
        function.setFunctionType("javascript");
        function.setFunctionContent("var x = 1 + 1;");

        store = new StateStore(30, ChronoUnit.MINUTES);

        store.addPayload(stream1.getSource(),
                triggerTime1,
                new Gson().fromJson("{\"data\":\"some data\"}", JsonObject.class)
        );

        store.addPayload(stream2.getSource(),
                triggerTime1.minus(5, ChronoUnit.MINUTES),
                new Gson().fromJson("{\"data\":\"some data\"}", JsonObject.class)
        );

        store.addPayload(stream3.getSource(),
                triggerTime2,
                new Gson().fromJson("{\"data\":\"some data\"}", JsonObject.class)
        );

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("foo", 123);
        parameters.put("bar", "foo");

        List<String> outputAliases = ImmutableList.of("o1", "o2");

        store.addTransform("transform1", function, triggerTime1, ImmutableList.of(stream1, stream2), parameters, outputAliases);
        store.addTransform("transform2", function, triggerTime2, ImmutableList.of(stream3, transform1), parameters, outputAliases);

    }

    @Test
    public void testInitialStoreState() {
        List<PendingTransform> pendingTransforms = store.getTransformsReadyForExecution();
        assertEquals(1, pendingTransforms.size());
        assertEquals("transform1", pendingTransforms.get(0).getTransformKey());
    }

    @Test
    public void transformGeneratedAPendingPayload() throws TransformException, UnsupportedEncodingException {
        List<PendingTransform> pendingTransforms = store.getTransformsReadyForExecution();
        store.transformExecutionPromise(pendingTransforms.get(0), true);

        TransformInputPayload payload = store.getNearestPayload("transform1", triggerTime1);
        assertEquals(triggerTime1, payload.getTimestamp());
        assertTrue(payload.isPending());
        assertNull(payload.getValue());

        pendingTransforms = store.getTransformsReadyForExecution();
        assertEquals(0, pendingTransforms.size());


        store.addPayload("transform1",
                triggerTime1,
                new Gson().fromJson("{\"data\":\"some data\"}", JsonObject.class)
        );

//        payload = store.getNearestPayload("transform1", triggerTime1);
//        System.out.println("Payload: " + payload);


        pendingTransforms = store.getTransformsReadyForExecution();
        assertEquals(1, pendingTransforms.size());

    }
}
