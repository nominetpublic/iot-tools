/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.integrationTesting;

import com.google.gson.JsonArray;
import uk.nominet.iot.bridge.streaming.processor.StatefulTransformProcessor;
import uk.nominet.iot.bridge.streaming.state.PendingTransform;
import uk.nominet.iot.bridge.streaming.state.TransformInputPayloadWindow;
import uk.nominet.iot.bridge.streaming.state.TransformState;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SimpleTransformITSuite {
    private static StatefulTransformProcessor transformProcessor =
            new StatefulTransformProcessor(30, ChronoUnit.MINUTES);

    private static final Instant currentTime = Instant.now();
    private static final Instant timestamp = currentTime.minus(29, ChronoUnit.MINUTES);

    @Test
    public void step01_setupInitialTimeseriesPoints() throws Exception {

        JsonArray timeseriesPoints = new JsonArray();

        timeseriesPoints.add(
                StateTransformProcessorIntegrationIT.generateTimeseriesPoint(timestamp, "device1.stream1")
        );

        timeseriesPoints.add(
                StateTransformProcessorIntegrationIT.generateTimeseriesPoint(timestamp, "device1.stream2")
        );

        transformProcessor.processTimeseriesPoints(timeseriesPoints);

        HashMap<String, TransformInputPayloadWindow> payloadsWindowMap =
                transformProcessor.getStateStore().getPayloadsWindow();

        HashMap<String, TransformState> pendingTransformsMap =
                transformProcessor.getStateStore().getPendingTransforms();

        assertEquals(1, payloadsWindowMap.get("device1.stream1").size());
        assertEquals(1, payloadsWindowMap.get("device1.stream2").size());
        assertEquals(1, pendingTransformsMap.get("device1.transform1").getTransforms().size());
        assertNotNull(
                pendingTransformsMap
                        .get("device1.transform1")
                        .getTransforms()
                        .lastEntry()
                        .getValue()
                        .getFunction()
                        .getFunctionContent()
        );
    }

    @Test
    public void step02_scheduleExecutionFormPendingTransform() throws Exception {

        List<PendingTransform> pendingTransforms = transformProcessor.getStateStore().getTransformsReadyForExecution();

        for (PendingTransform transform : pendingTransforms) {
            transformProcessor.transformExecutionPromise(transform);
        }

        HashMap<String, TransformInputPayloadWindow> payloadsWindowMap =
                transformProcessor.getStateStore().getPayloadsWindow();

        HashMap<String, TransformState> pendingTransformsMap =
                transformProcessor.getStateStore().getPendingTransforms();


        assertEquals(1, payloadsWindowMap.get("device1.stream1").size());
        assertEquals(1, payloadsWindowMap.get("device1.stream2").size());
        assertNull(payloadsWindowMap.get("device1.transform1"));
        assertEquals(0, pendingTransformsMap.get("device1.transform1").getTransforms().size());
    }

}
