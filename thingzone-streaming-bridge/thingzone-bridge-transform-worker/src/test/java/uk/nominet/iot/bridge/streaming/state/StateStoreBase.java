/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.state;

import uk.nominet.iot.model.registry.RegistryTransformSource;

public class StateStoreBase {
    public static RegistryTransformSource stream1;
    public static RegistryTransformSource stream2;
    public static RegistryTransformSource stream3;
    public static RegistryTransformSource transform1;

    static {
        stream1 = new RegistryTransformSource();
        stream1.setSource("stream1");
        stream1.setAlias("stream1Alias");
        stream1.setTrigger(true);

        stream2 = new RegistryTransformSource();
        stream2.setSource("stream2");
        stream2.setAlias("stream2Alias");
        stream2.setTrigger(false);

        stream3 = new RegistryTransformSource();
        stream3.setSource("stream3");
        stream3.setAlias("stream3Alias");
        stream3.setTrigger(true);

        transform1 = new RegistryTransformSource();
        transform1.setSource("transform1");
        transform1.setAlias("transform1Alias");
        transform1.setTrigger(true);
    }

}
