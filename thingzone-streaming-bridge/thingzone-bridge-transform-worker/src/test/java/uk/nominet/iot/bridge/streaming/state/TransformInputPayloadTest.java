/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.state;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.junit.Before;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.junit.Assert.*;

public class TransformInputPayloadTest {

    private JsonObject oldValue;
    private JsonObject newerValue;
    private Instant time;


    @Before
    public void setUp() throws Exception {
        oldValue = new Gson().fromJson("{\"data\":\"some old data\"}", JsonObject.class);
        newerValue =new Gson().fromJson("{\"data\":\"some new data\"}", JsonObject.class);
        time = Instant.now();
    }

    @Test
    public void newPayloadWithoutValueIsPending() {
        TransformInputPayload inputPayload = new TransformInputPayload("stream1",
                Instant.now());
        assertTrue(inputPayload.isPending());
    }

    @Test
    public void newPayloadWithWalueIsNotPending() throws UnsupportedEncodingException {
        TransformInputPayload inputPayload =
                new TransformInputPayload("stream1",
                        Instant.now(),
                        new Gson().fromJson("{\"data\":\"some data\"}", JsonObject.class));
        assertFalse(inputPayload.isPending());
    }

    @Test
    public void updateWithNewerValue() throws UnsupportedEncodingException {
        TransformInputPayload inputPayload =
                new TransformInputPayload("stream1",
                        time.minus(1, ChronoUnit.MILLIS),
                        oldValue);

        inputPayload.updateValue(time, newerValue);


        assertEquals(newerValue, inputPayload.getValue());
        assertEquals(time, inputPayload.getTimestamp());

    }


    @Test
    public void updateWithOlderValue() {
        TransformInputPayload inputPayload =
                new TransformInputPayload("stream1",
                        time, oldValue);

        inputPayload.updateValue(time.minus(1, ChronoUnit.MILLIS), newerValue);

        assertEquals(oldValue, inputPayload.getValue());
        assertEquals(time, inputPayload.getTimestamp());

    }

}
