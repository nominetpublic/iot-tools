#!/usr/bin/env bash
# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

CLASSPATH=/thingzone/thingzone-bridge-transform-worker.jar
JAVA_OPTIONS="-Dlog4j.configuration=file:/thingzone/log4j.xml"
JAVA_CLASS=uk.nominet.iot.bridge.streaming.TransformWorkerApp
CONFIG_FILE=/thingzone/config/thingzone-bridge-transform-worker.properties

# clean logs
rm -f /thingzone/logs/transform-worker.log

ATTEMPTS=0
MAX_ATTEMPTS=20

while ! echo dump | nc zookeeper 2181 | grep brokers ; do
    if [ "$ATTEMPTS" -gt "$MAX_ATTEMPTS" ]; then
        exit 1
    fi
    echo "Waiting for kafka to start ..."
    sleep 3
    ATTEMPTS=$((ATTEMPTS+1))
done

/usr/bin/java $JAVA_OPTIONS -cp ${CLASSPATH} $JAVA_CLASS $CONFIG_FILE
