/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.utils;

import java.util.Properties;

import kafka.admin.AdminUtils;
import kafka.admin.RackAwareMode;
import kafka.utils.ZKStringSerializer$;
import kafka.utils.ZkUtils;
import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.ZkConnection;

public class KafkaTopics {
    public static final int SESSION_TIME_OUT_IN_MS = 15 * 1000; // 15 secs
    public static final int CONNECTION_TIME_OUT_IN_MS = 10 * 1000; // 10 secs

    public static void createBasicTopic(String zookeeperHosts, String topicName) {
        ZkClient zkClient = null;
        ZkUtils zkUtils;
        try {
            int sessionTimeOutInMs = SESSION_TIME_OUT_IN_MS;
            int connectionTimeOutInMs = CONNECTION_TIME_OUT_IN_MS;

            zkClient = new ZkClient(zookeeperHosts, sessionTimeOutInMs,
                    connectionTimeOutInMs, ZKStringSerializer$.MODULE$);
            zkUtils = new ZkUtils(zkClient, new ZkConnection(zookeeperHosts), false);

            int noOfPartitions = 1;
            int noOfReplication = 1;
            Properties topicConfiguration = new Properties();
            AdminUtils.createTopic(zkUtils, topicName, noOfPartitions, noOfReplication,
                    topicConfiguration, RackAwareMode.Enforced$.MODULE$);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (zkClient != null) {
                zkClient.close();
            }
        }
    }

    public static void main(String[] args) throws Exception {
        if (args.length < 2) {
            System.out.println("Usage: KafkaTopics zookeperHosts topic1 ...");
            System.exit(1);
        }

        for (int i = 1; i < args.length; i++) {
            createBasicTopic(args[0], args[i]);
        }

    }
}
