/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CountDownLatch;

public class KafkaStreamingProcessExecutor {
    private static final Logger LOG = LoggerFactory.getLogger(KafkaStreamingProcessExecutor.class);
    KafkaStreamingProcess process;
    final CountDownLatch latch;

    public KafkaStreamingProcessExecutor(KafkaStreamingProcess process) {
        this.process = process;
        this.latch = new CountDownLatch(1);

        // attach shutdown handler to catch control-c
        Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
            @Override
            public void run() {
                LOG.info("Shutting down stream processing...");
                process.getStreams().close();
                latch.countDown();
            }
        });

    }

    public void start() {
        try {
            process.getStreams().start();
            latch.await();
        } catch (Throwable e) {
            LOG.error(e.getMessage());
            System.exit(1);
        }
        System.exit(0);
    }


}
