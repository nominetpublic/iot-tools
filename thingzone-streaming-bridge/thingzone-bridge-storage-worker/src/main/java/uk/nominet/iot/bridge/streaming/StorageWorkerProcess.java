/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming;

import java.util.Properties;
import java.util.UUID;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.driver.core.Cluster;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import uk.nominet.iot.bridge.streaming.config.StorageWorkerConfig;
import uk.nominet.iot.bridge.streaming.config.StorageWorkerConstant;
import uk.nominet.iot.config.CassandraConstant;
import uk.nominet.iot.config.CassandraDriverConfig;
import uk.nominet.iot.config.ElasticsearchConstant;
import uk.nominet.iot.config.ElasticsearchDriverConfig;
import uk.nominet.iot.config.RegistryConstant;
import uk.nominet.iot.config.RegistryDriverConfig;
import uk.nominet.iot.driver.CassandraDriver;
import uk.nominet.iot.driver.ElasticsearchDriver;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.error.DataCachingException;
import uk.nominet.iot.error.DataStorageException;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.manager.ThingzoneTimeseriesManager;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;
import uk.nominet.iot.utils.TimeseriesUtils;

public class StorageWorkerProcess implements KafkaStreamingProcess {
    private static final Logger LOG = LoggerFactory.getLogger(StorageWorkerProcess.class);
    private static final JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    private final KafkaStreams streams;
    private final Topology topology;
    private Cluster cluster;

    public StorageWorkerProcess(StorageWorkerConfig config) {
        System.out.println("Init Storage Worker Process");

        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG,
                  config.getString(StorageWorkerConstant.APPLICATION_ID_CONFIG));
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG,
                  config.getString(StorageWorkerConstant.BOOTSTRAP_SERVERS_CONFIG));
        props.put(StreamsConfig.EXACTLY_ONCE, true);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());

        if (!CassandraDriver.isInitialized()) {
            CassandraDriverConfig cassandraConfig = new CassandraDriverConfig();
            cassandraConfig.addProperty(CassandraConstant.CASSANDRA_SERVER_HOST,
                    config.getString(CassandraConstant.CASSANDRA_SERVER_HOST));
            cassandraConfig.addProperty(CassandraConstant.CASSANDRA_SERVER_PORT,
                    config.getString(CassandraConstant.CASSANDRA_SERVER_PORT));
            CassandraDriver.setConnectionProperties(cassandraConfig);
            CassandraDriver.globalInitialise();
        }

        if (!ElasticsearchDriver.isInitialized()) {
            ElasticsearchDriverConfig elasticsearchConfig = new ElasticsearchDriverConfig();
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_SCHEME,
                    config.getString(ElasticsearchConstant.ELASTICSEARCH_SERVER_SCHEME));
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_HOST,
                    config.getString(ElasticsearchConstant.ELASTICSEARCH_SERVER_HOST));
            elasticsearchConfig.addProperty(ElasticsearchConstant.ELASTICSEARCH_SERVER_PORT,
                    config.getString(ElasticsearchConstant.ELASTICSEARCH_SERVER_PORT));
            ElasticsearchDriver.setConnectionProperties(elasticsearchConfig);
            ElasticsearchDriver.globalInitialise();
        }

        if (!RegistryAPIDriver.isInitialized()) {
            RegistryDriverConfig registryConfig = new RegistryDriverConfig();
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_SCHEME,
                    config.getString(RegistryConstant.REGISTRY_SERVER_SCHEME));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST,
                    config.getString(RegistryConstant.REGISTRY_SERVER_HOST));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT,
                    config.getString(RegistryConstant.REGISTRY_SERVER_PORT));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_USERNAME,
                    config.getString(RegistryConstant.REGISTRY_SERVER_USERNAME));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD,
                    config.getString(RegistryConstant.REGISTRY_SERVER_PASSWORD));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS,
                    config.getString(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS));
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }

        final StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> inputStream = builder.stream(config.getString(StorageWorkerConstant.INPUT_TOPIC));

        inputStream.foreach((s, kafkaMessage) -> {

            JsonParser jsonParser = new JsonParser();
            JsonElement messages = jsonParser.parse(kafkaMessage);

            JsonArray timeseriesMessages = messages.getAsJsonArray();


            for (JsonElement timeseriesMessage : timeseriesMessages) {
                AbstractTimeseriesPoint point = null;

                try {
                   point = TimeseriesUtils.decodeFromJsonString(timeseriesMessage.toString());
                } catch (Exception e) {
                    LOG.error(e.getMessage());
                    e.printStackTrace();
                }

                try {
                    assert point != null;
                    UUID id = ThingzoneTimeseriesManager
                            .createManager()
                            .timeseries(point.getStreamKey(), point.klassObject())
                            .addPoint(point);
                    LOG.debug("---------- point added " + point.klassObject() + " -> " + id.toString());
                } catch (Exception e) {
                    LOG.error(e.getMessage() + "point: " + point);
                    e.printStackTrace();
                    //throw new RuntimeException(e);
                }
            }
        });

        topology = builder.build();
        streams = new KafkaStreams(topology, props);
    }

    @Override
    public KafkaStreams getStreams() {
        return streams;
    }

    @Override
    public Topology getTopology() {
        return topology;
    }
}
