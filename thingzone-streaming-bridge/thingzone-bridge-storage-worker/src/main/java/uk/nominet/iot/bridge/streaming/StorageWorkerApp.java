/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming;

import java.util.Properties;

import org.apache.kafka.common.config.ConfigException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;

import uk.nominet.iot.bridge.config.ConfigurationLoader;
import uk.nominet.iot.bridge.streaming.config.StorageWorkerConfig;

public class StorageWorkerApp {
    private static final Logger LOG = LoggerFactory.getLogger(StorageWorkerApp.class);
    private static StorageWorkerConfig config;

    public static void main(String[] args) throws Exception {
        LOG.info("Start Storage Worker Streaming service");

        if (args.length != 1) {
            System.out.println("Usage: StorageWorkerApp configFile");
            System.exit(1);
        }

        Properties configProp = ConfigurationLoader.fromFileName(args[0]);

        try {
            config = new StorageWorkerConfig(Maps.fromProperties(configProp));
        } catch (ConfigException e) {
            LOG.error(e.getMessage());
            throw new Exception("Couldn't start Storage Worker due to config error", e);
        }

        StorageWorkerProcess storageWorkerProcess = new StorageWorkerProcess(config);
        KafkaStreamingProcessExecutor kafkaStreamingProcessExecutor = new KafkaStreamingProcessExecutor(storageWorkerProcess);
        kafkaStreamingProcessExecutor.start();
    }
}
