/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming;

import com.google.common.collect.ImmutableList;
import com.google.gson.reflect.TypeToken;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.nominet.iot.bridge.streaming.model.DelayPushTask;
import uk.nominet.iot.bridge.streaming.model.PushRequestConfig;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;
import uk.nominet.iot.model.registry.RegistryPushSubscriberInternal;

import java.time.Instant;
import java.util.*;

public class PushWorkerTaskDispatcher implements Runnable {
    private Properties consumerProps;
    private Properties producerProps;
    private Collection<String> topics;
    private static JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
    private static final Logger LOG = LoggerFactory.getLogger(PushWorkerTaskDispatcher.class);


    public PushWorkerTaskDispatcher(String bootstrapServers, Collection<String> topics) {
        consumerProps = new Properties();
        consumerProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        consumerProps.put(ConsumerConfig.GROUP_ID_CONFIG, "pushTaskDispatcher");
        consumerProps.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");
        consumerProps.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        consumerProps.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");

        producerProps = new Properties();
        producerProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        producerProps.put(ProducerConfig.CLIENT_ID_CONFIG, "pushTaskProducer");
        producerProps.put(ProducerConfig.ACKS_CONFIG, "all");
        producerProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        producerProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");

        this.topics = topics;
    }

    public void run() {
        LOG.info("Running dispatcher...");
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(consumerProps);
        consumer.subscribe(topics);
        Producer<String, String> producer = new KafkaProducer<>(producerProps);


        while (!Thread.currentThread().isInterrupted()) {
            ConsumerRecords<String, String> records = consumer.poll(100);

            for (ConsumerRecord<String, String> record : records) {
                try {
                    String topic = record.topic();
                    String value = record.value();

                    List<AbstractTimeseriesPoint> timeseriesPoints =
                            jsonStringSerialiser
                                    .getDefaultSerialiser()
                                    .fromJson(value, new TypeToken<List<AbstractTimeseriesPoint>>() {
                                    }.getType());


                    PushRequestConfig pushRequestConfig = new PushRequestConfig();
                    pushRequestConfig.setRetrySequence(ImmutableList.of("1m", "1m", "1m"));
                    pushRequestConfig.setMethod("POST");
                    pushRequestConfig.setUri("https://test.com");


                    for (AbstractTimeseriesPoint timeseriesPoint : timeseriesPoints) {

                        TopicPartition topicPartition = new TopicPartition(topic, record.partition());


                        LOG.debug(String.format(Instant.now() + "DISPATCHER(%s)(%d): %s -- %s",
                                                             topicPartition, 0,
                                                             timeseriesPoint.getStreamKey(),
                                                             value));


                        for (RegistryPushSubscriberInternal pushSubscriber :
                                PushSubscriptionPoll.getPushSubscribersByStreamKey(timeseriesPoint.getStreamKey())) {

                            DelayPushTask pushTask = new DelayPushTask(0,
                                                                       timeseriesPoint.getStreamKey(),
                                                                       value,
                                                                       pushSubscriber,
                                                                       topicPartition,
                                                                       record.offset(),
                                                                       0);


                            ProducerRecord<String, String> producerRecord =
                                    new ProducerRecord<>("push-tasks",
                                                         pushTask.getId().toString(),
                                                         jsonStringSerialiser.writeObject(pushTask));
                            producerRecord.headers().add("push_retry_attempt", "0".getBytes());
                            producer.send(producerRecord);
                        }



                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

}
