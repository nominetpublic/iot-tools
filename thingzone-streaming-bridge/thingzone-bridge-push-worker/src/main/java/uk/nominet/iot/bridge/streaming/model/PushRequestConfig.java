/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.model;

import java.util.List;
import java.util.Map;

public class PushRequestConfig {
    private String method;
    private String uri;
    private Map<String, String> parameters;
    private Map<String, String> headers;
    private List<String> retrySequence;

    public PushRequestConfig() {
    }

    public PushRequestConfig(String method, String uri, Map<String, String> parameters, Map<String, String> headers, List<String> retrySequence) {
        this.method = method;
        this.uri = uri;
        this.parameters = parameters;
        this.headers = headers;
        this.retrySequence = retrySequence;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public List<String> getRetrySequence() {
        return retrySequence;
    }

    public void setRetrySequence(List<String> retrySequence) {
        this.retrySequence = retrySequence;
    }
}
