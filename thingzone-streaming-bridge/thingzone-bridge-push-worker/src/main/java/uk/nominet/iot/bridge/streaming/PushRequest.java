/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming;

import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import uk.nominet.iot.model.registry.RegistryPushSubscriberInternal;

import java.util.Map;

public class PushRequest {
    RegistryPushSubscriberInternal pushSubscriber;
    String jsonData;

    public PushRequest(RegistryPushSubscriberInternal pushSubscriber, String jsonData) {
        this.pushSubscriber = pushSubscriber;
        this.jsonData = jsonData;
    }

    public HttpResponse execute() throws Exception {
        Request request = null;

        String method = pushSubscriber.getMethod().toLowerCase();

        // Compose url with parameters
        URIBuilder urlBuilder = new URIBuilder(pushSubscriber.getUri());
        for (Map.Entry<String, Object> param : pushSubscriber.getUriParams().entrySet()) {
            urlBuilder.setParameter(param.getKey(), param.getValue().toString());
        }

        switch (method) {
            case "post":
                request = Request.Post(urlBuilder.build());
                break;
            case "put":
                request = Request.Put(urlBuilder.build());
                break;
            default:
                throw new Exception("Method " + method + " not supported");
        }

        // Add headers to message
        for (Map.Entry<String, Object> header : pushSubscriber.getHeaders().entrySet()) {
            request.addHeader(header.getKey(), header.getValue().toString());
        }

        // Add body of the message
        request.bodyString(jsonData, ContentType.APPLICATION_JSON);

        return request.execute().returnResponse();
    }
}
