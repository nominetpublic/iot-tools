/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.model;

import com.google.common.primitives.Ints;
import org.apache.kafka.common.TopicPartition;
import uk.nominet.iot.model.registry.RegistryPushSubscriberInternal;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class DelayPushTask implements Delayed {
    private static final long DELAY_1M = 60000;
    private static final long DELAY_5M = DELAY_1M * 5;
    private static final long DELAY_30M = DELAY_1M * 30;
    private static final long DELAY_1H = DELAY_1M * 60;
    private static final long DELAY_1D = DELAY_1H * 24;

    public static Map<String, Long> delayValues;
    static {
        delayValues = new HashMap<>();
        delayValues.put("1m", DELAY_1M);
        delayValues.put("5m", DELAY_5M);
        delayValues.put("30m", DELAY_30M);
        delayValues.put("1h", DELAY_1H);
        delayValues.put("1d", DELAY_1D);
    }

    private UUID id;
    private String streamKey;
    private String data;
    private long startTime;
    private RegistryPushSubscriberInternal pushSubscriber;
    private TopicPartition partition;
    private long offset;
    private int attempts;

    public DelayPushTask(long delayInMilliseconds, String streamKey, String data,  RegistryPushSubscriberInternal pushSubscriber, TopicPartition partition, long offset, int attempts) {
        this.streamKey = streamKey;
        this.data = data;
        this.startTime = System.currentTimeMillis() + delayInMilliseconds;;
        this.pushSubscriber = pushSubscriber;
        this.attempts = attempts;
        this.partition = partition;
        this.offset = offset;
        this.id = UUID.randomUUID();
    }

    @Override
    public long getDelay(TimeUnit timeUnit) {
        long diff = startTime - System.currentTimeMillis();
        return timeUnit.convert(diff, TimeUnit.MILLISECONDS);
    }

    @Override
    public int compareTo(Delayed delayed) {
        return Ints.saturatedCast(
                this.startTime - ((DelayPushTask) delayed).startTime);
    }

    public String getData() {
        return data;
    }

    public int incrementAttempts() {
        attempts++;
        return attempts;
    }

    public TopicPartition getPartition() {
        return partition;
    }

    public long getOffset() {
        return offset;
    }


    public String getStreamKey() {
        return streamKey;
    }

    public void setStartTime(long delayInMilliseconds) {
        this.startTime = System.currentTimeMillis() + delayInMilliseconds;;
    }

    public int getAttempts() {
        return attempts;
    }

    public UUID getId() {
        return id;
    }

    public RegistryPushSubscriberInternal getPushSubscriber() {
        return pushSubscriber;
    }

    public void setPushSubscriber(RegistryPushSubscriberInternal pushSubscriber) {
        this.pushSubscriber = pushSubscriber;
    }

    public void setPartition(TopicPartition partition) {
        this.partition = partition;
    }

    @Override
    public String toString() {
        return "DelayPushTask{" +
               "id=" + id +
               ", streamKey='" + streamKey + '\'' +
               ", data='" + data + '\'' +
               ", startTime=" + startTime +
               ", pushSubscriber=" + pushSubscriber +
               ", partition=" + partition +
               ", offset=" + offset +
               ", attempts=" + attempts +
               '}';
    }
}
