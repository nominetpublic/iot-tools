/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.config;

import org.apache.kafka.common.config.AbstractConfig;
import org.apache.kafka.common.config.ConfigDef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.nominet.iot.config.GenericConstant;
import uk.nominet.iot.config.RegistryConstant;

import java.util.Map;

public class PushWorkerConfig extends AbstractConfig {
    private static final Logger LOG = LoggerFactory.getLogger(PushWorkerConfig.class);

    public static ConfigDef baseConfigDef() {
        return new ConfigDef()
                .define(PushWorkerConstant.APPLICATION_ID_CONFIG,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Application ID")
                .define(PushWorkerConstant.APPLICATION_STATE_ID_CONFIG,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Application State ID")
                .define(PushWorkerConstant.BOOTSTRAP_SERVERS_CONFIG,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Bootstrap servers")
                .define(PushWorkerConstant.INPUT_TOPIC,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Input Topic")
                .define(PushWorkerConstant.PUSH_TASK_TOPIC,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Push Task Topic")
                .define(PushWorkerConstant.RETRY_TOPIC_PREFIX,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Retry topic prefix")
                .define(RegistryConstant.REGISTRY_SERVER_SCHEME,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Registry Server Scheme")
                .define(RegistryConstant.REGISTRY_SERVER_HOST,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Registry Host")
                .define(RegistryConstant.REGISTRY_SERVER_PATH,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Registry Path")
                .define(RegistryConstant.REGISTRY_SERVER_PORT,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Registry Port")
                .define(RegistryConstant.REGISTRY_SERVER_USERNAME,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Registry Username")
                .define(RegistryConstant.REGISTRY_SERVER_PASSWORD,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Registry Password")
                .define(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS,
                        ConfigDef.Type.STRING,
                        ConfigDef.Importance.HIGH,
                        "Registry Max Attempts");


    }

    public static final ConfigDef CONFIG_DEF = baseConfigDef();

    /**
     * Constructor
     *
     * @param props	properties map for configuration
     */
    public PushWorkerConfig(Map<String, String> props) {
        super(CONFIG_DEF, props);
        LOG.info("Initialize Push Worker configuration");
    }
}
