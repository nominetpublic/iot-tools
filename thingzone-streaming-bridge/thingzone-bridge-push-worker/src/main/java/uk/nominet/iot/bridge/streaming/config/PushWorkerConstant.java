/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.config;

public class PushWorkerConstant {
    public static final String APPLICATION_ID_CONFIG = "bridge.pushWorker.applicationId";
    public static final String APPLICATION_STATE_ID_CONFIG = "bridge.pushWorker.applicationStateId";
    public static final String BOOTSTRAP_SERVERS_CONFIG = "bridge.pushWorker.bootstrapServers";
    public static final String INPUT_TOPIC = "bridge.pushWorker.inputTopic";
    public static final String PUSH_TASK_TOPIC = "bridge.pushWorker.pushTaskTopic";
    public static final String RETRY_TOPIC_PREFIX = "bridge.pushWorker.retry_topic_prefix";
}
