/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.model.registry.RegistryPushSubscriberInternal;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class PushSubscriptionPoll {
    private static final Logger LOG = LoggerFactory.getLogger(PushSubscriptionPoll.class);
    private static final long FETCH_INTERVAL = 30000;
    protected static volatile List<RegistryPushSubscriberInternal> pushSubscribers = new ArrayList<>();
    private static ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();



    private static void poll() {
        try {
            pushSubscribers = RegistryAPIDriver.getSession().internal.listPushSubscribers();
            LOG.debug("POLL subscribers: " + pushSubscribers);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<RegistryPushSubscriberInternal> getPushSubscribersByStreamKey(String streamKey) {
        return pushSubscribers.stream()
                              .filter(push -> push.getDataStream().getKey().equals(streamKey))
                              .collect(Collectors.toList());
    }


   public static void start() {
       ScheduledExecutorService es = Executors.newScheduledThreadPool(1);
       exec.scheduleAtFixedRate(PushSubscriptionPoll::poll, 0, FETCH_INTERVAL, TimeUnit.MILLISECONDS);
   }
}
