/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming;

import org.apache.http.HttpResponse;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.nominet.iot.bridge.streaming.model.DelayPushTask;
import uk.nominet.iot.json.JsonStringSerialiser;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class DelayQueuePushExecutor implements Runnable {
    private static final Logger LOG = LoggerFactory.getLogger(DelayQueuePushExecutor.class);
    private BlockingQueue<DelayPushTask> queue;
    public AtomicInteger numberOfConsumedElements = new AtomicInteger();
    private KafkaConsumer<String, String> consumer;
    private Producer<String, String> producer;
    private static JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();

    private Map<TopicPartition, DelayPushTask> latestCompletedTasks;

    public DelayQueuePushExecutor(BlockingQueue<DelayPushTask> queue, KafkaConsumer<String, String> consumer, Producer<String, String> producer) {
        this.queue = queue;
        this.consumer = consumer;
        this.producer = producer;
        latestCompletedTasks = new HashMap<>();
    }

    public Map<TopicPartition, DelayPushTask> getLatestCompletedTasks() {
        return latestCompletedTasks;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                DelayPushTask pushTask = queue.take();
                numberOfConsumedElements.incrementAndGet();
                LOG.debug("Push task " + pushTask);

                boolean fail = true;
                try {
                    PushRequest pushRequest = new PushRequest(pushTask.getPushSubscriber(), pushTask.getData());
                    HttpResponse response = pushRequest.execute();

                    if (response.getStatusLine().getStatusCode() == 200) fail = false;

                    LOG.debug("Response: " + response.getStatusLine().getStatusCode());

                } catch (Exception e) {
                    e.printStackTrace();
                    fail = true;
                } finally {
                    if (fail) {
                        LOG.error("Request fail. Task: " + pushTask.getId().toString());

                        List<String> retrySequence =
                                Arrays.asList(pushTask.getPushSubscriber().getRetrySequence().split("\\s+"));

                        // Update task
                        int attempts = pushTask.incrementAttempts();

                        LOG.debug("Retry sequence: " + retrySequence.size() + " attempts: " + attempts);

                        if (retrySequence != null && retrySequence.size() > 0 && attempts <= retrySequence.size()) {
                            pushTask.setPartition(new TopicPartition("retry_push_" + retrySequence.get(attempts - 1), 0));
                            ProducerRecord<String, String> producerRecord = new ProducerRecord<>(
                                    pushTask.getPartition().topic(),
                                    pushTask.getId().toString(),
                                    jsonStringSerialiser.writeObject(pushTask));

                            producerRecord.headers().add("push_retry_attempt", Integer.toString(attempts).getBytes());
                            producer.send(producerRecord);
                        } else {
                            LOG.error("no more elements in the sequence. Task: " + pushTask.getId().toString());
                        }
                    }

                    latestCompletedTasks.put(pushTask.getPartition(), pushTask);
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
