/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming.utils;

import com.github.filosganga.geogson.model.Point;
import com.github.filosganga.geogson.model.positions.SinglePosition;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;
import uk.nominet.iot.model.timeseries.Config;
import uk.nominet.iot.model.timeseries.Event;
import uk.nominet.iot.model.timeseries.Json;
import uk.nominet.iot.model.timeseries.ThingzoneConfigOrigin;

import java.time.Instant;
import java.util.*;

public class PushMessagesGenerator {
    public static void main(String[] args) throws InterruptedException {
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:29092");
        props.put("acks", "all");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");


        JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
        Random r = new Random();

        Producer<String, String> producer = new KafkaProducer<>(props);
        for (int i = 0; i < 10000; i++) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("someConfigProperty", String.format("%d",i));

            JsonObject jsonObjectReport = new JsonObject();
            jsonObjectReport.addProperty("reportProp1", String.format("%d",i));

            JsonObject metadata = new JsonObject();
            metadata.addProperty("meta", "data");

            Config config = new Config();
            config.setId(UUID.randomUUID());
            config.setStreamKey("ept-ct00octophine001n.config");
            config.setConfig(jsonStringSerialiser.getDefaultSerialiser().fromJson(jsonObject, new TypeToken<Map<String, Object>>() {}.getType()));
            config.setTimestamp(Instant.now());
            config.setRevision(String.format("0.%d",i));
            config.setDerivedFromConfig(UUID.randomUUID());
            config.setOrigin(ThingzoneConfigOrigin.DEVICE);
            config.setTags(Arrays.asList("tag1", "tag2"));
            config.setMetadata(jsonStringSerialiser.getDefaultSerialiser().fromJson(metadata, new TypeToken<Map<String, Object>>() {}.getType()));
            System.out.println("JSON: " + jsonStringSerialiser.writeObject(config));


            Json json = new Json();
            json.setId(UUID.randomUUID());
            json.setStreamKey("ept-ct00octophine001n.report");
            json.setValue(jsonStringSerialiser.getDefaultSerialiser().fromJson(jsonObjectReport, new TypeToken<Map<String, Object>>() {}.getType()));
            json.setTimestamp(Instant.now());


            Event event = new Event();
            event.setId(UUID.randomUUID());
            event.setStreamKey("ept-ct00octophine001n.event");
            event.setLocation(new Point(new SinglePosition(-1.227, 51.746 + (i*0.01), 0.0)));
            event.setName("test event");
            event.setTimestamp(Instant.now());

            List<AbstractTimeseriesPoint> payloads = Arrays.asList(config, json, event);
            System.out.println("Pushing message: " + config.getId().toString() + " - " + config.getTimestamp() + " value: " + config.getConfig());


            ProducerRecord<String, String> producerRecord = new ProducerRecord<String, String>("timeseries", config.getId().toString(), jsonStringSerialiser.writeObject(payloads));
            //producerRecord.headers().add("push_retry_attempt", "0".getBytes());
            producer.send(producerRecord);

            Thread.sleep(10000);
        }

        producer.close();
    }
}
