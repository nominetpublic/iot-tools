/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming;

import com.google.common.collect.ImmutableList;
import com.google.gson.reflect.TypeToken;

import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.nominet.iot.bridge.streaming.model.DelayPushTask;
import uk.nominet.iot.json.JsonStringSerialiser;
import uk.nominet.iot.model.core.AbstractTimeseriesPoint;

import java.time.Instant;
import java.util.*;
import java.util.concurrent.*;


public class PushWorkerTaskExecutor implements Runnable {
    private static final Logger LOG = LoggerFactory.getLogger(PushWorkerTaskExecutor.class);
    private Properties consumerProps;
    private Properties producerProps;
    private Collection<String> topics;
    private BlockingQueue<DelayPushTask> pushTasksQueue;
    private static JsonStringSerialiser jsonStringSerialiser = new JsonStringSerialiser();
    private long delay;


    public PushWorkerTaskExecutor(String bootstrapServers, String topic, long delay) {
        this.delay = delay;
        consumerProps = new Properties();
        consumerProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        consumerProps.put(ConsumerConfig.GROUP_ID_CONFIG, "pushClient" + delay);
        consumerProps.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        consumerProps.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        consumerProps.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");

        producerProps = new Properties();
        producerProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        producerProps.put(ProducerConfig.CLIENT_ID_CONFIG, "pushClientProducer" + delay);
        producerProps.put(ProducerConfig.ACKS_CONFIG, "all");
        producerProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        producerProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");

        topics = ImmutableList.of(topic);
        pushTasksQueue = new DelayQueue<>();
    }


    public void run() {
       LOG.debug("Running task executor with delay: " + delay + "ms topic: " + topics);
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(consumerProps);
        consumer.subscribe(topics);
        ExecutorService executor = Executors.newFixedThreadPool(1);

        Producer<String, String> producer = new KafkaProducer<>(producerProps);

        DelayQueuePushExecutor delayQueuePushExecutor = new DelayQueuePushExecutor(pushTasksQueue, consumer, producer);

        executor.submit(delayQueuePushExecutor);

        while (!Thread.currentThread().isInterrupted()) {
            ConsumerRecords<String, String> records = consumer.poll(100);

            for (ConsumerRecord<String, String> record : records) {
                try {
                    String topic = record.topic();
                    String value = record.value();

                    // Deserialize task
                    DelayPushTask pushTask =
                            jsonStringSerialiser
                                    .getDefaultSerialiser()
                                    .fromJson(value, DelayPushTask.class);

                    List<AbstractTimeseriesPoint> timeseriesPoints =
                            jsonStringSerialiser
                                    .getDefaultSerialiser()
                                    .fromJson(pushTask.getData(), new TypeToken<List<AbstractTimeseriesPoint>>() {
                                    }.getType());

                    for (AbstractTimeseriesPoint timeseriesPoint : timeseriesPoints) {

                        TopicPartition topicPartition = new TopicPartition(topic, record.partition());
                        pushTask.setPartition(topicPartition);

                        LOG.debug(String.format(Instant.now() + " -> TASKEXECUTOR(%s)(%d): %s -- %s",
                                                         topicPartition,
                                                         pushTask.getAttempts(),
                                                         timeseriesPoint.getStreamKey(),
                                                         value));
                        pushTask.setStartTime(delay);
                        pushTasksQueue.put(pushTask);

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            for (Map.Entry<TopicPartition, DelayPushTask> latestTaskEntry :
                    delayQueuePushExecutor.getLatestCompletedTasks().entrySet()) {

                consumer.commitSync(Collections.singletonMap(latestTaskEntry.getValue().getPartition(),
                                                              new OffsetAndMetadata(latestTaskEntry.getValue().getOffset() + 1)));


            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        try {
            executor.awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        executor.shutdown();
    }

}
