/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.iot.bridge.streaming;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import org.apache.kafka.common.config.ConfigException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.nominet.iot.bridge.config.ConfigurationLoader;
import uk.nominet.iot.bridge.streaming.config.PushWorkerConfig;
import uk.nominet.iot.bridge.streaming.config.PushWorkerConstant;
import uk.nominet.iot.bridge.streaming.model.DelayPushTask;
import uk.nominet.iot.config.RegistryConstant;
import uk.nominet.iot.config.RegistryDriverConfig;
import uk.nominet.iot.driver.RegistryAPIDriver;
import uk.nominet.iot.model.registry.RegistryPushSubscriberInternal;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PushWorkerApp {
    private static final Logger LOG = LoggerFactory.getLogger(PushWorkerApp.class);

    public static void main(String[] args) throws Exception {
        LOG.info("Start Push Worker service");

        if (args.length != 1) {
            System.out.println("Usage: PushWorkerApp configFile");
            System.exit(1);
        }

        Properties configProp = ConfigurationLoader.fromFileName(args[0]);

        PushWorkerConfig config;
        try {
            config = new PushWorkerConfig(Maps.fromProperties(configProp));
        } catch (ConfigException e) {
            LOG.error(e.getMessage());
            throw new Exception("Couldn't start Push Worker due to config error", e);
        }

        if (!RegistryAPIDriver.isInitialized()) {
            RegistryDriverConfig registryConfig = new RegistryDriverConfig();
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_SCHEME,
                                       config.getString(RegistryConstant.REGISTRY_SERVER_SCHEME));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_HOST,
                                       config.getString(RegistryConstant.REGISTRY_SERVER_HOST));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PORT,
                                       config.getString(RegistryConstant.REGISTRY_SERVER_PORT));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_USERNAME,
                                       config.getString(RegistryConstant.REGISTRY_SERVER_USERNAME));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_PASSWORD,
                                       config.getString(RegistryConstant.REGISTRY_SERVER_PASSWORD));
            registryConfig.addProperty(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS,
                                       config.getString(RegistryConstant.REGISTRY_SERVER_MAX_ATTEMPTS));
            RegistryAPIDriver.setConnectionProperties(registryConfig);
            RegistryAPIDriver.globalInitialise();
        }

        Collection<String> topicsDispatcher = ImmutableList.of(
                config.getString(PushWorkerConstant.INPUT_TOPIC)
                                                              );

        ExecutorService executor = Executors.newFixedThreadPool(DelayPushTask.delayValues.size() + 2);

        PushWorkerTaskDispatcher dispatcher = new PushWorkerTaskDispatcher(config.getString(PushWorkerConstant.BOOTSTRAP_SERVERS_CONFIG), topicsDispatcher);
        executor.submit(dispatcher);


        // Start the subscription poll thread
        PushSubscriptionPoll.start();

        // Main push worker task
        PushWorkerTaskExecutor taskExecutor = new PushWorkerTaskExecutor(
                config.getString(PushWorkerConstant.BOOTSTRAP_SERVERS_CONFIG),
                config.getString(PushWorkerConstant.PUSH_TASK_TOPIC),
                0);
        executor.submit(taskExecutor);

        // Delay task executors
        for (Map.Entry<String, Long> delayValueEntry : DelayPushTask.delayValues.entrySet()) {
            PushWorkerTaskExecutor taskExecutorDelay = new PushWorkerTaskExecutor(
                    config.getString(PushWorkerConstant.BOOTSTRAP_SERVERS_CONFIG),
                    String.format("%s%s", config.getString(PushWorkerConstant.RETRY_TOPIC_PREFIX), delayValueEntry.getKey()),
                    delayValueEntry.getValue());
            executor.submit(taskExecutorDelay);
        }

    }
}
