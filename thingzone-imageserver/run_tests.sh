#!/bin/bash
# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


# clean
APP_CLASS=uk.nominet.research.imageserver.webserver.WebServerApp
pkill -f $APP_CLASS
sleep 1
mkdir -p log
rm -f log/*

mkdir -p data
rm -rf data/*

mkdir -p temp
rm -f temp/*


# make sure we're up to date
make
RESULT=$?
if [ $RESULT -ne 0 ]; then
	echo "make failed"
	exit $RESULT
fi


# start registry, with logback to a local file
export IMAGESERVER_LOGBACK_CONFIG=logback_teamcity.xml
nohup ./run.sh > log/imageserver_start.log 2>&1 < /dev/null &

echo "waiting for imageserver to start..."


# wait for imageserver to start
touch log/imageserver.log
COUNT=0
MAX_WAIT=15
while true; do
	sleep 1
	COUNT=$(($COUNT+1))
	
	echo "checking logfile.."
	tail -1 log/imageserver.log  | grep "org.eclipse.jetty.server.Server - Started" > /dev/null
	RESULT=$?
	if [ $RESULT -eq 0 ]; then
		echo "imageserver started"
		break
	else
		echo "waiting..."
	fi
	
	if [ $COUNT -ge $MAX_WAIT ]; then
		echo "gave up waiting for imageserver to start after ${MAX_WAIT}s"
		pkill -f $APP_CLASS
		exit 1
	fi
done

echo "running tests..."
sleep 1
(cd test && bash ./do_test.sh)
RESULT=$?

pkill -f $APP_CLASS

if [ $RESULT -ne 0 ]; then
	echo "tests failed"
	exit $RESULT
fi


