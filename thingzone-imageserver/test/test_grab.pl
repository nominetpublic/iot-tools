# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;

use JSON;
use Data::Dumper;
use File::Compare;

use test_common;


unlink glob "$TEMP_DIR/*";

my @image_urls = qw(http://lorempixel.com/400/400/animals/2/);
#my @image_urls = qw(not_a_valid_url);
#my @image_urls = qw(http://lorempixel.com/thisdoesntexist/);
#my @image_urls = qw(http://lorempixel.com/400/400/animals/2);

my %image_metadata;

print "======================================================\n";
print "\n$0: upload_url/download/list tests\n";


for my $image_url (@image_urls) {
	chomp($image_url);
	
	print "uploading file from URL $image_url\n";

# downloading manually
# curl http://lorempixel.com/400/400/animals/2/ -o gorilla.jpg

	my $response = `curl -s -X POST -H "Content-Type: application/x-www-form-urlencoded" -d "image_url=$image_url" $GRAB_URL`;
#	print "response=$response\n";
	
	if ($response !~ /"result":"ok"/) {
		die "failed: response=$response\n";
	}
	
#	print "uploaded: response=$response\n";
	my $response_json = decode_json($response) or die "failed to decode";
#	print Dumper($json);
	print Data::Dumper->Dump([$response_json], [qw(response)]);
	
	die unless $$response_json{result} eq "ok";
	$image_metadata{$image_url} = $response_json;
	
	print "\n";
}

#print scalar(@images)." uploaded OK\n\n";
print "======================================================\n";


# read back
while (my ($image_url, $metadata) = each %image_metadata) {
	my $key = $$metadata{name};
	print "retrieving '$image_url' ($key) from imageserver\n";

	# (-w "%{http_code}" gets curl to print HTTP status code)
	my $result = `curl -s -w "%{http_code}" -o $TEMP_DIR/$key $DOWNLOAD_URL/$key`;
	print "result=$result\n";
	
	die unless $result eq "200";


	# get the original
	print "retrieving original '$image_url'\n";
	$result = `curl -s -w "%{http_code}" -o $TEMP_DIR/${key}_original $image_url`;
	die unless $result eq "200";

	
	if (compare("$TEMP_DIR/${key}_original","$TEMP_DIR/$key") != 0) {
		die "returned file didn't match";
	}
	print "PASS: result file $TEMP_DIR/$key matched original file $TEMP_DIR/${key}_original\n";
}


#print scalar(@images)." downloaded OK\n\n";


print "\n$0: all tests passed\n";



	


