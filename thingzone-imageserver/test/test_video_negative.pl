# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;

use JSON;
use Data::Dumper;
use File::Compare;

use test_common;


unlink glob "$TEMP_DIR/*";


my ($json_data, $response, $response_json);

print "======================================================\n";
print "$0: negative tests\n";

#-------

print "\nTEST: missing height\n";
$json_data=qq(--data '
{ 
"width": 640,
"inputFrameRate" : 1.0, 
"outputFrameRate" : 30, 
"frameSequence": [ 
	{"key":"irrevelevant", "count":2}
]}');

$response = `curl -s -X POST $VIDEO_URL/compose $JSON_HEADER $json_data`;
$response_json = decode_json($response) or die "failed to decode";
print Data::Dumper->Dump([$response_json], [qw(response)]);

die unless $$response_json{result} eq "fail";
die unless $$response_json{reason} =~ /missing \'height\' parameter/;
#-------

print "\nTEST: bogus height\n";
$json_data=qq(--data '
{ 
"width": 640, "height": false,
"inputFrameRate" : 1.0, 
"outputFrameRate" : 30, 
"frameSequence": [ 
	{"key":"irrevelevant", "count":2}
]}');

$response = `curl -s -X POST $VIDEO_URL/compose $JSON_HEADER $json_data`;
$response_json = decode_json($response) or die "failed to decode";
print Data::Dumper->Dump([$response_json], [qw(response)]);

die unless $$response_json{result} eq "fail";
die unless $$response_json{reason} =~ /Can not deserialize instance of int/;

#-------

print "\nTEST: missing inputFrameRate\n";
$json_data=qq(--data '
{ 
"width": 640, "height": 480, 
"frameSequence": [ 
	{"key":"irrevelevant", "count":2}
]}');

$response = `curl -s -X POST $VIDEO_URL/compose $JSON_HEADER $json_data`;
$response_json = decode_json($response) or die "failed to decode";
print Data::Dumper->Dump([$response_json], [qw(response)]);

die unless $$response_json{result} eq "fail";
die unless $$response_json{reason} =~ /missing \'inputFrameRate\' parameter/;

#-------

print "\nTEST: non-numeric frameRate\n";
$json_data=qq(--data '
{ 
"width": 640, "height": 480, 
"inputFrameRate" : "wrong type", 
"outputFrameRate" : 30, 
"frameSequence": [ 
	{"key":"irrevelevant", "count":2}
]}');

$response = `curl -s -X POST $VIDEO_URL/compose $JSON_HEADER $json_data`;
#print $response;
$response_json = decode_json($response) or die "failed to decode";
print Data::Dumper->Dump([$response_json], [qw(response)]);

die unless $$response_json{result} eq "fail";
die unless $$response_json{reason} =~ /not a valid Float value/;


#-------

print "\nTEST: bogus container\n";
$json_data=qq(--data '
{ 
"width": 640, "height": 480, 
"inputFrameRate" : 1.0, 
"outputFrameRate" : 30, 
"container" : "bogus",
"codec" : "x264", 
"frameSequence": [ 
	{"key":"irrevelevant", "count":2}
]}');

$response = `curl -s -X POST $VIDEO_URL/compose $JSON_HEADER $json_data`;
#print $response;
$response_json = decode_json($response) or die "failed to decode";
print Data::Dumper->Dump([$response_json], [qw(response)]);

die unless $$response_json{result} eq "fail";
die unless $$response_json{reason} =~ /invalid container/;



#-------

print "\nTEST: bogus codec\n";
$json_data=qq(--data '
{ 
"width": 640, "height": 480, 
"inputFrameRate" : 1.0, 
"outputFrameRate" : 30, 
"container" : "mp4",
"codec" : "bogus", 
"frameSequence": [ 
	{"key":"irrevelevant", "count":2}
]}');

$response = `curl -s -X POST $VIDEO_URL/compose $JSON_HEADER $json_data`;
#print $response;
$response_json = decode_json($response) or die "failed to decode";
print Data::Dumper->Dump([$response_json], [qw(response)]);

die unless $$response_json{result} eq "fail";
die unless $$response_json{reason} =~ /invalid codec/;




#-------

print "\nTEST: bogus codec/container combination\n";
$json_data=qq(--data '
{ 
"width": 640, "height": 480, 
"inputFrameRate" : 1.0, 
"outputFrameRate" : 30, 
"container" : "mp4",
"codec" : "vp8", 
"frameSequence": [ 
	{"key":"irrevelevant", "count":2}
]}');

$response = `curl -s -X POST $VIDEO_URL/compose $JSON_HEADER $json_data`;
#print $response;
$response_json = decode_json($response) or die "failed to decode";
print Data::Dumper->Dump([$response_json], [qw(response)]);

die unless $$response_json{result} eq "fail";
die unless $$response_json{reason} =~ /invalid codec/;




#-------

print "\nTEST: bogus frame specified\n";
$json_data=qq(--data '
{ 
"width": 640, "height": 480, 
"inputFrameRate" : 1.0, 
"outputFrameRate" : 30, 
"container" : "mp4",
"codec" : "x264", 
"frameSequence": [ 
	{"key":"totally_bogus", "count":2}
]}');

$response = `curl -s -X POST $VIDEO_URL/compose $JSON_HEADER $json_data`;
$response_json = decode_json($response) or die "failed to decode";
print Data::Dumper->Dump([$response_json], [qw(response)]);

die unless $$response_json{result} eq "fail";
die unless $$response_json{reason} =~ /not found/;


#-------------------------------------------------------------------------
# some negative tests get a bit further, so need image

my @images = qw (IMG_0749.jpg);	# just 1 needed

my %image_metadata;

print "======================================================\n";
print "\n$0: video negative test: upload test images\n";


for my $image (@images) {
	chomp($image);
	
	my $upload_file = "$IMAGE_DIR/$image";
	print "uploading file $upload_file\n";

	my $response = `curl -s -X POST -F "file=\@$upload_file" $UPLOAD_URL`;
	if ($response !~ /"result":"ok"/) {
		die "failed: response=$response\n";
	}
	
	my $response_json = decode_json($response) or die "failed to decode";
	print Data::Dumper->Dump([$response_json], [qw(response)]);
	
	die unless $$response_json{result} eq "ok";
	$image_metadata{$image} = $response_json;
	
	print "\n";
}

print scalar(@images)." uploaded OK\n\n";


# a test we can remove one happy day
print "\nTEST: Ogg/Theora not implemented\n";
my $json_data=qq(--data '
{ 
"width": 640, "height": 480, 
"inputFrameRate" : 2, 
"outputFrameRate" : 30, 
"container" : "ogg",
"codec": "Theora", 
"frameSequence": [ 
	{"key":"${image_metadata{'IMG_0749.jpg'}{'name'}}", "count":2}
]}');

my $response = `curl -s -X POST $VIDEO_URL/compose $JSON_HEADER $json_data`;
$response_json = decode_json($response) or die "failed to decode";
print Data::Dumper->Dump([$response_json], [qw(response)]);

die unless $$response_json{result} eq "fail";
die unless $$response_json{reason} =~ /not.* implemented/;



print "\n$0: all tests passed\n";



	


