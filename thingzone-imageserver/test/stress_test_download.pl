# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;

# stress test to see how fast we can upload images

my $UPLOAD_URL = "http://127.0.0.1:8082/upload";
my $DOWNLOAD_URL = "http://127.0.0.1:8082/images";
my $UPLOAD_FILE = "images/P2161776.JPG";
#my $UPLOAD_FILE = "images/small.jpg";

my $total_images = 1000;
my $parallelism = 25;

use Image::Magick;
use JSON;
use Data::Dumper;

# upload test image

print "uploading file $UPLOAD_FILE\n";

my $response = `curl -s -X POST -F "file=\@$UPLOAD_FILE" $UPLOAD_URL`;
if ($response !~ /"result":"ok"/) {
	die "failed: response=$response\n";
}
my $response_json = decode_json($response) or die "failed to decode";
print Data::Dumper->Dump([$response_json], [qw(response)]);
	
die unless $$response_json{result} eq "ok";
my $image_metadata = $response_json;
my $key = $$image_metadata{name} or die;



my @children;


$SIG{CHLD} = 'DEFAULT';  # turn off auto reaper

my $remaining = $total_images;
for my $processes (1..$parallelism) {

	my $allocated = int ($remaining / ($parallelism-$processes+1));
	$remaining -= $allocated;
	
	my $pid = fork();
	if ($pid) {  # If I have a child PID, then I must be the parent
		push @children, $pid;
	} else { # I am the child
		worker($allocated);
		exit 0; # Exit the child
	}
}

# wait for forked children
while (getppid() != 1) {
	my $child = waitpid(-1, 0);
	last if $child == -1;	# no children
}

sub worker {
	my $count = shift or die;

	for (my $i=0;$i<$count;$i++) {	
	
		my $url = "$DOWNLOAD_URL/$key";
		my $result = `curl -s -w "%{http_code}" -o /dev/null $url`;
		print "result=$result\n";
		die unless $result eq "200";
	
		print "$$: downloaded #$i\n";
	}
	
	print "processed $count\n";
	print "Child $$ child exited\n";
}

