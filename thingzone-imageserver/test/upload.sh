#!/bin/bash
# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


UPLOAD_FILE=images/tick-mark_small.png
URL=http://127.0.0.1:8082/upload
FORM_NAME="file"

echo "uploading file $UPLOAD_FILE as form element \"$FORM_NAME\" to $URL"

curl -X POST -F "$FORM_NAME=@$UPLOAD_FILE" $URL


#curl -X POST -F "file=@$UPLOAD_FILE" http://127.0.0.1:8081/upload
