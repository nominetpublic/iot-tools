# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;

use JSON;
use Data::Dumper;
use File::Compare;

use test_common;


my $response;

print "\n======================================================\n";
print "\n$0: negative tests\n";

# negative tests

# upload "not an image"
print "\nTest: uploading 'not an image'\n";
$response = `curl -s -X POST -F "file=\@$0" $UPLOAD_URL`;
print "response=$response\n";
if ($response =~ /"result":"fail"/
	&& $response =~ /"reason":"failed to identify image properties"/) {
	print "PASS: expected response\n";
} else { die "not expected" }


# download non-existent
print "\nTest: downloading non-existent\n";
my $result = `curl -s -w "%{http_code}" -o $TEMP_DIR/nonexist $DOWNLOAD_URL/nonexistent`;
print "result=$result\n";
if ($result eq '404') {
	print "PASS: expected response\n";
} else { die "not expected" }

print "\n$0: all tests passed\n";


	


