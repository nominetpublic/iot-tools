#!/usr/bin/perl -w
use strict;

package test_common;

use base 'Exporter';

use Readonly;	# RPM=perl-Readonly
use Image::Magick;	# RPM=ImageMagick-perl

our @EXPORT = qw(
	$TEMP_DIR
	$BASE_URL
	$UPLOAD_URL
	$GRAB_URL
	$DOWNLOAD_URL
	$LIST_URL
	$IMAGE_DIR
	$VIDEO_URL
	$JSON_HEADER
	);

Readonly::Scalar our $BASE_URL => "http://127.0.0.1:8082";
Readonly::Scalar our $UPLOAD_URL =>"$BASE_URL/upload";
Readonly::Scalar our $GRAB_URL =>"$BASE_URL/grab";
Readonly::Scalar our $DOWNLOAD_URL => "$BASE_URL/images";
Readonly::Scalar our $LIST_URL => "$BASE_URL/list";

Readonly::Scalar our $IMAGE_DIR => 'images';
Readonly::Scalar our $TEMP_DIR => 'temp';
Readonly::Scalar our $VIDEO_URL => "$BASE_URL/videos";

Readonly::Scalar our $JSON_HEADER => '--header "Content-Type:application/json"';



1;
