# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;

use JSON;
use Data::Dumper;
use File::Compare;

use test_common;


#print ffmpeg_get_video_codecs();
my $FFMPEG_x264 = ffmpeg_has_x264_support();
my $FFMPEG_VP8 = ffmpeg_has_VP8_support();
my $FFMPEG_Theora = ffmpeg_has_Theora_support();
my $FFMPEG_MPEG4_Part2 = ffmpeg_has_MPEG4_Part2_support();

print "ffmpeg_has_x264_support = ".($FFMPEG_x264 ? "Y":"N")."\n";
print "ffmpeg_has_VP8_support = ".($FFMPEG_VP8 ? "Y":"N")."\n";
print "ffmpeg_has_Theora_support = ".($FFMPEG_Theora ? "Y":"N")."\n";
print "ffmpeg_has_MPEG4_Part2_support = ".($FFMPEG_MPEG4_Part2 ? "Y":"N")."\n";

unlink glob "$TEMP_DIR/*";

my @images = qw (IMG_0749.jpg IMG_0750.jpg IMG_0751.jpg IMG_0752.jpg IMG_0753.jpg IMG_0754.jpg );

my %image_metadata;

print "======================================================\n";
print "\n$0: video test: first upload images\n";


for my $image (@images) {
	chomp($image);
	
	my $upload_file = "$IMAGE_DIR/$image";
	print "uploading file $upload_file\n";

	my $response = `curl -s -X POST -F "file=\@$upload_file" $UPLOAD_URL`;
	if ($response !~ /"result":"ok"/) {
		die "failed: response=$response\n";
	}
	
	my $response_json = decode_json($response) or die "failed to decode";
	print Data::Dumper->Dump([$response_json], [qw(response)]);
	
	die unless $$response_json{result} eq "ok";
	$image_metadata{$image} = $response_json;
	
	print "\n";
}

print scalar(@images)." uploaded OK\n\n";
print "======================================================\n";


print "\n$0 - video test: request video generation\n";

#curl -X POST "http://127.0.0.1:8082/videos/compose" \
#	--header "Content-Type:application/json" \
#	--data '{ "width": 640, "height": 480, "frameRate" : 0.5, "frameSequence": [ {"key":"ABCD", "count":1}, {"key":"BCDE", "count":5}]}'


if ($FFMPEG_x264) {

	my $json_data=qq(--data '
	{ 
	"width": 640, "height": 480, 
	"inputFrameRate" : 2, 
	"outputFrameRate" : 30, 
	"container" : "mp4",
	"codec": "x264", 
	"frameSequence": [ 
		{"key":"${image_metadata{'IMG_0749.jpg'}{'name'}}", "count":2}, 
		{"key":"${image_metadata{'IMG_0750.jpg'}{'name'}}", "count":2}, 
		{"key":"${image_metadata{'IMG_0751.jpg'}{'name'}}", "count":2}, 
		{"key":"${image_metadata{'IMG_0752.jpg'}{'name'}}", "count":2}, 
		{"key":"${image_metadata{'IMG_0753.jpg'}{'name'}}", "count":2}, 
		{"key":"${image_metadata{'IMG_0754.jpg'}{'name'}}", "count":10}
	]}');

	my $response = `curl -s -X POST $VIDEO_URL/compose $JSON_HEADER $json_data`;
	if ($response !~ /"result":"ok"/) {
		die "failed: response=$response\n";
	}

	my $response_json = decode_json($response) or die "failed to decode";
	print Data::Dumper->Dump([$response_json], [qw(response)]);
	my $key = $$response_json{name} or die;


	my $download_video = "$DOWNLOAD_URL/$key";
	print "checking download as $download_video\n";

	# (-w "%{http_code}" gets curl to print HTTP status code)
	my $result = `curl -s -w "%{http_code}" -o $TEMP_DIR/$key $download_video`;
	print "result=$result\n";

	die unless $result eq "200";
}
else {
	print "SKIPPING - x264 encoding - not supported by installed ffmpeg build\n";
}


if ($FFMPEG_MPEG4_Part2) {


	print "\n$0 - video test: benny hill tribute\n";

	my $json_data=qq(--data '
	{ 
	"width": 640, "height": 480, 
	"inputFrameRate" : 10, 
	"outputFrameRate" : 30, 
	"container" : "avi",
	"codec": "FMP4", 
	"frameSequence": [ 
		{"key":"${image_metadata{'IMG_0749.jpg'}{'name'}}", "count":5}, 
		{"key":"${image_metadata{'IMG_0750.jpg'}{'name'}}", "count":5}, 
		{"key":"${image_metadata{'IMG_0751.jpg'}{'name'}}", "count":5}, 
		{"key":"${image_metadata{'IMG_0752.jpg'}{'name'}}", "count":5}, 
		{"key":"${image_metadata{'IMG_0753.jpg'}{'name'}}", "count":5}, 
		{"key":"${image_metadata{'IMG_0754.jpg'}{'name'}}", "count":5},
		{"key":"${image_metadata{'IMG_0753.jpg'}{'name'}}", "count":5}, 
		{"key":"${image_metadata{'IMG_0752.jpg'}{'name'}}", "count":5}, 
		{"key":"${image_metadata{'IMG_0751.jpg'}{'name'}}", "count":5}, 
		{"key":"${image_metadata{'IMG_0750.jpg'}{'name'}}", "count":5}, 
		{"key":"${image_metadata{'IMG_0749.jpg'}{'name'}}", "count":10}, 
		{"key":"${image_metadata{'IMG_0750.jpg'}{'name'}}", "count":1}, 
		{"key":"${image_metadata{'IMG_0751.jpg'}{'name'}}", "count":1}, 
		{"key":"${image_metadata{'IMG_0752.jpg'}{'name'}}", "count":1}, 
		{"key":"${image_metadata{'IMG_0753.jpg'}{'name'}}", "count":1}, 
		{"key":"${image_metadata{'IMG_0754.jpg'}{'name'}}", "count":1},
		{"key":"${image_metadata{'IMG_0753.jpg'}{'name'}}", "count":1}, 
		{"key":"${image_metadata{'IMG_0752.jpg'}{'name'}}", "count":1}, 
		{"key":"${image_metadata{'IMG_0751.jpg'}{'name'}}", "count":1}, 
		{"key":"${image_metadata{'IMG_0750.jpg'}{'name'}}", "count":1}, 
		{"key":"${image_metadata{'IMG_0749.jpg'}{'name'}}", "count":10}
	]}');

	my $response = `curl -s -X POST $VIDEO_URL/compose $JSON_HEADER $json_data`;
	if ($response !~ /"result":"ok"/) {
		die "failed: response=$response\n";
	}

	my $response_json = decode_json($response) or die "failed to decode";
	print Data::Dumper->Dump([$response_json], [qw(response)]);
	my $key = $$response_json{name} or die;


	my $download_video = "$DOWNLOAD_URL/$key";
	print "checking download as $download_video\n";

	# (-w "%{http_code}" gets curl to print HTTP status code)
	my $result = `curl -s -w "%{http_code}" -o $TEMP_DIR/$key $download_video`;
	print "result=$result\n";

	die unless $result eq "200";
}
else {
	print "SKIPPING - MPEG4 encoding - not supported by installed ffmpeg build\n";
}




print "\n$0: all tests passed\n";


sub ffmpeg_get_codecs {
	return `ffmpeg -v 0 -codecs`;
}

sub ffmpeg_get_video_codecs {
	return grep {/^..EV/} ffmpeg_get_codecs;
}

sub ffmpeg_has_x264_support {
	return (grep {/ h264   /} ffmpeg_get_video_codecs()) ? 1 : undef;
}
sub ffmpeg_has_VP8_support {
	return (grep {/ vp8   /} ffmpeg_get_video_codecs()) ? 1 : undef;
}
sub ffmpeg_has_Theora_support {
	return (grep {/ theora   /} ffmpeg_get_video_codecs()) ? 1 : undef;
}
sub ffmpeg_has_MPEG4_Part2_support {
	return (grep {/ mpeg4   /} ffmpeg_get_video_codecs()) ? 1 : undef;
}
	


