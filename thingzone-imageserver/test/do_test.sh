#!/bin/bash
# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#



# not using ./test_video.pl because not sure that ffmpeg is installed everywhere


./test_upload.pl \
	&& ./test_grab.pl \
	&& ./test_bogus.pl \
	&& ./test_resize.pl \
	&& ./test_video.pl \
	&& ./test_video_negative.pl \
	&& echo "" && echo "test suite PASS"

