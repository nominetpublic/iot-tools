# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;

use JSON;
use Data::Dumper;
use File::Compare;

use test_common;

mkdir $TEMP_DIR;
unlink glob "$TEMP_DIR/*";

my @images = sort `ls $IMAGE_DIR`;

my %image_metadata;

print "======================================================\n";
print "\n$0: upload/download/list tests\n";


for my $image (@images) {
	chomp($image);
	
#	UPLOAD_FILE=tick-mark_small.png
#	URL=http://127.0.0.1:8081/upload
#	ORM_NAME="file"
	
	my $upload_file = "$IMAGE_DIR/$image";
	print "uploading file $upload_file\n";

	#echo "uploading file $UPLOAD_FILE as form element \"$FORM_NAME\" to $URL"

	my $response = `curl -s -X POST -F "file=\@$upload_file" $UPLOAD_URL`;
#	print "response=$response\n";
	
	if ($response !~ /"result":"ok"/) {
		die "failed: response=$response\n";
	}
	
#	print "uploaded: response=$response\n";
	my $response_json = decode_json($response) or die "failed to decode";
#	print Dumper($json);
	print Data::Dumper->Dump([$response_json], [qw(response)]);
	
	die unless $$response_json{result} eq "ok";
	$image_metadata{$image} = $response_json;
	
	print "\n";
}

print scalar(@images)." uploaded OK\n\n";
print "======================================================\n";

# read back
while (my ($image, $metadata) = each %image_metadata) {
	my $key = $$metadata{name};
	print "retrieving '$image' ($key)\n";
	
	# (-w "%{http_code}" gets curl to print HTTP status code)
	my $result = `curl -s -w "%{http_code}" -o $TEMP_DIR/$key $DOWNLOAD_URL/$key`;
	print "result=$result\n";
	
	die unless $result eq "200";

	if (compare("$IMAGE_DIR/$image","$TEMP_DIR/$key") != 0) {
		print "compared $IMAGE_DIR/$image & $TEMP_DIR/$key\n";
		die "returned file didn't match";
	}
	print "PASS: result file $TEMP_DIR/$key matched input file $IMAGE_DIR/$image\n";
}


print scalar(@images)." downloaded OK\n\n";

print "======================================================\n";
print "list test\n";

my $response = `curl -s $LIST_URL`;
#print "response=$response\n";
if ($response !~ /"result":"ok"/) {
	die "failed: response=$response\n";
}
my $response_json = decode_json($response) or die "failed to decode";
#print Data::Dumper->Dump([$response_json], [qw(response)]);
die unless $$response_json{result} eq "ok";

print "got list containing ".@{@$response_json{images}}." images\n";


# check they're all there
while (my ($image, $metadata) = each %image_metadata) {
	my $key = $$metadata{name};
	if (grep (/$key/, @{@$response_json{images}}) == 1) {
		print "found $key in list\n";
	} else { die "missing image $key" }
}
print "PASS: list contained all the files we uploaded\n";


print "\n$0: all tests passed\n";



	


