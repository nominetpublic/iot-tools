# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#!/usr/bin/perl -w
use strict;

use JSON;
use Data::Dumper;
use File::Compare;

use test_common;


unlink glob "$TEMP_DIR/*";

my @images = sort `ls $IMAGE_DIR`;

my %image_metadata;

print "======================================================\n";
print "\n$0: resize tests\n";


my $image_name = "Numbat.jpg";
my $png_image_name = "IMG_5525.png";

my $upload_file = "$IMAGE_DIR/$image_name";
print "uploading file $upload_file\n";

my $response = `curl -s -X POST -F "file=\@$upload_file" $UPLOAD_URL`;
if ($response !~ /"result":"ok"/) {
	die "failed: response=$response\n";
}
my $response_json = decode_json($response) or die "failed to decode";
print Data::Dumper->Dump([$response_json], [qw(response)]);
	
die unless $$response_json{result} eq "ok";
my $image_metadata = $response_json;
my $key = $$image_metadata{name} or die;



# also upload png
$upload_file = "$IMAGE_DIR/$png_image_name";
print "uploading file $upload_file\n";

$response = `curl -s -X POST -F "file=\@$upload_file" $UPLOAD_URL`;
if ($response !~ /"result":"ok"/) {
	die "failed: response=$response\n";
}
$response_json = decode_json($response) or die "failed to decode";
print Data::Dumper->Dump([$response_json], [qw(response)]);
	
die unless $$response_json{result} eq "ok";
$image_metadata = $response_json;
my $png_key = $$image_metadata{name} or die;



print "uploaded OK\n\n";
print "======================================================\n";

my ($url, $result, $file, $img);


# read back

$url = "$DOWNLOAD_URL/$key";
print "retrieving original '$image_name' ($url)\n";
	
$result = `curl -s -w "%{http_code}" -o $TEMP_DIR/$key.jpg $url`;
print "result=$result\n";
die unless $result eq "200";


$url = "$DOWNLOAD_URL/$key/original";
print "retrieving original '$image_name' ($url)\n";
	
$result = `curl -s -w "%{http_code}" -o $TEMP_DIR/$key.original.jpg $url`;
print "result=$result\n";
die unless $result eq "200";
	
print "======================================================\n";


# read back

$url = "$DOWNLOAD_URL/$key/r64x64";
$file = "$TEMP_DIR/$key.r64x64.jpg";
print "retrieving thumbnail '$image_name' ($url) to $file\n";
$result = `curl -s -w "%{http_code}" -o $file $url`;
print "result=$result\n";
die unless $result eq "200";
$img=Image::Magick->New();
$img->Read($file);
my ($width, $height) = $img->Get('width', 'height');
print "width, height = $width, $height\n";
die unless $width==64 && $height==48;	# NB not stretched


$url = "$DOWNLOAD_URL/$key/r128x32";
$file = "$TEMP_DIR/$key.r128x32.jpg";
print "retrieving thumbnail '$image_name' ($url) to $file\n";
$result = `curl -s -w "%{http_code}" -o $file $url`;
print "result=$result\n";
die unless $result eq "200";
$img=Image::Magick->New();
$img->Read($file);
($width, $height) = $img->Get('width', 'height');
print "width, height = $width, $height\n";
die unless $width==43 && $height==32;	# NB not stretched


$url = "$DOWNLOAD_URL/$key/r32x128";
$file = "$TEMP_DIR/$key.r32x128.jpg";
print "retrieving thumbnail '$image_name' ($url) to $file\n";
$result = `curl -s -w "%{http_code}" -o $file $url`;
print "result=$result\n";
die unless $result eq "200";
$img=Image::Magick->New();
$img->Read($file);
($width, $height) = $img->Get('width', 'height');
print "width, height = $width, $height\n";
die unless $width==32 && $height==24;	# NB not stretched

# with teal border
$url = "$DOWNLOAD_URL/$key/r64x64_b006E7B";
$file = "$TEMP_DIR/$key.r64x64_b006E7B.jpg";
print "retrieving thumbnail '$image_name' ($url) to $file\n";
$result = `curl -s -w "%{http_code}" -o $file $url`;
print "result=$result\n";
die unless $result eq "200";
$img=Image::Magick->New();
$img->Read($file);
($width, $height) = $img->Get('width', 'height');
print "width, height = $width, $height\n";
die unless $width==64 && $height==64;	# NB fills space



# with raspberry border
$url = "$DOWNLOAD_URL/$key/r64x64_bF4364C";
$file = "$TEMP_DIR/$key.r64x64_bF4364C.jpg";
print "retrieving thumbnail '$image_name' ($url) to $file\n";
$result = `curl -s -w "%{http_code}" -o $file $url`;
print "result=$result\n";
die unless $result eq "200";
$img=Image::Magick->New();
$img->Read($file);
($width, $height) = $img->Get('width', 'height');
print "width, height = $width, $height\n";
die unless $width==64 && $height==64;	# NB fills space


# with transparent border (using png)
$url = "$DOWNLOAD_URL/$png_key/r64x64_btrans";
$file = "$TEMP_DIR/$png_key.r64x64_btrans.png";
print "retrieving thumbnail '$png_image_name' ($url) to $file\n";
$result = `curl -s -w "%{http_code}" -o $file $url`;
print "result=$result\n";
die unless $result eq "200";
$img=Image::Magick->New();
$img->Read($file);
($width, $height) = $img->Get('width', 'height');
print "width, height = $width, $height\n";
die unless $width==64 && $height==64;	# NB fills space

print "======================================================\n";

print "\n$0: all tests passed\n";



	


