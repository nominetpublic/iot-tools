<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		 xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>nominet.iot</groupId>
	<artifactId>thingzone-imageserver</artifactId>
	<version>1.0-SNAPSHOT</version>
	<packaging>jar</packaging>

	<name>Image server</name>
	<url>http://maven.apache.org</url>

	<parent>
		<groupId>nominet.iot</groupId>
		<artifactId>iot-tools</artifactId>
		<version>1.0-SNAPSHOT</version>
	</parent>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

		<!-- <jetty.version>9.1.2.v20140210</jetty.version> -->
		<jetty.version>9.2.7.v20150116</jetty.version>

		<jersey.version>2.15</jersey.version>

		<dockerfile-maven-version>1.4.7</dockerfile-maven-version>
	</properties>


	<dependencies>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.11</version>
			<scope>test</scope>
		</dependency>


		<dependency>
			<groupId>org.eclipse.jetty</groupId>
			<artifactId>jetty-webapp</artifactId>
			<version>${jetty.version}</version>
		</dependency>

		<dependency>
			<groupId>org.eclipse.jetty</groupId>
			<artifactId>jetty-rewrite</artifactId>
			<version>${jetty.version}</version>
		</dependency>

		<dependency>
			<groupId>org.eclipse.jetty</groupId>
			<artifactId>jetty-jmx</artifactId>
			<version>${jetty.version}</version>
		</dependency>

		<dependency>
			<groupId>org.hamcrest</groupId>
			<artifactId>hamcrest-library</artifactId>
			<version>1.3</version>
		</dependency>

		<dependency>
			<groupId>nominet.iot</groupId>
			<artifactId>thingzone-commons-utils</artifactId>
			<version>1.0-SNAPSHOT</version>
		</dependency>

		<dependency>
			<groupId>com.google.guava</groupId>
			<artifactId>guava</artifactId>
			<version>18.0</version>
		</dependency>

		<dependency>
			<groupId>org.eclipse.jetty</groupId>
			<artifactId>jetty-server</artifactId>
			<version>${jetty.version}</version>
		</dependency>


		<!-- maybe not all required? -->
		<dependency>
			<groupId>org.glassfish.jersey.core</groupId>
			<artifactId>jersey-server</artifactId>
			<version>${jersey.version}</version>
		</dependency>
		<dependency>
			<groupId>org.glassfish.jersey.core</groupId>
			<artifactId>jersey-client</artifactId>
			<version>${jersey.version}</version>
		</dependency>
		<dependency>
			<groupId>org.glassfish.jersey.containers</groupId>
			<artifactId>jersey-container-servlet-core</artifactId>
			<version>${jersey.version}</version>
		</dependency>
		<dependency>
			<groupId>org.glassfish.jersey.containers</groupId>
			<artifactId>jersey-container-jetty-http</artifactId>
			<version>${jersey.version}</version>
		</dependency>
		<dependency>
			<groupId>org.glassfish.jersey.media</groupId>
			<artifactId>jersey-media-json-jackson</artifactId>
			<version>${jersey.version}</version>
		</dependency>

		<dependency>
			<groupId>org.glassfish.jersey.media</groupId>
			<artifactId>jersey-media-multipart</artifactId>
			<version>${jersey.version}</version>
		</dependency>

		<dependency>
			<groupId>org.im4java</groupId>
			<artifactId>im4java</artifactId>
			<version>1.4.0</version>
		</dependency>

		<!-- logging -->
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>1.7.5</version>
		</dependency>
		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-classic</artifactId>
			<version>1.0.13</version>
		</dependency>
		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-core</artifactId>
			<version>1.0.13</version>
		</dependency>


	</dependencies>

	<build>
		<plugins>
			<!-- using Java 8 -->
			<plugin>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>2.3.2</version>
				<configuration>
					<verbose>true</verbose>
					<fork>true</fork>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>

			<!-- produce a single jar -->
			<plugin>
				<artifactId>maven-assembly-plugin</artifactId>
				<version>2.4</version>
				<executions>
					<execution>
						<phase>package</phase>
						<goals>
							<goal>single</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<descriptorRefs>
						<descriptorRef>jar-with-dependencies</descriptorRef>
					</descriptorRefs>
				</configuration>
			</plugin>

			<plugin>
				<groupId>com.spotify</groupId>
				<artifactId>dockerfile-maven-plugin</artifactId>
				<version>${dockerfile-maven-version}</version>
				<executions>
					<execution>
						<id>default</id>
						<goals>
							<goal>build</goal>
							<goal>push</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<repository>thingzone-imageserver</repository>
					<tag>latest</tag>
				</configuration>
			</plugin>

		</plugins>

	</build>

	<profiles>
		<profile>
			<id>licence-test</id>
			<activation>
				<!--  activate with "-Dlicence-test=yes org.codehaus.mojo:license-maven-plugin:add-third-party" -->
				<property>
					<name>licence-test</name>
					<value>yes</value>
				</property>
			</activation>
			<build>
				<plugins>
					<plugin>
						<!-- license-testing plugin-->
						<!-- generates target/generated-sources/license/THIRD-PARTY.txt -->
						<groupId>org.codehaus.mojo</groupId>
						<artifactId>license-maven-plugin</artifactId>
						<version>1.14</version>
						<executions>
							<execution>
								<goals>
									<goal>download-licenses</goal>
									<goal>add-third-party</goal>
									<!--
										<goal>aggregate-download-licenses</goal>
										<goal>aggregate-add-third-party</goal>
									-->
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>

</project>

