#!/bin/bash
# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#



PGREP_FILTER=uk.nominet.research.imageserver.webserver.WebServerApp

pgrep -f $PGREP_FILTER > /dev/null 2>&1
RESULT=$?

if [ $RESULT -ne 0 ]; then
	echo "not running"
	exit 0
fi
echo "killing imageserver process"
pkill -f $PGREP_FILTER

while true; do 
	sleep 0.1
	pgrep -f $PGREP_FILTER > /dev/null 2>&1
	RESULT=$?

	if [ $RESULT -ne 0 ]; then
		echo "imageserver stopped"
		exit 0
	fi
	
	echo "waiting for imageserver to stop"
done
