#!/bin/bash
# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


if [ "$RUNNING_IN_DOCKER" == "" ]; then
    make
    RESULT=$?
    if [ $RESULT -ne 0 ]; then
        echo "make failed"
        exit $RESULT
    fi
fi

#  it not work when including in fat jar
JAR_BODGE=bodgery/jersey-media-json-jackson-2.15.jar

JAR=./target/thingzone-imageserver-1.0-SNAPSHOT-jar-with-dependencies.jar
CLASSPATH=logback.xml:$JAR:$JAR_BODGE

if [ "$IMAGESERVER_LOGBACK_CONFIG" == "" ]; then
	IMAGESERVER_LOGBACK_CONFIG=logback.xml
fi
LOGBACK=-Dlogback.configurationFile=$IMAGESERVER_LOGBACK_CONFIG

echo LOGBACK=$LOGBACK

JAVA_BIN=/usr/bin/java
if [ -x /usr/lib/jvm/jdk1.7.0 ]; then
	export JAVA_HOME=/usr/lib/jvm/jdk1.7.0
	JAVA_BIN=$JAVA_HOME/bin/java
fi

$JAVA_BIN -cp $CLASSPATH $LOGBACK uk.nominet.research.imageserver.webserver.WebServerApp

