/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.processor;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;



@RunWith(JUnit4.class)
public class VideoCodecTest {
	
	@Test
	public void testCompatibility() {
		assertTrue(VideoCodec.CODEC_THEORA.isCompatibleWith(VideoContainer.CONTAINER_OGG));
		assertTrue(VideoCodec.CODEC_VP8.isCompatibleWith(VideoContainer.CONTAINER_WEBM));
		assertTrue(VideoCodec.CODEC_VP8.isCompatibleWith(VideoContainer.CONTAINER_MATROSKA));
		assertTrue(VideoCodec.CODEC_x264.isCompatibleWith(VideoContainer.CONTAINER_MP4));
		assertTrue(VideoCodec.CODEC_x264.isCompatibleWith(VideoContainer.CONTAINER_AVI));
		assertTrue(VideoCodec.CODEC_MPEG4_PART2.isCompatibleWith(VideoContainer.CONTAINER_MP4));

		// not all enumerated here...
		assertFalse(VideoCodec.CODEC_THEORA.isCompatibleWith(VideoContainer.CONTAINER_AVI));
		assertFalse(VideoCodec.CODEC_x264.isCompatibleWith(VideoContainer.CONTAINER_WEBM));
	}

	// the reverse
	@Test
	public void testContainer() {
		assertTrue(VideoContainer.CONTAINER_OGG.isCompatibleWith(VideoCodec.CODEC_THEORA));
		assertFalse(VideoContainer.CONTAINER_WEBM.isCompatibleWith(VideoCodec.CODEC_x264));
	}

}
