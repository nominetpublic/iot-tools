/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.common;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import uk.nominet.iot.utils.error.ProgramDefectException;


@RunWith(JUnit4.class)
public class TaskExecutorTest {

	@Test
	public void testNonZeroStderr() {
		TaskExecutor te = new TaskExecutor(Arrays.asList("grep", "foo", "bar"));
		te.execute();
		
		assertEquals (2, te.getReturnCode());
		assertEquals ("grep: bar: No such file or directory\n", te.getStderr());
	}

	@Test
	public void testMultiline() {
		TaskExecutor te = new TaskExecutor(Arrays.asList("ls", "-l"));
		te.execute();
		
		assertEquals (0, te.getReturnCode());
	}

	@Test(expected = ProgramDefectException.class)
	public void testFailed() {
		TaskExecutor te = new TaskExecutor(Arrays.asList("nosuchprogram"));
		te.execute();
	}

}
