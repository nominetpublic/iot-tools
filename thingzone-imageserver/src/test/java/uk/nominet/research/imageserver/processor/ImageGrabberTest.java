/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.processor;

import org.junit.Ignore;
import org.junit.Test;
import uk.nominet.research.imageserver.common.DataException;

import java.io.InputStream;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

public class ImageGrabberTest {

	private final static String validUrl = "http://lorempixel.com/400/400/animals/2/";
	private final static String bogusUrl = "not_a_valid_url";
	private final static String nonexistentUrl = "http://lorempixel.com/thisdoesntexist/";


	@Ignore
	@Test
	public void downloadFromLoremIpsum() {
		ImageGrabber ig = new ImageGrabber(validUrl);
		InputStream is = ig.getStream();
	}

	@Test
	public void bogusUrlThrowsDataException() {
		try {
			ImageGrabber ig = new ImageGrabber(bogusUrl);
			InputStream is = ig.getStream();
			fail();
		} catch (DataException e) {
			assertThat(e.getMessage(), containsString("URI is not absolute"));
		}
	}

	@Ignore
	@Test
	public void nonexistentUrlThrowsDataException() {
		try {
			ImageGrabber ig = new ImageGrabber(nonexistentUrl);
			InputStream is = ig.getStream();
			fail();
		} catch (DataException e) {
			assertThat(e.getMessage(), containsString("404: Not Found"));
		}
	}

}
