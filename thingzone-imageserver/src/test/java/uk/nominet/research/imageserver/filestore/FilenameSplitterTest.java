/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.filestore;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import uk.nominet.iot.utils.error.ProgramDefectException;



@RunWith(JUnit4.class)
public class FilenameSplitterTest {

	@Test (expected=ProgramDefectException.class)
	public void inadequateLengthIsRefused() {
		FilenameSplitter.split("foo");
	}
	
	@Test
	public void sampleSplit() {
		String filename="ABCDEFGH.txt";
		List<String> split = FilenameSplitter.split(filename);
		assertEquals(3, split.size());
		assertEquals("AB", split.get(0));
		assertEquals("ABCD", split.get(1));
		assertEquals("ABCDEFGH.txt", split.get(2));
	}
	
	@Test
	public void directoriesAssembled() {
		List<String> parts = Arrays.asList("A", "AB", "ABC", "ABCD.txt");
		List<String> paths = FilenameSplitter.directories(parts);
		assertEquals(3, paths.size());
		assertEquals("A", paths.get(0));
		assertEquals("A/AB", paths.get(1));
		assertEquals("A/AB/ABC", paths.get(2));
		// we don't expect to get the file as well, i.e. "A/AB/ABC.txt" 
	}

	@Test
	public void filepathCreated() {
		List<String> parts = Arrays.asList("A", "AB", "ABC", "ABCD.txt");
		assertEquals("A/AB/ABC/ABCD.txt", FilenameSplitter.filepath(parts));
	}
}
