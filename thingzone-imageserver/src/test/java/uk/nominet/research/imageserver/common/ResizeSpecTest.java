/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.common;

import static org.junit.Assert.*;

import java.awt.Transparency;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;


@RunWith (JUnit4.class)
public class ResizeSpecTest {
	
	@Test(expected=DataException.class)
	public void bogusSpecThrows() {
		new ResizeSpec("this is bogus!");
	}
	
	@Test(expected=DataException.class)
	public void nullSpecRefused() {
		new ResizeSpec(null);
	}

	@Test(expected=DataException.class)
	public void emptySpecRefused() {
		new ResizeSpec("");
	}

	@Test
	public void originalIsParsed() {
		ResizeSpec rs = new ResizeSpec("original");
		assertTrue(rs.isOriginal());
	}

	@Test
	public void resizeIsParsed() {
		ResizeSpec rs = new ResizeSpec("r64x128");
		assertEquals(64, rs.getWidth());
		assertEquals(128, rs.getHeight());
		assertFalse(rs.hasBorder());
	}

	@Test
	public void borderedResizeIsParsed() {
		ResizeSpec rs = new ResizeSpec("r64x128_b00ff0A");
		assertEquals(64, rs.getWidth());
		assertEquals(128, rs.getHeight());
		assertTrue(rs.hasBorder());
		assertEquals(0, rs.getBorder().getRed());
		assertEquals(255, rs.getBorder().getGreen());
		assertEquals(10, rs.getBorder().getBlue());
		assertEquals(Transparency.OPAQUE, rs.getBorder().getTransparency());
	}


	@Test(expected = DataException.class)
	public void bogusResizeIsRejected() {
		new ResizeSpec("r64x128_b00ff0g");
	}

	@Test
	public void transparentBorderedResizeIsParsed() {
		ResizeSpec rs = new ResizeSpec("r64x128_btrans");
		assertEquals(64, rs.getWidth());
		assertEquals(128, rs.getHeight());
		assertTrue(rs.hasBorder());
		assertEquals(Transparency.BITMASK, rs.getBorder().getTransparency());
		assertTrue(ResizeSpec.isTransparent(rs.getBorder()));
	}

}
