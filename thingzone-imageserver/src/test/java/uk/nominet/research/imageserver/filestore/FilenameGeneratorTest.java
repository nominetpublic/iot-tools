/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.filestore;

import static org.junit.Assert.*;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import uk.nominet.research.imageserver.filestore.FilenameGenerator;

@RunWith(JUnit4.class)
public class FilenameGeneratorTest {

	@Test
	public void keyIsCorrectLength() {
		String key = FilenameGenerator.generateKey();
		assertEquals(FilenameGenerator.KEY_BITS / FilenameGenerator.BITS_PER_BASE32, key.length());
		assertEquals(11, key.length());
	}
	
	@Test
	public void uidsAreDifferent() {
		String key1 = FilenameGenerator.generateKey();
		String key2 = FilenameGenerator.generateKey();
		assertNotEquals(key1, key2);
	}


	@Test
	public void validNameIsPlausible() {
		assertEquals(true, FilenameGenerator.isPlausible("RFNX66WTJXN"));
	}

	@Test
	public void shortNameNotPlausible() {
		assertEquals(false, FilenameGenerator.isPlausible("RFN"));
	}

	@Test
	public void longNameNotPlausible() {
		assertEquals(false, FilenameGenerator.isPlausible("RFNX66WTJXNRFNX66WTJXN"));
	}
	
	@Test
	public void invalidBase32NotPlausible() {
		assertEquals(false, FilenameGenerator.isPlausible("RFNX_6WTJXN"));
	}
	
}
