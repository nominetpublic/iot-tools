/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.common;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

import org.im4java.core.ConvertCmd;
import org.im4java.core.IM4JavaException;
import org.im4java.core.IMOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImageResizer {
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	ResizeSpec spec;
	String inFilename;
	String outFilename;
	
	public ImageResizer(ResizeSpec resizeSpec, String inFilename, String outFilename) {
		this.spec = resizeSpec;
		this.inFilename = inFilename;
		this.outFilename = outFilename;
	}
	
	public void resize() {
			ConvertCmd cmd = new ConvertCmd();
			log.info("resizing {}", spec);

			
			IMOperation op = new IMOperation();
			op.addImage(inFilename);
			op.strip();
			op.resize(spec.getWidth(),spec.getHeight());
			if (spec.hasBorder()) {
				log.info("adding border {}", spec.getBorder());
				if (ResizeSpec.isTransparent(spec.getBorder())) {
					op.background("none");
				}
				else {
					// don't add quotes; they're added automatically
					op.background("#"+ResizeSpec.toHtmlFormat(spec.getBorder()));
				}
				op.gravity("center");
				op.extent(spec.getWidth(),spec.getHeight());
			}
			op.addImage(outFilename);
			try {
//				cmd.createScript("im4java_debug.sh",op);
				cmd.run(op);
			} catch (IOException | InterruptedException | IM4JavaException e) {
				throw new DataException ("failed to resize "+inFilename +" to "+spec, e);
			}
	}

}
