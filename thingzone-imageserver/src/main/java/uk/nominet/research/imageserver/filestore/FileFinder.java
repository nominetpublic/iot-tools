/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.filestore;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Splitter;

import uk.nominet.research.imageserver.common.TaskExecutor;
import uk.nominet.iot.utils.error.ProgramDefectException;

public class FileFinder {
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	//$ find ../data -name "*.meta" -not -iwholename "*incoming*" -printf "%f\n"
	// (but don't need quotes because not going via shell)
	public List<String> getFiles(String directory) {
		
		log.info("searching for all files in {} ...", directory);
		
		TaskExecutor te = new TaskExecutor(Arrays.asList(
			"find", directory, 
			"-name", "*.meta", 
			"-not", "-iwholename", "*incoming*",
			"-printf", "%f" 
			));
		te.execute();
		if (te.getReturnCode() != 0) {
			log.error ("find command failed: stdout={}, stderr={}", te.getStdout(), te.getStderr());
			throw new ProgramDefectException ("bad return code from \"find\"");
		}
//		log.info("stdout={}", te.getStdout());
//		log.info("stderr={}", te.getStderr());

		List<String> files = new ArrayList<>();
		for (String s: Splitter.on('\n').trimResults().omitEmptyStrings().split(te.getStdout())) {
//			log.info("found: {}", s);
			// remove the ".meta"
			files.add (s.substring(0, s.lastIndexOf('.')));
		}
		log.info("found {} images", files.size());
		return files;
	}
	
	
	
}
