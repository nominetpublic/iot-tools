/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.common;

import java.util.*;

import uk.nominet.iot.utils.config.ValidatingConfigProperties;

public class ImageServerProperties extends ValidatingConfigProperties implements ImageServerConfig {
	
	final static String PROPNAME_STORE_DIRECTORY = "StoreDirectory";
	final static String PROPNAME_TEMP_DIRECTORY = "TempDirectory";
	
    ImageServerProperties(Properties properties) {
        // superclass will validate, calling our overridden methods
        super(properties);
    }
    
    private final static String[] INTEGER_PROPERTY_NAMES = new String[] {
    	// none
    };
    private final static String[] STRING_PROPERTY_NAMES = new String[] {
    	PROPNAME_STORE_DIRECTORY, 
    	PROPNAME_TEMP_DIRECTORY 
    };

    @Override
    protected String[] getIntegerPropertyNames() {
        return INTEGER_PROPERTY_NAMES;
    }

    @Override
    protected String[] getStringPropertyNames() {
        return STRING_PROPERTY_NAMES;
    }

    @Override
	public String getStoreDirectory() {
		return FileUtils.stripTrailingSlash(stringProps.get(PROPNAME_STORE_DIRECTORY)) +"/";
    }
    @Override
	public String getTempDirectory() {
		return FileUtils.stripTrailingSlash(stringProps.get(PROPNAME_TEMP_DIRECTORY)) +"/";
    }
    
}
