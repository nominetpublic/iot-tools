/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.common;

import java.io.File;
import java.lang.invoke.MethodHandles;
import java.nio.file.Files;
import java.nio.file.LinkOption;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// a File wrapper that cleans up on close
public class TemporaryFile extends File implements AutoCloseable {
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	private static final long serialVersionUID = -7986034608089829523L;

	public TemporaryFile(String pathname) {
		super(pathname);
	}

	// if file exists, delete it
	@Override
	public void close() throws Exception {
		try {
//			log.info("name {}", this.getName());
//			log.info("exists() {}", exists());
//			log.info("isFile() {}", isFile());
//			log.info("isSymbolicLink() {}", Files.isSymbolicLink(toPath()));
//			log.info("isDirectory() {}", Files.isDirectory(toPath()));
//			log.info("isDirectory(LinkOption.NOFOLLOW_LINKS) {}", Files.isDirectory(toPath(), LinkOption.NOFOLLOW_LINKS));
			
			if (exists() && isFile()) {
				delete();
				log.info("cleaned up temp file {}", getName());
			} else if (Files.isSymbolicLink(toPath()) && ! Files.isDirectory(toPath(), LinkOption.NOFOLLOW_LINKS)) {
				delete();
				log.info("cleaned up temp link {}", getName());
			}
		} catch (Exception e) {
			// not serious, but surprising
			log.warn("exception deleting file: " + getName(), e);
		}

	}
}
