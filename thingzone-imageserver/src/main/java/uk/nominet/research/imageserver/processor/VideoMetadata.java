/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.processor;

public class VideoMetadata extends Metadata {

	
	// not createable
	protected VideoMetadata(String fileKey) {
		super(fileKey);
	}
	protected VideoMetadata(String fileKey, String type) {
		super(fileKey, type);
	}
	
	static VideoMetadata fromFile(String fileKey) {
		VideoMetadata m = new VideoMetadata(fileKey);
		m.load();
		return m;
	}

	static VideoMetadata create(String fileKey, String type) {
		return new VideoMetadata(fileKey, type);
	}
	
	@Override protected String getPropfileComment() {
    	return "Video metadata";
    }

	final static String PROP_CONTAINER = "container";
	final static String PROP_CODEC = "codec";
	final static String PROP_INPUT_FRAMERATE = "inputFrameRate";
	final static String PROP_OUTPUT_FRAMERATE = "outputFrameRate";
	final static String PROP_LENGTH_FRAMES = "lengthFrames";

	public String getContainer() {
		return getProperty(PROP_CONTAINER);
	}
	public void setContainer(String container) {
		setProperty(PROP_CONTAINER, container);
	}
	
	public String getCodec() {
		return getProperty(PROP_CODEC);
	}
	public void setCodec(String codec) {
		setProperty(PROP_CODEC, codec);
	}
	

	public String getInputFrameRate() {
		return getProperty(PROP_INPUT_FRAMERATE);
	}
	public void setInputFrameRate(String framerate) {
		setProperty(PROP_INPUT_FRAMERATE, framerate);
	}
	public String getOutputFrameRate() {
		return getProperty(PROP_OUTPUT_FRAMERATE);
	}
	public void setOutputFrameRate(String framerate) {
		setProperty(PROP_OUTPUT_FRAMERATE, framerate);
	}
	

	public String getLengthFrames() {
		return getProperty(PROP_LENGTH_FRAMES);
	}
	public void setLengthFrames(String lengthFrames) {
		setProperty(PROP_LENGTH_FRAMES, lengthFrames);
	}
	

}
