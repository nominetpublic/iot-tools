/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.processor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.invoke.MethodHandles;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.ByteStreams;


import uk.nominet.research.imageserver.common.DataException;
import uk.nominet.research.imageserver.common.TaskExecutor;
import uk.nominet.research.imageserver.common.TemporaryFile;
import uk.nominet.research.imageserver.common.TemporaryFiles;
import uk.nominet.research.imageserver.filestore.FileStore;
import uk.nominet.research.imageserver.filestore.FilenameGenerator;
import uk.nominet.research.imageserver.webserver.api.support.VideoComposeParams;
import uk.nominet.research.imageserver.webserver.api.support.VideoComposeParams.Frame;
import uk.nominet.iot.utils.error.ProgramDefectException;

public class VideoComposer {
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	// compose video:
	// 0: check source frames exist
	// 1: generate frame sequence (in temp dir)
	// 2. generate links to frames (in temp dir)
	// 3. generate video (in incoming dir)
	// 4. move to final location, create metadata

	public ComposedVideoDetails compose(VideoComposeParams params) {
		try {
			params.validate();
		} catch (DataException e) {
			// errors we we expected
			log.info("invalid video compose params: {}", e.getMessage());
			return ComposedVideoDetails.failed(e.getMessage());
		}
		// key for final video (we'll use this in temp files)
		String fileKey = FilenameGenerator.generateKey();
		String frameFileExtension = null;

		String resizeSpec = String.format("r%dx%d", params.getWidth(), params.getHeight());
		
		String metadataFilename = FileStore.getIncomingFilename(fileKey, VideoMetadata.FILE_SUFFIX+".tmp");

		for (VideoComposeParams.Frame f: params.frameSequence) {
			if (!ImageDownloader.exists(f.key, "original")) {
				return ComposedVideoDetails.failed("not found: "+f.key);
			}
		}

		// keep track of all the files we create, for clean-up
		try (TemporaryFiles tempFiles = new TemporaryFiles();
				FileOutputStream metadataStream = new FileOutputStream(metadataFilename)
				){
		
			// 1. resize into temp directory & generate links
			int videoFrameIndex = 0;	// frame in final video (as opposed to source sequence)
	
			for (int sourceIndex = 0; sourceIndex < params.frameSequence.size(); sourceIndex++) {
				Frame frame = params.frameSequence.get(sourceIndex);
					
				// generate resized image for frame
				ImageDownloader dl = new ImageDownloader(frame.key, resizeSpec);
				String frameFilename = FileStore.getTempFilename(fileKey, "_" + Integer.toString(sourceIndex)+"_"+frame.key+"." + dl.getFileExtension());
				dl.generateResizedImage(frameFilename);
				tempFiles.add(new TemporaryFile(frameFilename));
				 
				// create links to repeated frames
				for (int repeat=0 ; repeat<frame.getCount(); repeat++) {
					String linkFile = FileStore.getTempFilename(fileKey, 
							"_"+String.format("%05d", videoFrameIndex++)
							+ "."+dl.getFileExtension());
					Path link = Files.createSymbolicLink(new File(linkFile).toPath(), new File(frameFilename).getAbsoluteFile().toPath());
					tempFiles.add(new TemporaryFile(link.toString()));
				}

				log.info("created frame {} and {} symlinks", frameFilename, frame.getCount());

				// remember extension so we can tell ffmpeg
				// NB ffmpeg is fussy about file extensions - won't operate without them
				if (frameFileExtension==null) { frameFileExtension = dl.getFileExtension(); }
			}
			
			// generate video
			String framePattern = 
					FileStore.getTempFilename(
							fileKey, 
							"_%05d."+frameFileExtension);	// NB ffmpeg understands printf-format wildcard
			
			String videoFileExtension = params.getContainer().fileExtension();
			String tempVideoFilename = FileStore.getIncomingFilename(
					fileKey, ".tmp" + "." + videoFileExtension);

			List<String> args = new ArrayList<>();
			args.addAll(Arrays.asList(
				"ffmpeg",
				"-framerate", params.getInputFrameRate(),
				"-i", framePattern,
				"-c:v", params.getCodec().ffmpegCodec()
				)); //"libx264
			args.addAll(params.getCodec().ffmpegCodecOptions());
			args.addAll(Arrays.asList(
					"-r", params.getOutputFrameRate(), //"30", 
					"-pix_fmt", "yuv420p",
					tempVideoFilename
					));
			TaskExecutor task = new TaskExecutor(args);					
			
			task.execute();
			
			if (task.getReturnCode() == 0) {
				log.info("video generated successfully");
				log.debug("ffmpeg stdout={}", task.getStdout());
				log.debug("ffmpeg stderr={}", task.getStderr());
			} else {
				log.warn("video generation failed - returned {}", task.getReturnCode());
				log.warn("ffmpeg stdout={}", task.getStdout());
				log.warn("ffmpeg stderr={}", task.getStderr());
				throw new ProgramDefectException ("failed to generate video");
			}

			
			// create metadata file
			VideoMetadata metadata = new VideoMetadata (fileKey, params.getContainer().toString());
			metadata.setCodec(params.getCodec().toString());
			metadata.setContainer(params.getContainer().toString());
			metadata.setWidth(params.getWidth());
			metadata.setHeight(params.getHeight());
			metadata.setInputFrameRate(params.getInputFrameRate());
			metadata.setOutputFrameRate(params.getOutputFrameRate());
			metadata.persist(metadataStream);
			
			// move metadata
			FileStore.moveToStore(new File(metadataFilename), fileKey, VideoMetadata.FILE_SUFFIX);
			
			// move video to final location
			FileStore.moveToStore(new File(tempVideoFilename), fileKey, "."+videoFileExtension);

			// clean up sources 
			// (no code required - will be done by try-with-resources :-)
			
			return new ComposedVideoDetails(fileKey);
		} catch (DataException e) {
			// errors we we expected
			log.info("caught DataException & returning error to client: {}", e.getMessage());
			return ComposedVideoDetails.failed(e.getMessage());
		} catch (Exception e) {
			// errors we didn't expect
			log.error("caught error: "+e.getMessage(), e);
			return ComposedVideoDetails.failed("internal error");
		}
		
	}	

	
	void storeFile(File file, InputStream inputStream) throws FileNotFoundException, IOException {
		try (OutputStream outputStream = new FileOutputStream(file)){
			ByteStreams.copy(inputStream, outputStream);
			outputStream.flush();
			outputStream.close();
		}
	}

}
