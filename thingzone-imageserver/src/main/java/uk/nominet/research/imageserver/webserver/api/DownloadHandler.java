/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.webserver.api;

import java.io.InputStream;
import java.lang.invoke.MethodHandles;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nominet.research.imageserver.common.DataException;
import uk.nominet.research.imageserver.processor.ImageDownloader;

@Path("/images")

public class DownloadHandler {
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	@Context
    UriInfo url;

    @Context
    Request request;
	
    @Context
    private javax.servlet.http.HttpServletRequest hsr;    

    // raw image
    @GET
    @Path("{imageKey}")
	public Response imageFile(@PathParam("imageKey") String imageKey) {
		log.info("GET [images/{}]", imageKey);
		return supplyImage(imageKey, "original");
    }
    	

    // provide /original 
    @GET
    @Path("{imageKey}/original")
	public Response imageFileOriginal(@PathParam("imageKey") String imageKey) {
		log.info("GET [images/{}/original]", imageKey);
		return supplyImage(imageKey, "original");
    }
    
    
    // resized image
    @GET
    @Path("{imageKey}/{resizeSpec}")
	public Response imageFileResized(
			@PathParam("imageKey") String imageKey,
			@PathParam("resizeSpec") String resizeSpec
			) {
		log.info("GET [images/{}/{}]", imageKey, resizeSpec);
		return supplyImage(imageKey, resizeSpec);
    }
    
    // common error handling & call to image downloader
	Response supplyImage(String imageKey, String resizeSpec) {
    	
		InputStream imageStream=null;	// manual close needed due to return
		try { 
			ImageDownloader imageDownloader = new ImageDownloader(imageKey, resizeSpec);
			String imageType = imageDownloader.getImageType();
			imageStream = imageDownloader.download();
			
			return Response.ok()
					.type("image/"+imageType)
					.entity(imageStream)
					.build();
		} 
		catch (Exception e) {
			log.info("caught {} & returning NOT_FOUND to client: {}",
					e.getClass().getSimpleName(), e.getMessage());
			if (!(e instanceof DataException)) {
				log.error(e.getMessage(), e);
			}
	
			if (imageStream != null) {
				try{
					imageStream.close();
				} catch (Exception e2) {}
			}
			return Response.status(Status.NOT_FOUND)
					.build();
		}
	}
}
