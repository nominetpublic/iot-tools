/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.processor;

import java.io.File;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import uk.nominet.research.imageserver.common.DataException;
import uk.nominet.research.imageserver.common.ImageResizer;
import uk.nominet.research.imageserver.common.ResizeSpec;
import uk.nominet.research.imageserver.common.TemporaryFile;
import uk.nominet.research.imageserver.filestore.FileStore;
import uk.nominet.iot.utils.error.ProgramDefectException;

public class ImageDownloader {
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	String fileKey;
	ResizeSpec resizeSpec;
	Metadata metadata;
	
	public ImageDownloader(String fileKey, String resizeSpec) {
		this.fileKey = fileKey;
		
		if (!FileStore.isPlausibleKey(fileKey)) {
			throw new DataException ("bogus name \""+fileKey+"\"");
		}

		// parse resize spec (if any)
		this.resizeSpec = new ResizeSpec(resizeSpec);

		metadata = Metadata.fromFile(fileKey);
	}

	// original image, e.g. "foo.jpg"
	String originalImageSuffix() {
		return "."+getImageType();
	}

	// resized, e.g. "foo_r64x64.jpg"
	String resizedImageSuffix() {
		return "_" + resizeSpec.getFileSpec() + originalImageSuffix();
	}

    // provide stream for file download    
	public InputStream download() {
		if (resizeSpec.isOriginal()) {
			return downloadOriginal();
		} else {
			return downloadResized();
		}
	}	
	
	InputStream downloadOriginal() {
		return FileStore.streamFile(fileKey, originalImageSuffix());
	}

	InputStream downloadResized() {
		String suffix = resizedImageSuffix();
		if (!FileStore.fileExists(fileKey, suffix)) {
			generateResizedImage();
		}
		return FileStore.streamFile(fileKey, suffix);
	}	

	// generate a file & move to store 	
	void generateResizedImage() {
		// store in "incoming" for now 
		String temporaryKey = FileStore.generateKey();
		String tempSuffix = "_resize";
		String tempFilename = FileStore.getTempFilename(temporaryKey, tempSuffix);

		// use temp files to auto-delete on exception
		try (TemporaryFile tempFile = new TemporaryFile(tempFilename);
				) {
			
			// resize
			generateResizedImage(tempFilename);
			
			// move (atomically) to store
			File dest = FileStore.moveToStore(tempFile, fileKey, resizedImageSuffix());
			
			log.info("resized {} to {} to create {}", fileKey, resizeSpec, dest);
						
		} catch (Exception e) {
			throw new ProgramDefectException ("failed to resize", e);
		}
		
	}

	// just generate file with specified name
	void generateResizedImage(String outputFilename) {
		String originalFile = FileStore.getStoreFilename(fileKey, originalImageSuffix());
		new ImageResizer(resizeSpec, originalFile, outputFilename).resize();
	}


	public String getImageType() {
		return metadata.getType();
	}
	public String getFileExtension() {
		return metadata.getType();
	}

	//--------------------------------------------------
	// support for video generation
	public static boolean exists(String fileKey, String resizeSpec) {
		try {
			new ImageDownloader(fileKey, resizeSpec);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	public void resizeFrameForVideo(String frameFilename) {
		try (TemporaryFile frameFile = new TemporaryFile(frameFilename) ) {
			// resize
			String originalFile = FileStore.getStoreFilename(fileKey, originalImageSuffix());
			new ImageResizer(resizeSpec, originalFile, frameFile.getName()).resize();
			
			log.info("resized {} to {} to create frame {}", fileKey, resizeSpec, frameFile.getName());
		} catch (Exception e) {
			throw new ProgramDefectException ("failed to resize", e);
		}
	}




}
