/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.invoke.MethodHandles;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nominet.iot.utils.error.ProgramDefectException;

public class TaskExecutor {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	List<String> commandLine;
	boolean executed = false;
	String stdout = "";
	String stderr = "";
	int returnCode;
	
	public TaskExecutor(List<String> commandLine) {
		this.commandLine = commandLine;
	}
	
	public void execute() {
		if (executed) throw new ProgramDefectException ("already executed");
	
		log.debug("running external command: {}", commandLine);
		ProcessBuilder processBuilder = new ProcessBuilder(commandLine);
		
		Process p;
		try {
			p = processBuilder.start();
		
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
					BufferedReader readerErr = new BufferedReader(new InputStreamReader(p.getErrorStream()));
				    ) {
				try {
					p.waitFor();
				} catch (InterruptedException e) {
					throw new ProgramDefectException ("operation was interrupted", e);
				}
				returnCode = p.exitValue();
				
				String line;
				//logger.info("exit: "+Integer.toString(p.exitValue()));
				while ((line = reader.readLine()) != null) {
					stdout += line + "\n";
				}
				while ((line = readerErr.readLine()) != null) {
					stderr += line + "\n";
				}
			}
		} catch (IOException e) {
			throw new ProgramDefectException ("operation failed: "+e.getMessage(), e);
		}
		
		executed = true;
	}

	void checkExecuted() {
		if (!executed) throw new ProgramDefectException ("not executed yet");
	}
	
	public String getStdout() {
		checkExecuted();
		return stdout;
	}
	public String getStderr() {
		checkExecuted();
		return stderr;
	}
	public int getReturnCode() {
		checkExecuted();
		return returnCode;
	}

}
