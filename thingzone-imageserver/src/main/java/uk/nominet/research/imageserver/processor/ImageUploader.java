/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.processor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.ByteStreams;

import uk.nominet.research.imageserver.common.DataException;
import uk.nominet.research.imageserver.common.TemporaryFile;
import uk.nominet.research.imageserver.filestore.FileStore;

public class ImageUploader {
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	// save uploaded file to new location
	public UploadedImageDetails upload(InputStream inputStream) {
		
		String fileKey = FileStore.generateKey();
		
		// store in "incoming" for now (using temp files to auto-delete in case of errors)
		String incomingFilename = FileStore.getIncomingFilename(fileKey, ".tmp");
		String metadataFilename = FileStore.getIncomingFilename(fileKey, ImageMetadata.FILE_SUFFIX+".tmp");

		try (TemporaryFile incomingFile = new TemporaryFile(incomingFilename);
				TemporaryFile metadataFile = new TemporaryFile(metadataFilename);
				FileOutputStream metadataStream = new FileOutputStream(metadataFile)
				) { 

			// store incoming stream as file
			persistStream(incomingFile, inputStream);

			// detect type while still in incoming location, so we can name correctly
			ImageIdentifier id = new ImageIdentifier (incomingFilename);

			// create metadata file
			ImageMetadata metadata = ImageMetadata.create(fileKey, id.getFormat());
			metadata.setWidth(id.getWidth());
			metadata.setHeight(id.getHeight());
			metadata.persist(metadataStream);

			// move both files to final location (metadata second)
			
			// move image
			FileStore.moveToStore(incomingFile, fileKey, "."+id.getFormat());
//			log.info("stored image {}", destImage);

			// move metadata
			FileStore.moveToStore(metadataFile, fileKey, ImageMetadata.FILE_SUFFIX);
//			log.info("stored metadata {}", destMetadata);
			
			// return details to caller (for conversion to JSON)
			return new UploadedImageDetails(fileKey, id);

		} catch (DataException e) {
			// errors we we expected
			log.info("caught DataException & returning error to client: {}", e.getMessage());
			return UploadedImageDetails.failed(e.getMessage());
		} catch (Exception e) {
			// errors we didn't expect
			log.error("caught error: "+e.getMessage(), e);
			return UploadedImageDetails.failed("internal error");
		}
	}	


	void persistStream(File file, InputStream inputStream) throws FileNotFoundException, IOException {
		try (OutputStream outputStream = new FileOutputStream(file)){
			ByteStreams.copy(inputStream, outputStream);
			outputStream.flush();
			outputStream.close();
		}
	}

}
