/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.common;

import java.io.File;

import uk.nominet.iot.utils.error.ProgramDefectException;


public class FileUtils {


    public static String stripTrailingSlash(String s) {
        s = s.trim();
        while (s.endsWith("/")) {
        	s = s.substring(0, s.length() - 1);
        }
        return s;
    }



    public static boolean directoryExists(String name) {
    	try {
    		checkDirectoryExists(name);
    		return true;
    	} catch (ProgramDefectException e) {
    		return false;
    	}
    }


    public static boolean fileExists(String name) {
    	try {
    		checkFileExists(name);
    		return true;
    	} catch (ProgramDefectException e) {
    		return false;
    	}
    }

    public static void checkDirectoryExists(String name) {
        File path = new File(name);
        if (!path.exists()) {
            throw new ProgramDefectException("directory " + quote(path.getName()) + " does not exist");
        }
        if (!path.isDirectory()) {
            throw new ProgramDefectException("not a directory: " + quote(path.getName()));
        }
    }

    public static void checkFileExists(String fileName) {
        File file = new File(fileName);
        if (!file.exists()) {
            throw new ProgramDefectException("file " + quote(file.getName()) + " does not exist");
        }
        if (!file.isFile()) {
            throw new ProgramDefectException("not a file: " + quote(file.getName()));
        }
    }

    public static void checkFileReadable(String fileName) {
        File file = new File(fileName);
        if (!file.canRead()) {
            throw new ProgramDefectException("file " + quote(file.getName()) + " is not readable");
        }
    }
    
    static String quote(String s) {
        return "\"" + s + "\"";
    }
	
}
