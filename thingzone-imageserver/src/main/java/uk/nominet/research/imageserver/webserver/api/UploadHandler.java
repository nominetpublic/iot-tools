/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.webserver.api;

import java.io.InputStream;
import java.lang.invoke.MethodHandles;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nominet.research.imageserver.processor.ImageUploader;
import uk.nominet.research.imageserver.processor.UploadedImageDetails;
 
@Path("/")

public class UploadHandler {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	@Context
    UriInfo url;

    @Context
    Request request;
	
    @Context
    private javax.servlet.http.HttpServletRequest hsr;    
 
    // send file e.g. with:
    // curl -X POST -F "file=@tick-mark_small.png" http://127.0.0.1:8081/upload

    // curl -X POST --upload-file tick-mark_small.png http://127.0.0.1:8081/upload
    // ... but OTOH you might want to upload other metadata

    @POST
	@Path("/upload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
	public Object uploadFile(
		@FormDataParam("file") InputStream incomingInputStream,
		@FormDataParam("file") FormDataContentDisposition fileDetail) {
    	
//		RequestDebugger.dump(hsr);
		
//		log.info("hsr.getCharacterEncoding={}", hsr.getCharacterEncoding());
//		log.info("hsr.getContentLength={}", hsr.getContentLength());
//		log.info("hsr.getContentType={}", hsr.getContentType());		
		
//		log.info("fileDetail.getFileName={}", fileDetail.getFileName());
//		log.info("fileDetail.getName={}", fileDetail.getName());	// name as supplied to form
//		log.info("fileDetail.getCreationDate={}", fileDetail.getCreationDate());
//		log.info("fileDetail.getModificationDate={}", fileDetail.getModificationDate());
//		log.info("fileDetail.getFileSize={}", fileDetail.getSize());
//		log.info("fileDetail.getFileType={}", fileDetail.getType());

    	// probably not interesting; will have come via Bridge
//		String originalName = fileDetail.getFileName();
    	

		UploadedImageDetails details = new ImageUploader().upload(incomingInputStream);
		// gets converted to JSON
		return details.output();
	}


}
