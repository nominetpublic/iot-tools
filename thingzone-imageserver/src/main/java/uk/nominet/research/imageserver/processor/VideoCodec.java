/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.processor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import uk.nominet.research.imageserver.common.DataException;

public enum VideoCodec {
	
	
	CODEC_x264 {
		@Override
		public String toString() { return x264_TEXT; }

		@Override
		public boolean isCompatibleWith(VideoContainer container) {
			return VideoContainer.CONTAINER_MP4.equals(container) || VideoContainer.CONTAINER_AVI.equals(container);
		}
		
		@Override
		public String ffmpegCodec() { return "libx264"; }

		@Override
		public List<String> ffmpegCodecOptions() { 
			return Arrays.asList(	// see https://trac.ffmpeg.org/wiki/Encode/H.264
					"-crf", "25",	// output quality - 18(good)..28(ok)
					"-preset", "medium"	// choice between speed/performance 
				); 
		}	
	} ,

	CODEC_VP8 {
		@Override
		public String toString() { return VP8_TEXT; }
		@Override
		public boolean isCompatibleWith(VideoContainer container) {
			return VideoContainer.CONTAINER_MATROSKA.equals(container) || VideoContainer.CONTAINER_WEBM.equals(container);
		}
		@Override
		public String ffmpegCodec() { throw new DataException ("VP8 not yet implemented"); }
	},
	
	CODEC_THEORA {
		@Override
		public String toString() { return THEORA_TEXT; }
		@Override
		public boolean isCompatibleWith(VideoContainer container) {
			return VideoContainer.CONTAINER_OGG.equals(container);
		}
		@Override
		public String ffmpegCodec() { throw new DataException ("Theora not yet implemented"); }
	},
	
	CODEC_MPEG4_PART2 {
		@Override
		public String toString() { return MPEG4_PART2_TEXT; }
		
		@Override
		public boolean isCompatibleWith(VideoContainer container) {
			return VideoContainer.CONTAINER_MP4.equals(container) || VideoContainer.CONTAINER_AVI.equals(container);
		}
		
		@Override
		public String ffmpegCodec() { return "mpeg4"; }
		
		@Override
		public List<String> ffmpegCodecOptions() { 
			return Arrays.asList(
				"-qscale:v", "5"	// VBR quality level - https://trac.ffmpeg.org/wiki/Encode/MPEG-4
				); 
		}	
	},
	;

	public final static String x264_TEXT="x264";
	public final static String VP8_TEXT="VP8";
	public final static String THEORA_TEXT="Theora";
	public final static String MPEG4_PART2_TEXT="FMP4";
	
	public static boolean isValid(String s) {
		if (s == null || s.isEmpty())
			return false;
		
		try
		{
			fromString(s);
			return true;
		}
		catch(IllegalArgumentException e) {
			return false;
		}
	}

	public static VideoCodec fromString(String s) {
		if (s == null || s.isEmpty())
			throw new IllegalArgumentException ("VideoCodec cannot be empty");
		
		switch(s) {
			case x264_TEXT: 
				return CODEC_x264;
			case VP8_TEXT: 
				return CODEC_VP8;
			case THEORA_TEXT: 
				return CODEC_THEORA;
			case MPEG4_PART2_TEXT: 
				return CODEC_MPEG4_PART2;
			default	: 
				throw new IllegalArgumentException("s is not a valid VideoCodec");
		}
	}
	
	abstract public boolean isCompatibleWith(VideoContainer container);
	abstract public String ffmpegCodec();

	public List<String> ffmpegCodecOptions() {
		return new ArrayList<>();
	}

}
