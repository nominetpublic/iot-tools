/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.processor;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.invoke.MethodHandles;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nominet.research.imageserver.common.DataException;
import uk.nominet.research.imageserver.filestore.FileStore;
import uk.nominet.iot.utils.error.ProgramDefectException;

public class Metadata {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	String fileKey;
	Properties props;
	
	final static public String FILE_SUFFIX = ".meta";

	// not createable
	protected Metadata(String fileKey) {
		this.fileKey = fileKey;
	}
	protected Metadata(String fileKey, String type) {
		this.fileKey = fileKey;
		props = new Properties();
		props.setProperty(PROP_KEY, fileKey);
		props.setProperty(PROP_TYPE, type);
	}
	
	static Metadata fromFile(String fileKey) {
		Metadata m = new Metadata(fileKey);
		m.load();
		return m;
	}

	static Metadata create(String fileKey, String type) {
		Metadata m = new Metadata(fileKey, type);
		return m;
	}
	
	protected void load() {

		// do we have the directory?
		if (!FileStore.storeExists(fileKey)) {
			throw new DataException ("not found \""+fileKey+"\"");
		}

		// look for metadata
		String suffix = ".meta";
		if (!FileStore.fileExists(fileKey, suffix)) {
			throw new DataException ("metadata not found \""+fileKey+"\"");
		}

		// read 
		props = new Properties();
		String metadataFilename = FileStore.getStoreFilename(fileKey, suffix);
	    try (FileInputStream fis =new FileInputStream(metadataFilename))
	    {
	    	props.load(fis);
		} catch (IOException e) {
			throw new ProgramDefectException ("failed to load metadata from file "+metadataFilename, e);
		}
	    
	    // check that key contained is correct
	    if (!fileKey.equals(getKey())) {
			throw new ProgramDefectException ("metadata from file "+metadataFilename+" didn't match name");
	    }
	    	
	}
	

    void persist (OutputStream os) throws IOException {
    	props.store(os, getPropfileComment());
    	os.close();
    }
	
    protected String getPropfileComment() {
    	return "metadata";
    }
    

	final static String PROP_TYPE = "type";
	final static String PROP_KEY = "key";

	public String getType() {
		return getProperty(PROP_TYPE);
	}

	public String getKey() {
		return getProperty(PROP_KEY);
	}
	
//	void ensureProperty(String key) {
//		if (!props.containsKey(key)) {
//			throw new ProgramDefectException ("metadata for "+fileKey+" missing "+ key);
//		}
//	}
		
		
	String getProperty(String property) {
//		ensureProperty(property);
		if (!props.containsKey(property)) {
			throw new ProgramDefectException ("metadata for "+fileKey+" missing "+ property);
		}
		return props.getProperty(property);
	}
	void setProperty(String property, String value) {
		props.setProperty(property, value);
	}
	


	// width & height are common to video+images
	final static String PROP_WIDTH = "width";
	final static String PROP_HEIGHT = "height";

	public String getWidth() {
		return getProperty(PROP_WIDTH);
	}

	public void setWidth(int width) {
		setProperty(PROP_WIDTH, Integer.toString(width));
	}
	public void setHeight(int height) {
		setProperty(PROP_HEIGHT, Integer.toString(height));
	}
	
}
