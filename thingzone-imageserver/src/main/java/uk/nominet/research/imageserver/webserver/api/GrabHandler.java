/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.webserver.api;

import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.nominet.research.imageserver.common.DataException;
import uk.nominet.research.imageserver.processor.ImageGrabber;
import uk.nominet.research.imageserver.processor.ImageUploader;
import uk.nominet.research.imageserver.processor.UploadedImageDetails;
import uk.nominet.research.imageserver.webserver.api.support.JsonErrorResult;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;
import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;

@Path("/")

public class GrabHandler {
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	@Context
    UriInfo url;

    @Context
    Request request;

    @SuppressWarnings("unused")
    @Context
    private javax.servlet.http.HttpServletRequest hsr;
 

	@POST
	@Path("/grab")
	@Produces(MediaType.APPLICATION_JSON)
	public Object grabFile(
			final @FormParam(value = "image_url") String imageUrl
	)
	{
		log.info("[/grab] (image_url={})", imageUrl);

		if (Strings.isNullOrEmpty(imageUrl))
			return new JsonErrorResult("missing param \"imageUrl\"");

		try(InputStream grabbedInputStream = new ImageGrabber(imageUrl).getStream()) {
			UploadedImageDetails details = new ImageUploader().upload(grabbedInputStream);
			return details.output();
		} catch (DataException e) {
			// our attempt at handling error: return gracefully
			return new JsonErrorResult(e.getMessage());
		} catch (IOException e) {
			// catch-all; return 50x
			throw new RuntimeException("Unexpected exception", e);
		}
	}
}
