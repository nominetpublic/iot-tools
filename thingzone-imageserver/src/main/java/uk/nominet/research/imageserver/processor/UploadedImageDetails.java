/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.processor;

import java.util.LinkedHashMap;

public class UploadedImageDetails {
	LinkedHashMap<String, Object> resultContainer = new LinkedHashMap<String, Object>();

	public UploadedImageDetails(String fileKey, ImageIdentifier id) {
		resultContainer.put("result", "ok");
		resultContainer.put("name", fileKey);
		resultContainer.put("format", id.getFormat());
		resultContainer.put("width", id.getWidth());
		resultContainer.put("height", id.getHeight());
	}

	public LinkedHashMap<String, Object> output() {
		return resultContainer;
	}


	// alternative error object
	private UploadedImageDetails() { }

	public static UploadedImageDetails failed(String message) {
		UploadedImageDetails err = new UploadedImageDetails();
		err.resultContainer.put("result", "fail");
		err.resultContainer.put("reason", message);
		return err;
	}
}

