/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.webserver;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nominet.research.imageserver.webserver.api.support.JsonErrorResult;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;


/*
 * This class catches some errors thrown by Jackson when deserialising objects
 *  and instead returns HTTP sucess, with error message in JSON   
 */
public class ResponseJsonErrorFilter implements ContainerResponseFilter {
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	final static Pattern CANNOT_CONSTRUCT = Pattern.compile ("^Can not construct instance.*$");
	final static Pattern CANNOT_DESERIALIZE = Pattern.compile ("^Can not deserialize instance.*$");
	final static Pattern UNRECOGNISED_FIELD = Pattern.compile ("^Unrecognized field.*$");
	
	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
		
		if (responseContext.getStatus() != Response.Status.OK.getStatusCode()) {
			if (responseContext.hasEntity()) {
				Object entity =responseContext.getEntity();
				if (entity instanceof String) {
					String message = (String) entity;
//					log.info ("checking error s=\"{}\"", message );
					List<String> lines = Lists.newArrayList(Splitter.on('\n').trimResults().omitEmptyStrings().split(message));
					if (lines.size() >= 1 && 
							matches (lines.get(0), CANNOT_CONSTRUCT)
							|| matches (lines.get(0), CANNOT_DESERIALIZE)
							|| matches (lines.get(0), UNRECOGNISED_FIELD)
							) {
						
						String errmsg = lines.get(0);	// strip out our classname from err message
						errmsg = errmsg.replaceAll("\\(class uk.nominet[^\\)]*\\)", "");
						
						// rewrite as JSON error message
						responseContext.setStatus(Response.Status.OK.getStatusCode());
						responseContext.getHeaders().remove("Content-Type");
						responseContext.getHeaders().add("Content-Type", "application/json");
						String replacementContents = new JsonErrorResult(errmsg).asJsonString(); 
						responseContext.setEntity(replacementContents);
						
						log.info("rewrote as JSON 'failed' with message {}", replacementContents);
				    }
				    else {
				    	// keep an eye out for messages we haven't seen before 
				    	log.warn("not handled s=\"{}\"", message);
				    }
				}
			} else {
//				log.info("wot no entirty");
			}
		}
	}
	
	boolean matches(String string, Pattern pattern) {
	    Matcher matcher = pattern.matcher(string);
	    return matcher.matches();
	}
}
