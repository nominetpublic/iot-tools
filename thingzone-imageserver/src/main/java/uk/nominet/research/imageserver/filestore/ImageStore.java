/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.filestore;


import java.io.File;
import java.lang.invoke.MethodHandles;
import java.nio.file.Files;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nominet.research.imageserver.common.ConfigFactory;
import uk.nominet.research.imageserver.common.FileUtils;
import uk.nominet.research.imageserver.common.ImageServerConfig;

public class ImageStore {
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	static ImageServerConfig getConfig() {
		return ConfigFactory.getConfig();
	}
	static String getStoreDirectory() {
		return getConfig().getStoreDirectory();
	}

	static String INCOMING_PREFIX = "incoming";
	// NB incoming directory must be on same filesystem
	static String getIncomingDirectory() {
		return FileUtils.stripTrailingSlash(getConfig().getStoreDirectory()) +"/"+INCOMING_PREFIX+"/";
	}

	static String getTempDirectory() {
		return FileUtils.stripTrailingSlash(getConfig().getTempDirectory()) +"/";
	}

	public static void initialise() {
		String storeDir = getStoreDirectory();
		FileUtils.checkDirectoryExists(storeDir);
		log.info("checked that store directory \"{}\" exists", storeDir);
		
		// create incoming directory if necessary
		String incomingDir = getIncomingDirectory();
		if (!FileUtils.directoryExists(incomingDir)) {
			new File(incomingDir).mkdir();
			log.info("created directory \"{}\" for incoming files", incomingDir);
		} else {
			log.info("directory for incoming files \"{}\" already exists", incomingDir);
		}
		
		cleanDirectory(incomingDir);
		
		String tempDir = getTempDirectory();
		FileUtils.checkDirectoryExists(tempDir);
		log.info("checked that temp directory \"{}\" exists", tempDir);
		cleanDirectory(tempDir);
	}
	
	// clean out any files in incoming directory left from previous runs (for sanity + hygiene)
	static void cleanDirectory(String incomingDir) {
		File dir = new File(incomingDir);
		for (File f : dir.listFiles()) {
			if (Files.isSymbolicLink(f.toPath()) || Files.isRegularFile(f.toPath())) {
				f.delete();
				log.info("cleaned out file \"{}\" from directory", f);
			} else {
				log.info("can't clean out non-file \"{}\" in directory", f);
			}
		}
	}

	public static List<String> listImageFiles() {
		String storeDirectory = ImageStore.getStoreDirectory();
		return new FileFinder().getFiles(storeDirectory);
	}
	
	
	
}
