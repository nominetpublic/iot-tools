/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.webserver.api.support;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/*
 * JsonErrorResult is a container for an "error" response, with single String reason
 *
 * {"result":"fail","reason":<reason> }
 *
 * Usage example:
 *	return new JsonErrorResult("message");
 */
public class JsonErrorResult {
	final static String FAIL = "fail";
	public final String result = FAIL;
	public final String reason;

	public JsonErrorResult(String reason) {
		this.reason = reason;
	}

	public Object output() {	// provide a similar interface to JsonMapResult etc (don't need to call this though) 
		return this;
	}
	public String asJsonString() {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			return objectMapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			 return "{\"result\":\"fail\",\"reason\":\"failed to serialise JsonErrorResult\" }";
		}
	}
}
