/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.processor;

public enum VideoContainer {
	
	
	CONTAINER_WEBM {
		@Override
		public String toString() { return WEBM_TEXT; }
	} ,
	CONTAINER_MATROSKA {
		@Override
		public String toString() { return MATROSKA_TEXT; }
	},
	CONTAINER_AVI {
		@Override
		public String toString() { return AVI_TEXT; }
	},
	CONTAINER_MP4 {
		@Override
		public String toString() { return MP4_TEXT; }
	},
	CONTAINER_OGG {
		@Override
		public String toString() { return OGG_TEXT; }
	},
	;

	// NB currently relying on these being same as file extensions (see fileExtension())
	public final static String WEBM_TEXT="webm";
	public final static String MATROSKA_TEXT="mkv";
	public final static String AVI_TEXT="avi";
	public final static String MP4_TEXT="mp4";
	public final static String OGG_TEXT="ogg";

	public static boolean isValid(String s) {
		if (s == null || s.isEmpty())
			return false;
		
		try
		{
			fromString(s);
			return true;
		}
		catch(IllegalArgumentException e) {
			return false;
		}
	}

	public static VideoContainer fromString(String s) {
		if (s == null || s.isEmpty())
			throw new IllegalArgumentException ("VideoContainer cannot be empty");
		
		switch(s.toLowerCase()) {
			case WEBM_TEXT: 
				return CONTAINER_WEBM;
			case MATROSKA_TEXT: 
				return CONTAINER_MATROSKA;
			case AVI_TEXT: 
				return CONTAINER_AVI;
			case MP4_TEXT: 
				return CONTAINER_MP4;
			case OGG_TEXT: 
				return CONTAINER_OGG;
			default	: 
				throw new IllegalArgumentException("s is not a valid VideoContainer");
		}
	}

	// provide an inverse
	public boolean isCompatibleWith(VideoCodec codec) {
		return codec.isCompatibleWith(this);
	}
	
	public String fileExtension() {
		return toString();
	}
	
}
