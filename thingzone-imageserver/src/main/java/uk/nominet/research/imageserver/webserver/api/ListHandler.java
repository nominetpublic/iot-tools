/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.webserver.api;

import java.lang.invoke.MethodHandles;
import java.util.LinkedHashMap;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nominet.research.imageserver.filestore.ImageStore;

@Path("/")

public class ListHandler {
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	@Context
    UriInfo url;

    @Context
    Request request;
	
    @Context
    private javax.servlet.http.HttpServletRequest hsr;    
 
    @GET
    @Path("list")
    @Produces(MediaType.APPLICATION_JSON)
	public Object list() {

		log.info("GET [list]");
		
		try {
			List<String> files = ImageStore.listImageFiles();
	    	
			LinkedHashMap<String, Object> resultContainer = new LinkedHashMap<String, Object>();
			resultContainer.put("result", "ok");
			resultContainer.put("images", files);
			
			return resultContainer;
		}
		catch (Exception e) {
			log.error("caught {} & returning INTERNAL_SERVER_ERROR to client: {}",
					e.getClass().getSimpleName(), e.getMessage());
			log.error(e.getMessage(), e);

			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.build();
		}

	}
}
