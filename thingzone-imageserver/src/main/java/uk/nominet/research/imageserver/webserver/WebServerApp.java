/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.webserver;

import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nominet.research.imageserver.filestore.ImageStore;

import com.google.common.base.Strings;

public class WebServerApp {
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	public static void main(String[] args) {
		log.info("running Web server App...");

		try {
			WebServerApp app = new WebServerApp();
			app.run(args);
		} catch (Throwable e) {
			// we need exception info to be logged, not just printed to stderr
			log.error("exception thrown from main():", e);
		} finally {
//    		DatabaseDriver.globalDispose();
		}
	}

	void run(String[] args) throws Exception {
//		DatabaseApp.initialiseDatabase();
		
		initialiseWebServerConfig();
		initialiseImageServerConfig();
		verifyImageServerConfig();

		runWebServer();
	}

	public static void runWebServer() throws Exception {
		// run webserver
		WebServer.webMain();
	}

	
	public static void initialiseWebServerConfig() {
		String webPropertiesFile = System.getProperty("web.properties");
		if (Strings.isNullOrEmpty(webPropertiesFile)) {
			log.info("no web.properties specified; using default");
			webPropertiesFile="config/web.properties";
		}
		
		// web config
		uk.nominet.research.imageserver.webserver.ConfigFactory.loadConfig(webPropertiesFile);
	}

	public static void initialiseImageServerConfig() {
		String imageServerPropertiesFile = System.getProperty("imageserver.properties");
		if (Strings.isNullOrEmpty(imageServerPropertiesFile)) {
			log.info("no imageserver.properties specified; using default");
			imageServerPropertiesFile="config/imageserver.properties";
		}
		
		uk.nominet.research.imageserver.common.ConfigFactory.loadConfig(imageServerPropertiesFile);
	}

	public static void verifyImageServerConfig() {
		ImageStore.initialise();
	}

	
}
