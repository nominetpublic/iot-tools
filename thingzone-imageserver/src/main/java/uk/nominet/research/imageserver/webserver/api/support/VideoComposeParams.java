/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.webserver.api.support;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Joiner;

import uk.nominet.research.imageserver.common.DataException;
import uk.nominet.research.imageserver.processor.VideoContainer;
import uk.nominet.research.imageserver.processor.VideoCodec;


// JSON-receiving object
// (must be static, with setters or public members)
public class VideoComposeParams {
	public Integer width;
	public Integer height;
	public String container;
	public String codec;
	public Float inputFrameRate;
	public Float outputFrameRate;
	
	@JsonIgnore public VideoContainer eContainer;
	@JsonIgnore public VideoCodec eCodec;

	
	public void validate() {
		if (width==null) {
			throw new DataException ("missing 'width' parameter");
		}
		if (height==null) {
			throw new DataException ("missing 'height' parameter");
		}
		if (inputFrameRate==null) {
			throw new DataException ("missing 'inputFrameRate' parameter");
		}
		if (outputFrameRate==null) {
			throw new DataException ("missing 'outputFrameRate' parameter");
		}
		if (container==null) {
			throw new DataException ("missing 'container' parameter");
		}
		if (VideoContainer.isValid(container)) {
			eContainer = VideoContainer.fromString(container);
		} else {
			throw new DataException ("invalid container - supported are: "+Joiner.on(", ").join(VideoContainer.values()));
		}
		if (VideoCodec.isValid(codec)) {
			eCodec = VideoCodec.fromString(codec);
		} else {
			throw new DataException ("invalid codec - supported are: "+Joiner.on(", ").join(VideoCodec.values()));
		}

		if (!eCodec.isCompatibleWith(eContainer)) {
			throw new DataException ("codec \""+eCodec+"\" not supported with container \"" +eContainer+"\"");
		}
	}
	
	public static class Frame {
		public String key;
		public Integer count;
    	@Override public String toString() {
    		return getClass().getSimpleName() + "{" + " key=\'"+key + "\'"+", count="+count+"}"; 
    	}
    	public String getKey() { return key; }
    	public int getCount() { return count; }
	}
	
	public List<Frame> frameSequence;
	
	public int getWidth() { return width; }
	public int getHeight() { return height; }
	public VideoCodec getCodec() { return eCodec; }
	public VideoContainer getContainer() { return eContainer; }
	public String getInputFrameRate() { return Float.toString(inputFrameRate); }
	public String getOutputFrameRate() { return Float.toString(outputFrameRate); }
	
	@Override public String toString() {
		return getClass().getSimpleName() + "{"
				+ " width="+width
				+ ", height="+height
				+ ", container=\'"+container+"\'"
				+ ", codec=\'"+codec+"\'"
				+ ", inputFrameRate="+inputFrameRate
				+ ", outputFrameRate="+outputFrameRate
				+ ", frameSequence="+frameSequence
				+"}"; 
	}
}
