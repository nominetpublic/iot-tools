/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.common;

import java.lang.invoke.MethodHandles;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestDebugger {
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	public static void dump(HttpServletRequest hsr) {
		log.info("dumping request:");
		log.info("====================");
		log.info("method={}", hsr.getMethod());
		log.info("getContextPath={}", hsr.getContextPath());
		log.info("getPathInfo={}", hsr.getPathInfo());
		log.info("getQueryString={}", hsr.getQueryString());

		log.info("getCharacterEncoding={}", hsr.getCharacterEncoding());
		log.info("getContentLength={}", hsr.getContentLength());
		log.info("getContentType={}", hsr.getContentType());

		log.info("getLocalAddr={}", hsr.getLocalAddr());
		log.info("getLocalPort={}", hsr.getLocalPort());

		log.info("getProtocol={}", hsr.getProtocol());
		
		log.info("headers:");
		Enumeration<String> headerNames = hsr.getHeaderNames();
		while(headerNames.hasMoreElements()) {
			String header = headerNames.nextElement();
			log.info("header: {}={}", header, hsr.getHeader(header));
		}

		log.info("parameters:");
		Enumeration<String> parameterNames = hsr.getParameterNames();
		boolean gotParams = false;
		while(parameterNames.hasMoreElements()) {
			String parameter = parameterNames.nextElement();
			log.info("parameter: {}={}", parameter, hsr.getParameterValues(parameter));
			gotParams = true;
		}
		if (!gotParams) log.info("(no params)");

	}
}
