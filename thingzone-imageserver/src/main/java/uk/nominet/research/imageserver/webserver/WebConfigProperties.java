/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.webserver;

import java.util.*;

import uk.nominet.iot.utils.error.FatalError;
import uk.nominet.iot.utils.config.ValidatingConfigProperties;

public class WebConfigProperties extends ValidatingConfigProperties implements WebConfig {
	private List<String> hostnames;
	
	final static String PROPNAME_PORT = "Port";
	final static String PROPNAME_IDLE_TIMEOUT = "IdleTimeout";
	
    WebConfigProperties(Properties properties) {
        // superclass will validate, calling our overridden methods
        super(properties);
    }
    
    @Override
    protected Properties customValidation(Properties props) {

        hostnames = new ArrayList<String>();

        // split up the list of hostnames
        if (!props.containsKey("Hostnames")) {
            throw new FatalError("Config file missing parameter \"Hostnames\"");
        }
        String value = props.getProperty("Hostnames");
        hostnames.addAll(Arrays.asList(value.split (",\\s*")));
        props.remove("Hostnames");

        return props;
    }

    private final static String[] INTEGER_PROPERTY_NAMES = new String[] {
    	PROPNAME_PORT, 
    	PROPNAME_IDLE_TIMEOUT, 
    };
    private final static String[] STRING_PROPERTY_NAMES = new String[] {
    	// Hostnames is not included because we handled it manually
    };

    @Override
    protected String[] getIntegerPropertyNames() {
        return INTEGER_PROPERTY_NAMES;
    }

    @Override
    protected String[] getStringPropertyNames() {
        return STRING_PROPERTY_NAMES;
    }

    @Override
	public List<String> getHostnames() {
        return Collections.unmodifiableList(hostnames);
    }
    @Override
	public int getPort() {
        return intProps.get(PROPNAME_PORT);
    }
    @Override
	public int getIdleTimeout() {
        return intProps.get(PROPNAME_IDLE_TIMEOUT);
    }
    
}
