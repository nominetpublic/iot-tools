/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.common;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// a wrapper for multiple temporary files that cleans up on close
public class TemporaryFiles implements AutoCloseable {
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	List<TemporaryFile> files;
	
	public TemporaryFiles() {
		files = new ArrayList<>();
	}
	
	public void add(TemporaryFile f) {
		files.add(f);
	}

	// if file exists, delete it
	@Override
	public void close() throws Exception {
		if (files.size() > 0) {
			log.info("cleaning {} temporary files...", files.size());
		}
		for (TemporaryFile f: files) {
			f.close();
		}

	}
}
