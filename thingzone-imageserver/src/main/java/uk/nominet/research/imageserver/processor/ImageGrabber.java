/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.nominet.research.imageserver.common.DataException;

import java.io.*;
import java.lang.invoke.MethodHandles;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

public class ImageGrabber {
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final String imageUrl;
	public ImageGrabber(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public InputStream getStream() {
		Client client = ClientBuilder.newClient();

		try {
			log.info ("grabbing image from "+imageUrl);
			Response response = client.target(imageUrl).request().get();

			int status = response.getStatus();
			if (response.getStatus() != Response.Status.OK.getStatusCode()) {
				throw new DataException("image grab returned "+status + ": "+response.getStatusInfo().getReasonPhrase());
			}
			return response.readEntity(InputStream.class);
		} catch (ProcessingException e) {
			throw new DataException("image grab returned: "+e.getMessage());
		}
	}


}
