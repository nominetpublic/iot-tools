/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.filestore;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Joiner;

import uk.nominet.iot.utils.error.ProgramDefectException;

public class FilenameSplitter {
	
	public static List<String> split(String filename) {
		List<String> result = new ArrayList<>();
		
		// v simple plausibility check - done properly in FilenameGenerator
		if(filename.length() < 6) {
			throw new ProgramDefectException("filename "+filename+" too short to split");
		}
		
		// simple 2-level scheme, 1000 per level
		// ie. AB/ABCD/ABCDEFGHIKJ.txt
		result.add(filename.substring(0,2));
		result.add(filename.substring(0,4));
		result.add(filename);
		return result;
	}
	
	// create directories from split filename
	public static List<String> directories(List<String> parts) {
		List<String> result = new ArrayList<>();
		for (int i=0; i<parts.size()-1; ++i) {
			result.add(Joiner.on("/").join(new ArrayList<String>(parts.subList(0,i+1))));
		}
		return result;
	}

	// create file path from split filename
	public static String filepath(List<String> parts) {
		return Joiner.on("/").join(parts);
	}

	
	// create directories..
//	for (int i=0;i<parts.size(); ++i) {
//		String path = Joiner.on("/").join(new ArrayList<String>(parts.subList(i, parts.size())));
//	}

}
