/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.processor;

import java.io.InputStream;
import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import uk.nominet.research.imageserver.common.DataException;
import uk.nominet.research.imageserver.filestore.FileStore;

public class VideoDownloader {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	String fileKey;
	VideoMetadata metadata;
	
	public VideoDownloader(String fileKey) {
		this.fileKey = fileKey;
		
		if (!FileStore.isPlausibleKey(fileKey)) {
			throw new DataException ("bogus name \""+fileKey+"\"");
		}

		metadata = VideoMetadata.fromFile(fileKey);
	}

	
	String suffix() {
		return "." + getFileType();
	}

	public InputStream download() {
		return FileStore.streamFile(fileKey, suffix());
	}
	

	public String getContentType() {
		return new String("video/"+getFileType());
	}
	public String getFileType() {
		return metadata.getType();
	}
	

}
