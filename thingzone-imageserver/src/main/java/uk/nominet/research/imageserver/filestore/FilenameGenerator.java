/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.filestore;

import java.security.SecureRandom;
import java.util.regex.Pattern;

import com.google.common.io.BaseEncoding;

public class FilenameGenerator {

	final static int BITS_PER_BYTE = 8;
	final static int BITS_PER_BASE32 = 6;

	final static int KEY_BITS = 66;		// enough to be effectively unguessable
	
	final static int LENGTH_AS_STRING =  KEY_BITS/BITS_PER_BASE32;
	final static String BASE32_CHARS="A-Z2-7";	// RFC4648
	
    final static Pattern validFilename = Pattern.compile(
    		String.format("[%s]{%d}", BASE32_CHARS, LENGTH_AS_STRING));

	/*
	 * Generates a random filename
	 * 
	 * @return	Hex encoded String containing KEY_BITS bits of entropy 
	 */
	public static String generateKey() {
		// get entropy
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[KEY_BITS / BITS_PER_BYTE +1];	// round up not down
		random.nextBytes(bytes);
		
		// convert to base32
		String result = BaseEncoding.base32().encode(bytes);
		
		return result.substring(0, LENGTH_AS_STRING);
	}

	public static boolean isPlausible(String filename) {
		if (filename.length() != LENGTH_AS_STRING)
			return false;
		
		if (!validFilename.matcher(filename).matches())
			return false;
		
		return true;
	}
	

}
