/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.common;

import java.awt.Color;
import java.awt.Transparency;
import java.lang.invoke.MethodHandles;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nominet.iot.utils.error.ProgramDefectException;

// a description of the resize that the user requested
public class ResizeSpec {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	String specString;
	
	boolean original = false;
	int width;
	int height;
	Color border;

	// e.g. "r64x64"
    final static Pattern simpleResizePattern = Pattern.compile ("^r(\\d+)x(\\d+)$");

    // e.g. "r64x64_b00ff00"
    final static Pattern borderedResizePattern = Pattern.compile ("^r(\\d+)x(\\d+)_b([0-9A-Fa-f]{6}|trans)$");

	
	public ResizeSpec(String specString) {
		this.specString = specString;

		if (checkParse() == false) {
			throw new DataException ("invalid resize spec");
		}

	}
	
	public boolean isOriginal() {
		return original;
	}
	
	boolean checkParse() {
		if (specString == null) 
			return false;

		// accept "original" to mean original image
		if ("original".equals(specString)) {
			original = true;
			return true;
		}
		
		// match basic resize
	    Matcher matcher = simpleResizePattern.matcher(specString);
	    if (matcher.matches()) {
	    	width = Integer.parseInt(matcher.group(1));
	    	height = Integer.parseInt(matcher.group(2));
//	    	log.info ("matched simple");
	    	return true;
	    }
		
	    // match bordered resize
	    matcher = borderedResizePattern.matcher(specString);
	    if (matcher.matches()) {
	    	width = Integer.parseInt(matcher.group(1));
	    	height = Integer.parseInt(matcher.group(2));
//	    	Color 
	    	if ("trans".equals(matcher.group(3))) {
	    		border = new Color(0,0,0,0);	// transparent
	    	} else {
	    		String r = matcher.group(3).substring(0, 2);
	    		String g = matcher.group(3).substring(2, 4);
	    		String b = matcher.group(3).substring(4, 6);
	    		border = new Color (Integer.parseInt(r, 16),Integer.parseInt(g, 16), Integer.parseInt(b, 16)); 
	    		
	    	}
	    	
//	    	log.info ("matched bordered");
//	    	border = matcher.group(2));
	    	return true;
	    }
		
		return false;		// not something we accept
	}
	
	int getWidth() {
		return width;
	}
	int getHeight() {
		return height;
	}
	boolean hasBorder() {
		return border != null;
	}
	Color getBorder() {
		return border;
	}

	/* generate string used for our file storage
	 * - so it doesn't have to be the same format as supplied in URL
	 * (although for now it is similar) 
	 */
	public String getFileSpec() { 
		if (isOriginal()) {
			throw new ProgramDefectException("no file spec for original file");
		}
		String suffix = "r";
		suffix += Integer.toString(getWidth()) + "x" + Integer.toString(getHeight());
		if (hasBorder()) {
			suffix += "_";
			if (isTransparent(border)) {
				suffix += "btrans";
			} else {
				suffix += "b"+toHtmlFormat(border);
			}
		}
		return suffix;
	}
	
	public String toString() {
		if (isOriginal()) {
			return "original";
		} else {
			return getFileSpec();	// this'll do for now
		}
	}
	
	public static boolean isTransparent(Color color) {
		return color.getTransparency() == Transparency.BITMASK;
	}
	public static String toHtmlFormat(Color color) {
		return String.format("%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue());
	}
}
