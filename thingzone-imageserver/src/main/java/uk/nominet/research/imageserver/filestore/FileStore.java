/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.filestore;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nominet.research.imageserver.common.FileUtils;
import uk.nominet.iot.utils.error.ProgramDefectException;

public class FileStore {
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	public static String generateKey() {
		return FilenameGenerator.generateKey();
	}
	
	public static boolean isPlausibleKey(String fileKey) {
		return FilenameGenerator.isPlausible(fileKey);
	}
	
	
	public static File moveToStore (File incomingFile, String key, String suffix) throws IOException {
		
		String destinationPath = createDirectoryTree(key);
		String destinationFilename = destinationPath+suffix; 
		File destinationFile = new File(destinationFilename); 

		return moveToStore(incomingFile, destinationFile);
	}
	
	public static File moveToStore (File incomingFile, File destinationFile) throws IOException {
		Files.move(
				incomingFile.toPath(), 
				destinationFile.toPath(), 
				StandardCopyOption.ATOMIC_MOVE);
		log.info("stored file {}", destinationFile.getName());

		return destinationFile;
	}
	
	// permanent storage for file
	public static String getStoreFilename(String fileKey, String suffix) {
		return ImageStore.getStoreDirectory()
				+ FilenameSplitter.filepath(FilenameSplitter.split(fileKey))
				+ suffix;
	}

	// temporary storage, on same filesystem as permanent
	public static String getIncomingFilename(String fileKey, String suffix) {
		return ImageStore.getIncomingDirectory()
				+ fileKey	// not split up into multiple dirs
				+ suffix;
	}

	// temporary storage 
	public static String getTempFilename(String fileKey, String suffix) {
		return ImageStore.getTempDirectory()
				+ fileKey	// not split up into multiple dirs
				+ suffix;
	}

	public static boolean storeExists(String fileKey) {
		List<String> parts = FilenameSplitter.split(fileKey);
		
		// check directories exist; fail if not
		for (String path: FilenameSplitter.directories(parts)) {
			String directory = ImageStore.getStoreDirectory()+path;
			if (!FileUtils.directoryExists(directory)) {
				log.info("no directory \"{}\"", directory);
				return false;
			}
		}
		return true;
	}

	public static boolean fileExists(String fileKey, String suffix) {
		List<String> parts = FilenameSplitter.split(fileKey);

		String filename = 
				ImageStore.getStoreDirectory()
				+ FilenameSplitter.filepath(parts)
				+ suffix;
		if (!FileUtils.fileExists(filename)) {
			log.info("not found \"{}\"", filename);
			return false;
		}
		return true;
	}
	
	public static InputStream streamFile(String fileKey, String suffix) {
		String filename = getStoreFilename(fileKey, suffix);
		try {
			return new FileInputStream(filename);
		} catch (IOException e) {
			throw new ProgramDefectException ("failed to read file \""+filename+"\"", e);
		}
	}


	static String createDirectoryTree(String fileKey) {
		// check or create directories to store file
		List<String> parts = FilenameSplitter.split(fileKey);
		for (String path: FilenameSplitter.directories(parts)) {
			String directory = ImageStore.getStoreDirectory()+path;
			if (!FileUtils.directoryExists(directory)) {
				new File(directory).mkdir();	// ok if already created (race condition)
//				log.info("created directory \"{}\"", directory);
			}
		}
		// path for file
		return ImageStore.getStoreDirectory() + FilenameSplitter.filepath(parts);
	}
}
