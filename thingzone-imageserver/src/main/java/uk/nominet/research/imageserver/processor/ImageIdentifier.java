/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.processor;

import java.lang.invoke.MethodHandles;

import org.im4java.core.Info;
import org.im4java.core.InfoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nominet.research.imageserver.common.DataException;

public class ImageIdentifier {
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	String filename;
	
	String format;
	int width;
	int height;
	
	public ImageIdentifier(String filename) {
		this.filename = filename;
		identify();
	}

	void identify() {
		Info imageInfo;
		try {
			imageInfo = new Info(filename, true);
//			log.info("Format: " + imageInfo.getImageFormat());
//			log.info("Width: " + imageInfo.getImageWidth());
//			log.info("Height: " + imageInfo.getImageHeight());
		//	log.info("Geometry: " + imageInfo.getImageGeometry());
		//	log.info("Depth: " + imageInfo.getImageDepth());
		//	log.info("Class: " + imageInfo.getImageClass());
			
			// we use same string as content-type "image/foo"
			switch(imageInfo.getImageFormat()) {
				case "JPG" :
				case "JPEG" : 	format = "jpg"; break;
				case "PNG" : 	format = "png"; break;
				case "GIF" : 	format = "gif"; break;
				default: format = "other"; break;
			}

			width = imageInfo.getImageWidth();
			height = imageInfo.getImageHeight();
			
			log.info("identified image {}: format={} ({}) {}x{}", 
					filename, 
					format,	// from our limited set
					imageInfo.getImageFormat(),	// as supplied by ImageMagick  
					width, height);
			
		} catch (InfoException e) {
			log.info("failed to identify image {}", filename);
			throw new DataException ("failed to identify image properties");
		}
	}

	public String getFormat() {
		return format;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public String getFilename() {
		return filename;
	}

}
