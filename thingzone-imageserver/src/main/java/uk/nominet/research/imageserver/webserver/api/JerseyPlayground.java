/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.webserver.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import uk.nominet.iot.utils.error.ProgramDefectException;
 
/**
 * Root resource (exposed at "playground" path)
 */
@Path("/playground")

public class JerseyPlayground {
	
	@Context
    UriInfo url;

    @Context
    Request request;
	
    @Context
    private javax.servlet.http.HttpServletRequest hsr;    
 
    // http://localhost:8081/api/playground 
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        return "Got it!";
    }

    // http://localhost:8081/api/playground/foo 
    @GET
    @Path("foo")
    @Produces(MediaType.TEXT_PLAIN)
    public String foo() {
        return "Got Foo :-)";
    }

    
    // http://localhost:8081/api/playground/bar
    // return a "response" object
    @GET
    @Path("bar")
    @Produces(MediaType.TEXT_PLAIN)
    public Response bar() {
        ResponseBuilder rb = Response.ok("this is a response");
        return rb.build();
    }

    // http://localhost:8081/api/playground/teapot 
    @GET
    @Path("teapot")
    @Produces(MediaType.TEXT_PLAIN)
    public Response teapot() {
        ResponseBuilder rb = Response.status(418);	// I'm a teapot
        return rb.build();
    }

    // http://localhost:8081/api/playground/param@domain=foo
    @GET
    @Path("param")
    @Produces(MediaType.TEXT_PLAIN)
    public Response param(
    		@QueryParam(value = "domain") String domain) {
    	
    	String msg = "";
//    	msg += "url="+url.toString()+"\n<br>";
//    	msg += "request="+request.toString()+"\n<br>";
		msg += "hsr="+hsr.getMethod()+" "+hsr.getContextPath()+" " + hsr.getPathInfo()+"\n";
		msg += "domain="+domain+"\n";
    	
        ResponseBuilder rb = Response.ok(msg);
        return rb.build();
    }

    

    // http://localhost:8081/api/playground/<anything> 
    @GET
    @Path("{paramName}")
    @Produces(MediaType.TEXT_HTML)
    public String getParty(@PathParam("paramName") String paramName)
    {
    	String response = "";
    	response += "url="+url.toString()+"\n<br>";
    	response += "request="+request.toString()+"\n<br>";
		response += "hsr="+hsr+"\n<br>";
		response += "paramName="+paramName+"\n<br>";
        return response;
    }
    
    
//    class Something {
//    	String name;
//    	int length;
//    }
    
/*    // http://localhost:8081/api/playground/<anything> 
    @GET
    @Path("json")
    @Produces(MediaType.APPLICATION_JSON)
    public Something getJson()
    {
    	Something s = new Something("fred", 42);
    	return s;
    }

    
    @GET
    @Path("json2")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getJson2()
    {
    	Something s = new Something("fred", 42);
        ResponseBuilder rb = Response.ok(s);
        return rb.build();
	}
*/
    
    // http://localhost:8081/api/playground/throw 
    @GET
    @Path("throw")
    @Produces(MediaType.TEXT_PLAIN)
    public String throwSomething() {
    	throw new ProgramDefectException("I'm not defective, this is deliberate!");
    }

    
}
