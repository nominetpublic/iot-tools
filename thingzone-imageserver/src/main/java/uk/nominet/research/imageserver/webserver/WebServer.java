/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.webserver;

import java.lang.invoke.MethodHandles;
import java.util.*;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nominet.research.imageserver.common.JmxServer;

import org.glassfish.jersey.server.ServerProperties;

import com.google.common.base.Joiner;

/*
 * WebServer provides HTTP API interface  
 */
public class WebServer {
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	static WebConfig getConfig() {
		return ConfigFactory.getConfig();
	}


	public static void webMain() throws Exception {

		Server server = new Server();

		// configure Jetty JMX
		server.addBean(JmxServer.getMBeanContainer());

		// get config from config file
		List<String> hosts = getConfig().getHostnames();
		int port = getConfig().getPort();
		int timeout = getConfig().getIdleTimeout();
		log.info("starting webserver on hosts [{}] port {}", Joiner.on(",").join(hosts), port);
		

		for (String host : hosts) {
			ServerConnector http = new ServerConnector(server);
			http.setHost(host);
			http.setPort(port);
			http.setIdleTimeout(timeout);
			server.addConnector(http);
		}


		ServletContextHandler context = createJerseyContext();

        context.setContextPath("/");
        server.setHandler(context);
        
        server.start();
		server.join();

	}
	
	static ServletContextHandler createJerseyContext() {
		
		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        
        ServletHolder jerseyServlet = context.addServlet(org.glassfish.jersey.servlet.ServletContainer.class, "/*");
        jerseyServlet.setInitOrder(0);
	
        // specify our handlers by classpath
        // (rather than setting individual classes with PROVIDER_CLASSNAMES)
        jerseyServlet.setInitParameter(
        		ServerProperties.PROVIDER_PACKAGES,
        		"uk.nominet.research.imageserver.webserver.api");

        List<String> providerClassnames = new ArrayList<String>();
        
        // add HTML multipart handler 
        // (see also pom.xml <artifactId>jersey-media-multipart</artifactId>)
        providerClassnames.add("org.glassfish.jersey.media.multipart.MultiPartFeature");
        
        // our CORS filter
        providerClassnames.add(ResponseCorsFilter.class.getName());

        // our JSON error filter
        providerClassnames.add(ResponseJsonErrorFilter.class.getName());

//        jerseyServlet.setInitParameter(
//        		ServerProperties.PROVIDER_CLASSNAMES, 
//        		"org.glassfish.jersey.media.multipart.MultiPartFeature"
//        		+";"+ResponseCorsFilter.class.getName()
//        		);

        jerseyServlet.setInitParameter(
        		ServerProperties.PROVIDER_CLASSNAMES,
        		Joiner.on(";").join(providerClassnames));

//        jerseyServlet.setInitParameter("javax.ws.rs.container.ContainerResponseFilter", 
//        		ResponseCorsFilter.class.getName());
//        jerseyServlet.setInitParameter(
//        		"com.sun.jersey.spi.container.ContainerResponseFilters"+";", 
//        		ServerProperties.PROVIDER_CLASSNAMES, 
//        		ResponseCorsFilter.class.getName());

		return context;
	}
	
}
