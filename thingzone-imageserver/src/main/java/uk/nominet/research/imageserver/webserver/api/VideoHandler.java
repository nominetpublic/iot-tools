/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.nominet.research.imageserver.webserver.api;

import java.io.InputStream;
import java.lang.invoke.MethodHandles;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nominet.research.imageserver.common.DataException;
import uk.nominet.research.imageserver.processor.ComposedVideoDetails;
import uk.nominet.research.imageserver.processor.VideoComposer;
import uk.nominet.research.imageserver.processor.VideoDownloader;
import uk.nominet.research.imageserver.webserver.api.support.VideoComposeParams;

@Path("/videos")

public class VideoHandler {
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	@Context
    UriInfo url;

    @Context
    Request request;
	
    @Context
    private javax.servlet.http.HttpServletRequest hsr;
    
    // JSON-receiving object
    // (must be static, with setters or public members)
    static class DemoJsonReceiver {
    	public String foo;
    	public String bar;
    	public String toString() {return getClass().getSimpleName() +": foo="+foo+" bar="+bar; }
    }

	@POST
	@Path("compose")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Object compose(VideoComposeParams vcp) {
		log.info("POST [videos/compose={}]", vcp);
		
		ComposedVideoDetails details = new VideoComposer().compose(vcp);
		
		// gets converted to JSON
		return details.output();
	}
	

    @GET
    @Path("{videoKey}")
	public Response videoFile(@PathParam("videoKey") String videoKey) {
		log.info("GET [videos/{}]", videoKey);
    	
		InputStream videoStream=null;	// manual close needed due to return
		try { 
			VideoDownloader videoDownloader = new VideoDownloader(videoKey);
			String contentType = videoDownloader.getContentType();
			videoStream = videoDownloader.download();
			
			return Response.ok()
					.type(contentType)
					.entity(videoStream)
					.build();
		} 
		catch (Exception e) {
			log.info("caught {} & returning NOT_FOUND to client: {}",
					e.getClass().getSimpleName(), e.getMessage());
			if (!(e instanceof DataException)) {
				log.error(e.getMessage(), e);
			}
	
			if (videoStream != null) {
				try{
					videoStream.close();
				} catch (Exception e2) {}
			}
			return Response.status(Status.NOT_FOUND)
					.build();
		}
	}
}
