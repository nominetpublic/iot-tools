/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  AfterViewInit,
  Component,
  ComponentFactoryResolver,
  Injector,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import * as MapboxDraw from '@mapbox/mapbox-gl-draw';
import bbox from '@turf/bbox';
import centroid from '@turf/centroid';
import * as basetheme from 'app/core/models/map/mapbox-draw-theme.js';
import { SatelliteSelectControl } from 'app/core/models/map/satellite-select-control';
import { ColourService } from 'app/core/services/api/colour.service';
import { ConfigService } from 'app/core/services/config/config.service';
import { GeoService } from 'app/core/services/geo/geo.service';
import { PanelResizeService } from 'app/core/services/panel-resize/panel-resize.service';
import { PreferenceService } from 'app/core/services/preference/preference.service';
import { AnnotationService } from 'app/core/services/visualisation/annotation.service';
import { ColourPickerComponent } from 'app/ui/colour-picker/colour-picker/colour-picker.component';
import * as mapboxgl from 'mapbox-gl';
import { Subscription } from 'rxjs';
import { filter, switchMap, tap } from 'rxjs/operators';

import { HtmlMarkerComponent } from '../html-marker/html-marker.component';


@Component({
  selector: 'annotation-edit',
  templateUrl: './annotation-edit.component.html',
  styleUrls: ['./annotation-edit.component.scss']
})
export class AnnotationEditComponent implements OnInit, AfterViewInit, OnDestroy {


  draw: MapboxDraw;
  map: mapboxgl.Map;
  defaultStyle: string;
  initialCentre: mapboxgl.LngLatLike;
  satelliteStyle: string;
  subscriptions: Subscription[] = [];

  annotationCollections: object[];
  entityKey: string = null;
  entityName: string;

  titleInput = new FormControl(this.entityName, [Validators.required]);
  titleEditable = true;
  latEdit: FormControl;
  lngEdit: FormControl;

  @ViewChildren('colourpicker')
  public ColourPicker: QueryList<ColourPickerComponent>;
  colourPickerOverlay: HTMLElement;
  showColourPicker: boolean;
  colour: string;

  popup: mapboxgl.Popup;

  constructor(
    protected geo: GeoService,
    private resolver: ComponentFactoryResolver,
    protected injector: Injector,
    protected panelResize: PanelResizeService,
    private prefService: PreferenceService,
    private annotationService: AnnotationService,
    private route: ActivatedRoute,
    private colourService: ColourService,
    private configService: ConfigService,
  ) {
    this.defaultStyle =  configService.getConfigValue('routingTheme');
    this.initialCentre =  configService.getConfigValue('initialCentre');
    this.satelliteStyle = configService.getConfigValue('satelliteTheme');
  }

  ngOnInit() {
    this.colour = this.colourService.getNext();
    this.lngEdit = new FormControl(this.initialCentre[0], [Validators.min(-360), Validators.max(360), Validators.required]);
    this.latEdit = new FormControl(this.initialCentre[1], [Validators.min(-90), Validators.max(90), Validators.required]);

    /* Load annotation resources */
    const resourceSubscription = this.route.params.pipe(
      filter(params => params.name),
      tap(params => this.entityKey = params.name),
      switchMap(params => this.prefService.getResource(params.name, 'annotation-collection'))
    ).subscribe(resources => {
      this.setName(resources.name);
      this.titleInput.setValue(resources.name);
      this.colour = resources.colour;
      this.draw.set(resources);

      if (resources.features.length !== 0) {
        resources.bbox = bbox(resources);
        this.map.fitBounds(resources.bbox, { padding: 100, duration: 0 });
      }
    });
    this.subscriptions.push(resourceSubscription);

    /* Initialise map */
    this.geo.setApiKey(mapboxgl);
    this.map = new mapboxgl.Map({
      container: 'map-annotation-editor',
      style: this.defaultStyle,
      zoom: 12,
      center: this.initialCentre
    });
    this.map.resize();


    this.popup = new mapboxgl.Popup({
      closeButton: false,
      closeOnClick: false,
      maxWidth: 'none',
      className: 'reset-style'
    });

    this.map.on('load', () => {
      this.map.resize();
      this.mapLoaded(this.map);
    });
  }

  mapLoaded(map: mapboxgl.Map): void {
    map.on('click', () => this.toggleColourPicker());
    map.on('moveend', e => this.updateCoords(e));
    map.on('draw.create', e => this.setFeatureColour(e));
    map.on('draw.delete', () => this.deleteFeature());
    map.on('draw.selectionchange', e => this.selectFeature(e));
    map.on('draw.update', () => this.saveChanges());
  }


  toggleColourPicker() {
    if (this.showColourPicker) this.toggleColourPickerView();
  }

  updateCoords(e: mapboxgl.MapboxEvent<MouseEvent | TouchEvent | WheelEvent> & mapboxgl.EventData): void {
    this.lngEdit.setValue(e.target.getCenter().lng.toFixed(6));
    this.latEdit.setValue(e.target.getCenter().lat.toFixed(6));
  }

  setFeatureColour(e): void {
    this.draw.setFeatureProperty(e.features[0].id, 'colour', this.colour);
  }

  deleteFeature(): void {
    this.popup.remove();
    this.annotationService.upsertResources(this.entityKey, this.entityName, this.draw.getAll(), this.colour);
  }

  updateMapCenter() {
    this.map.jumpTo({ center: [this.lngEdit.value, this.latEdit.value] });
  }

  selectFeature(e) {
    if (this.draw.getMode() === 'direct_select') {
      this.popup.remove();
    } else if (e.features.length === 1) {
      const mode = !e.features[0].properties['name'] ? 'create' : 'edit';
      this.popup
        .setLngLat(centroid(e.features[0]).geometry.coordinates)
        .setDOMContent(this.createPopupContent(mode, e.features[0]))
        .addTo(e.target);
    } else {
      this.popup.remove();
      this.draw.changeMode('simple_select');
    }
  }

  ngAfterViewInit(): void {
    const colourPickerSubscription = this.ColourPicker.changes.subscribe((colourPickerComponents: QueryList<ColourPickerComponent>) => {
      if (colourPickerComponents.first) {
        const colourPickerComponent = colourPickerComponents.first;
        colourPickerComponent.show();
        this.colourPickerOverlay = colourPickerComponent.overlayRef.overlayElement;
        colourPickerComponent.overlayRef.detachBackdrop();
      }
    });
    this.subscriptions.push(colourPickerSubscription);
  }

  ngOnDestroy(): void {
    this.map.off('click', () => this.toggleColourPicker());
    this.map.off('moveend', e => this.updateCoords(e));
    this.map.off('draw.create', e => this.setFeatureColour(e));
    this.map.off('draw.delete', () => this.deleteFeature());
    this.map.off('draw.selectionchange', e => this.selectFeature(e));
    this.map.off('draw.update', () => this.saveChanges());
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  setMapControls() {
    const satelliteControl = new SatelliteSelectControl(this.defaultStyle, this.satelliteStyle);
    this.draw = new MapboxDraw({
      userProperties: true,
      controls: {
        'combine_features': false,
        'uncombine_features': false,
      },
      styles: basetheme
    });
    this.map.addControl(satelliteControl, 'top-right');
    this.map.addControl(this.draw, 'top-left');
  }

  toggleColourPickerView() {
    this.showColourPicker = !this.showColourPicker;
    if (!this.showColourPicker) this.colourPickerOverlay.remove();
  }

  updateColour(colour: string) {
    this.colour = colour;
    this.draw.getAll().features.forEach(feature =>
      this.draw.setFeatureProperty(feature.id, 'colour', colour)
    );
    this.saveChanges().subscribe(() => this.pushNewCollection());
  }

  createPopupContent(mode: string, feature: GeoJSON.Feature) {
    const component = this.resolver.resolveComponentFactory(HtmlMarkerComponent).create(this.injector);
    component.instance.data = { key: this.entityKey, name: this.entityName, mode: mode, draw: this.draw, feature: feature };
    component.changeDetectorRef.detectChanges();
    return component.location.nativeElement;
  }

  saveTitle() {
    this.setName(this.titleInput.value);
    this.saveChanges().subscribe((res) => {
      this.entityKey = res.resource.key;
      this.pushNewCollection();
    });
  }

  setName(name: string) {
    if (!this.entityName) this.setMapControls();
    this.entityName = name;
    this.titleEditable = false;
  }

  saveChanges() {
    return this.annotationService.upsertResources(this.entityKey, this.entityName, this.draw.getAll(), this.colour);
  }

  pushNewCollection() {
    this.annotationService.annotationCollection.next({ key: this.entityKey, name: this.entityName, resourceType: this.colour });
  }
}
