/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PreferenceService } from 'app/core/services/preference/preference.service';
import { AnnotationService } from 'app/core/services/visualisation/annotation.service';
import { ConfirmDeleteDialogComponent } from 'app/ui/form-components/confirm-delete-dialog/confirm-delete-dialog.component';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

@Component({
  selector: 'annotation-gallery',
  templateUrl: './annotation-gallery.component.html',
  styleUrls: ['./annotation-gallery.component.scss']
})
export class AnnotationGalleryComponent implements OnInit, OnDestroy {
  selected: string;
  annotationCollections: { key: string, name: string, resourceType: string }[] = [];
  subscriptions: Subscription[] = [];


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private prefService: PreferenceService,
    private annotationService: AnnotationService,
    public dialog: MatDialog) {
  }

  ngOnInit() {
    const annotationListSubscription = this.annotationService.listAnnotations()
      .subscribe(result => this.annotationCollections = result.resources);
    this.subscriptions.push(annotationListSubscription);

    const annotationSubscription = this.annotationService.annotationCollection.subscribe(r => {
      const collection = this.annotationCollections
        .filter(c => c.key === r.key)
        .map(c => { c['name'] = r.name; c['resourceType'] = r.resourceType; return c; });
      if (collection.length === 0) this.annotationCollections.push(r);
      this.selectCollection(r.key);
    });
    this.subscriptions.push(annotationSubscription);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  collectionByName(index: number, annotationCollection: { key: string, name: string }): string {
    return annotationCollection.name;
  }

  selectCollection(selected: string) {
    this.selected = selected;
    this.router.navigate([`./edit/${selected}`], { relativeTo: this.route });
  }
  newAnnotationCollection() {
    this.selected = null;
    this.router.navigate(['./new/'], { relativeTo: this.route });
  }


  deleteCollection(key: string, name: string) {
    const dialogRef = this.dialog.open(ConfirmDeleteDialogComponent, {
      data: { message: 'Would you like to delete this annotation gallery?', confirmName: false }
    });

    const deleteDialogSubscription = dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.prefService.deleteResource(key, name).pipe(first()).subscribe();
        this.prefService.deleteEntity(key).pipe(first()).subscribe();
        this.annotationCollections = this.annotationCollections.filter((collection) => !(collection.name === name));
        this.router.navigate(['.'], { relativeTo: this.route });
      }
    });
    this.subscriptions.push(deleteDialogSubscription);
  }

}
