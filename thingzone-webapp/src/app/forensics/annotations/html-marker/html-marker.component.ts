/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, OnInit, Input } from '@angular/core';
import * as MapboxDraw from '@mapbox/mapbox-gl-draw';
import { AnnotationService } from 'app/core/services/visualisation/annotation.service';


@Component({
  selector: 'html-marker',
  templateUrl: './html-marker.component.html',
  styleUrls: ['./html-marker.component.scss']
})
export class HtmlMarkerComponent implements OnInit {
  @Input() data: {
    key: string;
    name: string;
    mode: string,
    draw: MapboxDraw,
    feature: GeoJSON.Feature,
  };

  name = '';
  description = '';

  constructor(private annotationService: AnnotationService) { }

  ngOnInit(): void {
    if (this.data.feature.properties['name']) this.name = this.data.feature.properties['name'];
    if (this.data.feature.properties['description']) this.description = this.data.feature.properties['description'];
  }

  save() {
    this.data.draw.setFeatureProperty(this.data.feature.id, 'name', this.name);
    this.data.draw.setFeatureProperty(this.data.feature.id, 'description', this.description);
    this.data.draw.changeMode('draw_line_string'); // should be direct_select... but doesn't trigger onSelectionChange (bug?)

    this.annotationService.upsertResources(this.data.key, this.data.name, this.data.draw.getAll(), this.data.feature.properties['colour']);

  }
}
