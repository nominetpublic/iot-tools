/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatInputModule } from '@angular/material';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faPlus, faTrash } from '@fortawesome/free-solid-svg-icons';
import { ColourPickerModule } from 'app/ui/colour-picker/colour-picker.module';
import { NavigationMenuModule } from 'app/ui/navigation-menu/navigation-menu.module';
import { VisualisationGalleryModule } from 'app/visualisations/visualisation-gallery/visualisation-gallery.module';

import { AnnotationEditComponent } from './annotations/annotation-edit/annotation-edit.component';
import { AnnotationGalleryComponent } from './annotations/annotation-gallery/annotation-gallery.component';
import { HtmlMarkerComponent } from './annotations/html-marker/html-marker.component';
import { ForensicsRoutingModule } from './forensics-routing.module';
import { DataWrapperComponent } from './sub-menu/data-wrapper/data-wrapper.component';

@NgModule({
  imports: [
    CommonModule,
    ForensicsRoutingModule,
    VisualisationGalleryModule,
    NavigationMenuModule,
    FontAwesomeModule,
    MatButtonModule,
    MatInputModule,
    ColourPickerModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    DataWrapperComponent,
    AnnotationEditComponent,
    AnnotationGalleryComponent,
    HtmlMarkerComponent,
  ],
  entryComponents: [
    HtmlMarkerComponent
  ],

})
export class ForensicsModule {
  constructor() {
    library.add(faPlus, faTrash);
  }
}
