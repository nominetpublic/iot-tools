/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AnnotationEditComponent } from './annotations/annotation-edit/annotation-edit.component';
import { AnnotationGalleryComponent } from './annotations/annotation-gallery/annotation-gallery.component';
import { DataWrapperComponent } from './sub-menu/data-wrapper/data-wrapper.component';

const routes: Routes = [
  {
    path: '',
    component: DataWrapperComponent,
    children: [
      { path: '', redirectTo: 'visualisations', pathMatch: 'full' },
      {
        path: 'annotations',
        component: AnnotationGalleryComponent,
        children: [
          { path: 'new', component: AnnotationEditComponent },
          { path: 'edit/:name', component: AnnotationEditComponent }]
      },
      {
        path: 'visualisations',
        children: [
          {
            path: '',
            data: { visualisationType: 'visualisation' },
            loadChildren: () => import('../visualisations/visualisation-gallery/visualisation-gallery.module')
              .then(mod => mod.VisualisationGalleryModule),
          },
        ],
      },
    ]
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ForensicsRoutingModule { }
