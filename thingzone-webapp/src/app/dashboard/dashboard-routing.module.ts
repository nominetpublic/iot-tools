/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { IntroductionComponent } from './introduction/introduction.component';
import { DashboardWrapperComponent } from './sub-menu/dash-wrapper/dash-wrapper.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardWrapperComponent,
    children: [
      { path: '', redirectTo: 'introduction', pathMatch: 'introduction' },
      {
        path: 'introduction',
        component: IntroductionComponent,
      },
      {
        path: 'overview',
        data: { widgetType: 'widget-overview' },
        loadChildren: () => import('../visualisations/widget-gallery/widget-gallery.module').then(mod => mod.WidgetGalleryModule),
      },
      {
        path: 'events',
        data: { widgetType: 'events-overview' },
        loadChildren: () => import('../visualisations/widget-gallery/widget-gallery.module').then(mod => mod.WidgetGalleryModule),
      },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class DashRoutingModule { }
