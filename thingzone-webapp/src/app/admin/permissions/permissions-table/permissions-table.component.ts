/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatCheckboxChange, MatPaginator } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { Permission } from 'app/core/models/common/permission';
import { PermissionsList } from 'app/core/models/common/permissions-list';
import { SelectOption } from 'app/core/models/common/select-option';
import { UserApiService } from 'app/core/services/api/user-api.service';
import { AuthService } from 'app/core/services/auth/auth.service';
import { BehaviorSubject, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';

import { EntityPermissions } from '../models/entity-permissions';
import { PermissionsDataSource } from '../models/permissionsDataSource';

@Component({
  selector: 'permissions-table',
  templateUrl: './permissions-table.component.html',
  styleUrls: ['./permissions-table.component.scss']
})
export class PermissionsTableComponent implements OnInit, AfterViewInit {

  nameFilter: string = null;
  typeFilter: string = null;
  groupFilter: string = null;
  showPermissions = false;
  limit = 10;
  page = 1;
  entityData = new Subject<any>();
  dataSource: PermissionsDataSource;

  filterForm = this.formBuilder.group({
    username: [],
    groupname: [],
    type: [],
    name: [],
  });

  private usernameOptionsSubject: BehaviorSubject<SelectOption[]> = new BehaviorSubject([]);
  usernameOptions$ = this.usernameOptionsSubject.asObservable();

  private groupOptionsSubject: BehaviorSubject<SelectOption[]> = new BehaviorSubject([]);
  groupOptions$ = this.groupOptionsSubject.asObservable();

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  baseColumns = ['key', 'entity', 'type', 'name'];
  permissionColumns = ['admin', 'modify', 'discover', 'consume', 'upload', 'view_metadata', 'view_location',
    'consume_events', 'upload_events', 'annotate'];
  displayedColumns = this.baseColumns;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private userApi: UserApiService
  ) { }

  ngOnInit() {
    this.dataSource = new PermissionsDataSource(this.userApi);
    this.dataSource.loadEntities(this.showPermissions);
    this.userApi.listUsers().subscribe(names => {
      const usernames: SelectOption[] = names.map(name => ({ title: name, value: name }));
      usernames.unshift({ title: '', value: null });
      this.usernameOptionsSubject.next(usernames);
    });

    this.userApi.listGroups(false, true).subscribe(names => {
      const groupnames: SelectOption[] = names.map(name => ({ title: name, value: name }));
      groupnames.unshift({ title: '', value: null });
      this.groupOptionsSubject.next(groupnames);
    });

    this.filterForm.valueChanges
      .pipe(
        debounceTime(200),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.firstPage();
          this.loadEntitiesPage();
        })
      )
      .subscribe();
  }

  ngAfterViewInit() {
    this.paginator.page
      .pipe(
        tap(() => this.loadEntitiesPage())
      )
      .subscribe();
  }

  loadEntitiesPage(): void {
    this.page = this.paginator.pageIndex;
    if (this.showPermissions) {
      this.displayedColumns = this.baseColumns.concat(this.permissionColumns);
    } else {
      this.displayedColumns = this.baseColumns;
    }
    let name: string = this.filterForm.get('name').value;
    if (name != null) {
      name = name.trim();
    }
    let type: string = this.filterForm.get('type').value;
    if (type != null) {
      type = type.trim();
    }
    this.dataSource.loadEntities(this.showPermissions, this.filterForm.get('username').value, this.filterForm.get('groupname').value,
      name, type, this.paginator.pageSize, this.page);
  }

  selectUsername(username: string): void {
    if (this.filterForm.get('groupname').value != null) {
      this.selectGroup(null);
    }
    this.showPermissions = username != null ? true : false;
    this.filterForm.get('username').setValue(username);
  }

  selectGroup(groupname: string): void {
    if (this.filterForm.get('username').value != null) {
      this.selectUsername(null);
    }
    this.showPermissions = groupname != null ? true : false;
    this.filterForm.get('groupname').setValue(groupname);
  }

  hasPermission(row: EntityPermissions, permission: string): boolean {
    const enumPermission: Permission = permission as Permission;
    if (row.userPermissions != null) {
      const username = this.filterForm.get('username').value;
      if (username in row.userPermissions) {
        return row.userPermissions[username].some(e => e === enumPermission);
      }
      return false;
    }
    if (row.groupPermissions != null) {
      const groupname = this.filterForm.get('groupname').value;
      if (groupname in row.groupPermissions) {
        return row.groupPermissions[groupname].some(e => e === enumPermission);
      } else {
        return false;
      }
    }
  }

  togglePermission(row: EntityPermissions, permission: string, event: MatCheckboxChange): void {
    const enumPermission: Permission = permission as Permission;
    if (row.userPermissions != null) {
      const username = this.filterForm.get('username').value;
      this.setPermission(row.key, row.userPermissions, enumPermission, event.checked, username, null);
    }
    if (row.groupPermissions != null) {
      const groupname = this.filterForm.get('groupname').value;
      this.setPermission(row.key, row.groupPermissions, enumPermission, event.checked, null, groupname);
    }
  }

  setPermission(key: string, permissionList: PermissionsList, permission: Permission, checked: boolean,
    username: string, groupname: string) {
    if (checked) {
      this.userApi.updatePermission([key], [permission.toString()], username, groupname).subscribe(() => {
        if (name in permissionList) {
          if (!permissionList[name].some(e => e === permission)) {
            permissionList[name].push(permission);
          }
        } else {
          permissionList[name] = [permission];
        }
      });
    } else {
      this.userApi.updatePermission([key], [permission.toString()], username, groupname, true).subscribe(() => {
        if (permissionList[name].some(e => e === permission)) {
          permissionList[name] = permissionList[name].filter(e => e !== permission);
        }
      });
    }
  }
}
