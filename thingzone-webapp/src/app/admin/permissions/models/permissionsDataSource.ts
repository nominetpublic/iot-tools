/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { UserApiService } from 'app/core/services/api/user-api.service';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';

import { EntityPermissions } from './entity-permissions';

export class PermissionsDataSource implements DataSource<EntityPermissions> {

    private permissionsSubject = new BehaviorSubject<EntityPermissions[]>([]);
    private entitiesCountSubject = new BehaviorSubject<number>(0);
    public entitiesCount$ = this.entitiesCountSubject.asObservable();
    private loadingSubject = new BehaviorSubject<boolean>(false);
    public loading$ = this.loadingSubject.asObservable();

    constructor(private userApi: UserApiService) { }

    connect(collectionViewer: CollectionViewer): Observable<EntityPermissions[]> {
        return this.permissionsSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.permissionsSubject.complete();
        this.loadingSubject.complete();
    }

    loadEntities(permissions: boolean, username?: string, groupname?: string, nameFilter?: string,
        typeFilter?: string, size?: number, page?: number) {
        this.userApi.listAdminEntities(nameFilter, typeFilter, permissions, username, groupname, size, page)
            .pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false)))
            .subscribe(res => {
                this.permissionsSubject.next(res);
                this.entitiesCountSubject.next(res.length);
            });
    }
}
