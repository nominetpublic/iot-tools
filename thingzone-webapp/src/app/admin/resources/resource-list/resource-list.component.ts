/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Constants } from 'app/core/models/common/constants';
import { ResourceModel } from 'app/core/models/common/resource-model';
import { PreferenceService } from 'app/core/services/preference/preference.service';
import { BehaviorSubject, Subscription } from 'rxjs';

@Component({
  selector: 'resource-list',
  templateUrl: './resource-list.component.html',
  styleUrls: ['./resource-list.component.scss']
})
export class ResourceListComponent implements OnInit, OnDestroy {
  applicationType: string = Constants.applicationType;
  private subscriptions: Subscription[] = [];
  icons$ = new BehaviorSubject<ResourceModel[]>([]);
  config$ = new BehaviorSubject<ResourceModel[]>([]);
  addIcon = false;
  addConfig = false;

  filterIconText = '';
  filterIconControl = new FormControl('');

  filterConfigText = '';
  filterConfigControl = new FormControl('');

  constructor(
    private preferenceService: PreferenceService
  ) { }

  ngOnInit() {
    this.updateIcons();
    this.updateConfig();

    const filterIconSub = this.filterIconControl.valueChanges.subscribe(value => {
      this.filterIconText = value;
    });
    this.subscriptions.push(filterIconSub);

    const filterConfigSub = this.filterConfigControl.valueChanges.subscribe(value => {
      this.filterConfigText = value;
    });
    this.subscriptions.push(filterConfigSub);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  updateIcons() {
    this.preferenceService.listResources(this.applicationType, 'mapIcon').subscribe(res => {
      const iconList: ResourceModel[] = res.resources.sort((a, b) => a.resourceName.localeCompare(b.resourceName));
      this.icons$.next(iconList);
      this.addIcon = false;
    });
  }

  newIcon() {
    this.addIcon = !this.addIcon;
  }

  updateConfig() {
    this.preferenceService.listJsonResources(this.applicationType, 'configuration')
      .subscribe(resources => {
        const configList: ResourceModel[] = resources.sort((a, b) => a.resourceName.localeCompare(b.resourceName));
        this.config$.next(configList);
        this.addConfig = false;
      });
  }

  newConfig() {
    this.addConfig = !this.addConfig;
  }

}
