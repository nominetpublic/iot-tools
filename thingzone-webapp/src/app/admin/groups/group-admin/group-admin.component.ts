/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GroupModel } from 'app/core/models/auth/group-model';
import { UserApiService } from 'app/core/services/api/user-api.service';
import { AuthService } from 'app/core/services/auth/auth.service';
import { Privilege } from 'app/core/services/auth/privilege';
import { of, Subject, Subscription } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';

import { GroupsService } from '../groups.service';

@Component({
  providers: [GroupsService],
  selector: 'group-admin',
  templateUrl: './group-admin.component.html',
  styleUrls: ['./group-admin.component.scss']
})
export class GroupAdminComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  groups: GroupModel[] = [];
  updateGroups$ = new Subject();
  showCreate = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userApi: UserApiService,
    private authService: AuthService,
    private groupsService: GroupsService
  ) { }

  ngOnInit() {
    const groupSub = this.groupsService.groups$.subscribe(groups => {
      this.groups = groups;
    });
    this.subscriptions.push(groupSub);

    this.authService.checkUserRole(Privilege.CREATE_GROUP).subscribe(hasPrivilege => {
      this.showCreate = hasPrivilege;
    });

    const updateGroupsSub = this.groupsService.updateGroups$.pipe(
      switchMap(() => this.userApi.listGroups(true)
        .pipe(
          catchError(error => {
            return of(null);
          })
        ))
    ).subscribe(res => {
      const grouplist: GroupModel[] = res.map(name => ({ name: name, users: null }));
      this.groupsService.setGroups(grouplist);
    });
    this.subscriptions.push(updateGroupsSub);

    this.groupsService.updateGroups();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  newGroup() {
    this.router.navigate([`./new`], { relativeTo: this.route });
  }

  updateGroup(groupName: string) {
    this.router.navigate([`./update/${groupName}`], { relativeTo: this.route });
  }
}
