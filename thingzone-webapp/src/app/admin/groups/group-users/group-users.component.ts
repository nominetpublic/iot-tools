/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { GroupModel } from 'app/core/models/auth/group-model';
import { UserApiService } from 'app/core/services/api/user-api.service';

import { GroupsService } from '../groups.service';

@Component({
  selector: 'group-users',
  templateUrl: './group-users.component.html',
  styleUrls: ['./group-users.component.scss']
})
export class GroupUsersComponent implements OnInit {

  @Input() group: GroupModel;
  addUserForm = this.formBuilder.group({
    name: [],
  });

  constructor(
    private userApi: UserApiService,
    private groupsService: GroupsService,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    if (this.group.users == null) {
      this.group.users = [];
      this.groupsService.updateGroupByName(this.group);
      this.updateUsers();
    }
  }

  addUser() {
    const formValues = this.addUserForm.value;
    if (formValues['name'] != null && formValues['name'] !== '') {
      this.userApi.addGroupUsers(this.group.name, [formValues['name']]).subscribe(res => {
        this.group.users.push({ username: formValues['name'], admin: false });
        this.groupsService.updateGroupByName(this.group);
        this.addUserForm.reset();
      });
    }
  }

  removeUser(username) {
    this.userApi.removeGroupUsers(this.group.name, [username]).subscribe(res => {
      this.group.users = this.group.users.filter(user => user.username !== username);
      this.groupsService.updateGroupByName(this.group);
    });
  }

  setAdmin(username, currentAdmin) {
    const newAdmin = !currentAdmin;
    this.userApi.groupUserAdmin(this.group.name, username, newAdmin).subscribe(res => {
      this.group.users = this.group.users.map(user => {
        if (user.username === username) {
          user.admin = newAdmin;
        }
        return user;
      });
      this.groupsService.updateGroupByName(this.group);
    });
  }

  updateUsers() {
    this.userApi.listGroupUsers(this.group.name).subscribe(res => {
      if (res.length > 0) {
        const updateGroup: GroupModel = { name: this.group.name, users: res };
        this.groupsService.updateGroupByName(updateGroup);
      }
    });
  }
}
