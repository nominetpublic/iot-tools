/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { GroupModel } from 'app/core/models/auth/group-model';
import { UserApiService } from 'app/core/services/api/user-api.service';
import { BehaviorSubject, Subject } from 'rxjs';


@Injectable()
export class GroupsService {
  groups$ = new BehaviorSubject([]);

  updateGroups$ = new Subject();

  constructor(
    private userApi: UserApiService,
  ) { }

  setGroups(groups: GroupModel[]): void {
    this.groups$.next(groups);
  }

  getGroups(): GroupModel[] {
    return this.groups$.getValue();
  }

  updateGroups() {
    this.updateGroups$.next(true);
  }

  updateGroupByName(updatedGroup: GroupModel) {
    const groups = this.getGroups();
    const updatedItems = groups.map(e => {
      if (e['name'] === updatedGroup.name) {
        return updatedGroup;
      }
      return e;
    });
    this.setGroups(updatedItems);
  }
}
