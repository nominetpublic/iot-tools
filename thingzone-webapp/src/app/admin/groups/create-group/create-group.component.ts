/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserApiService } from 'app/core/services/api/user-api.service';
import { Subscription } from 'rxjs';

import { GroupsService } from '../groups.service';

@Component({
  selector: 'create-group',
  templateUrl: './create-group.component.html',
  styleUrls: ['./create-group.component.scss']
})
export class CreateGroupComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  groupForm = this.formBuilder.group({
    name: ['', Validators.required],
  });
  updated: boolean;

  constructor(
    private formBuilder: FormBuilder,
    protected route: ActivatedRoute,
    protected router: Router,
    private userApi: UserApiService,
    private groupsService: GroupsService,
  ) { }

  ngOnInit() {
    const formSub = this.groupForm.valueChanges.subscribe(() => {
      this.updated = this.groupForm.invalid ? false : true;
    });
    this.subscriptions.push(formSub);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  close() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  createGroup() {
    if (!this.groupForm.invalid) {
      const formValues = this.groupForm.value;
      this.userApi.createGroup(formValues['name']).subscribe(res => {
        this.groupsService.updateGroups();
        this.close();
      });
    }
  }

}
