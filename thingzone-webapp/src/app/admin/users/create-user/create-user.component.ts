/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserApiService } from 'app/core/services/api/user-api.service';
import { Subscription } from 'rxjs';

import { UserListService } from '../user-list.service';

@Component({
  selector: 'create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  userForm = this.formBuilder.group({
    username: ['', Validators.required],
    password: ['', [Validators.required, Validators.minLength(6)]],
    email: [],
    privileges: [],
  });
  updated: boolean;

  constructor(
    private formBuilder: FormBuilder,
    protected route: ActivatedRoute,
    protected router: Router,
    private userApi: UserApiService,
    private userListService: UserListService) { }

  ngOnInit() {
    const formSub = this.userForm.valueChanges.subscribe(() => {
      this.updated = true;
    });
    this.subscriptions.push(formSub);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  close() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  createUser() {
    const formValues = this.userForm.value;
    this.userApi.createUser(formValues).subscribe(res => {
      this.userListService.updateUsers();
      this.close();
    });
  }
}
