/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { UserModel } from 'app/core/models/auth/user-model';
import { UserApiService } from 'app/core/services/api/user-api.service';
import { ConfirmDeleteDialogComponent } from 'app/ui/form-components/confirm-delete-dialog/confirm-delete-dialog.component';
import { Subscription } from 'rxjs';

import { UserListService } from '../user-list.service';

@Component({
  selector: 'update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.scss']
})
export class UpdateUserComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  privilegeForm = this.formBuilder.group({
    privileges: [],
  });
  userForm = this.formBuilder.group({
    username: [],
  });
  updated: boolean;
  privilegeUpdated: boolean;
  user: UserModel;
  tokenParams: { username: string; token: string; } = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private userApi: UserApiService,
    private userListService: UserListService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if ('name' in params) {
        this.userForm.reset();
        this.privilegeForm.reset();
        this.userApi.getOtherUser(params.name).subscribe(res => {
          this.user = res;
          if (this.user.privileges) {
            this.privilegeForm.patchValue({ privileges: this.user.privileges }, { onlySelf: true, emitEvent: false });
          }
        });
      }
    });

    const userFormSub = this.userForm.valueChanges.subscribe(() => {
      this.updated = this.userForm.invalid ? false : true;
    });
    this.subscriptions.push(userFormSub);
    const privFormSub = this.privilegeForm.valueChanges.subscribe(() => {
      this.privilegeUpdated = this.privilegeForm.invalid ? false : true;
    });
    this.subscriptions.push(privFormSub);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  close() {
    this.router.navigate(['../../'], { relativeTo: this.route });
  }

  updatePrivileges() {
    const formValues = this.privilegeForm.value;
    this.user.privileges = formValues['privileges'].split(',');
    this.userApi.setUserPrivileges(this.user).subscribe(res => {
      this.privilegeUpdated = false;
    });
  }

  updateUser() {
    const formValues = this.userForm.value;
    this.userApi.updateUser(this.user.username, formValues['username']).subscribe(res => {
      this.userListService.updateUsers();
      this.close();
    }, (error: HttpErrorResponse) => {
      if (error.status === 409) {
        this.userForm.get('username').setErrors({ forbiddenName: true });
      }
    });
  }

  resetPassword() {
    const dialogRef = this.dialog.open(ConfirmDeleteDialogComponent, {
      data: { message: `Are you sure you want to reset the password for user ${this.user.username}`, confirmName: false }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.userApi.requestPasswordToken(this.user.username).subscribe(res => {
          this.tokenParams = { username: this.user.username, token: res.token };
        });
      }
    });
  }
}
