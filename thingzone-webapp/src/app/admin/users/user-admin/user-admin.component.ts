/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserApiService } from 'app/core/services/api/user-api.service';
import { AuthService } from 'app/core/services/auth/auth.service';
import { Privilege } from 'app/core/services/auth/privilege';
import { of, Subscription } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';

import { UserListService } from '../user-list.service';

@Component({
  providers: [UserListService],
  selector: 'user-admin',
  templateUrl: './user-admin.component.html',
  styleUrls: ['./user-admin.component.scss']
})
export class UserAdminComponent implements OnInit, OnDestroy {

  subscriptions: Subscription[] = [];
  users: string[];
  showCreate = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private userApi: UserApiService,
    private userListService: UserListService) { }

  ngOnInit() {
    const userSub = this.userListService.updateUsers$.pipe(
      switchMap(() => this.userApi.listUsers()
        .pipe(
          catchError(error => {
            return of(null);
          })
        ))
    ).subscribe(usersList => {
      this.users = usersList;
    });
    this.subscriptions.push(userSub);
    this.authService.checkUserRole(Privilege.CREATE_USER).subscribe(hasPrivilege => {
      this.showCreate = hasPrivilege;
    });
    this.userListService.updateUsers();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  newUser() {
    this.router.navigate([`./new`], { relativeTo: this.route });
  }

  editUser(username: string) {
    this.router.navigate([`./update/${username}`], { relativeTo: this.route });
  }
}
