/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PrivilegeGuard } from 'app/core/services/auth/guards/privilege.guard';
import { Privilege } from 'app/core/services/auth/privilege';

import { CreateGroupComponent } from './groups/create-group/create-group.component';
import { GroupAdminComponent } from './groups/group-admin/group-admin.component';
import { UpdateGroupComponent } from './groups/update-group/update-group.component';
import { PermissionsTableComponent } from './permissions/permissions-table/permissions-table.component';
import { ChangePasswordComponent } from './profile/change-password/change-password.component';
import { ProfileComponent } from './profile/profile/profile.component';
import { ResourceListComponent } from './resources/resource-list/resource-list.component';
import { AdminWrapperComponent } from './sub-menu/admin-wrapper/admin-wrapper.component';
import { CreateUserComponent } from './users/create-user/create-user.component';
import { UpdateUserComponent } from './users/update-user/update-user.component';
import { UserAdminComponent } from './users/user-admin/user-admin.component';

export const routes: Routes = [
  {
    path: '', component: AdminWrapperComponent, children: [
      { path: '', redirectTo: 'profile', pathMatch: 'full' },
      {
        path: 'profile',
        component: ProfileComponent,
        children: [
          {
            path: 'password',
            component: ChangePasswordComponent,
          },
        ]
      },
      {
        path: 'resources',
        canActivate: [PrivilegeGuard],
        data: { role: Privilege.CREATE_RESOURCE },
        component: ResourceListComponent,
      },
      {
        path: 'users',
        canActivate: [PrivilegeGuard],
        data: { role: Privilege.USER_ADMIN },
        component: UserAdminComponent,
        children: [
          {
            path: 'new',
            canActivate: [PrivilegeGuard],
            data: { role: Privilege.CREATE_USER },
            component: CreateUserComponent,
          },
          {
            path: 'update/:name',
            canActivate: [PrivilegeGuard],
            data: { role: Privilege.USER_ADMIN },
            component: UpdateUserComponent,
          }
        ],
      },
      {
        path: 'groups',
        component: GroupAdminComponent,
        children: [
          {
            path: 'new',
            canActivate: [PrivilegeGuard],
            data: { role: Privilege.CREATE_GROUP },
            component: CreateGroupComponent,
          },
          {
            path: 'update/:name',
            component: UpdateGroupComponent,
          }
        ],
      },
      {
        path: 'permissions',
        canActivate: [PrivilegeGuard],
        data: { role: Privilege.USER_ADMIN },
        component: PermissionsTableComponent,
      },
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AdminRoutingModule { }
