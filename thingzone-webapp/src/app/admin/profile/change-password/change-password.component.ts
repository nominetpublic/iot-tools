/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserApiService } from 'app/core/services/api/user-api.service';
import { SessionService } from 'app/core/services/session/session.service';

@Component({
  selector: 'change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  passwordForm = this.formBuilder.group({
    currentPassword: ['', Validators.required],
    newPassword: ['', Validators.required],
  });
  updated = false;
  error = false;
  errorMessage: string;
  username: string;

  constructor(
    private formBuilder: FormBuilder,
    protected route: ActivatedRoute,
    protected router: Router,
    private session: SessionService,
    private userApi: UserApiService) { }

  ngOnInit() {
    this.username = this.session.getUser();
    const formSub = this.passwordForm.valueChanges.subscribe(() => {
      this.updated = this.passwordForm.invalid ? false : true;
      this.error = false;
    });
  }

  close() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  updatePassword() {
    if (!this.passwordForm.invalid) {
      const formValues = this.passwordForm.value;
      this.userApi.updatePassword(formValues['currentPassword'], formValues['newPassword']).subscribe(res => {
        this.close();
      }, (error) => {
        this.error = true;
        this.errorMessage = error;
      });
    }
  }
}
