/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatFormFieldModule,
  MatInputModule,
  MatPaginatorIntl,
  MatPaginatorModule,
  MatTableModule,
} from '@angular/material';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faPen, faSave, faSpinner, faStar, faTimes, faTrash, faUpload } from '@fortawesome/free-solid-svg-icons';
import { MatPaginatorLengthService } from 'app/core/services/pagination/mat-paginator-length.service';
import { CardComponentsModule } from 'app/ui/card-components/card-components.module';
import { DynamicSelectModule } from 'app/ui/dynamic-select/dynamic-select.module';
import { FormComponentsModule } from 'app/ui/form-components/form-components.module';
import { ItemPanelModule } from 'app/ui/item-panel/item-panel.module';
import { NavigationMenuModule } from 'app/ui/navigation-menu/navigation-menu.module';
import { ResourceEditorModule } from 'app/ui/resource-editor/resource-editor.module';
import { SharedPipesModule } from 'app/ui/shared-pipes/shared-pipes.module';

import { AdminRoutingModule } from './admin-routing.module';
import { CreateGroupComponent } from './groups/create-group/create-group.component';
import { GroupAdminComponent } from './groups/group-admin/group-admin.component';
import { GroupUsersComponent } from './groups/group-users/group-users.component';
import { PermissionsTableComponent } from './permissions/permissions-table/permissions-table.component';
import { ChangePasswordComponent } from './profile/change-password/change-password.component';
import { ProfileComponent } from './profile/profile/profile.component';
import { ResourceListComponent } from './resources/resource-list/resource-list.component';
import { AdminWrapperComponent } from './sub-menu/admin-wrapper/admin-wrapper.component';
import { CreateUserComponent } from './users/create-user/create-user.component';
import { UpdateUserComponent } from './users/update-user/update-user.component';
import { UserAdminComponent } from './users/user-admin/user-admin.component';
import { UpdateGroupComponent } from './groups/update-group/update-group.component';

@NgModule({
  providers: [
    { provide: MatPaginatorIntl, useClass: MatPaginatorLengthService }
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FontAwesomeModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    NavigationMenuModule,
    SharedPipesModule,
    CardComponentsModule,
    ResourceEditorModule,
    ItemPanelModule,
    DynamicSelectModule,
    FormComponentsModule,
  ],
  declarations: [
    AdminWrapperComponent,
    ResourceListComponent,
    ProfileComponent,
    UserAdminComponent,
    GroupAdminComponent,
    CreateUserComponent,
    CreateGroupComponent,
    GroupUsersComponent,
    ChangePasswordComponent,
    UpdateUserComponent,
    PermissionsTableComponent,
    UpdateGroupComponent,
  ]
})
export class AdminModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faTrash, faUpload, faSave, faTimes, faStar, faPen, faSpinner);
  }
}
