/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SessionService {

    private user;
    private sessionKey;
    sessionKey$ = new BehaviorSubject<boolean>(this.sessionKey);

    private redirectUrl = '/';
    private DEFAULT_URL = '/';
    private bannedRedirects = [
        'login'
    ];

    // Instantiate data when service
    // is loaded
    constructor(
        private router: Router
    ) {
        this.init();
    }

    init(): void {
        this.user = localStorage.getItem('session.user');
        this.sessionKey = localStorage.getItem('session.sessionKey');
        this.sessionKey$.next(this.sessionKey);
    }

    clear(): void {
        this.init();
    }

    getUser(): string {
        return this.user;
    }

    getUserNameShort(): string {
        const name = this.getUser();
        if (name === null) return;
        if (name.indexOf('@') > -1) return name.split('@')[0];
        return name;
    }

    setUser(user): void {
        this.user = user;
        localStorage.setItem('session.user', user);
    }

    getSessionKey(): string {
        return this.sessionKey;
    }

    setSessionKey(key): void {
        this.sessionKey = key;
        localStorage.setItem('session.sessionKey', key);
        this.sessionKey$.next(key);
    }

    getDetails(): object {
        return JSON.parse(localStorage.getItem(localStorage.getItem('session.user') + '.details'));
    }

    setDetails(details): void {
        localStorage.setItem(this.user + '.details', JSON.stringify(details));
    }

    setPrivileges(privs): void {
        localStorage.setItem(this.user + '.privs', JSON.stringify(privs));
    }

    getPrivileges(): string[] {
        return JSON.parse(localStorage.getItem(localStorage.getItem('session.user') + '.privs'));
    }

    getPreferences(): object {
        return JSON.parse(localStorage.getItem(localStorage.getItem('session.user') + '.preferences'));
    }

    setPreferences(preferences): void {
        localStorage.setItem(this.user + '.preferences', JSON.stringify(preferences));
    }

    savePreferences(): void {
        localStorage.setItem(this.user + '.preferences', JSON.stringify(this.getPreferences()));
    }

    getRedirectURL(): string {
        // Get the redirect URL from our auth service
        // If no redirect has been set, use the default
        return this.redirectUrl ? this.redirectUrl : this.DEFAULT_URL;
    }

    setRedirect(url: string): void {
        const strippedUrl = url.replace(/\//g, '').toLowerCase();
        const isBanned = this.bannedRedirects.includes(strippedUrl);
        if (!isBanned) this.redirectUrl = url;
    }

    login(loginObj): void {
        this.setUser(loginObj.username);
        this.setSessionKey(loginObj.sessionKey);
        this.setDetails(loginObj.details || null);
        this.setPreferences(loginObj.preferences || null);
    }

    logout(url?): void {
        if (url) this.setRedirect(url);
        this.router.navigate(['/login']);
        this.destroy();
    }

    destroy(): void {
        // Remove localStorage keys based on username before removing
        // the localStorage username key itself.
        localStorage.removeItem(this.user + '.details');
        localStorage.removeItem(this.user + '.preferences');
        localStorage.removeItem(this.user + '.privs');

        this.user = this.sessionKey = null;
        this.sessionKey$.next(null);

        // Remove username/key localStorage items.
        localStorage.removeItem('session.user');
        localStorage.removeItem('session.sessionKey');
    }
}
