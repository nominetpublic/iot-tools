/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Observer, throwError } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';

import { ApiService } from '../api/api.service';
import { ConfigService } from '../config/config.service';
import { SessionService } from '../session/session.service';

/**
 * Service for querying resources
 */
@Injectable({
  providedIn: 'root'
})
export class PreferenceService {
  application: any;
  defaultApplication: any;

  constructor(private api: ApiService, private http: HttpClient, private session: SessionService, private configService: ConfigService) {
    this.application = configService.getConfigValue('applicationKey');
    this.defaultApplication = configService.getConfigValue('defaultApplicationKey');
  }

  getBlobResource(entityKey, resourceName): Observable<Blob> {
    const url = this.api.getRegistryServer() + `api/resource/${entityKey}/${resourceName}`;
    const options = { headers: this.api.getRegistryHeaders() };

    return this.api.get(url, { ...options, responseType: 'blob' }).pipe(
      catchError(this.api.handleHttpError)
    );
  }

  getResource(entityKey, resourceName): Observable<any> {
    const url = this.api.getRegistryServer() + `api/resource/${entityKey}/${resourceName}`;
    const options = { headers: this.api.getRegistryHeaders() };

    return this.api.get(url, options);
  }

  listResourceEntity(entityKey): Observable<any> {
    const url = this.api.getRegistryServer() + `api/resource/list/${entityKey}`;
    const options = { headers: this.api.getRegistryHeaders() };

    return this.api.get(url, options).pipe(map(res => res.resources));
  }

  listResources(type?: string, resource_type?: string): Observable<any> {
    const url = this.api.getRegistryServer() + `api/resource/list`;
    const options = {
      headers: this.api.getRegistryHeaders(),
    };
    const params = {};
    if (type != null) {
      params['type'] = type;
    }
    if (resource_type != null) {
      params['resource_type'] = resource_type;
    }
    options['params'] = params;

    return this.api.get(url, options);
  }

  listJsonResources(type?: string, resource_type?: string): Observable<any[]> {
    const url = this.api.getRegistryServer() + `api/resource/json`;
    const options = {
      headers: this.api.getRegistryHeaders(),
    };
    const params = {};
    if (type != null) {
      params['type'] = type;
    }
    if (resource_type != null) {
      params['resource_type'] = resource_type;
    }
    options['params'] = params;

    return this.api.get(url, options).pipe(map(res => res.resources));
  }

  upsertResource(resourceName: string, content: Blob, entityKey?: string, resourceType?: string,
    entityType?: string, entityName?: string): Observable<any> {
    const url = this.api.getRegistryServer() + 'api/resource/upsert';
    const options = { headers: this.api.getRegistryUpsertHeaders() };

    const formData = new FormData();
    formData.append('resource_name', resourceName);
    formData.append('content', content);
    if (entityKey != null) formData.append('key', entityKey);
    if (resourceType != null) formData.append('resource_type', resourceType);
    if (entityType != null) formData.append('type', entityType);
    if (entityName != null) formData.append('name', entityName);

    return this.api.put(url, formData, options);
  }

  deleteResource(entityKey: string, resourceName: string): Observable<any> {
    const url = this.api.getRegistryServer() + `api/resource/${entityKey}/${resourceName}`;
    const options = { headers: this.api.getRegistryUpsertHeaders() };

    return this.api.delete(url, options);
  }

  deleteEntity(key: string): Observable<any> {
    const url = this.api.getRegistryServer() + `api/entity/delete`;
    const options = { headers: this.api.getRegistryHeaders() };
    const params = { key: key };
    options['params'] = params;
    return this.api.delete(url, options);
  }

  getJsonPreference(key): Observable<any> {
    return this.getResource(this.application, key).pipe(
      map(data => {
        return data;
      }),
      catchError(err => {
        return this.getResource(this.defaultApplication, key).pipe(
          map(defaultData => {
            return defaultData;
          }),
          catchError(defaultError => {
            return throwError(`Failed to load resource ${key}`);
          }));
      }));
  }

  getImageResource(name, key = this.application): Observable<HTMLImageElement> {
    return this.getBlobResource(key, name).pipe(
      mergeMap(data => {
        return this.readFile(data);
      }),
      mergeMap(imageFile => {
        return this.loadImage(imageFile);
      })
    );
  }

  private loadImage(imagePath: string): Observable<HTMLImageElement> {
    return new Observable((observer: Observer<HTMLImageElement>) => {
      const img = new Image();
      img.src = imagePath;
      img.onload = () => {
        observer.next(img);
        observer.complete();
      };
      img.onerror = err => {
        observer.error(err);
      };
    });
  }

  readFile(file: Blob): Observable<string> {
    return new Observable((observer: Observer<string>) => {
      const reader = new FileReader();
      if (file) {
        reader.readAsDataURL(file);
      }
      reader.addEventListener('error', () => {
        reader.abort();
      });

      reader.addEventListener('load', () => {
        observer.next(reader.result as string);
        observer.complete();
      }, false);
    });
  }

}
