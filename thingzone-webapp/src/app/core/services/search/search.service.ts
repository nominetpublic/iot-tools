/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable, OnDestroy } from '@angular/core';
import { SearchTerm } from 'app/core/models/common/search-term';
import { SavedSearch } from 'app/ui/search/models/active-search';
import { SearchResource } from 'app/ui/search/models/search-resource';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { PreferenceService } from '../preference/preference.service';
import { SessionService } from '../session/session.service';


@Injectable({
  providedIn: 'root',
})
export class SearchService implements OnDestroy {
  subscriptions: Subscription[] = [];
  name = 'New Search';

  private modalOpen = new BehaviorSubject('');
  $modalOpen = this.modalOpen.asObservable();

  private search: BehaviorSubject<SavedSearch> = new BehaviorSubject({ terms: [] });
  $search = this.search.asObservable().pipe(debounceTime(0));

  private deviceSearches: BehaviorSubject<SearchResource[]> = new BehaviorSubject(null);
  deviceSavedSearches$ = this.deviceSearches.asObservable();

  private alertSearches: BehaviorSubject<SearchResource[]> = new BehaviorSubject(null);
  alertSavedSearches$ = this.alertSearches.asObservable();

  constructor(
    private session: SessionService,
    private preferenceService: PreferenceService) {
    // reset on logout
    const sessionSubscription = this.session.sessionKey$.subscribe(key => {
      if (key == null) {
        this.clearTerms();
        this.deviceSearches.next(null);
        this.alertSearches.next(null);
      }
    });
    this.subscriptions.push(sessionSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  clearTerms() {
    this.search.next({ terms: [] });
  }

  /**
   * Remove the SearchTerm block matching the property
   */
  removeBlock(property: string): void {
    const currentSearch: SavedSearch = this.search.getValue();

    const filteredTerms = currentSearch.terms.filter(a => a.property !== property);
    // Do not keep current search name after modifying values
    this.search.next({ terms: filteredTerms });
  }

  /**
   * Remove an individual value from a SearchTerm block
   */
  removeTerm(property: string, value: any): void {
    const currentSearch = this.search.getValue();
    const index = currentSearch.terms.findIndex(a => a.property === property);

    if (index === -1) return;

    const existing: SearchTerm = currentSearch.terms[index];
    const filteredValues = existing.values.filter((a => a !== value));

    // The searchTerm values are now empty - remove the entire block
    if (!filteredValues.length) {
      this.removeBlock(property);
    } else {
      // otherwise replace the current SearchTerm block with one with the value removed
      const searchBlock: SearchTerm = {
        property: property,
        values: filteredValues
      };
      // Dont mutate state, use slice(0) to make shallow copy
      const newTerms = currentSearch.terms.slice(0);
      newTerms[index] = searchBlock;
      // Do not keep current search name after modifying values
      this.search.next({ terms: newTerms });
    }
  }

  /**
   * Add a new search type
   */
  addBlock(property: string, value: any): void {
    const searchBlock: SearchTerm = {
      property: property,
      values: [value]
    };

    const currentSearch = this.search.getValue();
    const newTerms = [...currentSearch.terms, searchBlock];
    // Do not keep current search name after modifying values
    this.search.next({ terms: newTerms });
  }

  /**
   * Add an individual value to a SearchTerm block
   */
  addTerm(property: string, value: string, toggle = false): void {
    const currentSearch = this.search.getValue();
    const index = currentSearch.terms.findIndex(a => a.property === property);

    // Check Exists
    if (index === -1) {
      this.addBlock(property, value);
      return;
    }

    const existingSearchTerm = currentSearch.terms[index];
    const existingValue = existingSearchTerm.values.find(a => a === value);

    if (existingValue != null) {
      if (toggle) this.removeTerm(property, value);
      return;
    }

    const searchBlock: SearchTerm = {
      property: property,
      values: [...existingSearchTerm.values, value]
    };

    // Dont mutate state, use slice(0) to make shallow copy
    const newTerms = currentSearch.terms.slice(0);
    newTerms[index] = searchBlock;
    // Do not keep current search name after modifying values
    this.search.next({ terms: newTerms });
  }

  getSearch(): SavedSearch {
    return this.search.getValue();
  }

  setSearch(search: SavedSearch) {
    this.search.next(search);
  }

  /**
   * Save a search as a registry resource
  */
  saveSearch(saveType, searchName): Observable<any> {
    const currentSearch: SavedSearch = this.getSearch();
    const searches = this.sortSavedSearches(this.getSearchResources(saveType));
    currentSearch.name = searchName;
    currentSearch.link = false;
    if (searches.length > 0) {
      currentSearch.after = searches[searches.length - 1].key;
    }
    const content = new Blob([JSON.stringify(currentSearch)], { type: 'application/json' });
    return this.preferenceService.upsertResource(searchName, content, null, saveType, saveType, searchName);
  }

  updateSearch(searchResource: SearchResource): Observable<any> {
    const content = new Blob([JSON.stringify(searchResource.content)], { type: 'application/json' });
    return this.preferenceService.upsertResource(
      searchResource.name, content, searchResource.key, searchResource.type, searchResource.type, searchResource.name);
  }

  getSavedSearch(search: SearchResource): SavedSearch {
    const savedSearch = search.content;
    savedSearch.name = search.name;
    return savedSearch;
  }

  getTerms(): SearchTerm[] {
    return this.search.getValue().terms;
  }

  setTerms(terms: SearchTerm[]) {
    const currentSearch = this.search.getValue();
    this.search.next({ ...currentSearch, terms: terms });
  }

  toggleModal(name?): void {
    if (!name || this.getModal() === name) {
      this.modalOpen.next(null);
    } else {
      this.modalOpen.next(name);
    }
  }

  closeModal(): void {
    this.modalOpen.next(null);
  }

  getModal(): string {
    return this.modalOpen.getValue();
  }

  querySearchResources(type: string): void {
    switch (type) {
      case 'device.savedSearches':
        this.queryDeviceSavedSearches();
        break;
      case 'alert.savedSearches':
        this.queryAlertSavedSearches();
        break;
    }
  }

  getSearchResources(type: string): SearchResource[] {
    switch (type) {
      case 'device.savedSearches':
        return this.getDeviceSavedSearches();
      case 'alert.savedSearches':
        return this.getAlertSavedSearches();
    }
  }

  setSearchResources(type: string, searches: SearchResource[]): void {
    switch (type) {
      case 'device.savedSearches':
        return this.setDeviceSearches(searches);
      case 'alert.savedSearches':
        return this.setAlertSearches(searches);
    }
  }

  queryDeviceSavedSearches(): void {
    this.preferenceService.listJsonResources('device.savedSearches').subscribe((resources) => {
      this.setDeviceSearches(resources);
    });
  }

  getDeviceSavedSearches(): SearchResource[] {
    return this.deviceSearches.getValue();
  }

  setDeviceSearches(searches: SearchResource[]): void {
    this.deviceSearches.next(this.sortSavedSearches(searches));
  }

  queryAlertSavedSearches(): void {
    this.preferenceService.listJsonResources('alert.savedSearches').subscribe((resources) => {
      this.setAlertSearches(resources);
    });
  }

  getAlertSavedSearches(): SearchResource[] {
    return this.alertSearches.getValue();
  }

  setAlertSearches(searches: SearchResource[]): void {
    this.alertSearches.next(this.sortSavedSearches(searches));
  }

  showTerm(searchValue: any, searchTerm: string, index: number): string {
    if (searchTerm === 'geoboundary') {
      return `AREA ${index + 1}`;
    }
    return searchValue;
  }

  sortSavedSearches(searches: SearchResource[]): SearchResource[] {
    if (searches == null) {
      return [];
    }
    if (searches.length <= 1) {
      return searches;
    }
    const sortedList: SearchResource[] = [];
    const map = {};
    let currentKey = null;

    for (let i = 0; i < searches.length; i++) {
      const item: SearchResource = searches[i];
      if (item.content.after == null) {
        currentKey = item.key;
        sortedList.push(item);
      } else {
        map[item.content.after] = i;
      }
    }
    // The map does not have the right number of keys to allow list ordering so something has gone wrong
    if ((Object.keys(map).length + 1) !== searches.length) {
      return this.reorderList(searches);
    }
    while (sortedList.length < searches.length) {
      // get the item with a previous item ID referencing the current item
      if (currentKey in map) {
        const nextItem = searches[map[currentKey]];
        sortedList.push(nextItem);
        currentKey = nextItem.key;
      } else {
        // the map does not contain all possible keys, so stop trying to sort and reset the ordering
        return this.reorderList(searches);
      }
    }
    return sortedList;
  }

  /**
   * Modifies list in place to reset linked list order
   */
  reorderList(searches: SearchResource[]): SearchResource[] {
    let currentKey = null;
    for (let i = 0; i < searches.length; i++) {
      const item: SearchResource = searches[i];
      if (item.content.after !== currentKey) {
        item.content.after = currentKey;
        this.updateSearch(item);
      }
      currentKey = item.key;
    }
    return searches;
  }
}
