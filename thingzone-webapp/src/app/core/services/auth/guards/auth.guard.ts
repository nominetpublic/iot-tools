/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { SessionService } from '../../session/session.service';
import { AuthService } from '../auth.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private session: SessionService,
    private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const url: string = state.url;
    const authTest = this.authService.hardLoginCheck();
    return this.handleAuthStatus(authTest, url);
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const url: string = state.url;
    const authTest = this.authService.isLoggedIn();

    return this.handleAuthStatus(authTest, url);
  }

  handleAuthStatus(authTest, url) {
    return authTest.pipe(
      map(status => {
        if (status) return true;

        return this.authFail(url);
      }));
  }

  authFail(url): boolean {
    // Store the attempted URL for redirecting
    this.session.setRedirect(url);

    // Navigate to the login page with extras
    this.router.navigate(['/login']);

    return false;
  }
}
