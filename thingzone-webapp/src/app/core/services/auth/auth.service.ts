/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { catchError, flatMap, map } from 'rxjs/operators';

import { ApiService } from '../api/api.service';
import { SessionService } from '../session/session.service';
import { Privilege } from './privilege';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private api: ApiService,
    private session: SessionService) { }

  isLoggedIn(): Observable<boolean> {
    return of(this.session.getUser() != null && this.session.getSessionKey() != null);
  }

  hardLoginCheck(): Observable<boolean> {
    if (this.isLoggedIn() && this.session.getSessionKey()) {
      return this.api.sessionKeepAlive().pipe(
        map((res: any) => {
          if (res.result === 'ok') {
            return true;
          } else {
            return false;
          }
        }),
        catchError(err => {
          return of(false);
        }));
    }
    return of(false);
  }

  login(username, password): Observable<string> {
    return this.api.createSessionKey(username, password).pipe(
      map((res: any) => {
        const loginObj = {
          username: username,
          sessionKey: res.sessionKey,
          details: res.user.details,
          preferences: res.user.preferences,
        };
        this.session.login(loginObj);
        return this.session.getRedirectURL();
      }),
      catchError((err: HttpErrorResponse) => {
        let message = err.message;
        if (err.status === 401) message = 'Username or Password Incorrect';
        return throwError(message);
      }));
  }

  loginComplete() {
    this.session.setRedirect('/');
  }

  logout(): void {
    this.session.logout();
  }

  private getUser(): Observable<any> {
    const url = this.api.getRegistryServer() + 'api/user/get';
    const options = {
      headers: this.api.getRegistryHeaders(),
    };
    return this.api.get(url, options).pipe(map((succ: any) => succ.user));
  }

  resetPassword(username: string, token: string, newPassword: string): Observable<any> {
    const url = this.api.getRegistryServer() + 'api/user/set_password';
    const options = {
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    };
    const body = new HttpParams()
      .set('username', username)
      .set('token', token)
      .set('newPassword', newPassword)
      .toString();
    return this.api.put(url, body, options);
  }

  checkUserRole(role: Privilege): Observable<boolean> {
    const currentPrivileges = this.session.getPrivileges();
    if (currentPrivileges == null) {
      return this.getUser().pipe(
        flatMap(user => {
          const privileges: string[] = user.privileges;
          this.session.setPrivileges(privileges);
          return this.checkPrivileges(role, privileges);
        }),
        catchError(error => of(false)));
    } else {
      return this.checkPrivileges(role, currentPrivileges);
    }
  }

  private checkPrivileges(role: Privilege, privileges: string[]): Observable<boolean> {
    if (privileges.includes(role)) {
      return of(true);
    } else {
      return of(false);
    }
  }

}
