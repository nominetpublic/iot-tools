/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export enum Privilege {
  USER_ADMIN = 'USER_ADMIN',
  CREATE_USER = 'CREATE_USER',
  CREATE_GROUP = 'CREATE_GROUP',
  CREATE_ENTITY = 'CREATE_ENTITY',
  CREATE_DEVICE = 'CREATE_DEVICE',
  CREATE_DATASTREAM = 'CREATE_DATASTREAM',
  CREATE_TRANSFORM = 'CREATE_TRANSFORM',
  CREATE_TRANSFORMFUNCTION = 'CREATE_TRANSFORMFUNCTION',
  CREATE_RESOURCE = 'CREATE_RESOURCE',
  CREATE_DETECTOR = 'CREATE_DETECTOR',
  RUN_DETECTOR = 'RUN_DETECTOR',
  DUMP_ALL = 'DUMP_ALL',
  USE_SESSION = 'USE_SESSION',
  QUERY_STREAMS = 'QUERY_STREAMS',
}
