/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { TransformModel } from 'app/devices/domain-models/transform-model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ApiService } from './api.service';

/**
 * The model for a transform function
 */
export interface TransformFunction {
  name: string;
  updatedName?: string;
  functionType: string;
  functionContent: string;
}

/**
 * Service used for transform configuration queries
 */
@Injectable({
  providedIn: 'root'
})
export class TransformApiService {

  constructor(
    private api: ApiService,
  ) { }

  listTranformFunctions(showDetail = false): Observable<any> {
    const url = this.api.getRegistryServer() + 'api/transform/list_functions';
    const options = { headers: this.api.getRegistryHeaders() };
    if (showDetail) {
      options['params'] = { detail: 'full' };
    }
    return this.api.get(url, options).pipe(map(res => res.transformFunctions));
  }

  getTransformFunction(name: string): Observable<TransformFunction> {
    const url = this.api.getRegistryServer() + 'api/transform/get_function';
    const options = {
      headers: this.api.getRegistryHeaders(),
      params: { name: name },
    };
    return this.api.get(url, options).pipe(map(res => res.function));
  }

  updateTransformFunction(transform: TransformFunction): Observable<any> {
    const url = this.api.getRegistryServer() + 'api/transform/modify_function';
    const options = {
      headers: this.api.getRegistryUpsertHeaders(),
      'Content-Type': 'multipart/form-data'
    };
    const formData = new FormData();
    formData.append('name', transform.name);
    if (transform.functionType != null) formData.append('functionType', transform.functionType);
    if (transform.functionContent != null) formData.append('functionContent', transform.functionContent);
    if (transform.updatedName != null) formData.append('updatedName', transform.updatedName);

    return this.api.put(url, formData, options);
  }

  createTransformFunction(transform: TransformFunction): Observable<any> {
    const url = this.api.getRegistryServer() + 'api/transform/create_function';
    const options = {
      headers: this.api.getRegistryUpsertHeaders(),
      'Content-Type': 'multipart/form-data'
    };
    const formData = new FormData();
    formData.append('name', transform.name);
    formData.append('functionType', transform.functionType);
    formData.append('functionContent', transform.functionContent);

    return this.api.put(url, formData, options);
  }

  deleteTransformFunction(name: string): Observable<any> {
    const url = this.api.getRegistryServer() + 'api/transform/delete_function';
    const options = {
      headers: this.api.getRegistryHeaders(),
      params: { name: name },
    };
    return this.api.delete(url, options);
  }

  upsertTransform(transformKey: string, transform: TransformModel, devices: String[] = []): Observable<any> {
    const options = { headers: this.api.getRegistryUpsertHeaders() };
    const url = this.api.getRegistryServer() + 'api/transform/upsert';
    const formData = new FormData();

    Object.keys(transform).forEach((key: string) => {
      let data = null;
      if (typeof transform[key] === 'string' || typeof transform[key] === 'number' || typeof transform[key] === 'boolean') {
        data = transform[key];
      } else {
        data = new Blob([JSON.stringify(transform[key])], { type: 'application/json' });
      }
      formData.append(key, data);
    });
    if (devices != null && devices.length > 0) {
      const deviceLinks = devices.map((deviceKey, index) => ({ key: deviceKey, linkName: 'stream' + index }));
      const devicesList = new Blob([JSON.stringify(deviceLinks)], { type: 'application/json' });
      formData.append('devices', devicesList);
    }
    formData.append('transform', transformKey);
    return this.api.put(url, formData, options).pipe(map(res => res.transform));
  }

  getTransform(key: string): Observable<object> {
    const url = this.api.getRegistryServer() + 'api/transform/get';
    const options = {
      headers: this.api.getRegistryHeaders(),
      params: {
        transform: key,
        detail: 'full',
        showPermissions: true
      },
    };
    return this.api.get(url, options).pipe(map(res => res.transform));
  }

  deleteTransform(key: string): Observable<object> {
    const url = this.api.getRegistryServer() + 'api/transform/delete';
    const options = {
      headers: this.api.getRegistryHeaders(),
      params: { transform: key },
    };
    return this.api.delete(url, options);
  }
}
