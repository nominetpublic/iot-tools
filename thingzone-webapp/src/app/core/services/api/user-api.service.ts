/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserModel } from 'app/core/models/auth/user-model';
import { Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';

import { ApiService } from './api.service';

/**
 * Service for the user and groups admin queries
 */
@Injectable({
  providedIn: 'root'
})
export class UserApiService {

  constructor(private api: ApiService) {
  }

  updatePassword(existingPassword: string, newPassword: string): Observable<any> {
    const url = this.api.getRegistryServer() + `api/user/change_password`;

    const options = { headers: this.api.getRegistryHeaders() };
    const body = new HttpParams()
      .set('existingPassword', existingPassword)
      .set('newPassword', newPassword)
      .toString();
    return this.api.put(url, body, options);
  }

  listUsers(): Observable<string[]> {
    const url = this.api.getRegistryServer() + `api/user/list_names`;

    const options = { headers: this.api.getRegistryHeaders() };
    return this.api.get(url, options)
      .pipe(map(res => res.users));
  }

  createUser(user: UserModel): Observable<any> {
    const url = this.api.getRegistryServer() + `api/user/create`;
    const options = { headers: this.api.getRegistryHeaders() };

    let params = new HttpParams();
    Object.keys(user).forEach((key: string) => {
      if (user[key] != null) {
        params = params.append(key, user[key]);
      }
    });
    const body = params.toString();
    return this.api.post(url, body, options);
  }

  getOtherUser(username: string): Observable<any> {
    const url = this.api.getRegistryServer() + 'api/user/get';
    const params = new HttpParams()
      .set('username', username);
    const options = {
      headers: this.api.getRegistryHeaders(),
      params: params
    };
    return this.api.get(url, options)
      .pipe(map(res => res.user));
  }

  setUserPrivileges(user: UserModel): Observable<any> {
    const url = this.api.getRegistryServer() + `api/user/set_privileges`;
    const options = {
      headers: this.api.getRegistryHeaders(),
    };
    const privileges = Array.isArray(user.privileges) ? user.privileges.join(',') : user.privileges;
    const params = new HttpParams()
      .set('username', user.username)
      .set('privileges', privileges);
    return this.api.put(url, params.toString(), options);
  }

  updateUser(currentUsername: string, newUsername: string, preferences?: object, details?: object): Observable<any> {
    const url = this.api.getRegistryServer() + `api/user/update`;

    const options = { headers: this.api.getRegistryUpsertHeaders() };
    const body = new FormData();
    body.append('username', currentUsername);
    body.append('updatedName', newUsername);
    if (preferences != null) {
      body.append('preferences', new Blob([JSON.stringify(preferences)], { type: 'application/json' }));
    }
    if (details != null) {
      body.append('details', new Blob([JSON.stringify(details)], { type: 'application/json' }));
    }
    return this.api.put(url, body, options);
  }

  requestPasswordToken(username: string): Observable<any> {
    const url = this.api.getRegistryServer() + 'api/user/request_token';
    const params = new HttpParams()
      .set('username', username);
    const options = {
      headers: this.api.getRegistryHeaders(),
      params: params
    };
    return this.api.get(url, options);
  }

  listGroups(owned = false, all = false): Observable<any> {
    const url = this.api.getRegistryServer() + `api/groups`;
    const params = new HttpParams()
      .set('owned', owned.toString())
      .set('all', all.toString());

    const options = {
      headers: this.api.getRegistryHeaders(),
      params: params
    };
    return this.api.get(url, options)
      .pipe(map(res => res.groups));
  }

  createGroup(name: string): Observable<any> {
    const url = this.api.getRegistryServer() + `api/groups/${name}`;

    const options = { headers: this.api.getRegistryHeaders() };
    return this.api.post(url, {}, options);
  }

  updateGroup(name: string, updatedName: string): Observable<any> {
    const url = this.api.getRegistryServer() + `api/groups/${name}/update`;

    const options = { headers: this.api.getRegistryHeaders() };
    const body = new HttpParams()
      .set('updatedName', updatedName)
      .toString();
    return this.api.put(url, body, options);
  }

  listGroupUsers(name: string): Observable<{ username: string, admin: boolean }[]> {
    const url = this.api.getRegistryServer() + `api/groups/${name}/users`;
    const options = {
      headers: this.api.getRegistryHeaders(),
    };
    return this.api.get(url, options).pipe(map(res => res.users));
  }

  addGroupUsers(name: string, users: string[]): Observable<any> {
    const url = this.api.getRegistryServer() + `api/groups/${name}/users/add`;
    const options = {
      headers: this.api.getRegistryHeaders(),
    };
    const body = new HttpParams()
      .set('usernames', users.join(','))
      .toString();
    return this.api.put(url, body, options);
  }

  removeGroupUsers(name: string, users: string[]): Observable<any> {
    const url = this.api.getRegistryServer() + `api/groups/${name}/users/remove`;
    const options = {
      headers: this.api.getRegistryHeaders(),
    };
    const body = new HttpParams()
      .set('usernames', users.join(','))
      .toString();
    return this.api.put(url, body, options);
  }

  groupUserAdmin(name: string, user: string, admin: boolean): Observable<any> {
    const url = this.api.getRegistryServer() + `api/groups/${name}/admin`;
    const options = {
      headers: this.api.getRegistryHeaders(),
    };
    const body = new HttpParams()
      .set('username', user)
      .set('admin', admin.toString())
      .toString();
    return this.api.put(url, body, options);
  }

  listAdminEntities(name: string, type: string, showPermissions: boolean,
    username: string, groupname: string, size: number, page: number): Observable<any> {
    const url = this.api.getRegistryServer() + `api/entity/list_admin`;
    let params = new HttpParams().set('showPermissions', showPermissions.toString());
    if (type != null && type !== '') {
      params = params.append('type', type);
    }
    if (name != null && name !== '') {
      params = params.append('name', name);
    }
    if (username != null && username !== '') {
      params = params.append('username', username);
    }
    if (groupname != null && groupname !== '') {
      params = params.append('groupname', groupname);
    }
    if (size != null) {
      params = params.append('size', size.toString());
    } else {
      params = params.append('size', '10');
    }
    if (page != null) {
      params = params.append('page', page.toString());
    } else {
      params = params.append('page', '0');
    }
    const options = {
      headers: this.api.getRegistryHeaders(),
      params: params
    };

    return this.api.get(url, options).pipe(
      map(res => res.entities)
    );
  }

  updatePermission(keys: string[], permissions: string[], username: string, group: string, remove = false): Observable<any> {
    const url = this.api.getRegistryServer() + `api/permission/set_keys`;
    const options = {
      headers: this.api.getRegistryHeaders(),
    };
    let params = new HttpParams()
      .set('keys', keys.join(','))
      .set('permissions', permissions.join(','))
      .set('remove', remove.toString());
    if (username != null) {
      params = params.append('username', username);
    } else if (group != null) {
      params = params.append('group', group);
    } else {
      return throwError(new Error('no username or group name specified to set_keys'));
    }
    return this.api.put(url, params.toString(), options);
  }
}
