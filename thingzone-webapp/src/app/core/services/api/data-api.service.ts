/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { SearchTerm } from 'app/core/models/common/search-term';
import { DataQueryType } from 'app/core/models/queries/data-query';
import { QuerySource } from 'app/core/models/visualisations/display-data-source';
import { SecondaryQuery } from 'app/core/models/visualisations/secondary-query';
import { ApiService } from 'app/core/services/api/api.service';
import { EventApiService } from 'app/core/services/api/event-api.service';
import { DateUtils } from 'app/core/utils/date-utils';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { DateRange } from '../visualisation/display-context.service';

/**
 * Service for API requests related to displaying visualisations and widgets
 */
@Injectable({
  providedIn: 'root'
})
export class DataApiService {
  allMetricFields: string[] = [
    'count',
    'min',
    'max',
    'avg',
    'sum',
    'sum_of_squares',
    'variance',
    'std_deviation'
  ];

  constructor(
    private api: ApiService,
    private eventApi: EventApiService) {
  }

  getMetricsAggregation(dataSource: QuerySource, dates: DateRange = this.api.defaultDates()): Observable<any> {
    const url = this.api.getDataServer() + 'api/v1/metrics/query/agg';

    const body = {};
    this.setGenericParameters(body, dataSource);
    this.setQuerySpecificParameters(body, dataSource, dates);

    const options = { headers: this.api.getDataHeaders() };
    return this.api.post(url, body, options).pipe(map((succ: any) => succ.response));
  }

  getMetricsList(dataSource: QuerySource, dates = this.api.defaultDates()): Observable<any> {
    const url = this.api.getDataServer() + 'api/v1/metrics/query/list';

    const body = {};
    this.setGenericParameters(body, dataSource);
    this.setQuerySpecificParameters(body, dataSource, dates);

    const options = { headers: this.api.getDataHeaders() };
    return this.api.post(url, body, options).pipe(map((succ: any) => succ.response));
  }

  getMetricsGeo(dataSource: QuerySource, dates: DateRange, boundingPolygon?): Observable<any> {
    const url = this.api.getDataServer() + 'api/v1/metrics/query/geo';

    const body = {};
    this.setGenericParameters(body, dataSource);
    this.setQuerySpecificParameters(body, dataSource, dates, boundingPolygon);

    const options = { headers: this.api.getDataHeaders() };
    return this.api.post(url, body, options).pipe(map((succ: any) => succ.response));
  }

  getCombinationGeo(dataSource: QuerySource, dates: DateRange, boundingPolygon?): Observable<any> {
    const url = this.api.getDataServer() + 'api/v1/combination/query/geo';

    const body = {};
    this.setGenericParameters(body, dataSource);
    const geoMetricsQuery = this.getGeoMetricQuery(dataSource, dates, boundingPolygon);
    const secondaryQueries = this.getSecondaryQueries(dataSource, dates);
    body['combinationGeoQuery'] = {
      primary: {
        metricsGeoQuery: geoMetricsQuery,
      },
      secondary: secondaryQueries
    };
    body['streamListQuery']['secondaryNames'] = ['Velocity'];

    const options = { headers: this.api.getDataHeaders() };
    return this.api.post(url, body, options).pipe(map((succ: any) => succ.response));
  }

  getImages(dataSource: QuerySource, dates: DateRange): Observable<any> {
    const url = this.api.getDataServer() + 'api/v1/images/query/list';

    const body = {};
    this.setGenericParameters(body, dataSource);
    this.setQuerySpecificParameters(body, dataSource, dates);

    const options = { headers: this.api.getDataHeaders() };
    return this.api.post(url, body, options).pipe(map((succ: any) => succ.response));
  }

  getEventsGeo(dataSource: QuerySource, dates: DateRange, boundingPolygon?): Observable<any> {
    const url = this.api.getDataServer() + 'api/v1/event/query/geo';

    const body = {};
    this.setGenericParameters(body, dataSource);
    this.setQuerySpecificParameters(body, dataSource, dates, boundingPolygon);

    const options = { headers: this.api.getDataHeaders() };
    return this.api.post(url, body, options).pipe(map((succ: any) => succ.response));
  }

  getEvents(dataSource: QuerySource, dates: DateRange): Observable<any> {
    const url = this.api.getDataServer() + 'api/v1/event/query/list';

    const body = {};
    this.setGenericParameters(body, dataSource);
    this.setQuerySpecificParameters(body, dataSource, dates);

    const options = { headers: this.api.getDataHeaders() };
    return this.api.post(url, body, options).pipe(map((succ: any) => succ.response));
  }

  getStreamNames(dataSource: QuerySource, types: String[] = []): Observable<any> {
    const url = this.api.getDataServer() + 'api/v1/stream/query/common';

    const deviceQuery = this.getDeviceQuery(dataSource);
    const commonStreamsQuery = {};
    if (types.length > 0) {
      commonStreamsQuery['types'] = types;
    }
    const body = {
      deviceQuery: deviceQuery,
      commonStreamsQuery: commonStreamsQuery
    };
    const options = { headers: this.api.getDataHeaders() };

    return this.api.post(url, body, options).pipe(map(res => res.response));
  }

  setGenericParameters(parameters: object, source: QuerySource): void {
    parameters['deviceQuery'] = this.getDeviceQuery(source);
    parameters['streamListQuery'] = this.getDatastreamQuery(source);
  }

  setQuerySpecificParameters(parameters, source: QuerySource, dates?: DateRange, boundingPolygon?): void {
    switch (source.query) {
      case DataQueryType.AGGREGATION:
        parameters['metricsAggQuery'] = this.getMetricAggQuery(source, dates);
        break;
      case DataQueryType.METRICS:
        parameters['metricsListQuery'] = this.getRawMetricQuery(source, dates);
        break;
      case DataQueryType.LOCATION:
        parameters['metricsGeoQuery'] = this.getGeoMetricQuery(source, dates, boundingPolygon);
        break;
      case DataQueryType.EVENTS:
        parameters['eventListQuery'] = this.getEventQuery(source, dates);
        break;
      case DataQueryType.EVENTLOCATION:
        parameters['eventGeoQuery'] = this.getEventQuery(source, dates, boundingPolygon);
        break;
      case DataQueryType.IMAGES:
        parameters['imagesListQuery'] = this.getImageQuery(source, dates);
        break;
    }
  }

  setSecondaryQuerySpecificParameters(parameters, source: QuerySource, secondary: SecondaryQuery): void {
    switch (secondary.query) {
      case DataQueryType.METRICS:
        parameters['metricsListQuery'] = {};
        break;
      case DataQueryType.EVENTS:
        parameters['eventListQuery'] = this.getEventQuery(source);
        break;
    }
  }

  getMetricAggQuery(source: QuerySource, dates: DateRange): object {
    let filterFields = this.allMetricFields;
    let interval = 3600;
    let groupBy = false;

    const aggConfig = source.configuration.aggregationQuery;
    if (aggConfig != null) {
      if (aggConfig.math) {
        filterFields = [aggConfig.math];
      }
      if (aggConfig.interval) {
        // convert to seconds
        const now = moment();
        const then = DateUtils.calcTimeDelta(now, aggConfig.interval);
        interval = now.diff(then, 'seconds');
      }
      if (aggConfig.groupByStream) {
        groupBy = aggConfig.groupByStream;
      }
    }

    return {
      'timeLimit': this.api.dateMathDifference(dates),
      'time': dates.to.toISOString(),
      'metricType': 'value_int',
      'interval': interval,
      'fields': filterFields,
      'groupByStream': groupBy,
    };
  }

  getRawMetricQuery(source, dates: DateRange, size = 50): object {
    const metricQuery = {
      'timeLimit': this.api.dateMathDifference(dates),
      'time': dates.to.toISOString(),
      'size': size
    };
    const locationConfig = source.configuration.locationQuery;
    if (locationConfig != null) {
      if (locationConfig.singlePoint != null && locationConfig.singlePoint === true) {
        metricQuery['size'] = 1;
      } else if (locationConfig.sizeFromCurrent != null) {
        metricQuery['size'] = locationConfig.sizeFromCurrent;
      }
    }
    return metricQuery;
  }

  getGeoMetricQuery(source: QuerySource, dates: DateRange, polygons = []): object {
    const geoMetricQuery = {
      'time': dates.to.toISOString(),
      'geoOutput': 'points',
    };
    if (polygons.length > 0) {
      geoMetricQuery['boundingPolygons'] = polygons;
    }

    const locationConfig = source.configuration.locationQuery;
    if (locationConfig != null) {
      if (locationConfig.forward != null && locationConfig.forward) {
        geoMetricQuery['time'] = dates.from.toISOString();
        geoMetricQuery['forward'] = true;
      }
      if (locationConfig.sizeFromCurrent != null) {
        geoMetricQuery['size'] = locationConfig.sizeFromCurrent;
      }
      if (locationConfig.showAsLine != null && locationConfig.showAsLine === true) {
        geoMetricQuery['geoOutput'] = 'line';
      }
      if (locationConfig.timeFromCurrent != null && locationConfig.timeFromCurrent) {
        let time: moment.Moment;
        if (locationConfig.forward) {
          time = DateUtils.calcTimeDeltaForwards(moment(dates.from), locationConfig.timeFromCurrent);
        } else {
          time = DateUtils.calcTimeDelta(moment(dates.to), locationConfig.timeFromCurrent);
        }
        // do not return results outside of time range
        if (!locationConfig.forward && time.isBefore(dates.from)) {
          geoMetricQuery['timeLimit'] = this.api.dateMathDifference(dates);
        } else if (locationConfig.forward && time.isAfter(dates.to)) {
          geoMetricQuery['timeLimit'] = this.api.dateMathDifference(dates);
        } else {
          geoMetricQuery['timeLimit'] = locationConfig.timeFromCurrent;
        }
      }
    }
    if (!('timeLimit' in geoMetricQuery)) {
      geoMetricQuery['timeLimit'] = this.api.dateMathDifference(dates);
    }
    return geoMetricQuery;
  }

  getImageQuery(source: QuerySource, dates?: DateRange): object {
    const imageQuery = {
      'burstInterval': 200,
    };
    if (dates != null) {
      imageQuery['timeLimit'] = this.api.dateMathDifference(dates);
      imageQuery['time'] = dates.to.toISOString();
    }

    const imageConfig = source.configuration.imageQuery;
    if (imageConfig != null) {
      if (imageConfig.size != null) {
        imageQuery['size'] = imageConfig.size;
      }
      if (imageConfig.burstInterval != null) {
        imageQuery['burstInterval'] = imageConfig.burstInterval;
      }
      if (imageConfig.tags != null) {
        imageQuery['tags'] = imageConfig.tags;
      }
      if (imageConfig.groupBy != null) {
        imageQuery['groupBy'] = imageConfig.groupBy;
      }
    }
    return imageQuery;
  }

  getEventQuery(source: QuerySource, dates?: DateRange, polygons = []): object {
    const queryConfig = source.configuration.eventQuery;
    let terms = [];
    const eventQuery = {};
    if (queryConfig != null && queryConfig.eventFilter != null && queryConfig.eventFilter.terms.length > 0) {
      terms = queryConfig.eventFilter.terms;
    }

    if (dates == null) { // secondary query
      this.eventApi.eventQuery(eventQuery, terms, null, null);
    } else {
      this.eventApi.eventQuery(eventQuery, terms, dates, null);
    }
    if (queryConfig != null) {
      if (queryConfig.size != null) {
        eventQuery['size'] = queryConfig.size;
      }
      if (queryConfig.timeLimit != null && queryConfig.timeLimit) {
        let time: moment.Moment;
        if (queryConfig.forward) {
          time = DateUtils.calcTimeDeltaForwards(moment(dates.from), queryConfig.timeLimit);
        } else {
          time = DateUtils.calcTimeDelta(moment(dates.to), queryConfig.timeLimit);
        }
        // do not return results outside of time range
        if (!queryConfig.forward && time.isBefore(dates.from)) {
          eventQuery['timeLimit'] = this.api.dateMathDifference(dates);
        } else if (queryConfig.forward && time.isAfter(dates.to)) {
          eventQuery['timeLimit'] = this.api.dateMathDifference(dates);
        } else {
          eventQuery['timeLimit'] = queryConfig.timeLimit;
        }
      }
      if (queryConfig.forward != null && queryConfig.forward === true) {
        eventQuery['forward'] = true;
        eventQuery['time'] = dates.from.toISOString();
      }
    }
    if (polygons.length > 0) {
      eventQuery['boundingPolygons'] = polygons;
    }
    if (!('timeLimit' in eventQuery)) {
      eventQuery['timeLimit'] = this.api.dateMathDifference(dates);
    }
    return eventQuery;
  }

  getSecondaryQueries(dataSource: QuerySource, dates?: DateRange | string): any[] {
    if (dataSource.secondaryQueries == null || dataSource.secondaryQueries.length === 0) {
      return [];
    }
    const secondaryQueries = [];
    for (const secondary of dataSource.secondaryQueries) {
      const body = {};
      body['streamName'] = secondary.streamName;
      this.setSecondaryQuerySpecificParameters(body, dataSource, secondary);
      secondaryQueries.push(body);
    }
    return secondaryQueries;
  }

  getDatastreamQuery(source: QuerySource): object {
    const dataStreamQuery = {};
    if (source.streamFilter != null) {
      dataStreamQuery['name'] = source.streamFilter;
    }
    return dataStreamQuery;
  }

  getDeviceQuery(source: QuerySource, bounds?): object {
    let searchTerms: SearchTerm[] = [];
    if (source.deviceFilter) {
      searchTerms = source.deviceFilter.terms;
    }
    const deviceQuery = {};
    searchTerms.forEach(term => {
      if (term.property === 'type') {
        deviceQuery['types'] = term.values;
      } else if (term.property === 'keywords') {
        deviceQuery['keywords'] = term.values;
      }
    });
    if (bounds != null && bounds.length > 0) {
      deviceQuery['boundingPolygons'] = bounds;
    }
    if (source.streamFilter != null) {
      deviceQuery['streamName'] = source.streamFilter;
    }
    return deviceQuery;
  }

  getGraphData(aggregationData, aggregationType = 'avg', dataSourceName): any[] {
    const seriesData = [];
    aggregationData.forEach(stream => {
      const mappedData = this.reduceAggregationData(stream.values, aggregationType);
      if (mappedData.length > 0) {
        if (stream.merged) {
          seriesData.push({ name: dataSourceName, series: mappedData });
        } else {
          seriesData.push({ name: stream.streamKey, series: mappedData });
        }
      }
    });
    return seriesData;
  }

  private reduceAggregationData(values, aggregationType): any[] {
    return values.reduce((acc, val) => {
      if (val[aggregationType] != null) {
        acc.push({
          name: new Date(val.timestamp),
          value: val[aggregationType],
        });
      }
      return acc;
    }, []);
  }
}
