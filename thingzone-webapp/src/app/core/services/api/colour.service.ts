/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';

/**
 * Service to support the colour picker
 */
@Injectable({
  providedIn: 'root'
})
export class ColourService {

  colourArray = ['#024bfd', '#94fd02', '#fd02de', '#02fdd3', '#fd8a02', '#4102fd', '#0dfd02', '#fd0256', '#029ffd', '#e8fd02', '#c802fd', '#02fd7f', '#fd3602', '#0217fd', '#61fd02', '#fd02aa', '#02f3fd', '#fdbe02', '#7402fd', '#02fd2b', '#fd0222', '#026bfd', '#b5fd02', '#fc02fd', '#02fdb3', '#fd6a02'];

  constructor() { }

  rgbToHex(r, g, b): string {
    r = r.toString(16);
    g = g.toString(16);
    b = b.toString(16);

    if (r.length === 1) {
      r = '0' + r;
    }
    if (g.length === 1) {
      g = '0' + g;
    }
    if (b.length === 1) {
      b = '0' + b;
    }

    return '#' + r + g + b;
  }

  rgbStringToHex(rgb: string) {
    // Choose correct separator
    const sep = rgb.indexOf(',') > -1 ? ',' : ' ';
    // Turn "rgb(r,g,b)" into [r,g,b]
    const rgbSplit = rgb.substr(4).split(')')[0].split(sep);

    let r = (+rgbSplit[0]).toString(16),
      g = (+rgbSplit[1]).toString(16),
      b = (+rgbSplit[2]).toString(16);

    if (r.length === 1) {
      r = '0' + r;
    }
    if (g.length === 1) {
      g = '0' + g;
    }
    if (b.length === 1) {
      b = '0' + b;
    }

    return '#' + r + g + b;
  }

  hexToRgb(h: string): string {
    let r = 0, g = 0, b = 0;

    // 3 digits
    if (h.length === 4) {
      r = parseInt(h[1] + h[1], 16);
      g = parseInt(h[2] + h[2], 16);
      b = parseInt(h[3] + h[3], 16);

      // 6 digits
    } else if (h.length === 7) {
      r = parseInt(h.slice(1, 3), 16);
      g = parseInt(h.slice(3, 5), 16);
      b = parseInt(h.slice(5, 7), 16);
    }

    return 'rgb(' + r + ',' + g + ',' + b + ')';
  }

  hexToRgba(h: string, transparency: number): string {
    let r = 0, g = 0, b = 0;

    // 3 digits
    if (h.length === 4) {
      r = parseInt(h[1] + h[1], 16);
      g = parseInt(h[2] + h[2], 16);
      b = parseInt(h[3] + h[3], 16);

      // 6 digits
    } else if (h.length === 7) {
      r = parseInt(h.slice(1, 3), 16);
      g = parseInt(h.slice(3, 5), 16);
      b = parseInt(h.slice(5, 7), 16);
    }

    return 'rgba(' + r + ',' + g + ',' + b + ',' + transparency + ')';
  }

  hexToRgbArray(h: string): number[] {
    if (h == null || h === '') {
      return [255, 0, 0];
    }
    let r = 0, g = 0, b = 0;

    // 3 digits
    if (h.length === 4) {
      r = parseInt(h[1] + h[1], 16);
      g = parseInt(h[2] + h[2], 16);
      b = parseInt(h[3] + h[3], 16);

      // 6 digits
    } else if (h.length === 7) {
      r = parseInt(h.slice(1, 3), 16);
      g = parseInt(h.slice(3, 5), 16);
      b = parseInt(h.slice(5, 7), 16);
    }

    return [r, g, b];
  }

  rgbToHsl(r, g, b): number[] {
    // Make r, g, and b fractions of 1
    r /= 255, g /= 255, b /= 255;

    // Find greatest and smallest channel values
    const cmin = Math.min(r, g, b);
    const cmax = Math.max(r, g, b);
    const delta = cmax - cmin;
    let h, s, l = 0;

    // Calculate hue
    // No difference
    if (delta === 0) {
      h = 0;
    } else if (cmax === r) {
      h = ((g - b) / delta) % 6;
    } else if (cmax === g) {
      h = (b - r) / delta + 2;
    } else {
      h = (r - g) / delta + 4;
    }

    h = Math.round(h * 60);

    // Make negative hues positive behind 360°
    if (h < 0) {
      h += 360;
    }

    // Calculate lightness
    l = (cmax + cmin) / 2;

    // Calculate saturation
    s = delta === 0 ? 0 : delta / (1 - Math.abs(2 * l - 1));

    // Multiply l and s by 100
    s = +(s * 100).toFixed(1);
    l = +(l * 100).toFixed(1);

    return [h, s, l];
  }

  hslFromHex(hexColour: string): number[] {
    const rgb: number[] = this.hexToRgbArray(hexColour);
    const hsl: number[] = this.rgbToHsl(rgb[0], rgb[1], rgb[2]);
    return hsl;
  }

  rgbToHsv(r: number, g: number, b: number) {
    r /= 255, g /= 255, b /= 255;

    const max = Math.max(r, g, b);
    const min = Math.min(r, g, b);
    let h, s = max;
    const v = max;

    const delta = max - min;
    s = max === 0 ? 0 : delta / max;

    if (max === min) {
      h = 0;
    } else {
      switch (max) {
        case r: h = (g - b) / delta + (g < b ? 6 : 0); break;
        case g: h = (b - r) / delta + 2; break;
        case b: h = (r - g) / delta + 4; break;
      }
      h /= 6;
    }
    return [h, s, v];
  }

  getColours(size: number): string[] {
    while (size > this.colourArray.length) {
      this.colourArray = this.colourArray.concat(this.colourArray);
    }
    const start = Math.floor(Math.random() * this.colourArray.length);
    const doubleArray = this.colourArray.concat(this.colourArray);
    return doubleArray.slice(start, start + size);
  }

  getNext(colour?: string): string {
    const index = colour ? this.colourArray.findIndex(c => c === colour) : -1;
    switch (index) {
      case -1:
        const i = Math.floor(Math.random() * this.colourArray.length);
        return this.colourArray[i];
      case this.colourArray.length - 1:
        return this.colourArray[0];
      default:
        return this.colourArray[index + 1];
    }
  }

}
