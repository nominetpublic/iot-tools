/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SearchTerm } from 'app/core/models/common/search-term';
import { ApiService } from 'app/core/services/api/api.service';
import { DeviceModel } from 'app/devices/domain-models/device-model';
import { StreamModel } from 'app/devices/domain-models/stream-model';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { DateRange } from '../visualisation/display-context.service';

/**
 * Service for device and stream configuration
 */
@Injectable({
  providedIn: 'root'
})
export class DeviceApiService {

  constructor(private api: ApiService) {
  }

  deviceList(searchTerms?: SearchTerm[], from = 0, size = 1000, streamName?: string): Observable<any> {
    const url = this.api.getDataServer() + 'api/v1/device/search/list';
    const body = {
      'from': from,
      'size': size
    };

    searchTerms.forEach(term => {
      if (term.property === 'type') {
        body['types'] = term.values;
      } else if (term.property === 'keywords') {
        body['keywords'] = term.values;
      }
    });
    if (streamName != null) {
      body['streamName'] = streamName;
    }

    const options = { headers: this.api.getDataHeaders() };
    return this.api.post(url, body, options).pipe(map(res => res.response));
  }

  deviceLocation(searchTerms?: SearchTerm[], bounds?: any[], streamName?: string, time?: Date): Observable<any> {
    const url = this.api.getDataServer() + 'api/v1/device/search/geo';
    const body = {};

    searchTerms.forEach(term => {
      if (term.property === 'type') {
        body['types'] = term.values;
      } else if (term.property === 'keywords') {
        body['keywords'] = term.values;
      }
    });
    if (bounds != null && bounds.length > 0) {
      body['boundingPolygons'] = bounds;
    }
    if (streamName != null) {
      body['streamName'] = streamName;
    }
    if (time != null) {
      body['time'] = time.toISOString();
    }

    const options = { headers: this.api.getDataHeaders() };
    return this.api.post(url, body, options).pipe(map(res => res.response));
  }

  deviceFacetSearch(): Observable<any> {
    const url = this.api.getDataServer() + 'api/v1/device/facets';
    const options = { headers: this.api.getDataHeaders() };
    return this.api.get(url, options).pipe(map((succ: any) => succ.response));
  }

  getDeviceStreams(key): Observable<any> {
    const url = this.api.getDataServer() + 'api/v1/stream/list';
    const body = {
      devices: [key],
    };
    const options = { headers: this.api.getDataHeaders() };
    return this.api.post(url, body, options).pipe(map((succ: any) => succ.response));
  }

  getDevice(deviceKey): Observable<any> {
    const url = this.api.getRegistryServer() + 'api/device/get';
    const options = {
      headers: this.api.getRegistryHeaders(),
      params: { device: deviceKey, detail: 'full', showPermissions: true }
    };
    return this.api.get(url, options).pipe(map((succ: any) => succ.device));
  }

  findEntity(key): Observable<any> {
    const url = this.api.getRegistryServer() + `api/search/identify`;
    const options = {
      headers: this.api.getRegistryHeaders(),
      params: { key: key }
    };
    return this.api.get(url, options);
  }

  validateKey(value: string): Observable<any> {
    const pattern = /^(?!-)[a-z0-9-.]{0,63}$/g;
    if (!pattern.test(value)) {
      return throwError({ show: true, message: 'Invalid key' });
    } else if (value == null) {
      return throwError({ show: true, message: 'Key is required' });
    }
    return this.findEntity(value).pipe(
      map(res => {
        return { error: true, show: true, message: 'Duplicate key found' };
      }),
      catchError(err => {
        if (err.status === 404) {
          return of(true);
        } else if (err.status === 403) {
          return throwError({ error: true, show: true, message: 'Duplicate key found' });
        }
        return throwError({ error: true, show: true, message: err.error.reason });
      })
    );
  }

  /**
   * Upsert device
   */
  saveDevice(deviceKey: string, device: DeviceModel): Observable<any> {
    const options = { headers: this.api.getRegistryUpsertHeaders() };
    const url = this.api.getRegistryServer() + 'api/device/upsert';
    const formData = new FormData();

    Object.keys(device).forEach((key: string) => {
      let data = null;
      if (key !== 'dataStreams') {
        if (typeof device[key] === 'string' || typeof device[key] === 'number') {
          data = device[key];
        } else {
          data = new Blob([JSON.stringify(device[key])], { type: 'application/json' });
        }
        formData.append(key, data);
      }
    });
    formData.append('device', deviceKey);
    return this.api.put(url, formData, options);
  }

  deleteDevice(key: string): Observable<object> {
    const url = this.api.getRegistryServer() + 'api/device/delete';
    const options = {
      headers: this.api.getRegistryHeaders(),
      params: { device: key },
    };
    return this.api.delete(url, options);
  }

  upsertStream(streamKey, stream: StreamModel, devices: String[] = []): Observable<any> {
    const options = { headers: this.api.getRegistryUpsertHeaders() };
    const url = this.api.getRegistryServer() + 'api/datastream/upsert';
    const formData = new FormData();

    Object.keys(stream).forEach((key: string) => {
      let data = null;
      if (key !== 'key') {
        if (typeof stream[key] === 'string' || typeof stream[key] === 'number') {
          data = stream[key];
        } else {
          data = new Blob([JSON.stringify(stream[key])], { type: 'application/json' });
        }
        formData.append(key, data);
      }
    });
    if (devices != null && devices.length > 0) {
      const deviceLinks = devices.map((deviceKey, index) => ({ key: deviceKey, linkName: 'stream' + index }));
      const devicesList = new Blob([JSON.stringify(deviceLinks)], { type: 'application/json' });
      formData.append('devices', devicesList);
    }
    formData.append('dataStream', streamKey);
    return this.api.put(url, formData, options).pipe(map(res => res.dataStream));
  }

  addStream(deviceKey, dataStreamKey, name): Observable<any> {
    const url = this.api.getRegistryServer() + 'api/device/add_stream';
    const body = new HttpParams()
      .set('device', deviceKey)
      .set('dataStream', dataStreamKey)
      .set('name', name)
      .toString();

    const options = {
      headers: this.api.getRegistryHeaders(),
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    return this.api.put(url, body, options);
  }

  getStream(key: string): Observable<object> {
    const url = this.api.getRegistryServer() + 'api/datastream/get';
    const options = {
      headers: this.api.getRegistryHeaders(),
      params: {
        dataStream: key,
        detail: 'full',
        showPermissions: true
      },
    };
    return this.api.get(url, options).pipe(map(res => res.dataStream));
  }

  deleteStream(key: string): Observable<object> {
    const url = this.api.getRegistryServer() + 'api/datastream/delete';
    const options = {
      headers: this.api.getRegistryHeaders(),
      params: { dataStream: key },
    };
    return this.api.delete(url, options);
  }

  getStreamAggregations(streams: string[], dates = this.api.defaultDates(),
    interval = 3600, type: string, fields?: string[], groupByStream?: boolean): Observable<any> {
    const url = this.api.getDataServer() + 'api/v1/metrics/agg';
    const metricType = (type === 'uk.nominet.iot.model.timeseries.MetricInt') ? 'value_int' : 'value_double';
    const body = {
      'streams': streams,
      'timeLimit': this.api.dateMathDifference(dates),
      'time': dates.to.toISOString(),
      'metricType': metricType,
      'interval': interval
    };
    if (fields) {
      body['fields'] = fields;
    }
    if (groupByStream) {
      body['groupByStream'] = true;
    }
    const options = { headers: this.api.getDataHeaders() };

    return this.api.post(url, body, options).pipe(map((succ: any) => succ.response));
  }

  getJson(streams: string[], dates: DateRange = this.api.defaultDates(), size?: number): Observable<any> {
    const url = this.api.getDataServer() + 'api/v1/json/list';
    const body = {
      'streams': streams,
      'time': dates.to.toISOString(),
    };
    if (dates.from != null) {
      body['timeLimit'] = this.api.dateMathDifference(dates);
    }
    if (size != null) {
      body['size'] = size;
    }
    const options = { headers: this.api.getDataHeaders() };

    return this.api.post(url, body, options).pipe(map((succ: any) => succ.response));
  }

  getConfig(streams: string[], size?: number, to?: string, limit?: string, forward?: boolean): Observable<any> {
    const url = this.api.getDataServer() + 'api/v1/config/list';
    const body = {
      'streams': streams,
      'time': 'now',
    };
    if (to != null) {
      body['time'] = to;
    }
    if (limit != null) {
      body['timeLimit'] = limit;
    }
    if (size != null) {
      body['size'] = size;
    }
    if (forward != null) {
      body['forward'] = forward;
    }
    const options = { headers: this.api.getDataHeaders() };

    return this.api.post(url, body, options).pipe(map((succ: any) => succ.response));
  }

  setConfig(streamKey: string, content: any): Observable<any> {
    const url = this.api.getBridgeServer() + `api/v1/upload/${streamKey}`;
    const headers = this.api.getDataHeaders();
    headers['content-type'] = 'application/json';
    const options = { headers: headers };

    return this.api.post(url, content, options).pipe(map((succ: any) => succ.response));
  }

}
