/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { ClassificationModel } from 'app/visualisations/media-gallery/models/classification-model';
import { Observable } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import { PreferenceService } from '../preference/preference.service';
import { ApiService } from './api.service';

/**
 * Service for image api queries - used by the media-gallery
 */
@Injectable({
  providedIn: 'root'
})
export class ImageApiService {

  constructor(
    private api: ApiService,
    private preferenceApi: PreferenceService
  ) { }

  getImages(streams: string[], ids: string[] = [], dates = this.api.defaultDates(), metadata = {}, interval = 200): Observable<any> {
    const url = this.api.getDataServer() + 'api/v1/images/list';
    const body = {
      'streams': streams,
      'timeLimit': this.api.dateMathDifference(dates),
      'time': dates.to.toISOString(),
      'burstInterval': interval
    };
    if (metadata !== null && Object.getOwnPropertyNames(metadata).length > 0) {
      body['metadata'] = metadata;
    }
    if (ids.length > 0) {
      body['ids'] = ids;
    }
    const options = { headers: this.api.getDataHeaders() };

    return this.api.post(url, body, options).pipe(map((succ: any) => succ.response));
  }

  getImage(streamKey: string, id: string): Observable<string> {
    const url = this.api.getDataServer() + `api/v1/images/${streamKey}/${id}`;
    const options = { headers: this.api.getDataHeaders() };

    return this.api.get(url, { ...options, responseType: 'blob' }).pipe(
      mergeMap(data => {
        return this.preferenceApi.readFile(data);
      }),
    );
  }

  addTag(streamKey: string, id: string, tag: string): Observable<any> {
    tag = encodeURIComponent(tag);
    const url = this.api.getDataServer() + `api/v1/images/${streamKey}/${id}/tag/${tag}`;
    const options = { headers: this.api.getDataHeaders() };

    return this.api.put(url, {}, options).pipe(map(res => res.response));
  }

  deleteTag(streamKey: string, id: string, tag: string): Observable<any> {
    tag = encodeURIComponent(tag);
    const url = this.api.getDataServer() + `api/v1/images/${streamKey}/${id}/tag/${tag}`;
    const options = { headers: this.api.getDataHeaders() };

    return this.api.delete(url, options).pipe(map(res => res.response));
  }

  deleteImage(streamKey: string, id: string): Observable<any> {
    const url = this.api.getDataServer() + `api/v1/images/${streamKey}/${id}`;
    const options = { headers: this.api.getDataHeaders() };

    return this.api.delete(url, options).pipe(map(res => res.response));
  }

  updateClassifications(streamKey: string, id: string, classifications: ClassificationModel[]): Observable<any> {
    const url = this.api.getDataServer() + `api/v1/images/${streamKey}/${id}/classification`;
    const options = { headers: this.api.getDataHeaders() };
    const body = classifications;

    return this.api.put(url, body, options).pipe(map(res => res.response));

  }
}
