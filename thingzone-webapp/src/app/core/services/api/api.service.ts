/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SearchTerm } from 'app/core/models/common/search-term';
import * as moment from 'moment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { ConfigService } from '../config/config.service';
import { GeoService } from '../geo/geo.service';
import { SessionService } from '../session/session.service';
import { DateRange } from '../visualisation/display-context.service';

/**
 * The main api service, sets up server connections and http requests
 */
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private imageServer: string;
  private dataServer: string;
  private bridgeServer: string;
  private registryServer: string;
  private geotoolsServer: string;
  private geocodingServer: string;
  private menuFeatures: object;

  constructor(
    private session: SessionService,
    private http: HttpClient,
    private geoService: GeoService,
    private configService: ConfigService,
  ) {
    this.dataServer = configService.getConfigValue('dataServer');
    this.registryServer = configService.getConfigValue('registryServer');
    this.imageServer = configService.getConfigValue('imageServer');
    this.bridgeServer = configService.getConfigValue('bridgeServer');
    this.geotoolsServer = configService.getConfigValue('geotoolsServer');
    this.geocodingServer = configService.getConfigValue('geocodingServer');
    this.menuFeatures = configService.getConfigValue('menuFeatures');
  }
  logoutTimeout = null;

  getDataServer() {
    return this.dataServer;
  }

  getRegistryServer() {
    return this.registryServer;
  }

  getBridgeServer() {
    return this.bridgeServer;
  }

  getImageServer() {
    return this.imageServer;
  }

  getGeotoolsServer() {
    return this.geotoolsServer;
  }

  getGeocodingServer() {
    return this.geocodingServer;
  }

  getMenuFeatures() {
    return this.menuFeatures;
  }

  handleHttpError(error: HttpErrorResponse): Observable<any> {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      if (error.status === 401 || (error.error.result === 'fail' && error.error.reason === 'session has expired')) {
        // If a session failure is detected in any query,
        // forcibly log user out.
        // Store the attempted URL for redirecting
        const pathname = window.location.pathname;
        clearTimeout(this.logoutTimeout);
        this.logoutTimeout = setTimeout(() => { this.session.logout(pathname); }, 100);
        error.error.reason = 'not authenticated';
      }
      return throwError(error);
    }
    // return an observable error for further processing
    return throwError(error);
  }

  createSearchObj(searchTerms: SearchTerm[] = [], polygons = []): object {
    const out: any = {};
    const restrictions = [];

    searchTerms.forEach(term => {
      if (term.property === 'type') {
        restrictions.push(this.createRestriction(['type'], term.values));
      } else if (term.property === 'name') {
        restrictions.push(this.createRestriction(['name'], term.values));
        restrictions.push(this.createRestriction(['metadata', 'name'], term.values));
      } else if (term.property === 'keywords') {
        out.keyword = {
          values: term.values
        };
      } else if (term.property === 'geoboundary') {
        let bounds = [];
        term.values.forEach(location => {
          const geometry = this.geoService.getGeometry(location);
          bounds = bounds.concat(geometry.coordinates);
        });

        if (bounds.length > 0) {
          out.boundingPolygons = bounds;
        }
      } else {
        const metadataPath = term.property instanceof Array ? ['metadata', ...term.property] : ['metadata', term.property];
        restrictions.push(this.createRestriction(metadataPath, term.values));
      }
    });

    if (restrictions.length > 0) {
      out.facet = {
        'restrictions': restrictions
      };
    }
    if (polygons.length > 0) {
      if (!('boundingPolygons' in out)) {
        out.boundingPolygons = polygons;
      }
    }
    return out;
  }

  createRestriction(path: string[], values: any[]) {
    return {
      'path': path,
      'values': values
    };
  }

  getRegistryHeaders() {
    return {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-FollyBridge-SessionKey': this.session.getSessionKey()
    };
  }

  getRegistryUpsertHeaders() {
    return {
      'Accept': 'application/json',
      'X-FollyBridge-SessionKey': this.session.getSessionKey()
    };
  }

  getRiskHeaders() {
    return {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-Introspective-Session-Key': this.session.getSessionKey()
    };
  }

  getDataHeaders() {
    return {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-Introspective-Session-Key': this.session.getSessionKey()
    };
  }

  defaultDates(): DateRange {
    const to = new Date();
    const from = new Date(
      to.getFullYear(),
      to.getMonth() - 1,
      to.getDate()
    );
    return { to, from };
  }

  dateMathDifference(dates: DateRange): string {
    const seconds = moment(dates.to).diff(dates.from, 'seconds');
    return seconds + 's';
  }

  getMetricTimeSeries(streamkeys: string[], fromDate: string, toDate: string, size?: number): Observable<any> {
    const url = this.getDataServer() + 'api/v1/metrics/list';
    const body = {
      'streams': streamkeys,
      'timeLimit': this.dateMathDifference({ to: new Date(toDate), from: new Date(fromDate) }),
      'time': toDate,
      'size': size ? size : 10000
    };
    const headers = { headers: this.getDataHeaders() };
    return this.http.post(url, body, headers);
  }

  getAvailableDates(streamkeys: string[], fromDate?: string, toDate?: string): Observable<any> {
    const url = this.getDataServer() + 'api/v1/metrics/dates';
    const body = {
      'streams': streamkeys,
      'timeLimit': this.dateMathDifference({ to: new Date(toDate), from: new Date(fromDate) }),
      'time': toDate,
    };
    const headers = { headers: this.getDataHeaders() };

    return this.http.post(url, body, headers);
  }

  get(url, options): Observable<any> {
    return this.http.get(url, options)
      .pipe(
        catchError(err => this.handleHttpError(err))
      );
  }

  put(url, body, options): Observable<any> {
    return this.http.put(url, body, options)
      .pipe(
        catchError(err => this.handleHttpError(err))
      );
  }

  post(url: string, body, options): Observable<any> {
    return this.http.post(url, body, options)
      .pipe(
        catchError(err => this.handleHttpError(err))
      );
  }

  delete(url, options): Observable<any> {
    return this.http.delete(url, options)
      .pipe(
        catchError(err => this.handleHttpError(err))
      );
  }

  createSessionKey(username, password): Observable<any> {
    const url = this.registryServer + 'api/session/login';
    const body = new HttpParams()
      .set('username', username)
      .set('password', password)
      .toString();
    const options = {
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    };
    return this.post(url, body, options);
  }

  sessionKeepAlive(): Observable<any> {
    const sessionKey = this.session.getSessionKey();
    const url = this.registryServer + 'api/session/keepalive';
    const options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'X-FollyBridge-SessionKey': sessionKey ? sessionKey : ''
      }
    };
    return this.post(url, null, options);
  }

  consumeError(err) {
    if (err.message) console.log(err.message);
  }
}
