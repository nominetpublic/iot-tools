/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { SearchTerm } from 'app/core/models/common/search-term';
import { StreamType } from 'app/core/models/queries/data-query';
import { EventUtils } from 'app/core/utils/event-utils';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { GeoService } from '../geo/geo.service';
import { DateRange } from '../visualisation/display-context.service';
import { ApiService } from './api.service';

/**
 * Service for event and alert queries
 */
@Injectable({
    providedIn: 'root',
})
export class EventApiService {

    constructor(
        private api: ApiService,
        private geoService: GeoService,
    ) { }

    list(dates: DateRange, searchTerms?: SearchTerm[], size = 8000): Observable<any> {
        const url = this.api.getDataServer() + `api/v1/event/list`;
        const body = {};

        this.eventQuery(body, searchTerms, dates, size);

        const options = { headers: this.api.getDataHeaders() };
        return this.api.post(url, body, options).pipe(map(res => res.response));
    }

    geo(dates: DateRange, searchTerms: SearchTerm[], polygon?, size = 8000): Observable<any> {
        const url = this.api.getDataServer() + `api/v1/event/geo`;
        const body = {
            'boundingPolygons': polygon,
        };

        this.eventQuery(body, searchTerms, dates, size);

        const options = { headers: this.api.getDataHeaders() };
        return this.api.post(url, body, options).pipe(map(res => res.response));
    }

    eventQuery(queryObject: any, searchTerms: SearchTerm[], dates?: DateRange, size?: number): void {
        if (dates != null) {
            queryObject['time'] = dates.to.toISOString();
            if (dates.from) {
                queryObject['timeLimit'] = this.api.dateMathDifference(dates);
            } else {
                queryObject['size'] = (size == null) ? 1000 : size;
            }
        }

        Object.assign(queryObject,
            EventUtils.getSeverity(searchTerms),
            EventUtils.getFromSearch(searchTerms, 'eventType'),
            EventUtils.getFromSearch(searchTerms, 'tags'),
            EventUtils.getFromSearch(searchTerms, 'keywords'),
            EventUtils.getFromSearch(searchTerms, 'types'),
            EventUtils.getFromSearch(searchTerms, 'status'),
            EventUtils.getFromSearch(searchTerms, 'devices'),
            this.getNew(searchTerms)
        );

        searchTerms.forEach(term => {
            if (term.property === 'geoboundary') {
                let bounds = [];
                term.values.forEach(location => {
                    const geometry = this.geoService.getGeometry(location);
                    bounds = bounds.concat(geometry.coordinates);
                });

                if (bounds.length > 0) {
                    queryObject.boundingPolygons = bounds;
                }
            }
        });

        if (!('status' in queryObject)) {
            queryObject['status'] = ['open', ''];
        }
    }

    allValues(response): { alerts: any[] } {
        let values = [];
        response.forEach(stream => {
            values = values.concat(stream.values);
        });
        return { alerts: values };
    }

    getEventStreamNames(deviceKeys: string[]): Observable<any> {
        const url = this.api.getDataServer() + 'api/v1/stream/common';

        const body = {
            devices: deviceKeys,
            types: [StreamType.EVENT]
        };
        const options = { headers: this.api.getDataHeaders() };

        return this.api.post(url, body, options).pipe(map(res => res.response));
    }

    private getNew(searchTerms: SearchTerm[]): { new: boolean } {
        const selected = EventUtils.getFromSearch(searchTerms, 'new');
        if (selected == null || !selected.hasOwnProperty('new')) {
            return;
        }
        if (selected['new'].length > 0) {
            return { new: true };
        }
        return;
    }

    get(streamKey: string, id: string): Observable<any> {
        const url = this.api.getDataServer() + `api/v1/event/${streamKey}/${id}`;
        const options = { headers: this.api.getDataHeaders() };

        return this.api.get(url, options).pipe(map(res => res.response));
    }

    addFeedback(streamKey: string, id: string, feedback: any): Observable<any> {
        const url = this.api.getDataServer() + `api/v1/event/${streamKey}/${id}/feedback`;
        const options = { headers: this.api.getDataHeaders() };

        return this.api.put(url, feedback, options).pipe(map(res => res.response));
    }

    deleteFeedback(streamKey: string, id: string, feedbackId: string): Observable<any> {
        const url = this.api.getDataServer() + `api/v1/event/${streamKey}/${id}/feedback/${feedbackId}`;
        const options = { headers: this.api.getDataHeaders() };

        return this.api.delete(url, options).pipe(map(res => res.response));
    }

    addTag(streamKey: string, id: string, tag: string): Observable<any> {
        tag = encodeURIComponent(tag);
        const url = this.api.getDataServer() + `api/v1/event/${streamKey}/${id}/tag/${tag}`;
        const options = { headers: this.api.getDataHeaders() };

        return this.api.put(url, {}, options).pipe(map(res => res.response));
    }

    deleteTag(streamKey: string, id: string, tag: string): Observable<any> {
        tag = encodeURIComponent(tag);
        const url = this.api.getDataServer() + `api/v1/event/${streamKey}/${id}/tag/${tag}`;
        const options = { headers: this.api.getDataHeaders() };

        return this.api.delete(url, options).pipe(map(res => res.response));
    }

    getFacets(): Observable<any> {
        const url = this.api.getDataServer() + `api/v1/event/facets`;
        const options = { headers: this.api.getDataHeaders() };

        return this.api.get(url, options).pipe(map(res => res.response));
    }

    updateSeverity(streamKey: string, id: string, severity: number): Observable<any> {
        const url = this.api.getDataServer() + `api/v1/event/${streamKey}/${id}/severity/${severity}`;
        const options = { headers: this.api.getDataHeaders() };

        return this.api.put(url, {}, options).pipe(map(res => res.response));
    }

    updateStatus(streamKey: string, id: string, status: string): Observable<any> {
        const url = this.api.getDataServer() + `api/v1/event/${streamKey}/${id}/status/${status}`;
        const options = { headers: this.api.getDataHeaders() };

        return this.api.put(url, {}, options).pipe(map(res => res.response));
    }

    getDeviceKeys(relatedTo: any[]): string[] {
        if (relatedTo == null || relatedTo.length === 0) {
            return [];
        }
        const devices = {};
        // Find all unique related devices
        relatedTo.forEach(item => {
            if (item.klass === 'uk.nominet.iot.model.registry.RegistryDevice') {
                devices[item.key] = item.description;
            }
        });
        return Object.keys(devices);
    }

    getLinkRelations(relatedTo: any[]): string[] {
        if (relatedTo == null || relatedTo.length === 0) {
            return [];
        }
        const links = relatedTo.filter(item => item.type === 'link');
        return links;
    }

    getImageKeys(derivedFrom: any[]): string[] {
        if (derivedFrom == null || derivedFrom.length === 0) {
            return [];
        }
        const images = {};
        // Find all unique related devices
        derivedFrom.forEach(item => {
            if (item.klass === 'uk.nominet.iot.model.timeseries.Image') {
                images[item.streamKey] = item.description;
            }
        });
        return Object.keys(images);
    }

}
