/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Injectable()
export class ItemSelectionService {

  private itemHover = new BehaviorSubject(null);
  $itemHover = this.itemHover.asObservable();

  private itemSelect = new BehaviorSubject<string>(null);
  $itemSelect: Observable<string> = this.itemSelect.asObservable();

  private itemFocus = new BehaviorSubject<string>(null);
  $itemFocus: Observable<string> = this.itemFocus.asObservable();

  private itemBlur = new Subject();
  $itemBlur = this.itemBlur.asObservable();

  private itemMutate = new Subject();
  $itemMutate = this.itemMutate.asObservable();

  constructor() { }

  hover(key): void {
    this.itemHover.next(key);
  }

  leave(): void {
    this.itemHover.next(null);
  }

  select(key, toggle = false): void {
    if (key !== this.itemSelect.getValue()) {
      this.itemSelect.next(key);
    } else if (toggle) {
      this.blur();
    }
  }

  getSelectedKey(): string {
    return this.itemSelect.getValue();
  }

  focus(key): void {
    this.itemFocus.next(key);
  }

  blur(): void {
    this.itemSelect.next(null);
    this.itemFocus.next(null);
    this.itemBlur.next(null);
  }
}
