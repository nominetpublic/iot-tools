/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable, OnDestroy } from '@angular/core';
import { SelectOption } from 'app/core/models/common/select-option';
import { AggregationQuery } from 'app/core/models/queries/aggregation-query';
import { DataQuery, DataQueryType, EventType, StreamType } from 'app/core/models/queries/data-query';
import { DeviceLocationQuery } from 'app/core/models/queries/device-location-query';
import { DeviceQuery } from 'app/core/models/queries/device-query';
import { EventLocationQuery } from 'app/core/models/queries/event-location-query';
import { EventsQuery } from 'app/core/models/queries/events-query';
import { ImagesQuery } from 'app/core/models/queries/images-query';
import { LocationQuery } from 'app/core/models/queries/location-query';
import { MetricsQuery } from 'app/core/models/queries/metrics-query';
import { QuerySource } from 'app/core/models/visualisations/display-data-source';
import { DisplayResource } from 'app/core/models/visualisations/display-resource';
import { VisualisationComponent, VisualisationComponentType } from 'app/core/models/visualisations/visualisation-component';
import { VisualisationContent } from 'app/core/models/visualisations/visualisation-content';
import { VisualisationDataLayer } from 'app/core/models/visualisations/visualisation-data-layer';
import { DeviceApiService } from 'app/core/services/api/device-api.service';
import { PreferenceService } from 'app/core/services/preference/preference.service';
import { SessionService } from 'app/core/services/session/session.service';
import { ObjectUtils } from 'app/core/utils/object-utils';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { ApiService } from '../api/api.service';
import { DataApiService } from '../api/data-api.service';

export interface VisualisationLayout {
  value: string;
  title: string;
  displays: string[];
}

/**
 * Service containing methods for displaying visualisations and their configuration
 */
@Injectable({
  providedIn: 'root'
})
export class VisualisationService implements OnDestroy {
  subscriptions: Subscription[] = [];

  typeOptions: SelectOption[] = [
    { value: VisualisationComponentType.MAP, title: 'Map', icon: 'map-marker-alt' },
    { value: VisualisationComponentType.LINEGRAPH, title: 'Line Graph', icon: 'chart-line' },
    { value: VisualisationComponentType.BARGRAPH, title: 'Bar Graph', icon: 'chart-bar' },
    { value: VisualisationComponentType.GAUGE, title: 'Gauge', icon: 'tachometer-alt' },
    { value: VisualisationComponentType.PIECHART, title: 'Pie Chart', icon: 'chart-pie' },
    { value: VisualisationComponentType.EVENTS, title: 'Scatter Graph', icon: 'calendar-alt' },
    { value: VisualisationComponentType.IMAGES, title: 'Gallery', icon: 'image' },
    { value: VisualisationComponentType.VALUE, title: 'Value', icon: 'font' },
  ];

  layouts: VisualisationLayout[] = [
    { value: 'single', title: 'Single Display', displays: ['Main Window'] },
    { value: 'column', title: '2 Columns', displays: ['Left Column', 'Right Column'] },
    { value: 'horizontal', title: 'Horizontal Split', displays: ['Top', 'Bottom'] },
  ];

  aggregationTypes: SelectOption[] = [
    { value: 'avg', title: 'Average' },
    { value: 'variance', title: 'Variance' },
    { value: 'std_deviation', title: 'Standard Deviation' },
    { value: 'sum', title: 'Sum' },
    { value: 'sum_of_squares', title: 'Sum of Squares' },
    { value: 'count', title: 'Count' },
    { value: 'min', title: 'Minimum' },
    { value: 'max', title: 'Maximum' },
  ];

  queryTypeOptions: SelectOption[] = [
    { value: DataQueryType.DEVICELOCATION, title: 'Device Locations' },
    { value: DataQueryType.DEVICE, title: 'Device' },
    { value: DataQueryType.LOCATION, title: 'Stream Location' },
    { value: DataQueryType.METRICS, title: 'Stream Results' },
    { value: DataQueryType.AGGREGATION, title: 'Aggregated Results' },
    { value: DataQueryType.IMAGES, title: 'Images' },
    { value: DataQueryType.EVENTLOCATION, title: 'Event Locations' },
    { value: DataQueryType.EVENTS, title: 'Events' },
  ];

  eventTypes: SelectOption[] = [
    { value: '', title: '' },
    { value: EventType.ALERT, title: 'Alerts' },
    { value: EventType.GENERIC, title: 'Generic' },
    { value: EventType.OBSERVATION, title: 'Observation' },
  ];

  imageGroupByOptions: SelectOption[] = [
    { value: '', title: '' },
    { value: 'HOUR', title: 'Hour' },
    { value: 'DAY', title: 'Day' },
    { value: 'WEEK', title: 'Week' },
    { value: 'MONTH', title: 'Month' },
    { value: 'YEAR', title: 'Year' },
  ];

  private visualisations: BehaviorSubject<DisplayResource[]> = new BehaviorSubject(null);
  visualisations$ = this.visualisations.asObservable();

  private visualisationType: BehaviorSubject<string> = new BehaviorSubject(null);
  visualisationType$: Observable<string> = this.visualisationType.asObservable();

  private mapCentre: BehaviorSubject<[number, number]> = new BehaviorSubject(null);
  mapCentre$ = this.mapCentre.asObservable();

  private displayLayers: BehaviorSubject<{ [index: string]: VisualisationDataLayer[] }> = new BehaviorSubject(null);
  displayLayers$ = this.displayLayers.asObservable();
  imageServer: any;

  constructor(
    private session: SessionService,
    private api: ApiService,
    private dataApi: DataApiService,
    private deviceApi: DeviceApiService,
    private preferenceService: PreferenceService) {
    const sessionSubscription = this.session.sessionKey$.subscribe(key => {
      if (key == null) {
        this.visualisations.next(null);
        this.displayLayers.next(null);
      }
    });
    this.subscriptions.push(sessionSubscription);

    this.imageServer = this.api.getImageServer();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  queryVisualisations(visualisationType: string): void {
    if (visualisationType == null) {
      this.setVisualisations([], visualisationType);
      return;
    }
    this.preferenceService.listJsonResources(visualisationType).subscribe(resources => {
      let displayResources: DisplayResource[] = [];
      resources.forEach(resource => {
        displayResources.push(ObjectUtils.parseResource(resource));
      });
      displayResources = this.sortVisualisations(displayResources);
      this.setVisualisations(displayResources, visualisationType);
    });
  }

  setVisualisations(resources: DisplayResource[], visType: string): void {
    this.visualisations.next(resources);
    this.visualisationType.next(visType);
  }

  getCurrentVisualisationType(): string {
    return this.visualisationType.getValue();
  }

  getVisualisations(): DisplayResource[] {
    return this.visualisations.getValue();
  }

  getVisualisation(key: string): DisplayResource {
    const visualisations: DisplayResource[] = this.getVisualisations();
    if (visualisations == null) {
      return;
    }
    return visualisations.find(v => v.key === key);
  }

  getEmptyVisualisation(): DisplayResource {
    return {
      resourceName: 'visualisation',
      type: 'visualisation',
      name: '',
      content: {
        components: [],
        configuration: {
          layout: 'single'
        }
      }
    };
  }

  parseVisualisation(resource: DisplayResource): DisplayResource {
    const display = ObjectUtils.parseResource(resource);
    if ((<VisualisationContent>display.content).components !== undefined) {
      return display;
    }
  }

  queryStreamNames(displayType: VisualisationComponentType, dataSource: QuerySource): Observable<SelectOption[]> {
    const streamTypes: StreamType[] = this.selectStreamType(displayType, dataSource.query);

    return this.dataApi.getStreamNames(dataSource, streamTypes).pipe(map(res => {
      const options: SelectOption[] = res.map(stream => ({
        title: stream.name, value: stream.name
      }));
      return options;
    }));
  }

  getComponentId(): number {
    return new Date().getTime();
  }

  getEmptyComponent(layoutIndex, type = null): VisualisationComponent {
    return {
      id: this.getComponentId(),
      type: type,
      name: '',
      sources: [],
      layoutComponent: layoutIndex,
      configuration: {}
    };
  }

  getEmptyDataSource(componentType: VisualisationComponentType): QuerySource {
    let query = DataQueryType.AGGREGATION;
    switch (componentType) {
      case VisualisationComponentType.MAP:
        query = DataQueryType.DEVICELOCATION;
        break;
      case VisualisationComponentType.IMAGES:
        query = DataQueryType.IMAGES;
        break;
      case VisualisationComponentType.EVENTS:
        query = DataQueryType.EVENTS;
        break;
      default:
        query = DataQueryType.AGGREGATION;
        break;
    }
    return {
      id: this.getComponentId(),
      query: query,
      name: '',
      configuration: {},
      deviceFilter: {
        terms: []
      }
    };
  }

  findLayout(layoutValue: string): VisualisationLayout {
    return this.layouts.find((layout: VisualisationLayout) => layout.value === layoutValue);
  }

  getComponentsForLayout(displayConfig: VisualisationContent, layoutIndex: number): VisualisationComponent[] {
    return displayConfig.components.filter(c => c.layoutComponent === layoutIndex);
  }

  queryFactory(source: QuerySource): DataQuery {
    switch (source.query) {
      case DataQueryType.AGGREGATION:
        return new AggregationQuery(this.dataApi);
      case DataQueryType.DEVICELOCATION:
        return new DeviceLocationQuery(this.deviceApi);
      case DataQueryType.DEVICE:
        return new DeviceQuery(this.deviceApi);
      case DataQueryType.LOCATION:
        return new LocationQuery(this.dataApi);
      case DataQueryType.METRICS:
        return new MetricsQuery(this.dataApi);
      case DataQueryType.IMAGES:
        return new ImagesQuery(this.dataApi);
      case DataQueryType.EVENTLOCATION:
        return new EventLocationQuery(this.dataApi);
      case DataQueryType.EVENTS:
        return new EventsQuery(this.dataApi);
    }
  }

  selectQueryType(displayType: VisualisationComponentType): DataQueryType[] {
    switch (displayType) {
      case VisualisationComponentType.MAP:
        return [DataQueryType.DEVICELOCATION, DataQueryType.LOCATION, DataQueryType.EVENTLOCATION];
      case VisualisationComponentType.IMAGES:
        return [DataQueryType.IMAGES];
      case VisualisationComponentType.EVENTS:
        return [DataQueryType.EVENTS, DataQueryType.IMAGES];
      case VisualisationComponentType.VALUE:
        return [DataQueryType.AGGREGATION, DataQueryType.DEVICE, DataQueryType.EVENTS];
      case VisualisationComponentType.PIECHART:
        return [DataQueryType.AGGREGATION, DataQueryType.EVENTS, DataQueryType.DEVICE, DataQueryType.METRICS];
      case VisualisationComponentType.BARGRAPH:
      case VisualisationComponentType.GAUGE:
        return [DataQueryType.AGGREGATION, DataQueryType.EVENTS, DataQueryType.METRICS];
      default:
        return [DataQueryType.AGGREGATION];
    }
  }

  selectStreamType(displayType: VisualisationComponentType, queryType: DataQueryType): StreamType[] {
    switch (displayType) {
      case VisualisationComponentType.MAP:
        if (queryType === DataQueryType.LOCATION) {
          return [StreamType.LATLONG];
        } else if (queryType === DataQueryType.EVENTLOCATION) {
          return [StreamType.EVENT];
        } else if (queryType === DataQueryType.DEVICELOCATION) {
          return [StreamType.DOUBLE, StreamType.INTEGER, StreamType.STRING, StreamType.EVENT, StreamType.IMAGE];
        }
        break;
      case VisualisationComponentType.IMAGES:
        return [StreamType.IMAGE];
      case VisualisationComponentType.EVENTS:
        if (queryType === DataQueryType.EVENTS) {
          return [StreamType.EVENT];
        } else if (queryType === DataQueryType.IMAGES) {
          return [StreamType.IMAGE];
        }
        break;
      case VisualisationComponentType.VALUE:
        if (queryType === DataQueryType.EVENTS) {
          return [StreamType.EVENT];
        } else {
          return [StreamType.DOUBLE, StreamType.INTEGER];
        }
      case VisualisationComponentType.BARGRAPH:
      case VisualisationComponentType.GAUGE:
      case VisualisationComponentType.PIECHART:
        if (queryType === DataQueryType.EVENTS) {
          return [StreamType.EVENT];
        } if (queryType === DataQueryType.METRICS) {
          return [StreamType.STRING];
        } else {
          return [StreamType.DOUBLE, StreamType.INTEGER];
        }
      default:
        return [StreamType.DOUBLE, StreamType.INTEGER];
    }
  }

  selectQueryOptions(displayType: VisualisationComponentType): SelectOption[] {
    const queries = this.selectQueryType(displayType);
    return this.queryTypeOptions.filter(queryType => queries.includes(queryType.value));
  }

  setMapCentre(centre: [number, number]) {
    this.mapCentre.next(centre);
  }

  getMinMaxGraphData(aggregationData, aggregationType = 'avg'): any[] {
    const seriesData = [];
    aggregationData.forEach(stream => {
      const mappedData = this.reduceMinMaxAggregationData(stream.values, aggregationType);
      seriesData.push({ name: stream.streamKey, series: mappedData });
    });
    return seriesData;
  }

  private reduceMinMaxAggregationData(values, aggregationType): any[] {
    return values.reduce((acc, val) => {
      acc.push({
        name: new Date(val.timestamp),
        value: val[aggregationType],
        min: val.min,
        max: val.max
      });
      return acc;
    }, []);
  }

  /**
   * Returns a new list of widgets sorted by after
   * @param {DisplayResource[]} resources
   * @returns {DisplayResource[]}
   */
  private sortVisualisations(resources: DisplayResource[]): DisplayResource[] {
    if (resources.length <= 1) {
      return resources;
    }
    const sortedList: DisplayResource[] = [];
    const visMap = {};
    let currentKey = null;

    for (let i = 0; i < resources.length; i++) {
      const item: DisplayResource = resources[i];
      if (item.content.after == null) {
        currentKey = item.key;
        sortedList.push(item);
      } else {
        visMap[item.content.after] = i;
      }
    }
    // The map does not have the right number of keys to allow list ordering so something has gone wrong
    if ((Object.keys(visMap).length + 1) !== resources.length) {
      return this.reorderVisualisations(resources);
    }
    while (sortedList.length < resources.length) {
      // get the item with a previous item ID referencing the current item
      if (currentKey in visMap) {
        const nextItem = resources[visMap[currentKey]];
        sortedList.push(nextItem);
        currentKey = nextItem.key;
      } else {
        // the map does not contain all possible keys, so stop trying to sort and reset the ordering
        return this.reorderVisualisations(resources);
      }
    }
    return sortedList;
  }

  /**
  * Modifies widgets so that resource ordering matches the given list
  *
  * @param {SearchResource[]} searches
  * @returns {SearchResource[]}
  */
  reorderVisualisations(resources: DisplayResource[]): DisplayResource[] {
    let currentKey = null;
    for (let i = 0; i < resources.length; i++) {
      const item: DisplayResource = resources[i];
      if (item.content.after !== currentKey) {
        item.content.after = currentKey;
        this.upsertVisualisation(item, item.type);
      }
      currentKey = item.key;
    }
    return resources;
  }

  upsertVisualisation(item: DisplayResource, type: string): Observable<any> {
    const content = new Blob([JSON.stringify(item.content)], { type: 'application/json' });
    const key = (item.key) ? item.key : null;
    return this.preferenceService.upsertResource('visualisation', content, key, type, type, item.name);
  }

  deleteVisualisation(key: string): Observable<any> {
    return this.preferenceService.deleteResource(key, 'visualisation');
  }

  setDisplayLayers(layers: { [index: string]: VisualisationDataLayer[] }): void {
    this.displayLayers.next(layers);
  }

  getCurrentLayers(): { [index: string]: VisualisationDataLayer[] } {
    return this.displayLayers.getValue();
  }

  clearDisplayLayers(): void {
    this.displayLayers.next({});
  }

  getThumbnailUrl(ref): string {
    return `${this.imageServer}images/${ref}/r150x100`;
  }

}
