/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { from, Observable, Subject } from 'rxjs';

import { PreferenceService } from '../preference/preference.service';

@Injectable({
  providedIn: 'root'
})
export class AnnotationService {
  annotationCollection = new Subject<{ key: string, name: string, resourceType: string }>();

  constructor(private prefService: PreferenceService) { }

  listAnnotations(): Observable<any> {
    return this.prefService.listResources('map-annotation');
  }

  upsertResources(entityKey: string, entityName: string, featureCollection: GeoJSON.FeatureCollection, colour: string): Observable<any> {
    featureCollection['name'] = entityName;
    featureCollection['colour'] = colour;

    return this.prefService.upsertResource(
      'annotation-collection',
      new Blob([JSON.stringify(featureCollection)], { type: 'application/json' }),
      entityKey,
      colour,
      'map-annotation',
      entityName
    );
  }

  getAnnotations(key: string): Observable<GeoJSON.FeatureCollection> {
    return from(this.prefService.getResource(key, 'annotation-collection'));
  }
}
