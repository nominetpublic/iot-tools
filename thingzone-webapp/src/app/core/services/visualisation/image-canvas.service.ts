/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable, ElementRef } from '@angular/core';
import { Subject } from 'rxjs';
import { ClassificationModel } from 'app/visualisations/media-gallery/models/classification-model';

@Injectable({
  providedIn: 'root'
})
export class ImageCanvasService {

  imageContainerDimensions = new Subject<{ height: number, width: number }>();
  imageDimensions = new Subject<{ height: number, width: number }>();
  canvasDimensions = new Subject<{ height: number, width: number }>();

  constructor() { }

  clearBounds(ctx: CanvasRenderingContext2D, canvasRef: ElementRef) {
    ctx.clearRect(0, 0, canvasRef ? canvasRef.nativeElement.width : 2000, canvasRef ? canvasRef.nativeElement.height : 2000);
  }

  public adjustCanvas(imageContainerRef: ElementRef, imageRef: ElementRef, canvasRef: ElementRef) {
    const imageContainerDimensions = imageContainerRef.nativeElement.getBoundingClientRect();
    this.imageContainerDimensions.next(imageContainerDimensions);
    const imageBounds = { width: imageRef.nativeElement.naturalWidth, height: imageRef.nativeElement.naturalHeight };
    const canvasSize = this.downSize(imageBounds, imageContainerDimensions);

    imageRef.nativeElement.height = canvasSize.height;
    imageRef.nativeElement.width = canvasSize.width;
    this.imageDimensions.next(imageRef.nativeElement.getBoundingClientRect());

    canvasRef.nativeElement.height = canvasSize.height;
    canvasRef.nativeElement.width = canvasSize.width;
    this.canvasDimensions.next(canvasRef.nativeElement.getBoundingClientRect());
  }

  public drawBounds(ctx: CanvasRenderingContext2D, classification: ClassificationModel,
    canvasDimensions: { height: number, width: number }) {
    if (classification.selection) {
      // bbox[ymin,xmin,ymax,xmax]
      const regex = /bbox\[([0-9\.]*?),\s*([0-9\.]*?),\s*([0-9\.]*?),\s*([0-9\.]*?)\]/;
      const found = classification.selection.match(regex);
      if (found.length === 5) {
        const ymin = Number(found[1]); const xmin = Number(found[2]);
        const ymax = Number(found[3]); const xmax = Number(found[4]);

        this.drawDoubleRectangle(
          ctx,
          canvasDimensions.width * xmin,
          canvasDimensions.height * ymin,
          (canvasDimensions.width * xmax) - (canvasDimensions.width * xmin),
          (canvasDimensions.height * ymax) - (canvasDimensions.height * ymin),
          classification.colour);
      }
    }
  }

  public drawDoubleRectangle(ctx: CanvasRenderingContext2D, x: number, y: number, w: number, h: number, colour: string) {
    ctx.strokeStyle = '#ffffff';
    ctx.lineWidth = 5;
    ctx.strokeRect(x, y, w, h);
    ctx.strokeStyle = colour;
    ctx.lineWidth = 3;
    ctx.strokeRect(x, y, w, h);
  }

  private downSize(rawImage: { height: number, width: number }, container: { height: number, width: number })
    : { height: number, width: number, scale: number } {

    const output = { height: 0, width: 0, scale: 0 };
    output.scale = Math.min(Math.min(container.width / rawImage.width, container.height / rawImage.height), 1);
    output.width = Math.round(output.scale * rawImage.width);
    output.height = Math.round(output.scale * rawImage.height);

    return output;
  }




}
