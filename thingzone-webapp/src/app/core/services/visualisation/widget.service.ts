/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable, OnDestroy } from '@angular/core';
import { DisplayResource } from 'app/core/models/visualisations/display-resource';
import { PreferenceService } from 'app/core/services/preference/preference.service';
import { SessionService } from 'app/core/services/session/session.service';
import { ObjectUtils } from 'app/core/utils/object-utils';
import { BehaviorSubject, Observable, Subject, Subscription } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class WidgetService implements OnDestroy {
  subscriptions: Subscription[] = [];

  private widgets: BehaviorSubject<DisplayResource[]> = new BehaviorSubject(null);
  widgets$: Observable<DisplayResource[]> = this.widgets.asObservable();

  private widgetType: BehaviorSubject<string> = new BehaviorSubject(null);
  widgetType$: Observable<string> = this.widgetType.asObservable();

  mapState$: Subject<boolean> = new BehaviorSubject(null);

  constructor(
    private session: SessionService,
    private preferenceService: PreferenceService,
  ) {
    const sessionSubscription = this.session.sessionKey$.subscribe(key => {
      if (key == null) {
        this.widgets.next(null);
      }
    });
    this.subscriptions.push(sessionSubscription);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  queryWidgets(widgetType: string): void {
    if (widgetType == null) {
      this.setWidgets([], null);
      return;
    }
    this.preferenceService.listJsonResources(widgetType).subscribe(resources => {
      let displayResources: DisplayResource[] = [];

      resources.forEach(resource => {
        displayResources.push(ObjectUtils.parseResource(resource));
      });
      displayResources = this.sortWidgets(displayResources);
      this.setWidgets(displayResources, widgetType);
    });
  }

  clearWidgets(): void {
    this.widgets.next(null);
    this.widgetType.next(null);
  }

  setWidgets(resources: DisplayResource[], widgetType: string): void {
    this.widgets.next(resources);
    this.widgetType.next(widgetType);
  }

  /**
   * Deep copy widget using json parsing
   */
  cloneWidget(widget: DisplayResource): DisplayResource {
    return ObjectUtils.parseResource(widget);
  }

  getWidgets(): DisplayResource[] {
    const widgets = this.widgets.getValue();
    if (widgets == null) { return []; }
    return widgets;
  }

  getWidget(key: string): DisplayResource {
    const widgets = this.widgets.getValue();
    if (widgets == null) { return; }
    return widgets.find(v => v.key === key);
  }

  getEmptyWidget(widgetType = 'widget'): DisplayResource {
    return {
      resourceName: 'widget',
      type: widgetType,
      name: '',
      content: {
        sources: [],
        type: 'map',
        configuration: {},
      }
    };
  }

  getCurrentWidgetType(): string {
    return this.widgetType.getValue();
  }

  saveMapState(): void {
    this.mapState$.next(true);
  }

  /**
   * Returns a new list of widgets sorted by after
   * @param {DisplayResource[]} resources
   * @returns {DisplayResource[]}
   */
  private sortWidgets(resources: DisplayResource[]): DisplayResource[] {
    if (resources.length <= 1) {
      return resources;
    }
    const sortedList: DisplayResource[] = [];
    const map = {};
    let currentKey = null;

    for (let i = 0; i < resources.length; i++) {
      const item: DisplayResource = resources[i];
      if (item.content.after == null) {
        currentKey = item.key;
        sortedList.push(item);
      } else {
        map[item.content.after] = i;
      }
    }
    // The map does not have the right number of keys to allow list ordering so something has gone wrong
    if ((Object.keys(map).length + 1) !== resources.length) {
      return this.reorderWidgets(resources);
    }
    while (sortedList.length < resources.length) {
      // get the item with a previous item ID referencing the current item
      if (currentKey in map) {
        const nextItem = resources[map[currentKey]];
        sortedList.push(nextItem);
        currentKey = nextItem.key;
      } else {
        // the map does not contain all possible keys, so stop trying to sort and reset the ordering
        return this.reorderWidgets(resources);
      }
    }
    return sortedList;
  }

  /**
  * Modifies widgets so that resource ordering matches the given list
  *
  * @param {SearchResource[]} searches
  * @returns {SearchResource[]}
  */
  reorderWidgets(resources: DisplayResource[]): DisplayResource[] {
    let currentKey = null;
    for (let i = 0; i < resources.length; i++) {
      const item: DisplayResource = resources[i];
      if (item.content.after !== currentKey) {
        item.content.after = currentKey;
        this.upsertWidget(item, item.type);
      }
      currentKey = item.key;
    }
    return resources;
  }

  upsertWidget(item: DisplayResource, type: string): Observable<any> {
    const content = new Blob([JSON.stringify(item.content)], { type: 'application/json' });
    const key = (item.key) ? item.key : null;
    return this.preferenceService.upsertResource('widget', content, key, type, type, item.name);
  }

  deleteWidget(key: string): Observable<any> {
    return this.preferenceService.deleteResource(key, 'widget');
  }

}
