/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { DateUtils } from 'app/core/utils/date-utils';
import * as moment from 'moment';
import { BehaviorSubject } from 'rxjs';
import { debounceTime, filter, groupBy, mergeMap } from 'rxjs/operators';

export interface DateRange {
  from?: Date;
  to: Date;
}

export interface DisplayContext {
  scope: string;
  range: DateRange;
  current: Date;
  interval?: DateRange;
  limit?: string;
  boundingPolygon?;
}

/**
 * Service for tracking the time context for visualisations
 */
@Injectable()
export class DisplayContextService {

  private defaultToDate = new Date();
  private defaultFromDate = new Date();

  private context: BehaviorSubject<DisplayContext> = new BehaviorSubject(null);
  public context$ = this.context.asObservable();

  constructor() {
    this.newContext(this.getDefaultContext(), 'range');
  }

  getContext() {
    return this.context.getValue();
  }

  newContext(context: DisplayContext, scope: string) {
    return { ...context, scope: scope };
  }

  clearContext(): void {
    if (this.context != null) {
      this.setContext(null);
    }
  }

  getDefaultContext(): DisplayContext {
    return {
      scope: 'range',
      range: {
        from: this.defaultFromDate,
        to: this.defaultToDate,
      },
      current: this.defaultFromDate,
    };
  }

  createRangeContext(from: Date, to: Date) {
    return {
      scope: 'range',
      range: {
        from: from,
        to: to,
      },
      current: to,
    };
  }

  createWidgetContext(timeDelta: string): DisplayContext {
    let timeLimit: string;
    let from = moment().subtract(1, 'days');
    if (timeDelta == null) {
      const diff = moment().diff(from, 'seconds');
      timeLimit = diff + 's';
    } else {
      timeLimit = timeDelta;
      from = DateUtils.calcTimeDelta(moment(), timeDelta);
    }
    const now = moment().toDate();
    return {
      scope: 'range',
      range: {
        from: from.toDate(),
        to: now,
      },
      limit: timeLimit,
      current: now,
    };
  }

  setContext(context: DisplayContext) {
    this.context.next(context);
  }

  updateDateRange(to: Date, from: Date, current?: Date, refreshContext = false) {
    if (to == null) {
      to = new Date();
    }
    let currentContext = this.getContext();
    if (refreshContext) {
      currentContext = this.getDefaultContext();
    }
    const context = { ...currentContext, range: { from, to } };
    context.current = (current == null) ? from : current;

    this.context.next(
      this.newContext(context, 'range')
    );
  }

  updateCurrentDate(liveDate: Date) {
    const context = { ...this.getContext(), current: liveDate };

    this.context.next(
      this.newContext(context, 'current')
    );
  }

  updateBoundingPolygon(polygon) {
    const context = { ...this.getContext(), boundingPolygon: polygon };

    this.context.next(
      this.newContext(context, 'boundingPolygon')
    );
  }

  dateChange() {
    return this.context$.pipe(
      filter(a => a != null && (a.scope === 'current' || a.scope === 'range'))
    );
  }

  dateDebounced(fn, debounce = 200) {
    return this.context$.pipe(
      filter(a => a != null && (a.scope === 'current' || a.scope === 'range')),
      debounceTime(debounce)
    ).subscribe(fn);
  }

  contextDebounced(fn, debounce = 200) {
    if (this.context.getValue() != null) {
      return this.context$.pipe(
        groupBy(context => context.scope),
        mergeMap(group => group.pipe(
          debounceTime(debounce)))
      ).subscribe(fn);
    }
    return this.context$.subscribe(fn);
  }
}
