/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import centroid from '@turf/centroid';

import { ConfigService } from '../config/config.service';

@Injectable({
  providedIn: 'root'
})
export class GeoService {
  private accessToken: string;

  constructor(
    private configService: ConfigService,
  ) {
    this.accessToken = configService.getConfigValue('accessToken');
  }

  /* MAP SETUP */
  setApiKey(mapboxgl) {
    Object.getOwnPropertyDescriptor(mapboxgl, 'accessToken')
      .set(this.accessToken);
  }

  getMapBoundingBox(map, bufferFactor = 2) {
    const mapBounds = map.getBounds();

    let N = mapBounds.getNorth();
    let E = mapBounds.getEast();
    let S = mapBounds.getSouth();
    let W = mapBounds.getWest();

    if (bufferFactor) {
      // Scale bufferFactor by 0.5 as it will
      // be added twice to each side of the axis
      bufferFactor = bufferFactor * 0.5;
      const yBuffer = (N - S) * bufferFactor;
      const xBuffer = (E - W) * bufferFactor;

      N += yBuffer;
      E += xBuffer;
      S -= yBuffer;
      W -= xBuffer;
    }

    // Closed polygon representing
    // map bounding box + buffer
    return [
      [W, N],
      [E, N],
      [E, S],
      [W, S],
      [W, N]
    ];
  }

  createFeature(device) {
    return {
      'type': 'Feature',
      'properties': {
        'key': device.key,
        'name': device.name,
        'type': device.type
      },
      'id': device.key,
      'geometry': {
        'coordinates': device.singlePosition,
        'type': 'Point'
      }
    };
  }

  /**
   * Get the geometry from a valid geojson feature
   *
   * @param {*} geojson
   * @returns
   * @memberof GeoService
   */
  getGeometry(geojson): any {
    if (!('type' in geojson)) {
      return;
    }
    let firstFeature = null;
    if (geojson.features != null && geojson.features.length > 0) {
      firstFeature = geojson.features[0];
    }

    switch (geojson.type) {
      case 'FeatureCollection':
        if (firstFeature != null && 'geometry' in firstFeature) {
          return firstFeature.geometry;
        }
        break;
      case 'Feature':
        if (firstFeature != null && 'geometry' in firstFeature) {
          return firstFeature.geometry;
        }
        break;
      case 'Point':
      case 'LineString':
      case 'Polygon':
        return geojson;
    }
  }

  /**
   * Find the centre coordinates of a geojson geometry
   *
   * @param {*} geometry
   * @returns
   * @memberof GeoService
   */
  getCentre(geometry): [number, number] {
    if (geometry.type === 'Point') {
      return geometry.coordinates;
    } else if (geometry.type === 'LineString' || geometry.type === 'Polygon') {
      return centroid(geometry).geometry.coordinates;
    }
  }
}
