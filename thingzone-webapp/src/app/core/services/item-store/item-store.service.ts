/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { BehaviorSubject, EMPTY, Observable, of } from 'rxjs';
import { first, mergeMap } from 'rxjs/operators';

import { ItemSelectionService } from '../item-selection/item-selection.service';

@Injectable()
export class ItemStoreService {
  private _items = new BehaviorSubject([]);
  items$ = this._items.asObservable();

  private _facets = new BehaviorSubject([]);
  facets$ = this._facets.asObservable();

  private keySchema: {key: string } = { key: 'key' };
  private idSchema: {key: string } = { key: 'id' };

  private schema = this.keySchema;

  constructor(
    // Loads the itemSelection service.
    // This service should not be a singleton
    // ensure it is being provided in a component.
    private itemSelection: ItemSelectionService
  ) { }

  createFacet(
    id: any = Date.now(),
    value,
    name = value.toString(),
    count = 1
  ) {
    return { id, value, name, count };
  }

  setResult(items: any[]): void {
    this._items.next(items);
  }

  setKeySchema(): void {
    this.schema = this.keySchema;
  }

  setIdSchema(): void {
    this.schema = this.idSchema;
  }

  setFacets(facets: any[]): void {
    this._facets.next(facets);
  }

  getFacets(): any[] {
    return this._facets.getValue();
  }

  getItems(): any[] {
    return this._items.getValue();
  }

  createResponse(key, item, index): { key: string, item: any, index: number } {
    if (!key) return null;
    return {
      key, item, index
    };
  }

  // Gets an item from the store
  getItemByKey(id: string): Observable<any> {
    const storeItems = this._items.getValue();
    let response: Observable<any> = EMPTY;
    if (!id || !this.schema || !storeItems) {
      response = of(this.createResponse(id, null, null));
    } else {
      const key = this.schema.key;
      const index = storeItems.findIndex((a: any) => a[key] === id);

      if (index === -1) {
        response = of(this.createResponse(id, null, index));
      } else {
        const item = storeItems[index];
        response = of(this.createResponse(id, item, index));
      }
    }
    return response.pipe(first());
  }

  updateItemByKey(id: string, value): void {
    const storeItems = this._items.getValue();
    const key = this.schema.key;

    if (id && this.schema && storeItems && value) {
      const updatedItems = storeItems.map(e => {
        if (e[key] === id) {
          return value;
        }
        return e;
      });

      this._items.next(updatedItems);
    }
  }

  // STORE EVENTS
  // These provide a level of abstraction from the
  // itemSelection service. On an event the store
  // will take the key and then return the full obj

  onHover(): Observable<any> {
    return this.itemSelection.$itemHover.pipe(
      mergeMap(key => this.getItemByKey(key))
    );
  }

  onSelect(): Observable<any> {
    return this.itemSelection.$itemSelect.pipe(
      mergeMap(key => this.getItemByKey(key))
    );
  }

  onFocus(): Observable<any> {
    return this.itemSelection.$itemFocus.pipe(
      mergeMap(key => this.getItemByKey(key))
    );
  }
}
