/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { RouterModule } from '@angular/router';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faUser } from '@fortawesome/free-regular-svg-icons';
import { faKey } from '@fortawesome/free-solid-svg-icons';
import { AppRoutingModule } from 'app/app-routing.module';
import { NavigationMenuModule } from 'app/ui/navigation-menu/navigation-menu.module';

import { LoginComponent } from './containers/login/login.component';
import { NavigationComponent } from './containers/navigation/navigation.component';
import { ResetPasswordComponent } from './containers/reset-password/reset-password.component';

@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule,
    RouterModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    MatInputModule,
    MatButtonModule,
    NavigationMenuModule,
  ],
  declarations: [
    NavigationComponent,
    LoginComponent,
    ResetPasswordComponent,
  ]
})
export class CoreModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faKey, faUser);
  }
}
