/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DisplayResource } from 'app/core/models/visualisations/display-resource';
import * as moment from 'moment';

export class ObjectUtils {
  /**
   * Extract a property value form an object by a given path
   * @param path property path
   * @param object object to extract value from
   */
  public static getProperty(path: string, object: any) {
    if (!path) return '';
    const parts = path.split('.');
    const length = parts.length;

    let property = object || this;

    for (let i = 0; i < length; i++) {
      property = property[parts[i]];
    }

    return property;
  }


  /**
   * Parse the json for a resource into a DisplayResource object
   *
   * @static
   * @param {*} json the json of the resource
   * @returns {DisplayResource}
   */
  public static parseResource(json): DisplayResource {
    return JSON.parse(JSON.stringify(json), (key, value) => {
      if (typeof value === 'string') {
        const date = moment(value, moment.ISO_8601);
        if (date.isValid()) {
          return date.toDate();
        }
      }
      return value;
    });
  }

  /**
   * Parse the json for a resource into a generic object
   *
   * @static
   * @param {*} json the json of the resource
   * @returns {Object}
   */
  public static parseJson(json): any {
    return JSON.parse(JSON.stringify(json), (key, value) => {
      if (typeof value === 'string') {
        const date = moment(value, moment.ISO_8601);
        if (date.isValid()) {
          return date.toDate();
        }
      }
      return value;
    });
  }

}
