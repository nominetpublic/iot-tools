/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { timeDay, timeHour, timeMinute, timeMonth, timeSecond, timeWeek, timeYear } from 'd3-time';
import { timeFormat } from 'd3-time-format';

export class D3Utils {
    static d3TimeFormat(date) {
        const formatMillisecond = timeFormat('.%L'),
            formatSecond = timeFormat(':%Ss'),
            formatMinute = timeFormat('%H:%M'),
            formatHour = timeFormat('%H:%M'),
            formatDay = timeFormat('%a %d'),
            formatWeek = timeFormat('%b %d'),
            formatMonth = timeFormat('%B'),
            formatYear = timeFormat('%Y');

        return (timeSecond(date) < date ? formatMillisecond
            : timeMinute(date) < date ? formatSecond
                : timeHour(date) < date ? formatMinute
                    : timeDay(date) < date ? formatHour
                        : timeMonth(date) < date ? (timeWeek(date) < date ? formatDay : formatWeek)
                            : timeYear(date) < date ? formatMonth
                                : formatYear)(date);
    }
}
