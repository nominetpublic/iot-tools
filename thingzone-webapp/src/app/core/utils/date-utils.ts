/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import * as moment from 'moment';

export class DateUtils {

    /**
     * Calculate the time delta string relative to the date
     *
     * @static
     * @param {moment.Moment} relativeTo
     * @param {string} delta
     * @param {number} repeat
     * @returns {moment.Moment} The relative date with the delta applied
     * @memberof DateUtils
     */
    public static calcTimeDelta(relativeTo: moment.Moment, delta: string, repeat = 1): moment.Moment {
        const output = relativeTo.clone();
        const duration = this.findDuration(delta, repeat);
        return output.subtract(duration);
    }

    /**
     * Calculate the time delta string in the future relative to the date
     *
     * @static
     * @param {moment.Moment} relativeTo
     * @param {string} delta
     * @param {number} repeat
     * @returns {moment.Moment} The relative date with the delta applied
     * @memberof DateUtils
     */
    public static calcTimeDeltaForwards(relativeTo: moment.Moment, delta: string, repeat = 1): moment.Moment {
        const output = relativeTo.clone();
        const duration = this.findDuration(delta, repeat);
        return output.add(duration);
    }

    /**
     * Convert a time difference regex into a moment duration
     *
     * @param {string} delta the time interval
     * @param {number} repeat number of times to repeat the interval
     * @returns {moment.Duration}
     */
    public static findDuration(delta: string, repeat: number): moment.Duration {
        const duration = {};
        const seconds = delta.match(/(\d+)s/);
        if (seconds != null) {
            const amount = Number(seconds[1]);
            duration['seconds'] = (amount * repeat);
        }
        const minutes = delta.match(/(\d+)m/);
        if (minutes != null) {
            const amount = Number(minutes[1]);
            duration['minutes'] = (amount * repeat);
        }
        const hours = delta.match(/(\d+)h/);
        if (hours != null) {
            const amount = Number(hours[1]);
            duration['hours'] = (amount * repeat);
        }
        const days = delta.match(/(\d+)d/);
        if (days != null) {
            const amount = Number(days[1]);
            duration['days'] = (amount * repeat);
        }
        const weeks = delta.match(/(\d+)w/);
        if (weeks != null) {
            const amount = Number(weeks[1]);
            duration['weeks'] = (amount * repeat);
        }
        const months = delta.match(/(\d+)M/);
        if (months != null) {
            const amount = Number(months[1]);
            duration['months'] = (amount * repeat);
        }
        const years = delta.match(/(\d+)y/);
        if (years != null) {
            const amount = Number(years[1]);
            duration['years'] = (amount * repeat);
        }
        return moment.duration(duration);
    }


    static formatDate(date: string): string {
        return moment(date).format('ll');
    }

    static formatTime(date: string): string {
        return moment(date).format('H:m, MMM D');
    }

}
