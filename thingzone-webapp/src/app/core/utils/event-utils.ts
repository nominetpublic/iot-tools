/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { SearchTerm } from '../models/common/search-term';

export class EventUtils {
    public static severityRange: Array<Array<number>> = [[0, 0.3], [0.3, 0.6], [0.6, 1.0]];
    public static severityColors = [
        {
            name: 'Low',
            value: 'rgb(143, 140, 136)'
        },
        {
            name: 'Medium',
            value: 'rgb(247, 163, 7)'
        },
        {
            name: 'High',
            value: 'rgb(216, 0, 11)'
        }
    ];

    public static getFromSearch(searchTerms: SearchTerm[], searchKey: string, objectKey?: string): any {
        if (searchTerms == null) {
            return;
        }
        if (objectKey == null) {
            objectKey = searchKey;
        }
        let values: {} = null;
        searchTerms.forEach(block => {
            if (block.property === searchKey) {
                if (block.values != null) {
                    values = { [objectKey]: block.values };
                }
            }
        });
        return values;
    }

    public static getSeverity(searchTerms: SearchTerm[]): { severity: Array<number> } {
        const currentSeverity = EventUtils.getFromSearch(searchTerms, 'severity');
        if (currentSeverity == null || currentSeverity['severity'].length !== 1) {
            return;
        }
        switch (currentSeverity['severity'][0]) {
            case 'Low':
                return { severity: EventUtils.severityRange[0] };
            case 'Medium':
                return { severity: EventUtils.severityRange[1] };
            case 'High':
                return { severity: EventUtils.severityRange[2] };
            default:
                return;
        }
    }

    public static getSeverityName(severity: number): string {
        if (severity < EventUtils.severityRange[0][1]) {
            return 'Low';
        } else if (severity >= EventUtils.severityRange[1][0] && severity <= EventUtils.severityRange[1][1]) {
            return 'Medium';
        } else if (severity > EventUtils.severityRange[2][0]) {
            return 'High';
        }
    }
}
