/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { SearchTerm } from 'app/core/models/common/search-term';
import { DataQueryType } from 'app/core/models/queries/data-query';
import { SavedSearch } from 'app/ui/search/models/active-search';

import { SecondaryQuery } from './secondary-query';

export interface QuerySource {
  /**
   * The unique ID of the component
   */
  readonly id: number;

  /**
   * The title of this data source
   */
  name?: string;

  /**
   * The type of query to use
   */
  query: DataQueryType;

  /**
   * Any secondary queries for the combination api
   */
  secondaryQueries?: SecondaryQuery[];

  /**
   * The device filter
   */
  deviceFilter: SavedSearch;

  /**
   * Whether each device should be individually selectable
   */
  selectDevice?: boolean;

  /**
   * Not used if this is a device only query
   */
  streamFilter?: string;

  /**
   * Any additional configuration options to use
   */
  configuration?: DataQueryConfiguration;
}

export interface DataQueryConfiguration {
  mapConfiguration?: MapConfiguration;
  valueConfiguration?: ValueConfiguration;
  aggregationQuery?: AggQueryConfiguration;
  locationQuery?: LocationQueryConfiguration;
  eventQuery?: EventQueryConfiguration;
  imageQuery?: ImageQueryConfiguration;
}

export interface MapConfiguration {
  colour?: string;
  opacity?: number;
  outlineWidth?: number;
  outlineColour?: string;
  radius?: number;
  icon?: string;
  timeHover?: boolean;
  nameHover?: boolean;
  focusMap?: boolean;
  heatMap?: boolean;
}

export interface AggQueryConfiguration {
  math?: string;
  minMax?: boolean;
  interval?: string;
  groupByStream?: boolean;
  size?: number;
  useCurrentTime?: boolean;
}

export interface ImageQueryConfiguration {
  burstInterval?: number;
  tags?: string[];
  groupBy?: string;
  size?: number;
}

export interface ValueConfiguration {
  showLabel?: boolean;
  units?: string;
  valueColour?: string;
  valueSize?: number;
  precision?: number;
  type?: string;
}

export interface LocationQueryConfiguration {
  singlePoint?: boolean;
  showAsLine?: boolean;
  timeFromCurrent?: string;
  sizeFromCurrent?: number;
  forward?: boolean;
}

export interface EventQueryConfiguration {
  size?: number;
  timeLimit?: string;
  eventFilter?: SavedSearch;
  forward?: boolean;
  yValue?: boolean;
  valueName?: string;
  interval?: string;
}

export interface StreamFilter {
  terms: SearchTerm[];
  devices?: string[];
}
