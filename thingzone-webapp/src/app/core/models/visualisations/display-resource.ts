/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { VisualisationContent } from './visualisation-content';
import { WidgetContent } from './widget-content';

export interface DisplayResource {
  /**
   * The entity name, can be left empty to autogenerate on new items
   */
  key?: string;

  /**
   * The entity type = 'visualisation' or 'widget'
   */
  type: string;

  /**
   * The entity name, used as the visualisation title
   */
  name?: string;

  /**
   * The resource name
   */
  resourceName: string;

  /**
   * The display configuration data
   */
  content: VisualisationContent | WidgetContent;
}
