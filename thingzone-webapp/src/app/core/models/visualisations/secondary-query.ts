/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DataQueryType, SecondaryQueryType } from '../queries/data-query';

export interface SecondaryQuery {
  /**
   * The unique ID of the component
   */
  readonly id: number;
  /**
   * The name of the stream to link to
   */
  streamName: string;

  /**
   * The type of query to use
   */
  query: DataQueryType;

  /**
   * The type of secondary query
   */
  secondaryType: SecondaryQueryType;

  /**
   * Any additional configuration options to use
   */
  configuration?: SecondaryQueryConfiguration;
}

export interface SecondaryQueryConfiguration {
  mapColourConfiguration?: MapColourConfiguration;
  mapHoverConfiguration?: MapHoverConfiguration;
}

export interface MapColourConfiguration {
  stops: [{ number: number, colour: string }];
  interval: boolean; // exponential as default
  radius?: number;
  legend?: boolean;
  legendTitle?: string;
}

export interface MapHoverConfiguration {
  label: string;
}
