/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { VisualisationComponent } from './visualisation-component';

export interface VisualisationContent {

  /**
   * The display components used by this visualisation
   */
  components: VisualisationComponent[];

  /**
   * The key of the visualisation ordered before this one
   *
   * @type {number}
   */
  after?: string;

  /**
   * The configuration of the visualisation
   */
  configuration: {
    /**
     * The key of the visualisation layout being used
     */
    layout?: string;

    /**
     * The default from date for the display context
     */
    from?: Date;

    /**
     * The default to date for the display context, leave blank to use now
     */
    to?: Date;

    /**
     * The starting slider time, if empty default to from.
     */
    current?: Date;
  };
}
