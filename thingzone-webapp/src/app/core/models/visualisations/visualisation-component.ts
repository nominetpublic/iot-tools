/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { QuerySource } from './display-data-source';

export interface VisualisationComponent {
  /**
   * The unique ID of the component
   */
  readonly id: number;

  /**
   * The component type; e.g. 'map'.
   */
  type: VisualisationComponentType;

  /**
   * The title of this component
   */
  name: string;

  /**
   * The id of the layout component this is added to
   */
  layoutComponent: number;

  /**
   * List of data inputs for this component
   */
  sources: QuerySource[];

  /**
   * Component configuration - contents dependent on type;
   */
  configuration: MapDisplayConfiguration | GraphDisplayConfiguration;
}

export enum VisualisationComponentType {
  MAP = 'MAP',
  IMAGES = 'IMAGES',
  EVENTS = 'EVENTS',
  VALUE = 'VALUE',
  LINEGRAPH = 'LINEGRAPH',
  BARGRAPH = 'BARGRAPH',
  GAUGE = 'GAUGE',
  PIECHART = 'PIECHART',
  MAPCOMBINATION = 'MAPCOMBINATION',
}

export interface MapDisplayConfiguration {
  mapCentre?: [number, number];
  longitude?: number;
  latitude?: number;
  mapZoom?: number;
  pitch?: number;
  bearing?: number;
  theme?: string;
  annotations?: string[];
}

export interface GraphDisplayConfiguration {
  theme?: string;
  hideXAxis?: boolean;
  xLabel?: string;
  hideYAxis?: boolean;
  yLabel?: string;
  size?: number;
  min?: number | Date;
  max?: number | Date;
  formatType?: string;
  displayType?: string;
  stops?: [{ low: number, high: number, name: string }];
}
