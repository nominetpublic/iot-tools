/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { QuerySource } from './display-data-source';
import { GraphDisplayConfiguration, MapDisplayConfiguration } from './visualisation-component';

export interface WidgetContent {
  /**
   * List of data inputs for this widget
   * @type {QuerySource[]}
   * @memberof WidgetContent
   */
  sources: QuerySource[];

  /**
   * The display type of the widget
   * @type {string}
   * @memberof WidgetContent
   */
  type: string;

  /**
   * The key of the widget ordered before this one
   *
   * @type {number}
   */
  after?: string;

  /**
   * The configuration for the widget
   * @type {(MapDisplayConfiguration | GraphDisplayConfiguration)}
   * @memberof WidgetContent
   */
  configuration: MapDisplayConfiguration | GraphDisplayConfiguration;

  /**
   * The time relative to the current date to define a range
   * @type {string}
   * @memberof WidgetContent
   */
  timeDelta?: string;

  /**
   * The interval in seconds between updates
   *
   * @type {number}
   * @memberof WidgetContent
   */
  interval?: number;

}
