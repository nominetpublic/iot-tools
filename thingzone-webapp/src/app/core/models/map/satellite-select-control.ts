/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export class SatelliteSelectControl {
  map: any;
  container: HTMLDivElement;
  satelliteButton: any;
  satelliteStyle: string;
  defaultStyle: string;
  satellite: boolean;

  constructor(
    defaultStyle: string,
    satelliteTheme: string,
  ) {
    this.defaultStyle = defaultStyle;
    this.satellite = false;
    this.container = document.createElement('div');
    this.container.className = 'mapboxgl-ctrl-group mapboxgl-ctrl';
    this.satelliteButton = this.createButton('mapboxgl-satellite');
    this.createSpan('mapboxgl-satellite-icon', this.satelliteButton);
    this.satelliteStyle = satelliteTheme;
  }

  onAdd(map) {
    this.map = map;
    return this.container;
  }

  onRemove() {
    this.container.parentNode.removeChild(this.container);
    this.map = undefined;
  }

  createButton(className: string): HTMLButtonElement {
    const button = window.document.createElement('button');
    button.className = className;
    button.type = 'button';
    button.textContent = 'Sat';
    button.title = 'Switch to satellite view';
    button.addEventListener('click', (e) => {
      if (this.satellite) {
        this.map.setStyle(this.defaultStyle);
        this.satellite = false;
      } else {
        this.map.setStyle(this.satelliteStyle);
        this.satellite = true;
      }
      button.textContent = (this.satellite) ? 'Map' : 'Sat';
      button.title = (this.satellite) ? 'Switch to map view' : 'Switch to satellite view';
    });
    this.container.appendChild(button);
    return button;
  }

  createSpan(className: string, button: HTMLButtonElement): HTMLSpanElement {
    const span = window.document.createElement('span');
    span.className = className;
    button.appendChild(span);
    return span;
  }

}
