/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DataApiService } from 'app/core/services/api/data-api.service';
import { DisplayContext } from 'app/core/services/visualisation/display-context.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { QuerySource } from '../visualisations/display-data-source';
import { VisualisationComponentType } from '../visualisations/visualisation-component';
import { DataQuery, DataQueryType, StreamType } from './data-query';

export class MetricsQuery implements DataQuery {
  type = DataQueryType.METRICS;
  scope = 'range';

  constructor(
    private dataApi: DataApiService,
  ) {
  }

  runQuery(source: QuerySource, context: DisplayContext, displayType: VisualisationComponentType): Observable<any> {
    switch (displayType) {
      case VisualisationComponentType.BARGRAPH:
      case VisualisationComponentType.GAUGE:
      case VisualisationComponentType.PIECHART:
        return this.dataApi.getMetricsList(source, context.range).pipe(
          map((res: any) => this.aggregateResults(res)));
      default:
        return this.dataApi.getMetricsList(source, context.range).pipe(
          map((res: any) => res));
    }
  }
  aggregateResults(data: any) {
    const stringValues = {};
    data.forEach(stream => {
      if (stream.type === StreamType.STRING) {
        stream.values.forEach(element => {
          const text = element.value_text;
          if (text in stringValues) {
            stringValues[text] = stringValues[text] + 1;
          } else {
            stringValues[text] = 0;
          }
        });
      }
    });

    if (Object.keys(stringValues).length > 0) {
      const seriesData = Object.keys(stringValues)
        .map(value => ({
          name: value,
          value: stringValues[value],
        }));
      return seriesData;
    }
  }
}
