/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DeviceApiService } from 'app/core/services/api/device-api.service';
import { DisplayContext } from 'app/core/services/visualisation/display-context.service';
import { EMPTY, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { QuerySource } from '../visualisations/display-data-source';
import { VisualisationComponentType } from '../visualisations/visualisation-component';
import { DataQuery, DataQueryType } from './data-query';

export class DeviceQuery implements DataQuery {
  type = DataQueryType.DEVICE;
  scope = ['range'];

  constructor(
    private deviceApi: DeviceApiService,
  ) { }

  runQuery(source: QuerySource, context: DisplayContext, displayType: VisualisationComponentType): Observable<any> {
    const terms = source.deviceFilter != null ? source.deviceFilter.terms : [];
    let observable: Observable<any> = EMPTY;
    if (displayType === VisualisationComponentType.PIECHART) {
      if (source.configuration.valueConfiguration != null && source.configuration.valueConfiguration.type === 'status') {
        observable = this.deviceApi.deviceList(terms, 0, 1000).pipe(map(res => {
          return this.getStatus(res.devices);
        }));
      } else {
        observable = this.deviceApi.deviceFacetSearch().pipe(map(res => {
          const facet = res.facets.find(item => item.property === 'type');
          if (facet == null || facet.options.length === 0) { return []; }
          return facet.options.map(option => ({ name: option.value, value: option.count }));
        }));
      }
    } else {
      observable = this.deviceApi.deviceList(terms, 0, 1000).pipe(map(res => {
        if (displayType === VisualisationComponentType.VALUE) {
          const values: string[] = [res.total];
          return values;
        }
        return res;
      }));
    }
    return observable;
  }

  getStatus(devices) {
    const status = {
      unknown: 0,
      warning: 0,
      bad: 0,
      good: 0
    };
    devices.forEach(element => {
      if (element.status === 0) {
        status['bad'] += 1;
      }
      if (element.status > 0 && element.status < 1) {
        status['warning'] += 1;
      } else if (element.status === 1) {
        status['good'] += 1;
      } else {
        status['unknown'] += 1;
      }
    });
    return Object.keys(status).map(key => ({ name: key, value: status[key] }));
  }
}

