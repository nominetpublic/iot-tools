/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DataApiService } from 'app/core/services/api/data-api.service';
import { DateRange, DisplayContext } from 'app/core/services/visualisation/display-context.service';
import { DateUtils } from 'app/core/utils/date-utils';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { QuerySource } from '../visualisations/display-data-source';
import { VisualisationComponentType } from '../visualisations/visualisation-component';
import { DataQuery, DataQueryType } from './data-query';

export class AggregationQuery implements DataQuery {
  type: DataQueryType.AGGREGATION;
  scope = ['range', 'current'];

  constructor(
    private dataApi: DataApiService,
  ) { }

  runQuery(source: QuerySource, context: DisplayContext, displayType: VisualisationComponentType): Observable<any> {
    const config = source.configuration;
    const agg = config.aggregationQuery != null && config.aggregationQuery.math ? config.aggregationQuery.math : 'avg';
    const dates: DateRange = config.aggregationQuery != null && config.aggregationQuery.useCurrentTime ?
      { from: context.range.from, to: context.current } : context.range;
    const interval = (config.aggregationQuery != null && config.aggregationQuery.interval != null) ?
      config.aggregationQuery.interval : '1h';

    switch (displayType) {
      case VisualisationComponentType.LINEGRAPH:
        return this.dataApi.getMetricsAggregation(source, dates).pipe(
          map(res => {
            return this.dataApi.getGraphData(res, agg, source.name);
          }));
      case VisualisationComponentType.BARGRAPH:
      case VisualisationComponentType.GAUGE:
      case VisualisationComponentType.PIECHART:
        if (config.aggregationQuery != null && config.aggregationQuery.groupByStream) {
          // If grouped by stream then return the most recent grouped data
          dates.to = context.current;
          dates.from = DateUtils.calcTimeDelta(moment(dates.to), interval).toDate();
        }
        return this.dataApi.getMetricsAggregation(source, dates).pipe(
          map(res => {
            if (res.length > 0) {
              if (res.length === 1 && res[0].merged === true) {
                return this.aggregateIntervalGroups(res, dates, interval, agg);
              } else {
                return this.aggregateStreamGroups(res, agg);
              }
            }
          }));
      case VisualisationComponentType.VALUE:
        return this.dataApi.getMetricsAggregation(source, dates).pipe(
          map(res => {
            const values: string[] = [];
            const data = res[0];

            if (data.values != null && data.values.length > 0) {
              const last = data.values[data.values.length - 1];
              values.push(last[agg]);
            }
            return values;
          }));
    }
  }

  aggregateStreamGroups(aggregationData, aggregationType = 'avg'): any[] {
    const seriesData = [];
    aggregationData.forEach(stream => {
      if (stream.values.length > 0) {
        const lastValue = stream.values.pop();
        const value = lastValue[aggregationType] != null ? lastValue[aggregationType] : 0;
        seriesData.push({ name: stream.streamKey, value: value });
      } else {
        seriesData.push({ name: stream.streamKey, value: 0 });
      }
    });
    return seriesData;
  }

  aggregateIntervalGroups(aggregationData, dates, interval, aggregationType = 'avg'): any[] {
    const timeHistogram = {};
    let currentDate = moment(dates.to);
    let prevDate = DateUtils.calcTimeDelta(moment(dates.to), interval);

    while (moment(dates.from).isBefore(prevDate)) {
      currentDate = prevDate;
      prevDate = DateUtils.calcTimeDelta(moment(currentDate), interval);
      timeHistogram[currentDate.toISOString()] = 0;
    }

    aggregationData.forEach(stream => {
      if (stream.values.length > 0) {
        stream.values.forEach(element => {
          const value = element[aggregationType] != null ? element[aggregationType] : 0;
          timeHistogram[moment(element['timestamp']).toISOString()] = value;
        });
      }
    });

    const seriesData = Object.keys(timeHistogram).sort((a, b) => moment(a).diff(b))
      .map(time => ({
        name: moment(time).format('MMM D, HH:mm'),
        value: timeHistogram[time],
        timestamp: moment(time).toISOString()
      }));
    return seriesData;
  }

}
