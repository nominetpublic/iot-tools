/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DeviceApiService } from 'app/core/services/api/device-api.service';
import { DisplayContext } from 'app/core/services/visualisation/display-context.service';
import { EMPTY, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { QuerySource } from '../visualisations/display-data-source';
import { DataQuery, DataQueryType } from './data-query';

export class DeviceLocationQuery implements DataQuery {
  type = DataQueryType.DEVICELOCATION;
  scope = ['boundingPolygon', 'current'];

  constructor(
    private deviceApi: DeviceApiService,
  ) { }

  runQuery(source: QuerySource, context: DisplayContext): Observable<any> {
    if (!context.boundingPolygon) return EMPTY;

    const terms = source.deviceFilter != null ? source.deviceFilter.terms : [];
    // if individual devices are selectable then don't limit query to map bounds
    const polygon = source.selectDevice != null && source.selectDevice ? [] : [context.boundingPolygon];
    if (source.streamFilter) {
      const time = context.current;
      return this.deviceApi.deviceLocation(terms, polygon, source.streamFilter, time).pipe(map((res: any) => {
        return res;
      }));
    }
    return this.deviceApi.deviceLocation(terms, polygon).pipe(map((res: any) => {
      return res;
    }));
  }
}

