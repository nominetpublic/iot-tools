/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DataApiService } from 'app/core/services/api/data-api.service';
import { DisplayContext } from 'app/core/services/visualisation/display-context.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { QuerySource } from '../visualisations/display-data-source';
import { VisualisationComponentType } from '../visualisations/visualisation-component';
import { DataQuery, DataQueryType } from './data-query';

export class ImagesQuery implements DataQuery {
  type = DataQueryType.IMAGES;
  scope = 'range';

  constructor(
    private dataApi: DataApiService,
  ) { }

  runQuery(source: QuerySource, context: DisplayContext, displayType: VisualisationComponentType): Observable<any> {
    return this.dataApi.getImages(source, context.range).pipe(
      map((res: any) => {
        if (displayType === VisualisationComponentType.EVENTS) {
          return this.getBubbleGraphData(res, source.name);
        } else {
          const images = res[0].values.reduce((acc, val) => {
            acc.push(val.images);
            return acc;
          }, []);
          return images;
        }
      })
    );
  }

  getBubbleGraphData(data, dataSourceName): any[] {
    let seriesData = [];
    data.forEach(stream => {
      seriesData = seriesData.concat(stream.values.map(burst => {
        if (burst.images.length > 0) {
          const firstImage = burst.images[0];
          const tags = firstImage.tags ? firstImage.tags.join(', ') : '';
          return {
            'name': tags,
            'x': new Date(firstImage.timestamp),
            'y': 0.5,
            'r': 10,
            'imageRef': firstImage.imageRef
          };
        }
      }));
    });

    return [{ name: dataSourceName, series: seriesData }];
  }

}
