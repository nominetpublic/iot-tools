/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DataApiService } from 'app/core/services/api/data-api.service';
import { DateRange, DisplayContext } from 'app/core/services/visualisation/display-context.service';
import { DateUtils } from 'app/core/utils/date-utils';
import { EventUtils } from 'app/core/utils/event-utils';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { QuerySource } from '../visualisations/display-data-source';
import {
  GraphDisplayConfiguration,
  MapDisplayConfiguration,
  VisualisationComponentType,
} from '../visualisations/visualisation-component';
import { DataQuery, DataQueryType } from './data-query';

export class EventsQuery implements DataQuery {
  type = DataQueryType.EVENTS;
  scope = ['range', 'current'];

  constructor(
    private dataApi: DataApiService,
  ) { }

  runQuery(source: QuerySource, context: DisplayContext, displayType: VisualisationComponentType,
    configuration: MapDisplayConfiguration | GraphDisplayConfiguration): Observable<any> {
    const dates: DateRange = { from: context.range.from, to: context.range.to };
    if (displayType === VisualisationComponentType.VALUE) {
      if (source.configuration.eventQuery == null) {
        source.configuration.eventQuery = {};
      }
      source.configuration.eventQuery['size'] = 1;
      if (source.configuration.eventQuery.timeLimit != null) {
        dates.to = context.current;
      }
    }
    let yValue = false;
    if (source.configuration.eventQuery != null && source.configuration.eventQuery.yValue != null) {
      yValue = source.configuration.eventQuery.yValue;
    }
    let valueName = null;
    if (source.configuration.eventQuery != null && source.configuration.eventQuery.valueName != null) {
      valueName = source.configuration.eventQuery.valueName;
    }

    switch (displayType) {
      case VisualisationComponentType.EVENTS:
        return this.dataApi.getEvents(source, dates).pipe(
          map((res: any) => this.getBubbleGraphData(res, source.name, yValue, valueName))
        );
      case VisualisationComponentType.VALUE:
        return this.dataApi.getEvents(source, dates).pipe(
          map((res: any) => {
            const values: string[] = [res[0].total];
            return values;
          }));
      case VisualisationComponentType.BARGRAPH:
      case VisualisationComponentType.GAUGE:
      case VisualisationComponentType.PIECHART:
        const config = configuration as GraphDisplayConfiguration;
        let interval = null;
        if (source.configuration.eventQuery != null && source.configuration.eventQuery.interval != null) {
          interval = source.configuration.eventQuery.interval;
        }
        let size: number = null;
        if (displayType === VisualisationComponentType.BARGRAPH
          && source.configuration.aggregationQuery != null && source.configuration.aggregationQuery.size != null) {
          size = source.configuration.aggregationQuery.size;
        }
        return this.dataApi.getEvents(source, dates).pipe(
          map((res: any) => {
            if (config.displayType === 'severity') {
              return this.graphSeverity(res);
            } else {
              return this.graphIntervalGroups(res, dates, interval, size);
            }
          })
        );
    }
  }

  getBubbleGraphData(eventData, dataSourceName, yValue, valueName): any[] {
    let seriesData = [];
    eventData.forEach(stream => {
      seriesData = seriesData.concat(stream.values.map(event => ({
        'name': event.name,
        'x': new Date(event.timestamp),
        'y': yValue ? event.value : 0.5,
        'r': 10,
        'valueName': valueName,
        'displayY': yValue,
      })));
    });

    return [{ name: dataSourceName, series: seriesData }];
  }

  graphIntervalGroups(data, dates: DateRange, interval: string = null, size: number = 10): any[] {
    if (interval == null) {
      interval = '1h';
    }

    const timeHistogram = {};
    data.forEach(stream => {
      let currentDate = moment(dates.to);
      let prevDate = DateUtils.calcTimeDelta(moment(dates.to), interval);
      timeHistogram[currentDate.toISOString()] = 0;

      for (const event of stream.values) {
        const eventTime = moment(event.timestamp);
        if (eventTime.isAfter(prevDate) && eventTime.isBefore(currentDate)) {
          timeHistogram[currentDate.toISOString()]++;
        } else {
          while (eventTime.isBefore(prevDate)) {
            currentDate = prevDate;
            prevDate = DateUtils.calcTimeDelta(moment(currentDate), interval);
            timeHistogram[currentDate.toISOString()] = 0;
            if (eventTime.isAfter(prevDate) && eventTime.isBefore(currentDate)) {
              timeHistogram[currentDate.toISOString()]++;
              break;
            }
          }
        }
      }
    });
    const seriesData = Object.keys(timeHistogram).sort((a, b) => moment(a).diff(b))
      .map(time => ({ name: moment(time).format('MMM D, HH:mm'), value: timeHistogram[time] }));
    if (size != null) {
      return seriesData.slice(0, size);
    }
    return seriesData;
  }


  graphSeverity(data): any {
    const severityHistogram = {
      Low: 0,
      Medium: 0,
      High: 0
    };

    data.forEach(stream => {
      for (const event of stream.values) {
        const severityName = EventUtils.getSeverityName(event.severity);
        severityHistogram[severityName] = severityHistogram[severityName] + 1;
      }
    });
    const seriesData = Object.keys(severityHistogram)
      .map(severity => ({ name: severity, value: severityHistogram[severity] }));
    return seriesData;
  }

}
