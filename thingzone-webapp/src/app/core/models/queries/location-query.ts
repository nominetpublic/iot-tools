/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DataApiService } from 'app/core/services/api/data-api.service';
import { DisplayContext } from 'app/core/services/visualisation/display-context.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { QuerySource } from '../visualisations/display-data-source';
import { DataQuery, DataQueryType } from './data-query';

export class LocationQuery implements DataQuery {
  type = DataQueryType.LOCATION;
  scope = ['boundingPolygon', 'current'];

  constructor(
    private dataApi: DataApiService,
  ) {
  }

  runQuery(source: QuerySource, context: DisplayContext, displayType: string): Observable<any> {
    let dates = { from: context.range.from, to: context.current };
    if (source.configuration != null
      && source.configuration.locationQuery != null
      && source.configuration.locationQuery.forward) {
      // date range needs to change when going forward in time
      dates = { from: context.current, to: context.range.to };
    }

    const mapBounds = (source.configuration != null
      && source.configuration.mapConfiguration != null
      && source.configuration.mapConfiguration.focusMap) ?
      [] : [context.boundingPolygon];
    if (source.secondaryQueries != null && source.secondaryQueries.length > 0) {
      return this.dataApi.getCombinationGeo(source, dates, mapBounds).pipe(map((res: any) => {
        return this.getMapStreamData(res);
      }));
    } else {
      return this.dataApi.getMetricsGeo(source, dates, mapBounds).pipe(map((res: any) => {
        return this.getMapStreamData(res);
      }));
    }
  }

  getMapStreamData(data: any[]): any {
    const features = data.reduce((acc, val) => {
      if (val.geojson.features.length > 0) {
        return acc.concat(val.geojson.features);
      }
      return acc;
    }, []);
    return {
      geojson: {
        type: 'FeatureCollection',
        features: features
      }
    };
  }
}

