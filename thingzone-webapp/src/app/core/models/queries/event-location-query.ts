/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DisplayContext } from 'app/core/services/visualisation/display-context.service';
import { map } from 'rxjs/operators';

import { DataApiService } from '../../services/api/data-api.service';
import { QuerySource } from '../visualisations/display-data-source';
import { DataQuery, DataQueryType } from './data-query';

export class EventLocationQuery implements DataQuery {
  type = DataQueryType.EVENTLOCATION;
  scope = ['boundingPolygon', 'range', 'current'];

  constructor(
    private dataApi: DataApiService,
  ) { }

  runQuery(source: QuerySource, context: DisplayContext) {
    let dates = { from: context.range.from, to: context.current };
    if (source.configuration != null
      && source.configuration.eventQuery != null
      && source.configuration.eventQuery.forward) {
      // date range needs to change when going forward in time
      dates = { from: context.current, to: context.range.to };
    }

    return this.dataApi.getEventsGeo(source, dates, [context.boundingPolygon]).pipe(
      map((res: any) => res[0])
    );
  }
}
