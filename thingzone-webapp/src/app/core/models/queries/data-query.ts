/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DisplayContext } from 'app/core/services/visualisation/display-context.service';
import { Observable } from 'rxjs';

import { QuerySource } from '../visualisations/display-data-source';
import {
  GraphDisplayConfiguration,
  MapDisplayConfiguration,
  VisualisationComponentType,
} from '../visualisations/visualisation-component';

/**
 * Define queries to display data for visualisations and widgets
 */
export interface DataQuery {
  type: DataQueryType;
  scope: string | string[];

  runQuery(source: QuerySource, context: DisplayContext, displayType: VisualisationComponentType,
    configuration: MapDisplayConfiguration | GraphDisplayConfiguration): Observable<any>;
}

export enum DataQueryType {
  IMAGES = 'IMAGES',
  DEVICELOCATION = 'DEVICELOCATION',
  DEVICE = 'DEVICE',
  LOCATION = 'LOCATION',
  AGGREGATION = 'AGGREGATION',
  METRICS = 'METRICS',
  EVENTS = 'EVENTS',
  EVENTLOCATION = 'EVENTLOCATION',
}

export enum SecondaryQueryType {
  COLOUR = 'COLOUR',
  HOVER = 'HOVER',
}

export enum EventType {
  ALERT = 'ALERT',
  GENERIC = 'GENERIC',
  OBSERVATION = 'OBSERVATION',
}

export enum StreamType {
  INTEGER = 'uk.nominet.iot.model.timeseries.MetricInt',
  DOUBLE = 'uk.nominet.iot.model.timeseries.MetricDouble',
  IMAGE = 'uk.nominet.iot.model.timeseries.Image',
  EVENT = 'uk.nominet.iot.model.timeseries.Event',
  LATLONG = 'uk.nominet.iot.model.timeseries.MetricLatLong',
  JSON = 'uk.nominet.iot.model.timeseries.Json',
  STRING = 'uk.nominet.iot.model.timeseries.MetricString',
  STATUS = 'uk.nominet.iot.model.timeseries.Status',
  CONFIG = 'uk.nominet.iot.model.timeseries.Config',
  INPUTDATAJSON = 'uk.nominet.iot.model.timeseries.InputData',
}
