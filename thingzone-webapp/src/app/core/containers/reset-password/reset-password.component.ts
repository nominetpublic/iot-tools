/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'app/core/services/auth/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  resetPasswordForm = this.formBuilder.group({
    username: ['', Validators.required],
    token: ['', Validators.required],
    newPassword: ['', [Validators.required, Validators.minLength(6)]],
  });
  error = true;
  errorMessage = '';

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    public authService: AuthService,
  ) { }

  ngOnInit() {
    const querySub = this.route.queryParamMap.subscribe(params => {
      const patch = {};
      if (params.has('username')) {
        patch['username'] = params.get('username');
      }
      if (params.has('token')) {
        patch['token'] = params.get('token');
      }
      this.resetPasswordForm.patchValue(patch, { onlySelf: true, emitEvent: false });
    });
    this.subscriptions.push(querySub);

    const formSub = this.resetPasswordForm.valueChanges.subscribe(formValues => {
      this.error = this.resetPasswordForm.invalid;
    });
    this.subscriptions.push(formSub);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  resetPassword() {
    if (!this.resetPasswordForm.invalid) {
      const formValues = this.resetPasswordForm.value;
      this.authService.resetPassword(formValues['username'], formValues['token'], formValues['newPassword']).subscribe(res => {
        this.authService.logout();
        this.login();
      }, (error) => {
        this.error = true;
        this.errorMessage = error;
      });
    }
  }

  login() {
    this.router.navigate([`/login`]);
  }

  clearError() {
    this.error = this.resetPasswordForm.invalid;
    this.errorMessage = '';
  }
}
