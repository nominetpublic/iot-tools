/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, OnInit } from '@angular/core';
import { RoutingLink } from 'app/core/models/common/routing-link';
import { ApiService } from 'app/core/services/api/api.service';
import { AuthService } from 'app/core/services/auth/auth.service';
import { PanelResizeService } from 'app/core/services/panel-resize/panel-resize.service';
import { SessionService } from 'app/core/services/session/session.service';


@Component({
  selector: 'navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  navOpen = false;
  routes: { submenu: string, routes: RoutingLink[] }[] = [
    {
      submenu: 'home',
      routes: [
        { name: 'introduction', title: 'Introduction', icon: 'home', iconlib: 'fas', link: '/home/introduction', usercontext: 'all' },
        { name: 'dashboard', title: 'Dashboard', icon: 'th-large', iconlib: 'fas', link: '/home/overview', usercontext: 'full' }
      ]
    },
    {
      submenu: 'assets',
      routes: [
        { name: 'config', title: 'Asset Config', icon: 'sitemap', iconlib: 'fas', link: '/assets/overview', usercontext: 'full' },
        { name: 'asset', title: 'Asset Status', icon: 'bullseye', iconlib: 'fas', link: '/assets/status', usercontext: 'full' },
      ]
    },
    {
      submenu: 'events',
      routes: [
        {
          name: 'detection', title: 'Event Detection', icon: 'wave-square', iconlib: 'fas', link: '/detect/detection',
          usercontext: 'full'
        },
        { name: 'event', title: 'Events', icon: 'calendar-alt', iconlib: 'far', link: '/detect/events', usercontext: 'full' },
        { name: 'alerts', title: 'Alerts', icon: 'bell', iconlib: 'far', link: '/detect/alerts', usercontext: 'full' },
        {
          name: 'visualisation', title: 'Visualisations', icon: 'chart-area', iconlib: 'fas',
          link: '/detect/visualisations', usercontext: 'full'
        },
      ]
    },
    {
      submenu: 'analysis',
      routes: [
        { name: 'forensics', title: 'Forensics', icon: 'eye', iconlib: 'far', link: '/forensics', usercontext: 'full' },
      ]
    },
  ];
  accountRoutes = [
    {
      submenu: 'account',
      routes: [
        { name: 'admin', title: 'Admin', icon: 'cog', iconlib: 'fas', link: '/admin', func: () => { }, usercontext: 'full' },
        {
          name: 'logout', title: 'Logout', icon: 'user', iconlib: 'fas', link: '/account', func: this.logout.bind(this),
          usercontext: 'all'
        },
      ]
    }
  ];

  constructor(
    private authService: AuthService,
    private panelResize: PanelResizeService,
    private apiService: ApiService,
    private session: SessionService,
  ) { }

  ngOnInit() {
    this.hideMenuFeatures();
  }

  hideMenuFeatures() {
    const menuFeatures = this.apiService.getMenuFeatures();
    Object.keys(menuFeatures).forEach(submenuKey => {
      for (const [key, value] of Object.entries(menuFeatures[submenuKey])) {
        if (value === false) {
          const submenu = this.routes.find(element => element.submenu === submenuKey);
          if (submenu != null) {
            submenu.routes = submenu.routes.filter(item => item.name !== key);
          }
        }
      }
    });
  }

  dispatchEvent() {
    // Fire for the duration of the CSS animation
    const eventFunction = setInterval(() => {
      this.panelResize.dispatch();
    }, 5);
    setTimeout(() => clearInterval(eventFunction), 160);
  }

  toggleNav() {
    this.navOpen = !this.navOpen;
    this.dispatchEvent();
  }

  closeNav() {
    if (this.navOpen) this.navOpen = false;
    this.dispatchEvent();
  }

  logout() {
    this.authService.logout();
  }
}
