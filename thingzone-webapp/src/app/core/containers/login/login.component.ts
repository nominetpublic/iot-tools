/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'app/core/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']

})
export class LoginComponent implements OnInit {

  DEFAULT_URL = '/';
  loginMessage = 'SIGN IN';
  error = '';

  loginForm = this.formBuilder.group({
    username: [],
    password: [],
  });

  constructor(
    public authService: AuthService,
    public router: Router,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    // If the user is already logged in, redirect them
    this.authService.hardLoginCheck().subscribe(loggedIn => {
      if (loggedIn === true) this.router.navigate([this.DEFAULT_URL]);
    });
  }

  login() {
    this.startProgress();
    const formValues = this.loginForm.value;

    this.authService.login(formValues['username'], formValues['password']).subscribe(redirectURL => {
      // Redirect the user
      this.router.navigate([redirectURL]).then(null, err => {
        // Redirect failed, retry to '/'
        return this.router.navigate([this.DEFAULT_URL]);
      }).then(() => {
        this.authService.loginComplete();
      }, err => {
        // Redirect still failed, critical unknown error
        this.error = 'Internal App Error - Please Contact Nominet';
        this.endProgress();
      });
    }, err => {
      this.error = err;
      this.endProgress();
    });
  }

  clearError() {
    this.error = '';
  }

  startProgress() {
    this.error = '';
    this.loginMessage = 'SIGNING IN...';
  }

  endProgress() {
    this.loginMessage = 'SIGN IN';
  }
}
