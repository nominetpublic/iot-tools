/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faSave } from '@fortawesome/free-regular-svg-icons';
import { faTrash, faUpload } from '@fortawesome/free-solid-svg-icons';

import { JsonEditorModule } from '../json-editor/json-editor.module';
import { FileResourceComponent } from './file-resource/file-resource.component';
import { ImageResourceComponent } from './image-resource/image-resource.component';
import { JsonResourceComponent } from './json-resource/json-resource.component';

@NgModule({
  imports: [
    CommonModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    JsonEditorModule,
  ],
  declarations: [FileResourceComponent, JsonResourceComponent, ImageResourceComponent],
  exports: [FileResourceComponent, JsonResourceComponent, ImageResourceComponent]
})
export class ResourceEditorModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faTrash, faUpload, faSave);
  }
}
