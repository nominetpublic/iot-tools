/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Constants } from 'app/core/models/common/constants';
import { ResourceModel } from 'app/core/models/common/resource-model';
import { PreferenceService } from 'app/core/services/preference/preference.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'image-resource',
  templateUrl: './image-resource.component.html',
  styleUrls: ['./image-resource.component.scss']
})
export class ImageResourceComponent implements OnInit, OnDestroy {
  @Input() resource: ResourceModel;
  @Input() newResource = false;
  @Input() newTitle = 'New Resource';
  @Input() resourceKey = Constants.applicationKey;
  @Input() resourceType = 'mapIcon';
  @Output() update: EventEmitter<string> = new EventEmitter();
  @Output() delete: EventEmitter<string> = new EventEmitter();
  private subscriptions: Subscription[] = [];
  updated = false;
  mimeType;

  formGroup = this.fb.group({
    name: ['', Validators.required],
    file: [null]
  });
  imageUrl: SafeResourceUrl;

  constructor(
    protected cd: ChangeDetectorRef,
    private fb: FormBuilder,
    private sanitizer: DomSanitizer,
    private preferenceService: PreferenceService
  ) { }

  ngOnInit() {
    if (!this.newResource) {
      this.updateImage();
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  updateImage() {
    this.updated = false;
    this.mimeType = null;
    this.subscriptions.push(this.preferenceService.getImageResource(this.resource.resourceName, this.resourceKey).subscribe(res => {
      if (res != null) {
        this.imageUrl = this.sanitizer.bypassSecurityTrustResourceUrl(res.src);
        this.cd.detectChanges();
      }
    }));
  }

  get disableSave(): boolean {
    if (!this.newResource) {
      return !this.updated;
    } else {
      return !this.updated || this.formGroup.get('name').invalid;
    }
  }

  onFileChange(event) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.onloadend = () => {
        this.formGroup.patchValue({
          file: reader.result
        });
      };
      reader.readAsArrayBuffer(file);

      this.mimeType = file.type;
      this.updated = true;
    }
  }

  saveResource(): void {
    const file = new Blob([this.formGroup.get('file').value], { type: this.mimeType });
    if (this.newResource) {
      const resourceName = this.formGroup.get('name').value;
      this.preferenceService.upsertResource(resourceName, file, this.resourceKey, this.resourceType).subscribe(() => {
        this.update.emit(this.resourceType);
      });
    } else {
      this.preferenceService.upsertResource(this.resource.resourceName, file, this.resource.key).subscribe(() => {
        this.updateImage();
      });
    }
  }

  deleteResource(): void {
    this.preferenceService.deleteResource(this.resource.key, this.resource.resourceName).subscribe(() => {
      this.update.emit(this.resourceType);
      this.delete.emit(this.resourceType);
    });
  }


}
