/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Constants } from 'app/core/models/common/constants';
import { ResourceModel } from 'app/core/models/common/resource-model';
import { PreferenceService } from 'app/core/services/preference/preference.service';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'json-resource',
  templateUrl: './json-resource.component.html',
  styleUrls: ['./json-resource.component.scss']
})
export class JsonResourceComponent implements OnInit {
  @Input() resource: ResourceModel;
  @Input() newResource = false;
  @Input() newTitle = 'New Configuration Entry';
  @Input() resourceKey = Constants.applicationKey;
  @Input() resourceType = 'configuration';
  @Output() updateList: EventEmitter<string> = new EventEmitter();
  @Output() delete: EventEmitter<string> = new EventEmitter();
  error = false;
  updated = false;
  initialJson = null;
  updatedJson = null;

  formGroup = this.fb.group({
    name: ['', Validators.required],
  });

  constructor(
    protected cd: ChangeDetectorRef,
    private fb: FormBuilder,
    private preferenceService: PreferenceService
  ) { }

  ngOnInit() {
    if (!this.newResource) {
      if (this.resource.content == null) {
        this.preferenceService.getResource(this.resourceKey, this.resource.resourceName).subscribe(res => {
          this.initialJson = res;
          this.cd.markForCheck();
        });
      } else {
        this.initialJson = this.resource.content;
      }
    }
  }

  setError(value: boolean): void {
    this.error = value;
  }

  get disableSave(): boolean {
    return !this.updated || this.error;
  }

  saveResource(): void {
    if (!this.error) {
      const content = new Blob([JSON.stringify(this.updatedJson)], { type: 'application/json' });

      if (this.newResource) {
        const resourceName = this.formGroup.get('name').value;
        this.preferenceService.upsertResource(resourceName, content, this.resourceKey, this.resourceType).subscribe(() => {
          this.updateList.emit(this.resourceType);
        });
      } else {
        this.preferenceService.upsertResource(this.resource.resourceName, content, this.resource.key).subscribe(() => {
          this.updated = false;
        });
      }
    }
  }

  updateJson(value: object): void {
    this.updated = true;
    this.updatedJson = value;
  }

  deleteResource(): void {
    this.preferenceService.deleteResource(this.resource.key, this.resource.resourceName).subscribe(() => {
      this.updateList.emit(this.resourceType);
      this.delete.emit(this.resourceType);
    });
  }
}
