/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { PanelResizeService } from 'app/core/services/panel-resize/panel-resize.service';

@Component({
  selector: 'side-modal',
  templateUrl: './side-modal.component.html',
  styleUrls: ['./side-modal.component.scss']
})
export class SideModalComponent implements OnInit, OnDestroy {

  @Input() title = '';
  @Input() subtitle = '';
  @Input() docked = false;

  @Output() close = new EventEmitter();

  constructor(
    protected panelResize: PanelResizeService
  ) { }

  ngOnInit() {
    this.panelResize.dispatch();
  }

  ngOnDestroy() {
    this.panelResize.dispatch();
  }

  emitClose() {
    this.close.emit();
  }

}
