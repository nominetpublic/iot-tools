/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatInputModule } from '@angular/material/input';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

import { ConfirmDeleteDialogComponent } from './confirm-delete-dialog/confirm-delete-dialog.component';
import { KeyValueRowComponent } from './key-value-row/key-value-row.component';
import { NumericalValueRowComponent } from './numerical-value-row/numerical-value-row.component';
import { StringValueRowComponent } from './string-value-row/string-value-row.component';
import { TagsComponent } from './tags/tags.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    FontAwesomeModule,
    MatInputModule,
    MatButtonModule,
    MatChipsModule,
    MatDialogModule,
  ],
  declarations: [
    KeyValueRowComponent,
    TagsComponent,
    NumericalValueRowComponent,
    StringValueRowComponent,
    ConfirmDeleteDialogComponent,
  ],
  entryComponents: [
    ConfirmDeleteDialogComponent,
  ],
  exports: [
    KeyValueRowComponent,
    TagsComponent,
    StringValueRowComponent,
    NumericalValueRowComponent,
  ],
})
export class FormComponentsModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faTrash);
  }
}
