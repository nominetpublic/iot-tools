/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'numerical-value-row',
  templateUrl: './numerical-value-row.component.html',
  styleUrls: ['./numerical-value-row.component.scss']
})
export class NumericalValueRowComponent implements OnInit, OnChanges, OnDestroy  {
  subscriptions: Subscription[] = [];

  @Input() key = '';
  @Input() value: number = Number('');

  @Input() unsavedValue = false;
  @Input() unsavedKey = false;

  @Input() keyError = false;
  @Input() valueError = false;
  @Input() readonly = false;
  @Input() readonlyKey = false;
  @Input() canDelete = false;

  @Output() delete = new EventEmitter();
  @Output() valueChange = new EventEmitter();

  valueControl = new FormControl('');

  ngOnInit(): void {
    const valueSub = this.valueControl.valueChanges.subscribe(value => {
      this.saveValue(value);
      this.setErrors();
    });
    this.subscriptions.push(valueSub);
    this.initFormValues();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.initFormValues();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  initFormValues() {
    this.valueControl.setValue(this.value, {emitEvent: false});
    if (this.readonly && this.valueControl.enabled) {
      this.valueControl.disable();
    } else if (!this.readonly && this.valueControl.disabled) {
      this.valueControl.enable();
    }

    this.setErrors();
  }

  setErrors() {
    if (this.valueError) {
      this.valueControl.setErrors({ 'incorrect': true });
    }
  }

  saveValue(value) {
    if (!this.readonly) this.valueChange.emit(value);
  }

  deleteRow() {
    this.delete.emit();
  }


}
