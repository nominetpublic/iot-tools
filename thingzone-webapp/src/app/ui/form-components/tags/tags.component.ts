/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss']
})
export class TagsComponent {
  @Input() removable = true;
  @Input() addable = true;
  @Input() tags;
  @Output() addTag: EventEmitter<string> = new EventEmitter();
  @Output() removeTag: EventEmitter<string> = new EventEmitter();

  add(event): void {
    if (event.key === 'Enter' || event.key === 'Comma') {
      const input = event.target;
      const value = input.value;
      input.value = '';

      this.newTag(value);
    }
  }

  newTag(value) {
    if ((value || '').trim()) {
      this.addTag.emit(value);
    }
  }

  remove(tag): void {
    this.removeTag.emit(tag);
  }
}
