/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { SearchService } from 'app/core/services/search/search.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.scss']
})
export class SearchBoxComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  searchtext = '';
  currentModal = '';

  searchForm = new FormGroup({
    text: new FormControl(''),
  });

  constructor(private search: SearchService) { }

  ngOnInit() {
    const modalSub = this.search.$modalOpen.subscribe(modal => {
      this.currentModal = modal;
    });
    this.subscriptions.push(modalSub);

    const searchTextSub = this.searchForm.valueChanges.subscribe(formValues => {
      this.searchtext = formValues['text'];
    });
    this.subscriptions.push(searchTextSub);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  addTerm(event) {
    event.preventDefault();
    if (!this.searchtext) return false;

    this.search.addTerm('keywords', this.searchtext);

    // clear search text
    this.searchtext = '';
    this.search.toggleModal();
  }

  doOpenModal(name) {
    this.search.toggleModal(name);
  }
}
