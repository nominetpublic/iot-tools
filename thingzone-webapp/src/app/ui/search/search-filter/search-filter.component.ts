/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { SearchService } from 'app/core/services/search/search.service';

import { SavedSearch } from '../models/active-search';

@Component({
  selector: 'search-filter',
  templateUrl: './search-filter.component.html',
  styleUrls: ['./search-filter.component.scss']
})
export class SearchFilterComponent implements OnInit, OnDestroy {

  @Input() facets;
  @Input() count;

  subscriptions = [];
  activeSearch: SavedSearch;

  constructor(private search: SearchService) { }

  ngOnInit() {
    const searchSub = this.search.$search.subscribe(search => {
      this.activeSearch = search;
    });

    this.subscriptions.push(searchSub);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  selectAll(facetGroup) {
    if (this.anySelected(facetGroup)) {
      this.search.removeBlock(facetGroup.property);
    } else {
      facetGroup.options.forEach(option => {
        this.addTerm(option, facetGroup, false);
      });
    }
  }

  addTerm(option, facetGroup, toggle = true) {
    if (facetGroup.selectMode === 'single') {
      facetGroup.options.forEach(o => {
        if (o.id !== option.id) {
          this.search.removeTerm(facetGroup.property, o.value);
        }
      });
    }
    this.search.addTerm(facetGroup.property, option.value, toggle);
  }

  anySelected(facetGroup) {
    const property = facetGroup.property;
    return this.activeSearch.terms.some(a => a.property === property);
  }
}
