/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';

import { SearchResource } from '../models/search-resource';

@Component({
  selector: 'saved-search-collapse',
  templateUrl: './saved-search-collapse.component.html',
  styleUrls: ['./saved-search-collapse.component.scss']
})
export class SavedSearchCollapseComponent {

  @Input() resource: SearchResource;
  @Input() open = false;

  @Output() search: EventEmitter<SearchResource> = new EventEmitter();
  @Output() link: EventEmitter<SearchResource> = new EventEmitter();

  constructor() { }

  toggleOpen() {
    this.open = !this.open;
  }

  runSearch() {
    this.search.emit(this.resource);
  }

  toggleLink() {
    this.link.emit(this.resource);
  }

  sort() {

  }
}
