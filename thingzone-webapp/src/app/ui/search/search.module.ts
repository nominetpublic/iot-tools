/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faCircle as fasCircle, faSquare } from '@fortawesome/free-regular-svg-icons';
import {
  faCaretDown,
  faCaretUp,
  faCheck,
  faCheckSquare,
  faCircle,
  faLink,
  faSave,
  faSearch,
  faSlidersH,
  faSort,
  faTimes,
  faTrash,
  faUnlink,
} from '@fortawesome/free-solid-svg-icons';

import { PageOverviewModule } from '../page-overview/page-overview.module';
import { CurrentSearchComponent } from './current-search/current-search.component';
import { FacetListComponent } from './facet-list/facet-list.component';
import { SavedSearchCollapseComponent } from './saved-search-collapse/saved-search-collapse.component';
import { SavedSearchListComponent } from './saved-search-list/saved-search-list.component';
import { SearchBoxComponent } from './search-box/search-box.component';
import { SearchFilterComponent } from './search-filter/search-filter.component';
import { SearchListComponent } from './search-list/search-list.component';
import { SearchModalComponent } from './search-modal/search-modal.component';
import { SearchNameDialogComponent } from './search-name-dialog/search-name-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    DragDropModule,
    PageOverviewModule,
    FontAwesomeModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
  ],
  declarations: [
    SearchModalComponent,
    SearchBoxComponent,
    CurrentSearchComponent,
    SearchFilterComponent,
    FacetListComponent,
    SearchListComponent,
    SavedSearchListComponent,
    SearchNameDialogComponent,
    SavedSearchCollapseComponent,
  ],
  entryComponents: [
    SearchNameDialogComponent,
  ],
  exports: [
    SearchBoxComponent,
    CurrentSearchComponent,
    SearchFilterComponent,
    SearchListComponent,
    SavedSearchListComponent,
  ],
})
export class SearchModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faSearch, faSort, faSlidersH, faSave, faTrash, faTimes, faLink, faUnlink, faSort,
      faCheck, faCircle, faCaretDown, faCaretUp, faCheckSquare, faSquare, fasCircle);
  }
}
