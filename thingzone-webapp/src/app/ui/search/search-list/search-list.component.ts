/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { SearchService } from 'app/core/services/search/search.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'search-list',
  templateUrl: './search-list.component.html',
  styleUrls: ['./search-list.component.scss']
})
export class SearchListComponent implements OnInit, OnDestroy {

  @Input() facets = [];
  @Input() count = 0;
  @Input() saveType = 'device.savedSearches';
  subscriptions: Subscription[] = [];
  currentModal: string;

  constructor(protected search: SearchService) { }

  ngOnInit() {
    const modalSub = this.search.$modalOpen.subscribe(modal => {
      this.currentModal = modal;
    });
    this.subscriptions.push(modalSub);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

}
