/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { DisplayResource } from 'app/core/models/visualisations/display-resource';
import { PreferenceService } from 'app/core/services/preference/preference.service';
import { SearchService } from 'app/core/services/search/search.service';
import { Observable, Subscription, zip } from 'rxjs';

import { SearchResource } from '../models/search-resource';

@Component({
  selector: 'saved-search-list',
  templateUrl: './saved-search-list.component.html',
  styleUrls: ['./saved-search-list.component.scss']
})
export class SavedSearchListComponent implements OnInit, OnDestroy {

  @Input() saveType: string;
  savedSearches: SearchResource[];
  subscriptions: Subscription[] = [];

  constructor(
    private preferenceService: PreferenceService,
    protected search: SearchService) {
  }

  ngOnInit() {
    if (this.saveType === 'device.savedSearches') {
      const savedSearchSub = this.search.deviceSavedSearches$.subscribe(res => {
        if (res == null) {
          this.search.querySearchResources(this.saveType);
        }
        this.savedSearches = res;
      });
      this.subscriptions.push(savedSearchSub);
    } else if (this.saveType === 'alert.savedSearches') {
      const savedSearchSub = this.search.alertSavedSearches$.subscribe(res => {
        if (res == null) {
          this.search.querySearchResources(this.saveType);
        }
        this.savedSearches = res;
      });
      this.subscriptions.push(savedSearchSub);
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  loadSearch(searchResource: SearchResource) {
    const selected: SearchResource = this.savedSearches.find((resource) =>
      (resource.key === searchResource.key && resource.resourceName === searchResource.resourceName));
    this.search.setSearch(selected.content);
    this.search.closeModal();
  }

  deleteSearch(entityKey, resourceName) {
    this.preferenceService.deleteResource(entityKey, resourceName).subscribe((res) => {
      const deleted: SearchResource = this.savedSearches.find(resource => resource.key === entityKey);
      const next: SearchResource = this.savedSearches.find(resource => resource.content.after === entityKey);
      if (next) {
        next.content.after = deleted.content.after;
        this.search.updateSearch(next).subscribe(() =>
          this.search.querySearchResources(deleted.type)
        );
      } else {
        this.search.querySearchResources(deleted.type);
      }
    });
  }

  showTerm(searchValue: any, searchTerm: string, index: number) {
    return this.search.showTerm(searchValue, searchTerm, index);
  }

  toggleLink(searchResource: SearchResource) {
    const savedSearch = searchResource.content;
    savedSearch.link = savedSearch.link ? false : true;

    this.search.updateSearch(searchResource).subscribe(() => {
      this.search.setSearchResources(searchResource.type, this.savedSearches);
    });
  }

  searchByKey(index: number, search: SearchResource) {
    return search.key;
  }

  /**
   * Change order of searches - requires list to be sorted
   */
  drop(event: CdkDragDrop<DisplayResource[]>): void {
    const fromIndex = event.previousIndex;
    const toIndex = event.currentIndex;
    const from = this.clamp(fromIndex, this.savedSearches.length - 1);
    const to = this.clamp(toIndex, this.savedSearches.length - 1);

    if (from === to) {
      return;
    }
    const searches: Observable<any>[] = [];
    const insertBefore = to < from ? 0 : 1;
    const moved = this.savedSearches[fromIndex];

    // remove from list
    if (from !== this.savedSearches.length - 1) {
      const next = this.savedSearches[from + 1];
      next.content.after = moved.content.after;
      searches.push(this.search.updateSearch(next));
    }

    if (to === this.savedSearches.length - 1) {
      // moved to end of the list
      const replaced = this.savedSearches[to];
      moved.content.after = replaced.key;
      searches.push(this.search.updateSearch(moved));
    } else {
      // move to internal node of list
      const replaced = this.savedSearches[toIndex];
      if (insertBefore) {
        const next = this.savedSearches[toIndex + 1];
        next.content.after = moved.key;
        searches.push(this.search.updateSearch(next));
        moved.content.after = replaced.key;
      } else {
        moved.content.after = replaced.content.after;
        replaced.content.after = moved.key;
        searches.push(this.search.updateSearch(replaced));
      }
      searches.push(this.search.updateSearch(moved));
    }

    zip(searches).subscribe(() => {
      this.search.setSearchResources(moved.type, this.savedSearches);
    });
  }

  /** Clamps a number between zero and a maximum. */
  clamp(value: number, max: number): number {
    return Math.max(0, Math.min(max, value));
  }
}
