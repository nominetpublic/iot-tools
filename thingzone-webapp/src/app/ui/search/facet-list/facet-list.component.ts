/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { SearchTerm } from 'app/core/models/common/search-term';

@Component({
  selector: 'facet-list',
  templateUrl: './facet-list.component.html',
  styleUrls: ['./facet-list.component.scss']
})
export class FacetListComponent {

  @Input() count;
  @Input() facetGroup;
  @Input() activeTerms: SearchTerm[] = [];

  @Output() selectAll = new EventEmitter();
  @Output() selectSingle = new EventEmitter();

  constructor() { }

  doSelectAll(group) {
    this.selectAll.emit(group);
  }

  doAddTerm(option, group) {
    this.selectSingle.emit({ option, facetGroup: group });
  }

  isSelected(facet, group): boolean {
    const value = facet.value;
    const groupTerms = this.activeTerms.filter(a => a.property === group.property);
    return groupTerms.some(a =>
      a.values.some(b => b === value)
    );
  }

  allSelected(group): boolean {
    if (this.activeTerms.length === 0) {
      return false;
    }
    let currentTerms: any[] = [];
    this.activeTerms.filter(searchTerm => searchTerm.property === group.property)
      .map(searchTerm => currentTerms = currentTerms.concat(searchTerm.values));
    return group.options.every(option => currentTerms.some(term => option.value === term));
  }
}
