/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SearchTerm } from 'app/core/models/common/search-term';
import { SearchService } from 'app/core/services/search/search.service';
import { Subscription } from 'rxjs';

import { SearchNameDialogComponent } from '../search-name-dialog/search-name-dialog.component';

export interface DialogData {
  name: string;
}

@Component({
  selector: 'current-search',
  templateUrl: './current-search.component.html',
  styleUrls: ['./current-search.component.scss']
})
export class CurrentSearchComponent implements OnInit, OnDestroy {
  @Input() saveType: string;
  subscriptions: Subscription[] = [];
  searchTerms: SearchTerm[] = [];
  searchName: string;
  showSave = true;

  constructor(
    private search: SearchService,
    public dialog: MatDialog) {
  }

  ngOnInit() {
    const searchSub = this.search.$search.subscribe(currentSearch => {
      this.searchTerms = currentSearch.terms;
      this.searchName = currentSearch.name;
      this.showSave = (this.searchName == null) ? true : false;
    });
    this.subscriptions.push(searchSub);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  removeTerm(block, search) {
    this.search.removeTerm(block.property, search);
  }

  clearAll() {
    this.search.clearTerms();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(SearchNameDialogComponent, {
      width: '250px',
      data: { name: this.searchName }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.searchName = result;
      if (this.searchName != null) {
        this.saveSearch();
      }
    });
  }

  saveSearch() {
    this.search.saveSearch(this.saveType, this.searchName).subscribe(() => {
      this.showSave = false;
      this.search.querySearchResources(this.saveType);
    });
  }

  showTerm(searchValue: any, searchTerm: string, index: number) {
    return this.search.showTerm(searchValue, searchTerm, index);
  }
}
