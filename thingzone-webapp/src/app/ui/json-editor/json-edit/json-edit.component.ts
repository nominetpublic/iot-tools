/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import * as ace from 'ace-builds/src-min-noconflict/ace';
import * as JSONEditor from 'jsoneditor';

@Component({
  selector: 'json-edit',
  templateUrl: './json-edit.component.html',
  styleUrls: ['./json-edit.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class JsonEditComponent implements OnInit, OnChanges {
  @Input() json: any;
  @Input() schema: any = null;
  @Input() mode: 'tree' | 'view' | 'form' | 'code' | 'text' | 'preview' = 'view';
  @Input() expanded = false;
  @Output() error: EventEmitter<boolean> = new EventEmitter();
  @Output() update: EventEmitter<object> = new EventEmitter();

  editor;
  @ViewChild('jsonEditor', { static: true }) editorElement: ElementRef;
  errorMessage = null;

  constructor() { }

  ngOnInit() {
    const element = this.editorElement.nativeElement as HTMLElement;
    const self = this;
    const options = {
      ace: ace,
      onChange: function () {
        self.onJsonChange();
      },
      onValidationError: function (errors) {
        self.onError(errors);
      }
    };

    this.errorMessage = null;
    this.editor = new JSONEditor(element, options);
    this.editor.setMode(this.mode);
    if (this.json != null) {
      this.editor.set(this.json);
      this.expand();
    }
    if (this.schema != null) {
      this.setSchema(this.schema);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    this.errorMessage = null;
    if (this.editor != null) {
      if ('json' in changes && changes['json'].currentValue != null) {
        this.editor.set(changes['json'].currentValue);
        if (this.expanded) {
          this.expand();
        }
      }
      if ('schema' in changes && changes['schema'].currentValue != null) {
        this.setSchema(changes['schema'].currentValue);
      }
    }
  }

  onJsonChange() {
    if (this.editor != null) {
      try {
        const currentJson = this.editor.get();
        this.update.emit(currentJson);
      } catch (err) {
        // caught by onValidationError error listener
      }
    }
  }

  setSchema(schema) {
    try {
      this.editor.setSchema(schema);
    } catch (err) {
      this.errorMessage = err.message;
    }
  }

  expand() {
    if (this.expanded && ['tree', 'view', 'form'].includes(this.mode)) {
      this.editor.expandAll();
    }
  }

  onError(errors: any[]) {
    if (errors.length > 0) {
      this.error.emit(true);
    } else {
      this.error.emit(false);
    }
  }

}
