/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AfterViewInit, Component, ElementRef, EventEmitter, HostListener, Input, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'colour-slider',
  templateUrl: './colour-slider.component.html',
  styleUrls: ['./colour-slider.component.scss']
})
export class ColourSliderComponent implements AfterViewInit {
  @Input() hue = 0;
  @Output() hueChange: EventEmitter<number[]> = new EventEmitter();

  @ViewChild('canvas', { static: false }) canvas: ElementRef<HTMLCanvasElement>;
  @ViewChild('rect', { static: false }) rect: ElementRef<SVGRectElement>;
  private ctx: CanvasRenderingContext2D;
  dragging: boolean;
  selectedHeight = 0;
  mousePosition: { x: number, y: number } = { x: 0, y: 0 };

  constructor() { }

  ngAfterViewInit(): void {
    this.draw();
  }

  draw(): void {
    if (!this.ctx) {
      this.ctx = this.canvas.nativeElement.getContext('2d');
    }
    const width = this.canvas.nativeElement.width;
    const height = this.canvas.nativeElement.height;
    this.ctx.clearRect(0, 0, width, height);

    const gradient = this.ctx.createLinearGradient(0, 0, 0, height);

    for (let d = 0; d < 360; d++) {
      gradient.addColorStop(d / 360, 'hsl(' + d + ', 100%, 50%)');
      gradient.addColorStop(d / 360, 'hsl(' + d + ', 100%, 50%)');
    }

    this.ctx.beginPath();
    this.ctx.rect(0, 0, width, height);
    this.ctx.fillStyle = gradient;
    this.ctx.fill();
    this.ctx.closePath();

    this.selectedHeight = this.getPositionFromColour(this.hue, height);
  }

  onMouseDown(event: MouseEvent): void {
    if (!this.dragging) {
      event.preventDefault();
      if (this.contained(this.selectedHeight, this.mousePosition)) {
        this.dragging = true;
      } else {
        this.pickColour();
      }
    }
  }

  onMouseMove(event: MouseEvent): void {
    this.mousePosition = { x: event.offsetX, y: event.offsetY };

    if (this.dragging) {
      event.preventDefault();
      this.pickColour();
    }
  }

  @HostListener('window:mouseup', ['$event'])
  onMouseUp(event: MouseEvent): void {
    if (this.dragging) {
      event.preventDefault();
      this.dragging = false;
    }
  }

  pickColour(): void {
    this.selectedHeight = this.mousePosition.y;
    this.emitColour(this.mousePosition.x, this.mousePosition.y);
  }

  emitColour(x: number, y: number): void {
    const rgb = this.getColorAtPosition(x, y);
    this.hueChange.emit(rgb);
  }

  getColorAtPosition(x: number, y: number): number[] {
    const imageData = this.ctx.getImageData(x, y, 1, 1).data;
    return [imageData[0], imageData[1], imageData[2]];
  }

  getPositionFromColour(hue: number, sliderHeight: number): number {
    return hue / (360 / sliderHeight);
  }

  contained(position1: number, position2: { x: number, y: number }): boolean {
    const b = position1 - position2.y;
    return Math.abs(b) < 5;
  }

}
