/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Overlay, OverlayPositionBuilder, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import {
  AfterViewInit,
  Component,
  ComponentRef,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatFormField } from '@angular/material';
import { Subscription } from 'rxjs';

import { ColourPickerOverlayComponent } from '../colour-picker-overlay/colour-picker-overlay.component';

@Component({
  selector: 'colour-picker',
  templateUrl: './colour-picker.component.html',
  styleUrls: ['./colour-picker.component.scss']
})
export class ColourPickerComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() colour: string;
  @Input() placeholder = '#0097a8';
  @Input() name = 'colour-select';
  @Input() label = 'Select Colour';
  @Output() colourChange: EventEmitter<string> = new EventEmitter();

  subscriptions: Subscription[] = [];
  width = 350;
  height = 270;

  colourControl = new FormControl('', [Validators.pattern('^#(?:[0-9a-f]{3}){1,2}$')]);

  @ViewChild('colourFormField', { static: false }) colourField: MatFormField;
  public overlayRef: OverlayRef;

  constructor(private overlayPositionBuilder: OverlayPositionBuilder,
    private overlay: Overlay) {
  }

  ngOnInit(): void {
    const colourSub = this.colourControl.valueChanges.subscribe(value => {
      this.colour = value;
      this.colourChange.emit(this.colour);
    });
    this.subscriptions.push(colourSub);
  }

  ngAfterViewInit() {
    const positionStrategy = this.overlayPositionBuilder
      .flexibleConnectedTo(this.getConnectedOverlayOrigin())
      .withViewportMargin(8)
      .withPositions([{
        originX: 'start',
        originY: 'bottom',
        overlayX: 'start',
        overlayY: 'top'
      },
      {
        originX: 'start',
        originY: 'top',
        overlayX: 'start',
        overlayY: 'bottom'
      },
      {
        originX: 'end',
        originY: 'bottom',
        overlayX: 'end',
        overlayY: 'top'
      },
      {
        originX: 'end',
        originY: 'top',
        overlayX: 'end',
        overlayY: 'bottom'
      }]);

    // Connect position strategy
    this.overlayRef = this.overlay.create({
      width: this.width,
      height: this.height,
      backdropClass: 'mat-overlay-transparent-backdrop',
      positionStrategy: positionStrategy,
      hasBackdrop: true,
      panelClass: 'colour-picker-popup',
      scrollStrategy: this.overlay.scrollStrategies.block()
    });

    this.overlayRef.backdropClick().subscribe(() => this.hide());
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  @HostListener('mousedown', ['$event'])
  show() {
    const colourSelect = new ComponentPortal(ColourPickerOverlayComponent);
    const colourSelectRef: ComponentRef<ColourPickerOverlayComponent> = this.overlayRef.attach(colourSelect);

    // Pass content to tooltip component instance
    colourSelectRef.instance.colour = this.colour;
    colourSelectRef.instance.colourChange.subscribe(colour => {
      this.colourControl.setValue(colour);
      this.colourChange.emit(colour);
    });
    colourSelectRef.changeDetectorRef.detectChanges();
  }

  hide(): void {
    this.overlayRef.detach();
  }

  getConnectedOverlayOrigin(): ElementRef {
    return this.colourField.getConnectedOverlayOrigin();
  }
}
