/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { ColourService } from 'app/core/services/api/colour.service';

@Component({
  selector: 'colour-palette',
  templateUrl: './colour-palette.component.html',
  styleUrls: ['./colour-palette.component.scss']
})
export class ColourPaletteComponent implements AfterViewInit, OnChanges {

  @Input() rgb: number[] = [255, 0, 0];
  @Output() updateSaturation: EventEmitter<number[]> = new EventEmitter();

  @ViewChild('canvas', { static: false }) canvas: ElementRef<HTMLCanvasElement>;
  @ViewChild('circle', { static: false }) circle: ElementRef<SVGCircleElement>;

  private ctx: CanvasRenderingContext2D;
  private dragging = false;
  public circlePosition: { x: number; y: number } = { x: 0, y: 0 };
  public mousePosition: { x: number; y: number } = { x: 0, y: 0 };

  constructor(private colourService: ColourService) { }

  ngAfterViewInit() {
    this.draw();
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('rgb' in changes && !changes['rgb'].isFirstChange()) {
      this.draw();
    }
  }

  draw(): void {
    if (!this.ctx) {
      this.ctx = this.canvas.nativeElement.getContext('2d');
    }
    const width = this.canvas.nativeElement.width;
    const height = this.canvas.nativeElement.height;

    this.circlePosition = this.getPositionFromColour(this.rgb, width, height);

    // saturation
    const gradientH = this.ctx.createLinearGradient(0, 0, width, 0);
    const hsl = this.colourService.rgbToHsl(this.rgb[0], this.rgb[1], this.rgb[2]);
    gradientH.addColorStop(1 / 255, '#fff');
    gradientH.addColorStop(254 / 255, 'hsl(' + hsl[0] + ', 100%, 50%)');

    this.ctx.fillStyle = gradientH;
    this.ctx.fillRect(0, 0, width, height);

    // lightness
    const gradientV = this.ctx.createLinearGradient(0, 0, 0, height);
    gradientV.addColorStop(1 / 255, 'rgba(0,0,0,0)');
    gradientV.addColorStop(254 / 255, '#000');

    this.ctx.fillStyle = gradientV;
    this.ctx.fillRect(0, 0, width, height);
  }

  getPositionFromColour(rgb: number[], width: number, height: number): { x: number; y: number } {
    const hsv = this.colourService.rgbToHsv(rgb[0], rgb[1], rgb[2]);
    return {
      x: Math.round((hsv[1]) * width),
      y: height - Math.round((hsv[2]) * height)
    };
  }

  distance(position1, position2): number {
    const a = position1.x - position2.x;
    const b = position1.y - position2.y;

    return Math.sqrt(a * a + b * b);
  }

  @HostListener('window:mouseup', ['$event'])
  onMouseUp(event: MouseEvent) {
    this.dragging = false;
  }

  onMouseDown(event: MouseEvent) {
    if (!this.dragging) {
      const circleRadius = this.circle.nativeElement.r.animVal.value;
      if (this.distance(this.circlePosition, this.mousePosition) < circleRadius) {
        this.dragging = true;
      } else {
        this.pickColour();
      }
    }
  }

  onMouseMove(event: MouseEvent) {
    this.mousePosition = { x: event.offsetX, y: event.offsetY };

    if (this.dragging) {
      this.pickColour();
    }
  }

  pickColour(): void {
    this.circlePosition = this.mousePosition;
    this.emitColour(this.circlePosition.x, this.circlePosition.y);
  }

  emitColour(x: number, y: number): void {
    const rgb = this.getColorAtPosition(x, y);
    this.updateSaturation.emit(rgb);
  }

  getColorAtPosition(x: number, y: number): number[] {
    const imageData = this.ctx.getImageData(x, y, 1, 1).data;
    return [imageData[0], imageData[1], imageData[2]];
  }
}
