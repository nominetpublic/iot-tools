/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { OverlayModule } from '@angular/cdk/overlay';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faCircle } from '@fortawesome/free-solid-svg-icons';

import { ColourPaletteComponent } from './colour-palette/colour-palette.component';
import { ColourPickerOverlayComponent } from './colour-picker-overlay/colour-picker-overlay.component';
import { ColourPickerComponent } from './colour-picker/colour-picker.component';
import { ColourSliderComponent } from './colour-slider/colour-slider.component';

@NgModule({
  declarations: [ColourSliderComponent, ColourPaletteComponent, ColourPickerComponent, ColourPickerOverlayComponent],
  exports: [ColourPickerComponent],
  entryComponents: [ColourPickerOverlayComponent],
  imports: [
    CommonModule,
    OverlayModule,
    ReactiveFormsModule,
    MatInputModule,
    FontAwesomeModule,
  ]
})
export class ColourPickerModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faCircle);
  }
}
