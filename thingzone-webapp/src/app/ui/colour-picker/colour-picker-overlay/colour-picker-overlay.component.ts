/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ColourService } from 'app/core/services/api/colour.service';

@Component({
  selector: 'colour-picker-overlay',
  templateUrl: './colour-picker-overlay.component.html',
  styleUrls: ['./colour-picker-overlay.component.scss']
})
export class ColourPickerOverlayComponent implements OnInit {

  @Input() colour: string;
  @Output() colourChange: EventEmitter<string> = new EventEmitter(true);
  rgb: number[] = [255, 0, 0];
  hue = 0;

  constructor(private colourService: ColourService) { }

  ngOnInit() {
    this.rgb = this.colourService.hexToRgbArray(this.colour);
    this.hue = this.colourService.rgbToHsl(this.rgb[0], this.rgb[1], this.rgb[2])[0];
  }

  hueChange(value: number[]): void {
    this.updateColour(value);
  }

  updateSaturation(value: number[]): void {
    this.updateColour(value);
  }

  updateColour(value: number[]): void {
    const r = value[0];
    const g = value[1];
    const b = value[2];
    this.rgb = value;
    this.hue = this.colourService.rgbToHsl(r, g, b)[0];
    const hexColour = this.colourService.rgbToHex(r, g, b);
    this.colourChange.emit(hexColour);
  }
}

