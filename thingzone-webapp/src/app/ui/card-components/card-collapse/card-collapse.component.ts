/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'card-collapse',
    templateUrl: './card-collapse.component.html',
    styleUrls: ['./card-collapse.component.scss']
})
export class CardCollapseComponent {
    @Input() count;
    @Input() title = '';
    @Input() addbtn = false;
    @Input() addMessage: string;
    @Input() editbtn = false;
    @Input() editMessage: string;
    @Input() deletebtn = false;
    @Input() deleteMessage: string;
    @Input() open = true;

    @Output() add = new EventEmitter();
    @Output() edit = new EventEmitter();
    @Output() delete = new EventEmitter();
    @Output() openStatus: EventEmitter<boolean> = new EventEmitter();

    constructor() { }

    toggleOpen() {
        this.open = !this.open;
        this.openStatus.emit(this.open);
    }

    addEmit() {
        this.open = true;
        this.add.emit();
    }

    editEmit() {
        this.edit.emit();
    }

    deleteEmit() {
        this.delete.emit();
    }
}
