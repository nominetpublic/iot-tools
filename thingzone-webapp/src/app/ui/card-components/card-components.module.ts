/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faCaretDown, faCaretUp, faPen, faPlus, faTrash } from '@fortawesome/free-solid-svg-icons';

import { CardCollapseComponent } from './card-collapse/card-collapse.component';
import { CardWrapperComponent } from './card-wrapper/card-wrapper.component';
import { CardComponent } from './card/card.component';

@NgModule({
  imports: [
    CommonModule,
    FontAwesomeModule,
    MatButtonModule,
  ],
  declarations: [
    CardWrapperComponent,
    CardCollapseComponent,
    CardComponent,
  ],
  exports: [
    CardWrapperComponent,
    CardCollapseComponent,
    CardComponent,
  ]
})
export class CardComponentsModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faCaretDown, faCaretUp, faPlus, faTrash, faPen);
  }
}
