/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'map-position-input',
  templateUrl: './map-position-input.component.html',
  styleUrls: ['./map-position-input.component.scss']
})
export class MapPositionInputComponent implements OnInit, OnChanges, OnDestroy {

  @Input() longitude: number;
  @Input() latitude: number;
  @Input() index: number;
  @Input() changed;
  @Output() updatePosition = new EventEmitter<any>();
  subscriptions: Subscription[] = [];
  longitudeControl = new FormControl(0, [Validators.required, Validators.min(-180), Validators.max(180)]);
  latitudeControl = new FormControl(0, [Validators.required, Validators.min(-90), Validators.max(90)]);

  ngOnInit(): void {
    this.initValues();
    const longitudeSub = this.longitudeControl.valueChanges.subscribe(longitude => {
      if (this.longitudeControl.valid) {
        this.saveLongitude(longitude);
        this.update();
      }
    });
    this.subscriptions.push(longitudeSub);

    const latitudeSub = this.latitudeControl.valueChanges.subscribe(latitude => {
      if (this.latitudeControl.valid) {
        this.saveLatitude(latitude);
        this.update();
      }
    });
    this.subscriptions.push(latitudeSub);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.initValues();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  initValues() {
    this.longitudeControl.setValue(this.longitude, { onlySelf: true, emitEvent: false });
    this.latitudeControl.setValue(this.latitude, { onlySelf: true, emitEvent: false });
  }

  update() {
    this.updatePosition.emit({ index: this.index, value: [this.longitude, this.latitude] });
  }

  saveLongitude(value) {
    if (!isNaN(Number(value))) {
      this.longitude = value;
    }
  }

  saveLatitude(value) {
    if (!isNaN(Number(value))) {
      this.latitude = value;
    }
  }
}
