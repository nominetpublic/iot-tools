/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import * as MapboxDraw from '@mapbox/mapbox-gl-draw';
import StaticMode from '@mapbox/mapbox-gl-draw-static-mode';
import * as theme from 'app/core/models/map/mapbox-draw-theme.js';
import { ConfigService } from 'app/core/services/config/config.service';
import { GeoService } from 'app/core/services/geo/geo.service';
import * as mapboxgl from 'mapbox-gl';

@Component({
  selector: 'map-display',
  templateUrl: './map-display.component.html',
  styleUrls: ['./map-display.component.scss']
})
export class MapDisplayComponent implements OnInit, OnChanges, OnDestroy {

  @Input() location;
  @Input() geometry;
  @Input() displayOnly = false;
  @Input() defaultCentre: [number, number] = [-1.221267, 51.713977];
  @Output() updateFeature = new EventEmitter();
  private resizeFunction = this.resize.bind(this);
  draw: MapboxDraw;
  map: mapboxgl.Map;
  lightTheme: string;

  @ViewChild('mapDisplay', { static: true }) mapElement: ElementRef;

  constructor(
    private geo: GeoService,
    private configService: ConfigService
  ) {
    this.lightTheme = configService.getConfigValue('lightTheme');
  }

  ngOnInit() {
    window.addEventListener('panelResize', this.resizeFunction.bind(this));

    const htmlMapElement = this.mapElement.nativeElement as HTMLElement;
    this.geo.setApiKey(mapboxgl);
    this.map = new mapboxgl.Map({
      container: htmlMapElement,
      style: this.lightTheme,
      zoom: 14,
      center: this.getCentre(this.geometry)
    });

    if (this.displayOnly) {
      const modes = MapboxDraw.modes;
      modes.static = StaticMode;
      this.draw = new MapboxDraw({
        modes: modes,
        styles: theme,
        keybindings: false,
        displayControlsDefault: false
      });

      this.map.addControl(this.draw);
      if (this.location != null) {
        this.draw.add(this.location);
      }
      this.map.on('load', () => {
        this.draw.changeMode('static');
        this.map.resize();
      });
    } else {
      this.draw = new MapboxDraw({
        displayControlsDefault: false,
        styles: theme,
        controls: {
          point: true,
          line_string: true,
          polygon: true,
          trash: true
        }
      });

      this.map.addControl(this.draw);
      if (this.location != null) {
        this.draw.add(this.location);
      }

      this.map.on('load', () => {
        this.map.resize();
      });

      this.map.on('draw.modechange', () => {
        const data = this.draw.getAll();
        const ids = [];

        // ID of the added template empty feature
        const last_id = data.features[data.features.length - 1].id;

        data.features.forEach((f) => {
          if (f.id !== last_id) {
            ids.push(f.id);
          }
        });
        this.draw.delete(ids);
      });
      this.map.on('draw.create', () => {
        this.updateLocation();
      });
      this.map.on('draw.update', () => {
        this.updateLocation();
      });
      this.map.on('draw.delete', () => {
        this.updateLocation();
      });
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    this.resizeFunction();
    const location = changes.location && changes.location.currentValue;
    if (location && !changes.location.isFirstChange()) {
      if (location.type === 'FeatureCollection') {
        this.draw.set(location);
      } else {
        this.draw.deleteAll();
        this.draw.add(location);
      }
    }
    if (changes['geometry'] && !changes['geometry'].isFirstChange()) {
      const newCentre = this.getCentre(changes['geometry'].currentValue);
      this.map.setCenter(newCentre);
    }
  }

  ngOnDestroy() {
    if (this.map) {
      this.map.remove();
    }
  }

  resize(): void {
    setTimeout(() => {
      if (this.map) {
        this.map.resize();
      }
    });
  }

  getCentre(geometry): [number, number] {
    if (geometry != null && geometry.type != null) {
      return this.geo.getCentre(geometry);
    }
    // return default
    return this.defaultCentre;
  }

  updateLocation(): void {
    const data = this.draw.getAll();
    this.updateFeature.emit(data);
  }
}
