/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';

import { MapDisplayComponent } from './map-display/map-display.component';
import { MapEditComponent } from './map-edit/map-edit.component';
import { MapFormComponent } from './map-form/map-form.component';
import { MapPositionInputComponent } from './map-position-input/map-position-input.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatInputModule,
  ],
  declarations: [
    MapEditComponent,
    MapDisplayComponent,
    MapFormComponent,
    MapPositionInputComponent
  ],
  exports: [
    MapEditComponent
  ]
})
export class MapEditModule { }
