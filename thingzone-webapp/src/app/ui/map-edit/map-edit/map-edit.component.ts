/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { GeoService } from 'app/core/services/geo/geo.service';

@Component({
  selector: 'map-edit',
  templateUrl: './map-edit.component.html',
  styleUrls: ['./map-edit.component.scss']
})
export class MapEditComponent implements OnChanges {

  @Input() location;
  @Input() changed = false;
  @Input() displayOnly = false;
  @Output() update = new EventEmitter();

  geometry;

  constructor(
    private geoService: GeoService
  ) {
  }

  ngOnChanges(changes) {
    const loc = changes.location;
    if (loc.currentValue) {
      this.setGeometry(loc.currentValue);
    }
  }

  updateLocation(location) {
    this.setGeometry(location);
    this.update.emit(this.geometry);
  }

  setGeometry(location) {
    this.geometry = this.geoService.getGeometry(location);
  }

  updateGeometry(geometry) {
    this.updateLocation(geometry);
  }
}
