/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { geometry } from '@turf/helpers';

@Component({
  selector: 'map-form',
  templateUrl: './map-form.component.html',
  styleUrls: ['./map-form.component.scss']
})
export class MapFormComponent {

  @Input() geometry;
  @Input() changed;
  @Output() updateGeometry = new EventEmitter();

  updatePosition(value) {
    const index: any = value.index;
    const position: number[] = value.value;
    let updatedGeometry;
    if (index === null) {
      updatedGeometry = geometry('Point', position);
    } else if (Array.isArray(index)) {
      // polygon
      const coordinates = [...this.geometry.coordinates[index[0]]];
      coordinates[index[1]] = position;
      // special case for the connecting points of a polygon
      if (index[1] === 0) {
        coordinates[coordinates.length - 1] = position;
      }
      const polygonCoordinates = [];
      polygonCoordinates[0] = coordinates;
      updatedGeometry = geometry('Polygon', polygonCoordinates);
    } else {
      // line
      const coordinates = [...this.geometry.coordinates];
      coordinates[index] = position;
      updatedGeometry = geometry('LineString', coordinates);
    }
    this.updateGeometry.emit(updatedGeometry);
  }
}
