/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import 'ace-builds/src-noconflict/ext-language_tools';
import 'ace-builds/src-noconflict/mode-javascript';
import 'ace-builds/src-noconflict/theme-monokai';

import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import * as ace from 'ace-builds/src-min-noconflict/ace';
import { fromEvent, merge, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

const language = 'ace/mode/javascript';
const theme = 'ace/theme/monokai';

@Component({
  selector: 'syntax-editor',
  templateUrl: './syntax-editor.component.html',
  styleUrls: ['./syntax-editor.component.scss']
})
export class SyntaxEditorComponent implements OnInit, OnChanges, OnDestroy {

  @Input() code;

  @ViewChild('ace', { static: true }) aceElement: ElementRef;
  private subscriptions: Subscription[] = [];
  aceEditor: ace.Ace.Editor;
  editorBeautify: any;
  editorElement: HTMLElement;

  constructor(
    private cd: ChangeDetectorRef,
    private element: ElementRef) {
    this.editorElement = element.nativeElement;
  }

  ngOnInit() {
    this.bindWindowResizeEvent();
    ace.require('ace/ext/language_tools');
    this.editorBeautify = ace.require('ace/ext/beautify');
    const element = this.aceElement.nativeElement;
    const editorOptions = this.getEditorOptions();

    this.aceEditor = ace.edit(element, editorOptions);
    this.aceEditor.setTheme(theme);
    this.aceEditor.session.setMode(language);
    this.aceEditor.setShowFoldWidgets(true);
    this.aceEditor.setShowPrintMargin(false);
    this.setContent(this.code);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ('code' in changes) {
      this.setContent(changes.code.currentValue);
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  private getEditorOptions(): Partial<ace.Ace.EditorOptions> {
    const basicEditorOptions: Partial<ace.Ace.EditorOptions> = {
      highlightActiveLine: true,
      minLines: 14,
      maxLines: Infinity,
      autoScrollEditorIntoView: true,
      enableBasicAutocompletion: true
    };
    return basicEditorOptions;
  }

  public getContent() {
    if (this.aceEditor) {
      const code = this.aceEditor.getValue();
      return code;
    }
  }

  public setContent(content: string): void {
    if (this.aceEditor && content != null) {
      this.aceEditor.session.setValue(content, -1);
    }
  }

  private bindWindowResizeEvent(): void {
    const resize$ = fromEvent(window, 'resize');
    const panelResize$ = fromEvent(window, 'panelResize');
    const subscription = merge(resize$, panelResize$).pipe(debounceTime(200)).subscribe(() => {
      if (this.cd) {
        this.aceEditor.resize();
        this.cd.markForCheck();
      }
    });
    this.subscriptions.push(subscription);
  }
}
