/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faAngleRight, faSave, faSpinner, faTimes } from '@fortawesome/free-solid-svg-icons';

import { ItemPanelHeaderComponent } from './item-panel-header/item-panel-header.component';
import { ItemPanelSubheaderComponent } from './item-panel-subheader/item-panel-subheader.component';
import { ItemPanelWrapperComponent } from './item-panel-wrapper/item-panel-wrapper.component';
import { SaveButtonsComponent } from './save-buttons/save-buttons.component';

@NgModule({
  imports: [
    CommonModule,
    FontAwesomeModule,
    MatButtonModule,
  ],
  declarations: [ItemPanelWrapperComponent, ItemPanelHeaderComponent, ItemPanelSubheaderComponent, SaveButtonsComponent],
  exports: [ItemPanelWrapperComponent, ItemPanelHeaderComponent, ItemPanelSubheaderComponent, SaveButtonsComponent]
})
export class ItemPanelModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faSpinner, faTimes, faAngleRight, faSave);
  }
}
