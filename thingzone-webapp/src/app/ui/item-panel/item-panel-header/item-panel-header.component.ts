/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'item-panel-header',
  templateUrl: './item-panel-header.component.html',
  styleUrls: ['./item-panel-header.component.scss']
})
export class ItemPanelHeaderComponent {

  @Input() title = '';
  @Input() subtitle = '';
  @Input() showError = false;
  @Input() errorMessage = '';
  @Output() close = new EventEmitter();
  @Output() hideError = new EventEmitter();

  constructor() { }

  emitClose() {
    this.close.emit();
  }

  emitErrorClose() {
    this.hideError.emit();
  }

  hasMessage() {
    if (this.showError) {
      if (this.errorMessage == null || this.errorMessage === '') {
        return false;
      }
      return true;
    }
    return false;
  }
}
