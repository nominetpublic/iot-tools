/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'item-panel-subheader',
  templateUrl: './item-panel-subheader.component.html',
  styleUrls: ['./item-panel-subheader.component.scss']
})
export class ItemPanelSubheaderComponent {

  @Input() title = '';
  @Input() returnMessage = '';
  @Input() showError = false;
  @Input() errorMessage = '';
  @Output() close = new EventEmitter();
  @Output() hideError = new EventEmitter();

  emitErrorClose() {
    this.hideError.emit();
  }

  emitClose() {
    this.close.emit();
  }

  hasMessage() {
    if (this.showError) {
      if (this.errorMessage == null || this.errorMessage === '') {
        return false;
      }
      return true;
    }
    return false;
  }

}
