/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { FormlyJsonschema } from '@ngx-formly/core/json-schema';

@Component({
  selector: 'schema-form',
  templateUrl: './schema-form.component.html',
  styleUrls: ['./schema-form.component.scss']
})
export class SchemaFormComponent implements OnInit {
  @Input() name = 'Form';
  @Input() schema;
  @Input() json;
  @Output() error: EventEmitter<boolean> = new EventEmitter();
  @Output() update: EventEmitter<object> = new EventEmitter();
  form = new FormGroup({});
  options: FormlyFormOptions = {};
  allowedFieldTypes = [ 'string', 'number', 'integer', 'boolean', 'enum', 'null'];

  fields: FormlyFieldConfig[];

  constructor(
    private formlyJsonschema: FormlyJsonschema,
  ) {
  }

  ngOnInit() {
    this.fields = [this.formlyJsonschema.toFieldConfig(this.schema)];
    this.fields.forEach(object => {
      object.fieldGroup.forEach(field => {
        if (!this.allowedFieldTypes.includes(field.type)) {
          field.type = 'string';
        }
      });
    });
  }

  submit() {
    if (this.form.valid) {
      this.update.emit(this.json);
    }
  }
}
