/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, MatSelectModule } from '@angular/material';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyMaterialModule } from '@ngx-formly/material';

import { SchemaFormComponent } from './schema-form/schema-form.component';
import { NullTypeComponent } from './types/null.type';
import { ObjectTypeComponent } from './types/object-type/object-type.component';

export function minlengthValidationMessage(err, field) {
  return `Should have atleast ${field.templateOptions.minLength} characters`;
}

export function maxlengthValidationMessage(err, field) {
  return `This value should be less than ${field.templateOptions.maxLength} characters`;
}

export function minValidationMessage(err, field) {
  return `This value should be more than ${field.templateOptions.min}`;
}

export function maxValidationMessage(err, field) {
  return `This value should be less than ${field.templateOptions.max}`;
}

@NgModule({
  declarations: [SchemaFormComponent, ObjectTypeComponent, NullTypeComponent],
  exports: [SchemaFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatSelectModule,
    FormlyModule.forRoot({
      validationMessages: [
        { name: 'required', message: 'This field is required' },
        { name: 'minlength', message: minlengthValidationMessage },
        { name: 'maxlength', message: maxlengthValidationMessage },
        { name: 'min', message: minValidationMessage },
        { name: 'max', message: maxValidationMessage },
      ],
      types: [
        {
          name: 'string',
          extends: 'input',
          defaultOptions: {
            templateOptions: {
              appearance: 'outline',
            },
          },
        },
        {
          name: 'number',
          extends: 'input',
          defaultOptions: {
            templateOptions: {
              type: 'number',
              appearance: 'outline',
            },
          },
        },
        {
          name: 'integer',
          extends: 'input',
          defaultOptions: {
            templateOptions: {
              type: 'number',
              appearance: 'outline',
            },
          },
        },
        {
          name: 'boolean',
          extends: 'checkbox',
          defaultOptions: {
            templateOptions: {
              appearance: 'outline',
            },
          },
        },
        {
          name: 'enum',
          extends: 'select',
          defaultOptions: {
            templateOptions: {
              appearance: 'outline',
            },
          },
        },
        { name: 'object', component: ObjectTypeComponent },
        { name: 'null', component: NullTypeComponent, wrappers: ['form-field'] },
      ],
    }),
    FormlyMaterialModule,
  ]
})
export class ModelFormsModule { }
