/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MediaGalleryModule } from 'app/visualisations/media-gallery/media-gallery.module';
import { VisualisationComponentsModule } from 'app/visualisations/visualisation-components/visualisation-components.module';

import { JsonEditorModule } from '../json-editor/json-editor.module';
import { DeviceStreamStatusComponent } from './device-stream-status/device-stream-status.component';

@NgModule({
  imports: [
    CommonModule,
    VisualisationComponentsModule,
    MediaGalleryModule,
    JsonEditorModule,
  ],
  declarations: [DeviceStreamStatusComponent],
  exports: [DeviceStreamStatusComponent],
})
export class DeviceInfoModule { }
