/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { StreamType } from 'app/core/models/queries/data-query';
import { ApiService } from 'app/core/services/api/api.service';
import { DataApiService } from 'app/core/services/api/data-api.service';
import { DeviceApiService } from 'app/core/services/api/device-api.service';
import { ImageApiService } from 'app/core/services/api/image-api.service';
import { DateRange } from 'app/core/services/visualisation/display-context.service';
import * as moment from 'moment';

@Component({
  selector: 'device-stream-status',
  templateUrl: './device-stream-status.component.html',
  styleUrls: ['./device-stream-status.component.scss']
})
export class DeviceStreamStatusComponent implements OnInit, OnChanges {
  StreamType = StreamType;
  @Input() device;
  @Input() dateRange: DateRange;
  streams: any[] = [];

  constructor(
    protected api: ApiService,
    protected deviceApi: DeviceApiService,
    private dataApi: DataApiService,
    private imageApi: ImageApiService,
    private ref: ChangeDetectorRef,
  ) { }

  ngOnInit() {
    this.getStreams();
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('device' in changes && changes.device.currentValue !== changes.device.previousValue) {
      this.getStreams();
    } else if ('dateRange' in changes) {
      const currentTo = moment(changes.dateRange.currentValue['to']);
      const prevTo = moment(changes.dateRange.previousValue['to']);
      if (currentTo.diff(prevTo) > 1000) {
        this.getStreams();
      }
    }
  }

  getStreams() {
    this.deviceApi.getDeviceStreams(this.device.key).subscribe(streamList => {
      this.streams = streamList.map((stream) => {
        const streamName = stream.name ? stream.name : stream.key;
        return {
          key: stream.key,
          name: streamName,
          metadata: stream.metadata,
          type: stream.type,
          scaling: 1,
          ready: false,
          data: []
        };
      });

      streamList.forEach((stream, i) => {
        if (stream.type === StreamType.IMAGE) {
          this.imageApi.getImages([stream.key]).subscribe(data => {
            if (this.streams[i] != null) {
              const images = data[0].values.reduce((acc, val) => {
                acc.push(val.images);
                return acc;
              }, []);
              this.streams[i].data = images;
              this.streams[i].ready = true;
            }
          });
        } else if (stream.type === StreamType.DOUBLE
          || stream.type === StreamType.INTEGER) {
          this.deviceApi.getStreamAggregations([stream.key], this.dateRange, 3600, stream.type, ['avg']).subscribe(data => {
            const mappedData = this.dataApi.getGraphData(data, 'avg', stream.name);
            if (this.streams[i] != null) {
              this.streams[i].data = mappedData;
              this.streams[i].ready = true;
            }
          });
        } else if (stream.type === StreamType.JSON) {
          this.deviceApi.getJson([stream.key], this.dateRange).subscribe(data => {
            if (this.streams[i] != null) {
              this.streams[i].data = data[0].values;
              this.streams[i].ready = true;
            }
          });
        } else if (stream.type === StreamType.CONFIG) {
          this.deviceApi.getConfig([stream.key], 1).subscribe(data => {
            if (this.streams[i] != null) {
              this.streams[i].data = data[0].values;
              this.streams[i].ready = true;
            }
          });
        }
      });
    });
  }
}
