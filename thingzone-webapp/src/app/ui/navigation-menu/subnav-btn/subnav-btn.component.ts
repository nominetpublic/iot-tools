/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { RoutingLink } from 'app/core/models/common/routing-link';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'subnav-btn',
  templateUrl: './subnav-btn.component.html',
  styleUrls: ['./subnav-btn.component.scss']
})
export class SubnavBtnComponent implements OnInit {

  @Input() link: RoutingLink;

  constructor() { }

  ngOnInit() {
  }

}
