/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faBell, faCalendarAlt, faChartBar, faEye } from '@fortawesome/free-regular-svg-icons';
import {
  faAngleLeft,
  faAngleRight,
  faBullseye,
  faChartArea,
  faCog,
  faGlobe,
  faHome,
  faMapMarkedAlt,
  faSitemap,
  faThLarge,
  faUser,
  faWaveSquare,
} from '@fortawesome/free-solid-svg-icons';

import { SideMenuComponent } from './side-menu/side-menu.component';
import { SubnavBtnComponent } from './subnav-btn/subnav-btn.component';
import { PageWrapperComponent } from './subnav-page-wrapper/subnav-page-wrapper.component';
import { SubnavComponent } from './subnav/subnav.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FontAwesomeModule,
  ],
  declarations: [
    PageWrapperComponent,
    SubnavComponent,
    SubnavBtnComponent,
    SideMenuComponent
  ],
  exports: [
    PageWrapperComponent,
    SubnavComponent,
    SideMenuComponent,
  ],
})
export class NavigationMenuModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faAngleRight, faAngleLeft, faUser, faBell, faChartBar, faCog, faThLarge, faGlobe,
      faEye, faMapMarkedAlt, faHome, faSitemap, faWaveSquare, faCalendarAlt, faBullseye, faChartArea);
  }
}
