/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { MatButtonToggleChange } from '@angular/material/button-toggle';
import { MatSelectChange } from '@angular/material/select';
import { SelectOption } from 'app/core/models/common/select-option';

@Component({
  selector: 'dynamic-select',
  templateUrl: './dynamic-select.component.html',
  styleUrls: ['./dynamic-select.component.scss']
})
export class DynamicSelectComponent implements OnChanges {

  @Input() model;
  @Input() name;
  @Input() placeHolder = '';
  @Input() required = false;
  @Input() options: SelectOption[] = [];
  @Input() upperBound = 4;
  @Input() lowerBound = 2;
  @Input() disabled = false;
  @Input() selectClass = '';
  @Input() hint = null;
  @Input() emptyHint = null;
  @Input() multiple = false;

  @Output() modelChange = new EventEmitter();

  view = 'select';

  constructor() { }

  ngOnChanges(changes) {
    const currentValue = changes.options && changes.options.currentValue;
    if (currentValue) {
      const length = currentValue.length;
      this.view = this.multiple || (length < this.lowerBound || length >= this.upperBound) ? 'select' : 'row';
    }
  }

  selectRadio(val) {
    if (!this.disabled) {
      this.model = val;
      this.updateValue(val);
    }
  }

  onChange(event: MatSelectChange) {
    this.updateValue(event.value);
  }

  onButtonGroupChange(event: MatButtonToggleChange) {
    this.updateValue(event.value);
  }

  updateValue(value) {
    this.model = value;
    this.modelChange.emit(this.model);
  }

}
