/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatCalendar } from '@angular/material';
import * as moment from 'moment';

@Component({
  selector: 'date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DatePickerComponent implements OnChanges {

  @Input() minDate: moment.Moment;
  @Input() maxDate: moment.Moment;

  @Input() date: moment.Moment;
  @Input() title = '';
  @Input() error = false;

  @Output() dateChange: EventEmitter<moment.Moment> = new EventEmitter();

  @ViewChild('datePicker', { static: true }) calendar: MatCalendar<moment.Moment>;

  hrsArr = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];
  minArr = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
    24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44,
    45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59];

  timeForm = this.formBuilder.group({
    hrs: [0],
    mins: [0],
    seconds: [0],
  });

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ('date' in changes) {
      if (changes.date.firstChange || !moment(changes.date.previousValue).isSame(this.date)) {
        this.initDate(this.date);
      }
    }
  }

  dateSelect(date: moment.Moment): void {
    this.date = date;
    this.dateChange.emit(date);
  }

  formatDate(date: moment.Moment): string {
    if (date == null) {
      return 'now';
    }
    if (moment.isMoment(date)) {
      return date.format('lll');
    } else {
      return date;
    }
  }

  momentDate(date: Date): moment.Moment {
    if (date == null) {
      return null;
    }
    return moment(date);
  }

  initDate(date: moment.Moment): void {
    if (date != null) {
      if (date instanceof Date) {
        date = moment(date);
      }
      if (moment.isMoment(date)) {
        this.date = date;
        this.initTimeForm(date);
      }
    }
    if (date == null) {
      this.initTimeForm(moment());
    }
  }

  initTimeForm(date: moment.Moment): void {
    this.timeForm.get('hrs').setValue(date.hours());
    this.timeForm.get('mins').setValue(date.minutes());
    this.timeForm.get('seconds').setValue(date.seconds());
  }

  updateTime(): void {
    const currentDate = moment.isMoment(this.date) ? this.date.clone() : moment();

    this.date = currentDate.hours(this.timeForm.get('hrs').value)
      .minutes(this.timeForm.get('mins').value)
      .seconds(this.timeForm.get('seconds').value);
    this.dateSelect(this.date);
  }

  formatNumbers(a): string {
    return a.toString().padStart(2, '0');
  }
}
