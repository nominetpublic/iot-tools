/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'date-range-selector',
  templateUrl: './date-range-selector.component.html',
  styleUrls: ['./date-range-selector.component.scss']
})
export class DateRangeSelectorComponent implements OnChanges {
  @Input() fromDate: moment.Moment;
  @Input() fromTitle = 'From: ';
  @Input() toDate: moment.Moment | string;
  @Input() toTitle = 'To: ';

  @Input() minFromDate: moment.Moment;
  @Input() maxFromDate: moment.Moment;
  @Input() minToDate: moment.Moment;
  @Input() maxToDate: moment.Moment;

  @Output() fromDateChange: EventEmitter<moment.Moment> = new EventEmitter();
  @Output() fromDateError: EventEmitter<boolean> = new EventEmitter();
  @Output() toDateChange: EventEmitter<moment.Moment> = new EventEmitter();
  @Output() toDateError: EventEmitter<boolean> = new EventEmitter();

  fromError = false;
  toError = false;

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    if ('fromDate' in changes) {
      if (changes.fromDate.firstChange || !moment(changes.fromDate.previousValue).isSame(this.fromDate)) {
        this.initFromDate(this.fromDate);
      }
    }
    if ('toDate' in changes) {
      if (changes.toDate.firstChange || !moment(changes.toDate.previousValue).isSame(this.toDate)) {
        this.initTodate(this.toDate);
      }
    }
  }

  fromDateSelect(date: moment.Moment): void {
    this.fromDate = date;
    let to;
    if (!moment.isMoment(this.toDate)) {
      to = moment();
    } else {
      to = this.toDate;
    }

    this.checkValidRange(this.fromDate, to);

    this.fromDateChange.emit(this.fromDate);
  }

  toDateSelect(date: moment.Moment): void {
    this.toDate = date;
    this.checkValidRange(this.fromDate, this.toDate);

    this.toDateChange.emit(this.toDate);
  }

  checkValidRange(from: moment.Moment, to: moment.Moment) {
    this.toError = false;
    this.fromError = false;
    // Dont let from be after to
    if (from.isAfter(to)) {
      this.fromError = true;
    }
    // Dont let to be before from
    if (to.isBefore(from)) {
      this.toError = true;
    }

    this.toDateError.emit(this.toError);
    this.fromDateError.emit(this.fromError);
  }

  momentDate(date): moment.Moment {
    if (date instanceof Date) {
      return moment(date);
    } if (moment.isMoment(date)) {
      return date;
    }
    return null;
  }

  initFromDate(date): void {
    this.fromDate = this.momentDate(date);
  }

  initTodate(date): void {
    this.toDate = this.momentDate(date);
  }
}
