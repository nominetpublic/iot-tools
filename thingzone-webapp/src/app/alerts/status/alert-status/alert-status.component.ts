/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EventType } from 'app/core/models/queries/data-query';
import { EventApiService } from 'app/core/services/api/event-api.service';
import { ItemSelectionService } from 'app/core/services/item-selection/item-selection.service';
import { ItemStoreService } from 'app/core/services/item-store/item-store.service';
import { PanelResizeService } from 'app/core/services/panel-resize/panel-resize.service';
import { SessionService } from 'app/core/services/session/session.service';
import { EventUtils } from 'app/core/utils/event-utils';
import * as moment from 'moment';
import { Subscription } from 'rxjs';

// Parent Class Services
@Component({
  selector: 'alert-status',
  templateUrl: './alert-status.component.html',
  styleUrls: ['./alert-status.component.scss'],
})
export class AlertStatusComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  loading = false;
  alert = null;
  alertId: string = null;
  streamKey: string = null;
  currentSeverity: string = null;
  deviceKeys: string[] = [];
  links: any[] = [];
  imageKeys: string[] = [];

  severityOptions = [
    { title: 'Low', value: 'Low' },
    { title: 'Medium', value: 'Medium' },
    { title: 'High', value: 'High' },
  ];

  statusOptions = [
    { title: 'Open', value: 'open' },
    { title: 'Closed', value: 'closed' },
  ];

  eventOptions = [
    { title: 'Alert', value: EventType.ALERT },
    { title: 'Generic', value: EventType.GENERIC },
    { title: 'Observation', value: EventType.OBSERVATION },
  ];

  constructor(protected panelResize: PanelResizeService,
    protected itemSelection: ItemSelectionService,
    protected itemStore: ItemStoreService,
    protected route: ActivatedRoute,
    protected router: Router,
    private session: SessionService,
    protected eventApi: EventApiService,
  ) { }

  ngOnInit() {
    this.panelResize.dispatch();

    const blurSubscription = this.itemSelection.$itemBlur.subscribe(() => {
      blurSubscription.unsubscribe();
      this.router.navigate(['../../../'], { relativeTo: this.route });
    });
    this.subscriptions.push(blurSubscription);

    this.route.params.subscribe(params => {
      if ('id' in params) {
        this.streamKey = params.stream;
        this.alertId = params.id;

        setTimeout(() => {
          this.selectItem(this.alertId);
          this.loadItem(this.streamKey, this.alertId);
        });
      } else {
        this.setAlert(null);
        this.itemSelection.leave();
      }
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
    this.panelResize.dispatch();
  }

  selectItem(key) {
    this.itemSelection.select(key);
  }

  loadItem(streamKey, id) {
    this.loading = true;
    this.eventApi.get(streamKey, id).subscribe(item => {
      if (!item) {
        this.close();
      } else {
        this.setAlert(item);
        this.loading = false;
      }
    }, () => {
      this.close();
    });
  }

  setAlert(alert) {
    this.alert = alert;

    this.currentSeverity = EventUtils.getSeverityName(this.alert.severity);
    this.deviceKeys = this.eventApi.getDeviceKeys(this.alert.relatedTo);
    this.links = this.eventApi.getLinkRelations(this.alert.relatedTo);
    this.imageKeys = this.eventApi.getImageKeys(this.alert.derivedFrom);
    if (!this.alert.read) {
      this.viewAlert();
    }
  }

  updateAlertItem(alert): void {
    this.alert = alert;
    this.itemStore.updateItemByKey(this.alertId, alert);
  }

  openDeviceSummary(key): void {
    this.router.navigate([`./device/${key}`], { relativeTo: this.route });
  }

  openImages(): void {
    this.router.navigate(['./images'], { relativeTo: this.route });
  }

  close(): void {
    this.itemSelection.blur();
  }

  updateSeverity(value: string): void {
    let newSeverity = 0;
    if (value === 'High') {
      newSeverity = 1;
    } else if (value === 'Medium') {
      newSeverity = EventUtils.severityRange[1][0];
    }
    this.alert.severity = newSeverity;
    this.eventApi.updateSeverity(this.alert.streamKey, this.alert.id, newSeverity).subscribe(res => {
      this.updateAlertItem(res);
    });
  }

  updateStatus(value: string): void {
    this.eventApi.updateStatus(this.alert.streamKey, this.alert.id, value).subscribe(res => {
      this.updateAlertItem(res);
    });
  }

  viewAlert(): void {
    const feedback = {
      'type': 'system',
      'category': 'read',
      'description': 'viewed'
    };
    this.eventApi.addFeedback(this.alert.streamKey, this.alert.id, feedback).subscribe(res => {
      this.updateAlertItem(res);
    });
  }

  sendFeedback(message): void {
    const feedback = {
      'timestamp': new Date().toISOString(),
      'user': this.session.getUser(),
      'type': 'comment',
      'category': 'category',
      'description': message
    };
    this.eventApi.addFeedback(this.alert.streamKey, this.alert.id, feedback).subscribe(res => {
      this.updateAlertItem(res);
    });
  }

  deleteFeedback(feedbackId): void {
    this.eventApi.deleteFeedback(this.alert.streamKey, this.alert.id, feedbackId).subscribe(res => {
      this.updateAlertItem(res);
    });
  }

  addTag(tag): void {
    this.eventApi.addTag(this.alert.streamKey, this.alert.id, tag).subscribe(res => {
      this.updateAlertItem(res);
    });
  }

  removeTag(tag): void {
    this.eventApi.deleteTag(this.alert.streamKey, this.alert.id, tag).subscribe(res => {
      this.updateAlertItem(res);
    });
  }

  formatDate(date: Date) {
    return moment(date).format('lll');
  }

  openLink(link: any) {
    window.open(link, '_blank');
  }
}
