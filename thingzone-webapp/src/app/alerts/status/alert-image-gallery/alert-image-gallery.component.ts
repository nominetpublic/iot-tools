/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EventApiService } from 'app/core/services/api/event-api.service';
import { ImageApiService } from 'app/core/services/api/image-api.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'alert-image-gallery',
  templateUrl: './alert-image-gallery.component.html',
  styleUrls: ['./alert-image-gallery.component.scss']
})
export class AlertImageGalleryComponent implements OnInit {
  alertId: string;
  streamKey: string;
  alert: object;
  imageData = [];
  images: BehaviorSubject<string[]> = new BehaviorSubject([]);
  imageCount: number;

  constructor(
    protected route: ActivatedRoute,
    private router: Router,
    private imageApi: ImageApiService,
    private eventApi: EventApiService
  ) { }

  ngOnInit() {
    this.route.parent.params.subscribe(params => {
      this.streamKey = params.stream;
      this.alertId = params.id;

      this.loadItem(this.streamKey, this.alertId);
    });
  }

  close() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  loadItem(streamKey, id) {
    this.eventApi.get(streamKey, id).subscribe(item => {
      if (!item) {
        return this.close();
      }

      this.images.next([]);
      this.imageCount = 0;
      this.alert = item;
      const imageIds: string[] = [];
      const imageStreams = {};
      if (item.derivedFrom !== null) {
        // Find all unique related devices
        item.derivedFrom.forEach(e => {
          if (e.klass === 'uk.nominet.iot.model.timeseries.Image') {
            this.imageCount++;
            if (e.streamKey && e.id != null) {
              imageStreams[e.streamKey] = 1;
              imageIds.push(e.id);
            }
          }
        });
      }
      const streamKeys: string[] = Object.keys(imageStreams);
      if (streamKeys.length > 0 && imageIds.length > 0) {
        this.imageApi.getImages(streamKeys, imageIds).subscribe(res => {
          const images = res[0].values.map(e => e.images);
          this.images.next(images);
        });
      }
    }, () => {
      this.close();
    });
  }
}
