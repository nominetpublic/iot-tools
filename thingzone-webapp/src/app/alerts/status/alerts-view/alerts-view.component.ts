/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DatePipe } from '@angular/common';
import { Component, Inject, InjectionToken, NgZone, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as MapboxDraw from '@mapbox/mapbox-gl-draw';
import { SearchTerm } from 'app/core/models/common/search-term';
import * as theme from 'app/core/models/map/mapbox-draw-theme.js';
import { SatelliteSelectControl } from 'app/core/models/map/satellite-select-control';
import { EventType } from 'app/core/models/queries/data-query';
import { EventApiService } from 'app/core/services/api/event-api.service';
import { ConfigService } from 'app/core/services/config/config.service';
import { GeoService } from 'app/core/services/geo/geo.service';
import { ItemSelectionService } from 'app/core/services/item-selection/item-selection.service';
import { ItemStoreService } from 'app/core/services/item-store/item-store.service';
import { PreferenceService } from 'app/core/services/preference/preference.service';
import { SearchService } from 'app/core/services/search/search.service';
import { DisplayContextService } from 'app/core/services/visualisation/display-context.service';
import { DateUtils } from 'app/core/utils/date-utils';
import { EventUtils } from 'app/core/utils/event-utils';
import { SavedSearch } from 'app/ui/search/models/active-search';
import { SearchResource } from 'app/ui/search/models/search-resource';
import { Feature, FeatureCollection } from 'geojson';
import * as mapboxgl from 'mapbox-gl';
import * as moment from 'moment';
import { combineLatest, of, Subject, Subscription } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';

export const FacetStoreService = new InjectionToken('facetstore');
export const MapStoreService = new InjectionToken('geostore');
export const DeviceMapStoreService = new InjectionToken('devicegeostore');

@Component({
  providers: [
    ItemSelectionService,
    ItemStoreService,
    { provide: MapStoreService, useClass: ItemStoreService },
    { provide: FacetStoreService, useClass: ItemStoreService },
    { provide: DeviceMapStoreService, useClass: ItemStoreService },
    DisplayContextService,
  ],
  selector: 'alerts-view',
  templateUrl: './alerts-view.component.html',
  styleUrls: ['./alerts-view.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AlertsViewComponent implements OnInit, OnDestroy {
  formatDate = DateUtils.formatDate;
  formatTime = DateUtils.formatTime;
  moment = moment;
  live = true;
  subscriptions: Subscription[] = [];
  private mapbox: mapboxgl.Map;
  private resizeFunction = this.resize.bind(this);
  private loadData = this.triggerUpdateMap.bind(this);
  updateMap$ = new Subject();
  popup: mapboxgl.Popup;
  alertPopup: mapboxgl.Popup;

  draw: MapboxDraw;
  isDrawing = false;
  geoProperty = 'geoboundary';
  markers: { [index: string]: mapboxgl.Marker } = {};
  context: any;
  openModal: string;
  defaultStyle: string;
  satelliteStyle: string;
  initialZoom: { zoom: number } = { zoom: 11 };
  zoom$ = new Subject<{ zoom: number }>();
  centre$ = new Subject<[number, number]>();

  private initialCentre: mapboxgl.LngLatLike;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private zone: NgZone,
    private datePipe: DatePipe,
    private search: SearchService,
    protected displayContext: DisplayContextService,
    private itemSelection: ItemSelectionService,
    private geo: GeoService,
    private preference: PreferenceService,
    private eventApi: EventApiService,
    private itemStore: ItemStoreService,
    private configService: ConfigService,
    @Inject(FacetStoreService) public facetStore: ItemStoreService,
    @Inject(MapStoreService) private mapStore: ItemStoreService,
  ) {
    this.initialCentre = configService.getConfigValue('initialCentre');
    this.defaultStyle = configService.getConfigValue('lightTheme');
    this.satelliteStyle = configService.getConfigValue('satelliteTheme');
  }

  ngOnInit() {
    window.addEventListener('panelResize', this.resizeFunction);
    this.setContext();

    const savedSearchSub = combineLatest([this.route.queryParamMap, this.search.alertSavedSearches$, this.route.parent.data])
      .subscribe(([params, searches, routeData]) => {
        if (params.has('search')) {
          if (searches == null) {
            this.search.queryAlertSavedSearches();
          }
          if (searches != null) {
            const searchName = params.get('search');
            const selected: SearchResource = searches.find(resource =>
              (resource.name === searchName));
            if (selected != null) {
              this.search.setSearch(selected.content);
            }
          }
        } else {
          if (this.search.getTerms().length > 0) {
            this.search.clearTerms();
          }
          if (routeData['filter'] != null) {
            this.search.setTerms([{ property: 'eventType', values: routeData['filter'] }]);
          }
        }
      });
    this.subscriptions.push(savedSearchSub);

    this.mapStore.setIdSchema();
    const searchModalSub = this.search.$modalOpen.subscribe((e: any) => {
      if (e === 'filter') {
        this.createSearchFilters();
      }
    });
    this.subscriptions.push(searchModalSub);

    const searchSub = this.search.$search.subscribe((search: SavedSearch) => {
      this.checkBounds(search.terms);
      this.loadData();
    });
    this.subscriptions.push(searchSub);

    const displayContextSub = this.displayContext.contextDebounced(context => {
      if (context != null) {
        this.context = context;
        this.loadData();
      }
    }, 1000);
    this.subscriptions.push(displayContextSub);

    this.mapInit();
  }

  ngOnDestroy() {
    if (this.mapbox) {
      this.mapbox.off('moveend', this.loadData);
      this.mapbox.remove();
    }

    window.removeEventListener('panelResize', this.resizeFunction);
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  resize() {
    setTimeout(() => {
      if (this.mapbox) {
        this.mapbox.resize();
      }
    });
  }

  mapInit() {
    this.geo.setApiKey(mapboxgl);
    this.mapbox = new mapboxgl.Map({
      container: 'alerts-map',
      style: this.defaultStyle,
      zoom: this.initialZoom.zoom,
      center: this.initialCentre
    });

    this.mapPreferences();
    this.mapControls();

    let initialLoad = true;
    this.mapbox.on('load', () => {
      this.mapLayers();
      this.mapListeners();
      this.mapSubscriptions();
      this.loadData();
      initialLoad = false;
    });
    this.mapbox.on('style.load', () => {
      if (!initialLoad) {
        this.mapLayers();
        this.loadData();
      }
    });

    this.popup = new mapboxgl.Popup({
      closeButton: false,
      closeOnClick: false,
      anchor: 'bottom',
      offset: [-8, -14],
    });

    this.alertPopup = new mapboxgl.Popup({
      closeButton: false,
      closeOnClick: false,
      anchor: 'left',
      offset: [30, -15],
    });
  }

  mapPreferences() {
    this.loadAlertIcons();

    const centreSub = this.centre$.subscribe(centre => {
      if (this.mapbox) {
        this.mapbox.jumpTo({ center: centre });
      }
    });
    this.subscriptions.push(centreSub);

    this.preference.getJsonPreference('alerts.default.location').subscribe(location => {
      if (location != null) {
        const geometry = this.geo.getGeometry(location);
        this.centre$.next(geometry.coordinates);
      }
    });

    const zoomSub = this.zoom$.subscribe(zoom => {
      if (this.mapbox) {
        this.mapbox.jumpTo(zoom);
      }
    });
    this.subscriptions.push(zoomSub);

    this.preference.getJsonPreference('alerts.default.zoom').subscribe((zoom: { zoom: number }) => {
      if (zoom != null) {
        this.zoom$.next(zoom);
      }
    });
  }

  mapControls() {
    this.draw = new MapboxDraw({
      displayControlsDefault: false,
      styles: theme,
      controls: {
        polygon: true,
        trash: true
      }
    });
    this.mapbox.addControl(this.draw);

    const satelliteControl = new SatelliteSelectControl(this.defaultStyle, this.satelliteStyle);
    this.mapbox.addControl(satelliteControl, 'top-right');
  }

  mapLayers() {
    this.mapbox.addSource('alert-source', {
      type: 'geojson',
      data: {
        'type': 'FeatureCollection',
        'features': []
      }
    });

    this.mapbox.addLayer({
      id: 'alerts',
      type: 'symbol',
      source: 'alert-source',
      filter: ['all', ['==', ['geometry-type'], 'Point'], ['==', ['get', 'eventType', ['get', 'point']], 'ALERT']],
      layout: {
        'icon-image': [
          'case',
          ['<', ['get', 'severity'], EventUtils.severityRange[0][1]],
          'severity-low',
          ['>', ['get', 'severity'], EventUtils.severityRange[2][0]],
          'severity-high',
          'severity-mid'
        ],
        'icon-size': 1,
        'icon-allow-overlap': true,
        'icon-anchor': 'bottom-left',
        'text-field': '{size}',
        'text-font': ['Arial Unicode MS Bold'],
        'text-size': 12,
        'text-offset': [1.4, -1.1],
        'text-optional': true,
        'symbol-z-order': 'viewport-y',
      },
      paint: { 'text-color': '#ffffff' },
    });

    this.mapbox.addLayer({
      id: 'events',
      type: 'circle',
      source: 'alert-source',
      filter: ['all', ['==', ['geometry-type'], 'Point'], ['!=', ['get', 'eventType', ['get', 'point']], 'ALERT']],
      paint: {
        'circle-color': ['case',
          ['<', ['get', 'severity'], EventUtils.severityRange[0][1]],
          '#8f8c88',
          ['>', ['get', 'severity'], EventUtils.severityRange[2][0]],
          '#d8000b',
          '#f7a307',
        ],
        'circle-radius': [
          'interpolate', ['linear'], ['zoom'],
          7, 5,
          18, 8
        ],
        'circle-stroke-width': 0,
      },
    });

    // display line alerts
    this.mapbox.addLayer({
      id: 'alert-line',
      type: 'line',
      source: 'alert-source',
      paint: {
        'line-color': ['case',
          ['<', ['get', 'severity'], EventUtils.severityRange[0][1]],
          '#8f8c88',
          ['>', ['get', 'severity'], EventUtils.severityRange[2][0]],
          '#d8000b',
          '#f7a307',
        ],
        'line-width': 3
      },
      filter: ['==', ['geometry-type'], 'LineString']
    });

    // display polygon alerts
    this.mapbox.addLayer({
      id: 'alert-shape',
      type: 'fill',
      source: 'alert-source',
      paint: {
        'fill-color': ['case',
          ['<', ['get', 'severity'], EventUtils.severityRange[0][1]],
          '#8f8c88',
          ['>', ['get', 'severity'], EventUtils.severityRange[2][0]],
          '#d8000b',
          '#f7a307',
        ],
        'fill-opacity': 0.4
      },
      filter: ['==', ['geometry-type'], 'Polygon']
    });
  }

  mapListeners() {
    this.mapbox.on('mousemove', 'alerts', e => this.setHover(e));
    this.mapbox.on('mouseleave', 'alerts', () => this.removeHover());

    this.mapbox.on('mousemove', 'events', e => this.setHover(e));
    this.mapbox.on('mouseleave', 'events', () => this.removeHover());

    this.mapbox.on('mousemove', 'alert-line', e => this.setHover(e));
    this.mapbox.on('mouseleave', 'alert-line', () => this.removeHover());

    this.mapbox.on('mousemove', 'alert-shape', e => this.setHover(e));
    this.mapbox.on('mouseleave', 'alerts-shape', () => this.removeHover());

    this.mapbox.on('moveend', this.loadData);

    this.mapbox.on('click', e => {
      // alerts
      const features = this.mapbox.queryRenderedFeatures(e.point, {
        layers: ['alerts']
      });

      if (features.length > 0) {
        // Map items fall outside of NgZone
        this.zone.run(() => {
          const properties = features[0].properties;
          const data = JSON.parse(properties.point);
          if (data != null) {
            this.viewAlert(data);
            this.itemSelection.select(data.id);
          }
        });
      }
    });

    this.mapbox.on('draw.modechange', (event) => {
      if (event.mode === 'draw_polygon') {
        this.isDrawing = true;
      } else if (event.mode === 'simple_select' && this.isDrawing) {
        this.createSearchBounds();
        this.isDrawing = false;
      }
    });
    this.mapbox.on('draw.update', (event) => {
      this.updateSearchBounds(event.features);
    });
    this.mapbox.on('draw.delete', (event) => {
      this.removeSearchBounds(event.features);
    });
    this.mapbox.on('draw.selectionchange', (event) => {
      this.showLabels();
    });
  }

  mapSubscriptions() {
    this.mapStore.onHover().subscribe((hover: any) => {
      this.mapHover(hover);
    });

    this.mapStore.onFocus().subscribe((e: any) => {
      if (!e) return false;
      if (e.item) return this.mapFocus(this.geo.getCentre(e.item.geometry));

      this.itemStore.getItemByKey(e.key).subscribe((item: any) => {
        if (item && item.item) {
          this.mapFocus(this.geo.getCentre(item.item.location));
        }
      });
    });

    const updateMapSub = this.updateMap$.pipe(
      switchMap(() =>
        this.eventApi.geo({ from: this.context.range.from, to: this.context.current },
          this.search.getTerms(),
          [this.geo.getMapBoundingBox(this.mapbox)])
          .pipe(
            catchError(error => {
              return of(null);
            })
          ))
    ).subscribe(res => {
      this.updateMapEvents(res);
    });
    this.subscriptions.push(updateMapSub);
  }

  setHover(event): void {
    if ('features' in event && event.features.length > 0) {
      this.mapbox.getCanvas().style.cursor = 'pointer';

      const key = event.features[0].properties.id;
      this.zone.run(() => {
        this.itemSelection.hover(key);
      });
    }
  }

  removeHover(): void {
    this.mapbox.getCanvas().style.cursor = '';
    this.zone.run(() => {
      this.itemSelection.leave();
    });
  }

  createSearchFilters() {
    this.eventApi.getFacets().subscribe(res => {
      const searchFilters = [{
        title: 'New',
        property: 'new',
        format: 'string',
        options: res.facets.new
      }, {
        title: 'Severity',
        property: 'severity',
        format: 'string',
        selectMode: 'single',
        options: [
          { id: 'High', value: 'High' },
          { id: 'Medium', value: 'Medium' },
          { id: 'Low', value: 'Low' }
        ]
      }, {
        title: 'Event Type',
        property: 'eventType',
        format: 'string',
        options: [
          { id: EventType.GENERIC, value: 'GENERIC' },
          { id: EventType.ALERT, value: 'ALERT' },
          { id: EventType.OBSERVATION, value: 'OBSERVATION' }
        ]
      }];
      if (res.facets.status.length > 0) {
        searchFilters.push({
          title: 'Status',
          property: 'status',
          format: 'string',
          options: [
            { id: '', value: '', 'title': 'unassigned' },
            { id: 'open', value: 'open' },
            { id: 'closed', value: 'closed' }
          ]
        });
      }
      searchFilters.push({
        title: 'Tags',
        property: 'tags',
        format: 'string',
        options: res.facets.tags
      });
      searchFilters.push({
        title: 'Types',
        property: 'types',
        format: 'string',
        options: res.facets.types
      });
      if (res.facets.devices.length > 0) {
        searchFilters.push({
          title: 'Devices',
          property: 'devices',
          format: 'string',
          options: res.facets.devices
        });
      }
      this.facetStore.setFacets(searchFilters);
    });
  }

  viewAlert(alert) {
    this.router.navigate([`./event/${alert.streamKey}/${alert.id}`], { relativeTo: this.route });
  }

  viewGallery(alert) {
    this.router.navigate([`./event/${alert.streamKey}/${alert.id}/images`], { relativeTo: this.route });
  }

  triggerUpdateMap() {
    this.updateMap$.next(true);
  }

  updateMapEvents(events) {
    if (this.mapbox && events != null) {
      this.setMapData(events.geojson.features, 'alert-source');
      events.geojson.features.map(feature => {
        feature.id = feature.properties.id;
      });
      this.mapStore.setResult(events.geojson.features);
    }
  }

  getAlert(id) {
    return this.itemStore.getItemByKey(id);
  }

  loadAlertIcons(): void {
    const addIcons = [];
    addIcons.push(this.addAlertIcon('high', 'rgba(216, 0, 11, 0.6)'));
    addIcons.push(this.addAlertIcon('mid', 'rgba(247, 163, 7, 0.6)'));
    addIcons.push(this.addAlertIcon('low', 'rgba(143, 140, 137, 0.6)'));
  }

  addAlertIcon(severity, colour) {
    const icon = this.drawTriangle(colour);
    return this.mapbox.addImage(`severity-${severity}`, icon);
  }

  drawTriangle(colour) {
    const side = 33;
    const h = side * (Math.sqrt(3) / 2);
    const canvas = document.createElement('canvas');
    canvas.width = side + 3;
    canvas.height = side + 3;
    const cx = canvas.width / 2;
    const cy = canvas.height / 2;
    const ctx = canvas.getContext('2d');

    ctx.strokeStyle = '#FFFFFF';
    ctx.lineWidth = 3;
    ctx.fillStyle = colour;
    ctx.translate(cx, cy);
    ctx.beginPath();
    ctx.moveTo(0, -h / 2);
    ctx.lineTo(-side / 2, h / 2);
    ctx.lineTo(side / 2, h / 2);
    ctx.lineTo(0, -h / 2);
    ctx.stroke();
    ctx.fill();
    ctx.closePath();

    return ctx.getImageData(0, 0, canvas.width, canvas.height);
  }

  setMapData(data, source) {
    if (!this.mapbox || !this.mapbox.getSource) return;

    const datasource: any = this.mapbox.getSource(source);
    datasource.setData({
      'type': 'FeatureCollection',
      'features': data
    });
  }

  mapHover(selection) {
    if (selection === null) {
      this.popup.remove();
      this.alertPopup.remove();
    } else {
      this.mapStore.getItemByKey(selection.key).subscribe((res: any) => {
        if (res && res.item != null) {
          const alerts = res.item.properties.combinedPoints;
          const alertCount = alerts.length;
          const alertCountText = alertCount > 1 ? `${alertCount} Events` : `${alertCount} Event`;

          let alertDetails = '';
          for (let i = 0; i < alertCount; i++) {
            const item = alerts[i];
            const date = this.datePipe.transform(item.timestamp, 'MMM d, HH:mm');
            if (i >= 3 && alertCount >= 3) {
              alertDetails += `...`;
              break;
            } else {
              alertDetails += `<div class='alert-name'>${item.name}</div>
                <div class='alert-date'>${date}</div>`;
            }
          }
          this.alertPopup.setLngLat(this.geo.getCentre(res.item.geometry))
            .setHTML(
              `<div class='alert-popup'><div class='alert-count'>${alertCountText}</div>
                <div class='alert-details'>
                ${alertDetails}
                </div>
              </div>`)
            .addTo(this.mapbox);
        }
      });
    }
  }

  mapFocus(coordinates) {
    if (!this.mapbox) return;

    this.mapbox.easeTo({
      center: [coordinates[0], coordinates[1]],
      zoom: 14,
    });
  }

  /**
   * Add labels to selected features
   */
  showLabels(): void {
    const searchTerms: SearchTerm[] = this.search.getTerms();
    const index: number = searchTerms.findIndex(a => a.property === this.geoProperty);
    const selectedIds = this.draw.getSelectedIds();
    if (index !== -1 && selectedIds.length > 0) {
      const areas: FeatureCollection[] = searchTerms[index].values;
      areas.forEach((area: FeatureCollection, i: number) => {
        area.features.forEach((feature: Feature) => {
          if (selectedIds.some(id => feature.id === id)) {
            // create marker label
            const label = document.createElement('div');
            label.innerHTML = `AREA ${i + 1}`;
            const latLong = this.geo.getCentre(feature.geometry);
            if (feature.id in this.markers) {
              this.markers[feature.id].remove();
            }
            this.markers[feature.id] = new mapboxgl.Marker(label)
              .setLngLat(latLong)
              .addTo(this.mapbox);
          } else {
            if (feature.id in this.markers) {
              this.markers[feature.id].remove();
              delete this.markers[feature.id];
            }
          }
        });
      });
    } else {
      Object.keys(this.markers).forEach(key => {
        this.markers[key].remove();
        delete this.markers[key];
      });
    }
  }

  /**
   * A new polygon has been created, add it to the current search
   */
  createSearchBounds(): void {
    const selectedFeature: FeatureCollection = this.draw.getSelected();
    const searchTerms: SearchTerm[] = this.search.getTerms();
    const index: number = searchTerms.findIndex(a => a.property === this.geoProperty);

    // Check Exists
    if (index === -1) {
      this.search.addBlock(this.geoProperty, selectedFeature);
      return;
    }
    const existingSearchTerm: SearchTerm = searchTerms[index];
    existingSearchTerm.values.push(selectedFeature);
    this.search.setTerms(searchTerms);
  }

  /**
   * The selected features have moved or updated, make sure the matching search terms are also updated
   */
  updateSearchBounds(features: Feature[]): void {
    const searchTerms: SearchTerm[] = this.search.getTerms();
    const index: number = searchTerms.findIndex(a => a.property === this.geoProperty);
    // If multiple features are selected then split them for the searchTerms
    const featureCollections = features.map(feature =>
      ({ type: 'FeatureCollection', features: [feature] }));

    // If the geoproperty is not in the search, then add it and the currently selected areas
    if (index === -1) {
      this.search.addBlock(this.geoProperty, featureCollections);
      return;
    }

    // otherwise update the search with the current version of the selected features
    const existingSearchTerm: SearchTerm = searchTerms[index];
    for (const featureData of featureCollections) {
      const existingIndex = existingSearchTerm.values.findIndex((searchFeature: FeatureCollection) => {
        return searchFeature.features.some(feature => feature.id === featureData.features[0].id);
      });

      if (existingIndex !== -1) {
        existingSearchTerm.values[existingIndex] = featureData;
      } else {
        existingSearchTerm.values.push(featureData);
      }
    }
    this.search.setTerms(searchTerms);
  }

  /**
   * Remove search terms matching the deleted features
   */
  removeSearchBounds(features: Feature[]): void {
    const searchTerms: SearchTerm[] = this.search.getTerms();
    const index = searchTerms.findIndex(a => a.property === this.geoProperty);
    if (index === -1) return;

    const existingSearchTerm: SearchTerm = searchTerms[index];
    for (const featureData of features) {
      const existingIndex = existingSearchTerm.values.findIndex((featureCollection: FeatureCollection) => {
        return featureCollection.features.some(feature => feature.id === featureData.id);
      });

      if (existingIndex !== -1) {
        existingSearchTerm.values.splice(existingIndex, 1);
      }
    }
    if (existingSearchTerm.values.length > 0) {
      this.search.setTerms(searchTerms);
    } else {
      this.search.removeBlock(this.geoProperty);
    }
  }

  /**
   * The search has been updated, make sure that if an area has been deleted it is removed from the map
   */
  checkBounds(searchTerms: SearchTerm[]): void {
    if (this.draw) {
      const index: number = searchTerms.findIndex(a => a.property === this.geoProperty);
      const drawn: FeatureCollection = this.draw.getAll();
      if (index === -1) {
        // all areas are deleted, remove everything drawn on map
        if (drawn.features.length > 0) {
          const allIds = drawn.features.map((feature: Feature) => feature.id);
          this.draw.delete(allIds);
        }
        this.showLabels();
        return;
      }

      const currentIds = drawn.features.map((feature: Feature) => feature.id);
      const searchIds = [];
      const deletedIds = [];
      const areas: FeatureCollection[] = searchTerms[index].values;
      for (const area of areas) {
        area.features.forEach(feature => {
          searchIds.push(feature.id);
          // draw any missing features on the map
          if (!currentIds.some(id => feature.id === id)) {
            this.draw.add(feature);
          }
        });
      }
      for (const mapId of currentIds) {
        if (!searchIds.some(id => mapId === id)) {
          // delete any map features not in the search
          deletedIds.push(mapId);
        }
      }
      this.draw.delete(deletedIds);
      this.showLabels();
    }
  }

  setContext(): void {
    this.context = this.displayContext.createWidgetContext('2w');
    this.displayContext.setContext(this.context);
  }

  toggleModal(id: string): void {
    if (this.openModal === id) {
      this.openModal = null;
    } else {
      this.openModal = id;
    }
    // send resize event after change detection has happened
    setTimeout(this.resizeFunction, 0);
  }

  updateDates(value: moment.Moment, key: string) {
    const date = value.toDate();
    if (key === 'from') {
      this.displayContext.updateDateRange(this.context.range.to, date, this.context.range.to);
    } else if (key === 'to') {
      this.live = false;
      this.displayContext.updateDateRange(date, this.context.range.from, date);
    } else if (key === 'current') {
      this.live = false;
      this.displayContext.updateCurrentDate(date);
    }
  }

}
