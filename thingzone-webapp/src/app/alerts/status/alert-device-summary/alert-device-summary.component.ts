/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'app/core/services/api/api.service';
import { DeviceApiService } from 'app/core/services/api/device-api.service';
import * as moment from 'moment';

@Component({
  selector: 'alert-device-summary',
  templateUrl: './alert-device-summary.component.html',
  styleUrls: ['./alert-device-summary.component.scss']
})
export class AlertDeviceSummaryComponent implements OnInit {
  private deviceKey;
  streams = [];
  device: any;
  metadataKeys = [];
  rootKeys = ['name', 'type'];

  constructor(
    protected route: ActivatedRoute,
    private router: Router,
    protected api: ApiService,
    protected deviceApi: DeviceApiService,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.deviceKey = params.deviceId;
      setTimeout(() => {
        this.loadItem(this.deviceKey);
      });
    });
  }

  close() {
    this.router.navigate(['../../'], { relativeTo: this.route });
  }

  loadItem(deviceId) {
    this.deviceApi.getDevice(deviceId).subscribe(item => {
      this.device = item;

      const excluded = [];
      this.metadataKeys = Object.keys(this.device.metadata).reduce((acc, key) => {
        if (!excluded.includes(key)) acc.push(key);
        return acc;
      }, []).sort((a, b) => {
        if (a === b) return 0;
        if (a > b) return 1;
        return -1;
      });
    });
  }

  lastWeekRange() {
    const to = new Date();
    const from = moment().subtract(1, 'w').toDate();

    return { to, from };
  }
}
