/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ListRange } from '@angular/cdk/collections';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { AfterViewInit, Component, EventEmitter, Inject, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { EventApiService } from 'app/core/services/api/event-api.service';
import { ItemSelectionService } from 'app/core/services/item-selection/item-selection.service';
import { ItemStoreService } from 'app/core/services/item-store/item-store.service';
import { SearchService } from 'app/core/services/search/search.service';
import { DisplayContextService } from 'app/core/services/visualisation/display-context.service';
import { EventUtils } from 'app/core/utils/event-utils';
import { Subscription } from 'rxjs';

import { FacetStoreService } from '../alerts-view/alerts-view.component';

@Component({
  selector: 'alert-list',
  templateUrl: './alert-list.component.html',
  styleUrls: ['./alert-list.component.scss']
})
export class AlertListComponent implements OnInit, OnDestroy, AfterViewInit {

  @Output() alertClick: EventEmitter<any> = new EventEmitter();
  @Output() imageClick: EventEmitter<any> = new EventEmitter();

  @ViewChild(CdkVirtualScrollViewport, { static: false }) viewport: CdkVirtualScrollViewport;

  lastDate: Date = new Date(0);
  items = [];
  itemCount = 0;
  subscriptions: Subscription[] = [];
  itemSelect;
  itemHover;

  facets: Array<Object> = null;

  constructor(
    public itemStore: ItemStoreService,
    public itemSelection: ItemSelectionService,
    @Inject(FacetStoreService) private facetStore: ItemStoreService,
    private searchTerms: SearchService,
    private displayContext: DisplayContextService,
    protected eventApi: EventApiService
  ) { }

  ngOnInit() {
    this.searchTerms.closeModal();

    const contextSub = this.displayContext.contextDebounced(context => {
      if (context != null) {
        this.getEvents(context.range.from, context.current);
      }
    });
    this.subscriptions.push(contextSub);

    const facetSub = this.facetStore.facets$.subscribe(facets => {
      this.facets = facets;
    });
    this.subscriptions.push(facetSub);

    this.itemStore.setIdSchema();
    const itemSub = this.itemStore.items$.subscribe((e: any) => {
      this.items = e;
    });
    this.subscriptions.push(itemSub);
  }

  ngAfterViewInit() {
    const selectSub = this.itemStore.onSelect().subscribe((e: any) => {
      if (e != null) {
        this.itemSelect = e.key != null ? e.key : null;

        this.scrollToIndex(e.index);
      } else {
        this.itemSelect = null;
      }
    });
    this.subscriptions.push(selectSub);

    const hoverSub = this.itemStore.onHover().subscribe((e: any) => {
      const key = e && e.key;
      this.itemHover = key ? key : null;
    });
    this.subscriptions.push(hoverSub);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  getEvents(from: Date, to: Date) {
    this.eventApi.list({ from: from, to: to }, this.searchTerms.getTerms(), null).subscribe(response => {
      this.itemCount = response[0].total;
      this.replaceAlerts(this.eventApi.allValues(response));
    });
  }

  scrollToIndex(target) {
    if (target != null) {
      const range: ListRange = this.viewport.getRenderedRange();
      if (target < range.start || target > range.end) {
        this.viewport.scrollToIndex(target);
      }
    }
  }

  appendAlerts(response) {
    this.lastDate = new Date();
    // check for new items
    if (response.alerts.length > 0) {
      // keep old values when getting adding alerts
      response.alerts.push(...this.items);
      this.itemStore.setResult(response.alerts);
    }
  }

  replaceAlerts(response) {
    this.lastDate = new Date();
    this.itemStore.setResult(response.alerts);
  }

  itemById(index: number, item: any) {
    return item.id;
  }

  hover(alertId) {
    this.itemSelection.hover(alertId);
  }

  leave() {
    this.itemSelection.leave();
  }

  select(alert, event) {
    event.stopPropagation();
    this.itemSelection.select(alert.id);
    this.alertClick.emit({ streamKey: alert.streamKey, id: alert.id });
  }

  focusItem(alert, event) {
    event.stopPropagation();
    this.itemSelection.focus(alert.id);
    this.itemSelection.select(alert.id);
  }

  hasImages(alert) {
    const imageKeys = this.eventApi.getImageKeys(alert.derivedFrom);
    if (imageKeys.length > 0) {
      return true;
    }
    return false;
  }

  showGallery(alert, event) {
    event.stopPropagation();
    this.itemSelection.select(alert.id);
    this.imageClick.emit({ streamKey: alert.streamKey, id: alert.id });
  }

  isSelected(id) {
    return id === this.itemSelect;
  }

  isHovered(id) {
    return id === this.itemHover;
  }

  getSeverity(item) {
    return EventUtils.getSeverityName(item.severity);
  }
}
