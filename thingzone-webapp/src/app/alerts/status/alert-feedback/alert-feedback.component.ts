/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ConfirmDeleteDialogComponent } from 'app/ui/form-components/confirm-delete-dialog/confirm-delete-dialog.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'alert-feedback',
  templateUrl: './alert-feedback.component.html',
  styleUrls: ['./alert-feedback.component.scss']
})
export class AlertFeedbackComponent implements OnInit, OnDestroy {
  @Input() feedback;
  @Output() sendFeedback: EventEmitter<any> = new EventEmitter();
  @Output() deleteFeedback: EventEmitter<any> = new EventEmitter();

  subscriptions: Subscription[] = [];
  feedbackForm = new FormGroup({
    comment: new FormControl(''),
  });
  newFeedback = '';

  constructor(public dialog: MatDialog) {
  }

  ngOnInit() {
    const commentSub = this.feedbackForm.valueChanges.subscribe(formValues => {
      this.newFeedback = formValues.comment;
    });
    this.subscriptions.push(commentSub);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  send() {
    if (this.newFeedback !== '') {
      this.sendFeedback.emit(this.newFeedback);
      this.newFeedback = '';
    }
  }

  enterFeedback(event) {
    if (event.key === 'Enter') {
      this.send();
    }
  }

  delete(feedbackId) {
    const dialogRef = this.dialog.open(ConfirmDeleteDialogComponent, {
      data: { message: 'Would you like to delete this comment?', confirmName: false }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.deleteFeedback.emit(feedbackId);
      }
    });
  }
}
