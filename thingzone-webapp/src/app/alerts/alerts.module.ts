/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ScrollingModule } from '@angular/cdk/scrolling';
import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faDotCircle } from '@fortawesome/free-regular-svg-icons';
import { faCircle, faImages, faPen, faTimes, faUser } from '@fortawesome/free-solid-svg-icons';
import { CardComponentsModule } from 'app/ui/card-components/card-components.module';
import { DateTimeModule } from 'app/ui/date-time/date-time.module';
import { DeviceInfoModule } from 'app/ui/device-info/device-info.module';
import { DynamicSelectModule } from 'app/ui/dynamic-select/dynamic-select.module';
import { FormComponentsModule } from 'app/ui/form-components/form-components.module';
import { ItemPanelModule } from 'app/ui/item-panel/item-panel.module';
import { MapEditModule } from 'app/ui/map-edit/map-edit.module';
import { NavigationMenuModule } from 'app/ui/navigation-menu/navigation-menu.module';
import { PageOverviewModule } from 'app/ui/page-overview/page-overview.module';
import { SearchModule } from 'app/ui/search/search.module';
import { MediaGalleryModule } from 'app/visualisations/media-gallery/media-gallery.module';
import { VisualisationGalleryModule } from 'app/visualisations/visualisation-gallery/visualisation-gallery.module';

import { AlertsRoutingModule } from './alerts-routing.module';
import { AlertsConfigComponent } from './configuration/alerts-config/alerts-config.component';
import { AlertDeviceSummaryComponent } from './status/alert-device-summary/alert-device-summary.component';
import { AlertFeedbackComponent } from './status/alert-feedback/alert-feedback.component';
import { AlertImageGalleryComponent } from './status/alert-image-gallery/alert-image-gallery.component';
import { AlertListComponent } from './status/alert-list/alert-list.component';
import { AlertStatusComponent } from './status/alert-status/alert-status.component';
import { AlertsViewComponent } from './status/alerts-view/alerts-view.component';
import { AlertsWrapperComponent } from './sub-menu/alerts-wrapper/alerts-wrapper.component';

@NgModule({
  imports: [
    AlertsRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    ScrollingModule,
    FontAwesomeModule,
    SearchModule,
    NavigationMenuModule,
    PageOverviewModule,
    ItemPanelModule,
    CardComponentsModule,
    FormComponentsModule,
    DynamicSelectModule,
    MediaGalleryModule,
    MapEditModule,
    DeviceInfoModule,
    VisualisationGalleryModule,
    DateTimeModule,
  ],
  declarations: [
    AlertsWrapperComponent,
    AlertsConfigComponent,
    AlertsViewComponent,
    AlertListComponent,
    AlertStatusComponent,
    AlertFeedbackComponent,
    AlertDeviceSummaryComponent,
    AlertImageGalleryComponent,
  ],
  providers: [
    DatePipe,
  ]
})
export class AlertsModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faImages, faUser, faTimes, faDotCircle, faPen, faCircle);
  }
}
