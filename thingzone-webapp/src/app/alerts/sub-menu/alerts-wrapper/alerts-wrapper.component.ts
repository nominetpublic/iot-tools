/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RoutingLink } from 'app/core/models/common/routing-link';
import { SearchService } from 'app/core/services/search/search.service';
import { SearchResource } from 'app/ui/search/models/search-resource';

@Component({
  templateUrl: './alerts-wrapper.component.html',
  styleUrls: ['./alerts-wrapper.component.scss']
})
export class AlertsWrapperComponent implements OnInit {
  pageTitle = 'Alerts';
  leftLinks: RoutingLink[] = [
    { title: 'View', link: './view' },
  ];
  rightLinks: RoutingLink[] = [
  ];
  searchLinks: RoutingLink[] = [];
  routerLinks: RoutingLink[] = [];

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    protected search: SearchService) {
  }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.pageTitle = data['title'];
    });

    this.search.alertSavedSearches$.subscribe((searches: SearchResource[]) => {
      if (searches == null) {
        this.search.queryAlertSavedSearches();
      } else {
        this.searchLinks = searches
          .filter((resource: SearchResource) => resource.content.link)
          .map((resource: SearchResource) => {
            return { title: resource.name, link: './view', params: { search: resource.name } };
          });
        this.updateLinks();
      }
    });
    this.updateLinks();
  }

  updateLinks() {
    this.routerLinks = this.leftLinks.concat(this.searchLinks, this.rightLinks);
  }

}
