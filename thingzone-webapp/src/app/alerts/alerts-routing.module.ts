/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AlertsConfigComponent } from './configuration/alerts-config/alerts-config.component';
import { AlertDeviceSummaryComponent } from './status/alert-device-summary/alert-device-summary.component';
import { AlertImageGalleryComponent } from './status/alert-image-gallery/alert-image-gallery.component';
import { AlertStatusComponent } from './status/alert-status/alert-status.component';
import { AlertsViewComponent } from './status/alerts-view/alerts-view.component';
import { AlertsWrapperComponent } from './sub-menu/alerts-wrapper/alerts-wrapper.component';

const routes: Routes = [
  {
    path: '',
    component: AlertsWrapperComponent,
    children: [
      { path: '', redirectTo: 'view', pathMatch: 'full' },
      {
        path: 'view',
        component: AlertsViewComponent,
        children: [
          {
            path: 'event/:stream/:id',
            component: AlertStatusComponent,
            children: [
              { path: 'device/:deviceId', component: AlertDeviceSummaryComponent },
              { path: 'images', component: AlertImageGalleryComponent }
            ]
          },
        ],
      },
      { path: 'config', component: AlertsConfigComponent },
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AlertsRoutingModule { }
