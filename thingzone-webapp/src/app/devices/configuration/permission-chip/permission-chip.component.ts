/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Permission } from 'app/core/models/common/permission';
import { SelectOption } from 'app/core/models/common/select-option';
import { Subscription } from 'rxjs';
import { pairwise, startWith } from 'rxjs/operators';

@Component({
  selector: 'permission-chip',
  templateUrl: './permission-chip.component.html',
  styleUrls: ['./permission-chip.component.scss']
})
export class PermissionChipComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  Permission = Permission;

  @Input() key = '';
  @Input() permissions: Permission[] = [];
  @Input() keyreadonly = false;
  @Input() valreadonly = false;
  @Input() canDelete = true;
  @Input() stream = false;
  @Output() updateKey: EventEmitter<{ previous: string, updated: string }> = new EventEmitter();
  @Output() updatePermissions: EventEmitter<{ key: string, value: Permission[] }> = new EventEmitter();
  @Output() removePermission: EventEmitter<string> = new EventEmitter();

  nameControl = new FormControl('');

  availablePermissions: SelectOption[] = [
    { title: 'ADMIN', value: Permission.ADMIN },
    { title: 'MODIFY', value: Permission.MODIFY },
    { title: 'DISCOVER', value: Permission.DISCOVER },
    { title: 'METADATA', value: Permission.VIEW_METADATA },
  ];

  devicePermissions: SelectOption[] = [
    { title: 'LOCATION', value: Permission.VIEW_LOCATION },
  ];

  streamPermissions: SelectOption[] = [
    { title: 'CONSUME', value: Permission.CONSUME },
    { title: 'UPLOAD', value: Permission.UPLOAD },
    { title: 'UPLOAD_EVENTS', value: Permission.UPLOAD_EVENTS },
    { title: 'CONSUME_EVENTS', value: Permission.CONSUME_EVENTS },
  ];

  ngOnInit() {
    this.initFormValues();

    const keySub = this.nameControl.valueChanges
      .pipe(startWith(''), pairwise())
      .subscribe(([prev, next]: [string, string]) => {
        this.saveKey(prev, next);
      });
    this.subscriptions.push(keySub);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  initFormValues() {
    if (this.stream === true) {
      this.availablePermissions = this.availablePermissions.concat(this.streamPermissions);
    } else {
      this.availablePermissions = this.availablePermissions.concat(this.devicePermissions);
    }

    this.nameControl.setValue(this.key, { onlySelf: true, emitEvent: false });
    if (this.keyreadonly) {
      this.nameControl.disable();
    } else {
      this.nameControl.enable();
    }
  }

  // do not update local values, single source of truth is parent
  saveKey(previousKey: string, updatedKey: string) {
    if (!this.keyreadonly) {
      this.key = updatedKey;
      this.updateKey.emit({ previous: previousKey, updated: updatedKey });
    }
  }

  removeKey() {
    if (this.canDelete) this.removePermission.emit(this.key);
  }

  /**
   * toggle the selection of a permission
   */
  toggleSelect(permission: SelectOption): void {
    const index = this.permissions.findIndex((element) => element === permission.value);

    if (index >= 0) {
      // remove 'permission' from list of permissions
      this.permissions = (this.permissions.filter(a => a !== permission.value));
    } else {
      // add 'permission' to list of permissions
      this.permissions = ([...this.permissions, permission.value]);
    }
    this.updatePermissions.emit({
      key: this.key, value: this.permissions
    });
  }
}
