/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AfterViewInit, Component, Inject, InjectionToken, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DeviceApiService } from 'app/core/services/api/device-api.service';
import { AuthService } from 'app/core/services/auth/auth.service';
import { Privilege } from 'app/core/services/auth/privilege';
import { ItemSelectionService } from 'app/core/services/item-selection/item-selection.service';
import { ItemStoreService } from 'app/core/services/item-store/item-store.service';
import { SearchService } from 'app/core/services/search/search.service';
import { DeviceConfigDataSource } from 'app/devices/services/deviceConfigDataSource';
import { SearchResource } from 'app/ui/search/models/search-resource';
import { combineLatest, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';

export const FacetStoreService = new InjectionToken('facetstore');

@Component({
  providers: [
    ItemSelectionService,
    ItemStoreService,
    { provide: FacetStoreService, useClass: ItemStoreService },
  ],
  selector: 'devices-config',
  templateUrl: './devices-config.component.html',
  styleUrls: ['./devices-config.component.scss']
})
export class DevicesConfigComponent implements OnInit, AfterViewInit, OnDestroy {
  subscriptions: Subscription[] = [];
  Privilege = Privilege;
  devices = [];
  facets = [];
  selectedKey;
  currentModal = '';
  from = 0;
  total = 10;
  dataSource: DeviceConfigDataSource;
  displayedColumns = ['key', 'name', 'type'];

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  deviceCount: any;
  searchName: string = null;
  showCreate: boolean;

  constructor(
    public route: ActivatedRoute,
    public router: Router,
    public itemSelection: ItemSelectionService,
    public itemStore: ItemStoreService,
    @Inject(FacetStoreService) public facetStore: ItemStoreService,
    public search: SearchService,
    protected deviceApi: DeviceApiService,
    public authService: AuthService,
  ) { }

  ngOnInit() {
    this.dataSource = new DeviceConfigDataSource(this.deviceApi, this.itemStore, this.total);
    this.dataSource.initialList(this.search.getTerms());
    this.deviceCount = this.dataSource.entitiesCount$;

    const savedSearchSub = combineLatest([this.route.queryParamMap, this.search.deviceSavedSearches$]).subscribe(([params, searches]) => {
      if (params.has('search')) {
        if (searches == null) {
          this.search.queryDeviceSavedSearches();
        }
        if (searches != null) {
          this.searchName = params.get('search');
          const selected: SearchResource = searches.find(resource => (resource.name === this.searchName));
          if (selected != null) {
            this.search.setSearch(selected.content);
          }
        }
      } else {
        this.searchName = null;
        this.search.clearTerms();
      }
    });
    this.subscriptions.push(savedSearchSub);

    this.subscriptions.push(this.authService.checkUserRole(Privilege.CREATE_DEVICE).subscribe(hasPrivilege => {
      this.showCreate = hasPrivilege;
    }));

    this.search.closeModal();
    const facetSub = this.facetStore.facets$.subscribe(facets => {
      this.facets = facets;
    });
    this.subscriptions.push(facetSub);
    const searchSub = this.search.$search
      .pipe(
        tap((res) => {
          this.paginator.firstPage();
          this.dataSource.updateSearchTerms(res.terms);
        })
      )
      .subscribe();
    this.subscriptions.push(searchSub);
    const modalSub = this.search.$modalOpen.subscribe(modal => {
      this.currentModal = modal;
    });
    this.subscriptions.push(modalSub);
    const selectionSub = this.itemSelection.$itemSelect.subscribe(key => {
      this.selectedKey = key;
    });
    this.subscriptions.push(selectionSub);
    this.searchFacets();
  }

  ngAfterViewInit() {
    this.paginator.page
      .pipe(
        tap((res: PageEvent) => {
          const startIndex = this.paginator.pageIndex * res.pageSize;
          this.dataSource.paginateList(startIndex, startIndex + res.pageSize);
        }))
      .subscribe();
  }


  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  searchFacets() {
    this.deviceApi.deviceFacetSearch().subscribe((res) => {
      this.facetStore.setFacets(res.facets);
    });
  }

  editDevice(key) {
    this.itemSelection.select(key);
    const options = { relativeTo: this.route };
    if (this.searchName != null) {
      options['queryParams'] = { search: this.searchName };
    }
    this.router.navigate([`./edit/${key}`], options);
  }

  createDevice() {
    this.itemSelection.blur();
    const options = { relativeTo: this.route };
    if (this.searchName != null) {
      options['queryParams'] = { search: this.searchName };
    }
    this.router.navigate([`./new`], options);
  }
}
