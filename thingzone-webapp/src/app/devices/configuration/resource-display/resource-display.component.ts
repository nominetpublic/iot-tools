/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ResourceModel } from 'app/core/models/common/resource-model';

@Component({
  selector: 'resource-display',
  templateUrl: './resource-display.component.html',
  styleUrls: ['./resource-display.component.scss']
})
export class ResourceDisplayComponent implements OnInit {
  @Input() resource: ResourceModel;
  @Output() close = new EventEmitter();
  @Output() update = new EventEmitter();
  displayType: 'JSON' | 'FILE' | 'IMAGE' = 'JSON';

  constructor(
  ) { }

  ngOnInit() {
    if (this.resource.contentType === 'application/json') {
      this.displayType = 'JSON';
    } else if (this.resource.contentType.startsWith('image')) {
      this.displayType = 'IMAGE';
    } else {
      this.displayType = 'FILE';
    }
  }

  updateResource() {
    this.update.emit();
  }

  deleteResource() {
    this.updateResource();
    this.closeResource();
  }

  closeResource() {
    this.close.emit();
  }
}
