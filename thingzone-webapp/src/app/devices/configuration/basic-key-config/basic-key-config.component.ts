/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SelectOption } from 'app/core/models/common/select-option';
import { StreamType } from 'app/core/models/queries/data-query';
import { DeviceApiService } from 'app/core/services/api/device-api.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'basic-key-config',
  templateUrl: './basic-key-config.component.html',
  styleUrls: ['./basic-key-config.component.scss']
})
export class BasicKeyConfigComponent implements OnInit, OnChanges, OnDestroy {
  subscriptions: Subscription[] = [];
  @Input() title = 'Device Information';
  @Input() data;
  @Input() stream = false;
  @Input() new = false;
  @Input() showDelete = false;
  @Input() deleteMessage;
  @Output() save: EventEmitter<any> = new EventEmitter();
  @Output() delete: EventEmitter<any> = new EventEmitter();
  formListen = true;

  streamOptions: SelectOption[] = [
    { value: StreamType.INTEGER, title: 'Integer' },
    { value: StreamType.DOUBLE, title: 'Decimal' },
    { value: StreamType.IMAGE, title: 'Image' },
    { value: StreamType.EVENT, title: 'Event' },
    { value: StreamType.LATLONG, title: 'Geolocation' },
    { value: StreamType.JSON, title: 'JSON' },
    { value: StreamType.STRING, title: 'Text' },
    { value: StreamType.STATUS, title: 'Status' },
    { value: StreamType.CONFIG, title: 'Configuration' },
    { value: StreamType.INPUTDATAJSON, title: 'InputData JSON' },
  ];

  keyForm: FormGroup = this.formBuilder.group({
    key: [null, Validators.required],
    name: [null],
    type: [null],
  });

  constructor(
    private formBuilder: FormBuilder,
    protected deviceApi: DeviceApiService
  ) { }

  ngOnInit() {
    this.initialiseForm();
    this.formChanges();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.initialiseForm();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  initialiseForm() {
    this.formListen = false;
    this.keyForm.reset();
    this.new ? this.keyForm.get('key').enable() : this.keyForm.get('key').disable();
    this.keyForm.patchValue(this.data, { onlySelf: true, emitEvent: false });
    this.formListen = true;
  }

  formChanges() {
    this.subscriptions.push(this.keyForm.get('key').valueChanges.subscribe(value => {
      if (this.formListen) {
        this.save.emit({ key: value });
      }
    }));

    this.subscriptions.push(this.keyForm.get('name').valueChanges.subscribe(value => {
      if (this.formListen) {
        this.save.emit({ name: value });
      }
    }));

    this.subscriptions.push(this.keyForm.get('type').valueChanges.subscribe(value => {
      if (this.formListen) {
        this.save.emit({ type: value });
      }
    }));
  }

  deleteEntity() {
    this.delete.emit();
  }

  selectStreamType(value) {
    this.keyForm.patchValue({ type: value });
  }

}
