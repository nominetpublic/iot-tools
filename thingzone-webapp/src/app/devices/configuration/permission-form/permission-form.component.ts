/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { Permission } from 'app/core/models/common/permission';
import { PermissionsList } from 'app/core/models/common/permissions-list';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'permission-form',
  templateUrl: './permission-form.component.html',
  styleUrls: ['./permission-form.component.scss']
})
export class PermissionFormComponent implements OnInit, OnChanges {

  @Input() title: string;
  @Input() permissions: PermissionsList;
  @Input() stream = false;
  @Output() update: EventEmitter<any> = new EventEmitter();
  permissionsList: { key: string; values: any; }[];

  constructor() { }

  ngOnInit() {
    this.initPermissions(this.permissions);
  }

  ngOnChanges() {
    this.initPermissions(this.permissions);
  }

  initPermissions(initialPermissions: PermissionsList) {
    this.permissionsList = Object.keys(initialPermissions)
      .sort((a, b) => a.localeCompare(b))
      .map(key =>
        ({ key: key, values: this.permissions[key] })
      );
  }

  addPermission(): void {
    const newKey = '';
    this.permissions[newKey] = [Permission.DISCOVER];
    this.permissionsList.push({ key: newKey, values: this.permissions[newKey] });
    this.update.emit(this.permissions);
  }

  updatePermissionKey(keyValues: { previous: string, updated: string }): void {
    const previousValues = this.permissions[keyValues.previous];
    this.permissions[keyValues.updated] = previousValues;
    delete this.permissions[keyValues.previous];
    this.update.emit(this.permissions);
  }

  updatePermissions(keyValues: {key: string, value: Permission[]}): void {
    this.permissions[keyValues.key] = keyValues.value;
    this.update.emit(this.permissions);
  }

  removePermission(key: string): void {
    delete this.permissions[key];
    this.permissionsList = this.permissionsList.filter(item => item.key !== key);
    this.update.emit(this.permissions);
  }
}
