/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { PushSubscriber, StreamModel } from 'app/devices/domain-models/stream-model';
import { TransformModel } from 'app/devices/domain-models/transform-model';
import { Subscription } from 'rxjs';


@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'subscriber-config',
  templateUrl: './subscriber-config.component.html',
  styleUrls: ['./subscriber-config.component.scss']
})
export class SubscriberConfigComponent implements OnInit, OnChanges, OnDestroy {
  @Input() stream: StreamModel | TransformModel;
  @Output() update: EventEmitter<any> = new EventEmitter();
  @Output() error: EventEmitter<{ show: boolean, message: string }> = new EventEmitter();
  subscriptions: Subscription[] = [];
  formListen = true;
  pushSubscribers: PushSubscriber[] = [];

  pullSubscriptionForm = this.formBuilder.group({
    uri: [],
    uriParams: this.formBuilder.array([
      this.parameter
    ]),
    interval: [],
    aligned: [],
  });

  constructor(
    private formBuilder: FormBuilder,
    protected cd: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.initFormValues();
    this.formChanges();
  }

  ngOnChanges() {
    this.initFormValues();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  initFormValues() {
    this.formListen = false;
    this.pullParams.controls.splice(0);
    if (this.stream.pullSubscription != null) {
      const pull = { ...this.stream.pullSubscription };
      if (this.stream.pullSubscription.uriParams != null) {
        // need to initialise the uri params as a list of key value pairs in the form
        const parameterList = Object.keys(this.stream.pullSubscription.uriParams);

        parameterList.forEach(() => this.addPullParameterValue());
        pull['uriParams'] = parameterList.map((key) => ({ key: key, value: this.stream.pullSubscription.uriParams[key] }));
      }
      this.pullSubscriptionForm.patchValue(pull, { onlySelf: true, emitEvent: false });
    }
    this.formListen = true;

    if (this.stream.pushSubscribers != null) {
      this.pushSubscribers = JSON.parse(JSON.stringify(this.stream.pushSubscribers));
    }
  }

  formChanges() {
    this.subscriptions.push(this.pullSubscriptionForm.valueChanges.subscribe(values => {
      if (this.formListen) {
        if (!this.pullSubscriptionForm.invalid) {
          this.convertParameters(values, 'uriParams');
          this.update.emit({ pullSubscription: values });
        }
        this.error.emit({ show: this.hasError, message: 'Invalid pull subscription'});
      }
    }));
  }

  get hasError(): boolean {
    return this.pullSubscriptionForm.invalid;
  }

  convertParameters(values, key: string) {
    if (key in values) {
      const params = {};
      values[key].forEach(item => {
        if (item.key !== '' && item.value != null && item.value !== '') {
          params[item.key] = item.value;
        }
      });
      values[key] = params;
    }
  }

  get parameter(): FormGroup {
    return this.formBuilder.group({
      key: [],
      value: [],
    });
  }

  get pullParams(): FormArray {
    return this.pullSubscriptionForm.get('uriParams') as FormArray;
  }

  addPullParameterValue(): void {
    this.pullParams.push(this.parameter);
  }

  removePullParameterValue(index: number): void {
    this.pullParams.removeAt(index);
  }

  trackPush(index, push) {
    return push ? index : null;
  }

  addPush(): void {
    if (this.pushSubscribers == null) {
      this.pushSubscribers = [];
    }
    this.pushSubscribers.push({ uri: '' });
  }

  removePush(index: number): void {
    this.pushSubscribers.splice(index, 1);
    this.update.emit({ pushSubscribers: this.pushSubscribers });
  }

  updatePush(index: number, value: PushSubscriber): void {
    this.pushSubscribers[index] = value;
    this.update.emit({ pushSubscribers: this.pushSubscribers });
  }
}
