/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ResourceModel } from 'app/core/models/common/resource-model';
import { StreamType } from 'app/core/models/queries/data-query';
import { ApiService } from 'app/core/services/api/api.service';
import { DeviceApiService } from 'app/core/services/api/device-api.service';
import { AuthService } from 'app/core/services/auth/auth.service';
import { Privilege } from 'app/core/services/auth/privilege';
import { ItemSelectionService } from 'app/core/services/item-selection/item-selection.service';
import { PanelResizeService } from 'app/core/services/panel-resize/panel-resize.service';
import { PreferenceService } from 'app/core/services/preference/preference.service';
import { SessionService } from 'app/core/services/session/session.service';
import { DeviceModel } from 'app/devices/domain-models/device-model';
import { StreamModel } from 'app/devices/domain-models/stream-model';
import { DeviceStreamService } from 'app/devices/services/device-stream.service';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  providers: [DeviceStreamService],
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'device-detail-config',
  templateUrl: './device-detail-config.component.html',
  styleUrls: ['./device-detail-config.component.scss']
})
export class DeviceDetailConfigComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  Privilege = Privilege;

  newDevice = false;
  hasChanges = {};
  hasError: boolean;
  errorMessage = 'There was a problem saving this device';
  loading = false;

  device: DeviceModel;
  deviceCopy: any;
  deviceKey: string = null;
  dataStreams: StreamModel[];
  transforms: StreamModel[];
  shadowConfigList: StreamModel[] = [];
  showResource: ResourceModel;
  showDelete = false;

  constructor(
    private cd: ChangeDetectorRef,
    protected panelResize: PanelResizeService,
    private session: SessionService,
    public authService: AuthService,
    protected itemSelection: ItemSelectionService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected api: ApiService,
    protected deviceApi: DeviceApiService,
    private deviceStream: DeviceStreamService,
    private preferenceService: PreferenceService,
  ) { }

  ngOnInit() {
    this.panelResize.dispatch();

    this.route.params.subscribe(params => {
      if ('id' in params) {
        const key = params.id;
        this.deviceKey = key;

        setTimeout(() => {
          this.selectItem(key);
          this.loadItem(key);
        });
      } else {
        this.setDevice(null);
      }
    });

    const blurSubscription = this.itemSelection.$itemBlur.subscribe(() => {
      blurSubscription.unsubscribe();
      if (this.newDevice) {
        this.router.navigate(['../'], { relativeTo: this.route });
      } else {
        this.router.navigate(['../../'], { relativeTo: this.route });
      }
    });
    this.subscriptions.push(blurSubscription);

    const streamSub = this.deviceStream.dataStreams$.subscribe((streams) => {
      this.transforms = []; this.dataStreams = []; this.shadowConfigList = [];
      streams.forEach(stream => {
        if (stream.isTransform) {
          this.transforms.push(stream);
        } else if (!stream.isTransform) {
          this.dataStreams.push(stream);
        }
        if (stream.type === StreamType.CONFIG) {
          this.shadowConfigList.push(stream);
        }
      });
    });
    this.subscriptions.push(streamSub);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
    this.panelResize.dispatch();
  }

  openStream(key: string): void {
    this.hasError = false;
    this.errorMessage = '';
    this.router.navigate([`./stream/${key}`], { relativeTo: this.route });
  }

  newStream(type: string): void {
    this.hasError = false;
    this.errorMessage = '';
    this.router.navigate([`./${type}`], { relativeTo: this.route });
  }

  openShadow(key: string): void {
    this.hasError = false;
    this.errorMessage = '';
    this.router.navigate([`./shadow/${key}`], { relativeTo: this.route });
  }

  openResource(resource: ResourceModel): void {
    this.showResource = resource;
    this.cd.markForCheck();
  }

  closeResource(): void {
    this.showResource = null;
    this.cd.markForCheck();
  }

  loadItem(key): void {
    this.loading = true;

    this.deviceApi.getDevice(key).subscribe(item => {
      if (!item) return this.close();

      this.setDevice(item);
      this.loading = false;
    }, () => {
      this.close();
    });
  }

  selectItem(key): void {
    if (!this.itemSelection.getSelectedKey()) this.itemSelection.select(key);
  }

  setDevice(device: DeviceModel): void {
    this.hasChanges = {};
    if (device == null) {
      device = this.createDevice();
    } else {
      this.newDevice = false;
    }
    this.device = device;
    this.deviceCopy = JSON.parse(JSON.stringify(device));

    this.transforms = []; this.dataStreams = []; this.shadowConfigList = [];
    if ('dataStreams' in device && device['dataStreams'] != null) {
      this.deviceStream.setStreams(device.dataStreams);
    }

    if (device.location == null || device.location === {}) {
      this.preferenceService.getJsonPreference('device.default.location').subscribe((location) => {
        if (location != null) {
          this.device.location = location;
        }
      });
    }
    // can only see permissions if admin, can only delete a device if all datastreams have already been removed
    this.showDelete = (!this.newDevice && device.userPermissions != null && Object.keys(device.userPermissions).length > 0
      && this.dataStreams.length === 0);

    if (this.newDevice === false) {
      this.updateResources(this.device.key);
    }
    this.cd.markForCheck();
  }

  resetDevice(device) {
    this.hasChanges = {};
    this.device = device;
    this.deviceCopy = JSON.parse(JSON.stringify(this.device));
    this.cd.markForCheck();
  }

  createDevice(): DeviceModel {
    this.newDevice = true;
    const permissions = {};
    permissions[this.session.getUser()] = ['ADMIN'];
    const device = {
      dataStreams: [],
      userPermissions: permissions,
      groupPermissions: {}
    };
    return device;
  }

  saveDevice(): void {
    let changes: DeviceModel = { key: this.device.key };
    if (this.newDevice) {
      changes = this.device;
    } else {
      Object.keys(this.hasChanges).forEach(key => {
        changes[key] = this.device[key];
      });
    }
    if (this.newDevice) {
      this.deviceApi.validateKey(changes.key).pipe(take(1)).subscribe((res) => {
        if (res.error) {
          this.setError(res);
        } else {
          this.upsertDevice(changes);
        }
      }, err => {
        this.setError(err);
      });
    } else {
      this.upsertDevice(changes);
    }
  }

  upsertDevice(device: DeviceModel): void {
    this.deviceApi.saveDevice(device.key, device).subscribe(res => {
      if (this.newDevice) {
        // redirect to device edit page
        this.router.navigate([`../edit/${res.device.key}`], { relativeTo: this.route });
      } else {
        // update values with device returned from upsert
        this.setDevice(res.device);
        this.setError({ show: false, message: '' });
      }
    }, err => {
      this.setError({ show: true, message: 'There was a problem saving this device' });
    });
  }

  updateResources(key: string): void {
    this.preferenceService.listResourceEntity(key).subscribe(res => {
      this.device.resources = res;
      this.cd.markForCheck();
    });
  }

  discardChanges(): void {
    this.clearErrors();
    this.resetDevice(this.deviceCopy);
  }

  streamByKey(index: number, stream: any): string {
    return stream.key;
  }

  close(): void {
    this.itemSelection.blur();
  }

  setError(error: { show: boolean, message: string }): void {
    this.hasError = error.show;
    this.errorMessage = error.message;
    this.cd.markForCheck();
  }

  clearErrors() {
    this.setError({ show: false, message: '' });
  }

  keysChanged(): boolean {
    return Object.keys(this.hasChanges).length > 0;
  }

  updateDevice(formValues): void {
    this.clearErrors();
    Object.keys(formValues).forEach(key => {
      this.hasChanges[key] = true;
      this.device[key] = formValues[key];
    });
  }

  deleteDevice() {
    if (this.newDevice) return;


    this.deviceApi.deleteDevice(this.device.key).subscribe(() => {
      this.close();
    }, (e) => {
      this.setError({ show: true, message: e.error.reason });
    });
  }
}
