/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SelectOption } from 'app/core/models/common/select-option';
import { TransformApiService } from 'app/core/services/api/transform-api.service';
import { TransformModel } from 'app/devices/domain-models/transform-model';
import { BehaviorSubject, Subscription } from 'rxjs';

@Component({
  selector: 'transform-config',
  templateUrl: './transform-config.component.html',
  styleUrls: ['./transform-config.component.scss']
})
export class TransformConfigComponent implements OnInit, OnChanges, OnDestroy {
  subscriptions: Subscription[] = [];
  @Input() transform: TransformModel;
  @Output() update: EventEmitter<any> = new EventEmitter();
  @Output() error: EventEmitter<{ show: boolean, message: string }> = new EventEmitter();
  formListen = true;

  private functionOptions: BehaviorSubject<SelectOption[]> = new BehaviorSubject([]);
  functionOptions$ = this.functionOptions.asObservable();

  transformForm: FormGroup = this.formBuilder.group({
    runSchedule: [],
    multiplexer: [false],
    functionName: [],
    params: this.formBuilder.array([
      this.parameter
    ]),
    outputStreams: this.formBuilder.array([
    ]),
    sources: this.formBuilder.array([
    ])
  });

  constructor(
    private formBuilder: FormBuilder,
    protected transformApi: TransformApiService) { }

  ngOnInit() {
    this.transformApi.listTranformFunctions().subscribe(res => {
      const options: SelectOption[] = res.map(stream => ({
        title: stream.name, value: stream.name
      }));
      this.functionOptions.next(options);
    });

    this.initFormValues();
    this.formChanges();
  }

  ngOnChanges() {
    this.initFormValues();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  initFormValues(): void {
    // don't trigger changes when initialising
    this.formListen = false;
    this.transformForm.patchValue(this.transform, { onlySelf: true, emitEvent: false });
    if (this.transform.function != null) {
      this.transformForm.patchValue({ functionName: this.transform.function.name }, { onlySelf: true, emitEvent: false });
    }
    if (this.transform.parameterValues != null && Object.keys(this.transform.parameterValues).length > 0) {
      const parameterList = Object.keys(this.transform.parameterValues);
      this.params.controls.splice(0);
      parameterList.forEach(() => this.addParameterValue());
      const parameters = parameterList.map((key) => ({ key: key, value: this.transform.parameterValues[key] }));
      this.transformForm.patchValue({ params: parameters }, { onlySelf: true, emitEvent: false });
    }
    if (this.transform.outputStreams != null) {
      this.outputStreams.controls.splice(0);
      this.transform.outputStreams.forEach(() => this.addOutputStream());
      this.transformForm.patchValue({ outputStreams: this.transform.outputStreams }, { onlySelf: true, emitEvent: false });
    }

    if (this.transform.sources != null) {
      this.sources.controls.splice(0);
      this.transform.sources.forEach(() => this.addSource());
      this.transformForm.patchValue({ sources: this.transform.sources }, { onlySelf: true, emitEvent: false });
    }
    this.formListen = true;
  }

  formChanges() {
    // Separate subscriptions so that only values that have changed are emitted
    this.subscriptions.push(this.transformForm.get('runSchedule').valueChanges.subscribe(values => {
      if (this.formListen) {
        this.update.emit({ parameterValues: values });
      }
    }));
    this.subscriptions.push(this.transformForm.get('functionName').valueChanges.subscribe(values => {
      if (this.formListen) {
        this.update.emit({ functionName: values });
      }
    }));
    this.subscriptions.push(this.transformForm.get('params').valueChanges.subscribe(values => {
      if (this.formListen) {
        const parameters = {};
        values.forEach((param) => {
          parameters[param.key] = param.value;
        });
        this.update.emit({ parameterValues: parameters });
      }
    }));
    this.subscriptions.push(this.transformForm.get('outputStreams').valueChanges.subscribe(values => {
      if (this.formListen) {
        this.update.emit({ outputStreams: values });
      }
    }));
    this.subscriptions.push(this.transformForm.get('sources').valueChanges.subscribe(values => {
      if (this.formListen) {
        if (!this.transformForm.get('sources').invalid) {
          this.update.emit({ sources: values });
        }
        this.error.emit({ show: this.transformForm.get('sources').invalid, message: 'Invalid transform source' });
      }
    }));
  }

  selectFunctionName(value): void {
    this.transformForm.patchValue({ functionName: value });
  }

  get outputStreams(): FormArray {
    return this.transformForm.get('outputStreams') as FormArray;
  }

  get outputStream(): FormGroup {
    return this.formBuilder.group({
      alias: [],
      key: [],
    });
  }

  addOutputStream(): void {
    this.outputStreams.push(this.outputStream);
  }

  removeOutputStream(index: number): void {
    this.outputStreams.removeAt(index);
  }

  get sources(): FormArray {
    return this.transformForm.get('sources') as FormArray;
  }

  get source(): FormGroup {
    return this.formBuilder.group({
      alias: ['', Validators.required],
      source: ['', Validators.required],
      trigger: [false],
      aggregationType: [''],
      aggregationSpec: [''],
    });
  }

  addSource(): void {
    this.sources.push(this.source);
  }

  removeSource(index: number): void {
    this.sources.removeAt(index);
  }

  get params(): FormArray {
    return this.transformForm.get('params') as FormArray;
  }

  get parameter(): FormGroup {
    return this.formBuilder.group({
      key: [],
      value: [],
    });
  }

  addParameterValue(): void {
    this.params.push(this.parameter);
  }

  removeParameterValue(index: number): void {
    this.params.removeAt(index);
  }

}
