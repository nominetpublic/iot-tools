/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ResourceModel } from 'app/core/models/common/resource-model';
import { DeviceApiService } from 'app/core/services/api/device-api.service';
import { TransformApiService } from 'app/core/services/api/transform-api.service';
import { ItemSelectionService } from 'app/core/services/item-selection/item-selection.service';
import { PreferenceService } from 'app/core/services/preference/preference.service';
import { SessionService } from 'app/core/services/session/session.service';
import { StreamModel } from 'app/devices/domain-models/stream-model';
import { TransformModel } from 'app/devices/domain-models/transform-model';
import { DeviceStreamService } from 'app/devices/services/device-stream.service';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'stream-detail-config',
  templateUrl: './stream-detail-config.component.html',
  styleUrls: ['./stream-detail-config.component.scss']
})
export class StreamDetailConfigComponent implements OnInit {

  stream: StreamModel | TransformModel;
  newStream: boolean;
  isTransform = false;
  hasChanges = {};
  hasError = false;
  errorMessage = '';
  streamCopy: any;
  showResource: ResourceModel;
  newTitle = 'New Data Stream';
  showDelete = false;

  constructor(
    protected cd: ChangeDetectorRef,
    private session: SessionService,
    protected itemSelection: ItemSelectionService,
    private deviceStream: DeviceStreamService,
    private route: ActivatedRoute,
    private router: Router,
    protected deviceApi: DeviceApiService,
    private preferenceService: PreferenceService,
    protected transformApi: TransformApiService,
  ) {
  }

  ngOnInit() {
    // update the stream values if the params change or the streams observable is updated
    combineLatest([this.route.params, this.deviceStream.dataStreams$]).subscribe(([params, dataStreams]) => {
      if ('id' in params && dataStreams != null) {
        const key = params.id;
        const stream = dataStreams.find(a => a.key === key);

        if (stream == null) {
          this.setStream(null);
        } else {
          this.setStream(stream, stream.isTransform);
        }
      } else {
        if (this.router.url.endsWith('Transform')) {
          this.setStream(null, true);
        } else {
          this.setStream(null);
        }
      }
    });
  }

  saveStream(): void {
    const deviceKey = this.itemSelection.getSelectedKey();
    let changes: TransformModel | StreamModel = { key: this.stream.key };
    if (this.newStream) {
      changes = this.stream;
    } else {
      Object.keys(this.hasChanges).forEach(key => {
        changes[key] = this.stream[key];
      });
    }
    if (this.newStream) {
      this.deviceApi.validateKey(changes.key).pipe(take(1)).subscribe((res) => {
        if (res.error) {
          this.setError(res);
        } else {
          if (this.isTransform) {
            this.upsertTransform(changes, deviceKey);
          } else {
            this.upsertStream(changes, deviceKey);
          }
        }
      }, err => {
        this.setError(err);
      });
    } else {
      if (this.isTransform) {
        this.upsertTransform(changes, deviceKey);
      } else {
        this.upsertStream(changes, deviceKey);
      }
    }
  }

  upsertStream(stream: StreamModel, deviceKey: string): void {
    let deviceLinks = null;
    if (this.newStream) {
      deviceLinks = [deviceKey];
    }
    this.deviceApi.upsertStream(stream.key, stream, deviceLinks).subscribe((res) => {
      this.deviceStream.updateStreamByKey(stream.key, res);
      if (this.newStream) {
        this.router.navigate([`../stream/${stream.key}`], { relativeTo: this.route });
      } else {
        this.clearErrors();
      }
    }, (e) => {
      this.setError({ show: true, message: e.error.reason });
    });
  }

  upsertTransform(transform: TransformModel, deviceKey: string): void {
    let deviceLinks = null;
    if (this.newStream) {
      deviceLinks = [deviceKey];
    }
    this.transformApi.upsertTransform(transform.key, transform, deviceLinks).subscribe((res) => {
      res.isTransform = true;
      this.deviceStream.updateStreamByKey(transform.key, res);
      if (this.newStream) {
        this.router.navigate([`../stream/${transform.key}`], { relativeTo: this.route });
      } else {
        this.clearErrors();
      }
    }, (e) => {
      this.setError({ show: true, message: e.error.reason });
    });
  }

  openResource(resource: ResourceModel): void {
    this.showResource = resource;
    this.cd.markForCheck();
  }

  closeResource(): void {
    this.showResource = null;
    this.cd.markForCheck();
  }

  setStream(stream, isTransform = false): void {
    this.hasChanges = {};
    this.isTransform = isTransform;
    if (stream == null) {
      stream = this.createStream(isTransform);
    } else {
      this.newStream = false;
    }
    this.stream = stream;
    this.streamCopy = JSON.parse(JSON.stringify(stream));

    if (this.newStream === false) {
      this.updateResources(this.stream.key);
    }

    if (this.isTransform) {
      if (!this.newStream && stream.runSchedule == null) {
        this.transformApi.getTransform(stream.key).subscribe(result => {
          result['isTransform'] = true;
          this.stream = result as TransformModel;
          this.deviceStream.updateStreamByKey(stream.key, this.stream);
        });
      } else {
        this.stream = stream as TransformModel;
      }
    } else {
      if (!this.newStream && stream.devices == null) {
        this.deviceApi.getStream(stream.key).subscribe(result => {
          this.stream = result as StreamModel;
          this.deviceStream.updateStreamByKey(stream.key, this.stream);
        });
      } else {
        this.stream = stream as StreamModel;
      }
    }
    // can only see permissions if admin, can only delete a device if all datastreams have already been removed
    this.showDelete = (!this.newStream && stream.userPermissions != null && Object.keys(stream.userPermissions).length > 0);
  }

  resetStream(stream) {
    this.hasChanges = {};
    this.stream = stream;
    this.streamCopy = JSON.parse(JSON.stringify(this.stream));
    this.cd.detectChanges();
  }

  createStream(transform: boolean): StreamModel | TransformModel {
    this.newTitle = transform ? 'New Transform' : 'New Data Stream';
    this.newStream = true;
    const permissions = {};
    permissions[this.session.getUser()] = ['ADMIN'];
    const stream = {
      metadata: {},
      isTransform: transform,
      userPermissions: permissions,
      groupPermissions: {}
    };
    if (transform) {
      stream['parameterValues'] = {};
    }
    return stream;
  }

  validateKey(key): void {
    this.deviceApi.validateKey(key).subscribe((res) => {
      this.setError({ show: res.showError, message: res.errorMessage });
    });
  }

  close(): void {
    if (this.newStream) {
      this.router.navigate(['../'], { relativeTo: this.route });
    } else {
      this.router.navigate(['../../'], { relativeTo: this.route });
    }
  }

  discardChanges(): void {
    this.clearErrors();
    this.resetStream(this.streamCopy);
  }

  setError(error: { show: boolean, message } = { show: true, message: 'The form data is invalid' }): void {
    if (this.hasError !== error.show) {
      this.hasError = error.show;
      this.errorMessage = error.message;
      this.cd.detectChanges();
    }
  }

  clearErrors() {
    this.setError({ show: false, message: '' });
  }

  updateStream(formValues): void {
    this.clearErrors();
    Object.keys(formValues).forEach(key => {
      this.hasChanges[key] = true;
      this.stream[key] = formValues[key];
    });
    this.cd.markForCheck();
  }

  keysChanged(): boolean {
    return Object.keys(this.hasChanges).length > 0;
  }

  updateResources(key: string, forceUpdate = false): void {
    if (this.stream['resources'] == null || forceUpdate) {
      this.preferenceService.listResourceEntity(key).subscribe(res => {
        this.stream.resources = res;
        this.cd.markForCheck();
      });
    } else {
      this.stream.resources = [];
    }
  }

  deleteStream() {
    if (this.newStream) return;

    if (this.isTransform) {
      this.transformApi.deleteTransform(this.stream.key).subscribe(() => {
        this.deviceStream.removeStreamByKey(this.stream.key, this.stream);
        this.close();
      }, (e) => {
        this.setError({ show: true, message: e.error.reason });
      });
    } else {
      this.deviceApi.deleteStream(this.stream.key).subscribe(() => {
        this.deviceStream.removeStreamByKey(this.stream.key, this.stream);
        this.close();
      }, (e) => {
        this.setError({ show: true, message: e.error.reason });
      });
    }
  }
}
