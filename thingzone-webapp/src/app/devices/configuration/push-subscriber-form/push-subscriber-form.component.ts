/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material';
import { SelectOption } from 'app/core/models/common/select-option';
import { PushSubscriber } from 'app/devices/domain-models/stream-model';
import { Subscription } from 'rxjs';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'push-subscriber-form',
  templateUrl: './push-subscriber-form.component.html',
  styleUrls: ['./push-subscriber-form.component.scss']
})
export class PushSubscriberFormComponent implements OnInit, OnDestroy {

  @Input() push: PushSubscriber;
  @Output() update: EventEmitter<any> = new EventEmitter();
  @Output() remove: EventEmitter<any> = new EventEmitter();
  @Output() error: EventEmitter<boolean> = new EventEmitter();
  subscriptions: Subscription[] = [];
  formListen = true;
  pushSubscriberForm = this.formBuilder.group({
    uri: [],
    method: ['POST'],
    retrySequence: this.formBuilder.array([]),
    uriParams: this.formBuilder.array([
      this.parameter
    ]),
    headers: this.formBuilder.array([
      this.parameter
    ]),
  });
  retryOptions: SelectOption[] = [
    { value: '1m', title: '1 minute' },
    { value: '5m', title: '5 minutes' },
    { value: '30m', title: '30 minutes' },
    { value: '1h', title: '1 hour' },
    { value: '1d', title: '1 day' },
  ];
  retryControl = new FormControl('');

  constructor(
    private formBuilder: FormBuilder,
    protected cd: ChangeDetectorRef) { }

  ngOnInit() {
    this.initFormValues();
    this.formChanges();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  initFormValues() {
    this.formListen = false;
    const patch = { uri: this.push.uri };
    if (this.push.uriParams != null) {
      // nesting the key value pairs for the uri parameters in the push subscriber array
      const parameterList = Object.keys(this.push.uriParams);
      const pushParams = this.pushSubscriberForm.get('uriParams') as FormArray;
      pushParams.controls.splice(0);
      parameterList.forEach(() => pushParams.push(this.parameter));
      patch['uriParams'] = parameterList.map((key) => ({ key: key, value: this.push.uriParams[key] }));
    }
    if (this.push.headers != null) {
      const parameterList = Object.keys(this.push.headers);
      const pushHeaders = this.pushSubscriberForm.get('headers') as FormArray;
      pushHeaders.controls.splice(0);
      parameterList.forEach(() => pushHeaders.push(this.parameter));
      patch['headers'] = parameterList.map((key) => ({ key: key, value: this.push.headers[key] }));
    }
    if (this.push.retrySequence != null) {
      const retry: string[] = this.push.retrySequence.split(' ');
      const pushRetry = this.pushSubscriberForm.get('retrySequence') as FormArray;
      pushRetry.controls.splice(0);
      retry.forEach(() => pushRetry.push(new FormControl()));
      patch['retrySequence'] = retry;
    }
    if (this.push.method != null) {
      patch['method'] = this.push.method;
    }
    this.pushSubscriberForm.patchValue(patch, { onlySelf: true, emitEvent: false });
    this.formListen = true;
  }

  formChanges() {
    const pushSub = this.pushSubscriberForm.valueChanges.subscribe(values => {
      if (this.formListen) {
        if (!this.pushSubscriberForm.invalid) {
          this.convertParameters(values, 'uriParams');
          this.convertParameters(values, 'headers');
          if ('retrySequence' in values) {
            values['retrySequence'] = values['retrySequence'].join(' ');
          }
        }
      }
      this.update.emit(values);
    });
    this.subscriptions.push(pushSub);
  }

  get parameter(): FormGroup {
    return this.formBuilder.group({
      key: [''],
      value: [''],
    });
  }

  convertParameters(values, key: string) {
    if (key in values) {
      const params = {};
      values[key].forEach(item => {
        params[item.key] = item.value;
      });
      values[key] = params;
    }
  }

  removePush(): void {
    this.remove.emit();
  }

  get params(): FormArray {
    return this.pushSubscriberForm.get('uriParams') as FormArray;
  }

  addParameterValue(): void {
    this.params.push(this.parameter);
  }

  removeParameterValue(index: number): void {
    this.params.removeAt(index);
  }

  get headers(): FormArray {
    return this.pushSubscriberForm.get('headers') as FormArray;
  }

  addHeaderValue(): void {
    this.headers.push(this.parameter);
  }

  removeHeaderValue(index: number): void {
    this.headers.removeAt(index);
  }

  get retrySequence(): FormArray {
    return this.pushSubscriberForm.get('retrySequence') as FormArray;
  }

  removePushRetryValue(paramIndex: number): void {
    this.retrySequence.removeAt(paramIndex);
  }

  selectRetry(select: MatAutocompleteSelectedEvent): void {
    this.retrySequence.push(new FormControl(select.option.value));
    this.retryControl.setValue(null);
  }

}
