/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ResourceModel } from 'app/core/models/common/resource-model';

@Component({
  selector: 'entity-resources',
  templateUrl: './entity-resources.component.html',
  styleUrls: ['./entity-resources.component.scss']
})
export class EntityResourcesComponent implements OnInit {

  @Input() title = 'Resources';
  @Input() key: string;
  @Input() resources: ResourceModel[] = [];
  @Output() update: EventEmitter<string> = new EventEmitter();
  @Output() view: EventEmitter<ResourceModel> = new EventEmitter();
  showAddForm: boolean;
  addForm = this.formBuilder.group({
    formType: [],
  });

  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {

  }

  addResource(): void {
    this.showAddForm = true;
  }

  createResource(): void {
    this.showAddForm = false;
    this.update.emit();
  }

  viewResource(resource: ResourceModel): void {
    this.view.emit(resource);
  }
}
