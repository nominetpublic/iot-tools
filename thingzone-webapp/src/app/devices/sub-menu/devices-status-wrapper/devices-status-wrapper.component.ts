/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { RoutingLink } from 'app/core/models/common/routing-link';
import { SearchService } from 'app/core/services/search/search.service';
import { SearchResource } from 'app/ui/search/models/search-resource';
import { Subscription } from 'rxjs';

@Component({
  templateUrl: './devices-status-wrapper.component.html',
  styleUrls: ['./devices-status-wrapper.component.scss']
})
export class DevicesStatusWrapperComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  rightLinks: RoutingLink[] = [
    { title: 'Search', link: '.' },
  ];
  searchLinks: RoutingLink[] = [];

  routerLinks: RoutingLink[] = [];

  constructor(
    protected search: SearchService) {
  }

  ngOnInit() {
    const savedSearchSub = this.search.deviceSavedSearches$.subscribe((searches: SearchResource[]) => {
      if (searches == null) {
        this.search.queryDeviceSavedSearches();
      } else {
        this.searchLinks = searches
          .filter((resource: SearchResource) => resource.content.link)
          .map((resource: SearchResource) => {
            return { title: resource.name, link: '.', params: { search: resource.name } };
          });
        this.updateLinks();
      }
    });
    this.subscriptions.push(savedSearchSub);
    this.updateLinks();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  updateLinks() {
    this.routerLinks = this.searchLinks.concat(this.rightLinks);
  }
}
