/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'app/core/services/api/api.service';
import { DeviceApiService } from 'app/core/services/api/device-api.service';
import { ItemSelectionService } from 'app/core/services/item-selection/item-selection.service';
import { ItemStoreService } from 'app/core/services/item-store/item-store.service';
import { PanelResizeService } from 'app/core/services/panel-resize/panel-resize.service';
import * as moment from 'moment';
import { Observable, of, Subscription } from 'rxjs';
import { first, flatMap } from 'rxjs/operators';

@Component({
  selector: 'device-detail-status',
  templateUrl: './device-detail-status.component.html',
  styleUrls: ['./device-detail-status.component.scss']
})
export class DeviceDetailStatusComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  streams = [];
  metadataKeys = [];
  rootKeys = ['name', 'type'];
  loading = false;
  device = null;
  deviceKey: string = null;
  initialDate: Date;

  constructor(
    protected panelResize: PanelResizeService,
    protected itemSelection: ItemSelectionService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected api: ApiService,

    // Extra Services
    protected deviceApi: DeviceApiService,
    private itemStore: ItemStoreService,
  ) { }

  ngOnInit() {
    this.panelResize.dispatch();
    this.initialDate = new Date();

    this.route.params.subscribe(params => {
      if ('id' in params) {
        const key = params.id;
        this.deviceKey = key;

        setTimeout(() => {
          this.selectItem(key);
          this.loadItem(key);
        });
      } else {
        this.setDevice(null);
      }
    });

    const blurSubscription = this.itemSelection.$itemBlur.subscribe(() => {
      blurSubscription.unsubscribe();
      this.router.navigate(['../'], { relativeTo: this.route });
    });
    this.subscriptions.push(blurSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
    this.panelResize.dispatch();
  }

  loadItem(key): void {
    this.loading = true;

    this.getItem(key).subscribe(item => {
      if (!item) return this.close();

      this.setDevice(item);
      this.loading = false;
    }, () => {
      this.close();
    });
  }

  selectItem(key): void {
    if (!this.itemSelection.getSelectedKey()) this.itemSelection.select(key);
  }

  close(): void {
    this.itemSelection.blur();
  }

  setDevice(device): void {
    this.device = device;

    // Currently no excluded metadata
    const excluded = [];
    this.metadataKeys = Object.keys(device.metadata).reduce((acc, key) => {
      if (!excluded.includes(key)) acc.push(key);
      return acc;
    }, []).sort((a, b) => {
      if (a === b) return 0;
      if (a > b) return 1;
      return -1;
    });
  }

  getItem(key): Observable<any> {
    return this.itemStore.getItemByKey(key).pipe(flatMap((e: any) => {
      const item = e.item;
      if (item) {
        return of(item).pipe(first());
      } else {
        return this.deviceApi.getDevice(key);
      }
    }));
  }

  lastWeekRange() {
    const from = moment(this.initialDate).subtract(1, 'w').toDate();
    return { to: this.initialDate, from };
  }
}
