/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ListRange } from '@angular/cdk/collections';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { DeviceApiService } from 'app/core/services/api/device-api.service';
import { ItemSelectionService } from 'app/core/services/item-selection/item-selection.service';
import { ItemStoreService } from 'app/core/services/item-store/item-store.service';
import { SearchService } from 'app/core/services/search/search.service';
import { DeviceListDataSource } from 'app/devices/services/deviceListDataSource';
import { Observable, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';

import { FacetStoreService } from '../devices-status/devices-status.component';

@Component({
  selector: 'device-list',
  templateUrl: './device-list.component.html',
  styleUrls: ['./device-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeviceListComponent implements OnInit, OnDestroy, AfterViewInit {

  @Output() deviceClick: EventEmitter<any> = new EventEmitter();
  deviceHover = null;
  deviceSelect = null;
  dataSource: DeviceListDataSource;
  deviceCount: Observable<number>;

  modalOpen = false;
  facets: Array<Object> = null;

  subscriptions: Subscription[] = [];
  @ViewChild(CdkVirtualScrollViewport, { static: false }) viewport: CdkVirtualScrollViewport;

  constructor(
    public itemSelection: ItemSelectionService,
    @Inject(FacetStoreService) private facetStore: ItemStoreService,
    public itemStore: ItemStoreService,
    public search: SearchService,
    protected deviceApi: DeviceApiService,
  ) {
  }

  ngOnInit() {
    this.dataSource = new DeviceListDataSource(this.deviceApi, this.search, this.itemStore);
    this.deviceCount = this.dataSource.entitiesCount$;

    this.search.closeModal();
    const facetSub = this.facetStore.facets$.subscribe(facets => {
      this.facets = facets;
    });
    this.subscriptions.push(facetSub);

    const searchSub = this.search.$search
      .pipe(
        tap(() => this.dataSource.updateSearchTerms())
      )
      .subscribe();
    this.subscriptions.push(searchSub);
  }

  ngAfterViewInit() {
    const hoverSub = this.itemStore.onHover().subscribe((e: any) => {
      if (e != null) {
        this.deviceHover = e.key != null ? e.key : null;
        this.scrollToIndex(e.index);
      }
    });
    this.subscriptions.push(hoverSub);

    const selectSub = this.itemStore.onSelect().subscribe((e: any) => {
      if (e != null) {
        this.deviceSelect = e.key != null ? e.key : null;
        this.scrollToIndex(e.index);
      }
    });
    this.subscriptions.push(selectSub);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  getDeviceStatusColor(device) {
    if (device.status === undefined) return '#787878';
    else return (device.status) < 1 ? '#c51d23' : '#618700';
  }

  getDeviceStatusIcon(device) {
    if (device.status === undefined) return 'circle';
    return (device.status) < 1 ? 'exclamation-circle' : 'check-circle';
  }

  scrollToIndex(target) {
    if (target != null) {
      const range: ListRange = this.viewport.getRenderedRange();
      if (target < range.start || target > range.end) {
        this.viewport.scrollToIndex(target);
      }
    }
  }

  itemByKey(index: number, item: any) {
    if (item !== undefined) {
      return item.key;
    }
  }

  focusItem(device, event) {
    event.stopPropagation();
    this.itemSelection.focus(device.key);
  }

  hover(device) {
    this.itemSelection.hover(device.key);
  }

  leave(device) {
    this.itemSelection.leave();
  }

  select(device, event) {
    event.stopPropagation();
    this.deviceClick.emit(device.key);
  }

  openModal() {
    this.modalOpen = !this.modalOpen;
  }
}
