/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Inject, InjectionToken, NgZone, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as MapboxDraw from '@mapbox/mapbox-gl-draw';
import { SearchTerm } from 'app/core/models/common/search-term';
import * as theme from 'app/core/models/map/mapbox-draw-theme.js';
import { SatelliteSelectControl } from 'app/core/models/map/satellite-select-control';
import { DeviceApiService } from 'app/core/services/api/device-api.service';
import { ConfigService } from 'app/core/services/config/config.service';
import { GeoService } from 'app/core/services/geo/geo.service';
import { ItemSelectionService } from 'app/core/services/item-selection/item-selection.service';
import { ItemStoreService } from 'app/core/services/item-store/item-store.service';
import { PreferenceService } from 'app/core/services/preference/preference.service';
import { SearchService } from 'app/core/services/search/search.service';
import { SavedSearch } from 'app/ui/search/models/active-search';
import { SearchResource } from 'app/ui/search/models/search-resource';
import { Feature, FeatureCollection } from 'geojson';
import * as mapboxgl from 'mapbox-gl';
import { combineLatest, of, Subject, Subscription } from 'rxjs';
import { catchError, debounceTime, switchMap } from 'rxjs/operators';

export const MapStoreService = new InjectionToken('geostore');
export const FacetStoreService = new InjectionToken('facetstore');

/**
 * Show devices on a map
 */
@Component({
  providers: [
    ItemSelectionService,
    ItemStoreService,
    { provide: MapStoreService, useClass: ItemStoreService },
    { provide: FacetStoreService, useClass: ItemStoreService },
  ],
  selector: 'devices-status',
  templateUrl: './devices-status.component.html',
  styleUrls: ['./devices-status.component.scss']
})

export class DevicesStatusComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  protected map: mapboxgl.Map;
  private resizeFunction = this.resize.bind(this);
  private loadData = this.triggerUpdateMap.bind(this);

  draw: MapboxDraw;
  isDrawing = false;
  geoProperty = 'geoboundary';
  markers: { [index: string]: mapboxgl.Marker } = {};
  defaultStyle: string;

  updateMap$ = new Subject();
  initialZoom: { zoom: number } = { zoom: 11 };
  zoom$ = new Subject<{ zoom: number }>();
  centre$ = new Subject<[number, number]>();
  searchName: string = null;

  private initialCentre: mapboxgl.LngLatLike;
  private satelliteStyle: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private search: SearchService,
    private itemSelection: ItemSelectionService,
    private geo: GeoService,
    private preference: PreferenceService,
    private deviceApi: DeviceApiService,
    private zone: NgZone,
    private itemStore: ItemStoreService,
    @Inject(MapStoreService) private mapStore: ItemStoreService,
    @Inject(FacetStoreService) private facetStore: ItemStoreService,
    private configService: ConfigService
  ) {
    this.defaultStyle = configService.getConfigValue('lightTheme');
    this.initialCentre = configService.getConfigValue('initialCentre');
    this.satelliteStyle = configService.getConfigValue('satelliteTheme');
  }

  ngOnInit() {
    window.addEventListener('panelResize', this.resizeFunction);

    const savedSearchSub = combineLatest([this.route.queryParamMap, this.search.deviceSavedSearches$]).subscribe(([params, searches]) => {
      if (params.has('search')) {
        if (searches == null) {
          this.search.queryDeviceSavedSearches();
        }
        if (searches != null) {
          this.searchName = params.get('search');
          const selected: SearchResource = searches.find(resource => (resource.name === this.searchName));
          if (selected != null) {
            this.search.setSearch(selected.content);
          }
        }
      } else {
        this.searchName = null;
        this.search.clearTerms();
      }
    });
    this.subscriptions.push(savedSearchSub);

    const searchSub = this.search.$search.subscribe((search: SavedSearch) => {
      this.checkBounds(search.terms);
      this.loadData();
    });
    this.subscriptions.push(searchSub);

    this.mapStore.setIdSchema();

    this.geo.setApiKey(mapboxgl);
    this.map = new mapboxgl.Map({
      container: 'devices-map',
      style: this.defaultStyle,
      zoom: this.initialZoom.zoom,
      center: this.initialCentre,
    });
    this.mapControls();
    this.mapPreferences();

    let initialLoad = true;
    this.map.on('load', () => {
      this.mapLayers();
      this.mapListeners();
      this.mapSubscriptions();
      this.loadData();
      initialLoad = false;
    });
    this.map.on('style.load', () => {
      if (!initialLoad) {
        this.mapLayers();
        this.loadData();
      }
    });

    this.deviceApi.deviceFacetSearch().subscribe((res: any) => {
      this.facetStore.setFacets(res.facets);
    });
  }

  ngOnDestroy() {
    if (this.map) {
      this.map.off('moveend', this.loadData);
      this.map.remove();
    }

    window.removeEventListener('panelResize', this.resizeFunction);
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  resize() {
    setTimeout(() => {
      if (this.map) {
        this.map.resize();
      }
    });
  }

  mapLayers() {
    const hoverColors = ['#b81e23', '#84d800'];
    const hoverBorderColors = ['#f0eeea', '#f0eeea'];
    const borderColors = ['#632626', '#2e4700'];
    const baseColors = ['#c51d23', '#84d800'];
    const defaultBaseColor = '#787878';
    const defaultBorderColor = '#404040';
    const defaultHoverBorderColor = '#f0eeea';
    const defaultPaint = {
      'circle-radius': {
        'stops': [[10, 6], [18, 10]]
      },
      'circle-color': {
        'property': 'status',
        'stops': [
          [0, baseColors[0]],
          [1, baseColors[1]]
        ],
        'default': defaultBaseColor
      },
      'circle-stroke-width': 2,
      'circle-stroke-color': {
        'property': 'status',
        'stops': [
          [0, borderColors[0]],
          [1, borderColors[1]]
        ],
        'default': defaultBorderColor
      },
      'circle-stroke-opacity': 1
    };

    this.map.addSource('device-source', {
      'type': 'geojson',
      'data': {
        'type': 'FeatureCollection',
        'features': []
      }
    });

    this.map.addLayer({
      'id': 'devices',
      'type': 'circle',
      'source': 'device-source',
      'paint': defaultPaint,
    });

    this.map.addLayer({
      'id': 'devices-hover',
      'type': 'circle',
      'source': 'device-source',
      'paint': {
        ...defaultPaint,
        'circle-radius': {
          'stops': [[10, 10], [18, 18]]
        },
        'circle-color': {
          'property': 'status',
          'stops': [
            [0, hoverColors[0]],
            [1, hoverColors[1]]
          ],
          'default': defaultBaseColor
        },
        'circle-stroke-width': 3,
        'circle-stroke-color': {
          'property': 'status',
          'stops': [
            [0, hoverBorderColors[0]],
            [1, hoverBorderColors[1]]
          ],
          'default': defaultHoverBorderColor
        },
        'circle-stroke-opacity': 1
      },
      'filter': ['==', 'name', '']
    });

    this.map.addLayer({
      'id': 'devices-select',
      'type': 'circle',
      'source': 'device-source',
      'paint': {
        ...defaultPaint,
        'circle-radius': {
          'stops': [[10, 10], [18, 18]]
        },
        'circle-color': {
          'property': 'status',
          'stops': [
            [0, hoverColors[0]],
            [1, hoverColors[1]]
          ],
          'default': defaultBaseColor
        },
        'circle-stroke-width': 3,
        'circle-stroke-color': {
          'property': 'status',
          'stops': [
            [0, hoverBorderColors[0]],
            [1, hoverBorderColors[1]]
          ],
          'default': defaultHoverBorderColor
        },
        'circle-stroke-opacity': 1,
      },
      'filter': ['==', 'name', '']
    });
  }

  mapControls() {
    this.draw = new MapboxDraw({
      displayControlsDefault: false,
      styles: theme,
      controls: {
        polygon: true,
        trash: true
      }
    });
    this.map.addControl(this.draw);

    const satelliteControl = new SatelliteSelectControl(this.defaultStyle, this.satelliteStyle);
    this.map.addControl(satelliteControl, 'top-right');
  }

  mapPreferences() {
    const centreSub = this.centre$.subscribe(centre => {
      if (this.map) {
        this.map.jumpTo({ center: centre });
      }
    });
    this.subscriptions.push(centreSub);

    this.preference.getJsonPreference('device.default.location').subscribe(location => {
      if (location != null) {
        const geometry = this.geo.getGeometry(location);
        this.centre$.next(geometry.coordinates);
      }
    });

    const zoomSub = this.zoom$.subscribe(zoom => {
      if (this.map) {
        this.map.jumpTo(zoom);
      }
    });
    this.subscriptions.push(zoomSub);

    this.preference.getJsonPreference('device.default.zoom').subscribe((zoom: { zoom: number }) => {
      if (zoom != null) {
        this.zoom$.next(zoom);
      }
    });
  }

  mapListeners() {
    const canvas = this.map.getCanvasContainer();
    this.map.on('mousemove', 'devices', e => {
      const key = e.features[0].properties.key;
      this.zone.run(() => {
        this.itemSelection.hover(key);
      });
      canvas.style.cursor = 'pointer';
    });

    this.map.on('mouseleave', 'devices', () => {
      this.zone.run(() => {
        this.itemSelection.leave();
      });
      canvas.style.cursor = '';
    });
    this.map.on('moveend', this.loadData);

    this.map.on('click', e => {
      const features = this.map.queryRenderedFeatures(e.point, {
        layers: ['devices'] // replace this with the name of the layer
      });

      if (features.length > 0 && features[0].properties != null && features[0].properties.device != null) {
        const device = JSON.parse(features[0].properties.device);

        // Map items fall outside of NgZone
        this.zone.run(() => {
          this.viewDevice(device.key);
        });
      }
    });

    this.map.on('draw.modechange', (event) => {
      if (event.mode === 'draw_polygon') {
        this.isDrawing = true;
      } else if (event.mode === 'simple_select' && this.isDrawing) {
        this.createSearchBounds();
        this.isDrawing = false;
      }
    });
    this.map.on('draw.update', (event) => {
      this.updateSearchBounds(event.features);
    });
    this.map.on('draw.delete', (event) => {
      this.removeSearchBounds(event.features);
    });
    this.map.on('draw.selectionchange', () => {
      this.showLabels();
    });
  }

  mapSubscriptions() {
    const focusSub = this.mapStore.onFocus().subscribe((e: any) => {
      if (!e) return false;

      if (e.item) return this.mapFocus(e.item.geometry.coordinates);

      this.itemStore.getItemByKey(e.key).subscribe((item: any) => {
        if (item && item.item) {
          // Item doesnt exist on map - add it so the transition is smoother.
          const data = this.mapStore.getItems().slice(0);
          const feature = this.geo.createFeature(item.item);
          data.push(feature);
          this.setMapData(data);
          this.mapFocus(item.item.singlePosition);
        }
      });
    });
    this.subscriptions.push(focusSub);

    const hoverSub = this.mapStore.onHover().subscribe((hover: any) => {
      const mapFilter = hover ? hover.key : '';
      this.map.setFilter('devices-hover', ['==', 'key', mapFilter]);
    });
    this.subscriptions.push(hoverSub);

    const selectSub = this.mapStore.onSelect().subscribe((selection: any) => {
      const mapFilter = selection ? selection.key : '';
      this.map.setFilter('devices-select', ['==', 'key', mapFilter]);
    });
    this.subscriptions.push(selectSub);

    const updateMapSub = this.updateMap$.pipe(
      debounceTime(200),
      switchMap(() =>
        this.deviceApi.deviceLocation(this.search.getTerms(), [this.geo.getMapBoundingBox(this.map)])
          .pipe(
            catchError(error => {
              return of(null);
            })
          ))
    ).subscribe(res => this.updateMapDevices(res));
    this.subscriptions.push(updateMapSub);
  }

  triggerUpdateMap() {
    this.updateMap$.next(true);
  }

  updateMapDevices(devices) {
    if (this.map && devices != null) {
      const features = devices.geojson.features;
      this.setMapData(features);
      this.mapStore.setResult(features);
    }
  }

  setMapData(data) {
    if (!this.map || !this.map.getSource) return false;

    const datasource: any = this.map.getSource('device-source');
    if (datasource != null) {
      datasource.setData({
        'type': 'FeatureCollection',
        'features': data
      });
    }
  }

  mapFocus(coordinates): void {
    if (!this.map) return;

    this.map.easeTo({
      center: [coordinates[0], coordinates[1]],
      zoom: 14,
    });
  }

  viewDevice(key): void {
    this.itemSelection.select(key);
    const options = { relativeTo: this.route };
    if (this.searchName != null) {
      options['queryParams'] = { search: this.searchName };
    }
    this.router.navigate([`./${key}`], options);
  }

  /**
   * The search has been updated, make sure that if an area has been deleted it is removed from the map
   */
  checkBounds(searchTerms: SearchTerm[]): void {
    if (this.draw) {
      const index: number = searchTerms.findIndex(a => a.property === this.geoProperty);
      const drawn: FeatureCollection = this.draw.getAll();
      if (index === -1) {
        // all areas are deleted, remove everything drawn on map
        if (drawn.features.length > 0) {
          const allIds = drawn.features.map((feature: Feature) => feature.id);
          this.draw.delete(allIds);
        }
        this.showLabels();
        return;
      }

      const currentIds = drawn.features.map((feature: Feature) => feature.id);
      const searchIds = [];
      const deletedIds = [];
      const areas: FeatureCollection[] = searchTerms[index].values;
      for (const area of areas) {
        area.features.forEach(feature => {
          searchIds.push(feature.id);
          // draw any missing features on the map
          if (!currentIds.some(id => feature.id === id)) {
            this.draw.add(feature);
          }
        });
      }
      for (const mapId of currentIds) {
        if (!searchIds.some(id => mapId === id)) {
          // delete any map features not in the search
          deletedIds.push(mapId);
        }
      }
      this.draw.delete(deletedIds);
      this.showLabels();
    }
  }

  /**
   * Add labels to selected features
   */
  showLabels(): void {
    const searchTerms: SearchTerm[] = this.search.getTerms();
    const index: number = searchTerms.findIndex(a => a.property === this.geoProperty);
    const selectedIds = this.draw.getSelectedIds();
    if (index !== -1 && selectedIds.length > 0) {
      const areas: FeatureCollection[] = searchTerms[index].values;
      areas.forEach((area: FeatureCollection, i: number) => {
        area.features.forEach((feature: Feature) => {
          if (selectedIds.some(id => feature.id === id)) {
            // create marker label
            const label = document.createElement('div');
            label.innerHTML = `AREA ${i + 1}`;
            const latLong = this.geo.getCentre(feature.geometry);
            if (feature.id in this.markers) {
              this.markers[feature.id].remove();
            }
            this.markers[feature.id] = new mapboxgl.Marker(label)
              .setLngLat(latLong)
              .addTo(this.map);
          } else {
            if (feature.id in this.markers) {
              this.markers[feature.id].remove();
              delete this.markers[feature.id];
            }
          }
        });
      });
    } else {
      Object.keys(this.markers).forEach(key => {
        this.markers[key].remove();
        delete this.markers[key];
      });
    }
  }

  /**
   * A new polygon has been created, add it to the current search
   */
  createSearchBounds(): void {
    const selectedFeature: FeatureCollection = this.draw.getSelected();
    const searchTerms: SearchTerm[] = this.search.getTerms();
    const index: number = searchTerms.findIndex(a => a.property === this.geoProperty);

    // Check Exists
    if (index === -1) {
      this.search.addBlock(this.geoProperty, selectedFeature);
      return;
    }
    const existingSearchTerm: SearchTerm = searchTerms[index];
    existingSearchTerm.values.push(selectedFeature);
    this.search.setTerms(searchTerms);
  }

  /**
   * The selected features have moved or updated, make sure the matching search terms are also updated
   */
  updateSearchBounds(features: Feature[]): void {
    const searchTerms: SearchTerm[] = this.search.getTerms();
    const index: number = searchTerms.findIndex(a => a.property === this.geoProperty);
    // If multiple features are selected then split them for the searchTerms
    const featureCollections = features.map(feature =>
      ({ type: 'FeatureCollection', features: [feature] }));

    // If the geoproperty is not in the search, then add it and the currently selected areas
    if (index === -1) {
      this.search.addBlock(this.geoProperty, featureCollections);
      return;
    }

    // otherwise update the search with the current version of the selected features
    const existingSearchTerm: SearchTerm = searchTerms[index];
    for (const featureData of featureCollections) {
      const existingIndex = existingSearchTerm.values.findIndex((searchFeature: FeatureCollection) => {
        return searchFeature.features.some(feature => feature.id === featureData.features[0].id);
      });

      if (existingIndex !== -1) {
        existingSearchTerm.values[existingIndex] = featureData;
      } else {
        existingSearchTerm.values.push(featureData);
      }
    }
    this.search.setTerms(searchTerms);
  }

  /**
   * Remove search terms matching the deleted features
   */
  removeSearchBounds(features: Feature[]): void {
    const searchTerms: SearchTerm[] = this.search.getTerms();
    const index = searchTerms.findIndex(a => a.property === this.geoProperty);
    if (index === -1) return;

    const existingSearchTerm: SearchTerm = searchTerms[index];
    for (const featureData of features) {
      const existingIndex = existingSearchTerm.values.findIndex((featureCollection: FeatureCollection) => {
        return featureCollection.features.some(feature => feature.id === featureData.id);
      });

      if (existingIndex !== -1) {
        existingSearchTerm.values.splice(existingIndex, 1);
      }
    }
    if (existingSearchTerm.values.length > 0) {
      this.search.setTerms(searchTerms);
    } else {
      this.search.removeBlock(this.geoProperty);
    }
  }

}
