/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PrivilegeGuard } from 'app/core/services/auth/guards/privilege.guard';
import { Privilege } from 'app/core/services/auth/privilege';

import { DeviceDetailConfigComponent } from './configuration/device-detail-config/device-detail-config.component';
import { DevicesConfigComponent } from './configuration/devices-config/devices-config.component';
import { StreamDetailConfigComponent } from './configuration/stream-detail-config/stream-detail-config.component';
import { DeviceShadowConfigComponent } from './device-shadow/device-shadow-config/device-shadow-config.component';
import { DeviceDetailStatusComponent } from './status/device-detail-status/device-detail-status.component';
import { DevicesStatusComponent } from './status/devices-status/devices-status.component';
import { DevicesConfigWrapperComponent } from './sub-menu/devices-config-wrapper/devices-config-wrapper.component';
import { DevicesStatusWrapperComponent } from './sub-menu/devices-status-wrapper/devices-status-wrapper.component';

const routes: Routes = [

  {
    path: '',
    component: DevicesConfigWrapperComponent,
    children: [
      { path: '', redirectTo: 'overview', pathMatch: 'full' },
      {
        path: 'overview',
        data: { widgetType: 'widget' },
        loadChildren: () => import('../visualisations/widget-gallery/widget-gallery.module').then(mod => mod.WidgetGalleryModule),
      },
      {
        path: 'config',
        component: DevicesConfigComponent,
        children: [
          {
            path: 'new',
            canActivate: [PrivilegeGuard],
            data: { role: Privilege.CREATE_DEVICE },
            component: DeviceDetailConfigComponent,
          },
          {
            path: 'edit/:id',
            component: DeviceDetailConfigComponent,
            children: [
              { path: 'stream/:id', component: StreamDetailConfigComponent },
              { path: 'shadow/:id', component: DeviceShadowConfigComponent },
              {
                path: 'newStream',
                canActivate: [PrivilegeGuard],
                data: { role: Privilege.CREATE_DATASTREAM },
                component: StreamDetailConfigComponent,
              },
              {
                path: 'newTransform',
                canActivate: [PrivilegeGuard],
                data: { role: Privilege.CREATE_TRANSFORM },
                component: StreamDetailConfigComponent,
              }
            ]
          }
        ],
      }
    ]
  },
  {
    path: 'status',
    component: DevicesStatusWrapperComponent,
    children: [
      {
        path: '',
        component: DevicesStatusComponent,
        children: [
          { path: ':id', component: DeviceDetailStatusComponent }
        ]
      },
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class DevicesRoutingModule { }
