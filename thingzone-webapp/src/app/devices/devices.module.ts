/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ScrollingModule } from '@angular/cdk/scrolling';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
  MatAutocompleteModule,
  MatCheckboxModule,
  MatPaginatorModule,
  MatRadioModule,
  MatTableModule,
} from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatInputModule } from '@angular/material/input';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faDotCircle } from '@fortawesome/free-regular-svg-icons';
import {
  faCheckCircle,
  faCircle,
  faCode,
  faExclamationCircle,
  faList,
  faPen,
  faSave,
  faShare,
  faUndo,
} from '@fortawesome/free-solid-svg-icons';
import { DeviceInfoModule } from 'app/ui/device-info/device-info.module';
import { DynamicSelectModule } from 'app/ui/dynamic-select/dynamic-select.module';
import { JsonEditorModule } from 'app/ui/json-editor/json-editor.module';
import { ModelFormsModule } from 'app/ui/model-forms/model-forms.module';
import { NavigationMenuModule } from 'app/ui/navigation-menu/navigation-menu.module';
import { ResourceEditorModule } from 'app/ui/resource-editor/resource-editor.module';

import { CardComponentsModule } from '../ui/card-components/card-components.module';
import { FormComponentsModule } from '../ui/form-components/form-components.module';
import { ItemPanelModule } from '../ui/item-panel/item-panel.module';
import { MapEditModule } from '../ui/map-edit/map-edit.module';
import { PageOverviewModule } from '../ui/page-overview/page-overview.module';
import { SearchModule } from '../ui/search/search.module';
import { WidgetGalleryModule } from '../visualisations/widget-gallery/widget-gallery.module';
import { BasicKeyConfigComponent } from './configuration/basic-key-config/basic-key-config.component';
import { DeviceDetailConfigComponent } from './configuration/device-detail-config/device-detail-config.component';
import { DevicesConfigComponent } from './configuration/devices-config/devices-config.component';
import { EntityResourcesComponent } from './configuration/entity-resources/entity-resources.component';
import { PermissionChipComponent } from './configuration/permission-chip/permission-chip.component';
import { PermissionFormComponent } from './configuration/permission-form/permission-form.component';
import { PushSubscriberFormComponent } from './configuration/push-subscriber-form/push-subscriber-form.component';
import { ResourceDisplayComponent } from './configuration/resource-display/resource-display.component';
import { StreamDetailConfigComponent } from './configuration/stream-detail-config/stream-detail-config.component';
import { SubscriberConfigComponent } from './configuration/subscriber-config/subscriber-config.component';
import { TransformConfigComponent } from './configuration/transform-config/transform-config.component';
import { DeviceShadowConfigComponent } from './device-shadow/device-shadow-config/device-shadow-config.component';
import { DeviceShadowHistoryComponent } from './device-shadow/device-shadow-history/device-shadow-history.component';
import { HistoryEntryComponent } from './device-shadow/history-entry/history-entry.component';
import { DevicesRoutingModule } from './devices-routing.module';
import { DeviceDetailStatusComponent } from './status/device-detail-status/device-detail-status.component';
import { DeviceListComponent } from './status/device-list/device-list.component';
import { DevicesStatusComponent } from './status/devices-status/devices-status.component';
import { DevicesConfigWrapperComponent } from './sub-menu/devices-config-wrapper/devices-config-wrapper.component';
import { DevicesStatusWrapperComponent } from './sub-menu/devices-status-wrapper/devices-status-wrapper.component';

@NgModule({
  imports: [
    DevicesRoutingModule,
    FontAwesomeModule,
    CommonModule,
    ReactiveFormsModule,
    ScrollingModule,
    PageOverviewModule,
    SearchModule,
    NavigationMenuModule,
    MatAutocompleteModule,
    MatTableModule,
    MatPaginatorModule,
    MatChipsModule,
    MatInputModule,
    MatCheckboxModule,
    MatRadioModule,
    MatButtonModule,
    ItemPanelModule,
    MapEditModule,
    CardComponentsModule,
    FormComponentsModule,
    DynamicSelectModule,
    WidgetGalleryModule,
    JsonEditorModule,
    DeviceInfoModule,
    ResourceEditorModule,
    ModelFormsModule,
  ],
  declarations: [
    DevicesStatusWrapperComponent,
    DevicesConfigWrapperComponent,
    DevicesStatusComponent,
    DevicesConfigComponent,
    DeviceListComponent,
    DeviceDetailConfigComponent,
    DeviceDetailStatusComponent,
    StreamDetailConfigComponent,
    PermissionChipComponent,
    PermissionFormComponent,
    TransformConfigComponent,
    BasicKeyConfigComponent,
    SubscriberConfigComponent,
    DeviceShadowConfigComponent,
    DeviceShadowHistoryComponent,
    HistoryEntryComponent,
    PushSubscriberFormComponent,
    EntityResourcesComponent,
    ResourceDisplayComponent,
  ],
})
export class DevicesModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faCheckCircle, faExclamationCircle, faCircle, faDotCircle, faSave, faUndo, faPen, faShare, faList, faCode);
  }
}
