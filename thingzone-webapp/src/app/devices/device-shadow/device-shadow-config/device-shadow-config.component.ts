/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { StreamType } from 'app/core/models/queries/data-query';
import { DeviceApiService } from 'app/core/services/api/device-api.service';
import { PreferenceService } from 'app/core/services/preference/preference.service';
import { ShadowModel } from 'app/devices/domain-models/shadow-model';
import { StreamModel } from 'app/devices/domain-models/stream-model';
import { DeviceStreamService } from 'app/devices/services/device-stream.service';
import { ConfirmDeleteDialogComponent } from 'app/ui/form-components/confirm-delete-dialog/confirm-delete-dialog.component';
import { combineLatest, Subscription } from 'rxjs';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'device-shadow-config',
  templateUrl: './device-shadow-config.component.html',
  styleUrls: ['./device-shadow-config.component.scss']
})
export class DeviceShadowConfigComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  stream: StreamModel;
  hasError: boolean;
  entry: ShadowModel;
  entryCopy: ShadowModel;
  updated = false;
  history = false;
  jsonSchema = null;
  derived = false;
  errorMessage = '';
  showError = false;
  showForm = false;

  shadowForm = this.formBuilder.group({
    revision: [],
  });

  constructor(
    private cd: ChangeDetectorRef,
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    protected deviceApi: DeviceApiService,
    private preferenceService: PreferenceService,
    private deviceStream: DeviceStreamService,
  ) { }

  ngOnInit() {
    combineLatest([this.route.params, this.deviceStream.dataStreams$])
      .subscribe(([params, dataStreams]) => {
        if ('id' in params && dataStreams != null) {
          const key = params.id;
          const stream = dataStreams.find(a => a.key === key);

          if (stream == null) this.setStream(null);

          this.setStream(stream);
        } else {
          this.setStream(null);
        }
      });

    const shadowFormSub = this.shadowForm.valueChanges.subscribe(formValues => {
      this.entry.revision = formValues['revision'];
      this.updated = true;
    });
    this.subscriptions.push(shadowFormSub);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  setStream(stream): void {
    this.stream = stream as StreamModel;

    if (this.stream != null) {
      if (this.stream.metadata != null && 'schema' in this.stream.metadata) {
        const schemaName = this.stream.metadata['schema'];
        this.preferenceService.getResource(this.stream.key, schemaName).subscribe((res) => {
          this.jsonSchema = res;
          this.showForm = true;
          this.cd.markForCheck();
        });
      }

      this.deviceApi.getConfig([this.stream.key], 1).subscribe(res => {
        if (res.length > 0) {
          const list = res[0];
          if (list.values.length > 0) {
            this.setEntry(list.values[0]);
          } else {
            this.showError = true;
            this.errorMessage = 'No device settings entries found';
            this.entry = this.newEntry();
            this.cd.markForCheck();
          }
        }
      });
    }
  }

  newEntry(): ShadowModel {
    return {
      streamKey: this.stream.key,
      origin: 'SYSTEM',
      klass: StreamType.CONFIG,
      revision: '',
      tags: [],
      config: {},
    };
  }

  setEntry(data): void {
    this.entryCopy = JSON.parse(JSON.stringify(data));
    this.entry = data;
    this.derived = false;
    this.shadowForm.patchValue({ 'revision': this.entry.revision }, { onlySelf: true, emitEvent: false });
    this.cd.markForCheck();
  }

  close(): void {
    this.router.navigate(['../../'], { relativeTo: this.route });
  }

  setError(value: boolean): void {
    this.hasError = value;
  }

  get disableSave(): boolean {
    return !this.updated || this.hasError;
  }

  addTag(value: string): void {
    if (!this.entry.tags.includes(value)) {
      this.entry.tags.push(value);
      this.updated = true;
    }
  }

  removeTag(value: string): void {
    this.entry.tags = this.entry.tags.filter(tag => tag !== value);
    this.updated = true;
  }

  updateJson(value: object): void {
    this.updated = true;
    this.entry.config = value;
  }

  discardChanges(): void {
    this.updated = false;
    this.setError(false);
    this.setEntry(this.entryCopy);
  }

  sendJson(): void {
    const dialogRef = this.dialog.open(ConfirmDeleteDialogComponent, {
      data: { message: 'This will add a new configuration entry to the stream', confirmName: false }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        const content: ShadowModel = {
          streamKey: this.stream.key,
          origin: 'SYSTEM',
          klass: this.entry.klass,
          revision: this.entry.revision,
          tags: this.entry.tags,
          config: this.entry.config,
        };
        if (this.derived && this.entry.derivedFromConfig) {
          content.derivedFromConfig = this.entry.derivedFromConfig;
        }
        this.deviceApi.setConfig(this.stream.key, content).subscribe(res => {
          this.updated = false;
          this.errorMessage = '';
          this.showError = false;
          this.cd.markForCheck();
        });
      }
    });
  }

  showHistory(): void {
    this.history = !this.history;
    this.cd.markForCheck();
  }

  toggleForm(): void {
    this.showForm = !this.showForm;
    this.cd.markForCheck();
  }

  updateFromHistory(history: ShadowModel): void {
    this.derived = true;
    history.derivedFromConfig = history.id;
    history.metadata = null;
    history.origin = 'SYSTEM';
    this.setEntry(history);
  }
}
