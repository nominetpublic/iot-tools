/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ShadowModel } from 'app/devices/domain-models/shadow-model';
import * as moment from 'moment';

@Component({
  selector: 'history-entry',
  templateUrl: './history-entry.component.html',
  styleUrls: ['./history-entry.component.scss']
})
export class HistoryEntryComponent {
  @Input() shadow: ShadowModel;
  @Output() derive: EventEmitter<ShadowModel> = new EventEmitter();
  show = false;

  constructor() { }

  showDate(timestamp: string): string {
    return moment(timestamp).format('MMM D, YYYY, H:mm:ss');
  }

  toggleShow() {
    this.show = !this.show;
  }

  selectRevision(entry: ShadowModel) {
    this.derive.emit(entry);
  }
}
