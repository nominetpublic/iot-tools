/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DeviceApiService } from 'app/core/services/api/device-api.service';
import { ShadowModel } from 'app/devices/domain-models/shadow-model';
import { StreamModel } from 'app/devices/domain-models/stream-model';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'device-shadow-history',
  templateUrl: './device-shadow-history.component.html',
  styleUrls: ['./device-shadow-history.component.scss']
})
export class DeviceShadowHistoryComponent implements OnInit {

  @Input() show: boolean;
  @Input() stream: StreamModel;
  @Output() derive: EventEmitter<ShadowModel> = new EventEmitter();
  history$ = new BehaviorSubject<ShadowModel[]>([]);

  constructor(
    private deviceApi: DeviceApiService
  ) { }

  ngOnInit() {
    if (this.stream != null) {
      this.deviceApi.getConfig([this.stream.key]).subscribe(res => {
        if (res.length > 0) {
          const list = res[0];
          if (list.values.length > 0) {
            this.history$.next(list.values);
          }
        }
      });
    }
  }

  deriveFromHistory(entry: ShadowModel) {
    this.derive.emit(entry);
  }

}
