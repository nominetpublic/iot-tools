/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { StreamModel } from '../domain-models/stream-model';
import { TransformModel } from '../domain-models/transform-model';

@Injectable()
export class DeviceStreamService {

  _dataStreams = new BehaviorSubject<StreamModel[]>([]);
  dataStreams$ = this._dataStreams.asObservable();

  constructor() { }

  setStreams(streams): void {
    this._dataStreams.next(streams);
  }

  updateStreamByKey(key: string, value: StreamModel | TransformModel) {
    const storeItems = this._dataStreams.getValue();

    if (key && value) {
      let updated = false;
      const updatedItems = storeItems.map(e => {
        if (e.key === key) {
          updated = true;
          return value;
        }
        return e;
      });
      // item not already in the list, so add it
      if (!updated) {
        updatedItems.push(value);
      }

      this._dataStreams.next(updatedItems);
    }
  }

  removeStreamByKey(key: string, value: StreamModel | TransformModel) {
    const storeItems = this._dataStreams.getValue();

    if (key && value) {
      const updatedItems = storeItems.filter(e => (e.key !== key));
      this._dataStreams.next(updatedItems);
    }
  }
}

