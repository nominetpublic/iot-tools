/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { SearchTerm } from 'app/core/models/common/search-term';
import { DeviceApiService } from 'app/core/services/api/device-api.service';
import { ItemStoreService } from 'app/core/services/item-store/item-store.service';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';

import { DeviceModel } from '../domain-models/device-model';


export class DeviceConfigDataSource implements DataSource<DeviceModel | undefined> {

    private devicesSubject = new BehaviorSubject<DeviceModel[]>([]);
    private entitiesCountSubject = new BehaviorSubject<number>(0);
    public entitiesCount$ = this.entitiesCountSubject.asObservable();
    private loadingSubject = new BehaviorSubject<boolean>(false);
    public loading$ = this.loadingSubject.asObservable();
    private currentSearch = new BehaviorSubject<SearchTerm[]>([]);
    private subscription$ = new Subscription();
    size: number;

    constructor(private deviceApi: DeviceApiService, private itemStore: ItemStoreService, defaultSize: number) {
        this.size = defaultSize;
    }

    connect(collectionViewer: CollectionViewer): Observable<DeviceModel[]> {
        return this.devicesSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.devicesSubject.complete();
        this.loadingSubject.complete();
        this.subscription$.unsubscribe();
    }

    public updateSearchTerms(searchTerms: SearchTerm[]) {
        this.devicesSubject.next([]);
        this.entitiesCountSubject.next(0);
        this.initialList(searchTerms, 0, this.size);
    }

    public initialList(searchTerms?: SearchTerm[], from = 0, size = this.size): void {
        this.currentSearch.next(searchTerms);
        this.deviceApi.deviceList(searchTerms, from, size)
            .pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false)))
            .subscribe(res => {
                this.devicesSubject.next(res.devices);
                this.entitiesCountSubject.next(res.total);
                this.itemStore.setResult(res.devices);
            });
    }

    public paginateList(from: number, size: number) {
        this.deviceApi.deviceList(this.currentSearch.getValue(), from, size)
            .pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false)))
            .subscribe(res => {
                const devices: DeviceModel[] = res.devices;
                const currentDevices = this.devicesSubject.getValue();
                this.itemStore.setResult(currentDevices.concat(devices));
                this.devicesSubject.next(devices);
            });
    }

    public totalCount(): Observable<number> {
        return this.entitiesCount$;
    }
}
