/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { DeviceApiService } from 'app/core/services/api/device-api.service';
import { ItemStoreService } from 'app/core/services/item-store/item-store.service';
import { SearchService } from 'app/core/services/search/search.service';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';

import { DeviceModel } from '../domain-models/device-model';

export class DeviceListDataSource extends DataSource<DeviceModel | undefined> {
    private initialLength = 500;
    private devicesSubject = new BehaviorSubject<(DeviceModel | undefined)[]>(this.createData());
    private entitiesCountSubject = new BehaviorSubject<number>(0);
    public entitiesCount$ = this.entitiesCountSubject.asObservable();
    private _subscription = new Subscription();
    private loadingSubject = new BehaviorSubject<boolean>(false);

    constructor(
        private deviceApi: DeviceApiService,
        private search: SearchService,
        private itemStore: ItemStoreService,
    ) {
        super();
    }

    connect(collectionViewer: CollectionViewer): Observable<(DeviceModel | undefined)[]> {
        this._subscription.add(collectionViewer.viewChange.subscribe(range => {
            const start = range.start;
            this.checkSize(range.end);
            this.paginateList(start, range.end);
        }));
        return this.devicesSubject.asObservable();
    }

    disconnect(): void {
        this.devicesSubject.complete();
        this.loadingSubject.complete();
        this.entitiesCountSubject.complete();
        this._subscription.unsubscribe();
    }

    private createData(length = this.initialLength): DeviceModel[] {
        return Array.from<DeviceModel>({ length: length });
    }

    private checkSize(end) {
        const devices = this.devicesSubject.getValue();
        if (devices.length <= end) {
            const total = this.entitiesCountSubject.getValue();
            if (total > 0 && (devices.length + this.initialLength) > total) {
                this.devicesSubject.next(devices.concat(this.createData(total - devices.length)));
            } else {
                this.devicesSubject.next(devices.concat(this.createData()));
            }
        }
    }

    public updateSearchTerms() {
        this.entitiesCountSubject.next(0);
        this.devicesSubject.next(this.createData());
        this.paginateList(0, 50);
    }

    public paginateList(from: number, end: number) {
        let devices = this.devicesSubject.getValue();
        if (devices.slice(from, end + 1).some(e => e === undefined)) {
            const size = end - from;
            this.deviceApi.deviceList(this.search.getTerms(), from, size)
                .pipe(
                    catchError(() => of([])),
                    finalize(() => this.loadingSubject.next(false)))
                .subscribe(res => {
                    this.entitiesCountSubject.next(res.total);
                    if (devices.length > res.total) {
                        devices = devices.slice(0, res.total);
                    }
                    this.itemStore.setResult(res.devices);
                    devices.splice(from, size, ...res.devices);
                    this.devicesSubject.next(devices);
                });
        } else {
            this.itemStore.setResult(devices.slice(from, end + 1));
            return;
        }
    }
}
