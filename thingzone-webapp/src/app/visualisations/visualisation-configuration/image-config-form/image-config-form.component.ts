/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ImageQueryConfiguration } from 'app/core/models/visualisations/display-data-source';
import { VisualisationService } from 'app/core/services/visualisation/visualisation.service';
import { Subscription } from 'rxjs';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'image-config-form',
  templateUrl: './image-config-form.component.html',
  styleUrls: ['./image-config-form.component.scss']
})
export class ImageConfigFormComponent implements OnInit, OnDestroy {

  @Input() imageQuery: ImageQueryConfiguration;
  @Output() save: EventEmitter<ImageQueryConfiguration> = new EventEmitter();
  @Output() hasError: EventEmitter<boolean> = new EventEmitter();

  subscriptions: Subscription[] = [];
  imageGroupByOptions = this.visualisationService.imageGroupByOptions;

  imageConfigForm = this.formBuilder.group({
    burstInterval: [],
    tags: [],
    size: [],
  });

  constructor(
    private formBuilder: FormBuilder,
    private visualisationService: VisualisationService) {
  }

  ngOnInit() {
    const imageSub = this.imageConfigForm.valueChanges.subscribe(formValues => {
      if (!this.imageConfigForm.invalid) {
        if (formValues.tags == null || formValues['tags'].trim() === '') {
          formValues['tags'] = null;
        } else if (typeof formValues.tags === 'string' && /\S/.test(formValues.tags)) {
          const tags = formValues.tags.split(',').map((tag: string) => tag.trim());
          formValues['tags'] = tags;
        }
        this.save.emit(formValues);
      }
      this.hasError.emit(this.imageConfigForm.invalid);
    });
    this.subscriptions.push(imageSub);

    if (this.imageQuery != null) {
      this.imageConfigForm.patchValue(this.imageQuery, { onlySelf: true, emitEvent: false });
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  selectImageGroupBy(value: string): void {
    this.imageConfigForm.get('groupBy').setValue(value);
  }
}
