/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { SelectOption } from 'app/core/models/common/select-option';
import { EventQueryConfiguration } from 'app/core/models/visualisations/display-data-source';
import { VisualisationComponentType } from 'app/core/models/visualisations/visualisation-component';
import { SearchService } from 'app/core/services/search/search.service';
import { VisualisationService } from 'app/core/services/visualisation/visualisation.service';
import { SavedSearch } from 'app/ui/search/models/active-search';
import { SearchResource } from 'app/ui/search/models/search-resource';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'event-config-form',
  templateUrl: './event-config-form.component.html',
  styleUrls: ['./event-config-form.component.scss']
})
export class EventConfigFormComponent implements OnInit, OnDestroy {
  VisualisationComponentType = VisualisationComponentType;
  @Input() eventQuery: EventQueryConfiguration;
  @Input() displayType: VisualisationComponentType;
  @Output() save: EventEmitter<EventQueryConfiguration> = new EventEmitter();
  @Output() hasError: EventEmitter<boolean> = new EventEmitter();

  subscriptions: Subscription[] = [];
  alertSavedSearches: SearchResource[] = [];
  eventFilterOptions: SelectOption[] = [];
  eventTypeOptions = this.visualisationService.eventTypes;

  eventConfigForm = this.formBuilder.group({
    size: [null, [Validators.pattern('-?\\d+(\\.\\d*?)?')]],
    timeLimit: [],
    interval: [],
    eventFilter: [],
    forward: [],
    yValue: [],
    valueName: [],
  });

  constructor(
    private formBuilder: FormBuilder,
    private search: SearchService,
    private visualisationService: VisualisationService) { }

  ngOnInit() {
    this.search.alertSavedSearches$.pipe(first(q => q != null)).subscribe(res => {
      this.setEventFilters(res);
    });

    const alertSub = this.eventConfigForm.valueChanges.subscribe(formValues => {
      if (!this.eventConfigForm.invalid) {
        this.save.emit(formValues);
      }
      this.hasError.emit(this.eventConfigForm.invalid);
    });
    this.subscriptions.push(alertSub);

    if (this.eventQuery != null) {
      this.eventConfigForm.patchValue(this.eventQuery, { onlySelf: true, emitEvent: false });
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  setEventFilters(searches): void {
    this.alertSavedSearches = searches;
    this.eventFilterOptions = searches.map((search, index) => ({ title: search.name, value: index }));
  }

  removeEventFilter() {
    this.eventConfigForm.get('eventFilter').setValue(null);
  }

  selectEventFilter(value): void {
    const searchResource: SearchResource = this.alertSavedSearches[value];
    const eventFilter: SavedSearch = searchResource.content;
    this.eventConfigForm.get('eventFilter').setValue(eventFilter);
  }
}
