/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { SelectOption } from 'app/core/models/common/select-option';
import { DataQueryType } from 'app/core/models/queries/data-query';
import {
  GraphDisplayConfiguration,
  VisualisationComponentType,
} from 'app/core/models/visualisations/visualisation-component';
import { Subscription } from 'rxjs';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'graph-settings',
  templateUrl: './graph-settings.component.html',
  styleUrls: ['./graph-settings.component.scss']
})
export class GraphSettingsComponent implements OnInit, OnChanges, OnDestroy {
  VisualisationComponentType = VisualisationComponentType;
  DataQueryType = DataQueryType;

  @Input() configuration: GraphDisplayConfiguration;
  @Input() displayType: VisualisationComponentType;
  @Input() queryTypes: DataQueryType[];
  @Output() update: EventEmitter<GraphDisplayConfiguration> = new EventEmitter();
  @Output() setError: EventEmitter<boolean> = new EventEmitter();

  error = false;
  subscriptions: Subscription[] = [];

  gaugeValueFormat: SelectOption[] = [
    { value: 'hidden', title: 'Hidden' },
    { value: 'average', title: 'Average' },
    { value: 'count', title: 'Count' },
  ];

  pieFormat: SelectOption[] = [
    { value: null, title: 'Standard' },
    { value: 'percent', title: 'Percent' },
  ];

  graphConfigForm = this.formBuilder.group({
    hideXAxis: [false],
    xLabel: [''],
    hideYAxis: [false],
    yLabel: [''],
    min: [null],
    max: [null],
    size: [null],
    formatType: [null],
    displayType: ['count'],
  });
  initialFormValues = this.graphConfigForm.value;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    const graphSub = this.graphConfigForm.valueChanges.subscribe(formValues => {
      if (!this.graphConfigForm.invalid) {
        this.configuration = { ...this.configuration, ...formValues };
        this.update.emit(this.configuration);
      }
      this.sendError(this.graphConfigForm.invalid);
    });
    this.subscriptions.push(graphSub);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.initialiseFormValues();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  /**
   * Set the correct form values from the configuration settings
   */
  initialiseFormValues(): void {
    this.graphConfigForm.reset(this.initialFormValues, { onlySelf: true, emitEvent: false });
    if (this.configuration != null) {
      this.graphConfigForm.patchValue({...this.initialFormValues, ...this.configuration}, { onlySelf: true, emitEvent: false });
    }
  }

  sendError(error: boolean) {
    this.error = error;
    this.setError.emit(error);
  }

  selectFormatType(value: string): void {
    this.graphConfigForm.get('formatType').setValue(value);
  }

  saveGraphRange(graphRange): void {
    this.configuration['stops'] = graphRange.stops;
    this.update.emit(this.configuration);
  }
}
