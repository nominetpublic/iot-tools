/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { SelectOption } from 'app/core/models/common/select-option';
import { DataQueryType } from 'app/core/models/queries/data-query';
import { MapDisplayConfiguration, VisualisationComponentType } from 'app/core/models/visualisations/visualisation-component';
import { Subscription } from 'rxjs';
import { AnnotationService } from 'app/core/services/visualisation/annotation.service';
import { map, flatMap } from 'rxjs/operators';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'map-settings',
  templateUrl: './map-settings.component.html',
  styleUrls: ['./map-settings.component.scss']
})
export class MapSettingsComponent implements OnInit, OnChanges, OnDestroy {
  VisualisationComponentType = VisualisationComponentType;
  DataQueryType = DataQueryType;

  @Input() configuration: MapDisplayConfiguration;
  @Input() displayType: VisualisationComponentType;
  @Input() queryTypes: DataQueryType[];
  @Output() update: EventEmitter<MapDisplayConfiguration> = new EventEmitter();
  @Output() setError: EventEmitter<boolean> = new EventEmitter();

  error = false;
  subscriptions: Subscription[] = [];

  mapThemeOptions: SelectOption[] = [
    { value: 'light', title: 'Light' },
    { value: 'dark', title: 'Dark' },
    { value: 'satellite', title: 'Satellite' },
  ];

  mapAnnotationOptions: SelectOption[] = [];

  // Type specific configuration forms
  mapConfigForm = this.formBuilder.group({
    latitude: [null, [Validators.min(-90), Validators.max(90), Validators.pattern('-?\\d+(\\.\\d*?)?')]],
    longitude: [null, [Validators.min(-180), Validators.max(180), Validators.pattern('-?\\d+(\\.\\d*?)?')]],
    mapZoom: [11, [Validators.min(0), Validators.max(24), Validators.pattern('-?\\d+(\\.\\d{0,6})?')]],
    pitch: [null, [Validators.min(0), Validators.max(60), Validators.pattern('-?\\d+(\\.\\d{0,6})?')]],
    bearing: [null, [Validators.min(-360), Validators.max(360)]],
    theme: ['light'],
    annotations: [[null]]
  });
  initialFormValues = this.mapConfigForm.value;

  constructor(
    private formBuilder: FormBuilder,
    private annotationService: AnnotationService
  ) { }

  ngOnInit() {
    const mapSub = this.mapConfigForm.valueChanges.subscribe(formValues => {
      if (!this.mapConfigForm.invalid) {
        let defaultCentre = null;
        if (formValues.latitude != null || formValues.longitude != null) {
          defaultCentre = [0, 0];
          defaultCentre[0] = formValues.longitude;
          defaultCentre[1] = formValues.latitude;
        }
        formValues['mapCentre'] = defaultCentre;
        this.update.emit(formValues);
        this.error = false;
      } else {
        this.error = true;
      }
      this.sendError();
    });
    this.subscriptions.push(mapSub);

    const annoationsSub = this.annotationService.listAnnotations()
      .pipe(flatMap(r => r.resources))
      .subscribe(a => this.mapAnnotationOptions.push({ value: a['key'], title: a['name'] }));
    this.subscriptions.push(annoationsSub);

    this.initialiseFormValues();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.initialiseFormValues();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  /*
  * Set the correct form values from the configuration settings
  */
  initialiseFormValues(): void {
    const config = this.configuration;
    this.mapConfigForm.reset(this.initialFormValues, { onlySelf: true, emitEvent: false });
    if (config != null) {
      config.longitude = (config.mapCentre != null && config.mapCentre[0] != null) ? config.mapCentre[0] : 0;
      config.latitude = (config.mapCentre != null && config.mapCentre[1] != null) ? config.mapCentre[1] : 0;

      this.mapConfigForm.patchValue({ ...this.initialFormValues, ...config }, { onlySelf: true, emitEvent: false });
    }
  }

  selectMapTheme(value: string): void {
    this.mapConfigForm.get('theme').setValue(value);
  }

  selectMapAnnotations(values: string[]): void {
    this.mapConfigForm.get('annotations').setValue(values);
  }

  sendError() {
    this.setError.emit(this.error);
  }
}
