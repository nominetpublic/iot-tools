/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { SelectOption } from 'app/core/models/common/select-option';
import { DataQueryType } from 'app/core/models/queries/data-query';
import { QuerySource } from 'app/core/models/visualisations/display-data-source';
import { PanelResizeService } from 'app/core/services/panel-resize/panel-resize.service';
import { VisualisationService } from 'app/core/services/visualisation/visualisation.service';
import { ConfirmDeleteDialogComponent } from 'app/ui/form-components/confirm-delete-dialog/confirm-delete-dialog.component';
import { BehaviorSubject, Subscription } from 'rxjs';

import {
  GraphDisplayConfiguration,
  MapDisplayConfiguration,
  VisualisationComponent,
  VisualisationComponentType,
} from '../../../core/models/visualisations/visualisation-component';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'visualisation-component-form',
  templateUrl: './visualisation-component-form.component.html',
  styleUrls: ['./visualisation-component-form.component.scss']
})
export class VisualisationComponentFormComponent implements OnInit, OnChanges, OnDestroy {
  @Input() component: VisualisationComponent;
  @Output() setError: EventEmitter<boolean> = new EventEmitter();

  VisualisationComponentType = VisualisationComponentType;
  subscriptions: Subscription[] = [];
  iconName: string;
  selectedSource: number;
  typeOptions: SelectOption[] = this.visualisationService.typeOptions;
  dataSourceError = false;
  displayConfigError = false;
  nameControl = new FormControl('');
  disableQuerySelect = false;
  private allowedQueryTypes: BehaviorSubject<DataQueryType[]> = new BehaviorSubject([]);
  allowedQueryTypes$ = this.allowedQueryTypes.asObservable();
  queryTypes: DataQueryType[] = [];

  constructor(
    protected panelResize: PanelResizeService,
    protected visualisationService: VisualisationService,
    protected ref: ChangeDetectorRef,
    public dialog: MatDialog) {
  }

  ngOnInit() {
    this.panelResize.dispatch();
    this.initFormValues();

    if (this.component.type) {
      const currentType = this.getCurrentType(this.component.type);
      this.iconName = currentType.icon;
    }

    const nameSub = this.nameControl.valueChanges.subscribe(name => {
      this.component.name = name;
    });
    this.subscriptions.push(nameSub);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.component.previousValue == null
      || changes.component.currentValue.id == null
      || changes.component.currentValue.id !== changes.component.previousValue.id) {
      this.initFormValues();
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  initFormValues() {
    this.onQueryChange();

    if (this.component.name != null) {
      this.nameControl.setValue(this.component.name);
    } else {
      this.nameControl.setValue('');
    }
  }

  getCurrentType(value: VisualisationComponentType): SelectOption {
    return this.typeOptions.find(type => type.value === value);
  }

  updateDisplayType(value: string): void {
    this.component.type = VisualisationComponentType[value];
    // clear type specific configuration
    this.component.configuration = {};
    this.limitQueryType();
  }

  addDataSource(): void {
    const displayType = VisualisationComponentType[this.component.type];
    const newSource = this.visualisationService.getEmptyDataSource(displayType);
    this.component.sources.unshift(newSource);
    this.onQueryChange();
    this.ref.markForCheck();
  }

  deleteDataSource(sourceId: number): void {
    const dialogRef = this.dialog.open(ConfirmDeleteDialogComponent, {
      data: { message: 'Would you like to delete this data query?', confirmName: false }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.component.sources = this.component.sources.filter(s => s.id !== sourceId);
        this.onQueryChange();
        this.ref.markForCheck();
      }
    });
  }

  updateConfig(value: MapDisplayConfiguration | GraphDisplayConfiguration) {
    this.component.configuration = value;
    this.limitQueryType();
  }

  setDisplayConfigError(value: boolean): void {
    if (value !== this.displayConfigError) {
      this.displayConfigError = value;
      this.updateError();
    }
  }

  setDataSourceError(value: boolean): void {
    if (value !== this.dataSourceError) {
      this.dataSourceError = value;
      this.updateError();
    }
  }

  updateError(): void {
    this.setError.emit(this.dataSourceError || this.displayConfigError);
  }

  onQueryChange(): void {
    this.disableQuerySelect = this.component.sources != null ?
      this.component.sources.length > 0 : false;

    this.limitQueryType();
    this.queryTypes = this.component.sources.map((source: QuerySource) => {
      return source.query;
    });
  }

  limitQueryType() {
    let newLimits = false;
    let allowedQueries: DataQueryType[] = [];
    if (this.component.type === VisualisationComponentType.PIECHART
      || this.component.type === VisualisationComponentType.BARGRAPH
      || this.component.type === VisualisationComponentType.GAUGE) {
      const config = this.component.configuration as GraphDisplayConfiguration;
      if (this.component.sources.length > 1) {
        newLimits = true;
        allowedQueries = [this.component.sources[this.component.sources.length - 1].query];
      } else if (config.stops != null && config.stops.length > 0) {
        newLimits = true;
        allowedQueries = [DataQueryType.AGGREGATION];
      } else {
        newLimits = true;
      }
    }
    if (newLimits) {
      this.allowedQueryTypes.next(allowedQueries);
      this.ref.markForCheck();
    }
  }
}
