/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { DisplayResource } from 'app/core/models/visualisations/display-resource';

export interface ImportVisualisationDialogData {
  vis: DisplayResource;
}

@Component({
  selector: 'import-visualisation-dialog',
  templateUrl: './import-visualisation-dialog.component.html',
  styleUrls: ['./import-visualisation-dialog.component.scss']
})
export class ImportVisualisationDialogComponent {

  formGroup = this.fb.group({
    file: [null]
  });
  file: Blob = null;
  uploaded = false;


  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<ImportVisualisationDialogComponent>
  ) { }

  onCancel(): void {
    this.dialogRef.close();
  }


  onFileChange(event): void {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.onloadend = () => {
        this.formGroup.patchValue({
          file: reader.result
        });
      };
      reader.readAsText(file);
      this.uploaded = true;
    }
  }

  uploadVisualisation(): void {
      const file = this.formGroup.get('file').value;
      const content = JSON.parse(file);
      const displayResource: DisplayResource = { resourceName: 'visualisation', type: 'visualisation', content: content };
      this.dialogRef.close(displayResource);
  }

}
