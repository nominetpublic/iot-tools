/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { DisplayResource } from 'app/core/models/visualisations/display-resource';
import { VisualisationComponent } from 'app/core/models/visualisations/visualisation-component';
import { VisualisationContent } from 'app/core/models/visualisations/visualisation-content';
import { PanelResizeService } from 'app/core/services/panel-resize/panel-resize.service';
import { VisualisationLayout, VisualisationService } from 'app/core/services/visualisation/visualisation.service';
import { ObjectUtils } from 'app/core/utils/object-utils';
import { ConfirmDeleteDialogComponent } from 'app/ui/form-components/confirm-delete-dialog/confirm-delete-dialog.component';
import * as moment from 'moment';

import { ExportVisualisationDialogComponent } from '../export-visualisation-dialog/export-visualisation-dialog.component';
import { ImportVisualisationDialogComponent } from '../import-visualisation-dialog/import-visualisation-dialog.component';

@Component({
  selector: 'visualisation-form',
  templateUrl: './visualisation-form.component.html',
  styleUrls: ['./visualisation-form.component.scss'],
})
export class VisualisationFormComponent implements OnInit, OnChanges, OnDestroy {

  @Input() resource: DisplayResource;
  @Input() newVisualisation: boolean;
  @Output() save = new EventEmitter();

  subscriptions = [];
  display: DisplayResource;
  selected: FormControl = new FormControl(0);
  error = false;
  configError = false;
  currentDateError = false;

  typeOptions = this.visualisationService.typeOptions;
  layouts = this.visualisationService.layouts;
  sourceOptions = [];
  filteredSourceOptions;

  currentLayout: VisualisationLayout = this.layouts[0];
  displayContent: VisualisationContent;
  currentComponent: VisualisationComponent;

  nameControl = new FormControl('');
  dateRangeForm = this.formBuilder.group({
    from: [moment().subtract(1, 'hour'), Validators.required],
    to: [moment()],
    current: [moment().subtract(1, 'hour'), Validators.required],
  });

  constructor(
    protected panelResize: PanelResizeService,
    protected visualisationService: VisualisationService,
    protected ref: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    public dialog: MatDialog) {
  }

  ngOnInit() {
    this.panelResize.dispatch();

    const visualisationSub = this.visualisationService.visualisations$.subscribe(resources => {
      if (resources == null) {
        this.visualisationService.queryVisualisations(this.resource.type);
      } else if (this.resource != null) {
        this.resource = resources.find(res => this.resource.key === res.key);
        this.updateDisplay();
      }
    });
    this.subscriptions.push(visualisationSub);

    const dateFormSub = this.dateRangeForm.valueChanges.subscribe(formValues => {
      if ('current' in formValues) {
        this.checkCurrentDate(formValues['current']);
      }
      if (!this.dateRangeForm.invalid) {
        this.displayContent.configuration.to = formValues.to != null ? formValues.to : null;
        if (formValues.from != null) {
          this.displayContent.configuration.from = formValues.from;
        }
        if (formValues.current != null) {
          this.displayContent.configuration.current = formValues.current;
        }
      }
      this.configError = this.dateRangeForm.invalid;
    });
    this.subscriptions.push(dateFormSub);

    const nameSub = this.nameControl.valueChanges.subscribe(name => {
      this.display.name = name;
    });
    this.subscriptions.push(nameSub);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.resource.previousValue == null && changes.resource.currentValue != null) {
      this.currentComponent = null;
      this.updateDisplay();
    } else if (changes.resource.currentValue != null) {
      if (changes.resource.currentValue.key !== changes.resource.previousValue.key) {
        this.currentComponent = null;
      }
      this.resource = changes.resource.currentValue;
      this.updateDisplay();
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
    this.panelResize.dispatch();
  }

  updateDisplay(): void {
    if (this.resource != null) {
      this.display = ObjectUtils.parseResource(this.resource);
      this.nameControl.setValue(this.display.name);
      this.displayContent = this.display.content as VisualisationContent;
      this.updateLayouts();
      if (this.currentComponent == null && this.displayContent.components.length > 0) {
        this.currentComponent = this.displayContent.components[0];
      }
      if (this.currentComponent) {
        this.selectComponent(this.currentComponent.id);
      }
      this.loadDateRangeForm();
    }
  }

  loadDateRangeForm(): any {
    if (this.displayContent.configuration.from != null) {
      this.dateRangeForm.get('from').setValue(moment(this.displayContent.configuration.from));
      if (this.displayContent.configuration.current == null) {
        this.dateRangeForm.get('current').setValue(moment(this.displayContent.configuration.from));
      }
    } else {
      this.displayContent.configuration.from = this.dateRangeForm.get('from').value;
    }
    if (this.displayContent.configuration.to == null) {
      this.dateRangeForm.get('to').setValue(null);
    } else {
      this.dateRangeForm.get('to').setValue(moment(this.displayContent.configuration.to));
    }
    if (this.displayContent.configuration.current != null) {
      this.dateRangeForm.get('current').setValue(moment(this.displayContent.configuration.current));
    }
  }

  updateLayouts(): void {
    this.currentLayout = this.visualisationService.findLayout(this.displayContent.configuration.layout);
  }

  submit(): void {
    this.save.emit(this.display);
  }

  setError(value: boolean): void {
    this.error = value;
  }

  hasError(): boolean {
    return (this.error || this.configError);
  }

  getIconName(value: string): string {
    const typeOption = this.typeOptions.find(type => type.value === value);
    if (typeOption) {
      return typeOption.icon;
    }
    return 'plus';
  }

  layoutChange(value: string): void {
    this.displayContent.configuration.layout = value;
    this.updateLayouts();
    this.displayContent.components.map(component => {
      if (component.layoutComponent > (this.currentLayout.displays.length - 1)) {
        component.layoutComponent = 0;
      }
    });
  }

  getLayoutComponents(displayIndex: number): VisualisationComponent[] {
    return this.visualisationService.getComponentsForLayout(this.displayContent, displayIndex);
  }

  componentLayoutMoveUp(componentId: number, componentLayoutIndex: number): void {
    const componentIndex = this.displayContent.components.findIndex(c => c.id === componentId);
    const currentComponent = this.displayContent.components[componentIndex];
    if (componentLayoutIndex > 0) {
      const componentList = this.getLayoutComponents(currentComponent.layoutComponent);
      const previousComponent = componentList[componentLayoutIndex - 1];
      const previousComponentIndex = this.displayContent.components.findIndex(c => c.id === previousComponent.id);
      this.displayContent.components.splice(previousComponentIndex, 0, this.displayContent.components.splice(componentIndex, 1)[0]);
    } else if (currentComponent.layoutComponent > 0) {
      currentComponent.layoutComponent--;
    }
  }

  componentLayoutMoveDown(componentId: number, componentLayoutIndex: number): void {
    const componentIndex = this.displayContent.components.findIndex(c => c.id === componentId);
    const currentComponent = this.displayContent.components[componentIndex];
    const componentList = this.getLayoutComponents(currentComponent.layoutComponent);
    if (componentLayoutIndex < (componentList.length - 1)) {
      const nextComponent = componentList[componentLayoutIndex + 1];
      const nextComponentIndex = this.displayContent.components.findIndex(c => c.id === nextComponent.id);
      this.displayContent.components.splice(nextComponentIndex, 0, this.displayContent.components.splice(componentIndex, 1)[0]);
    } else if (currentComponent.layoutComponent < (this.currentLayout.displays.length - 1)) {
      currentComponent.layoutComponent++;
    }
  }

  newComponent(layoutIndex: number): void {
    this.currentComponent = this.visualisationService.getEmptyComponent(layoutIndex);
    this.displayContent.components.push(this.currentComponent);
  }

  selectComponent(componentId: number): void {
    this.currentComponent = this.displayContent.components.find(c => c.id === componentId);
  }

  deleteComponent(componentId: number): void {
    const dialogRef = this.dialog.open(ConfirmDeleteDialogComponent, {
      data: { message: 'Would you like to delete this layout component?', confirmName: false }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        if (this.currentComponent.id === componentId) {
          this.currentComponent = null;
        }
        this.displayContent.components = this.displayContent.components.filter(c => c.id !== componentId);
        this.ref.markForCheck();
      }
    });
  }

  dateError(error: boolean, field: string) {
    if (error) {
      this.dateRangeForm.get(field).setErrors({ date: error });
    } else {
      this.dateRangeForm.get(field).setErrors(null);
    }
  }

  checkCurrentDate(value: moment.Moment) {
    this.currentDateError = false;
    if (value.isBefore(this.dateRangeForm.get('from').value)) {
      this.currentDateError = true;
    }
    if (this.dateRangeForm.get('to').value == null && value.isAfter(moment())) {
      this.currentDateError = true;
    } else if (value.isAfter(this.dateRangeForm.get('to').value)) {
      this.currentDateError = true;
    }
    this.dateError(this.currentDateError, 'current');
  }

  updateDateRange(value: moment.Moment, field: string) {
    this.dateRangeForm.get(field).setValue(value);
    const currentDate = this.dateRangeForm.get('current').value;
    if (field === 'to' && currentDate.isAfter(value)) {
      this.updateCurrentDate(value);
    }
    if (field === 'from' && currentDate.isBefore(value)) {
      this.updateCurrentDate(value);
    }
  }

  updateCurrentDate(value: moment.Moment) {
    this.checkCurrentDate(value);
    if (!this.currentDateError) {
      this.dateRangeForm.get('current').setValue(value);
    }
  }

  importVisualisation() {
    const dialogRef = this.dialog.open(ImportVisualisationDialogComponent, {
      data: { vis: this.resource }
    });

    dialogRef.afterClosed().subscribe(result => {
        this.resource = result;
        this.updateDisplay();
        this.ref.markForCheck();
    });
  }

  exportVisualisation() {
    this.dialog.open(ExportVisualisationDialogComponent, {
      data: { vis: this.resource }
    });
  }

}
