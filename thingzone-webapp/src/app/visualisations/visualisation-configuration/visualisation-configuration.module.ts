/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule, MatRadioModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {
  faArrowRight,
  faCalendarAlt,
  faCaretDown,
  faCaretUp,
  faChartBar,
  faChartLine,
  faChartPie,
  faChevronDown,
  faChevronUp,
  faFileExport,
  faFileImport,
  faFont,
  faImage,
  faMapMarkerAlt,
  faPen,
  faTimes,
  faTrash,
} from '@fortawesome/free-solid-svg-icons';
import { CardComponentsModule } from 'app/ui/card-components/card-components.module';
import { ColourPickerModule } from 'app/ui/colour-picker/colour-picker.module';
import { DateTimeModule } from 'app/ui/date-time/date-time.module';
import { DynamicSelectModule } from 'app/ui/dynamic-select/dynamic-select.module';
import { FormComponentsModule } from 'app/ui/form-components/form-components.module';

import { AggregationConfigFormComponent } from './aggregation-config-form/aggregation-config-form.component';
import { EventConfigFormComponent } from './event-config-form/event-config-form.component';
import { ExportVisualisationDialogComponent } from './export-visualisation-dialog/export-visualisation-dialog.component';
import { GraphRangeFormComponent } from './graph-range-form/graph-range-form.component';
import { GraphSettingsComponent } from './graph-settings/graph-settings.component';
import { ImageConfigFormComponent } from './image-config-form/image-config-form.component';
import { ImportVisualisationDialogComponent } from './import-visualisation-dialog/import-visualisation-dialog.component';
import { MapConfigFormComponent } from './map-config-form/map-config-form.component';
import { MapSettingsComponent } from './map-settings/map-settings.component';
import { QuerySourceConfigurationComponent } from './query-source-configuration/query-source-configuration.component';
import { QuerySourceFormComponent } from './query-source-form/query-source-form.component';
import { ValueConfigFormComponent } from './value-config-form/value-config-form.component';
import { VisualisationComponentFormComponent } from './visualisation-component-form/visualisation-component-form.component';
import { VisualisationFormComponent } from './visualisation-form/visualisation-form.component';


@NgModule({
  imports: [
    CommonModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    MatInputModule,
    MatCheckboxModule,
    MatRadioModule,
    MatButtonModule,
    MatDatepickerModule,
    MatDialogModule,
    DynamicSelectModule,
    ColourPickerModule,
    FormComponentsModule,
    CardComponentsModule,
    DateTimeModule,
  ],
  declarations: [
    VisualisationFormComponent,
    VisualisationComponentFormComponent,
    QuerySourceFormComponent,
    QuerySourceConfigurationComponent,
    EventConfigFormComponent,
    AggregationConfigFormComponent,
    ImageConfigFormComponent,
    MapConfigFormComponent,
    ValueConfigFormComponent,
    GraphRangeFormComponent,
    MapSettingsComponent,
    GraphSettingsComponent,
    ExportVisualisationDialogComponent,
    ImportVisualisationDialogComponent
  ],
  entryComponents: [
    ExportVisualisationDialogComponent,
    ImportVisualisationDialogComponent,
  ],
  exports: [
    VisualisationFormComponent,
    VisualisationComponentFormComponent,
    QuerySourceFormComponent,
    MapSettingsComponent,
    GraphSettingsComponent,
  ]
})
export class VisualisationConfigurationModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faArrowRight, faTrash, faTimes, faChevronUp, faChevronDown, faCaretUp, faCaretDown,
      faPen, faMapMarkerAlt, faChartLine, faChartBar, faChartPie, faCalendarAlt, faImage, faFont,
      faFileImport, faFileExport);
  }
}
