/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SelectOption } from 'app/core/models/common/select-option';
import { DataQueryType, SecondaryQueryType } from 'app/core/models/queries/data-query';
import { MapConfiguration, QuerySource } from 'app/core/models/visualisations/display-data-source';
import { SecondaryQuery, SecondaryQueryConfiguration } from 'app/core/models/visualisations/secondary-query';
import { VisualisationComponentType } from 'app/core/models/visualisations/visualisation-component';
import { VisualisationService } from 'app/core/services/visualisation/visualisation.service';
import { BehaviorSubject, Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

@Component({
  selector: 'map-config-form',
  templateUrl: './map-config-form.component.html',
  styleUrls: ['./map-config-form.component.scss']
})
export class MapConfigFormComponent implements OnInit, OnDestroy {
  DataQueryType = DataQueryType;

  @Input() querySource: QuerySource;
  @Output() save: EventEmitter<MapConfiguration> = new EventEmitter();
  @Output() saveSecondaries: EventEmitter<SecondaryQuery> = new EventEmitter();
  @Output() removeSecondary: EventEmitter<number> = new EventEmitter();
  @Output() hasError: EventEmitter<boolean> = new EventEmitter();

  mapConfig: MapConfiguration;
  private additionalStreamOptions: BehaviorSubject<SelectOption[]> = new BehaviorSubject(null);
  additionalStreamOptions$ = this.additionalStreamOptions.asObservable();
  additionalStreamsFetched = false;

  mapConfigForm = this.formBuilder.group({
    icon: [''],
    colour: [''],
    outlineWidth: [null, Validators.min(0)],
    outlineColour: [''],
    radius: [null, Validators.min(1)],
    timeHover: [false],
    nameHover: [false],
    focusMap: [false],
    opacity: [null, [Validators.min(0), Validators.max(100)]],
    heatMap: [false],
  });

  colourQueryForm = this.formBuilder.group({
    enable: [false],
    streamName: ['', Validators.required],
    legend: [false],
    legendTitle: [],
    interval: [false],
    stops: this.formBuilder.array([
      this.colourStop
    ])
  });

  hoverQueryForm = this.formBuilder.group({
    enable: [false],
    queries: this.formBuilder.array([
      this.hoverQuery
    ])
  });
  subscriptions: Subscription[] = [];

  constructor(
    private visualisationService: VisualisationService,
    private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.mapConfig = this.querySource.configuration['mapConfiguration'];
    this.initFormValues();

    const mapSub = this.mapConfigForm.valueChanges.subscribe(formValues => {
      if (!this.mapConfigForm.invalid) {
        this.save.emit(formValues);
      }
      this.hasError.emit(this.mapConfigForm.invalid);
    });
    this.subscriptions.push(mapSub);

    const colourSub = this.colourQueryForm.valueChanges.subscribe(formValues => {
      if (!this.colourQueryForm.invalid) {
        this.updateColourQuery(formValues);
      }
      if (this.colourQueryForm.get('enable').value) {
        this.fetchStreamNames();
      } else {
        this.removeColourQuery();
      }
      this.hasError.emit(this.colourQueryForm.invalid);
    });
    this.subscriptions.push(colourSub);

    const hoverSub = this.hoverQueryForm.valueChanges.subscribe(formValues => {
      if (!this.hoverQueryForm.invalid) {
        this.updateHoverQuery(formValues);
      }
      if (this.hoverQueryForm.get('enable').value) {
        this.fetchStreamNames();
      } else {
        this.removeHoverQueries();
      }
      this.hasError.emit(this.hoverQueryForm.invalid);
    });
    this.subscriptions.push(hoverSub);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  initFormValues(): void {
    if (this.mapConfig != null) {
      this.mapConfigForm.patchValue(this.mapConfig, { onlySelf: true, emitEvent: false });
    }
    const colourQuery = this.getColourQuery();
    const hoverQueries = this.getHoverQueries();
    if (colourQuery != null || hoverQueries.length > 0) {
      this.fetchStreamNames();
      let colourForm: object = { enable: false };
      if (colourQuery != null) {
        let interval = false;
        let stops = [];
        let legend = false;
        let legendTitle = null;
        if (colourQuery.configuration != null && colourQuery.configuration.mapColourConfiguration != null) {
          interval = colourQuery.configuration.mapColourConfiguration.interval;
          stops = colourQuery.configuration.mapColourConfiguration.stops;
          legend = colourQuery.configuration.mapColourConfiguration.legend ? true : false;
          legendTitle = colourQuery.configuration.mapColourConfiguration.legendTitle;
        }

        this.stops.controls.splice(0);
        stops.forEach(() => this.addColourStop());

        colourForm = {
          streamName: colourQuery.streamName,
          enable: true,
          interval: interval,
          stops: stops,
          legend: legend,
          legendTitle: legendTitle,
        };

        const colourSub = this.additionalStreamOptions$.pipe(first(q => q != null)).subscribe(() => {
          this.colourQueryForm.patchValue(colourForm, { onlySelf: true, emitEvent: false });
        });
        this.subscriptions.push(colourSub);
      }

      let hoverForm: object = { enable: false };
      if (hoverQueries.length > 0) {
        const queries = [];
        this.hover.controls.splice(0);
        for (const query of hoverQueries) {
          this.addHoverQuery();
          queries.push({ stream: query.streamName, label: query.configuration.mapHoverConfiguration.label });
        }
        hoverForm = {
          enable: true,
          queries: queries
        };

        const hoverSub = this.additionalStreamOptions$.pipe(first(q => q != null)).subscribe(() => {
          this.hoverQueryForm.patchValue(hoverForm, { onlySelf: true, emitEvent: false });
        });
        this.subscriptions.push(hoverSub);
      }
    }
  }

  removeSecondaryQuery(id: number) {
    this.removeSecondary.emit(id);
    this.querySource.secondaryQueries = this.querySource.secondaryQueries.filter(element => element.id !== id);
  }

  fetchStreamNames(forceUpdate = false): void {
    if (forceUpdate || !this.additionalStreamsFetched) {
      this.visualisationService.queryStreamNames(VisualisationComponentType.MAPCOMBINATION, this.querySource).subscribe(options => {
        this.additionalStreamOptions.next(options);
        this.additionalStreamsFetched = true;
      });
    }
  }

  selectStreamName(value): void {
    this.colourQueryForm.patchValue({ streamName: value });
  }

  getColourQuery(): SecondaryQuery {
    if (this.querySource.secondaryQueries != null && this.querySource.secondaryQueries.length > 0) {
      const colourQuery = this.querySource.secondaryQueries.find(query => query.secondaryType === SecondaryQueryType.COLOUR);
      return colourQuery;
    }
    return null;
  }

  get stops(): FormArray {
    return this.colourQueryForm.get('stops') as FormArray;
  }

  get colourStop(): FormGroup {
    return this.formBuilder.group({
      number: [''],
      colour: [''],
    });
  }

  updateColourStop(index: number, value: string) {
    this.stops.controls[index].get('colour').setValue(value);
  }

  addColourStop(): void {
    this.stops.push(this.colourStop);
  }

  removeStop(index: number): void {
    this.stops.removeAt(index);
  }

  removeColourQuery(): void {
    const colourQuery = this.getColourQuery();
    if (colourQuery != null) {
      this.removeSecondaryQuery(colourQuery.id);
    }
  }

  updateColourQuery(formValues: any): void {
    let colourQuery = this.getColourQuery();
    const configuration: SecondaryQueryConfiguration = {
      mapColourConfiguration: {
        interval: formValues['interval'],
        stops: formValues['stops'],
        legend: formValues['legend'],
        legendTitle: formValues['legendTitle'],
      }
    };

    if (colourQuery != null) {
      colourQuery.streamName = formValues['streamName'];
      colourQuery.configuration = configuration;
    } else {
      const newId = this.visualisationService.getComponentId();
      colourQuery = {
        id: newId,
        streamName: formValues['streamName'],
        query: DataQueryType.METRICS,
        secondaryType: SecondaryQueryType.COLOUR,
        configuration: configuration
      };
    }
    this.saveSecondaries.emit(colourQuery);
  }

  get hover(): FormArray {
    return this.hoverQueryForm.get('queries') as FormArray;
  }

  get hoverQuery(): FormGroup {
    return this.formBuilder.group({
      stream: ['', Validators.required],
      label: ['', Validators.required],
    });
  }

  removeHover(index: number): void {
    const streamName = this.hover.controls[index].get('stream').value;
    const hoverQuery = this.getHoverQuery(streamName);
    if (hoverQuery != null) {
      this.removeSecondaryQuery(hoverQuery.id);
    }
    this.hover.removeAt(index);
  }

  addHoverQuery(): void {
    this.hover.push(this.hoverQuery);
  }

  selectHoverStreamName(index: number, value): void {
    this.hover.controls[index].get('stream').setValue(value);
  }

  getHoverQueries(): SecondaryQuery[] {
    if (this.querySource.secondaryQueries != null && this.querySource.secondaryQueries.length > 0) {
      const colourQueries = this.querySource.secondaryQueries.filter(query =>
        query.secondaryType === SecondaryQueryType.HOVER);
      return colourQueries;
    }
    return [];
  }

  removeHoverQueries(): void {
    const hoverQueries = this.getHoverQueries();
    for (const query of hoverQueries) {
      this.removeSecondaryQuery(query.id);
    }
  }

  getHoverQuery(stream: string): SecondaryQuery {
    if (this.querySource.secondaryQueries != null && this.querySource.secondaryQueries.length > 0) {
      const colourQuery = this.querySource.secondaryQueries.find(query =>
        query.secondaryType === SecondaryQueryType.HOVER && query.streamName === stream);
      return colourQuery;
    }
    return null;
  }

  updateHoverQuery(formValues: any): void {
    for (const query of formValues['queries']) {
      let hoverQuery = this.getHoverQuery(query.stream);
      const configuration: SecondaryQueryConfiguration = {
        mapHoverConfiguration: {
          label: query.label
        }
      };

      if (hoverQuery != null) {
        if (hoverQuery.configuration.mapHoverConfiguration.label === query.label) {
          // query has not been updated
          continue;
        }
        hoverQuery.configuration = configuration;
      } else {
        const newId = this.visualisationService.getComponentId();
        hoverQuery = {
          id: newId,
          streamName: query.stream,
          query: DataQueryType.METRICS,
          secondaryType: SecondaryQueryType.HOVER,
          configuration: configuration
        };
      }
      this.saveSecondaries.emit(hoverQuery);
    }
  }
}
