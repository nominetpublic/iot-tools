/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { SelectOption } from 'app/core/models/common/select-option';
import { DataQueryType } from 'app/core/models/queries/data-query';
import { QuerySource } from 'app/core/models/visualisations/display-data-source';
import { VisualisationComponentType } from 'app/core/models/visualisations/visualisation-component';
import { SearchService } from 'app/core/services/search/search.service';
import { VisualisationService } from 'app/core/services/visualisation/visualisation.service';
import { SearchResource } from 'app/ui/search/models/search-resource';
import { BehaviorSubject, Subscription } from 'rxjs';
import { first } from 'rxjs/operators';


@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'query-source-form',
  templateUrl: './query-source-form.component.html',
  styleUrls: ['./query-source-form.component.scss']
})
export class QuerySourceFormComponent implements OnInit, OnChanges, OnDestroy {
  DataQueryType = DataQueryType;
  @Input() dataSource: QuerySource;
  @Input() displayType: VisualisationComponentType;
  @Input() open = true;
  @Input() widget = false;
  @Input() allowedQueryTypes: DataQueryType[] = [];
  @Output() delete: EventEmitter<number> = new EventEmitter();
  @Output() setError: EventEmitter<boolean> = new EventEmitter();
  @Output() updateQuery: EventEmitter<DataQueryType> = new EventEmitter();

  private streamOptions: BehaviorSubject<SelectOption[]> = new BehaviorSubject(null);
  streamOptions$ = this.streamOptions.asObservable();

  private queryTypeOptions: BehaviorSubject<SelectOption[]> = new BehaviorSubject([]);
  queryTypeOptions$ = this.queryTypeOptions.asObservable();

  subscriptions: Subscription[] = [];
  deviceSavedSearches: SearchResource[] = [];
  deviceFilterOptions: SelectOption[] = [];
  deviceFilterFetched = false;

  initialName: string;
  nameControl = new FormControl('');
  queryForm = this.formBuilder.group({
    queryType: ['', Validators.required],
    device: [],
    stream: [],
    selectDevice: [],
    deviceHover: [],
  });

  constructor(
    private visualisationService: VisualisationService,
    private search: SearchService,
    private formBuilder: FormBuilder,
    protected ref: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.initialName = (this.dataSource != null && this.dataSource.name)
      ? this.dataSource.name : 'New Query';
    this.search.deviceSavedSearches$.pipe(first(q => q != null)).subscribe((res: SearchResource[]) => {
      this.setDeviceFilters(res);
    });

    const nameSub = this.nameControl.valueChanges.subscribe(name => {
      this.dataSource.name = name;
    });
    this.subscriptions.push(nameSub);

    const querySub = this.queryForm.valueChanges.subscribe(formValues => {
      this.saveForm(formValues);
    });
    this.subscriptions.push(querySub);
  }

  ngOnChanges(changes: SimpleChanges) {
    let updated = false;
    if ('displayType' in changes) {
      if (changes.displayType.currentValue !== changes.displayType.previousValue) {
        updated = true;
        this.initFormValues();
      }
    }
    if (!updated && 'allowedQueryTypes' in changes) {
      if (changes.allowedQueryTypes.currentValue.length !== changes.allowedQueryTypes.previousValue.length ||
        !changes.allowedQueryTypes.currentValue.every((e) => changes.allowedQueryTypes.previousValue.includes(e))) {
        this.initFormValues();
      }
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  remove(): void {
    this.delete.emit(this.dataSource.id);
  }

  collapse(event): void {
    event.stopPropagation();
    this.open = false;
  }

  expand(event): void {
    event.stopPropagation();
    this.open = true;
  }

  initFormValues(): void {
    this.fetchStreamNames();
    this.nameControl.setValue(this.dataSource.name);

    let queries = this.visualisationService.selectQueryOptions(this.displayType);
    if (this.allowedQueryTypes.length > 0) {
      queries = queries.filter(queryType => this.allowedQueryTypes.includes(queryType.value));
    }
    if (queries.length === 1) {
      this.dataSource.query = queries[0].value;
    }
    this.queryTypeOptions.next(queries);

    // the 'device' value is not prepopulated as the selected option is not shown using a dropdown
    const querySettings = {};
    if (this.dataSource.streamFilter != null) {
      querySettings['stream'] = this.dataSource.streamFilter;
      querySettings['deviceHover'] = true;
    }
    if (this.dataSource.query != null) {
      querySettings['queryType'] = this.dataSource.query;
    }
    if (this.dataSource.selectDevice != null && this.dataSource.selectDevice) {
      querySettings['selectDevice'] = this.dataSource.selectDevice;
    }

    // make sure the correct streams are fetched before trying to display the form with data
    this.streamOptions$.pipe(first(q => q != null)).subscribe(() => {
      this.queryForm.patchValue(querySettings, { onlySelf: true, emitEvent: false });
      this.ref.markForCheck();
    });
  }

  saveForm(formValues): void {
    let fetchStreams = false;
    if (!this.queryForm.invalid) {
      if (this.dataSource.query !== formValues['queryType']) {
        fetchStreams = true;
      }
      this.dataSource.query = formValues['queryType'];
      this.updateQuery.emit(this.dataSource.query);

      if ('device' in formValues && formValues['device'] != null) {
        const searchResource: SearchResource = this.deviceSavedSearches.find(search => search.key === formValues['device']);
        this.dataSource.deviceFilter = this.search.getSavedSearch(searchResource);
        fetchStreams = true;
      }
      this.dataSource.streamFilter = null;
      if ('stream' in formValues && formValues['stream'] != null) {
          this.dataSource.streamFilter = formValues['stream'];
      }
      if (formValues['queryType'] === 'DEVICELOCATION' && !formValues['deviceHover']) {
        this.dataSource.streamFilter = null;
      }
      if ('selectDevice' in formValues) {
        this.dataSource.selectDevice = formValues['selectDevice'];
      }
      if (fetchStreams) {
        this.fetchStreamNames();
      }
    }
    this.ref.markForCheck();
  }

  setDeviceFilters(searches: SearchResource[]): void {
    if (searches != null) {
      this.deviceSavedSearches = searches;
      this.deviceFilterOptions = searches.map(search => ({ title: search.name, value: search.key }));
    }
  }

  selectQuery(value: DataQueryType): void {
    this.queryForm.patchValue({ queryType: value });
  }

  selectStreamFilter(value): void {
    this.queryForm.patchValue({ stream: value });
  }

  selectDeviceFilter(value): void {
    this.queryForm.patchValue({ device: value });
  }

  removeDeviceFilter(): void {
    this.queryForm.patchValue({ device: null });
    this.dataSource.deviceFilter = null;
    this.fetchStreamNames();
  }

  configError(value: boolean): void {
    this.setError.emit(value);
  }

  fetchStreamNames(): void {
    this.visualisationService.queryStreamNames(this.displayType, this.dataSource).subscribe(options => {
      this.streamOptions.next(options);
    }, () => {
      this.streamOptions.next([]);
    });
  }

  showTerm(searchValue: any, searchTerm: string, index: number): string {
    return this.search.showTerm(searchValue, searchTerm, index);
  }

}
