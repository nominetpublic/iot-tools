/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GraphDisplayConfiguration } from 'app/core/models/visualisations/visualisation-component';
import { Subscription } from 'rxjs';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'graph-range-form',
  templateUrl: './graph-range-form.component.html',
  styleUrls: ['./graph-range-form.component.scss']
})
export class GraphRangeFormComponent implements OnInit {
  @Input() config: GraphDisplayConfiguration;
  @Output() save: EventEmitter<GraphDisplayConfiguration> = new EventEmitter();
  @Output() hasError: EventEmitter<boolean> = new EventEmitter();

  rangeForm = this.formBuilder.group({
    enable: [false],
    stops: this.formBuilder.array([
      this.rangeBoundary
    ])
  });
  subscriptions: Subscription[] = [];

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.initFormValues();

    const colourSub = this.rangeForm.valueChanges.subscribe(formValues => {
      if (!this.rangeForm.invalid) {
        this.save.emit(formValues);
      }
      const colorQueryEnabled = this.rangeForm.get('enable').value;
      if (!colorQueryEnabled) {
        this.save.emit({ stops: undefined });
      }
      this.hasError.emit(this.rangeForm.invalid);
    });
    this.subscriptions.push(colourSub);
  }

  initFormValues() {
    let stops = [];
    let configRanges: object = { enable: false };

    if (this.config != null && this.config.stops != null && this.config.stops.length > 0) {
      stops = this.config.stops;

      this.stops.controls.splice(0);
      stops.forEach(() => this.addStop());

      configRanges = {
        enable: true,
        stops: stops
      };
    }
    this.rangeForm.patchValue(configRanges, { onlySelf: true, emitEvent: false });
  }

  get stops(): FormArray {
    return this.rangeForm.get('stops') as FormArray;
  }

  get rangeBoundary(): FormGroup {
    return this.formBuilder.group({
      low: [null],
      high: [null],
      name: ['', Validators.required],
    });
  }

  addStop(): void {
    this.stops.push(this.rangeBoundary);
  }

  removeStop(index: number): void {
    this.stops.removeAt(index);
  }

  move(currentIndex: number, newIndex: number) {
    const ranges = [...this.stops.value];
    if (currentIndex > 0 && currentIndex < ranges.length - 1) {
      [ranges[currentIndex], ranges[newIndex]] = [ranges[newIndex], ranges[currentIndex]];
      this.stops.setValue(ranges);
    }
  }
}
