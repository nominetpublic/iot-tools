/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AggQueryConfiguration } from 'app/core/models/visualisations/display-data-source';
import { VisualisationComponentType } from 'app/core/models/visualisations/visualisation-component';
import { VisualisationService } from 'app/core/services/visualisation/visualisation.service';
import { Subscription } from 'rxjs';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'aggregation-config-form',
  templateUrl: './aggregation-config-form.component.html',
  styleUrls: ['./aggregation-config-form.component.scss']
})
export class AggregationConfigFormComponent implements OnInit, OnDestroy {
  VisualisationComponentType = VisualisationComponentType;
  @Input() aggregationQuery: AggQueryConfiguration;
  @Input() displayType: VisualisationComponentType;
  @Input() widget = false;
  @Output() save: EventEmitter<AggQueryConfiguration> = new EventEmitter();
  @Output() hasError: EventEmitter<boolean> = new EventEmitter();

  subscriptions: Subscription[] = [];
  aggregationOptions = this.visualisationService.aggregationTypes;

  aggregationConfigForm = this.formBuilder.group({
    groupByStream: [],
    math: [],
    interval: [],
    size: [null, [Validators.pattern('-?\\d+(\\.\\d*?)?')]],
    useCurrentTime: [],
  });

  constructor(
    private formBuilder: FormBuilder,
    private visualisationService: VisualisationService
  ) { }

  ngOnInit() {
    const alertSub = this.aggregationConfigForm.valueChanges.subscribe(formValues => {
      if (!this.aggregationConfigForm.invalid) {
        this.save.emit(formValues);
      }
      this.hasError.emit(this.aggregationConfigForm.invalid);
    });
    this.subscriptions.push(alertSub);

    if (this.aggregationQuery != null) {
      this.aggregationConfigForm.patchValue(this.aggregationQuery, { onlySelf: true, emitEvent: false });
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  selectAggregation(value: string): void {
    this.aggregationConfigForm.get('math').setValue(value);
  }

}
