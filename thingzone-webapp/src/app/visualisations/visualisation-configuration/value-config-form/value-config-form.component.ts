/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DataQueryType } from 'app/core/models/queries/data-query';
import { QuerySource, ValueConfiguration } from 'app/core/models/visualisations/display-data-source';
import { Subscription } from 'rxjs';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'value-config-form',
  templateUrl: './value-config-form.component.html',
  styleUrls: ['./value-config-form.component.scss']
})
export class ValueConfigFormComponent implements OnInit, OnDestroy {
  DataQueryType = DataQueryType;
  @Input() dataSource: QuerySource;
  @Output() save: EventEmitter<ValueConfiguration> = new EventEmitter();
  @Output() hasError: EventEmitter<boolean> = new EventEmitter();

  subscriptions: Subscription[] = [];
  valueConfigForm = this.formBuilder.group({
    showLabel: [],
    units: [],
    valueColour: [],
    valueSize: ['', [Validators.min(0), Validators.max(200)]],
    precision: [null, [Validators.pattern('-?\\d+(\\.\\d*?)?')]],
    type: ['type'],
  });

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    const valueSub = this.valueConfigForm.valueChanges.subscribe(formValues => {
      if (!this.valueConfigForm.invalid) {
        this.save.emit(formValues);
      }
      this.hasError.emit(this.valueConfigForm.invalid);
    });
    this.subscriptions.push(valueSub);

    if (this.dataSource.configuration.valueConfiguration != null) {
      this.valueConfigForm.patchValue(this.dataSource.configuration.valueConfiguration, { onlySelf: true, emitEvent: false });
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

}
