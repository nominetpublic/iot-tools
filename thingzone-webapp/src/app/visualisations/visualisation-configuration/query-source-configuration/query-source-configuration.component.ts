/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataQueryType } from 'app/core/models/queries/data-query';
import { SecondaryQuery } from 'app/core/models/visualisations/secondary-query';
import { VisualisationComponentType } from 'app/core/models/visualisations/visualisation-component';
import { Subscription } from 'rxjs';

import {
  AggQueryConfiguration,
  EventQueryConfiguration,
  ImageQueryConfiguration,
  MapConfiguration,
  QuerySource,
  ValueConfiguration,
} from '../../../core/models/visualisations/display-data-source';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'query-source-configuration',
  templateUrl: './query-source-configuration.component.html',
  styleUrls: ['./query-source-configuration.component.scss']
})
export class QuerySourceConfigurationComponent implements OnInit, OnDestroy, OnChanges {
  VisualisationComponentType = VisualisationComponentType;
  DataQueryType = DataQueryType;
  @Input() displayType: VisualisationComponentType;
  @Input() queryType: DataQueryType;
  @Input() dataSource: QuerySource;
  @Input() widget = false;
  @Output() hasError: EventEmitter<boolean> = new EventEmitter();

  subscriptions: Subscription[] = [];

  locationConfigForm = this.formBuilder.group({
    showAsLine: [],
    timeFromCurrent: [],
    sizeFromCurrent: [null, [Validators.pattern('-?\\d+(\\.\\d*?)?')]],
    forward: [],
  });

  valueConfigForm = this.formBuilder.group({
    type: ['type'],
  });

  constructor(
    protected cd: ChangeDetectorRef,
    private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.initFormValues();

    const locationSub = this.locationConfigForm.valueChanges.subscribe(formValues => {
      this.saveConfigForm(this.locationConfigForm, 'locationQuery', formValues);
    });
    this.subscriptions.push(locationSub);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.initFormValues();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  initFormValues(): void {
    const config = this.dataSource.configuration ? this.dataSource.configuration : {};
    switch (this.displayType) {
      case VisualisationComponentType.MAP:
        if (!('locationQuery' in config)) {
          config['locationQuery'] = {};
        }
        this.locationConfigForm.patchValue(config['locationQuery'], { onlySelf: true, emitEvent: false });
        break;
      case VisualisationComponentType.PIECHART:
        if (!('valueConfiguration' in config)) {
          config['valueConfiguration'] = {};
        }
        this.valueConfigForm.patchValue(config['valueConfiguration'], { onlySelf: true, emitEvent: false });
        break;
    }
  }

  saveConfigForm(configForm: FormGroup, key, formValues): void {
    if (!configForm.invalid) {
      this.dataSource.configuration[key] = formValues;
    }
    this.setError(configForm.invalid);
  }

  saveEventConfig(eventQuery: EventQueryConfiguration): void {
    this.dataSource.configuration['eventQuery'] = eventQuery;
  }

  saveAggregationConfig(aggQuery: AggQueryConfiguration): void {
    this.dataSource.configuration['aggregationQuery'] = aggQuery;
  }

  saveImageConfig(imageQuery: ImageQueryConfiguration): void {
    this.dataSource.configuration['imageQuery'] = imageQuery;
  }

  saveMapConfig(mapConfig: MapConfiguration): void {
    this.dataSource.configuration['mapConfiguration'] = mapConfig;
  }

  saveValueConfig(mapConfig: ValueConfiguration): void {
    this.dataSource.configuration['valueConfiguration'] = mapConfig;
  }

  saveSecondaryQueries(secondaryQuery: SecondaryQuery): void {
    if (this.dataSource.secondaryQueries == null) {
      this.dataSource.secondaryQueries = [];
    }
    let query = this.dataSource.secondaryQueries.find(element => element.id === secondaryQuery.id);
    if (query != null) {
      query = secondaryQuery;
    } else {
      this.dataSource.secondaryQueries.push(secondaryQuery);
    }
  }

  removeSecondaryQuery(id: number): void {
    if (this.dataSource.secondaryQueries == null) {
      this.dataSource.secondaryQueries = [];
    }
    const queries = this.dataSource.secondaryQueries.filter(element => element.id !== id);
    this.dataSource.secondaryQueries = queries;
  }

  setError(value: boolean): void {
    this.hasError.emit(value);
  }

}
