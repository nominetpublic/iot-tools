/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { DisplayResource } from 'app/core/models/visualisations/display-resource';

export interface ExportVisualisationDialogData {
  vis: DisplayResource;
}

@Component({
  selector: 'export-visualisation-dialog',
  templateUrl: './export-visualisation-dialog.component.html',
  styleUrls: ['./export-visualisation-dialog.component.scss']
})
export class ExportVisualisationDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<ExportVisualisationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ExportVisualisationDialogData
  ) { }

  onCancel(): void {
    this.dialogRef.close();
  }

  downloadVisualisation() {
    const visualisation: DisplayResource = this.data.vis;
    const blob = new Blob([JSON.stringify(visualisation.content)], { type: 'application/json' });
    const link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    link.download = visualisation.name;
    link.click();
    this.dialogRef.close();
  }
}
