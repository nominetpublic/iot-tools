/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { GraphDisplayConfiguration } from 'app/core/models/visualisations/visualisation-component';
import { EventUtils } from 'app/core/utils/event-utils';
import * as moment from 'moment';

import { BarGraphData, ChartUtils, GraphData } from '../chart-utils';

@Component({
  selector: 'gauge',
  templateUrl: './gauge.component.html',
  styleUrls: ['./gauge.component.scss']
})
export class GaugeComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {

  @Input() dataLayers: GraphData[] = [];
  @Input() config: GraphDisplayConfiguration;

  colorScheme = ChartUtils.colourScheme();
  escapeLabel = ChartUtils.escapeLabel;
  results: BarGraphData[] = [];
  units: string = null;
  showText = true;
  showAxis = true;
  min = 0;
  max = 100;
  valueFormatter = this.gaugeValueFormatting.bind(this);
  rangeCalculated: boolean;
  ranges: [{ low: number; high: number; name: string; }];
  customColors = null;

  constructor(protected cd: ChangeDetectorRef) { }

  ngOnInit() {
    window.addEventListener('panelResize', this.resize.bind(this));
    this.updateDisplaySettings();
  }

  ngAfterViewInit() {
    this.resize();
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('config' in changes) {
      this.updateDisplaySettings();
    }
    if ('dataLayers' in changes) {
      this.getResults();
    }
  }

  ngOnDestroy() {
    window.removeEventListener('panelResize', this.resize);
  }

  resize(): void {
    // trigger ngx-charts resize
    window.dispatchEvent(new Event('resize'));
  }

  gaugeValueFormatting(value) {
    if (value == null) {
      return '';
    }
    if (this.config != null) {
      if (this.config.formatType === 'hidden') {
        return '';
      }
      if (this.config.formatType === 'average') {
        const average = value / this.results.length;
        return average.toLocaleString();
      }
    }
    return value.toLocaleString();
  }

  formatTooltipValue(value) {
    if (this.units != null) {
      value = value + this.units;
    }
    return value;
  }

  updateDisplaySettings() {
    this.showAxis = true;
    this.showText = true;

    if (this.config != null) {
      if (this.config.hideXAxis != null) {
        if (this.config.hideXAxis === false) {
          this.showText = true;
        } else if (this.config.hideXAxis === true) {
          this.showText = false;
        }
      }
      if (this.config.xLabel != null) {
        this.units = this.config.xLabel;
      }
      if (this.config.hideYAxis != null) {
        if (this.config.hideYAxis === false) {
          this.showAxis = true;
        } else if (this.config.hideYAxis === true) {
          this.showAxis = false;
        }
      }
      if (this.config.min != null) {
        this.min = this.config.min as number;
      }
      if (this.config.max != null) {
        this.max = this.config.max as number;
      }
      if (this.config.stops != null && this.config.stops.length > 0) {
        this.ranges = this.config.stops;
        this.calculateRanges(this.results);
      }
      if (this.config.displayType === 'severity') {
        this.customColors = EventUtils.severityColors;
      }
    }
  }

  getResults(): void {
    let newResults = [];
    for (const layer of this.dataLayers) {
      if (layer.data != null) {
        newResults = newResults.concat(layer.data);
      }
    }
    if (newResults.length > 0) {
      if (newResults.length > 10 && !this.ranges) {
        this.results = newResults.slice(0, 10);
      } else {
        this.results = [...newResults];
      }
      this.rangeCalculated = false;
      this.cd.markForCheck();
    }
    if (this.ranges) {
      this.calculateRanges(this.results);
    }
  }

  calculateRanges(results) {
    if (results.length > 0 && !this.rangeCalculated) {
      const rangeHistogram = {};
      let interval;
      let currentTime;
      let currentValue;
      let lastTime;
      let lastValue;
      // set up initial values
      for (const range of this.ranges) {
        rangeHistogram[range.name] = 0;
      }
      // calculate totals for each range
      for (const result of results) {
        currentTime = moment(result.timestamp);
        currentValue = result.value;
        if (lastTime) {
          interval = currentTime.diff(lastTime, 'seconds');
          for (const range of this.ranges) {
            if (range.low == null && range.high == null) {
              continue;
            }
            if ((range.low == null || result.value > range.low)
              && (range.high == null || result.value <= range.high)) {
              rangeHistogram[range.name] = rangeHistogram[range.name] + interval;
            }
          }
        }
        lastTime = currentTime;
        lastValue = currentValue;
      }
      this.results = Object.keys(rangeHistogram).map(name => ({ name: name, value: rangeHistogram[name] }));
      this.rangeCalculated = true;

      this.cd.markForCheck();
    }
  }
}
