/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Input, OnChanges, OnInit } from '@angular/core';
import { ValueConfiguration } from 'app/core/models/visualisations/display-data-source';
import { VisualisationDataLayer } from 'app/core/models/visualisations/visualisation-data-layer';

interface DisplayValue {
  value: string;
  label?: string;
  units?: string;
  colour?: string;
  size?: string;
}

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'value-widget',
  templateUrl: './value-widget.component.html',
  styleUrls: ['./value-widget.component.scss']
})
export class ValueWidgetComponent implements OnInit, OnChanges {
  @Input() dataLayers: VisualisationDataLayer[] = [];
  @Input() widget = false;

  values: DisplayValue[];
  widgetElement: HTMLElement;

  constructor(
    private element: ElementRef,
    private ref: ChangeDetectorRef) {
    this.widgetElement = element.nativeElement;
  }

  ngOnInit() {
    this.updateValues(this.dataLayers);
  }

  ngOnChanges(changes) {
    if ('dataLayers' in changes) {
      const current: VisualisationDataLayer[] = changes.dataLayers.currentValue || [];
      this.updateValues(current);
    }
  }

  updateValues(dataLayers: VisualisationDataLayer[]) {
    this.values = dataLayers.map((layer: VisualisationDataLayer) => {
      const config = layer.source.configuration.valueConfiguration as ValueConfiguration;
      let label = null;
      let units = null;
      let colour = null;
      let size = null;
      if (config != null) {
        if (config.showLabel != null && config.showLabel) {
          label = layer.label;
        }
        if (config.units != null) {
          units = config.units;
        }
        if (config.valueColour != null) {
          colour = config.valueColour;
        }
        if (config.valueColour != null) {
          size = config.valueSize;
        }
      }
      let value = '';
      if (layer.data != null && layer.data.length > 0) {
        if (layer.data && layer.data.length > 0) {
          const mostRecent = layer.data[0];
          if (typeof mostRecent === 'number') {
            value = config != null && config.precision != null
              ? mostRecent.toPrecision(config.precision) : mostRecent.toPrecision();
          } else {
            value = mostRecent;
          }
        }
      }
      return { value, units, label, colour, size };
    });
    this.ref.markForCheck();
  }

  get cols(): string {
    if (this.values.length === 1) {
      return '1';
    }
    return '2';
  }

  get rowHeight(): string {
    const height = this.widgetElement.offsetHeight;
    if (this.values.length <= 2) {
      return height + 'px';
    }
    const rowNumber = Math.round(this.values.length / 2);
    return height / rowNumber + 'px';
  }

}
