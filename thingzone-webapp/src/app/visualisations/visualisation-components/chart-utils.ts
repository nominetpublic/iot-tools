/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { QuerySource } from 'app/core/models/visualisations/display-data-source';
import * as moment from 'moment';

export interface GraphData {
  label: string;
  data: any[];
  source: QuerySource;
}

export interface BubbleGraphData {
  name: string;
  series: {
    name: string;
    x: number;
    y: number;
    r: number;
  }[];
}

export interface LineGraphData {
  name: string;
  series: {
    name: string;
    value: any;
    min: number;
    max: number;
  }[];
}

export interface BarGraphData {
  name: string;
  value: any;
  timestamp?: moment.Moment;
  extra?: object;
}

export class ChartUtils {

  public static colourScheme() {
    return {
      domain: [
        '#0099C8',
        '#F5685B',
        '#90D4AD',
        '#A9CAE9',
        '#F3A4BE',
        '#FFCC81',
        '#8685A0',
      ]
    };
  }

  public static escapeLabel(label: any): string {
    return label.toLocaleString().replace(/[&'`"<>]/g, match => {
      return {
        '&': '&amp;',
        '\'': '&#x27;',
        '`': '&#x60;',
        '"': '&quot;',
        '<': '&lt;',
        '>': '&gt;',
      }[match];
    });
  }
}
