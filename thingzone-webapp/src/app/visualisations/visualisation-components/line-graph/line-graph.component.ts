/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { animate, style, transition, trigger } from '@angular/animations';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  NgZone,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';
import {
  BaseChartComponent,
  calculateViewDimensions,
  ColorHelper,
  getScaleType,
  getUniqueXDomainValues,
  ViewDimensions,
} from '@swimlane/ngx-charts';
import { id } from '@swimlane/ngx-charts/release/utils';
import { GraphDisplayConfiguration } from 'app/core/models/visualisations/visualisation-component';
import { DisplayContext, DisplayContextService } from 'app/core/services/visualisation/display-context.service';
import { D3Utils } from 'app/core/utils/d3-utils';
import { scaleLinear, scalePoint, scaleTime } from 'd3-scale';
import { curveStepAfter } from 'd3-shape';
import * as moment from 'moment';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { ChartUtils, GraphData, LineGraphData } from '../chart-utils';

@Component({
  selector: 'line-graph',
  templateUrl: './line-graph.component.html',
  styleUrls: ['./line-graph.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('animationState', [
      transition(':leave', [
        style({
          opacity: 1
        }),
        animate(
          500,
          style({
            opacity: 0
          })
        )
      ])
    ])
  ]
})
export class LineGraphComponent extends BaseChartComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {

  @Input() dataLayers: GraphData[] = [];
  @Input() config: GraphDisplayConfiguration;
  @Input() showTime = true;

  @Input() curve: any = curveStepAfter;
  @Input() xScaleMin: any = null;
  @Input() xScaleMax: any = null;
  @Input() autoScale;
  @Input() timeline;
  @Input() gradient: boolean;
  @Input() showGridLines = true;
  @Input() activeEntries: any[] = [];
  @Input() schemeType: string;
  @Input() rangeFillOpacity: number;
  @Input() trimXAxisTicks = true;
  @Input() trimYAxisTicks = true;
  @Input() rotateXAxisTicks = true;
  @Input() maxXAxisTickLength = 16;
  @Input() maxYAxisTickLength = 16;
  @Input() xAxisTickFormatting: any = D3Utils.d3TimeFormat;
  @Input() yAxisTickFormatting: any;
  @Input() xAxisTicks: any[];
  @Input() yAxisTicks: any[];
  @Input() roundDomains = false;
  @Input() tooltipDisabled = false;
  @Input() showRefLines = false;
  @Input() referenceLines: any;
  @Input() showRefLabels = true;
  @Input() yScaleMin: number;
  @Input() yScaleMax: number;

  @Output() activate: EventEmitter<any> = new EventEmitter();
  @Output() deactivate: EventEmitter<any> = new EventEmitter();

  subscriptions: Subscription[] = [];
  results: LineGraphData[] = [];
  colorScheme = ChartUtils.colourScheme();
  showXAxisLabel = false;
  showYAxisLabel = false;
  xAxisLabel: string = null;
  yAxisLabel: string = null;
  xAxis = true;
  yAxis = true;
  view: [number, number];
  currentTime: Date;
  dims: ViewDimensions;
  xSet: any;
  xDomain: any;
  yDomain: any;
  seriesDomain: any;
  yScale: any;
  xScale: any;
  colors: ColorHelper;
  scaleType: string;
  transform: string;
  clipPath: string;
  clipPathId: string;
  series: any;
  areaPath: any;
  margin = [10, 20, 10, 20];
  hoveredVertical: any; // the value of the x axis that is hovered over
  xAxisHeight = 0;
  yAxisWidth = 0;
  filteredDomain: any;
  hasRange: boolean; // whether the line has a min-max range around it
  currentPos = -1;
  dragging = false;
  startDrag = 0;
  endDrag = 0;
  hideAxisTicks = this.axisHideTicks.bind(this);

  constructor(
    protected displayContext: DisplayContextService,
    protected chartElement: ElementRef,
    protected zone: NgZone,
    protected cd: ChangeDetectorRef
  ) {
    super(chartElement, zone, cd);
  }

  ngOnInit() {
    this.updateDisplaySettings();
    if (this.showTime) {
      const displayContextSub = this.displayContext.context$.subscribe((context: DisplayContext) => {
        this.currentPosition(context.current);
      });
      this.subscriptions.push(displayContextSub);
    }
  }

  ngAfterViewInit() {
    this.bindPanelResizeEvent();
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('dataLayers' in changes) {
      this.getResults();
    }
    if ('config' in changes) {
      this.updateDisplaySettings();
    }
    this.update();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  update(): void {
    super.update();

    this.dims = calculateViewDimensions({
      width: this.width,
      height: this.height,
      margins: this.margin,
      showXAxis: this.xAxis,
      showYAxis: this.yAxis,
      xAxisHeight: this.xAxisHeight,
      yAxisWidth: this.yAxisWidth,
      showXLabel: this.showXAxisLabel,
      showYLabel: this.showYAxisLabel,
      showLegend: false,
    });

    this.xDomain = this.getXDomain();
    if (this.filteredDomain) {
      this.xDomain = this.filteredDomain;
    }
    this.yDomain = this.getYDomain();
    this.seriesDomain = this.getSeriesDomain();

    this.xScale = this.getXScale(this.xDomain, this.dims.width);
    this.yScale = this.getYScale(this.yDomain, this.dims.height);

    this.setColors();
    this.transform = `translate(${this.dims.xOffset} , ${this.margin[0]})`;

    this.clipPathId = 'clip' + id().toString();
    this.clipPath = `url(#${this.clipPathId})`;
  }

  updateDisplaySettings() {
    this.xAxis = true;
    this.yAxis = true;
    if (this.config != null) {
      if (this.config.xLabel != null && this.config.xLabel.trim() !== '') {
        this.xAxisLabel = this.config.xLabel;
        this.showXAxisLabel = true;
      }
      if (this.config.hideXAxis != null && this.config.hideXAxis) {
        this.xAxisTickFormatting = this.hideAxisTicks;
      }
      if (this.config.hideYAxis != null && this.config.hideYAxis) {
        this.yAxisTickFormatting = this.hideAxisTicks;
      }
      if (this.config.yLabel != null && this.config.yLabel.trim() !== '') {
        this.yAxisLabel = this.config.yLabel;
        this.showYAxisLabel = true;
      }
    }
  }

  getResults() {
    let newResults = [];
    for (const layer of this.dataLayers) {
      if (layer.data != null) {
        newResults = newResults.concat(layer.data);
      }
    }
    if (newResults.length > 0) {
      this.results = [...newResults];
      this.cd.markForCheck();
    }
  }

  private bindPanelResizeEvent(): void {
    const source = fromEvent(window, 'panelResize');
    const subscription = source.pipe(debounceTime(100)).subscribe(e => {
      this.update();
      if (this.cd) {
        this.cd.markForCheck();
      }
    });
    this.subscriptions.push(subscription);
  }

  getXDomain(): any[] {
    let values = getUniqueXDomainValues(this.results);
    this.scaleType = getScaleType(values);
    let domain = [];

    if (this.scaleType === 'linear') {
      values = values.map(v => Number(v));
    }

    let min; let max;
    if (this.scaleType === 'time' || this.scaleType === 'linear') {
      min = this.xScaleMin ? this.xScaleMin : Math.min(...values);
      max = this.xScaleMax ? this.xScaleMax : Math.max(...values);
    }

    if (this.scaleType === 'time') {
      domain = [new Date(min), new Date(max)];
      this.xSet = [...values].sort((a, b) => {
        const aDate = a.getTime();
        const bDate = b.getTime();
        if (aDate > bDate) return 1;
        if (bDate > aDate) return -1;
        return 0;
      });
    } else if (this.scaleType === 'linear') {
      domain = [min, max];
      // Use compare function to sort numbers numerically
      this.xSet = [...values].sort((a, b) => a - b);
    } else {
      domain = values;
      this.xSet = values;
    }
    return domain;
  }

  getYDomain(): any[] {
    const domain = [];
    for (const result of this.results) {
      for (const d of result.series) {
        if (domain.indexOf(d.value) < 0) {
          domain.push(d.value);
        }
        if (d.min !== undefined) {
          this.hasRange = true;
          if (domain.indexOf(d.min) < 0) {
            domain.push(d.min);
          }
        }
        if (d.max !== undefined) {
          this.hasRange = true;
          if (domain.indexOf(d.max) < 0) {
            domain.push(d.max);
          }
        }
      }
    }
    const values = [...domain];
    if (!this.autoScale) {
      values.push(0);
    }

    const min = this.yScaleMin ? this.yScaleMin : Math.min(...values);
    const max = this.yScaleMax ? this.yScaleMax : Math.max(...values);
    return [min, max];
  }

  getSeriesDomain(): any[] {
    return this.results.map(d => d.name);
  }

  getXScale(domain, width): any {
    let scale;

    if (this.scaleType === 'time') {
      scale = scaleTime()
        .range([0, width])
        .domain(domain);
    } else if (this.scaleType === 'linear') {
      scale = scaleLinear()
        .range([0, width])
        .domain(domain);

      if (this.roundDomains) {
        scale = scale.nice();
      }
    } else if (this.scaleType === 'ordinal') {
      scale = scalePoint()
        .range([0, width])
        .padding(0.1)
        .domain(domain);
    }
    return scale;
  }

  getYScale(domain, height): any {
    const scale = scaleLinear()
      .range([height, 0])
      .domain(domain);
    return this.roundDomains ? scale.nice() : scale;
  }

  updateYAxisWidth({ width }): void {
    this.yAxisWidth = width;
    this.update();
  }

  updateXAxisHeight({ height }): void {
    this.xAxisHeight = height;
    this.update();
  }

  setColors(): void {
    let domain;
    if (this.schemeType === 'ordinal') {
      domain = this.seriesDomain;
    } else {
      domain = this.yDomain;
    }
    this.colors = new ColorHelper(this.colorScheme, this.schemeType, domain, this.customColors);
  }

  updateHoveredVertical(item): void {
    this.hoveredVertical = item.value;
    this.deactivateAll();
  }

  @HostListener('mouseleave')
  hideCircles(): void {
    this.hoveredVertical = null;
    this.deactivateAll();
  }

  onMouseDown(event: MouseEvent): void {
    if (!this.dragging) {
      event.preventDefault();
      // create rectangle for zoom
      this.dragging = true;
      this.startDrag = event.offsetX - this.dims.xOffset;
      this.endDrag = this.startDrag;
    }
  }

  @HostListener('mousemove', ['$event'])
  onDrag(event: MouseEvent): void {
    if (!this.dragging) return;
    event.preventDefault();
    // expand rectangle
    this.endDrag = event.offsetX - this.dims.xOffset;
  }

  @HostListener('mouseup', ['$event'])
  onDragEnd(event: MouseEvent): void {
    if (!this.dragging) return;
    event.preventDefault();
    this.dragging = false;
    if (Math.abs(this.startDrag - this.endDrag) > 10) {
      // change date context to match size of rectangle
      const firstTime = this.xScale.invert(this.startDrag);
      const secondTime = this.xScale.invert(this.endDrag);
      if (moment(firstTime).isBefore(secondTime)) {
        this.displayContext.updateDateRange(secondTime, firstTime);
      } else {
        this.displayContext.updateDateRange(firstTime, secondTime);
      }
    }
  }

  get dragX(): number {
    if (this.startDrag <= this.endDrag) {
      return this.startDrag;
    }
    return this.endDrag;
  }

  get dragWidth(): number {
    return Math.abs(this.endDrag - this.startDrag);
  }

  onClick(data): void {
    this.select.emit(data);
  }

  trackBy(index, item): string {
    return item.name;
  }

  onActivate(item) {
    this.deactivateAll();

    const idx = this.activeEntries.findIndex(d => {
      return d.name === item.name && d.value === item.value;
    });
    if (idx > -1) {
      return;
    }
    this.activeEntries = [item];
    this.activate.emit({ value: item, entries: this.activeEntries });
  }

  onDeactivate(item) {
    const idx = this.activeEntries.findIndex(d => {
      return d.name === item.name && d.value === item.value;
    });

    this.activeEntries.splice(idx, 1);
    this.activeEntries = [...this.activeEntries];
    this.deactivate.emit({ value: item, entries: this.activeEntries });
  }

  deactivateAll() {
    this.activeEntries = [...this.activeEntries];
    for (const entry of this.activeEntries) {
      this.deactivate.emit({ value: entry, entries: [] });
    }
    this.activeEntries = [];
  }

  currentPosition(currentTime) {
    if (this.showTime && this.scaleType === 'time') {
      this.currentPos = this.xScale(currentTime);
      this.cd.markForCheck();
    } else {
      this.currentPos = -1;
    }
  }

  axisHideTicks(value) {
    return '';
  }
}
