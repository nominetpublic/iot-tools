/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';
import {
  GraphDisplayConfiguration,
  MapDisplayConfiguration,
  VisualisationComponentType,
} from 'app/core/models/visualisations/visualisation-component';
import { VisualisationDataLayer } from 'app/core/models/visualisations/visualisation-data-layer';
import { VisualisationService } from 'app/core/services/visualisation/visualisation.service';
import { Subscription } from 'rxjs';

import { DisplayContextService } from '../../../core/services/visualisation/display-context.service';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'vis-wrapper',
  templateUrl: './vis-wrapper.component.html',
  styleUrls: ['./vis-wrapper.component.scss']
})
export class VisWrapperComponent implements OnInit, OnDestroy {
  VisualisationComponentType = VisualisationComponentType;
  subscriptions: Subscription[] = [];
  @Input() configuration: MapDisplayConfiguration | GraphDisplayConfiguration;
  @Input() type: VisualisationComponentType;
  @Input() key: string;
  @Input() componentId: string;
  @Input() startDate: Date;
  @Input() endDate: Date;
  @Input() widget = false;
  @Input() layers: VisualisationDataLayer[] = [];
  loading = false;

  constructor(
    protected displayContext: DisplayContextService,
    protected ref: ChangeDetectorRef,
    private visualisationService: VisualisationService,
  ) {
  }

  ngOnInit() {
    if (!this.widget) {
      this.subscriptions.push(this.visualisationService.displayLayers$.subscribe(displayLayers => {
        if (displayLayers != null && this.componentId in displayLayers) {
          this.layers = displayLayers[this.componentId];
          this.loading = this.layers.every(layer => layer.loading === true);
          this.ref.markForCheck();
        }
      }));
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }
}
