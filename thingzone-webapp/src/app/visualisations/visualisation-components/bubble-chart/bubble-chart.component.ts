/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { animate, style, transition, trigger } from '@angular/animations';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  NgZone,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';
import {
  BaseChartComponent,
  calculateViewDimensions,
  ColorHelper,
  getDomain,
  getScale,
  getScaleType,
  ViewDimensions,
} from '@swimlane/ngx-charts';
import { id } from '@swimlane/ngx-charts/release/utils';
import { GraphDisplayConfiguration } from 'app/core/models/visualisations/visualisation-component';
import { ApiService } from 'app/core/services/api/api.service';
import { DisplayContext, DisplayContextService } from 'app/core/services/visualisation/display-context.service';
import { D3Utils } from 'app/core/utils/d3-utils';
import { EventUtils } from 'app/core/utils/event-utils';
import { scaleLinear } from 'd3-scale';
import * as moment from 'moment';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { BubbleGraphData, ChartUtils, GraphData } from '../chart-utils';

@Component({
  selector: 'bubble-chart',
  templateUrl: './bubble-chart.component.html',
  styleUrls: ['./bubble-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('animationState', [
      transition(':leave', [
        style({
          opacity: 1
        }),
        animate(
          500,
          style({
            opacity: 0
          })
        )
      ])
    ])
  ]
})
export class BubbleChartComponent extends BaseChartComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {
  @Input() dataLayers: GraphData[] = [];
  @Input() config: GraphDisplayConfiguration;
  @Input() showTime = true;

  @Input() xScaleMin: any = null;
  @Input() xScaleMax: any = null;
  @Input() showGridLines = true;
  @Input() xAxis = true;
  @Input() yAxis = true;
  @Input() showXAxisLabel = false;
  @Input() showYAxisLabel = false;
  @Input() xAxisLabel: string;
  @Input() yAxisLabel: string;
  @Input() trimXAxisTicks = true;
  @Input() trimYAxisTicks = true;
  @Input() rotateXAxisTicks = true;
  @Input() maxXAxisTickLength = 16;
  @Input() maxYAxisTickLength = 16;
  @Input() xAxisTickFormatting: any = D3Utils.d3TimeFormat;
  @Input() yAxisTickFormatting: any;
  @Input() xAxisTicks: any[];
  @Input() yAxisTicks: any[];
  @Input() roundDomains = false;
  @Input() maxRadius = 15;
  @Input() minRadius = 7;
  @Input() autoScale: boolean;
  @Input() tooltipDisabled = false;

  @Output() activate: EventEmitter<any> = new EventEmitter();
  @Output() deactivate: EventEmitter<any> = new EventEmitter();

  colorScheme = ChartUtils.colourScheme();
  escapeLabel = ChartUtils.escapeLabel;
  subscriptions: Subscription[] = [];
  results: BubbleGraphData[] = [];
  yScaleMin = 0;
  yScaleMax = 1;
  view: [number, number];
  dims: ViewDimensions;
  colors: ColorHelper;
  margin = [10, 20, 10, 20];
  bubblePadding = [0, 0, 0, 0];
  data: any;
  schemeType = 'ordinal';

  transform: string;

  clipPath: string;
  clipPathId: string;

  seriesDomain: any[];
  xDomain: any[];
  yDomain: any[];
  rDomain: number[];

  xScaleType: string;
  yScaleType: string;

  yScale: any;
  xScale: any;
  rScale: any;

  xAxisHeight = 0;
  yAxisWidth = 0;

  activeEntries: any[] = [];
  currentPos = -1;

  dragging = false;
  startDrag = 0;
  endDrag = 0;
  imageServer: string;
  hideAxisTicks = this.axisHideTicks.bind(this);

  constructor(
    protected displayContext: DisplayContextService,
    protected api: ApiService,
    protected chartElement: ElementRef,
    protected zone: NgZone,
    protected cd: ChangeDetectorRef
  ) {
    super(chartElement, zone, cd);
  }

  ngOnInit() {
    this.getResults();
    this.updateDisplaySettings();
    this.imageServer = this.api.getImageServer();

    if (this.showTime) {
      const displayContextSub = this.displayContext.context$.subscribe((context: DisplayContext) => {
        this.currentPosition(context.current);
      });
      this.subscriptions.push(displayContextSub);
    }
  }

  ngAfterViewInit() {
    this.bindPanelResizeEvent();
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('dataLayers' in changes) {
      this.getResults();
    }
    if ('config' in changes) {
      this.updateDisplaySettings();
    }
    this.update();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  update(): void {
    super.update();

    this.dims = calculateViewDimensions({
      width: this.width,
      height: this.height,
      margins: this.margin,
      showXAxis: this.xAxis,
      showYAxis: this.yAxis,
      xAxisHeight: this.xAxisHeight,
      yAxisWidth: this.yAxisWidth,
      showXLabel: this.showXAxisLabel,
      showYLabel: this.showYAxisLabel,
      showLegend: false,
    });

    this.seriesDomain = this.results.map(d => d.name);
    this.rDomain = this.getRDomain();
    this.xDomain = this.getXDomain();
    this.yDomain = this.getYDomain();

    this.transform = `translate(${this.dims.xOffset},${this.margin[0]})`;

    this.colors = new ColorHelper(this.colorScheme, this.schemeType, this.seriesDomain);

    this.data = this.results;

    this.minRadius = Math.max(this.minRadius, 1);
    this.maxRadius = Math.max(this.maxRadius, 1);

    this.rScale = this.getRScale(this.rDomain, [this.minRadius, this.maxRadius]);

    this.bubblePadding = [0, 0, 0, 0];
    this.setScales();

    this.bubblePadding = this.getBubblePadding();
    this.setScales();

    this.clipPathId = 'clip' + id().toString();
    this.clipPath = `url(#${this.clipPathId})`;

    this.cd.markForCheck();
  }

  getResults() {
    let newResults = [];
    for (const layer of this.dataLayers) {
      if (layer.data != null) {
        if (layer.source.configuration != null && layer.source.configuration.eventQuery != null
          && layer.source.configuration.eventQuery.yValue != null && layer.source.configuration.eventQuery.yValue) {
          this.yScaleMin = null;
          this.yScaleMax = null;
        }
        newResults = newResults.concat(layer.data);
      }
    }
    if (newResults.length > 0) {
      this.results = [...newResults];
    }
  }

  updateDisplaySettings() {
    if (this.config != null) {
      if (this.config.xLabel != null && this.config.xLabel.trim() !== '') {
        this.xAxisLabel = this.config.xLabel;
        this.showXAxisLabel = true;
      }
      if (this.config.hideXAxis != null && this.config.hideXAxis) {
        this.xAxisTickFormatting = this.hideAxisTicks;
      }
      if (this.config.hideYAxis != null && this.config.hideYAxis) {
        this.yAxisTickFormatting = this.hideAxisTicks;
      }
      if (this.config.yLabel != null && this.config.yLabel.trim() !== '') {
        this.yAxisLabel = this.config.yLabel;
        this.showYAxisLabel = true;
      }
    }
  }

  @HostListener('mouseleave')
  hideCircles(): void {
    this.deactivateAll();
  }

  onMouseDown(event: MouseEvent): void {
    event.preventDefault();
    if (!this.dragging) {
      // create rectangle for zoom
      this.dragging = true;
      this.startDrag = event.offsetX - this.dims.xOffset;
      this.endDrag = this.startDrag;
    }
  }

  @HostListener('mousemove', ['$event'])
  onDrag(event: MouseEvent): void {
    if (!this.dragging) return;
    event.preventDefault();
    // expand rectangle
    this.endDrag = event.offsetX - this.dims.xOffset;
  }

  @HostListener('window:mouseup', ['$event'])
  onDragEnd(event: MouseEvent): void {
    if (!this.dragging) return;
    event.preventDefault();
    this.dragging = false;
    if (Math.abs(this.startDrag - this.endDrag) > 10) {
      // change date context to match size of rectangle
      const firstTime = this.xScale.invert(this.startDrag);
      const secondTime = this.xScale.invert(this.endDrag);
      if (moment(firstTime).isBefore(secondTime)) {
        this.displayContext.updateDateRange(secondTime, firstTime);
      } else {
        this.displayContext.updateDateRange(firstTime, secondTime);
      }
    }
  }

  get dragX(): number {
    if (this.startDrag <= this.endDrag) {
      return this.startDrag;
    }
    return this.endDrag;
  }

  get dragWidth(): number {
    return Math.abs(this.endDrag - this.startDrag);
  }

  onClick(data, series?): void {
    if (series) {
      data.series = series.name;
    }
    this.select.emit(data);
  }

  getBubblePadding() {
    let yMin = 0;
    let xMin = 0;
    let yMax = this.dims.height;
    let xMax = this.dims.width;

    for (const s of this.data) {
      for (const d of s.series) {
        const r = this.rScale(d.r);
        const cx = this.xScaleType === 'linear' ? this.xScale(Number(d.x)) : this.xScale(d.x);
        const cy = this.yScaleType === 'linear' ? this.yScale(Number(d.y)) : this.yScale(d.y);
        xMin = Math.max(r - cx, xMin);
        yMin = Math.max(r - cy, yMin);
        yMax = Math.max(cy + r, yMax);
        xMax = Math.max(cx + r, xMax);
      }
    }
    xMax = Math.max(xMax - this.dims.width, 0);
    yMax = Math.max(yMax - this.dims.height, 0);

    return [yMin, xMax, yMax, xMin];
  }

  setScales() {
    let width = this.dims.width;
    if (this.xScaleMin === undefined && this.xScaleMax === undefined) {
      width = width - this.bubblePadding[1];
    }
    let height = this.dims.height;
    if (this.yScaleMin === undefined && this.yScaleMax === undefined) {
      height = height - this.bubblePadding[2];
    }
    this.xScale = this.getXScale(this.xDomain, width);
    this.yScale = this.getYScale(this.yDomain, height);
  }

  getYScale(domain, height): any {
    return getScale(domain, [height, this.bubblePadding[0]], this.yScaleType, this.roundDomains);
  }

  getXScale(domain, width): any {
    return getScale(domain, [this.bubblePadding[3], width], this.xScaleType, this.roundDomains);
  }

  getRScale(domain, range): any {
    const scale = scaleLinear()
      .range(range)
      .domain(domain);
    return this.roundDomains ? scale.nice() : scale;
  }

  getXDomain(): any[] {
    const values = [];

    for (const results of this.results) {
      for (const d of results.series) {
        if (!values.includes(d.x)) {
          values.push(d.x);
        }
      }
    }
    this.xScaleType = getScaleType(values);
    return getDomain(values, this.xScaleType, this.autoScale, this.xScaleMin, this.xScaleMax);
  }

  getYDomain(): any[] {
    const values = [];

    for (const result of this.results) {
      for (const d of result.series) {
        if (!values.includes(d.y)) {
          values.push(d.y);
        }
      }
    }
    this.yScaleType = getScaleType(values);
    return getDomain(values, this.yScaleType, this.autoScale, this.yScaleMin, this.yScaleMax);
  }

  getRDomain(): number[] {
    let min = Infinity;
    let max = -Infinity;

    for (const results of this.results) {
      for (const d of results.series) {
        const value = Number(d.r) || 1;
        min = Math.min(min, value);
        max = Math.max(max, value);
      }
    }
    return [min, max];
  }

  updateYAxisWidth({ width }): void {
    this.yAxisWidth = width;
    this.update();
  }

  updateXAxisHeight({ height }): void {
    this.xAxisHeight = height;
    this.update();
  }

  private bindPanelResizeEvent(): void {
    const source = fromEvent(window, 'panelResize');
    const subscription = source.pipe(debounceTime(100)).subscribe(e => {
      this.update();
      if (this.cd) {
        this.cd.markForCheck();
      }
    });
    this.subscriptions.push(subscription);
  }

  onActivate(item): void {
    const idx = this.activeEntries.findIndex(d => {
      return d.name === item.name;
    });
    if (idx > -1) {
      return;
    }

    this.activeEntries = [item, ...this.activeEntries];
    this.activate.emit({ value: item, entries: this.activeEntries });
  }

  onDeactivate(item): void {
    const idx = this.activeEntries.findIndex(d => {
      return d.name === item.name;
    });

    this.activeEntries.splice(idx, 1);
    this.activeEntries = [...this.activeEntries];

    this.deactivate.emit({ value: item, entries: this.activeEntries });
  }

  deactivateAll() {
    this.activeEntries = [...this.activeEntries];
    for (const entry of this.activeEntries) {
      this.deactivate.emit({ value: entry, entries: [] });
    }
    this.activeEntries = [];
  }

  currentPosition(currentTime) {
    if (this.showTime && this.xScaleType === 'time') {
      this.currentPos = this.xScale(currentTime);
      this.cd.markForCheck();
    } else {
      this.currentPos = -1;
    }
  }

  trackBy(index, item): string {
    return item.name;
  }

  getSeverity(radius: number): string {
    const severity = radius / 10;
    return EventUtils.getSeverityName(severity);
  }

  getThumbnailUrl(ref): string {
    return `${this.imageServer}images/${ref}/r150x100`;
  }

  axisHideTicks(value) {
    return '';
  }

}
