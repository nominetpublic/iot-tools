/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatGridListModule } from '@angular/material';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import {
  BarChartModule,
  BubbleChartModule,
  ChartCommonModule,
  GaugeModule,
  LineChartModule,
  PieChartModule,
} from '@swimlane/ngx-charts';

import { MediaGalleryModule } from '../media-gallery/media-gallery.module';
import { BarGraphComponent } from './bar-graph/bar-graph.component';
import { BubbleChartComponent } from './bubble-chart/bubble-chart.component';
import { GaugeComponent } from './gauge/gauge.component';
import { LineGraphComponent } from './line-graph/line-graph.component';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { SparkLineComponent } from './spark-line/spark-line.component';
import { ValueWidgetComponent } from './value-widget/value-widget.component';
import { VisMapComponent } from './vis-map/vis-map.component';
import { VisWrapperComponent } from './vis-wrapper/vis-wrapper.component';

@NgModule({
  imports: [
    CommonModule,
    FontAwesomeModule,
    MatGridListModule,
    LineChartModule,
    ChartCommonModule,
    BubbleChartModule,
    BarChartModule,
    GaugeModule,
    PieChartModule,
    MediaGalleryModule
  ],
  declarations: [
    SparkLineComponent,
    LineGraphComponent,
    VisMapComponent,
    ValueWidgetComponent,
    BubbleChartComponent,
    BarGraphComponent,
    GaugeComponent,
    PieChartComponent,
    VisWrapperComponent,
  ],
  exports: [
    SparkLineComponent,
    LineGraphComponent,
    VisMapComponent,
    ValueWidgetComponent,
    BubbleChartComponent,
    BarGraphComponent,
    GaugeComponent,
    PieChartComponent,
    VisWrapperComponent,
  ]
})
export class VisualisationComponentsModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faSpinner);
  }
}
