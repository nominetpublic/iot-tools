/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';
import { GraphDisplayConfiguration } from 'app/core/models/visualisations/visualisation-component';
import { EventUtils } from 'app/core/utils/event-utils';
import * as moment from 'moment';

import { BarGraphData, ChartUtils, GraphData } from '../chart-utils';

@Component({
  selector: 'bar-graph',
  templateUrl: './bar-graph.component.html',
  styleUrls: ['./bar-graph.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class BarGraphComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {

  @Input() dataLayers: GraphData[] = [];
  @Input() config: GraphDisplayConfiguration;

  colorScheme = ChartUtils.colourScheme();
  escapeLabel = ChartUtils.escapeLabel;
  results: BarGraphData[] = [];
  showXAxisLabel = false;
  showYAxisLabel = false;
  xAxisLabel: string = null;
  yAxisLabel: string = null;
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  xAxisTickFormatting: any;
  yAxisTickFormatting: any;
  hideAxisTicks = this.axisHideTicks.bind(this);
  rangeCalculated: boolean;
  ranges: [{ low: number; high: number; name: string; }];
  size: number;
  customColors = null;

  constructor(protected cd: ChangeDetectorRef) { }

  ngOnInit() {
    window.addEventListener('panelResize', this.resize.bind(this));
    this.updateDisplaySettings();
  }

  ngAfterViewInit() {
    this.resize();
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('config' in changes) {
      this.updateDisplaySettings();
    }
    if ('dataLayers' in changes) {
      this.getResults();
    }
  }

  ngOnDestroy() {
    window.removeEventListener('panelResize', this.resize);
  }

  updateDisplaySettings() {
    if (this.config != null) {
      if (this.config.xLabel != null && this.config.xLabel.trim() !== '') {
        this.xAxisLabel = this.config.xLabel;
        this.showXAxisLabel = true;
      }
      if (this.config.hideXAxis != null && this.config.hideXAxis) {
        this.xAxisTickFormatting = this.hideAxisTicks;
      }
      if (this.config.hideYAxis != null && this.config.hideYAxis) {
        this.yAxisTickFormatting = this.hideAxisTicks;
      }
      if (this.config.yLabel != null && this.config.yLabel.trim() !== '') {
        this.yAxisLabel = this.config.yLabel;
        this.showYAxisLabel = true;
      }
      if (this.config.stops != null && this.config.stops.length > 0) {
        this.ranges = this.config.stops;
        this.calculateRanges(this.results);
      } else if (this.config.size != null) {
        this.size = this.config.size;
      }
      if (this.config.displayType === 'severity') {
        this.customColors = EventUtils.severityColors;
      }
    }
  }

  getResults(): void {
    let newResults = [];
    for (const layer of this.dataLayers) {
      if (layer.data != null) {
        newResults = newResults.concat(layer.data);
      }
    }
    if (newResults.length > 0) {
      this.results = [...newResults];
      this.rangeCalculated = false;
      this.cd.markForCheck();
    }
    if (this.ranges) {
      this.calculateRanges(this.results);
    } else if (this.results.length > 0 && this.size) {
      this.results = this.results.slice(0, this.size);
      this.cd.markForCheck();
    }
  }

  onSelect(event) {
  }

  resize(): void {
    // trigger ngx-charts resize
    window.dispatchEvent(new Event('resize'));
  }

  axisHideTicks(value) {
    return '';
  }

  calculateRanges(results) {
    if (results.length > 0 && !this.rangeCalculated) {
      const rangeHistogram = {};
      let interval;
      let currentTime;
      let currentValue;
      let lastTime;
      let lastValue;
      // set up initial values
      for (const range of this.ranges) {
        rangeHistogram[range.name] = 0;
      }
      // calculate totals for each range
      for (const result of results) {
        currentTime = moment(result.timestamp);
        currentValue = result.value;
        if (lastTime) {
          interval = currentTime.diff(lastTime, 'seconds');
          for (const range of this.ranges) {
            if (range.low == null && range.high == null) {
              continue;
            }
            if ((range.low == null || result.value > range.low)
              && (range.high == null || result.value <= range.high)) {
              rangeHistogram[range.name] = rangeHistogram[range.name] + interval;
            }
          }
        }
        lastTime = currentTime;
        lastValue = currentValue;
      }
      this.results = Object.keys(rangeHistogram).map(name => ({ name: name, value: rangeHistogram[name] }));
      this.rangeCalculated = true;

      this.cd.markForCheck();
    }
  }
}
