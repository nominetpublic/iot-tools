/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, ElementRef, HostListener, Input, OnChanges, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { SecondaryQueryType, StreamType } from 'app/core/models/queries/data-query';
import { QuerySource } from 'app/core/models/visualisations/display-data-source';
import { DisplayResource } from 'app/core/models/visualisations/display-resource';
import { MapDisplayConfiguration } from 'app/core/models/visualisations/visualisation-component';
import { VisualisationDataLayer } from 'app/core/models/visualisations/visualisation-data-layer';
import { ColourService } from 'app/core/services/api/colour.service';
import { ConfigService } from 'app/core/services/config/config.service';
import { GeoService } from 'app/core/services/geo/geo.service';
import { PreferenceService } from 'app/core/services/preference/preference.service';
import { AnnotationService } from 'app/core/services/visualisation/annotation.service';
import { DisplayContextService } from 'app/core/services/visualisation/display-context.service';
import { VisualisationService } from 'app/core/services/visualisation/visualisation.service';
import { WidgetService } from 'app/core/services/visualisation/widget.service';
import * as mapboxgl from 'mapbox-gl';
import * as moment from 'moment';
import { BehaviorSubject, of, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';


@Component({
  selector: 'vis-map',
  templateUrl: './vis-map.component.html',
  styleUrls: ['./vis-map.component.scss']
})
export class VisMapComponent implements OnInit, OnChanges, OnDestroy {

  @Input() mapLayers: VisualisationDataLayer[] = [];
  @Input() defaultLayer = 0;
  @Input() config: MapDisplayConfiguration;
  @Input() key: string;
  @Input() widget: boolean;

  subscriptions: Subscription[] = [];
  hoverId;
  theme;
  annotationKeys: string[];
  mapboxSourceIds: { id: number, layers: string[] }[] = [];
  mapbox: mapboxgl.Map;
  popup: mapboxgl.Popup;
  updateBounds = this.updateBoundingPolygon.bind(this);
  changeZoom = this.updateChangeZoom.bind(this);
  precision = 100000000;
  changedZoom: boolean;
  iconNames: string[] = [];
  legend$ = new BehaviorSubject<object>({});

  @ViewChild('visualisationMap', { static: true }) mapElement: ElementRef;
  satelliteStyle: string;
  lightStyle: string;
  darkStyle: string;

  constructor(
    protected geo: GeoService,
    protected colourService: ColourService,
    protected visualisationService: VisualisationService,
    protected widgetService: WidgetService,
    protected displayContext: DisplayContextService,
    private preference: PreferenceService,
    private annotationService: AnnotationService,
    private configService: ConfigService
  ) {
    this.lightStyle = configService.getConfigValue('lightTheme');
    this.darkStyle = configService.getConfigValue('darkTheme');
    this.satelliteStyle = configService.getConfigValue('satelliteTheme');
  }

  ngOnInit() {
    window.addEventListener('panelResize', this.resizeFunction.bind(this));
    this.visualisationService.setMapCentre(null);

    const centreSub = this.visualisationService.mapCentre$.subscribe((value: [number, number]) => {
      if (value != null && this.mapbox) {
        const mapCentre = this.mapbox.getCenter();
        if (Math.round(mapCentre.lng * this.precision) !== Math.round(value[0] * this.precision)
          || Math.round(mapCentre.lat * this.precision) !== Math.round(value[1] * this.precision)) {
          this.mapFocus(value);
        }
      }
    });
    this.subscriptions.push(centreSub);

    if (this.widget) {
      const mapStateSub = this.widgetService.mapState$.subscribe(value => {
        if (value === true) {
          this.saveMapConfig();
        }
      });
      this.subscriptions.push(mapStateSub);
    }

    this.geo.setApiKey(mapboxgl);

    const centre = (this.config != null && this.config.mapCentre != null && this.isNumeric(this.config.mapCentre[0])) ?
      mapboxgl.LngLat.convert(this.config.mapCentre)
      : mapboxgl.LngLat.convert([-1.261188, 51.751879]);
    const zoom = (this.config != null && this.config.mapZoom != null && this.isNumeric(this.config.mapZoom)) ? this.config.mapZoom : 11;
    const pitch = (this.config != null && this.config.pitch != null && this.isNumeric(this.config.pitch)) ? this.config.pitch : 0;
    const bearing = (this.config != null && this.config.bearing != null && this.isNumeric(this.config.bearing)) ? this.config.bearing : 0;
    this.theme = (this.config != null && this.config.theme != null) ? this.config.theme : 'light';
    this.annotationKeys = (this.config != null && this.config.annotations != null) ? this.config.annotations : [];
    const mapStyle = this.getStyle(this.theme);

    const htmlMapElement = this.mapElement.nativeElement as HTMLElement;
    this.mapbox = new mapboxgl.Map({
      container: htmlMapElement,
      style: mapStyle,
      zoom: zoom,
      center: centre,
      pitch: pitch,
      bearing: bearing,
    });

    this.popup = new mapboxgl.Popup({
      closeButton: false,
      closeOnClick: false,
    });

    this.mapbox.on('load', () => {
      this.mapLoaded(this.mapbox);
    });
  }

  ngOnChanges(changes) {
    const mapLayers = changes.mapLayers;
    if (mapLayers) {
      const current: VisualisationDataLayer[] = mapLayers.currentValue || [];
      const prev: VisualisationDataLayer[] = mapLayers.previousValue || [];

      // Only update layers which have been incremented
      const changedLayers = current.filter((a: VisualisationDataLayer) => {
        const prevItem = prev.find(b => b.id === a.id);
        if (prevItem && prevItem.increment === a.increment) { return false; }
        return true;
      });

      this.setMapLayers(changedLayers);
    }
  }

  ngOnDestroy() {
    if (this.mapbox) {
      this.mapbox.off('moveend', this.updateBounds);
      this.mapbox.off('resize', this.updateBounds);
      this.mapbox.off('zoom', this.changeZoom);
      this.mapbox.off('click', () => this.closePopup());

      this.mapbox.remove();
    }
    window.removeEventListener('panelResize', this.resizeFunction);
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  @HostListener('window:resize')
  resizeFunction(): void {
    if (this.mapbox) {
      this.mapbox.resize();
    }
  }

  updateChangeZoom(): void {
    this.changedZoom = true;
  }

  mapLoaded(mapbox: mapboxgl.Map): void {
    mapbox.on('resize', this.updateBounds);
    mapbox.on('moveend', this.updateBounds);
    mapbox.on('zoom', this.changeZoom);
    mapbox.on('click', () => this.closePopup());

    // Important to run incase layout has changed during load
    mapbox.resize();

    this.setAnnotationLayer();
    this.setMapLayers();
    this.set3dLayer();
  }

  closePopup(): void {
    if (this.popup.isOpen) this.popup.remove();
  }

  getStyle(theme): string {
    if (theme === 'light') {
      return this.lightStyle;
    } else if (theme === 'dark') {
      return this.darkStyle;
    } else if (theme === 'satellite') {
      return this.satelliteStyle;
    }
    return this.lightStyle;
  }

  set3dLayer(): void {
    const mapbox = this.mapbox;
    if (!mapbox) { return; }

    const fill_color = (this.theme === 'light') ? '#ccc' : '#7f887b';

    mapbox.addLayer({
      'id': '3d-buildings',
      'source': 'composite',
      'source-layer': 'building',
      'filter': ['==', 'extrude', 'true'],
      'type': 'fill-extrusion',
      'minzoom': 15,
      'paint': {
        'fill-extrusion-color': fill_color,
        // use an 'interpolate' expression to add a smooth transition effect to the
        // buildings as the user zooms in
        'fill-extrusion-height': [
          'interpolate', ['linear'], ['zoom'],
          15, 0,
          15.05, ['get', 'height']
        ],
        'fill-extrusion-base': [
          'interpolate', ['linear'], ['zoom'],
          15, 0,
          15.05, ['get', 'min_height']
        ],
        'fill-extrusion-opacity': .8
      }
    });
  }

  updateBoundingPolygon(): void {
    for (const layer of this.mapLayers) {
      if (this.mapFocusLock(layer)) { return; }
    }

    const bounds = this.geo.getMapBoundingBox(this.mapbox, 1);
    this.displayContext.updateBoundingPolygon(bounds);
  }

  setMapLayers(changedLayers?: VisualisationDataLayer[]): void {
    if (!changedLayers) { changedLayers = this.mapLayers; }
    if (!this.mapbox) { return; }

    // Always check mapboxSourceIds against full mapLayer array
    this.mapboxSourceIds = this.mapboxSourceIds.filter(({ id, layers }) => {
      if (!this.mapLayers.find(a => a.id === id && a.data)) {
        const sourceId = `source${id}`;
        layers.forEach(layerId => {
          if (this.mapbox.getLayer(layerId)) {
            this.mapbox.removeLayer(layerId);
          }
        });
        this.mapbox.removeSource(sourceId);
        return false;
      }
      return true;
    });

    changedLayers.forEach((layer: VisualisationDataLayer) => {
      if (!layer.data || !layer.data.geojson) {
        this.filteredLayer(layer);
        return false;
      }

      const sourceId = `source${layer.id}`;
      if (this.mapboxSourceIds.find(a => a.id === layer.id)) {
        this.updateSource(layer, sourceId);
      } else {
        this.createSource(layer, sourceId);
      }

      if (this.mapFocusLock(layer)) {
        if (this.mapbox.dragPan.isEnabled()) {
          this.mapbox.dragPan.disable();
        }
      }
    });
  }

  updateSource(layer: VisualisationDataLayer, sourceId: string): void {
    const source = this.mapbox.getSource(sourceId) as mapboxgl.GeoJSONSource;

    if (source) {
      if (this.mapFocusLock(layer)) {
        const centerCoordinates = [this.mapbox.getCenter().lng, this.mapbox.getCenter().lat];

        this.animateDataSource(centerCoordinates, layer, source, 30);
      } else {
        source.setData(layer.data.geojson);
      }
      this.updateFocus(layer);
    }
  }

  filteredLayer(layer: VisualisationDataLayer): void {
    if (layer.id in this.legend$.getValue()) {
      const legend = this.legend$.getValue();
      delete legend[layer.id];
      this.legend$.next(legend);
    }
  }

  animateDataSource(centerCoordinates, layer, source, steps) {
    if (this.changedZoom) {
      this.changedZoom = false;
      source.setData(layer.data.geojson);
      return;
    }
    let currentCoordinates = [];
    const featureGeometry = layer.data.geojson.features[0].geometry;
    if (featureGeometry.type === 'Point') {
      currentCoordinates = featureGeometry.coordinates;
    } else if (featureGeometry.type === 'LineString') {
      currentCoordinates = featureGeometry.coordinates[0];
    }

    const currGeojson = JSON.parse(JSON.stringify(layer.data.geojson));
    const dLon = currentCoordinates[0] - centerCoordinates[0];
    const dLat = currentCoordinates[1] - centerCoordinates[1];
    const stepLon = dLon / steps;
    const stepLat = dLat / steps;

    let i = 1;
    const interpolateSource = () => {
      if (featureGeometry.type === 'Point') {
        currGeojson.features[0].geometry.coordinates = [centerCoordinates[0] + (stepLon * i), centerCoordinates[1] + (stepLat * i)];
      } else if (featureGeometry.type === 'LineString') {
        currGeojson.features[0].geometry.coordinates[0] = [centerCoordinates[0] - (stepLon * i), centerCoordinates[1] - (stepLat * i)];
      }
      source.setData(currGeojson);
      i++;
      if (i <= steps) { requestAnimationFrame(interpolateSource); }
    };

    requestAnimationFrame(interpolateSource);
  }

  mapFocusLock(layer: VisualisationDataLayer): boolean {
    if (layer.source.configuration != null
      && layer.source.configuration.mapConfiguration != null
      && layer.source.configuration.mapConfiguration.focusMap) {
      return layer.source.configuration.mapConfiguration.focusMap;
    }
    return false;
  }

  updateFocus(layer: VisualisationDataLayer) {
    if (this.mapFocusLock(layer)) {
      const featureGeometry = layer.data.geojson.features[0].geometry;
      if (featureGeometry.type === 'Point') {
        this.mapFocus(featureGeometry.coordinates);
      } else if (featureGeometry.type === 'LineString') {
        this.mapFocus(featureGeometry.coordinates[0]);
      }
    }
  }

  createSource(layer: VisualisationDataLayer, sourceId: string): void {
    const layerId = layer.id;
    const geojson = layer.data.geojson;

    if (geojson.features.length === 0) { return; }

    this.mapbox.addSource(sourceId, {
      'type': 'geojson',
      'data': geojson
    });

    const layerArray = [];
    const config = layer.source.configuration.mapConfiguration != null ? layer.source.configuration.mapConfiguration : {};
    if (config.heatMap != null && config.heatMap) {
      layerArray.push(this.createHeatMapLayer(layerId, sourceId, layer.source));
    } else {
      layerArray.push(this.createPointLayer(layerId, sourceId, layer.source));
      layerArray.push(this.createLineLayer(layerId, sourceId, layer.source));
      layerArray.push(this.createPolygonLayer(layerId, sourceId, layer.source));
    }

    this.mapboxSourceIds.push({
      id: layerId,
      layers: layerArray
    });
  }

  createLineLayer(id, sourceId, query: QuerySource): string {
    const layerId = `lineLayer${id}`;
    const defaultPaint = {
      'line-color': '#0097a8',
      'line-width': 5,
    };

    if (query.configuration.mapConfiguration != null) {
      const config = query.configuration.mapConfiguration;
      if (config.colour != null && config.colour !== '') { defaultPaint['line-color'] = config.colour; }
      if (config.radius != null) { defaultPaint['line-width'] = Number(config.radius); }
      if (config.opacity != null) { defaultPaint['line-opacity'] = (config.opacity / 100); }
    }
    this.secondaryColourConfig(query, defaultPaint);

    this.mapbox.addLayer({
      'id': layerId,
      'type': 'line',
      'source': sourceId,
      'paint': defaultPaint,
      'layout': {
        'line-cap': 'round'
      },
      filter: ['==', ['geometry-type'], 'LineString']
    });

    this.mapbox.on('mousemove', layerId, e => this.setHover(e));
    this.mapbox.on('mouseleave', layerId, () => this.removeHover());
    return layerId;
  }

  createPolygonLayer(id, sourceId, query: QuerySource): string {
    const layerId = `polygonLayer${id}`;
    const defaultPaint = {
      'fill-color': '#0097a8',
      'fill-opacity': 0.4,
    };

    if (query.configuration.mapConfiguration != null) {
      const config = query.configuration.mapConfiguration;
      if (config.colour != null && config.colour !== '') { defaultPaint['fill-color'] = config.colour; }
      if (config.outlineColour != null && config.outlineColour !== '') { defaultPaint['fill-outline-color'] = config.outlineColour; }
      if (config.opacity != null) { defaultPaint['fill-opacity'] = (config.opacity / 100); }
    }
    this.secondaryColourConfig(query, defaultPaint);

    this.mapbox.addLayer({
      'id': layerId,
      'type': 'fill',
      'source': sourceId,
      'paint': defaultPaint,
      filter: ['==', ['geometry-type'], 'Polygon']
    });

    this.mapbox.on('mousemove', layerId, e => this.setHover(e));
    this.mapbox.on('mouseleave', layerId, () => this.removeHover());
    return layerId;
  }

  createPointLayer(id, sourceId, query: QuerySource): string {
    const layerId = `pointLayer${id}`;
    const config = query.configuration.mapConfiguration != null ? query.configuration.mapConfiguration : {};

    if (config.icon) {
      const iconName = 'icon.' + config.icon;
      if (this.mapbox.hasImage(iconName) || this.iconNames.includes(iconName)) {
        this.createIconLayer(layerId, sourceId, iconName, query);
      } else {
        this.addIcon(iconName).subscribe(() => {
          this.createIconLayer(layerId, sourceId, iconName, query);
        });
      }
    } else {
      const defaultPaint = {
        'circle-color': '#0097a8',
        'circle-radius': 5,
        'circle-stroke-width': 1,
        'circle-stroke-color': 'rgb(255,255,255)'
      };
      if (config.colour != null && config.colour !== '') { defaultPaint['circle-color'] = config.colour; }
      if (config.radius != null) { defaultPaint['circle-radius'] = Number(config.radius); }
      if (config.outlineWidth != null) { defaultPaint['circle-stroke-width'] = Number(config.outlineWidth); }
      if (config.outlineColour != null && config.outlineColour !== '') { defaultPaint['circle-stroke-color'] = config.outlineColour; }
      if (config.opacity != null) { defaultPaint['circle-opacity'] = (config.opacity / 100); }

      this.secondaryColourConfig(query, defaultPaint);

      this.mapbox.addLayer({
        'id': layerId,
        'type': 'circle',
        'source': sourceId,
        'paint': defaultPaint,
        filter: ['==', ['geometry-type'], 'Point']
      });
    }
    this.mapbox.on('mousemove', layerId, e => this.setHover(e));
    this.mapbox.on('mouseleave', layerId, () => this.removeHover());
    return layerId;
  }

  secondaryColourConfig(querySource: QuerySource, paint: object): void {
    if (querySource.secondaryQueries != null && querySource.secondaryQueries.length > 0) {
      const colourQuery = querySource.secondaryQueries.find(query => query.secondaryType === SecondaryQueryType.COLOUR);
      if (colourQuery != null && colourQuery.configuration != null && colourQuery.configuration.mapColourConfiguration != null) {
        const mapConfig = colourQuery.configuration.mapColourConfiguration;
        const interpolationType = mapConfig.interval ? ['linear'] : ['exponential', 1.75];
        const getProperty = ['case',
          ['==', 'uk.nominet.iot.model.timeseries.MetricInt',
            ['get', 'klass', ['get', colourQuery.streamName, ['get', 'combinedPoints', ['get', 'point']]]]],
          ['get', 'value_int', ['get', colourQuery.streamName, ['get', 'combinedPoints', ['get', 'point']]]],
          ['get', 'value_double', ['get', colourQuery.streamName, ['get', 'combinedPoints', ['get', 'point']]]],
        ];
        const interpolate: any[] = [
          'interpolate', interpolationType, getProperty,
        ];
        mapConfig.stops.sort((a, b) => a.number - b.number).map(item => {
          interpolate.push(Number(item.number));
          interpolate.push(item.colour);
        });

        if ('circle-color' in paint) {
          paint['circle-color'] = interpolate;
        } else if ('line-color' in paint) {
          paint['line-color'] = interpolate;
        } else if ('fill-color' in paint) {
          paint['fill-color'] = interpolate;
        }

        if (mapConfig.legend != null && mapConfig.legend) {
          const currentLegend = this.legend$.getValue();
          if (!(querySource.id in currentLegend)) {
            currentLegend[querySource.id] = {
              stops: mapConfig.stops,
              title: mapConfig.legendTitle,
            };
            this.legend$.next(currentLegend);
          }
        }
      }
    }
  }

  createIconLayer(layerId: string, sourceId: string, iconName: string, query: QuerySource) {
    const config = query.configuration.mapConfiguration != null ? query.configuration.mapConfiguration : {};
    const symbolLayout = {
      'icon-image': iconName,
      'icon-size': { 'stops': [[12, 0.6], [18, 1.25]] },
      'icon-anchor': 'center',
      'icon-allow-overlap': true
    } as mapboxgl.SymbolLayout;

    const defaultPaint = {};
    if (config.opacity != null) { defaultPaint['icon-opacity'] = (config.opacity / 100); }

    this.mapbox.addLayer({
      'id': layerId,
      'type': 'symbol',
      'source': sourceId,
      'layout': symbolLayout,
      'paint': defaultPaint,
      filter: ['==', ['geometry-type'], 'Point']
    });
  }

  createHeatMapLayer(id, sourceId, query: QuerySource): string {
    const layerId = `heatMapLayer${id}`;
    const defaultPaint = {
      'heatmap-radius': 5,
      'heatmap-color': [
        'interpolate',
        ['linear'],
        ['heatmap-density'],
        0, 'rgba(255, 255, 255, 0)',
        1, '#0097a8'
      ],
      'heatmap-intensity': [
        'interpolate',
        ['linear'],
        ['zoom'],
        0, 2,
        9, 4
      ],
    } as mapboxgl.HeatmapPaint;

    if (query.configuration.mapConfiguration != null) {
      const config = query.configuration.mapConfiguration;
      if (config.colour != null && config.colour !== '') {
        const outlineColour = (config.outlineColour != null && config.outlineColour !== '') ?
          this.colourService.hexToRgba(config.outlineColour, 60) : config.colour;
        defaultPaint['heatmap-color'] = [
          'interpolate',
          ['linear'],
          ['heatmap-density'],
          0, 'rgba(255, 255, 255, 0)',
          0.2, outlineColour,
          1, config.colour
        ];
      }
      if (config.radius != null) { defaultPaint['heatmap-radius'] = Number(config.radius); }
      if (config.opacity != null) { defaultPaint['heatmap-opacity'] = (config.opacity / 100); }
    }

    this.mapbox.addLayer({
      'id': layerId,
      'type': 'heatmap',
      'source': sourceId,
      'paint': defaultPaint,
    });

    this.mapbox.on('mousemove', layerId, e => this.setHover(e));
    this.mapbox.on('mouseleave', layerId, () => this.removeHover());
    return layerId;
  }

  addIcon(iconName, imageName?) {
    if (imageName == null) {
      imageName = iconName;
    }
    if (!this.iconNames.includes(iconName)) {
      this.iconNames.push(iconName);
      return this.preference.getImageResource(iconName).pipe(
        map((image) => {
          if (!this.mapbox.hasImage(imageName)) {
            this.mapbox.addImage(imageName, image);
          }
        }));
    } else {
      return of(null);
    }
  }

  setHover(event): void {
    if ('features' in event && event.features.length > 0) {
      this.mapbox.getCanvas().style.cursor = 'pointer';
      let html = '';
      const feature = event.features[0];

      if ('point' in feature.properties) {
        const point = JSON.parse(feature.properties.point);
        const layerId = feature.source.startsWith('source') ? feature.source.substring(6) : feature.source;
        const currentLayer: VisualisationDataLayer = this.mapLayers.find(layer => layer.id === +layerId);
        if (currentLayer != null) {
          const source: QuerySource = currentLayer.source;
          if (source.configuration.mapConfiguration != null && source.configuration.mapConfiguration.timeHover) {
            html += '<div>' + moment(point.timestamp).format('MMM D, YYYY, H:mm:ss') + '</div>';
          }
          if (source.configuration.mapConfiguration != null && source.configuration.mapConfiguration.nameHover) {
            html += '<div>' + point.name + '</div>';
          }
          if ('secondaryQueries' in source && source.secondaryQueries.length > 0) {
            const secondaryQueries = source.secondaryQueries;
            secondaryQueries.filter(query => query.secondaryType === SecondaryQueryType.HOVER)
              .forEach(query => {
                const label = query.configuration.mapHoverConfiguration.label;
                if (query.streamName in point.combinedPoints) {
                  const queryResults = point.combinedPoints[query.streamName];
                  const value = this.streamValue(queryResults);
                  html += '<div>' + label + ': ' + value + '</div>';
                }
              });
          }
        }
      } else if ('latest' in feature.properties) {
        const device = JSON.parse(feature.properties.device);
        const name = ('name' in device) ? device.name : device.key;
        html += '<div>' + name + '</div>';
        const latest = JSON.parse(feature.properties.latest);
        let streamName = '';
        let streamData = '';
        if (latest.length > 0) {
          latest.forEach(stream => {
            streamName = stream.name;
            if ('latestPoint' in stream) {
              const point = stream.latestPoint;
              const value = this.streamValue(point);
              if (point.klass === StreamType.IMAGE) {
                const imageUrl = this.visualisationService.getThumbnailUrl(value);
                streamData += '<div> <img src="' + imageUrl + '" /></div>';
              } else {
                streamData += '<div>' + stream.key + ': ' + value + '</div>';
              }
            }
          });
          html += '<div>Latest: ' + streamName + '</div>';
          html += streamData;
        }

      } else if ('device' in feature.properties) {
        const device = JSON.parse(feature.properties.device);
        const name = ('name' in device) ? device.name : device.key;
        html += '<div>' + name + '</div>';
      }

      if (html !== '') {
        this.popup.setLngLat(event.lngLat)
          .setHTML(html)
          .addTo(this.mapbox);
      }
    }
  }

  streamValue(result: object) {
    switch (result['klass']) {
      case StreamType.DOUBLE:
        const number = result['value_double'];
        return new Intl.NumberFormat().format(number);
      case StreamType.INTEGER:
        return result['value_int'];
      case StreamType.STRING:
        return result['value_text'];
      case StreamType.IMAGE:
        return result['imageRef'];
    }
  }

  removeHover(): void {
    this.mapbox.getCanvas().style.cursor = '';
    this.popup.remove();
  }

  mapFocus(coordinates: [number, number]): void {
    if (!this.mapbox) { return; }

    this.mapbox.panTo([coordinates[0], coordinates[1]]);
  }

  isNumeric(val: any): val is number | string {
    return !Array.isArray(val) && (val - parseFloat(val) + 1) >= 0;
  }

  saveMapConfig() {
    if (this.key != null && this.widget && this.mapbox != null) {
      const resource: DisplayResource = this.widgetService.getWidget(this.key);
      if (resource == null) {
        return;
      }
      const mapConfig = resource.content.configuration as MapDisplayConfiguration;
      const center: mapboxgl.LngLat = this.mapbox.getCenter();

      mapConfig.longitude = center.lng;
      mapConfig.latitude = center.lat;
      mapConfig.mapCentre = [center.lng, center.lat];
      mapConfig.pitch = Math.round(this.mapbox.getPitch() * 100) / 100;
      mapConfig.bearing = Math.round(this.mapbox.getBearing() * 100) / 100;
      mapConfig.mapZoom = Math.round(this.mapbox.getZoom() * 100) / 100;
      const content = new Blob([JSON.stringify(resource.content)], { type: 'application/json' });
      const key = (resource.key) ? resource.key : null;
      this.preference.upsertResource('widget', content, key, resource.type, resource.name).subscribe((res) => {
        this.widgetService.queryWidgets(resource.type);
      });
    }
  }

  setAnnotationLayer(): void {
    this.annotationKeys.forEach(key => {
      const annotationResourceSubscription = this.annotationService.getAnnotations(key)
        .subscribe(featureCollection => {
          this.mapbox.addSource(key,
            {
              'type': 'geojson',
              'data': {
                'type': 'FeatureCollection',
                'features': featureCollection.features
              }
            });
          this.mapbox.addLayer({
            'id': `${key}_polygon`,
            'type': 'fill',
            'source': key,
            'paint': {
              'fill-color': featureCollection['colour'],
              'fill-opacity': 0.4,
            },
            'filter': ['==', '$type', 'Polygon'],
          });
          this.mapbox.addLayer({
            'id': `${key}_point`,
            'type': 'circle',
            'source': key,
            'paint': {
              'circle-radius': 6,
              'circle-color': featureCollection['colour']
            },
            'filter': ['==', '$type', 'Point']
          });
          this.mapbox.addLayer({
            'id': `${key}_line`,
            'type': 'line',
            'source': key,
            'layout': {
              'line-cap': 'round',
              'line-join': 'round'
            },
            'paint': {
              'line-width': 4,
              'line-color': featureCollection['colour']
            },
            'filter': ['==', '$type', 'LineString']
          });
          this.mapbox.addLayer({
            'id': `${key}_symbols`,
            type: 'symbol',
            source: key,
            minzoom: 12,
            layout: {
              'text-field': '{name}',
              'text-size': 18,
              'text-offset': [1, -1],
              'symbol-z-order': 'viewport-y',
              'text-allow-overlap': true,
            },
            paint: { 'text-color': this.theme === 'light' ? 'black' : 'white' },
          });
        }, () => {
          const visualisation = this.visualisationService.getVisualisation(this.key);
          visualisation.content['components']
            .filter(c => c.configuration.annotations)
            .forEach(c => c.configuration.annotations = c.configuration.annotations.filter(a => a !== key));
          this.visualisationService.upsertVisualisation(visualisation, visualisation.type);
        });
      this.mapbox.on('click', e => {
        const annotationLayers = this.mapbox.queryRenderedFeatures(e.point).filter(f => this.annotationKeys.indexOf(f.source) > -1);
        if (annotationLayers.length > 0) {
          this.annotationClicked(e.lngLat, annotationLayers);
        }
      });
      this.subscriptions.push(annotationResourceSubscription);
    });
  }

  annotationClicked(clicked: mapboxgl.LngLat, features: GeoJSON.Feature[]): void {
    const html = features
      .map(f =>
        `<p>${f.properties['name']}</p><p>${f.properties['description']}</p>`)
      .reduce((unique, item) => unique.includes(item) ? unique : [...unique, item], [])
      .join('<br>');

    this.popup.setLngLat(clicked)
      .setHTML(html)
      .addTo(this.mapbox);
  }

}
