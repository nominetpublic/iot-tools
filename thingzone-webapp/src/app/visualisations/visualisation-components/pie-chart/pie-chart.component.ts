/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { GraphDisplayConfiguration } from 'app/core/models/visualisations/visualisation-component';
import { EventUtils } from 'app/core/utils/event-utils';
import * as moment from 'moment';

import { BarGraphData, ChartUtils, GraphData } from '../chart-utils';

@Component({
  selector: 'pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})
export class PieChartComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {

  @Input() dataLayers: GraphData[] = [];
  @Input() config: GraphDisplayConfiguration;

  colorScheme = ChartUtils.colourScheme();
  escapeLabel = ChartUtils.escapeLabel;
  results: BarGraphData[] = [];
  rangeCalculated = false;
  units: string = null;
  showLabels = true;
  doughnut = false;
  trimLabels = false;
  ranges: [{ low: number; high: number; name: string; }];
  percent = false;
  customColors = null;

  constructor(protected cd: ChangeDetectorRef) { }

  ngOnInit() {
    window.addEventListener('panelResize', this.resize.bind(this));
    this.updateDisplaySettings();
  }

  ngAfterViewInit() {
    this.resize();
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('config' in changes) {
      this.updateDisplaySettings();
    }
    if ('dataLayers' in changes) {
      this.getResults();
    }
  }

  ngOnDestroy() {
    window.removeEventListener('panelResize', this.resize);
  }

  resize(): void {
    // trigger ngx-charts resize
    window.dispatchEvent(new Event('resize'));
  }

  updateDisplaySettings() {
    this.doughnut = false;
    this.showLabels = true;
    if (this.config != null) {
      if (this.config.hideXAxis != null) {
        if (this.config.hideXAxis === false) {
          this.showLabels = true;
          if (this.config.xLabel != null) {
            this.units = this.config.xLabel;
          }
        } else if (this.config.hideXAxis === true) {
          this.showLabels = false;
        }
      }
      if (this.config.hideYAxis != null) {
        if (this.config.hideYAxis === true) {
          this.doughnut = true;
        } else if (this.config.hideYAxis === false) {
          this.doughnut = false;
        }
      }
      if (this.config.formatType != null) {
        if (this.config.formatType === 'percent') {
          this.percent = true;
          this.units = '%';
        }
      }
      if (this.config.stops != null && this.config.stops.length > 0) {
        this.ranges = this.config.stops;
        this.calculateRanges(this.results);
      }
      if (this.config.displayType === 'severity') {
        this.customColors = EventUtils.severityColors;
      }
    }
  }

  formatValue(value) {
    if (this.units != null) {
      value = value + this.units;
    }
    return value;
  }

  getResults(): void {
    let newResults = [];
    for (const layer of this.dataLayers) {
      if (layer.data != null) {
        newResults = newResults.concat(layer.data);
      }
    }
    if (newResults.length > 0) {
      this.results = [...newResults];
      this.rangeCalculated = false;
      if (this.percent) {
        this.calculatePercent(this.results);
      }
      this.cd.markForCheck();
    }
    if (this.ranges) {
      this.calculateRanges(this.results);
    }
  }

  calculatePercent(results) {
    if (results.length > 0) {
      let total = 0;
      for (const res of results) {
        total = total + res.value;
      }
      for (const result of results) {
        const percent = Math.floor((result.value / total) * 100);
        result.value = percent;
      }
    }
  }

  calculateRanges(results) {
    if (results.length > 0 && !this.rangeCalculated) {
      const rangeHistogram = {};
      let interval;
      let currentTime;
      let currentValue;
      let lastTime;
      let lastValue;
      // set up initial values
      for (const range of this.ranges) {
        rangeHistogram[range.name] = 0;
      }
      // calculate totals for each range
      for (const result of results) {
        currentTime = moment(result.timestamp);
        currentValue = result.value;
        if (lastTime) {
          interval = currentTime.diff(lastTime, 'seconds');
          for (const range of this.ranges) {
            if (range.low == null && range.high == null) {
              continue;
            }
            if ((range.low == null || result.value > range.low)
              && (range.high == null || result.value <= range.high)) {
              rangeHistogram[range.name] = rangeHistogram[range.name] + interval;
            }
          }
        }
        lastTime = currentTime;
        lastValue = currentValue;
      }
      this.results = Object.keys(rangeHistogram).map(name => ({ name: name, value: rangeHistogram[name] }));
      this.rangeCalculated = true;

      if (this.percent) {
        this.calculatePercent(this.results);
      }

      this.cd.markForCheck();
    }
  }
}
