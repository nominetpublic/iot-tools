/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CdkDragEnter, moveItemInArray } from '@angular/cdk/drag-drop';
import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DisplayResource } from 'app/core/models/visualisations/display-resource';
import { WidgetService } from 'app/core/services/visualisation/widget.service';
import { ConfirmDeleteDialogComponent } from 'app/ui/form-components/confirm-delete-dialog/confirm-delete-dialog.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'widget-gallery',
  templateUrl: './widget-gallery.component.html',
  styleUrls: ['./widget-gallery.component.scss']
})
export class WidgetGalleryComponent implements OnInit, AfterViewInit, OnDestroy {
  subscriptions: Subscription[] = [];

  widgetType: string = null;
  widgetList: DisplayResource[] = [];
  cols = 3;
  galleryElement: HTMLElement;

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    private element: ElementRef,
    private widgetService: WidgetService,
    public dialog: MatDialog,
  ) {
    this.galleryElement = element.nativeElement;
  }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.widgetType = data['widgetType'];
    });

    const widgetSub = this.widgetService.widgets$.subscribe(widgets => {
      if (widgets == null) {
        this.widgetService.queryWidgets(this.widgetType);
      } else if (this.widgetService.getCurrentWidgetType() !== this.widgetType) {
        this.widgetService.queryWidgets(this.widgetType);
      } else {
        this.widgetList = widgets;
      }
    });
    this.subscriptions.push(widgetSub);
  }

  ngAfterViewInit() {
    this.cols = this.resizeCols(this.galleryElement.offsetWidth);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  newWidget(): void {
    this.router.navigate(['./new'], { relativeTo: this.route });
  }

  editWidget(key: string): void {
    this.router.navigate(['./edit/' + key], { relativeTo: this.route });
  }

  deleteWidget(key: string): void {
    const dialogRef = this.dialog.open(ConfirmDeleteDialogComponent, {
      data: { message: 'Would you like to delete this widget?', confirmName: false }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        const widgets = this.widgetService.getWidgets();
        this.widgetService.deleteWidget(key).subscribe(() => {
          const filtered = widgets.filter((res) => !(res.key === key && res.resourceName === 'widget'));
          this.widgetService.setWidgets(filtered, this.widgetType);
        });
      }
    });
  }

  entered(event: CdkDragEnter): void {
    moveItemInArray(this.widgetList, event.item.data, event.container.data);
    this.widgetList = this.widgetService.reorderWidgets(this.widgetList);
    this.widgetService.setWidgets(this.widgetList, this.widgetType);
  }

  resizeCols(width): number {
    return Math.floor(width / 524);
  }

  onResize() {
    this.cols = this.resizeCols(this.galleryElement.offsetWidth);
  }
}


