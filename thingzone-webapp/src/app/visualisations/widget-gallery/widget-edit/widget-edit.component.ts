/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { SelectOption } from 'app/core/models/common/select-option';
import { DataQueryType } from 'app/core/models/queries/data-query';
import { QuerySource } from 'app/core/models/visualisations/display-data-source';
import { DisplayResource } from 'app/core/models/visualisations/display-resource';
import {
  GraphDisplayConfiguration,
  MapDisplayConfiguration,
  VisualisationComponentType,
} from 'app/core/models/visualisations/visualisation-component';
import { WidgetContent } from 'app/core/models/visualisations/widget-content';
import { SearchService } from 'app/core/services/search/search.service';
import { VisualisationService } from 'app/core/services/visualisation/visualisation.service';
import { WidgetService } from 'app/core/services/visualisation/widget.service';
import { ConfirmDeleteDialogComponent } from 'app/ui/form-components/confirm-delete-dialog/confirm-delete-dialog.component';
import { BehaviorSubject, combineLatest, Subscription } from 'rxjs';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'widget-edit',
  templateUrl: './widget-edit.component.html',
  styleUrls: ['./widget-edit.component.scss']
})
export class WidgetEditComponent implements OnInit, OnDestroy {
  VisualisationComponentType = VisualisationComponentType;
  subscriptions: Subscription[] = [];
  new = false;

  widgetKey: string = null;
  widgetType: string = null;
  widgetList: DisplayResource[] = [];
  initialWidget: DisplayResource = null;
  widget: DisplayResource = null;
  widgetContent: WidgetContent = null;

  errors = false;
  dataSourceError = false;
  displayConfigError = false;
  typeOptions: SelectOption[] = this.visualisationService.typeOptions;
  queryTypes: DataQueryType[] = [];

  nameControl = new FormControl('');
  widgetForm = new FormGroup({
    interval: new FormControl(),
    timeDelta: new FormControl(),
  });
  disableQuerySelect: boolean;
  private allowedQueryTypes: BehaviorSubject<DataQueryType[]> = new BehaviorSubject([]);
  allowedQueryTypes$ = this.allowedQueryTypes.asObservable();

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    protected widgetService: WidgetService,
    protected visualisationService: VisualisationService,
    private search: SearchService,
    protected ref: ChangeDetectorRef,
    public dialog: MatDialog,
  ) {
  }

  ngOnInit() {
    const routingSub = combineLatest([this.route.data, this.route.params]).subscribe(([data, params]) => {
      this.widgetType = data['widgetType'];
      this.widgetService.queryWidgets(this.widgetType);

      if ('id' in params) {
        this.widgetKey = params.id;
      } else {
        this.new = true;
        this.initWidget(this.widgetService.getEmptyWidget(this.widgetType));
      }
    });
    this.subscriptions.push(routingSub);

    const widgetSub = this.widgetService.widgets$.subscribe((widgets) => {
      if (widgets != null) {
        this.widgetList = widgets;
        this.setSource(this.widgetKey);
      }
    });
    this.subscriptions.push(widgetSub);

    if (this.search.getDeviceSavedSearches() == null) {
      this.search.queryDeviceSavedSearches();
    }

    const contentSub = this.widgetForm.valueChanges.subscribe(formValues => {
      this.widget.content = { ...this.widget.content, ...formValues };
      this.widgetContent = this.widget.content as WidgetContent;
    });
    this.subscriptions.push(contentSub);

    const nameSub = this.nameControl.valueChanges.subscribe(name => {
      this.widget.name = name;
    });
    this.subscriptions.push(nameSub);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  setSource(key: string): void {
    if (key != null) {
      const checkWidget = this.widgetService.getWidget(key);
      if (checkWidget != null) {
        this.initWidget(checkWidget);
      }
    }
  }

  initWidget(widget: DisplayResource): void {
    this.widget = widget;
    this.initialWidget = this.widgetService.cloneWidget(widget);
    this.widgetContent = widget.content as WidgetContent;
    this.widgetForm.patchValue(this.widgetContent, { onlySelf: true, emitEvent: false });
    this.nameControl.setValue(widget.name);
    this.ref.markForCheck();
  }

  deleteWidget(key: string): void {
    const dialogRef = this.dialog.open(ConfirmDeleteDialogComponent, {
      data: { message: 'Would you like to delete this widget?', confirmName: false }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        const widgets = this.widgetService.getWidgets();
        this.widgetService.deleteWidget(key).subscribe(() => {
          const filtered = widgets.filter((resource) => !(resource.key === key && resource.resourceName === 'widget'));
          this.widgetService.setWidgets(filtered, this.widgetType);
        });
        this.closeEdit();
      }
    });
  }

  submit(): void {
    const resource = this.widget;
    this.widgetService.upsertWidget(resource, this.widgetType).subscribe((res) => {
      this.widgetService.queryWidgets(this.widgetType);

      this.widgetKey = res.resource.key;
      if (this.new) {
        this.router.navigate(['../edit/' + this.widgetKey], { relativeTo: this.route });
      } else {
        this.initWidget(resource);
      }
    });
  }

  addDataSource(): void {
    const displayType = VisualisationComponentType[this.widgetContent.type.toUpperCase()];
    const newSource = this.visualisationService.getEmptyDataSource(displayType);
    this.widgetContent.sources.unshift(newSource);
  }

  deleteDataSource(sourceId: number): void {
    const dialogRef = this.dialog.open(ConfirmDeleteDialogComponent, {
      data: { message: 'Would you like to delete this data query?', confirmName: false }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.widgetContent.sources = this.widgetContent.sources.filter(s => s.id !== sourceId);
        this.onQueryChange();
        this.ref.markForCheck();
      }
    });
  }

  onQueryChange(): void {
    this.disableQuerySelect = this.widgetContent.sources != null ?
      this.widgetContent.sources.length > 0 : false;

    this.limitQueryType();
    this.queryTypes =  this.widgetContent.sources.map((source: QuerySource) => {
      return source.query;
    });
  }

  limitQueryType() {
    let newLimits = false;
    let allowedQueries: DataQueryType[] = [];
    if (this.widgetContent.type === VisualisationComponentType.PIECHART
      || this.widgetContent.type === VisualisationComponentType.BARGRAPH
      || this.widgetContent.type === VisualisationComponentType.GAUGE) {
      const config = this.widgetContent.configuration as GraphDisplayConfiguration;
      if (this.widgetContent.sources.length > 1) {
        newLimits = true;
        allowedQueries = [this.widgetContent.sources[this.widgetContent.sources.length - 1].query];
      } else if (config.stops != null && config.stops.length > 0) {
        newLimits = true;
        allowedQueries = [DataQueryType.AGGREGATION];
      } else {
        newLimits = true;
      }
    }
    if (newLimits) {
      this.allowedQueryTypes.next(allowedQueries);
      this.ref.markForCheck();
    }
  }

  setDataSourceError(value: boolean): void {
    if (value !== this.dataSourceError) {
      this.dataSourceError = value;
      this.updateError();
    }
  }

  setDisplayConfigError(value: boolean): void {
    if (value !== this.displayConfigError) {
      this.displayConfigError = value;
      this.updateError();
    }
  }

  updateWidgetType(value: string): void {
    this.widgetContent.type = value;
  }

  updateConfiguration(value: MapDisplayConfiguration | GraphDisplayConfiguration): void {
    this.widgetContent.configuration = value;
  }

  updateError(): void {
    this.errors = (this.dataSourceError || this.displayConfigError);
  }

  closeEdit(): void {
    if (this.new) {
      this.router.navigate(['../'], { relativeTo: this.route });
    } else {
      this.router.navigate(['../../'], { relativeTo: this.route });
    }
  }
}
