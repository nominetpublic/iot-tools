/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatGridListModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faPen, faPlus, faSpinner, faStreetView, faTimes, faTrash, faUndoAlt } from '@fortawesome/free-solid-svg-icons';
import { DynamicSelectModule } from 'app/ui/dynamic-select/dynamic-select.module';

import { MediaGalleryModule } from '../media-gallery/media-gallery.module';
import { VisualisationComponentsModule } from '../visualisation-components/visualisation-components.module';
import { VisualisationConfigurationModule } from '../visualisation-configuration/visualisation-configuration.module';
import { WidgetDisplayComponent } from './widget-display/widget-display.component';
import { WidgetEditComponent } from './widget-edit/widget-edit.component';
import { WidgetGalleryRoutingModule } from './widget-gallery-routing.module';
import { WidgetGalleryComponent } from './widget-gallery/widget-gallery.component';

@NgModule({
  imports: [
    WidgetGalleryRoutingModule,
    CommonModule,
    DragDropModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    DynamicSelectModule,
    MatInputModule,
    MatButtonModule,
    MatGridListModule,
    MediaGalleryModule,
    VisualisationConfigurationModule,
    VisualisationComponentsModule,
  ],
  declarations: [
    WidgetEditComponent,
    WidgetDisplayComponent,
    WidgetGalleryComponent,
  ],
  exports: [
    WidgetGalleryComponent,
  ]
})
export class WidgetGalleryModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faSpinner, faTrash, faPen, faPlus, faTimes, faStreetView, faUndoAlt);
  }
}
