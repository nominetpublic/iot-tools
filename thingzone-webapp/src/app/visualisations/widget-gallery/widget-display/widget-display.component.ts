/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { HttpErrorResponse } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { QuerySource } from 'app/core/models/visualisations/display-data-source';
import { VisualisationComponentType } from 'app/core/models/visualisations/visualisation-component';
import { VisualisationDataLayer } from 'app/core/models/visualisations/visualisation-data-layer';
import { WidgetContent } from 'app/core/models/visualisations/widget-content';
import { PanelResizeService } from 'app/core/services/panel-resize/panel-resize.service';
import { DisplayContext, DisplayContextService } from 'app/core/services/visualisation/display-context.service';
import { VisualisationService } from 'app/core/services/visualisation/visualisation.service';
import { WidgetService } from 'app/core/services/visualisation/widget.service';
import { interval, of, Subscription } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DisplayContextService],
  selector: 'widget-display',
  templateUrl: './widget-display.component.html',
  styleUrls: ['./widget-display.component.scss']
})
export class WidgetDisplayComponent implements OnInit, OnChanges, OnDestroy {
  @Input() widget: WidgetContent;
  @Input() key: string;
  @Input() editing = false;
  @Output() editWidget: EventEmitter<string> = new EventEmitter();
  @Output() deleteWidget: EventEmitter<string> = new EventEmitter();

  VisualisationComponentType = VisualisationComponentType;
  subscriptions: Subscription[] = [];
  layers: VisualisationDataLayer[] = [];
  context: DisplayContext;

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    protected panelResize: PanelResizeService,
    protected contextService: DisplayContextService,
    protected widgetService: WidgetService,
    protected visualisationService: VisualisationService,
    protected ref: ChangeDetectorRef
  ) {
  }

  ngOnInit() {
    this.context = this.contextService.createWidgetContext(this.widget.timeDelta);
    this.contextService.setContext(this.context);
    this.loadDataSets(this.widget);
    this.refreshData(this.context.scope);

    let updateInterval = 60000;
    if (this.widget.interval != null) {
      updateInterval = this.widget.interval * 1000;
    }
    const updateContextSub = interval(updateInterval).subscribe((val: number) => {
      this.context = this.contextService.createWidgetContext(this.widget.timeDelta);
      this.contextService.setContext(this.context);
      this.refreshData(this.context.scope);
    });
    this.subscriptions.push(updateContextSub);

    const displayContextSub = this.contextService.contextDebounced(context => {
      this.context = context;
      if (context != null) {
        this.refreshData(context.scope);
      }
    });
    this.subscriptions.push(displayContextSub);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.context != null) {
      if ('widget' in changes) {
        this.loadDataSets(changes.widget.currentValue);
        this.refreshData(this.context.scope);
      }
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  resize() {
    this.panelResize.dispatch();
  }

  refreshData(changedContext?) {
    for (const layer of this.layers) {
      // if the context that the query uses has not been changed then no need to update
      if (changedContext && !layer.query.scope.includes(changedContext)) continue;

      // check all required contexts for this layer are ready
      if (!this.isContextReady(layer.query.scope)) {
        layer.awaitingContext = true;
        continue;
      }
      this.dataQuery(layer);
    }
  }

  loadDataSets(widgetContent: WidgetContent) {
    this.visualisationService.clearDisplayLayers();
    // if no data sources are defined for this display then no data needs loading
    if (!widgetContent.sources || widgetContent.sources.length === 0) return;

    this.layers = widgetContent.sources.map(source => {
      return this.newLayer(source, widgetContent.type);
    });
    this.ref.markForCheck();
  }

  dataQuery(layer: VisualisationDataLayer): void {
    layer.query.runQuery(layer.source, this.context, layer.displayType, this.widget.configuration).pipe(
      map(data => {
        if ('error' in data) {
          return { ...layer, data: undefined, increment: (layer.increment + 1), loading: false, error: true };
        } else {
          return { ...layer, data: data, increment: (layer.increment + 1), loading: false, error: false, errorMessage: null };
        }
      }), catchError((error: HttpErrorResponse) => {
        return of({ ...layer, data: undefined, increment: (layer.increment + 1), loading: false, error: true, errorMessage: error.error });
      })).subscribe(dataLayer => {
        this.resolveLayer(dataLayer);
      });
  }

  resolveLayer(layer: VisualisationDataLayer): void {
    const id = layer.id;
    let updatedLayer = false;
    // Every id in the new array should exist in the old one
    // If it doesnt the view has changed and this result should be ignored
    if (!this.layers.find(a => a.id === id)) return;

    this.layers = this.layers.map(existingLayer => {
      if (id === existingLayer.id && layer.increment > existingLayer.increment) {
        updatedLayer = true;
        return layer;
      }
      return existingLayer;
    });

    if (updatedLayer) {
      this.ref.markForCheck();
    }
  }

  isContextReady(key: string | string[]): boolean {
    if (this.context == null) {
      return false;
    }
    const context = this.context;
    let ready = true;

    if (key instanceof Array) {
      ready = key.every(k => k in context);
    } else if (!(key in context)) {
      ready = false;
    }
    return ready;
  }

  newLayer(source: QuerySource, displayType: string): VisualisationDataLayer {
    const dataQuery = this.visualisationService.queryFactory(source);
    const componentType = VisualisationComponentType[displayType.toUpperCase()];
    return {
      componentId: 0,
      displayType: componentType,
      id: source.id,
      label: source.name,
      increment: 0,
      data: [],
      source: source,
      query: dataQuery,
      filtered: false,
      awaitingContext: false,
      loading: false,
      error: false,
    };
  }

  delete() {
    this.deleteWidget.emit();
  }

  edit(): void {
    this.editWidget.emit();
  }

  saveMapView(): void {
    this.widgetService.saveMapState();
  }

  resetTimeRange(): void {
    this.context = this.contextService.createWidgetContext(this.widget.timeDelta);
    this.contextService.setContext(this.context);
  }
}
