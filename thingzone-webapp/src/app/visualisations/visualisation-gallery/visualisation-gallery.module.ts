/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faChartBar, faDotCircle } from '@fortawesome/free-regular-svg-icons';
import {
  faBackward,
  faCalendar,
  faCaretDown,
  faCheck,
  faClock,
  faExclamationTriangle,
  faForward,
  faLayerGroup,
  faPause,
  faPen,
  faPlay,
  faPlus,
  faSpinner,
  faTachometerAlt,
  faTrash,
  faUndoAlt,
  faCheckCircle,
  faSave,
  faUndo,
} from '@fortawesome/free-solid-svg-icons';
import { DateTimeModule } from 'app/ui/date-time/date-time.module';
import { PageOverviewModule } from 'app/ui/page-overview/page-overview.module';
import { SharedPipesModule } from 'app/ui/shared-pipes/shared-pipes.module';

import { MediaGalleryModule } from '../media-gallery/media-gallery.module';
import { VisualisationComponentsModule } from '../visualisation-components/visualisation-components.module';
import { VisualisationConfigurationModule } from '../visualisation-configuration/visualisation-configuration.module';
import { LayerControlComponent } from './layer-control/layer-control.component';
import { TimeSliderComponent } from './time-slider/time-slider.component';
import { VisDisplayComponent } from './vis-display/vis-display.component';
import { VisEditComponent } from './vis-edit/vis-edit.component';
import { VisListComponent } from './vis-list/vis-list.component';
import { VisRowComponent } from './vis-row/vis-row.component';
import { VisualisationGalleryRoutingModule } from './visualisation-gallery-routing.module';

@NgModule({
  imports: [
    CommonModule,
    VisualisationGalleryRoutingModule,
    FontAwesomeModule,
    DragDropModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatInputModule,
    PageOverviewModule,
    SharedPipesModule,
    DateTimeModule,
    VisualisationConfigurationModule,
    VisualisationComponentsModule,
    MediaGalleryModule,
  ],
  declarations: [
    LayerControlComponent,
    TimeSliderComponent,
    VisDisplayComponent,
    VisEditComponent,
    VisRowComponent,
    VisListComponent,
  ],
  exports: [
    VisListComponent,
    TimeSliderComponent
  ],
})

export class VisualisationGalleryModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faTrash, faCaretDown, faPen, faPlus, faSpinner, faLayerGroup, faForward, faBackward, faUndoAlt, faSave, faUndo,
      faClock, faCalendar, faPlay, faPause, faCheck, faExclamationTriangle, faChartBar, faTachometerAlt, faDotCircle, faCheckCircle);
  }
}
