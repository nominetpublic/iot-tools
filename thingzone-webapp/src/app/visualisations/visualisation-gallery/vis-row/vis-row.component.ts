/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DisplayResource } from 'app/core/models/visualisations/display-resource';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'vis-row',
  templateUrl: './vis-row.component.html',
  styleUrls: ['./vis-row.component.scss']
})
export class VisRowComponent implements OnInit {

  @Input() resource: DisplayResource;
  @Input() name = 'Visualisation';
  @Output() change: EventEmitter<string> = new EventEmitter();
  @Output() edit: EventEmitter<string> = new EventEmitter();
  @Output() delete: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  changeResource(key: string) {
    this.change.emit(key);
  }

  editResource(key: string) {
    this.edit.emit(key);
  }

  deleteResource(key: string) {
    this.delete.emit(key);
  }

}
