/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DisplayResource } from 'app/core/models/visualisations/display-resource';
import { DisplayContextService } from 'app/core/services/visualisation/display-context.service';
import { VisualisationService } from 'app/core/services/visualisation/visualisation.service';
import { Subscription } from 'rxjs';

@Component({
  providers: [DisplayContextService],
  selector: 'vis-edit',
  templateUrl: './vis-edit.component.html',
  styleUrls: ['./vis-edit.component.scss']
})
export class VisEditComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  visualisationKey: string;
  visualisation: DisplayResource;
  visualisationType: string;
  newVisualisation = false;

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    protected visualisationService: VisualisationService,
    protected displayContext: DisplayContextService,
    protected ref: ChangeDetectorRef
  ) {
  }

  ngOnInit() {
    this.route.parent.data.subscribe(data => {
      this.visualisationType = data['visualisationType'];
    });

    this.route.params.subscribe(params => {
      if (params.id != null) {
        this.visualisationKey = params.id;
        this.setSource(this.visualisationKey);
        this.newVisualisation = false;
      } else {
        this.newVisualisation = true;
        this.visualisation = this.visualisationService.getEmptyVisualisation();
      }
      this.ref.detectChanges();
    });

    const visualisationSub = this.visualisationService.visualisations$.subscribe(displays => {
      if (displays != null && displays.length > 0) {
        if (displays[0].type !== this.visualisationType) {
          this.visualisationService.queryVisualisations(this.visualisationType);
        } else if (this.visualisationKey != null) {
          this.setSource(this.visualisationKey);
          this.ref.markForCheck();
        }
      }
    });
    this.subscriptions.push(visualisationSub);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  setSource(key: string): any {
    this.visualisation = this.visualisationService.getVisualisation(key);
  }

  editVisualisation(visualisationKey: string) {
    this.router.navigate(['../edit/' + visualisationKey], { relativeTo: this.route });
  }

  saveDisplayResource(resource: DisplayResource) {
    this.visualisationService.upsertVisualisation(resource, this.visualisationType).subscribe((res) => {
      this.visualisation = resource;
      this.visualisationKey = res.resource.key;
      this.visualisationService.queryVisualisations(this.visualisationType);
      if (resource.key == null) {
        this.editVisualisation(this.visualisationKey);
      }
      this.ref.detectChanges();
    });
  }

}
