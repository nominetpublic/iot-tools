/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DisplayResource } from 'app/core/models/visualisations/display-resource';
import { SearchService } from 'app/core/services/search/search.service';
import { VisualisationService } from 'app/core/services/visualisation/visualisation.service';
import { ConfirmDeleteDialogComponent } from 'app/ui/form-components/confirm-delete-dialog/confirm-delete-dialog.component';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'vis-list',
  templateUrl: './vis-list.component.html',
  styleUrls: ['./vis-list.component.scss'],
})
export class VisListComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  selectedVisualisation: string = null;
  visualisations: DisplayResource[] = [];
  filterText = '';
  filterControl = new FormControl('');
  visualisationType: string = null;
  dragging: boolean;

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    private ref: ChangeDetectorRef,
    private search: SearchService,
    private visualisationService: VisualisationService,
    public dialog: MatDialog,
  ) {
  }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.visualisationType = data['visualisationType'];
    });

    if (this.route.firstChild) {
      this.route.firstChild.params.subscribe(params => {
        this.selectedVisualisation = params.id;
      });
    }
    this.populateSearches();
    this.visualisationService.queryVisualisations(this.visualisationType);

    const visSub = this.visualisationService.visualisations$.pipe(
      filter(v => v != null && (v.length > 0 && v[0].type === this.visualisationType))
    ).subscribe(displays => {
      this.visualisations = displays;
      if (this.visualisations.length > 0 && !this.selectedVisualisation) {
        this.changeVisualisation(this.visualisations[0].key);
      }
      this.ref.markForCheck();
    });
    this.subscriptions.push(visSub);

    const filterSub = this.filterControl.valueChanges.subscribe(value => {
      this.filterText = value;
    });
    this.subscriptions.push(filterSub);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  populateSearches() {
    if (this.search.getAlertSavedSearches() == null) {
      this.search.queryAlertSavedSearches();
    }
    if (this.search.getDeviceSavedSearches() == null) {
      this.search.queryDeviceSavedSearches();
    }
  }

  changeVisualisation(key: string) {
    this.selectedVisualisation = key;
    this.router.navigate(['./display/' + key], { relativeTo: this.route });
  }

  editVisualisation(key: string) {
    this.selectedVisualisation = key;
    this.router.navigate(['./edit/' + key], { relativeTo: this.route });
  }

  newVisualisation() {
    this.selectedVisualisation = null;
    this.router.navigate(['./new/'], { relativeTo: this.route });
  }

  checkSelected(key: string): boolean {
    return this.selectedVisualisation === key;
  }

  deleteVisualisation(key: string): void {
    const dialogRef = this.dialog.open(ConfirmDeleteDialogComponent, {
      data: { message: 'Would you like to delete this visualisation?', confirmName: false }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        const visualisations = this.visualisationService.getVisualisations();
        this.visualisationService.deleteVisualisation(key).subscribe(() => {
          const filtered = visualisations.filter((resource) => !(resource.key === key && resource.resourceName === 'visualisation'));
          this.visualisationService.setVisualisations(filtered, this.visualisationType);
        });
      }
    });
  }

  visByKey(index: number, vis: DisplayResource): string {
    return vis.key;
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.visualisations, event.previousIndex, event.currentIndex);
    this.visualisations = this.visualisationService.reorderVisualisations(this.visualisations);
    this.visualisationService.setVisualisations(this.visualisations, this.visualisationType);
  }
}
