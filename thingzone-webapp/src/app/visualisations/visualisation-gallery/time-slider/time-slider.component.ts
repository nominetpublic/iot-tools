/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { DisplayContext, DisplayContextService } from 'app/core/services/visualisation/display-context.service';
import { D3Utils } from 'app/core/utils/d3-utils';
import { scaleTime } from 'd3-scale';
import * as moment from 'moment';
import { fromEvent, merge, Subscription, timer } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'time-slider',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './time-slider.component.html',
  styleUrls: ['./time-slider.component.scss'],
})
export class TimeSliderComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() initialContext: DisplayContext;
  @Input() live = false;
  @Input() liveUpdate = 1000; // time between updates for the live timer

  private subscriptions: Subscription[] = [];
  size: { width: number, height: number };
  margin = 20;

  sliderEl: HTMLElement;
  timeScale;
  currentRange: Date[];
  currentDate: Date;
  sliderValue = 0;
  moving = false;
  timer;
  rangeTimer;
  dragging: boolean;
  dragStart: number;
  tickNumber = 10;
  speed = 1;

  constructor(
    private element: ElementRef,
    private ref: ChangeDetectorRef,
    protected displayContext: DisplayContextService
  ) {
    this.sliderEl = element.nativeElement;
  }

  ngOnInit() {
    this.bindWindowResizeEvent();
    if (this.initialContext == null) {
      this.initialContext = this.displayContext.getContext();
    }
    this.currentDate = this.initialContext.current;
    this.currentRange = [this.initialContext.range.from, this.initialContext.range.to];
    if (this.currentRange != null) {
      this.calculateSize();
      this.setTimeScale();
    }

    if (this.live) {
      this.setLive();
    }

    const contextSub = this.displayContext.context$.subscribe(context => {
      this.updateContext(context);
    });
    this.subscriptions.push(contextSub);
  }

  ngAfterViewInit() {
    this.initSlider();
  }

  ngOnDestroy() {
    clearInterval(this.timer);
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  updateContext(context: DisplayContext): void {
    if (this.currentRange[0].getTime() !== context.range.to.getTime()
      || this.currentRange[1].getTime() !== context.range.from.getTime()) {
      this.currentRange = [context.range.from, context.range.to];
      this.setTimeScale();
      this.changeSlider(this.currentDate);
    }
    if (this.currentDate.getTime() !== context.current.getTime()) {
      this.currentDate = context.current;
      this.changeSlider(this.currentDate);
    }
  }

  calculateSize(): void {
    this.size = {
      width: this.sliderEl.offsetWidth - (this.margin * 2),
      height: 25,
    };
  }

  private bindWindowResizeEvent(): void {
    const resize$ = fromEvent(window, 'resize');
    const panelResize$ = fromEvent(window, 'panelResize');
    const subscription = merge(resize$, panelResize$).pipe(debounceTime(200)).subscribe(e => {
      this.resizeFunction();
      if (this.ref) {
        this.ref.markForCheck();
      }
    });
    this.subscriptions.push(subscription);
  }

  resizeFunction() {
    this.initSlider();
  }

  setTimeScale(): void {
    this.timeScale = scaleTime()
      .domain(this.currentRange)
      .range([0, this.size.width])
      .clamp(true);
    this.ref.markForCheck();
  }

  initSlider(): void {
    this.calculateSize();
    if (!this.size || !this.size.width || !this.size.height) {
      throw new Error('missing slider dimensions');
    }
    // timescale needs to be updated on resize
    this.setTimeScale();
    this.changeSlider(this.currentDate);
    this.ref.markForCheck();
  }

  changeSlider(date: Date): void {
    this.sliderValue = this.timeScale(date);
    this.ref.markForCheck();
  }

  formatDate(date: Date): string {
    const formatter = D3Utils.d3TimeFormat;
    return formatter(date);
  }

  onMouseDown(event: MouseEvent): void {
    event.preventDefault();
    if (this.live) {
      this.toggleLive();
    }
    if (!this.dragging) {
      const x = event.offsetX - this.margin;
      const y = event.offsetY - 20;
      const circleRadius = 6;
      if (this.distance(this.sliderValue, x, y) < circleRadius) {
        this.dragging = true;
        this.dragStart = event.x - this.sliderValue;
      } else {
        this.sliderValue = x;
        this.displayContext.updateCurrentDate(this.timeScale.invert(this.sliderValue));
        this.ref.markForCheck();
      }
    }
  }

  @HostListener('window:mousemove', ['$event'])
  onDrag(event: MouseEvent): void {
    if (!this.dragging) return;
    event.preventDefault();
    const moveEnd = event.x - this.dragStart;
    if (moveEnd >= 0 && moveEnd < this.size.width) {
      this.sliderValue = moveEnd;
      this.displayContext.updateCurrentDate(this.timeScale.invert(this.sliderValue));
      this.ref.markForCheck();
    }
  }

  @HostListener('window:mouseup', ['$event'])
  onDragEnd(event: MouseEvent): void {
    if (!this.dragging) return;
    event.preventDefault();
    this.dragging = false;
  }

  distance(position1, x, y): number {
    const a = position1 - x;
    return Math.sqrt(a * a + y);
  }

  playPause() {
    clearInterval(this.timer);
    clearInterval(this.rangeTimer);
    if (this.moving) {
      this.moving = false;
    } else {
      this.moving = true;
      this.timer = setInterval(this.stepTimer.bind(this), 1000);
    }
  }

  stepTimer() {
    const currentTime = moment(this.currentDate);
    const newTime = currentTime.add(1 * this.speed, 's');

    this.currentDate = newTime.toDate();
    this.sliderValue = this.timeScale(this.currentDate);

    this.displayContext.updateCurrentDate(this.currentDate);
    if (this.sliderValue >= this.size.width) {
      this.moving = false;
      clearInterval(this.timer);
    }
    this.ref.markForCheck();
  }

  tickTransform(tick): string {
    return 'translate(' + this.timeScale(tick) + ',' + 0 + ')';
  }

  toggleLive() {
    this.live = !this.live;
    this.moving = false;
    clearInterval(this.timer);
    clearInterval(this.rangeTimer);
    if (this.live) {
      this.setLive();
    } else {
      this.displayContext.updateCurrentDate(this.initialContext.current);
      this.displayContext.updateDateRange(this.initialContext.range.to, this.initialContext.range.from);
    }
  }

  setLive() {
    this.liveTimer();
    this.liveRange();
    this.sliderValue = this.size.width;
    this.moving = true;
    this.timer = setInterval(this.liveTimer.bind(this), this.liveUpdate);
    // update the range every 5 minutes
    this.rangeTimer = setInterval(this.liveRange.bind(this), 300000);
    this.speed = 1;
  }

  liveTimer() {
    this.displayContext.updateCurrentDate(new Date());
  }

  liveRange() {
    const now = new Date();
    const diff = moment(this.currentRange[1]).diff(this.currentRange[0], 'seconds');
    const from = moment(now).subtract(diff, 'seconds').toDate();
    this.displayContext.updateDateRange(now, from, now);
  }

  forward() {
    if (this.speed > 0) {
      this.speed = this.speed * 2;
    } else if (this.speed === -2) {
      this.speed = 1;
    } else {
      this.speed = this.speed / 2;
    }
  }

  resetSpeed() {
    this.speed = 1;
  }

  backward() {
    if (this.speed < 0) {
      this.speed = this.speed * 2;
    } else if (this.speed === 2) {
      this.speed = 1;
    } else if (this.speed === 1) {
      this.speed = -1;
    } else {
      this.speed = this.speed / 2;
    }
  }
}
