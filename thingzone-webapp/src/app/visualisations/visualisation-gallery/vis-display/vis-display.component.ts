/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { QuerySource } from 'app/core/models/visualisations/display-data-source';
import { DisplayResource } from 'app/core/models/visualisations/display-resource';
import { VisualisationComponent, VisualisationComponentType } from 'app/core/models/visualisations/visualisation-component';
import { VisualisationContent } from 'app/core/models/visualisations/visualisation-content';
import { VisualisationDataLayer } from 'app/core/models/visualisations/visualisation-data-layer';
import { PanelResizeService } from 'app/core/services/panel-resize/panel-resize.service';
import { DisplayContext, DisplayContextService } from 'app/core/services/visualisation/display-context.service';
import { VisualisationLayout, VisualisationService } from 'app/core/services/visualisation/visualisation.service';
import { DateUtils } from 'app/core/utils/date-utils';
import * as moment from 'moment';
import { of, Subscription } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DisplayContextService],
  selector: 'vis-display',
  templateUrl: './vis-display.component.html',
  styleUrls: ['./vis-display.component.scss']
})
export class VisDisplayComponent implements OnInit, OnDestroy {
  formatDate = DateUtils.formatDate;
  formatTime = DateUtils.formatTime;
  VisualisationComponentType = VisualisationComponentType;
  moment = moment;

  subscriptions: Subscription[] = [];
  visualisationKey: string = null;
  visualisation: DisplayResource;
  displayConfig: VisualisationContent;
  currentLayout: VisualisationLayout;

  title = '';
  notFound = false;
  sourceFilters: number[] = [];

  openModal = null;

  context: DisplayContext = null;
  components: { [index: string]: VisualisationComponent } = {};
  private resizeFunction = this.resize.bind(this);
  visualisationType: string;
  startDate: Date;
  endDate: Date;

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    protected panelResize: PanelResizeService,
    protected visualisationService: VisualisationService,
    protected displayContext: DisplayContextService,
    protected ref: ChangeDetectorRef
  ) {
  }

  ngOnInit() {
    this.route.parent.data.subscribe(data => {
      this.visualisationType = data['visualisationType'];
    });

    this.route.params.subscribe(params => {
      this.visualisationKey = params.id;
      this.context = null;

      this.setSource(this.visualisationKey);
      this.ref.markForCheck();
    });

    const displayContextSub = this.displayContext.contextDebounced(context => {
      this.context = context;
      if (context != null) {
        this.refreshData(context.scope);
      }
    });
    this.subscriptions.push(displayContextSub);

    const visualisationSub = this.visualisationService.visualisations$.subscribe(displays => {
      if (displays != null && displays.length > 0) {
        if (displays[0].type !== this.visualisationType) {
          this.visualisationService.queryVisualisations(this.visualisationType);
        } else if (this.visualisationKey != null) {
          this.setSource(this.visualisationKey);
          this.ref.markForCheck();
        }
      }
    });
    this.subscriptions.push(visualisationSub);

    const displayContextDateSub = this.displayContext.dateDebounced(context => {
      this.startDate = context.range.from;
      if (context.range.from != null) {
        this.endDate = context.range.to;
      } else {
        this.endDate = new Date();
      }
      this.ref.markForCheck();
    });
    this.subscriptions.push(displayContextDateSub);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  setSource(key: string): void {
    this.visualisation = this.visualisationService.getVisualisation(key);
    this.visualisationService.clearDisplayLayers();

    if (this.visualisation) {
      this.notFound = false;
      this.title = this.visualisation.name;
      this.displayConfig = this.visualisation.content as VisualisationContent;
      this.currentLayout = this.visualisationService.findLayout(this.displayConfig.configuration.layout);
      this.sourceFilters = [];
      this.displayConfig.components.forEach(component => this.components[component.id] = component);
      this.loadDataSets(this.displayConfig);
      this.displayContext.updateDateRange(this.displayConfig.configuration.to,
        this.displayConfig.configuration.from, this.displayConfig.configuration.current, true);
    } else {
      this.title = 'Error: Visualisation Not Found';
      this.notFound = true;
      this.displayConfig = null;
      this.context = null;
      this.displayContext.clearContext();
    }
  }

  refreshData(changedContext?): void {
    const layers = this.visualisationService.getCurrentLayers();
    for (const key of Object.keys(layers)) {
      for (const layer of layers[key]) {
        // the layer is currently filtered out
        if (layer.filtered === true) {
          continue;
        }
        // if the context that the query uses has not been changed then no need to update
        if (changedContext && !layer.query.scope.includes(changedContext)) continue;

        // check all required contexts for this layer are ready
        if (!this.isContextReady(layer.query.scope)) {
          layer.awaitingContext = true;
          continue;
        } else {
          layer.awaitingContext = false;
        }

        // always reload queries that use current data
        if (!layer.query.scope.includes('current')) layer.loading = true;

        // run the query and update the layer with data when it completes
        this.dataQuery(layer);
      }
    }
  }

  getLayoutComponents(displayIndex: number): VisualisationComponent[] {
    return this.visualisationService.getComponentsForLayout(this.displayConfig, displayIndex);
  }

  toggleModal(id: string): void {
    if (this.openModal === id) {
      this.openModal = null;
    } else {
      this.openModal = id;
    }
    // send resize event after change detection has happened
    setTimeout(this.resizeFunction, 0);
  }

  resize() {
    this.panelResize.dispatch();
  }

  layerSelect(dataSource: QuerySource): void {
    const id = dataSource.id;
    const index = this.sourceFilters.indexOf(id);

    if (index === -1) {
      this.sourceFilters = [...this.sourceFilters, id];
      this.filterLayers(dataSource, false);
    } else {
      this.sourceFilters = this.sourceFilters.filter(a => a !== id);
      this.filterLayers(dataSource, true);
    }
  }

  /**
   * Show or hide individual data source layers
   */
  filterLayers(dataSource: QuerySource, show: boolean): void {
    const layers = this.visualisationService.getCurrentLayers();
    for (const key of Object.keys(layers)) {
      layers[key] = layers[key].map(layer => {
        if (layer.id === dataSource.id) {
          if (show) {
            layer = { ...layer, filtered: false };
            this.dataQuery(layer);
          } else {
            return { ...layer, data: undefined, increment: (layer.increment + 1), filtered: true };
          }
        }
        return layer;
      });
    }
    this.visualisationService.setDisplayLayers(layers);
  }

  // load data sources to initialise visualisation data layers
  loadDataSets(display: VisualisationContent): void {
    const layers: { [index: string]: VisualisationDataLayer[] } = {};
    display.components.forEach(component => {
      // if no data sources are defined for this display then no data needs loading
      if (!component.sources || component.sources.length === 0) return;

      layers[component.id] = component.sources.map(source => {
        return this.newLayer(source, component);
      });
    });
    this.visualisationService.setDisplayLayers(layers);
  }

  dataQuery(layer: VisualisationDataLayer): void {
    layer.query.runQuery(layer.source, this.context, layer.displayType, this.components[layer.componentId].configuration).pipe(
      map(data => {
        if (data == null || 'error' in data) {
          return { ...layer, data: undefined, increment: (layer.increment + 1), loading: false, error: true };
        } else {
          return { ...layer, data: data, increment: (layer.increment + 1), loading: false, error: false, errorMessage: null };
        }
      }), catchError((error: HttpErrorResponse) => {
        return of({ ...layer, data: undefined, increment: (layer.increment + 1), loading: false, error: true, errorMessage: error.error });
      })).subscribe(dataLayer => {
        this.updateLayers(dataLayer);
      });
  }

  updateLayers(layer: VisualisationDataLayer): void {
    const id = layer.id;
    let updatedLayer = false;
    const currentLayers = this.visualisationService.getCurrentLayers();
    if (layer.componentId in currentLayers) {
      const dataLayers = currentLayers[layer.componentId];
      // Every id in the new array should exist in the old one
      // If it doesnt the view has changed and this result should be ignored
      if (!dataLayers.find(a => a.id === id)) return;

      currentLayers[layer.componentId] = dataLayers.map(existingLayer => {
        if (id === existingLayer.id && layer.increment > existingLayer.increment) {
          updatedLayer = true;
          return layer;
        }
        return existingLayer;
      });
    }
    if (updatedLayer) {
      this.visualisationService.setDisplayLayers(currentLayers);
    }
  }

  isContextReady(key: string | string[]): boolean {
    if (this.context == null) {
      return false;
    }
    const context = this.context;
    let ready = true;

    if (key instanceof Array) {
      ready = key.every(k => k in context);
    } else if (!(key in context)) {
      ready = false;
    }
    return ready;
  }

  resetContext(): void {
    if (this.displayConfig != null && this.displayConfig.configuration != null) {
      this.displayContext.updateDateRange(this.displayConfig.configuration.to, this.displayConfig.configuration.from);
    }
  }

  newLayer(source: QuerySource, component: VisualisationComponent): VisualisationDataLayer {
    const dataQuery = this.visualisationService.queryFactory(source);
    const displayType = VisualisationComponentType[component.type.toUpperCase()];
    return {
      componentId: component.id,
      displayType: displayType,
      id: source.id,
      label: source.name,
      increment: 0,
      data: [],
      source: source,
      query: dataQuery,
      filtered: false,
      awaitingContext: true,
      loading: false,
      error: false,
    };
  }

  updateDates(value: moment.Moment, key: string) {
    const date = value.toDate();
    if (key === 'from') {
      this.displayContext.updateDateRange(this.context.range.to, date, this.context.range.to);
    } else if (key === 'to') {
      this.displayContext.updateDateRange(date, this.context.range.from, date);
    } else if (key === 'current') {
      this.displayContext.updateCurrentDate(date);
    }
  }
}
