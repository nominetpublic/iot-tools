/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { QuerySource } from 'app/core/models/visualisations/display-data-source';
import { VisualisationContent } from 'app/core/models/visualisations/visualisation-content';
import { VisualisationDataLayer } from 'app/core/models/visualisations/visualisation-data-layer';
import { GeoService } from 'app/core/services/geo/geo.service';
import { VisualisationService } from 'app/core/services/visualisation/visualisation.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'layer-control',
  templateUrl: './layer-control.component.html',
  styleUrls: ['./layer-control.component.scss']
})
export class LayerControlComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  @Input() display: VisualisationContent;
  @Input() selected: number[] = [];
  @Output() select: EventEmitter<QuerySource> = new EventEmitter();

  layers: { [index: string]: VisualisationDataLayer[] } = {};
  errors: number[] = [];
  loading: number[] = [];

  constructor(
    private visualisationService: VisualisationService,
    private geoService: GeoService) { }


  ngOnInit(): void {
    this.subscriptions.push(this.visualisationService.displayLayers$.subscribe(layers => {
      this.update(layers);
    }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  update(layers: { [index: string]: VisualisationDataLayer[] }) {
    this.layers = layers;
    this.loading = [];
    this.errors = [];
    for (const [key, value] of Object.entries(layers)) {
      this.loading = this.loading.concat(value.filter(a => a.loading === true).map(a => a.id));
      this.errors = this.errors.concat(value.filter(a => a.error === true).map(a => a.id));
    }
  }

  toggleQuery(source: QuerySource) {
    this.select.emit(source);
  }

  isSelected(source: QuerySource) {
    return this.selected.includes(source.id);
  }

  isLoading(source: QuerySource) {
    return this.loading.includes(source.id);
  }

  isError(source: QuerySource) {
    return this.errors.includes(source.id);
  }

  errorMessage(componentId: number, queryId: number): string {
    const layer = this.layers[componentId].find(item => (item.id === queryId));
    if (layer != null) {
      return layer.errorMessage;
    }
    return '';
  }

  getDevices(componentId: string, queryId: number): [] {
    const layer = this.layers[componentId].find(item => (item.id === queryId));
    if (layer != null && layer.data) {
      if ('geojson' in layer.data) {
        return layer.data.geojson.features;
      }
    }
    return [];
  }

  focusDevice(geometry) {
    const centre = this.geoService.getCentre(geometry);
    if (centre != null) {
      this.visualisationService.setMapCentre(centre);
    }
  }

}
