/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, OnChanges } from '@angular/core';
import { VisualisationDataLayer } from 'app/core/models/visualisations/visualisation-data-layer';
import { ApiService } from 'app/core/services/api/api.service';
import { Subscription, Subject } from 'rxjs';
import { ClassificationModalComponent } from '../classification-modal/classification-modal.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'media-widget',
  templateUrl: './media-widget.component.html',
  styleUrls: ['./media-widget.component.scss']
})
export class MediaWidgetComponent implements OnChanges {
  @Input() dataLayers: VisualisationDataLayer[] = [];
  imageUrl: string;
  data: any;
  selected = 0;
  showModal = new Subject<boolean>();
  subscriptions: Subscription[] = [];

  constructor(
    protected api: ApiService,
    protected dialog: MatDialog,
  ) { }

  ngOnChanges(changes) {
    if (changes.dataLayers) {
      const current: VisualisationDataLayer[] = changes.dataLayers.currentValue || [];

      this.data = [];
      current.forEach((layer) => {
        if (layer.data != null) {
          this.data = this.data.concat(layer.data);
        }
      });
    }

    if (this.data.length > 0) {
      const burst = this.data[0];
      if (burst.length === 1) {
        this.imageUrl = this.getImageUrl(burst[0].imageRef);
      } else {
        burst[0].sort((a, b) => {
          const firstDate = new Date(a.timestamp);
          const secondDate = new Date(b.timestamp);
          return secondDate.getTime() - firstDate.getTime();
        });
        this.imageUrl = this.getImageUrl(burst[0].imageRef);
      }
    }
  }

  getImageUrl(ref) {
    const imageServer = this.api.getImageServer();
    return `${imageServer}images/${ref}`;
  }

  selectMedia(e): void {
    this.showModal.next(true);
    e.stopPropagation();
  }

  nextSelect(): void {
    if ((this.selected + 1) < this.data.length) {
      this.selected++;
    }
  }

  prevSelect(): void {
    if (this.selected > 0) {
      this.selected--;
    }
  }

  clearSelection(): void {
    this.showModal.next(false);
  }

  editClassifications(context) {
    this.showModal.next(false);
    const classificationEditModal = this.dialog.open(ClassificationModalComponent, {
      panelClass: 'dialog-container',
      maxHeight: '85%',
      maxWidth: '90%',
      data: context
    });

    this.subscriptions.push(classificationEditModal.beforeClosed()
      .subscribe(r => {
        if (r) this.data[this.selected][r.index].classifications = r.classifications;
        this.showModal.next(true);
      }));
  }

}
