/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, ElementRef, Input, OnChanges, OnInit, OnDestroy } from '@angular/core';
import { VisualisationDataLayer } from 'app/core/models/visualisations/visualisation-data-layer';
import { ClassificationModalComponent } from '../classification-modal/classification-modal.component';
import { MatDialog } from '@angular/material';
import { Subject, Subscription } from 'rxjs';

@Component({
  selector: 'media-gallery',
  templateUrl: './media-gallery.component.html',
  styleUrls: ['./media-gallery.component.scss']
})
export class MediaGalleryComponent implements OnInit, OnChanges, OnDestroy {

  @Input() dataLayers: VisualisationDataLayer[] = [];
  @Input() images = [];
  @Input() horizontal = false;

  data = [];
  selected = 0;
  showModal = new Subject<boolean>();
  wrapperEl;
  resizeTimeout;
  subscriptions: Subscription[] = [];

  constructor(
    element: ElementRef,
    protected dialog: MatDialog,
  ) {
    this.wrapperEl = element.nativeElement;
  }

  ngOnInit() {
    this.wrapperEl = this.wrapperEl.querySelector('.gallery-wrapper');
    window.addEventListener('resize', () => this.resize());
    this.resize();
  }

  ngOnChanges(changes) {
    if (changes.dataLayers) {
      const current: VisualisationDataLayer[] = changes.dataLayers.currentValue || [];

      this.data = [];
      current.forEach((layer) => {
        if (layer.data != null) {
          this.data = this.data.concat(layer.data);
        }
      });
    }

    if (changes.images) {
      this.data = changes.images.currentValue || [];
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  resize(): void {
    if (!this.wrapperEl) return;

    clearTimeout(this.resizeTimeout);
  }

  selectMedia(index: number, e): void {
    this.selected = index;
    this.showModal.next(true);
    e.stopPropagation();
  }

  nextSelect(): void {
    if ((this.selected + 1) < this.data.length) {
      this.selected++;
    }
  }

  prevSelect(): void {
    if (this.selected > 0) {
      this.selected--;
    }
  }

  clearSelection(): void {
    if (this.data[this.selected].length === 0) this.data.splice(this.selected, 1);
    this.showModal.next(false);
  }

  editClassifications(context) {
    this.showModal.next(false);
    const classificationEditModal = this.dialog.open(ClassificationModalComponent, {
      panelClass: 'dialog-container',
      maxHeight: '85%',
      maxWidth: '90%',
      data: context
    });

    this.subscriptions.push(classificationEditModal.beforeClosed()
      .subscribe(r => {
        if (r) this.data[this.selected][r.index].classifications = r.classifications;
        this.showModal.next(true);
      }));
  }

  trackByFn(index, item) {
    return item[0].imageRef;
  }
}
