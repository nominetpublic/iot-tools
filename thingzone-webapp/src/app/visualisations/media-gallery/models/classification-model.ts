/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export interface ClassificationModel {

    label: string;

    origin: string;

    timestamp: Date;

    confidence: number;

    /**
     * Bounding box format like;
     * "bbox[0.574931880108992,0.543071161048689,0.689373297002725,0.692883895131086]"
     *
     * @type {string}
     * @memberof ClassificationModel
     */
    selection: string;

    curated: boolean;

    colour?: string;
}
