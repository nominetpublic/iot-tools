/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  AfterViewChecked,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material';
import { ApiService } from 'app/core/services/api/api.service';
import { ColourService } from 'app/core/services/api/colour.service';
import { ImageApiService } from 'app/core/services/api/image-api.service';
import { PreferenceService } from 'app/core/services/preference/preference.service';
import { ImageCanvasService } from 'app/core/services/visualisation/image-canvas.service';
import { ConfirmDeleteDialogComponent } from 'app/ui/form-components/confirm-delete-dialog/confirm-delete-dialog.component';
import * as EXIF from 'exif-js';
import { Subject, Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { ClassificationModel } from '../models/classification-model';

@Component({
  selector: 'media-modal',
  templateUrl: './media-modal.component.html',
  styleUrls: ['./media-modal.component.scss'],
})
export class MediaModalComponent implements AfterViewInit, AfterViewChecked, OnChanges, OnDestroy {

  @Input() images: any[];
  @Input() hasPrev: boolean;
  @Input() hasNext: boolean;
  @Output() prev = new EventEmitter();
  @Output() next = new EventEmitter();
  @Output() close = new EventEmitter();
  @Output() classificationContext = new EventEmitter<object>();

  @ViewChild('imageContainer', { static: true }) imageContainerRef: ElementRef;
  @ViewChild('image', { static: true }) imageRef: ElementRef;
  @ViewChild('canvas', { static: true }) canvasRef: ElementRef<HTMLCanvasElement>;

  private subscriptions: Subscription[] = [];
  imageServer: string;
  imageUrl: string;
  tags: string[] = [];
  burst = false;
  imageIndex = 0;
  sliderWidth = 0;
  imageWidths: number[] = [];
  exifMetadata$ = new Subject<any>();
  thumbnail$ = new Subject<string>();
  classifications: ClassificationModel[] = [];
  private ctx: CanvasRenderingContext2D;
  imageDimensions;
  canvasDimensions;
  imageContainerDimensions;

  constructor(
    private cd: ChangeDetectorRef,
    protected api: ApiService,
    protected imageApi: ImageApiService,
    protected preferenceApi: PreferenceService,
    protected dialog: MatDialog,
    protected colourService: ColourService,
    protected imageCanvasService: ImageCanvasService
  ) {
    this.imageServer = this.api.getImageServer();
    this.subscriptions.push(this.imageCanvasService.imageContainerDimensions.subscribe(r => this.imageContainerDimensions = r));
    this.subscriptions.push(this.imageCanvasService.imageDimensions.subscribe(r => this.imageDimensions = r));
    this.subscriptions.push(this.imageCanvasService.canvasDimensions.subscribe(r => this.canvasDimensions = r));
  }

  ngAfterViewInit() {
    const exifSub = this.exifMetadata$.subscribe(tags => {
      if (tags != null && 'thumbnail' in tags) {
        this.getExifThumbnail(tags['thumbnail']);
      }
    });
    this.subscriptions.push(exifSub);
  }

  ngOnChanges(changes) {
    if (changes.images.previousValue == null
      || changes.images.previousValue[0].imageRef !== changes.images.currentValue[0].imageRef) {
      this.images = changes.images.currentValue;
      this.initImages();
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  ngAfterViewChecked() {
    const newImageDimensions = this.imageRef.nativeElement.getBoundingClientRect();
    if (this.imageDimensions && (
      this.imageDimensions.left !== newImageDimensions.left ||
      this.imageDimensions.top !== newImageDimensions.top ||
      this.imageDimensions.right !== newImageDimensions.right ||
      this.imageDimensions.bottom !== newImageDimensions.bottom
    )) {
      this.imageCanvasService.adjustCanvas(this.imageContainerRef, this.imageRef, this.canvasRef);
      this.getClassifications().forEach(c => this.imageCanvasService.drawBounds(this.ctx, c, this.canvasDimensions));
    }

  }

  initImages(): void {
    if (!this.ctx) {
      this.ctx = this.canvasRef.nativeElement.getContext('2d');
    }
    this.imageIndex = 0;
    if (this.images.length > 1) {
      this.burst = true;
      this.sliderWidth = this.images.length * 160;
    } else {
      this.burst = false;
    }
    this.imageUrl = this.getImageUrl();
    this.tags = this.images[this.imageIndex].tags;

    for (const image of this.images) {
      image.classifications = image.classifications != null ? image.classifications : [];
      const colours = this.colourService.getColours(image.classifications.length);
      colours.forEach((colour, index) => image.classifications[index].colour = colour);
    }
  }

  prevMedia(e): void {
    this.prev.emit();
    this.stopPropagation(e);
  }

  nextMedia(e): void {
    this.next.emit();
    this.stopPropagation(e);
  }

  emitClose(e): void {
    this.close.emit();
    this.stopPropagation(e);
  }

  stopPropagation(e): void {
    e.stopPropagation();
  }

  onLoadImage() {
    this.imageDimensions = this.imageRef.nativeElement.getBoundingClientRect();
    this.imageCanvasService.adjustCanvas(this.imageContainerRef, this.imageRef, this.canvasRef);

    const exif = this.exifMetadata$;
    this.exifMetadata$.next(null);
    this.imageRef.nativeElement.exifdata = null;
    EXIF.getData(this.imageRef.nativeElement, function () {
      exif.next(EXIF.getAllTags(this));
    });

    this.getClassifications().forEach(c => this.imageCanvasService.drawBounds(this.ctx, c, this.canvasDimensions));
  }

  getBurstThumbnail(index: number): string {
    return `${this.imageServer}images/${this.images[index].imageRef}/r150x100`;
  }

  getExifThumbnail(thumbnail) {
    if ('blob' in thumbnail) {
      const thumbSubscription = this.preferenceApi.readFile(thumbnail['blob']).subscribe(content => {
        this.thumbnail$.next(content);
      });
      this.subscriptions.push(thumbSubscription);
    }
  }

  showImage(index: number): void {
    this.imageIndex = index;
    this.changeIndex();
    this.centerScroll();
  }

  changeIndex(): void {
    this.imageUrl = this.getImageUrl();
    this.tags = this.images[this.imageIndex].tags;
  }

  getImageUrl(): string {
    const imageRef = this.images[this.imageIndex].imageRef;
    return `${this.imageServer}images/${imageRef}`;
  }

  getClassifications(): ClassificationModel[] {
    return this.images[this.imageIndex].classifications;
  }

  getTimestamp(): string {
    return this.images[this.imageIndex].timestamp;
  }

  slideLeft(): void {
    if (this.imageIndex > 0) {
      this.imageIndex--;
      this.changeIndex();
      this.centerScroll();
    }
  }

  centerScroll(): void {
    const scrollEl = document.getElementById('media-slider');
    const sliderCenter = scrollEl.clientWidth / 2;
    const leftImages = this.imageWidths.slice(0, this.imageIndex);
    let width = leftImages.reduce((acc, val) => {
      return acc + val + 8;
    }, 0);
    width += (this.imageWidths[this.imageIndex] / 2);

    scrollEl.scrollLeft = width - sliderCenter;
  }

  slideRight(): void {
    if (this.imageIndex <= this.images.length) {
      this.imageIndex++;
      this.changeIndex();
      this.centerScroll();
    }
  }

  addTag(tag: string): void {
    const image = this.images[this.imageIndex];
    this.imageApi.addTag(image.streamKey, image.id, tag).subscribe(res => {
      this.images[this.imageIndex] = res.image;
      this.tags = res.image.tags;
    });
  }

  removeTag(tag: string): void {
    const image = this.images[this.imageIndex];
    this.imageApi.deleteTag(image.streamKey, image.id, tag).subscribe(res => {
      this.images[this.imageIndex] = res.image;
      this.tags = res.image.tags;
    });
  }

  openClassificationEdit() {
    this.classificationContext.emit({
      classifications: this.getClassifications(),
      image: this.images[this.imageIndex],
      imageIndex: this.imageIndex,
      imageUrl: this.imageUrl,
    });
  }

  deleteImage() {
    const dialogRef = this.dialog.open(ConfirmDeleteDialogComponent, {
      data: { message: 'Would you like to delete this image?', confirmName: false }
    });

    dialogRef.afterClosed().pipe(first()).subscribe(result => {
      if (result === true) {
        const image = this.images[this.imageIndex];
        this.imageApi.deleteImage(image.streamKey, image.id).pipe(first())
          .subscribe();
        this.images.splice(this.imageIndex, 1);
        if (!this.images.length) this.close.emit();
        else {
          this.imageIndex === 0 ? this.imageIndex = 0 : this.imageIndex--;
          this.imageUrl = this.getImageUrl();
          this.imageCanvasService.clearBounds(this.ctx, this.canvasRef);
        }
      }
    });
  }

  trackByFn(index, item) {
    return item;
  }

  classificationsVisible(visible: boolean) {
    visible ?
      this.getClassifications().forEach(c => this.imageCanvasService.drawBounds(this.ctx, c, this.canvasDimensions)) :
      this.imageCanvasService.clearBounds(this.ctx, this.canvasRef);
  }

}
