/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { MatButtonModule, MatInputModule, MatDialogModule } from '@angular/material';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faCheckSquare, faCircle } from '@fortawesome/free-regular-svg-icons';
import { faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { CardComponentsModule } from 'app/ui/card-components/card-components.module';
import { FormComponentsModule } from 'app/ui/form-components/form-components.module';

import { MediaGalleryComponent } from './media-gallery/media-gallery.component';
import { MediaModalComponent } from './media-modal/media-modal.component';
import { MediaThumbComponent } from './media-thumb/media-thumb.component';
import { MediaWidgetComponent } from './media-widget/media-widget.component';
import { ClassificationModalComponent } from './classification-modal/classification-modal.component';

@NgModule({
  imports: [
    CommonModule,
    FontAwesomeModule,
    FormComponentsModule,
    FormsModule,
    CardComponentsModule,
    MatButtonModule,
    MatInputModule,
    MatDialogModule
  ],
  declarations: [
    MediaGalleryComponent,
    MediaModalComponent,
    MediaThumbComponent,
    MediaWidgetComponent,
    ClassificationModalComponent
  ],
  exports: [
    MediaGalleryComponent,
    MediaWidgetComponent,
  ],
  entryComponents: [
    ClassificationModalComponent
  ],
})
export class MediaGalleryModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faChevronLeft, faChevronRight, faCheckSquare, faCircle);
  }
}
