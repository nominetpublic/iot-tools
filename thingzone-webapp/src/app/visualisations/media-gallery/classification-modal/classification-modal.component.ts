/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, OnInit, Inject, ViewChild, ElementRef, OnDestroy, AfterViewChecked } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { ClassificationModel } from '../models/classification-model';
import { fromEvent, Subscription } from 'rxjs';
import { ColourService } from 'app/core/services/api/colour.service';
import { SessionService } from 'app/core/services/session/session.service';
import { ImageApiService } from 'app/core/services/api/image-api.service';
import { first } from 'rxjs/operators';
import { ImageCanvasService } from 'app/core/services/visualisation/image-canvas.service';

@Component({
  selector: 'classification-modal',
  templateUrl: './classification-modal.component.html',
  styleUrls: ['./classification-modal.component.scss']
})
export class ClassificationModalComponent implements OnInit, OnDestroy, AfterViewChecked {

  @ViewChild('imagecontainer', { static: false }) imageContainerRef: ElementRef;
  @ViewChild('image', { static: false }) imageRef: ElementRef;
  @ViewChild('canvas', { static: false }) canvasRef: ElementRef<HTMLCanvasElement>;
  @ViewChild('label', { static: false }) labelInputRef: ElementRef;

  modifiedClassifications: ClassificationModel[];
  editable: ClassificationModel;

  mousex = 0;
  mousey = 0;
  last_mousex = 0;
  last_mousey = 0;
  selectionWidth: number;
  selectionHeight: number;
  mousedown = false;

  imageContainerDimensions;
  imageDimensions;
  canvasDimensions;
  ctx: CanvasRenderingContext2D;

  subscriptions: Subscription[] = [];


  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private imageService: ImageApiService,
    private colourService: ColourService,
    private sessionService: SessionService,
    private imageCanvasService: ImageCanvasService) { }


  ngOnInit() {
    this.modifiedClassifications = this.deepCopyClassifications(this.data.classifications);
    this.subscriptions.push(this.imageCanvasService.imageContainerDimensions.subscribe(r => this.imageContainerDimensions = r));
    this.subscriptions.push(this.imageCanvasService.imageDimensions.subscribe(r => this.imageDimensions = r));
    this.subscriptions.push(this.imageCanvasService.canvasDimensions.subscribe(r => this.canvasDimensions = r));
  }

  onLoadImage() {
    this.imageDimensions = this.imageRef.nativeElement.getBoundingClientRect();
    this.imageCanvasService.adjustCanvas(this.imageContainerRef, this.imageRef, this.canvasRef);
    this.ctx = this.canvasRef.nativeElement.getContext('2d');

    this.subscriptions.push(fromEvent(this.canvasRef.nativeElement, 'mousedown').subscribe(e => this.onMouseDown(e)));
    this.subscriptions.push(fromEvent(this.canvasRef.nativeElement, 'mousemove').subscribe(e => this.onMouseMove(e)));
    this.subscriptions.push(fromEvent(this.canvasRef.nativeElement, 'mouseup').subscribe(() => this.onMouseUp()));
    this.subscriptions.push(fromEvent(this.canvasRef.nativeElement, 'mouseenter').subscribe(e => this.onMouseEnter(e)));
    this.subscriptions.push(fromEvent(this.canvasRef.nativeElement, 'mouseleave').subscribe(e => this.onMouseLeave(e)));
  }

  ngAfterViewChecked() {
    const newImageDimensions = this.imageRef.nativeElement.getBoundingClientRect();
    if (this.imageDimensions && (
      this.imageDimensions.left !== newImageDimensions.left ||
      this.imageDimensions.top !== newImageDimensions.top ||
      this.imageDimensions.right !== newImageDimensions.right ||
      this.imageDimensions.bottom !== newImageDimensions.bottom
    )) this.imageCanvasService.adjustCanvas(this.imageContainerRef, this.imageRef, this.canvasRef);

  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  onMouseDown(e): void {
    if (this.editable) {
      this.last_mousex = e.clientX - this.canvasDimensions.left;
      this.last_mousey = e.clientY - this.canvasDimensions.top;
      this.mousedown = true;
    }
  }

  onMouseMove(e): void {
    if (this.editable) {
      this.mousex = e.clientX - this.canvasDimensions.left;
      this.mousey = e.clientY - this.canvasDimensions.top;

      if (this.mousedown) {
        this.imageCanvasService.clearBounds(this.ctx, this.canvasRef);
        this.selectionWidth = this.mousex - this.last_mousex;
        this.selectionHeight = this.mousey - this.last_mousey;
        this.imageCanvasService.drawDoubleRectangle(
          this.ctx, this.last_mousex, this.last_mousey, this.selectionWidth, this.selectionHeight, this.editable.colour
        );
      }
    }
  }

  onMouseEnter(e): void {
    if (this.editable) e.target.style.cursor = 'crosshair';
  }

  onMouseLeave(e): void {
    e.target.style.cursor = 'auto';
    this.onMouseUp();
  }

  onMouseUp(): void {
    if (this.mousedown && this.editable) {
      this.mousedown = false;

      /* Convert from pixels to image ratio */
      const bboxXmin = this.last_mousex / this.canvasDimensions.width;
      const bboxYmin = this.last_mousey / this.canvasDimensions.height;
      const bboxXmax = (this.last_mousex + this.selectionWidth) / this.canvasDimensions.width;
      const bboxYmax = (this.last_mousey + this.selectionHeight) / this.canvasDimensions.height;

      this.editable.selection = `bbox[${bboxYmin},${bboxXmin},${bboxYmax},${bboxXmax}]`;
    }
  }

  editClassification(classification: ClassificationModel) {
    this.editable = classification;
    this.imageCanvasService.clearBounds(this.ctx, this.canvasRef);
    this.imageCanvasService.drawBounds(this.ctx, classification, this.canvasDimensions);
  }

  newClassification() {
    this.imageCanvasService.clearBounds(this.ctx, this.canvasRef);

    const previouColour = this.modifiedClassifications.length ?
      this.modifiedClassifications[this.modifiedClassifications.length - 1].colour :
      null;

    const classification = {
      label: '',
      origin: this.sessionService.getUser(),
      timestamp: new Date(),
      confidence: 1,
      selection: '',
      curated: true,
      colour: this.colourService.getNext(previouColour)
    };

    this.modifiedClassifications.push(classification);
    this.editable = classification;
    setTimeout(() => this.labelInputRef.nativeElement.focus(), 0);
  }

  trackByFn(index, item) {
    return index;
  }

  acceptClassification(classification: ClassificationModel) {
    classification.curated = true;
    classification.confidence = 1;
    classification.origin = this.sessionService.getUser();
  }

  deleteClassification(index: number) {
    this.modifiedClassifications.splice(index, 1);
    this.imageCanvasService.clearBounds(this.ctx, this.canvasRef);
    this.editable = null;
  }

  saveClassifications() {
    this.imageService
      .updateClassifications(this.data.image.streamKey, this.data.image.id, this.modifiedClassifications)
      .pipe(first()).subscribe();
  }

  resetClassifications() {
    this.modifiedClassifications = this.deepCopyClassifications(this.data.classifications);
    this.imageCanvasService.clearBounds(this.ctx, this.canvasRef);
  }

  saveDisabled(): boolean {
    for (const classification of this.modifiedClassifications) {
      if (classification.selection.length === 0 || classification.label.length === 0) return true;
    }
    return this.changed();
  }

  changed(): boolean {
    if (this.data.classifications.length !== this.modifiedClassifications.length) return false;

    for (let i = 0; i < this.data.classifications.length; i++) {
      if (
        this.data.classifications[i].label !== this.modifiedClassifications[i].label ||
        this.data.classifications[i].selection !== this.modifiedClassifications[i].selection ||
        this.data.classifications[i].curated !== this.modifiedClassifications[i].curated
      ) {
        this.modifiedClassifications[i].curated = true;
        this.modifiedClassifications[i].confidence = 1;
        return false;
      }
    }
    return true;
  }

  private deepCopyClassifications(copyable: ClassificationModel[]): ClassificationModel[] {
    return JSON.parse(JSON.stringify(copyable));
  }
}
