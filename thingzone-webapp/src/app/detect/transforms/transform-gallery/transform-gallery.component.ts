/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { TransformApiService, TransformFunction } from 'app/core/services/api/transform-api.service';
import { ConfirmDeleteDialogComponent } from 'app/ui/form-components/confirm-delete-dialog/confirm-delete-dialog.component';

@Component({
  selector: 'transform-gallery',
  templateUrl: './transform-gallery.component.html',
  styleUrls: ['./transform-gallery.component.scss']
})
export class TransformGalleryComponent implements OnInit {
  transforms: TransformFunction[];
  selected: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    private transformApi: TransformApiService,
  ) { }

  ngOnInit() {
    this.transformApi.listTranformFunctions(true).subscribe(transforms => {
      this.transforms = transforms;
    });

    this.route.params.subscribe(params => {
      if ('name' in params) {
        setTimeout(() => {
          this.selectTransform(params.name);
        });
      }
    });
  }

  transformByName(index: number, tf: TransformFunction): string {
    return tf.name;
  }

  selectTransform(selected: string) {
    this.selected = selected;
    this.router.navigate([`./edit/${selected}`], { relativeTo: this.route });
  }

  newTransformFunction() {
    this.selected = null;
    this.router.navigate(['./new/'], { relativeTo: this.route });
  }

  deleteTransformFunction(name: string): void {
    const dialogRef = this.dialog.open(ConfirmDeleteDialogComponent, {
      data: { message: 'Would you like to delete this transform function?', confirmName: false }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.transformApi.deleteTransformFunction(name).subscribe(() => {
          this.transforms = this.transforms.filter((transform) => !(transform.name === name));
        });
      }
    });
  }

}
