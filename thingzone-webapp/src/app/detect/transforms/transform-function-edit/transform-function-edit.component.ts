/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TransformApiService, TransformFunction } from 'app/core/services/api/transform-api.service';
import { SyntaxEditorComponent } from 'app/ui/text-editor/syntax-editor/syntax-editor.component';
import { fromEvent, merge, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'transform-function-edit',
  templateUrl: './transform-function-edit.component.html',
  styleUrls: ['./transform-function-edit.component.scss']
})
export class TransformFunctionEditComponent implements OnInit, AfterViewInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  transformName: string = null;
  transform: TransformFunction;
  @ViewChild('codeWrapper', { static: true }) codeWrapper: ElementRef;
  @ViewChild('code', { static: true }) code: SyntaxEditorComponent;

  transformForm = this.formBuilder.group({
    name: [null, Validators.required],
    functionType: [null, Validators.required],
    functionContent: [],
  });
  wrapperHeight: number;
  error = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private transformApi: TransformApiService,
    private formBuilder: FormBuilder,
    protected cd: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.bindWindowResizeEvent();
    this.route.params.subscribe(params => {
      if (params.name != null) {
        this.transformName = params.name;
        this.loadTransform(this.transformName);
      }
      this.cd.detectChanges();
    });
  }

  ngAfterViewInit(): void {
    this.calculateHeight();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  loadTransform(name: string) {
    this.transformApi.getTransformFunction(name).subscribe(result => {
      this.transform = result;
      this.transformForm.patchValue(this.transform, { onlySelf: true, emitEvent: false });
    });
  }

  calculateHeight(): void {
    const wrapperHeight = this.codeWrapper.nativeElement.offsetHeight - 30;
    this.code.editorElement.setAttribute('style', 'height: ' + wrapperHeight + 'px');
  }

  private bindWindowResizeEvent(): void {
    const resize$ = fromEvent(window, 'resize');
    const panelResize$ = fromEvent(window, 'panelResize');
    const subscription = merge(resize$, panelResize$).pipe(debounceTime(200)).subscribe(() => {
      this.calculateHeight();
      if (this.cd) {
        this.cd.markForCheck();
      }
    });
    this.subscriptions.push(subscription);
  }

  selectTransform(selected: string) {
    this.error = false;
    this.router.navigate([`../${selected}`], { relativeTo: this.route });
  }

  onSubmit(): void {
    this.transformForm.get('functionContent').setValue(this.code.getContent());
    if (this.transformName != null) {
      const updatedFunction: TransformFunction = this.transformForm.value;
      if (updatedFunction.name !== this.transformName) {
        updatedFunction.updatedName = updatedFunction.name;
        updatedFunction.name = this.transformName;
      }
      this.transformApi.updateTransformFunction(updatedFunction).subscribe(
        () => { // success path
          if (updatedFunction.updatedName != null) {
            this.selectTransform(updatedFunction.updatedName);
          } else {
            this.selectTransform(this.transformName);
          }
        },
        error => { // error path
          this.loadTransform(this.transformName);
          this.error = true;
        }
      );
    } else {
      const newFunction: TransformFunction = this.transformForm.value;
      this.transformApi.createTransformFunction(newFunction).subscribe(
        () => { // success path
          this.selectTransform(newFunction.name);
        },
        error => { // error path
          this.error = true;
        }
      );
    }
  }
}
