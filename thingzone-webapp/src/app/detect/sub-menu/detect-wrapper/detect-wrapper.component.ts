/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RoutingLink } from 'app/core/models/common/routing-link';

@Component({
  selector: 'detect-wrapper',
  templateUrl: './detect-wrapper.component.html',
  styleUrls: ['./detect-wrapper.component.scss']
})
export class DetectWrapperComponent implements OnInit {

  routerLinks: RoutingLink[] = [
  ];

  currentRoute = '';

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.url.subscribe(url => {
      this.currentRoute = url[0].path;

      if (this.currentRoute === 'detection' || this.currentRoute === 'transforms') {
        this.routerLinks = [
          { title: 'Overview', link: '/detect/detection' },
          { title: 'Transform Functions', link: '/detect/transforms' }
        ];
      }
    });
  }

}
