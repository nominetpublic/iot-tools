/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PrivilegeGuard } from 'app/core/services/auth/guards/privilege.guard';
import { Privilege } from 'app/core/services/auth/privilege';

import { DetectWrapperComponent } from './sub-menu/detect-wrapper/detect-wrapper.component';
import { TransformFunctionEditComponent } from './transforms/transform-function-edit/transform-function-edit.component';
import { TransformGalleryComponent } from './transforms/transform-gallery/transform-gallery.component';

const routes: Routes = [
  {
    path: 'detection',
    component: DetectWrapperComponent,
    children: [
      {
        path: '',
        data: { widgetType: 'detect-events' },
        loadChildren: () => import('../visualisations/widget-gallery/widget-gallery.module').then(mod => mod.WidgetGalleryModule),
      },
    ],
  },
  {
    path: 'transforms',
    component: DetectWrapperComponent,
    children: [
      {
        path: '',
        canActivate: [PrivilegeGuard],
        data: { role: Privilege.CREATE_TRANSFORMFUNCTION },
        component: TransformGalleryComponent,
        children: [
          { path: 'new', component: TransformFunctionEditComponent },
          { path: 'edit/:name', component: TransformFunctionEditComponent },
        ]
      },
    ],
  },
  {
    path: 'events',
    data: { title: 'Events', filter: ['GENERIC', 'OBSERVATION'] },
    loadChildren: () => import('../alerts/alerts.module').then(m => m.AlertsModule),
  },
  {
    path: 'alerts',
    data: { title: 'Alerts', filter: ['ALERT'] },
    loadChildren: () => import('../alerts/alerts.module').then(m => m.AlertsModule),
  },
  {
    path: 'visualisations',
    component: DetectWrapperComponent,
    children: [
      {
        path: '',
        data: { visualisationType: 'detect-visualisations' },
        loadChildren: () => import('../visualisations/visualisation-gallery/visualisation-gallery.module')
          .then(mod => mod.VisualisationGalleryModule),
      }
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class DetectRoutingModule { }
