/*
 * Copyright 2020 Nominet UK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatInputModule } from '@angular/material';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faPlus, faTrash } from '@fortawesome/free-solid-svg-icons';
import { FormComponentsModule } from 'app/ui/form-components/form-components.module';
import { NavigationMenuModule } from 'app/ui/navigation-menu/navigation-menu.module';
import { TextEditorModule } from 'app/ui/text-editor/text-editor.module';

import { DetectRoutingModule } from './detect-routing.module';
import { DetectWrapperComponent } from './sub-menu/detect-wrapper/detect-wrapper.component';
import { TransformFunctionEditComponent } from './transforms/transform-function-edit/transform-function-edit.component';
import { TransformGalleryComponent } from './transforms/transform-gallery/transform-gallery.component';

@NgModule({
  declarations: [
    DetectWrapperComponent,
    TransformGalleryComponent,
    TransformFunctionEditComponent
  ],
  imports: [
    CommonModule,
    DetectRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatInputModule,
    FontAwesomeModule,
    NavigationMenuModule,
    FormComponentsModule,
    TextEditorModule
  ]
})
export class DetectModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faPlus, faTrash);
  }
}
