#!/usr/bin/env bash
# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


if [ $# -ne 2 ]; then
 echo "Need tag name and build number";
 echo "tz docker-push tag build_number";
 exit 1
fi

tagName=$1
buildNumber=$2

images=("thingzone-registry-postgres" \
"thingzone-registry-api" \
"thingzone-imageserver" \
"thingzone-bridge-payload-dispatcher" \
"thingzone-bridge-storage-worker" \
"thingzone-bridge-registry-worker" \
"thingzone-bridge-push-worker" \
"thingzone-bridge-transform-worker" \
"thingzone-bridge-transform-output-dispatcher" \
"thingzone-bridge-http-upload" \
"thingzone-javascript-sandbox" \
"thingzone-data-store" \
"thingzone-bridge-docker-kafka-observer" \
"thingzone-webapp" \
"thingzone-image-classifier" \
"thingzone-docker-utils" )

versionRx='^(v[0-9]+\.+[0-9])$'


if [[ $tagName =~ $versionRx ]] || [[ $tagName = "master" ]]; then
    echo "Pushing tag: $tagName build number: $buildNumber"

    for image in "${images[@]}"
        do
           docker tag $image:latest $NEXUSURL/thingzone/$image:${tagName}
           docker tag $image:latest $NEXUSURL/thingzone/$image:latest

           if [[ $tagName =~ $versionRx ]]; then
               docker tag $image:latest $NEXUSURL/thingzone/$image:${tagName:1}.$buildNumber
           fi

           docker push $NEXUSURL/thingzone/$image
        done
else
    echo "No images to push for this branch"
fi
