#!/usr/bin/env bash
# Copyright 2020 Nominet UK
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

echo "Arguments:  $@"

# base init
docker-compose up -d cassandra elasticsearch kafka registry utils && \
docker-compose exec utils ./init_cassandra.sh && \
docker-compose exec utils ./init_elasticsearch.sh && \
docker-compose exec utils ./init_kafka.sh && \
docker-compose exec utils ./init_registry.sh environments/base/config.json

for var in "$@"
do
    docker-compose exec utils ./init_registry.sh environments/${var}/config.json environments/${var}/data.csv
done
