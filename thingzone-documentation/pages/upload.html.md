---
title: Upload
language_tabs:
  - python
toc_footers: []
includes: []
search: true
highlight_theme: darkula
headingLevel: 2

---

<h1 id="transform-api">Upload</h1>

## Http Upload 

Http Upload is the basic method for getting data into the system (note you can write custom 'Connectors' for integrating with other methods). It allows to upload input data into a defined data stream. Input data can be in the following formats:
* raw data stream
* events 
* alerts


See [here](bridge.html.md) for how to use the http upload API. In the following, the accepted raw data,
 event, alert formats are defined.


## Event and Alert Format
An event is defined as follows:

|Name|Type|Required|
|---|---|---|
|streamKey|string|true|
|klass|uk.nominet.iot.model.timeseries.Event|false|
|timestamp|string|true|
|metadata|dictionary|false|
|quality|list|false|
|tags|list|false|
|combinedPoints|list|false|
|name|string|false|
|description|string|false|
|type|string|false|
|eventType|GENERIC/ ALERT|true|
|category|string|false|
|startTimestamp|string|false|
|endTimestamp|string|false|
|location|geojson|false|
|receivers|list|false|
|feedback|list|false|
|severity|float|false|
|status|string|false|
|value|string|false|
|relatedTo|list(relatedTo-Format)|true|


###relatedTo format
|Name|Type|Required|
|---|---|---|
|type|string|false|
|klass|uk.nominet.iot.model.registry.RegistryDevice|true|
|description|string|false|
|key|device_id/ stream_id|true|




Example:
```python
event = {'streamKey': 'harsh_deceleration_event',
          'klass': 'uk.nominet.iot.model.timeseries.Event',
          'timestamp': "2019-10-20T13:14:15",
          'metadata': {},
          'quality': [],
          'tags': [],
          'combinedPoints': [],
          'name': 'harsh_deceleration',
          'description': 'gps_error_event',
          'type': 'instant',
          'eventType': 'GENERIC',
          'category': 'security',
          'startTimestamp': "2019-10-20T13:14:15",
          'endTimestamp': "2019-10-20T13:14:15",
          'location': geojson.Point((-1.2467, 52.4569)),
          'receivers': [],
          'feedback': [],
          'severity': .5,
          'status': '',
          'value': 10,
          'relatedTo': [
              {
                  "type": "relatedToVehicle",
                  "description": "Event about vehicle",
                  "klass": "uk.nominet.iot.model.registry.RegistryDevice",
                  "key": vehicle_id
               },
              {
                  "type": "relatedToThreat",
                  "description": "Event about threat",
                  "klass": "uk.nominet.iot.cyber.risk.Event",
                  "key": config.gps_spoofing_threat_id
              }
          ]
          }
```

