---
title: Fleet Management
  
toc_footers: []
includes: []
search: true
highlight_theme: darkula
headingLevel: 2

---
<h1 id="mapper">Case Study: Fleet Management</h1>

A fleet management model is presented here to show how a fleet of vehicles 
can be continuously monitored and analysed using Thingzone.

1. To initialise the system, the first step is to add assets and their data streams.

    * add a new asset

        |Device Information | |
        |---|---|
        |key|fleet-sim-car1|
        |name|FLEET-SIM-CAR1|
        |type|SIMCAV|

    * add new data streams

        __example 1__
        
        |Stream information| |
        |---|---|
        |key|fleet-sim-car1.velocity|
        |name|Velocity|
        |Stream Type|Double|
        |metadata|{unit:m/s, content_type:application/json}|
        
        __example 2__
        
        |Stream information| |
        |---|---|
        |key|fleet-sim-car1.events|
        |name|Vehicle Events|
        |Stream Type|Events|
        |metadata|{unit:event, content_type:application/json}|

2. Set up http upload on assets to post data to *Thingzone* platform

    * for instance, to send a velocity value of 50 from *fleet-sim-car1* to *Thingzone*, 
    we need to form a message, request a session key from *registry*, and post the message to 
    *Thingzone*:

```python
      data = {'streamKey': 'fleet-sim-car1.velocity',
              'klass': 'uk.nominet.iot.model.timeseries.MetricDouble',
              'timestamp': currentTime,
              'id': str(uuid.uuid1()),
              'value_double': 50.0}
      
      login_data = {"username": USERNAME, "password": PASSWORD}
      login_headers = {"content_type": "application/x-www-form-unlencoded"}
      login_r = requests.post(REGISRTY_URL, data=login_data, headers=login_headers, proxies=proxies)
      response = login_r.json()
      session_key = response['sessionKey']

      headers = {'content-type':'application/json',
              'X-Introspective-Session-Key': session-key}
  
      requests.post(BRIDGE_URL/'fleet-sim-car1.velocity', data=data, headers=headers)
```
   for more detail on the bridge API, see [here](./bridge.html)     

3. To monitor the received messages, a visualisation over desired data streams should be set up in the frontend.
    * a simple setup for monitoring the location, velocity, signal strength, and events for all vehicles is shown here:

    ![visualisation setup](/source/images/fleetVisaulisation.png)

  