---
title: System Architecture
toc_footers: []
includes: []
search: true
highlight_theme: darkula
headingLevel: 2

---

<h1 id="architecture">Data Structures</h1>

## Event
> Sample event
```json
{
    "name": "event123",
    "description": "event description",
    "type": "event type",
    "eventType": "GENERIC",
    "category": "category1",
    "timestamp": "2020-04-16T13:27:32.636Z",
    "startTimestamp": "2020-04-16T13:27:32.636Z",
    "endTimestamp": "2020-04-16T13:27:32.636Z",
    "location": {
        "type": "Point",
        "coordinates": [0.0, 0.0, 0.0]
    },
    "receivers": ["user1", "user2"],
    "configuration": {
        "foo": "bar"
    },
    "feedback": [{
        "id": "3bf7dfb2-08ca-4da2-8fd9-2db63f6f5ce0",
        "timestamp": "2020-04-16T13:27:32.620Z",
        "user": "user1",
        "type": "feedback type",
        "category": "category1",
        "description": "description"
    }],
    "severity": 0.5,
    "status": "OPEN",
    "value": 0.5,
    "relatedTo": [{
        "type": "associated image",
        "description": "relation description",
        "klass": "uk.nominet.iot.model.timeseries.Image",
        "key": "4ee6c83e-5960-4441-9a09-f923fb886e66"
    }],
    "derivedFrom": [{
        "type": "origin alert",
        "description": "relation description",
        "weight": 0.4,
        "klass": "uk.nominet.iot.model.timeseries.Event",
        "streamKey": "streamkey123",
        "id": "42fb7b29-906f-486f-8eb6-13c14d0fcce1"
    }],
    "month": 0,
    "klass": "uk.nominet.iot.model.timeseries.Event"

```
The properties of an event are defined as follows:

* `name` the name associated with the event
* `description` a description of the event
* `type` type of event defined by user
* `eventType` the internal event type. Allowed values are: `GENERIC`, `OBSERVATION `and `ALERT`
* `categoty` the category of the event 
* `timestamp` the timestamp used to characterise and event in a timeseries
* `startTimestamp` time when the event started
* `endTimestamp` time when the event ended
* `location` a geojson geometry representing the location of the event
* `receivers` the username of the people receiving notification of an event
* `configuration` the configuration parameters of the notification
* `feedback` a list of user feedback entries
* `severuty` the severity of the event from 0 to 1
* `status` a label representing the status of the event e.g. `open`, `closed`
* `value` a numerical value associated with the event
* `relatedTo` a list of links to other related entities in the system
* `derivedFrom` a list of links to other entities of the system that cause the event to happen
* `month` used for internal partitioning
* `klass` internal representation of the Event object
 
## Metric
TODO

## Image
TODO

## Json
TODO

## Config
TODO

