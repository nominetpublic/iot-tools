---
title: Transforms
language_tabs:
  - javascript--nodejs: Node.JS
toc_footers: []
includes: []
search: true
highlight_theme: darkula
headingLevel: 2

---

<h1 id="transform-api">Transforms</h1>

# Requirements
* node8.11.2

# Transform Configuration
A transform is a special kind of data stream that defines one to many input streams and one to many output stream. 
A transform applies a function to the input data generated zero to many output payloads.  

## Parameter Values
Parameters are static input values that are passes to the function when the transform is triggered

## Sources
Sources defines the input data streams for the transform. Sources have the following parameters:
* alias: used as an internal reference for the function to access the data associated with the source data stream
* source: the key of the input data stream
* aggregationType": (optional) type of aggregation. Allowed values: "basic"
* aggregationSpec": (optional) specified how much data to aggregate before the function is executed 
    - lastN: get the last n elements from the timeseries e.g. "lastN(100)"
    - lastT: get elements from time to now e.g. "lastT(2020-02-12T09:58:09.090Z)"
* aggregation: (optional) specified how much data to aggregate before the function is executed e.g. "trigger-1w5p".  
When omitted the transform only collects one payload.
* trigger: true / false, when set to true the transform will trigger every time a new payload is added to the source stream

## outputStreams
Defines additional output streams for the transform. The following parameters are defined:
* alias: used internally by te function to identify the output stream
* stream: the key of the output stream


# Transform Functions 

## Reading transform parameters
Parameters are accessible via the `parameters` object by their specific name.

```javascript
var myParameter1 = parameters.myParameter1;
var myParameter2 = parameters.myParameter2;

```

## Reading input data
Input data is available by accessing the `sources` object followed by the data stream alias.

```javascript
var temperatureReading = sources.temperature;
````
 
## Transform stream output
The output associated with the transform stream itself has to be assigned to a special variable: `result.transformStream `.
The output has to be structured according to any of the subclasses of `AbstractTimeseriesPoint`.
It is possible to use the `Point` javascript module to facilitate the creation of such objects. 

```javascript
result.transformStream = Point.metric(...);
````

## Additional stream output
Additional datastreams are accessible via the variable `result.streams` followed bu the alias of the output stream. 

```javascript
result.streams.battery  = Point.metric(...);
````

## Transform Example
> Logging

```javascript
result.log.info('Hello World!');
result.log.info(JSON.stringify(sources.foo));
result.log.error(JSON.stringify(parameters.foo));
```

> Accessing input sources

```json
{
 "foo": {
    "streamKey": "foo",
    "timestamp": "2018-10-03T08:51:47.163Z",
    "pending": false,
    "value": {
        "contentType": "application/json",
        "streamKey": "stream1key",
        "timestamp": "2018-10-03T08:51:47.163Z",
        "klass": "uk.nominet.iot.model.timeseries.MetricDouble",
        "id": "ebcf3a73-f0a5-4303-a627-db115e5984bb",
        "metadata": {}
    }
   },
  "bar": {
    "streamKey": "bar",
    "timestamp": "2018-10-03T09:19:09.814Z",
    "pending": false,
    "value": {
        "contentType": "application/json",
        "streamKey": "stream2key",
        "timestamp": "2018-10-03T09:19:09.814Z",
        "klass": "uk.nominet.iot.model.timeseries.MetricDouble",
        "id": "4041e466-8cae-42a7-9463-5cfd0f4f9f6c",
        "metadata": {}
    }
   }
}
```

> Accessing parameters

```json
{
  "foo": 10,
  "bar":0.5
}
```
> Assigning value to transform stream

```javascript

var a = sources.foo;
var b = sources.bar;
var m = metadata.x;

var c = a + b;

result.transformStream = Point.double(...);
result.streams.foo = Point.text("hello");
```

## Error reporting and logging
Error reporting and is possible via two utility functions: `result.log.error(content)` and  `result.log.info(content)`;
Each time the function is invoked the content is appended to a log list structure as part of the transform results.


```javascript
result.log.error("Could not classify image: " + error); 
result.log.info("Processed event data, predictions");
````


## Promises
If the transform function needs to make asyncronous calls, it is necessary to use the `promise` object e.g. `promise.then() -> { return ...});`.
If the result object is updated as part of the asyncronous code, the final result is used and the output of the transform after the root promise is fulfilled. 

```javascript
var base64Data = sources.someStream.base64Data;
var buff = Base64.toBuffer(base64Data);

promise.then( () => {
    return AsycLib.doStuff(buff).then(
        predictions => {
            result.streams.outputStream = Point.metric(new Date(), "label", {predictions: predictions});
        },
        error => {
            result.log.error("Some error: " + error);
        });
});
```` 
