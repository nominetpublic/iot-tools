---
title: About
  
toc_footers: []
includes: []
search: true
highlight_theme: darkula
headingLevel: 2

---
<h1 id="background">Background</h1>

The tools have been built for R&D projects which have strong geo-spatial requirements to them and are suited to 
Smart City applications. They originated with supporting the work of a community flood sensor project which 
involved deploying individual low cost LoRa flood sensors throughout the city waterways.
The system has since evolved significantly and been applied to car parking sensors, satellite based camera traps, 
solar powered water pumps, city transport systems and even autonomous vehicles. So it has gone from data collection 
from small, static, battery powered devices to analysing the routes of large moving complex vehicles. 
  
Experience from real on-the-ground projects have informed development throughout the current lifetime of the 
codebase. Some of the data collection aspects of the system have been running for over 5 years. 
  
Note we called the overall package IoT Tools because they were there to support research activities, 
not be the research itself. As with all software that has been built over time there are plenty of aspects we would have liked to have improved, 
tested further and definitely documented before now.
 
Also note the codebase uses a simple codename for the tools - Thingzone. 

For more information on Nominet research topics and news see nominet.uk. 

<h1 id="contributors">Contributors</h1>

A number of people have worked on the 
development of this codebase over the last several years but contributors to the initial release of this open source version include:

Edoardo Pignotti

Julia Altenbuchner

Maryam Kamali

James Wilkinson

Bryan Marshall

