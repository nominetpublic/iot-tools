---
title: Creating devices, datastreams, transforms and events with the UI
  
toc_footers: []
includes: []
search: true
highlight_theme: darkula
headingLevel: 2

---

<h1 id="deployment">Creating devices, datastreams, transforms and events with the UI</h1>

This guide will take you through setting up assets (devices and datastreams) in the user interface webapp.

The IoT Tools provide a back end service to send data from remote systems. There are options for two way 
services but to simplify matters let's consider a device to cloud data flow first. 

*Devices* - the ‘thing’ in the field. Before starting your configuration it is worth asking a number of simple questions.
Is the device static or does it move a lot? What types of data does it send? Are many data streams bundled into one packet of data or broken out into individual streams. How is it 
connected to internet services? 

For instance, this could be a battery powered air-quality monitoring device in the field. It sends 
monitoring data every 15 minutes and bundles values for CO2, NO and NO2 
levels into a single Json payload. The data is sent via a LoRaWAN public network back to a publishing service end 
point (running an automated service that can call HTTP requests). Every hour it also sends a battery level value so that the maintenance 
teams can monitor whether it needs servicing. 

For our example let’s say the device is set up near a roadside in the city of Oxford with location lon: -1.257 lat: 51.751. 

The second thing to consider is who are the *users* of the system? As an example you will probably have system 
admins, application owners (the people who are interested in the air quality levels) and maintenance teams who 
are responsible for keeping the equipment running. Permissions can be set of on a per device basis to control who can see and do what to devices and datastreams. 
However, for the sake of this guide we'll assume an admin user throughout to keep it simple. 

## Setting up the application

It is assumed that you have an instance of the tools running as described in the deployment notes. 
Note you can customise your application to have its own login logo, splash screen and introduction text. Just replace the default files in 
/config/webapp. 


## Basic application configuration

Once logged into the application as admin go into the **SETTINGS** menu on left hand side of the screen (cog icon). 

Settings give you a control over a number of cross-application settings including user management and application 
resources. 

In this instance go to **Resources** along the top menu bar and update default map settings for status and alerts.  
These settings enable some of the maps used in the interfaces to have useful starting locations. 
You can centre the maps on our device at lon: -1.257 lat: 51.751. Throughout the toolkit, unless lon: lat: is separately specified, assume the order is lon, lat.

Note in the resources area you can also load up icons for use in the application. If adding a new icon make sure 
to give it the name  “icon.[name]” and use the . to create nested structures such as icon.warning.high.

## Creating a device

Now go to the **ASSETS** submenu and the **Asset Configuration** page. Note the default view lands on the **Overview** top menu item 
which can be configured with dashboard 'widgets' to help summarise information about the assets.   
Switch to **Config** top menu option. This is where assets such as devices and data streams are created and configured.

So start with creating a device (top right button) and a new device sub menu appears.

You can either create a key for the device or let the system create a unique one. 

Let’s call ours *airqualityunit1* (note lower case always used for keys) and give it a description name *Air Quality Unit 1*.

Type is used for filtering so create something usable for the application - in this case *aqu*.

```
{ 
   modelnumber : ABCDEFGH
}
```

The metadata box enables a very flexible description of the system to fit the device. It uses Json format so let’s 
adds something like modelnumber to use across devices (metadata shown in right window).  

Next set the location of where the device has been deployed using the map widget. Click on the dot to make it go 
red and move to where you want it go. Alternatively you can add via Longitude and Latitude settings. Note, as with 
a lot of the system, this aspect is very flexible and powerful and allows you to create geo-Json based objects shapes, not 
just points. This means your *device* could be a representative zone where you are pulling in data from an external 
source or where you want to hide the exact position of the device. 

Next set the *permissions* the user has over this device. Click on the lozenges to turn them green. In this case 
mark all the lozenges ADMIN, MODIFY, DISCOVER, METADATA, LOCATION. Here you could add another user which only has, say, 
DISCOVER permissions and therefore would not be able to change settings of the device. Also note 
permissions are generally best administrated in Groups (which are set in the window below users) rather than individual users. 

Click update at the top of the page to ensure the data is saved and device created. Note, there is currently a lag 
in time between when the device is created and when it appears in the list on the left. The device has been 
registered with the registry but the worker job in the background needs to sync between the registry and datastore 
which is where the UI gets its data from. 

## Adding Datastreams

Now lets add some datastreams to the device. In our example the device sends a monitoring message every 15 minutes and a battery status update every hour. So 
we need two input streams, let’s call them 

`airqualityunit1.monitoring`
  
`airqualityunit1.batterylevel`

It’s important to note that these are two different types of streams. The first will be a Json payload that 
contains 3 different values - say CO2,NO and NO2. The second is a direct value - *batterylevel*.  Note also the use of lower case 
names for streams.

Let's start by creating the monitoring stream. Click on *add new data stream* in the device configuration for 
*Air Quality Unit 1*

* Use the settings:
    
    |setup parameters| |
    |---|---|
    |key| airqualityunit1.monitoring|
    |name| AQU1 Monitoring Stream| 
    |streamtype| JSON|

Metadata can be added in a similar manner as devices but we will leave that this time around. 

In the *user permissions* make all the lozenges green

Save changes and return to device. Repeat process for *batterylevel* but make the ```streamtype: Decimal``` 
(note the API uses the model type MetricDouble in the background). 

The *monitoring* stream will need unpacking into 3 different datastreams if we wish to do any analysis and 
visualisation on them. We’ll go through the steps on how to do that a little later but first we need to create 
an additional 3 streams. 

Lets call them 

`airqualityunit1.xco2`

`airqualityunit1.xno`

`airqualityunit1.xno2`

We have used an x just to denote that these streams will be ‘extracted’ from the incoming packet for clarity 
only, it is not a requirement. 
Create these streams in the similar way to above using the DECIMAL type. 

```
https://myisntancethingzone.uk/http-upload/api/v1/upload/airqualityunit1.batterylevel
```

We now have 5 data streams associated with the device with addressable names so an API address to upload some 
data could be:

    {
    	“value_double” : 5.5, 
    	"klass": "uk.nominet.iot.model.timeseries.MetricDouble
    }


In an application such as Postman, you could try to upload a value to the `batterylevel` using a message body of:

Note, you need to include the correct stream ‘klass’ in the payload.

 
## Unpacking a message with Transforms

Transforms are a very powerful part of the toolkit. They enable sandboxed javascript code to run on input 
datastreams. 

There are some key things to remember about transforms. They are effectively another stream in the system so 
output can be written either to the stream itself or to some output streams defined in the configuration.  
They take inputs, either datastreams or set parameters. 

To set up a transform you first need a transform function in place. These can be found under 
**EVENT DETECTION** and top menu option **Transform Functions**. 

Click on the + New Transform Function button at the bottom of the list. 

Create a name such as `MetricExtractionFnc` and write `javascript` into the Type box (javascript the only option at the moment but this could be extended 
into other languages in the future). 

```javascript
    var report = sources.report.value_json
    
    //useful logging 
    result.log.info(JSON.stringify(report));
    
    //note you can generate your 
    var timestamp = new Date();
    timestamp.setTime(report.timestamp * 1000);
    
    result.streams.out1 = {
        "value_int" : report.CO2level,
         //"timestamp": timestamp,
        "klass": "uk.nominet.iot.model.timeseries.MetricDouble",
        "tags": ["tag1", "tag2"]
    };
    
    result.streams.out2 = {
        "value_double" : report.NOlevel,
        //"timestamp": timestamp,
        "klass": "uk.nominet.iot.model.timeseries.MetricDouble",
        "tags": ["tag1", "tag2"]
    };
    
    result.streams.out3 = {
        "value_double" : report.NO2level,
        //"timestamp": timestamp,
        "klass": "uk.nominet.iot.model.timeseries.MetricDouble",
        "tags": ["tag1", "tag2"]
    };
```

Copy in the code in the right window: 

Note ```report``` and ```out1 etc``` are ‘aliases’ set up in the transform itself. 

If you wanted to write directly to the transform stream use ```result.transform``` instead of ```result.streams.out```

Save this and move onto creating the transform stream. 

Go back to the device config for `airqualityunit1` and scroll down page to find Transforms and click on the plus button 
next to it. 

First create the transform key, for instance `airqualityunit1.monitoringextraction` and give it a name.

As the transform stream isn’t being used as the output here, the Stream Type is not critical but set it to EVENT 
as we will use this stream as an event stream later.

In Transform Details set the function name we created before - `MetricExtractionFnc`.

Parameter values are not required in this case but you could use this as threshold value or something similar to 
used in the function. 

Add a *TRANSFORM INPUT SOURCE*. Call the Alias `report` and Source Key `airqualityunit1.monitoring`

The transform will want to trigger when a monitoring report arrives so tick the trigger option. 

Then add 3 OUTPUT STREAMS with Aliases out1/out2/out3 corresponding to Stream Keys 

`airqualityunit1.xco2`

`airqualityunit1.xno`
    
`airqualityunit1.xno2`
    
In user permissions tick all the lozenges for the moment. 

Save changes. The transform is now ready for data! 

You can do this by uploading a message payload to the API at  

`https://myisntancethingzone.uk/http-upload/api/v1/upload/airqualityunit1.monitoring`

```
{ 
    “value_json”: 
	{
            “CO2level” : 10.1,
            “NOlevel” : 5.785,
            “NO2level” : 20.5
            “klass” : “uk.nominet.iot.model.timeseries.Json”
	}
}
```
The payload could look something like (numbers are for illustration only):

You can trace the progress of your payload through the system using the ‘queues’ tool running at 

`https:myisntancethingzone.uk/queues`

(If configured to run)

Here you should see the payload land in the connector queue, which then sends the data into the defined Json 
Timeseries stream. 

At this point a transform task should trigger and a transform output be generated. The data from the output 
then also lands back into the Timeseries queue

A big note with transforms is that if you use more than one input source make sure your dependent input sources 
have data pre-loaded! This is a streaming system and requires data to be available in all streams. Adding 
resilience is something that is needed in future. 


##Creating a Transform to trigger an Event

Lets say we wanted our system to alert us if the CO2 level went above 20. We should use the events system. 
Once again this is highly configurable so we will need to keep to a simple example. 

In the previous **Device Config** Transforms window create a Parameter to be *CO2threshold*  and value 20. 

    var threshold = parameters.CO2threshold;

In the previous transform function add the following code at the start:


    if(report.CO2level >= threshold)
    {
        result.transformStream = {
        
        "name": "CO2 Threshold Issue",
        "description": "event description",
        "type": "testevent",
        "eventType": "GENERIC",
        "category": "category1",
        "location": {
            "type": "Point",
            "coordinates": [0.0, 0.0, 0.0]
        },
        
        "severity": 0.5,
        "status": "open",
        "value": 0.5,
        //"timestamp": timestamp,
        "klass": "uk.nominet.iot.model.timeseries.Event",
        "tags": ["tag1", "tag2"]
        };
    };

.and this at the end:



Note the event writes to *result.transformStream* which is the transform stream itself which we defined as an 
event stream earlier.

Save that and try to send some data which kicks in the threshold trigger. You should see an event posted in 
the **EVENTS** page list. 

In the Events UI you can filter by name, type and eventType and it useful to understand how best to structure 
the event names based on the UI interface. For instance a benefit of using the eventType `ALERT` is that it currently gets 
a dedicated page in the the current UI. 

Severity goes from 0-1 and colours the indicator dot on the map and next to the event in the list. 

* GREY : 0 < 0.3
* ORANGE : 0.3 < 0.6
* RED : 0.6 1.0

A big point to note also here is that the location of the device isn’t known. This is because a datastream could 
be linked to a number of devices or none at all so there is no simple 1:1 correspondence. A way around this is 
to create a location stream based on device location and use that stream as an input. It is not ideal but a number of users want their device location
in a stream to help with history and visualisation anyway.


##Using the Config datastream

The datastream type Config (uk.nominet.iot.model.timeseries.Config) is there to help with devices that require a remote configuration file. 

For instance a very simple config file could look like: 

```
{
    "name" : "Dave",
    "lon" : 0.0,
    "lat" : 0.0,
    "version" : 1,
    "opmode" : "deployed",
    "batterylowthreshold" : 3.5
    "klass" : "uk.nominet.iot.model.timeseries.Config"  
}
```

When this datastream type is created, the UI can provide more user-friendly buttons and sliders to adjust values rather then
directly editing metadata. 

To be able to do this first generate a valid schema for your config files and create a Config datastream (in similar manner as described previously).

In the datastream add a 'resource'. Give it a name, say, `configUISchema`.

Add the Json schema in the box below. 

Add in the metadata of the datastream `“schema” : “configUISchema”` and click the save icon.

Save the changes to the datastream itself. To see the interactive form go to Config Settings in datastream asset view and update the data in the form as required.
