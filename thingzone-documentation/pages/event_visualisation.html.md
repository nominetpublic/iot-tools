---
title: Event Visualisation
  
toc_footers: []
includes: []
search: true
highlight_theme: darkula
headingLevel: 2

---
<h1 id="mapper">Visualisations</h1>

As with many of the components of the IoT Tools the Visualisation system is highly configurable and very powerful once you get the hang of it.
Visualisations come in two forms: 

* Widgets - these are used in the OVERVIEW menu sections to dashboards
* Full page visualisations

They both use the same basic components although the main difference between widgets and visualisations is in the control of time window. Widgets are there to show latest information since the last 
x number of hours/days/weeks etc whereas visualisations are used for forensic examination of data over any period of time. 

##Overview of Visualisations

Visualisations are made up of core components: 

* Map
* Line Graph
* Bar Graph
* Gauge
* Pie Chart
* Scatter Graph
* Gallery
* Value

Data queries are added to each component and multiple queries can be added to the same component. Based on the selected display
type, the available query type is shown. For instance, if the display type Gallery is
selected, the query type will be "Images".  

Components are assigned to the screen layout which can be divided up as: 

* Single Display
* 2 Columns
* Horizontal Split

In any one section, multiple components can be added so you can stack up, say, a set of line charts. 

##Some key points to remember when building Visualisations

Device filters are all important. Build your filters in the ASSETS section. Note, if your filter has, say, three devices in it, but only two with the same 
name of datastream, it will not be possible to display the datastream of any of the devices. Make sure your device filters gather similarly configured devices together. 
 
Data won't show unless the date range is correct. If something is not appearing double check your dates and the position of the time slide
at the bottom of the screen. 

Time periods entered into forms are represented by using a suffix to the number e.g. 5s- seconds / m - minutes / h - hours / d - days / w - weeks / y -years 

Component names are useful - they can appear in hover-over boxes so make sure you fill them out. 

If you are displaying a long list of geo-spatial data, it will only display the max number of objects set in the form. 

If you want to draw a moving vehicle as an icon but also have a trailing set of data points, build two queries. One for the vehicle only 
using the latest data point and one query for the data trail. 

Events in scatter graphs use the 'value' field in the event for y position. 

##Exporting and importing Visualisation Templates
Building visualisations can take time so once you have found a good set up you can export your configuration as a template which can then 
be imported into other visualisations. 

