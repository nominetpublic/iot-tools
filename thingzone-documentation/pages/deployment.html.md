---
title: Deployment
  
toc_footers: []
includes: []
search: true
highlight_theme: darkula
headingLevel: 2

---

<h1 id="deployment">Deployment</h1>

It is recommended to use Docker for deploying the iot-tools. In the root of the project there is a sample docker compose file (`docker-compose.yaml`)  that can be used as a starting point for deployment.

### Configuration
Configuration files for a docker-compose based deployment are available under the `config` folder.

#### Webapp configuration
All the relevant configuration for the webapp are available under config/webapp.
In order to have full access to the maps in the webapp it is necessary to obtain a valid mapbox token https://www.mapbox.com/ and a link for dark and light map styles.
The webapp configuration file `config/webapp/config.json` needs to be updated with the appropiate mapbox token and styles.  

The `config/webapp` folder also contains the default images and logos shown in the login and welcome screen. Those can be changed by replacing the relevant image files in the `config/webapp` folder.


