---
title: System Architecture
toc_footers: []
includes: []
search: true
highlight_theme: darkula
headingLevel: 2
---

<h1 id="architecture">Services</h1>

![services](source/images/services.png)

## Databases
### Cassandra
Holds long term raw timeseries data such as metrics, json points, image references, events and configurations.
[Cassandra website](http://cassandra.apache.org/)

### Postgres
Contains device identities and metadata and all of the associated data streams information for the registry.
[Postgres website](https://www.postgresql.org/)

### Elasticsearch
Elasticsearch is uses to keep a cache of the registry data and the raw timeseries data to support data search and visualisations in teh frontend.
[Elasticsearch website](https://www.elastic.co/elasticsearch/)

## Messaging

### Zookeeper
Apache Zookeeper is a centralised service for managing distributed services such as Apache Kanfa.
[Zookeeper website](https://zookeeper.apache.org/)

### Kafka
Apache Kafka is a distributed streaming platform used for exchanging data between the IoT platfiorm data services.
[Kafka website](https://kafka.apache.org/)

## Monitoring
### Kibana
Kibana is a tool for exploring and visualising elasrticsearch data.
[Kibana website](https://www.elastic.co/kibana)

### Queue-observer
A custom tool for exploring the content of the kafka topics used to exchange messages between the data services.
*Codebase:* `[ROOT]/thingzone-streaming-bridge/thingzone-bridge-docker-kafka-observer`

### Dozzle
A real time log viewer for Docker.
[Dozzle website](https://github.com/amir20/dozzle)

## Data Services
### Registry
A service to manage device metadata, user identity, access control and application configuration and resources.
*Codebase:* `[ROOT]/thingzone-registry`

### Imageserver
A repository for large collection of images. Provides an API for uploading, resizing and downloading images with multiple formats.
*Codebase:* `[ROOT]/thingzone-imageserver`

## APIs

### Datastore
API used to search and aggregate the data processed by the system. *Codebase:* `[ROOT]/thingzone-datastore`


## Frontend
### Webapp
The webapp provides a web-based user interface to the various APIs and services. It is highly configurable and built around the concept of 
assets (devices and datastreams), events and alerting, visualisations and dashboards.  *Codebase:* `[ROOT]/thingzone-webapp`


## Utility
### Utils
A collection of utilities for initialising configuring and maintaining the databases and services. *Codebase:* `[ROOT]/thingzone-data-tools/thingzone-docker-utils`

<h1 id="architecture">Bridge Data Processing</h1>

![bridge](source/images/bridge.png)

## Bridge and Analytics Services
### image-classifier
A service that enables image classification via integration with TensorFlow. *Codebase:* `[ROOT]/thingzone-image-classifier`

### Http-upload
This service is used to accept incoming HTTP requests and to enqueue then for the rest of the system to process.
*Codebase:* `[ROOT]/thingzone-streaming-bridge/thingzone-bridge-http-upload`

### Payload-dispatcher
A streaming  service for processing data coming from Apache Kafka Connectors or the `http-upload` service.
The `payload-dispatcher` maps incoming data payloads into suitable timeseries data points based on the platform internal data model.
See supported [timeseries data structures](./data_structures.html) for further information. *Codebase:* `[ROOT]/thingzone-streaming-bridge/thingzone-bridge-payload-dispatcher`

### Storage-worker
Stores into the appropiate databases all of the processed timeseries data points. *Codebase:* `[ROOT]/thingzone-streaming-bridge/thingzone-bridge-storage-worker`

### Transform-worker
The transform worker streaming service detects if incoming payload messages need to be processed using one of the custom user functions (transforms) stored in the `thingzone-regitry`.
When this is detected, the transform worker generated a transform task to be processed but an available transform engine sandbox.
*Codebase:* `[ROOT]/thingzone-streaming-bridge/thingzone-bridge-transform-worker`

### Sandbox
A transform engine sandbox form processing transform tasks with functions written in javascript. *Codebase:* `[ROOT]/thingzone-javascript-sandbox/`

### Transform-output-dispatcher
A streaming service for processing the output of a transform engine sandbox and converting it to new timeseries data points. *Codebase:* `[ROOT]/thingzone-streaming-bridge/thingzone-bridge-transform-output-dispatcher`

### Push-worker
A service to process request for pushing timeseries data points into external APIs or services using HTTP requests configured by the user. *Codebase:* `[ROOT]/thingzone-streaming-bridge/thingzone-bridge-push-worker`

### Registry-worker
A standalone service to process device and stream metadata from the registry  to update the elasticsearch indexes. *Codebase:* `[ROOT]/thingzone-streaming-bridge/thingzone-bridge-registry-worker`

