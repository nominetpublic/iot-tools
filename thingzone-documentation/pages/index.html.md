---
title: Thingzone IoT Tools Documentation
language_tabs:
toc_footers:
  - '<a href="index.html">Back to main site</a>'
includes: []
search: true
highlight_theme: darkula
---

# IoT Tools Documentation
Welcome to the IoT Tools.

[Nominet](https://nominet.uk) has conducted significant research and development in the last several years in the areas of 
Internet of Things (IoT) and Autonomous Vehicles. A key enabler of its research has been Nominet’s internally 
developed IoT data collection, analysis and insight platform - it’s IoT Tools.
  
These tools are now open sourced for all to use and take forward. 
  
## System Architecture
* [Architecture](./architecture.html)

## Deployment
* [Deployment](./deployment.html)

## API
* [Registry API](./registry.html)
* [Datastore API](./datastore.html)
* [Bridge API](./bridge.html)


## Bridge
* [Transforms](./transforms.html)
* [Upload](./upload.html)
* [Mapper](./mapper.html)

## Data Structures
* [Data Structures](./data_structures.html)

## Classifier
* [Classifier](./classifier.html)

## User Guides
* [Creating devices, data streams, transforms and events with the UI](./setup-project.html)
* [Visualisations](./event_visualisation.html)
* [Fleet management case study](./fleet_management.html)

## Other
* [About](./about.html)
