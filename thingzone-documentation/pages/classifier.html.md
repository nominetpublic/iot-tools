---
title: Classifier  
toc_footers: []
includes: []
search: true
highlight_theme: darkula
headingLevel: 2

---

<h1 id="classifier">Classifier</h1>

This component enables the identifying and classification of objects in an image. It is built on top of the 
[TensorFlow Object Detection API](https://github.com/tensorflow/models/tree/master/research/object_detection).

## Classifier setup 
Follow the following steps to set up the classifier:

1. Create "models" folder, create sub-folders with the name of your models 
2. Create two folders within your model folder named "day" and "night". 
3. Add your trained models for day and night in the relevant folders
4. In both "day" and "night" folders, add "data" folders and put your label map file in it.

An example directory structure for "coco_mobilenet" model. 

```shell
Example directory Structure

+ YOUR_PATH/models/
    + coco_mobilenet/
        + day/
            + data/
                - mscoco_label_map.pbtxt
            + saved_model/
                - saved_model.pb
            ....
```
5. Add your default model path and its name to this config file: "image_classifier_config.ini"
```python
[MODELS]
DEFAULT_MODEL_PATH = models/coco_mobilenet
DEFAULT_MODEL = coco_mobilenet
```

## Classifier API
API for running the classifier

### localise and identify objects

`POST /get_objects`

> Body parameter

```yaml
image: elephant.jpg
datastream: {
                "metadata": {
                    "classifier": {
                        "modelVersion": "coco_mobilenet",
                        "mLuxThreshold": 900.0
                    }
                }
            } 
```


__Parameters__

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|header|header|object|true|none|
|Content-Type|header|multipart/form-data|true|none|
|body|body|object|true|none|
|image|body|file|true|The image for localising and identifying objects|
|dataStream|body|json object|true|the required metadata for the classifier to select the right model|

__dataStream Type__

```json
"metadata": {
                "classifier": {
                    "modelVersion": MODEL_NAME,
                    "mLuxThreshold": NUMBER
                }
            }
```

The modelVersion value should correspond to one of the models in the "models" folder. If no matching model
is found, the default model will be used. The "mLuxThreshold" value enables selecting of day or night models
provided the Image Description parameter of the image has been set. If not, by default, a day model is selected.

__Response__

> Response example
```yaml
[
  {
    "label": "elephant",
    "origin": "img_mobilenet",
    "selection": "bbox[0.21171346306800842, 0.005377918481826782, 0.9482252597808838, 0.5936315059661865]",
    "confidence": 0.9655020833015442
  },
  {
    "label": "elephant",
    "origin": "img_mobilenet",
    "selection": "bbox[0.27081239223480225, 0.5713156461715698, 0.8614740371704102, 0.9974731206893921]",
    "confidence": 0.8749179840087891
  }
]
```

Status Code: 200

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[classifier response]|true|a list of identified objects with the associated confidence level|

__classifier response__

A classifier object is composed of the following parameters: 
```yaml
    {
         "label": OBJECT_LABEL,    
         "origin": MODEL_NAME,
         "selection": bbox[x1, y1, x2, y2],
         "confidence": [0.0, 1.0]
     }
```

## Retrain a classifier model
Please refer to
[here](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/running_pets.md) 
for a guideline to retrain a model using your own dataset.


## Integration 
An image data stream can be updated to run a classifier over input images by setting
the classifier model in its metadata. An example of 'metadata' definition is as follows:

```yaml
{
        "classifier": {
            "modelVersion": "img_mobilenet_v1",
            "mLuxThreshold": 2000.0
        }
} 
```

Payload Dispatcher, containing a set of [mappers](mapper.html), monitors the input
queue and triggers different mappers based on the input content. If the content is
an image, [image mapper](image.html) is triggered. The classifier is then called, 
if the image data stream has the classifier option selected. For further detail on
the image mapper class, check [here](image.html).
