---
title: Mapper
language_tabs:
  - python: Python
  - shell: Shell
  - yaml: Yaml
  
toc_footers: []
includes: []
search: true
highlight_theme: darkula
headingLevel: 2

---

<h1 id="mapper">Mapper</h1>

Mappers decode payloads and publishes the decoded information into 
the "timeseries" queue or stores it to archives for future access.

Mappers are selected based on the content and stream key of a data point 
on the "input payloads". There are two general mappers implemented in this
project, however, new mappers can be added to take care of new data streams
type. 

## Image Mapper
If the content of a ```payload``` is an image, the ```ImageMapper``` 
class will create an image instance: 

```java
String reference = ImagesvrDriver.getAPIClient().upload.fromBytes(payload.getData());

Image imagePoint = new Image();
imagePoint.setId(UUID.randomUUID());
imagePoint.setStreamKey(payload.getKey());
imagePoint.setMetadata(payload.getMetadata());
imagePoint.setTimestamp(payload.getReceivedAt());
imagePoint.setImageRef(reference);

timeseries.add(imagePoint);
```

The stream key of the ```payload``` is used to fetch data stream information 
from ```registry```. 
 
```java
String streamKey = payload.getKey();
            RegistryDataStream stream =
                    (RegistryDataStream) RegistryAPIDriver
                                                 .getSession()
                                                 .internal
                                                 .identify(streamKey, true, false);
```

If the data stream metadata contains a ```classifier``` key, a request, 
including image and data stream metadata, is posted to the 
[classifier](classifier.html) for localising and identifying
objects in the image. The result of the classification is added to the image
instance.

```java
imagePoint.setClassifications(classifications);
```

AT the end, The image instance is published on the ```timeseries```
queue.

```java
timeseries.add(imagePoint);
```