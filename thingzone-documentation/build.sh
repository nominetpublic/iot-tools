#!/usr/bin/env bash

# Prepare the build folders
rm -rf build
rm -rf dist

cp -r node_modules/shins/ build

# cleanup
rm build/source/*.md
rm build/*.html

# convert swagger specs into md files
for swfolder in ./swagger/*; do
    [ -d "${swfolder}" ] || break
    apiname="${swfolder##*/}"
    ./node_modules/.bin/widdershins swagger/$apiname/index.yaml --resolve -o build/source/$apiname.html.md
done


# Copy static pages
cp pages/* build/source
cp -r images/*  build/source/images/

cd build
npm install
#node myshins.js --inline

for sourcefile in ./source/*.md; do
    [ -f "$sourcefile" ] || break
    filename="${sourcefile##*/}"
    output="${filename%.md}"
    echo $filename
    echo $output
    node shins.js --minify --output $output $sourcefile
done


#npm start

# copy necessary files to dist folder
cd ..
mkdir -p dist/source

cp -r build/pub/ dist/pub
cp -r build/source/images/ dist/source/images
cp -r build/source/fonts/ dist/source/fonts
cp build/*.html dist/







