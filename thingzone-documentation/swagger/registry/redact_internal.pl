#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';

# this script redacts lines from index.yaml that refer to the /internal API

my $swallow = undef;

while (<>) {
	chomp(my $line = $_);

	# calculate indent
	my $indent = length($line =~ s/^(\s*).*/$1/r);

	# swallow anything with larger indent, otherwise stop swallowing
	if (defined $swallow) {
		if ($indent > $swallow) {
			next;	# swallow the line
		} else {
			$swallow = undef;	# stop swallowing
		}
	}

	if ($line=~ /^\s*\- name: Internal/
			|| $line =~ /^\s*\'\/internal\//
			|| $line =~ /^\s*\'\/dump\// ) {
		# swallow this line & following lines with greater indent
		$swallow = $indent;
	}

	print "$line\n" unless defined $swallow;
}
