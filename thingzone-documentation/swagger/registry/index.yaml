openapi: 3.0.0
info:
  title: Registry API
  description: API for managing users and entities
  version: "1.0.0"
servers:
  - url: /registry/api
    description: Server

components:
  securitySchemes:
    ApiKeyAuth:
      type: apiKey
      in: header
      name: X-FollyBridge-SessionKey

tags:
  - name: Session
    description: Logging in and logging out
  - name: Users
    description: User management
  - name: Groups
    description: Group management
  - name: Entity
    description: Operations on individual keys
  - name: Permissions
    description: Managing the permissions for keys
  - name: Devices
    description: Device management
  - name: Datastreams
    description: Datastream management
  - name: Transforms
    description: Management of transform functions and related datastreams
  - name: Subscriptions
    description: Management of pull subscriptions and push subscribers on datastreams
  - name: Resources
    description: Manage entity resources
  - name: Internal
    description: API methods only accessible from privileged IP addresses

paths:
  '/session/login':
     $ref: 'sessions.yaml#/paths/~1session~1login'
  '/session/keepalive':
     $ref: 'sessions.yaml#/paths/~1session~1keepalive'
  '/session/logout':
     $ref: 'sessions.yaml#/paths/~1session~1logout'

  '/user/create':
     $ref: 'user.yaml#/paths/~1user~1create'
  '/user/change_password':
     $ref: 'user.yaml#/paths/~1user~1change_password'
  '/user/get':
     $ref: 'user.yaml#/paths/~1user~1get'
  '/user/list_names':
     $ref: 'user.yaml#/paths/~1user~1list_names'
  '/user/update':
     $ref: 'user.yaml#/paths/~1user~1update'
  '/user/set_privileges':
     $ref: 'user.yaml#/paths/~1user~1set_privileges'
  '/user/request_token':
     $ref: 'user.yaml#/paths/~1user~1request_token'
  '/user/set_password':
     $ref: 'user.yaml#/paths/~1user~1set_password'

  '/groups':
     $ref: 'groups.yaml#/paths/~1groups'
  '/groups/{name}':
     $ref: 'groups.yaml#/paths/~1groups~1{name}'
  '/groups/{name}/users':
     $ref: 'groups.yaml#/paths/~1groups~1{name}~1users'
  '/groups/{name}/users/add':
     $ref: 'groups.yaml#/paths/~1groups~1{name}~1users~1add'
  '/groups/{name}/users/remove':
     $ref: 'groups.yaml#/paths/~1groups~1{name}~1users~1remove'
  '/groups/{name}/admin':
     $ref: 'groups.yaml#/paths/~1groups~1{name}~1admin'
  '/groups/{name}/update':
     $ref: 'groups.yaml#/paths/~1groups~1{name}~1update'

  '/permission/list':
    $ref: 'permissions.yaml#/paths/~1permission~1list'
  '/permission/get':
    $ref: 'permissions.yaml#/paths/~1permission~1get'
  '/permission/set':
    $ref: 'permissions.yaml#/paths/~1permission~1set'
  '/permission/set_keys':
    $ref: 'permissions.yaml#/paths/~1permission~1set_keys'

  '/device/list':
     $ref: 'devices.yaml#/paths/~1device~1list'
  '/device/create':
     $ref: 'devices.yaml#/paths/~1device~1create'
  '/device/get':
     $ref: 'devices.yaml#/paths/~1device~1get'
  '/device/update':
     $ref: 'devices.yaml#/paths/~1device~1update'
  '/device/upsert':
     $ref: 'devices.yaml#/paths/~1device~1upsert'
  '/device/delete':
     $ref: 'devices.yaml#/paths/~1device~1delete'
  '/device/get_streams':
     $ref: 'devices.yaml#/paths/~1device~1get_streams'
  '/device/add_stream':
     $ref: 'devices.yaml#/paths/~1device~1add_stream'
  '/device/remove_stream':
     $ref: 'devices.yaml#/paths/~1device~1remove_stream'

  '/datastream/list':
     $ref: 'datastreams.yaml#/paths/~1datastream~1list'
  '/datastream/create':
     $ref: 'datastreams.yaml#/paths/~1datastream~1create'
  '/datastream/get':
     $ref: 'datastreams.yaml#/paths/~1datastream~1get'
  '/datastream/update':
     $ref: 'datastreams.yaml#/paths/~1datastream~1update'
  '/datastream/upsert':
     $ref: 'datastreams.yaml#/paths/~1datastream~1upsert'
  '/datastream/delete':
     $ref: 'datastreams.yaml#/paths/~1datastream~1delete'

  '/transform/list_functions':
     $ref: 'transforms.yaml#/paths/~1transform~1list_functions'
  '/transform/create_function':
     $ref: 'transforms.yaml#/paths/~1transform~1create_function'
  '/transform/get_function':
     $ref: 'transforms.yaml#/paths/~1transform~1get_function'
  '/transform/modify_function':
     $ref: 'transforms.yaml#/paths/~1transform~1modify_function'
  '/transform/delete_function':
     $ref: 'transforms.yaml#/paths/~1transform~1delete_function'
  '/transform/list':
     $ref: 'transforms.yaml#/paths/~1transform~1list'
  '/transform/create':
     $ref: 'transforms.yaml#/paths/~1transform~1create'
  '/transform/get':
     $ref: 'transforms.yaml#/paths/~1transform~1get'
  '/transform/update_function':
     $ref: 'transforms.yaml#/paths/~1transform~1update_function'
  '/transform/update_parameters':
     $ref: 'transforms.yaml#/paths/~1transform~1update_parameters'
  '/transform/update_runSchedule':
     $ref: 'transforms.yaml#/paths/~1transform~1update_runSchedule'
  '/transform/update_source':
     $ref: 'transforms.yaml#/paths/~1transform~1update_source'
  '/transform/update_outputStream':
     $ref: 'transforms.yaml#/paths/~1transform~1update_outputStream'
  '/transform/upsert':
     $ref: 'transforms.yaml#/paths/~1transform~1upsert'
  '/transform/delete':
     $ref: 'transforms.yaml#/paths/~1transform~1delete'

  '/pull_subscription/list':
     $ref: 'subscriptions.yaml#/paths/~1pull_subscription~1list'
  '/pull_subscription/get':
     $ref: 'subscriptions.yaml#/paths/~1pull_subscription~1get'
  '/pull_subscription/set':
     $ref: 'subscriptions.yaml#/paths/~1pull_subscription~1set'
  '/pull_subscription/delete':
     $ref: 'subscriptions.yaml#/paths/~1pull_subscription~1delete'
  '/push_subscribers/list':
     $ref: 'subscriptions.yaml#/paths/~1push_subscribers~1list'
  '/push_subscribers/get':
     $ref: 'subscriptions.yaml#/paths/~1push_subscribers~1get'
  '/push_subscribers/set':
     $ref: 'subscriptions.yaml#/paths/~1push_subscribers~1set'
  '/push_subscribers/delete':
     $ref: 'subscriptions.yaml#/paths/~1push_subscribers~1delete'

  '/entity/list':
     $ref: 'entity.yaml#/paths/~1entity~1list'
  '/entity/list_admin':
     $ref: 'entity.yaml#/paths/~1entity~1list_admin'
  '/entity/create_key':
     $ref: 'entity.yaml#/paths/~1entity~1create_key'
  '/entity/update':
     $ref: 'entity.yaml#/paths/~1entity~1update'
  '/entity/delete':
     $ref: 'entity.yaml#/paths/~1entity~1delete'
  '/search/list':
     $ref: 'entity.yaml#/paths/~1search~1list'
  '/search/identify':
     $ref: 'entity.yaml#/paths/~1search~1identify'
  '/dns/list_all_records':
     $ref: 'entity.yaml#/paths/~1dns~1list_all_records'
  '/dns/list_records':
     $ref: 'entity.yaml#/paths/~1dns~1list_records'
  '/dns/add_record':
     $ref: 'entity.yaml#/paths/~1dns~1add_record'
  '/dns/remove_record':
     $ref: 'entity.yaml#/paths/~1dns~1remove_record'

  '/resource/list':
    $ref: 'resources.yaml#/paths/~1resource~1list'
  '/resource/json':
    $ref: 'resources.yaml#/paths/~1resource~1json'
  '/resource/list/{key}':
    $ref: 'resources.yaml#/paths/~1resource~1list~1{key}'
  '/resource/{key}/{name}':
    $ref: 'resources.yaml#/paths/~1resource~1{key}~1{name}'
  '/resource/upsert':
    $ref: 'resources.yaml#/paths/~1resource~1upsert'

  '/internal/permission_check':
    $ref: 'internal.yaml#/paths/~1internal~1permission_check'
  '/internal/permission_filter':
    $ref: 'internal.yaml#/paths/~1internal~1permission_filter'
  '/internal/permissions_for_key':
    $ref: 'internal.yaml#/paths/~1internal~1permissions_for_key'
  '/internal/permitted_users':
    $ref: 'internal.yaml#/paths/~1internal~1permitted_users'
  '/internal/user_groups':
    $ref: 'internal.yaml#/paths/~1internal~1user_groups'
  '/internal/get_username':
    $ref: 'internal.yaml#/paths/~1internal~1get_username'
  '/internal/transforms_dependent_on_stream':
    $ref: 'internal.yaml#/paths/~1internal~1transforms_dependent_on_stream'
  '/internal/transforms_with_runSchedule':
    $ref: 'internal.yaml#/paths/~1internal~1transforms_with_runSchedule'
  '/internal/push_subscribers':
    $ref: 'internal.yaml#/paths/~1internal~1push_subscribers'
  '/internal/identify':
    $ref: 'internal.yaml#/paths/~1internal~1identify'
  '/internal/resource/{key}/{name}':
    $ref: 'internal.yaml#/paths/~1internal~1resource~1{key}~1{name}'
  '/dump/all':
    $ref: 'internal.yaml#/paths/~1dump~1all'
  '/dump/datastream':
    $ref: 'internal.yaml#/paths/~1dump~1datastream'
  '/dump/changes/{date}':
    $ref: 'internal.yaml#/paths/~1dump~1changes~1{date}'
