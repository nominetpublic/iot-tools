paths:
  #----------------------------------------------------------
  '/combination/list':
    post:
      summary: combination list
      description: 'Returns a timeseries combined data streams'
      security:
        - ApiKeyAuth: []
      operationId: combinationList
      requestBody:
        description: '

          Query parameters:

          - __primary__ (_object_) : __Required__ An object with a single property representing the primary query for the combination.
             The property name represent the type of list query (metricsListQuery, alertListQuery, eventListQuery, imagesListQuery), the property value is the content of the query. See related documentation.


          - __secondary__ (_array_) : An array of secondary queries. Each item in the array has two properties: One witch name represents the type of list query (metricsListQuery, alertListQuery, eventListQuery, imagesListQuery) and the value is the content of the query.
             A property called "steramName" which value is the name used to label the stream

          '

        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CombinationListQuery'
      responses:
        '200':
          description: Combined timeseries list
          content:
            application/json:
              schema:
                type: object
                properties:
                  respose:
                    $ref: '#/components/schemas/RawTimeseriesResponse'
                  result:
                    $ref: 'commons.yaml#/components/schemas/OkResult'
        '400':
          $ref: 'commons.yaml#/components/schemas/RequestError'
        '401':
          $ref: 'commons.yaml#/components/schemas/AuthError'
      tags:
        - Combination
  #----------------------------------------------------------
  '/combination/geo':
    post:
      summary: 'Combination geo'
      description: 'Returns a geojson object containing a combination of different data streams'
      security:
        - ApiKeyAuth: []
      operationId: combinationGeo
      requestBody:
        description: '

          Query parameters:

          - __primary__ (_object_) : __Required__ An object with a single property representing the primary query for the combination.
                       The property name represent the type of geo query (metricsGeoQuery, alertGeoQuery, eventGeoQuery), the property value is the content of the query. See related documentation.


          - __secondary__ (_array_) : An array of secondary queries. Each item in the array has two properties: One witch name represents the type of list query (metricsListQuery, alertListQuery, eventListQuery, imagesListQuery) and the value is the content of the query.
             A property called "steramName" which value is the name used to label the stream

          '
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CombinationGeoQuery'
      responses:
        '200':
          description: Geojson
          content:
            application/json:
              schema:
                type: object
                properties:
                  respose:
                    $ref: '#/components/schemas/CombinationGeoResponse'
                  result:
                    $ref: 'commons.yaml#/components/schemas/OkResult'
        '400':
          $ref: 'commons.yaml#/components/schemas/RequestError'
        '401':
          $ref: 'commons.yaml#/components/schemas/AuthError'
      tags:
        - Combination
  #----------------------------------------------------------
  '/combination/query/list':
    post:
      summary: 'Combination list query'
      description:  'Generate a timeseries of raw metrics based on a JSON query specified as the body of the request.'
      security:
        - ApiKeyAuth: []
      operationId: combinationPipelineList
      requestBody:
        required: true
        description: '

          Aggregation query parameters:

          - __combinationListQuery__ (_object_): __Required__ [Combination List Query](#/Combination/combinationList)


          - __intermediate__ (_boolean_) : Include intermediate values (list of devices and list of stream)


          - __deviceQuery__ (_object_): [Device Geo Search Query](#/Device/deviceGeoSearch)


          - __streamListQuery__ (_object_): [Stream Search Query](#/Stream/streamSearch)


          - __deviceList__ (_array_): Specify a list of devices to use as the input of the stream Query, when specified the device Query is ignored. Example: [{"name": "device1", "type": "type1", "key": "device1"}]


          - __streamList__ (_array_): Specify a list of streams to use as the input of the metric aggregation Query, when specified the stream Query, device query and device List are ignored. Example: [{"name": "stream1", "type": "type1", "key": "stream1"}]'

        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CombinationPipelineQueryList'
      responses:
        '200':
          description: Combined timeseries list
          content:
            application/json:
              schema:
                type: object
                properties:
                  response:
                    $ref: '#/components/schemas/CombinationListQuery'
                  intermediate:
                    type: array
                    items:
                      type: array
                      items:
                        $ref: 'commons.yaml#/components/schemas/DeviceEntity'
                  result:
                    $ref: 'commons.yaml#/components/schemas/OkResult'
        '400':
          $ref: 'commons.yaml#/components/schemas/RequestError'
        '401':
          $ref: 'commons.yaml#/components/schemas/AuthError'
      tags:
        - Combination
  #----------------------------------------------------------
  '/combination/query/geo':
    post:
      summary: 'Combination geo query'
      description:  'Returns a geojson object containing a combination of different data streams'
      security:
        - ApiKeyAuth: []
      operationId: combinationPipelineGeo
      requestBody:
        required: true
        description: '

          Aggregation query parameters:

          - __combinationLGeoQuery__ (_object_): __Required__ [Combination Geo Query](#/Combination/combinationGeo)


          - __intermediate__ (_boolean_) : Include intermediate values (list of devices and list of stream)


          - __deviceQuery__ (_object_): [Device Geo Search Query](#/Device/deviceGeoSearch)


          - __streamListQuery__ (_object_): [Stream Search Query](#/Stream/streamSearch)


          - __deviceList__ (_array_): Specify a list of devices to use as the input of the stream Query, when specified the device Query is ignored. Example: [{"name": "device1", "type": "type1", "key": "device1"}]


          - __streamList__ (_array_): Specify a list of streams to use as the input of the metric aggregation Query, when specified the stream Query, device query and device List are ignored. Example: [{"name": "stream1", "type": "type1", "key": "stream1"}]'

        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CombinationPipelineQueryGeo'
      responses:
        '200':
          description: Combined timeseries list
          content:
            application/json:
              schema:
                type: object
                properties:
                  response:
                    $ref: '#/components/schemas/CombinationGeoQuery'
                  intermediate:
                    type: array
                    items:
                      type: array
                      items:
                        $ref: 'commons.yaml#/components/schemas/DeviceEntity'
                  result:
                    $ref: 'commons.yaml#/components/schemas/OkResult'
        '400':
          $ref: 'commons.yaml#/components/schemas/RequestError'
        '401':
          $ref: 'commons.yaml#/components/schemas/AuthError'
      tags:
        - Combination
components:
  schemas:
    CombinationPipelineQueryList:
      type: object
      properties:
        intermediate:
          type: boolean
          example: true
        deviceQuery:
          $ref: 'geoSearch.yaml#/components/schemas/DeviceListQuery'
        streamListQuery:
          $ref: 'stream.yaml#/components/schemas/StreamQuery'
        combinationListQuery:
          $ref: 'combination.yaml#/components/schemas/CombinationListQuery'
        deviceList:
          type: array
          items:
            $ref: 'commons.yaml#/components/schemas/DeviceEntity'
        streamList:
          type: array
          items:
            $ref: 'commons.yaml#/components/schemas/DeviceEntity'
    CombinationPipelineQueryGeo:
      type: object
      properties:
        intermediate:
          type: boolean
          example: true
        deviceQuery:
          $ref: 'geoSearch.yaml#/components/schemas/DeviceListQuery'
        streamListQuery:
          $ref: 'stream.yaml#/components/schemas/StreamQuery'
        combinationGeoQuery:
          $ref: 'combination.yaml#/components/schemas/CombinationGeoQuery'
        deviceList:
          type: array
          items:
            $ref: 'commons.yaml#/components/schemas/DeviceEntity'
        streamList:
          type: array
          items:
            $ref: 'commons.yaml#/components/schemas/DeviceEntity'
    CombinationListQuery:
      type: object
      properties:
        primary:
          type: object
          properties:
            metricsListQuery:
              type: object
              example:
                streams: ["device1.stream.metric.double"]
                to: "now"
                size: 20
                from: "2019-06-28T15:08:21.724Z"
        secondary:
          type: array
          items:
            type: object
            properties:
              streamName:
                type: string
                example: "MetricInt"
              metricsListQuery:
                type: object
                example:
                  streams: ["device1.stream.metric.int"]
                  to: "now"
                  size: 20
                  from: "2019-06-28T15:08:21.724Z"
    CombinationGeoQuery:
      type: object
      properties:
        primary:
          type: object
          properties:
            metricsGeoQuery:
              type: object
              example:
                streams: ["device1.stream.metric.latlong"]
                to: "now"
                size: 20
                from: "2019-06-28T15:08:21.724Z"
        secondary:
          type: array
          items:
            type: object
            properties:
              streamName:
                type: string
                example: "MetricInt"
              metricsListQuery:
                type: object
                example:
                  streams: ["device1.stream.metric.int"]
                  to: "now"
                  size: 20
                  from: "2019-06-28T15:08:21.724Z"
    RawTimeseriesResponse:
      type: array
      items:
        type: object
        properties:
          streamKey:
            type: string
            example: 'stream123'
          name:
            type: string
            example: 'streamName'
          type:
            type: string
            example: 'uk.nominet.iot.model.MetricInt'
          metadata:
            type: object
            example:
              unit: 'm/s'
          values:
            type: array
            example:
              - value_lat_long:
                  lat: 51.749879332815155
                  lon: -1.2574946880340576
                streamKey: device1.stream.metric.latlong
                month: 6
                klass: uk.nominet.iot.model.timeseries.MetricLatLong
                timestamp: '2019-06-28T15:21:16.892Z'
                id: a999f407-487b-4c55-b57c-b6854c3d0052
                metadata:
                  test2: 20.34
                  test: abc
                  test1: 123
                quality:
                  - timestamp: '2019-07-08T15:06:57.964Z'
                    indicator: timeliness
                    value: 0.8
                  - timestamp: '2019-07-08T15:06:57.964Z'
                    indicator: correctness
                    value: 0.6
                combinedPoints:
                  MetricInt:
                    value_int: 1
                    streamKey: device1.stream.metric.int
                    month: 6
                    klass: uk.nominet.iot.model.timeseries.MetricInt
                    timestamp: '2019-06-28T15:21:16.892Z'
                    id: ecf086b7-5165-42f1-be14-6d0b9257ba77
                    metadata:
                      test2: 20.34
                      test: abc
                      test1: 123
                    quality:
                      - timestamp: '2019-07-08T15:06:57.919Z'
                        indicator: timeliness
                        value: 0.8
                      - timestamp: '2019-07-08T15:06:57.919Z'
                        indicator: correctness
                        value: 0.6
                  MetricString:
                    value_text: String_1
                    streamKey: device1.stream.metric.string
                    month: 6
                    klass: uk.nominet.iot.model.timeseries.MetricString
                    timestamp: '2019-06-28T15:21:16.892Z'
                    id: 2e6c2197-3816-41a0-9887-04e2c15d005c
                    metadata:
                      test2: 20.34
                      test: abc
                      test1: 123
                    quality:
                      - timestamp: '2019-07-08T15:06:57.942Z'
                        indicator: timeliness
                        value: 0.8
                      - timestamp: '2019-07-08T15:06:57.942Z'
                        indicator: correctness
                        value: 0.6
    CombinationGeoResponse:
      type: array
      items:
        type: object
        properties:
          streamKey:
            type: string
            example: 'stream123'
          name:
            type: string
            example: 'streamName'
          type:
            type: string
            example: 'uk.nominet.iot.model.MetricLatLong'
          metadata:
            type: object
            example:
              unit: latlong
          geojson:
            $ref: '#/components/schemas/GeoJson'
    GeoJson:
      type: object
      properties:
        type:
          type: string
          example: FeatureCollection'
        features:
          type: array
          example:
            type: FeatureCollection
            features:
              - type: Feature
                properties:
                  point:
                    value_lat_long:
                      lat: 50.79087933281515
                      lon: -1.2574946880340576
                    streamKey: device1.stream.metric.latlong
                    month: 7
                    klass: uk.nominet.iot.model.timeseries.MetricLatLong
                    timestamp: '2019-07-08T15:06:16.892Z'
                    id: 2bf8ac71-d68e-4868-a694-bfcb99ef641b
                    metadata:
                      test2: 20.34
                      test: abc
                      test1: 123
                    quality:
                      - timestamp: '2019-07-08T15:07:45.606Z'
                        indicator: timeliness
                        value: 0.8
                      - timestamp: '2019-07-08T15:07:45.606Z'
                        indicator: correctness
                        value: 0.6
                    combinedPoints:
                      MetricInt:
                        value_int: 0
                        streamKey: device1.stream.metric.int
                        month: 6
                        klass: uk.nominet.iot.model.timeseries.MetricInt
                        timestamp: '2019-06-28T20:06:16.892Z'
                        id: 4c51f391-5129-42da-87dc-f7eb6f9f0e66
                        metadata:
                          test2: 20.34
                          test: abc
                          test1: 123
                        quality:
                          - timestamp: '2019-07-08T15:06:59.336Z'
                            indicator: timeliness
                            value: 0.8
                          - timestamp: '2019-07-08T15:06:59.336Z'
                            indicator: correctness
                            value: 0.6
                      MetricString:
                        value_text: String_0
                        streamKey: device1.stream.metric.string
                        month: 6
                        klass: uk.nominet.iot.model.timeseries.MetricString
                        timestamp: '2019-06-28T20:06:16.892Z'
                        id: 147a37a1-d3aa-4a95-a70b-0ae02832f5e5
                        metadata:
                          test2: 20.34
                          test: abc
                          test1: 123
                        quality:
                          - timestamp: '2019-07-08T15:06:59.351Z'
                            indicator: timeliness
                            value: 0.8
                          - timestamp: '2019-07-08T15:06:59.351Z'
                            indicator: correctness
                            value: 0.6
                geometry:
                  type: Point
                  coordinates:
                    - -1.2574946880340576
                    - 50.79087933281515

