{
  "$schema": "http://json-schema.org/draft-07/schema",
  "$id": "http://yourdomain.uk/schemas/Config.json",
  "type": "object",
  "title": "Config",
  "description": "Timeseries data point for config objects",
  "required": [
    "config",
    "streamKey",
    "klass",
    "id"
  ],
  "properties": {
    "config": {
      "$id": "#/properties/config",
      "type": "object",
      "title": "Config object",
      "description": "The object containing the device configuration properties",
      "default": {},
      "examples": [
        {
          "foo": "bar"
        }
      ]
    },
    "origin": {
      "$id": "#/properties/origin",
      "type": "string",
      "title": "Origin",
      "description": "The originator of the configuration DEVICE or SYSTEM",
      "default": "",
      "examples": [
        "DEVICE",
        "SYSTEM"
      ]
    },
    "revision": {
      "$id": "#/properties/revision",
      "type": "string",
      "title": "Revision",
      "description": "Revision number",
      "default": "",
      "examples": [
        "0.1"
      ]
    },
    "derivedFromConfig": {
      "$id": "#/properties/derivedFromConfig",
      "type": "string",
      "title": "Derived from config",
      "description": "The id of a configuration data point where this configuration was derived from",
      "default": "",
      "examples": [
        "e7484e3e-6cc3-4ae2-98f8-f2d7e304d46e"
      ]
    },
    "streamKey": {
      "$id": "#/properties/streamKey",
      "type": "string",
      "title": "Stream key",
      "description": "The key of the stream associated with the data point",
      "default": "",
      "examples": [
        "streamkey123"
      ]
    },
    "month": {
      "$id": "#/properties/month",
      "type": "integer",
      "title": "Month",
      "description": "Integer value representiing the month in which the point was created. Used for partitioning.",
      "default": 0,
      "examples": [
        3
      ]
    },
    "klass": {
      "$id": "#/properties/klass",
      "type": "string",
      "title": "Timeseries point class",
      "description": "The class representing the data point. Used for serialisation/deserialisation",
      "default": "",
      "examples": [
        "uk.nominet.iot.model.timeseries.Config"
      ]
    },
    "timestamp": {
      "$id": "#/properties/timestamp",
      "type": "string",
      "title": "Timestamp",
      "description": "The timestamp of the data point represented in ISO 8601 UTC",
      "default": "",
      "examples": [
        "2020-03-18T09:12:59.079Z"
      ]
    },
    "id": {
      "$id": "#/properties/id",
      "type": "string",
      "title": "Id",
      "description": "The unique id of the datapoint using UUID format xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx",
      "default": "",
      "examples": [
        "c1383451-1b0d-45a3-a1bd-18b832b616ce"
      ]
    },
    "metadata": {
      "$id": "#/properties/metadata",
      "type": "object",
      "title": "Metadata",
      "description": "Data point metadata",
      "default": {},
      "examples": [
        {
          "foo": "bar"
        }
      ]
    },
    "quality": {
      "$id": "#/properties/quality",
      "type": "array",
      "title": "Quality",
      "description": "Quality indicators describing the quality of the data point",
      "default": [],
      "items": {
        "$id": "#/properties/quality/items",
        "type": "object",
        "title": "Quality Indicator",
        "description": "Quality Indicator",
        "default": {},
        "examples": [
          {
            "value": 0.8,
            "timestamp": "2020-03-18T09:12:59.075Z",
            "indicator": "timeliness"
          },
          {
            "timestamp": "2020-03-18T09:12:59.075Z",
            "indicator": "validity",
            "value": 0.9
          }
        ],
        "required": [
          "timestamp",
          "indicator",
          "value"
        ],
        "properties": {
          "timestamp": {
            "$id": "#/properties/quality/items/properties/timestamp",
            "type": "string",
            "title": "Timestamp",
            "description": "The timestamp of the data quality indicator represented in ISO 8601 UTC",
            "default": "",
            "examples": [
              "2020-03-18T09:12:59.075Z"
            ]
          },
          "indicator": {
            "$id": "#/properties/quality/items/properties/indicator",
            "type": "string",
            "title": "Indicator",
            "description": "The name of the indicator",
            "default": "",
            "examples": [
              "timeliness"
            ]
          },
          "value": {
            "$id": "#/properties/quality/items/properties/value",
            "type": "number",
            "title": "Quality value",
            "description": "Value associated to the quality indicator from 0 to 1",
            "default": 0,
            "examples": [
              0.8
            ]
          }
        }
      }
    },
    "tags": {
      "$id": "#/properties/tags",
      "type": "array",
      "title": "Tags",
      "description": "Tags",
      "default": [],
      "items": {
        "$id": "#/properties/tags/items",
        "type": "string",
        "title": "Tag",
        "description": "Tag",
        "default": "",
        "examples": [
          "tag1",
          "tag2"
        ]
      }
    },
    "classifications": {
      "$id": "#/properties/classifications",
      "type": "array",
      "title": "Classification",
      "description": "A list of data classifications",
      "default": [],
      "items": {
        "$id": "#/properties/classifications/items",
        "type": "object",
        "title": "Classification",
        "description": "Classification Object",
        "default": {},
        "examples": [
          {
            "timestamp": "2020-03-18T09:12:59.074Z",
            "selection": "selection",
            "origin": "user1",
            "confidence": 0.9,
            "label": "class1",
            "curated": false
          },
          {
            "curated": false,
            "timestamp": "2020-03-18T09:12:59.074Z",
            "selection": "selection",
            "origin": "user1",
            "confidence": 0.8,
            "label": "class2"
          }
        ],
        "required": [
          "label",
          "origin",
          "timestamp",
          "confidence"
        ],
        "properties": {
          "label": {
            "$id": "#/properties/classifications/items/properties/label",
            "type": "string",
            "title": "Label",
            "description": "Label describing the classification",
            "default": "",
            "examples": [
              "class1"
            ]
          },
          "origin": {
            "$id": "#/properties/classifications/items/properties/origin",
            "type": "string",
            "title": "Origin",
            "description": "Reference about the origin of the classification",
            "default": "",
            "examples": [
              "user1",
              "model1234"
            ]
          },
          "timestamp": {
            "$id": "#/properties/classifications/items/properties/timestamp",
            "type": "string",
            "title": "Timestamp",
            "description": "The timestamp of the classification represented in ISO 8601 UTC",
            "default": "",
            "examples": [
              "2020-03-18T09:12:59.074Z"
            ]
          },
          "confidence": {
            "$id": "#/properties/classifications/items/properties/confidence",
            "type": "number",
            "title": "Confidence",
            "description": "The confidence value associated with the classification",
            "default": 0,
            "examples": [
              0.9
            ]
          },
          "selection": {
            "$id": "#/properties/classifications/items/properties/selection",
            "type": "string",
            "title": "Selection",
            "description": "A string describing a subset of the data point associated to this classification",
            "default": "",
            "examples": [
              "selection"
            ]
          },
          "curated": {
            "$id": "#/properties/classifications/items/properties/curated",
            "type": "boolean",
            "title": "Curated",
            "description": "Specify if the classification has been curated by a person",
            "default": false,
            "examples": [
              false
            ]
          }
        }
      }
    }
  }
}