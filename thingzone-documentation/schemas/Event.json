{
  "$schema": "http://json-schema.org/draft-07/schema",
  "$id": "http://yourdomain.uk/schemas/Event.json",
  "type": "object",
  "title": "Event",
  "description": "Timeseries data point for and event object",
  "required": [
    "streamKey",
    "klass",
    "id"
  ],
  "properties": {
    "name": {
      "$id": "#/properties/name",
      "type": "string",
      "title": "Name",
      "description": "Event name",
      "default": "",
      "examples": [
        "event123"
      ]
    },
    "description": {
      "$id": "#/properties/description",
      "type": "string",
      "title": "Description",
      "description": "Event description",
      "default": "",
      "examples": [
        "event description"
      ]
    },
    "type": {
      "$id": "#/properties/type",
      "type": "string",
      "title": "Type",
      "description": "Event type defined by user",
      "default": "",
      "examples": [
        "event type"
      ]
    },
    "eventType": {
      "$id": "#/properties/eventType",
      "type": "string",
      "title": "System event type",
      "description": "The system event type: GENERIC, ALERT, OBSERVATION",
      "default": "",
      "examples": [
        "GENERIC",
        "ALERT",
        "OBSERVATION"
      ]
    },
    "category": {
      "$id": "#/properties/category",
      "type": "string",
      "title": "Category",
      "description": "Event category",
      "default": "",
      "examples": [
        "category1"
      ]
    },
    "startTimestamp": {
      "$id": "#/properties/startTimestamp",
      "type": "string",
      "title": "Start timestamp",
      "description": "The timestamp of the start of the event represented in ISO 8601 UTC",
      "default": "",
      "examples": [
        "2020-03-18T09:57:10.506Z"
      ]
    },
    "endTimestamp": {
      "$id": "#/properties/endTimestamp",
      "type": "string",
      "title": "End timestamp",
      "description": "The timestamp of the end of the event represented in ISO 8601 UTC",
      "default": "",
      "examples": [
        "2020-03-18T09:57:10.506Z"
      ]
    },
    "location": {
      "$id": "#/properties/location",
      "type": "object",
      "title": "Location",
      "description": "The location of the event represented as a geojson geometry",
      "default": {},
      "examples": [
        {
          "type": "Point",
          "coordinates": [
            0.0,
            0.0,
            0.0
          ]
        }
      ]
    },
    "receivers": {
      "$id": "#/properties/receivers",
      "type": "array",
      "title": "Receivers",
      "description": "The list of receivers for an alert",
      "default": [],
      "items": {
        "$id": "#/properties/receivers/items",
        "type": "string",
        "title": "Receiver",
        "description": "Receiver name",
        "default": "",
        "examples": [
          "user1",
          "user2"
        ]
      }
    },
    "configuration": {
      "$id": "#/properties/configuration",
      "type": "object",
      "title": "Configuration",
      "description": "Configuration properties for the event processor",
      "default": {},
      "examples": [
        {
          "foo": "bar"
        }
      ]
    },
    "feedback": {
      "$id": "#/properties/feedback",
      "type": "array",
      "title": "Feedback",
      "description": "User feedback for an event",
      "default": [],
      "items": {
        "$id": "#/properties/feedback/items",
        "type": "object",
        "title": "User feedback",
        "description": "User feedback object",
        "default": {},
        "examples": [
          {
            "type": "feedback type",
            "category": "category1",
            "id": "da8cef59-5b63-4a77-9e0d-45cd3fe41504",
            "description": "description",
            "user": "user1",
            "timestamp": "2020-03-18T09:57:10.474Z"
          }
        ],
        "required": [
          "id",
          "timestamp",
          "user",
          "type",
          "category",
          "description"
        ],
        "properties": {
          "id": {
            "$id": "#/properties/feedback/items/properties/id",
            "type": "string",
            "title": "ID",
            "description": "Feedback unique id",
            "default": "",
            "examples": [
              "da8cef59-5b63-4a77-9e0d-45cd3fe41504"
            ]
          },
          "timestamp": {
            "$id": "#/properties/feedback/items/properties/timestamp",
            "type": "string",
            "title": "Timestamp",
            "description": "Feedback timestamp",
            "default": "",
            "examples": [
              "2020-03-18T09:57:10.474Z"
            ]
          },
          "user": {
            "$id": "#/properties/feedback/items/properties/user",
            "type": "string",
            "title": "User",
            "description": "The username of the user giving the feedback",
            "default": "",
            "examples": [
              "user1"
            ]
          },
          "type": {
            "$id": "#/properties/feedback/items/properties/type",
            "type": "string",
            "title": "Type",
            "description": "Feedback type",
            "default": "",
            "examples": [
              "feedback type"
            ]
          },
          "category": {
            "$id": "#/properties/feedback/items/properties/category",
            "type": "string",
            "title": "Category",
            "description": "Feedback category",
            "default": "",
            "examples": [
              "category1"
            ]
          },
          "description": {
            "$id": "#/properties/feedback/items/properties/description",
            "type": "string",
            "title": "Description",
            "description": "Feedback description",
            "default": "",
            "examples": [
              "description"
            ]
          }
        }
      }
    },
    "severity": {
      "$id": "#/properties/severity",
      "type": "number",
      "title": "Severty",
      "description": "Event severity from 0 to 1",
      "default": 0,
      "examples": [
        0.5
      ]
    },
    "status": {
      "$id": "#/properties/status",
      "type": "string",
      "title": "Status",
      "description": "Event status",
      "default": "",
      "examples": [
        "OPEN"
      ]
    },
    "value": {
      "$id": "#/properties/value",
      "type": "number",
      "title": "Value",
      "description": "A numerical value associated with the event",
      "default": 0,
      "examples": [
        0.5
      ]
    },
    "relatedTo": {
      "$id": "#/properties/relatedTo",
      "type": "array",
      "title": "Related to",
      "description": "Other entities related to this event",
      "default": [],
      "items": {
        "$id": "#/properties/relatedTo/items",
        "type": "object",
        "title": "RelatedTo",
        "description": "Related to reference",
        "default": {},
        "examples": [
          {
            "type": "associated image",
            "klass": "uk.nominet.iot.model.timeseries.Image",
            "description": "relation description",
            "key": "65495915-0c74-4dd7-8f10-49c3e98c54d2"
          }
        ],
        "required": [
          "type",
          "klass",
          "key"
        ],
        "properties": {
          "type": {
            "$id": "#/properties/relatedTo/items/properties/type",
            "type": "string",
            "title": "Type",
            "description": "The type of relation",
            "default": "",
            "examples": [
              "associated image"
            ]
          },
          "description": {
            "$id": "#/properties/relatedTo/items/properties/description",
            "type": "string",
            "title": "Dscription",
            "description": "Relation description",
            "default": "",
            "examples": [
              "relation description"
            ]
          },
          "klass": {
            "$id": "#/properties/relatedTo/items/properties/klass",
            "type": "string",
            "title": "Klass",
            "description": "The class of the object the event is related to",
            "default": "",
            "examples": [
              "uk.nominet.iot.model.timeseries.Image"
            ]
          },
          "key": {
            "$id": "#/properties/relatedTo/items/properties/key",
            "type": "string",
            "title": "Key",
            "description": "The unique key of the object related to the event",
            "default": "",
            "examples": [
              "65495915-0c74-4dd7-8f10-49c3e98c54d2"
            ]
          }
        }
      }
    },
    "derivedFrom": {
      "$id": "#/properties/derivedFrom",
      "type": "array",
      "title": "Derivedfrom",
      "description": "Other entities in the system where the event is derived from",
      "default": [],
      "items": {
        "$id": "#/properties/derivedFrom/items",
        "type": "object",
        "title": "DerivedFrom",
        "description": "Derived from reference",
        "default": {},
        "examples": [
          {
            "type": "origin alert",
            "streamKey": "streamkey123",
            "id": "d8493a3e-436a-42fd-9723-788aa1225b64",
            "klass": "uk.nominet.iot.model.timeseries.Event",
            "description": "relation description",
            "weight": 0.4
          }
        ],
        "required": [
          "type",
          "klass",
          "id"
        ],
        "properties": {
          "type": {
            "$id": "#/properties/derivedFrom/items/properties/type",
            "type": "string",
            "title": "Type",
            "description": "The type of the relationship",
            "default": "",
            "examples": [
              "origin alert"
            ]
          },
          "description": {
            "$id": "#/properties/derivedFrom/items/properties/description",
            "type": "string",
            "title": "Description",
            "description": "The description of the relationship",
            "default": "",
            "examples": [
              "relation description"
            ]
          },
          "weight": {
            "$id": "#/properties/derivedFrom/items/properties/weight",
            "type": "number",
            "title": "Weight",
            "description": "A numerical weight associated with the relationship",
            "default": 0,
            "examples": [
              0.4
            ]
          },
          "klass": {
            "$id": "#/properties/derivedFrom/items/properties/klass",
            "type": "string",
            "title": "Klass",
            "description": "The class of the object the event is derived from",
            "default": "",
            "examples": [
              "uk.nominet.iot.model.timeseries.Event"
            ]
          },
          "streamKey": {
            "$id": "#/properties/derivedFrom/items/properties/streamKey",
            "type": "string",
            "title": "Stream key",
            "description": "The datastreamKey of the derived from entity",
            "default": "",
            "examples": [
              "streamkey123"
            ]
          },
          "id": {
            "$id": "#/properties/derivedFrom/items/properties/id",
            "type": "string",
            "title": "Id",
            "description": "The unique id of the derived from entity",
            "default": "",
            "examples": [
              "d8493a3e-436a-42fd-9723-788aa1225b64"
            ]
          }
        }
      }
    },
    "streamKey": {
      "$id": "#/properties/streamKey",
      "type": "string",
      "title": "Stream key",
      "description": "The key of the device data stream associated with the data point",
      "default": "",
      "examples": [
        "streamkey123"
      ]
    },
    "month": {
      "$id": "#/properties/month",
      "type": "integer",
      "title": "Month",
      "description": "Integer value representing the month in which the point was created. Used for partitioning.",
      "default": 0,
      "examples": [
        3
      ]
    },
    "klass": {
      "$id": "#/properties/klass",
      "type": "string",
      "title": "Timeseries point class",
      "description": "The class representing the data point. Used for serialisation/deserialisation",
      "default": "",
      "examples": [
        "uk.nominet.iot.model.timeseries.Event"
      ]
    },
    "timestamp": {
      "$id": "#/properties/timestamp",
      "type": "string",
      "title": "Timestamp",
      "description": "The timestamp of the data point represented in ISO 8601 UTC",
      "default": "",
      "examples": [
        "2020-03-18T09:12:59.079Z"
      ]
    },
    "id": {
      "$id": "#/properties/id",
      "type": "string",
      "title": "Id",
      "description": "The unique id of the data point using UUID format xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx",
      "default": "",
      "examples": [
        "c1383451-1b0d-45a3-a1bd-18b832b616ce"
      ]
    },
    "metadata": {
      "$id": "#/properties/metadata",
      "type": "object",
      "title": "Metadata",
      "description": "Data point metadata",
      "default": {},
      "examples": [
        {
          "foo": "bar"
        }
      ]
    },
    "quality": {
      "$id": "#/properties/quality",
      "type": "array",
      "title": "Quality",
      "description": "Quality indicators describing the quality of the data point",
      "default": [],
      "items": {
        "$id": "#/properties/quality/items",
        "type": "object",
        "title": "Quality Indicator",
        "description": "Quality Indicator",
        "default": {},
        "examples": [
          {
            "value": 0.8,
            "timestamp": "2020-03-18T09:12:59.075Z",
            "indicator": "timeliness"
          },
          {
            "timestamp": "2020-03-18T09:12:59.075Z",
            "indicator": "validity",
            "value": 0.9
          }
        ],
        "required": [
          "timestamp",
          "indicator",
          "value"
        ],
        "properties": {
          "timestamp": {
            "$id": "#/properties/quality/items/properties/timestamp",
            "type": "string",
            "title": "Timestamp",
            "description": "The timestamp of the data quality indicator represented in ISO 8601 UTC",
            "default": "",
            "examples": [
              "2020-03-18T09:12:59.075Z"
            ]
          },
          "indicator": {
            "$id": "#/properties/quality/items/properties/indicator",
            "type": "string",
            "title": "Indicator",
            "description": "The name of the indicator",
            "default": "",
            "examples": [
              "timeliness"
            ]
          },
          "value": {
            "$id": "#/properties/quality/items/properties/value",
            "type": "number",
            "title": "Quality value",
            "description": "Value associated to the quality indicator from 0 to 1",
            "default": 0,
            "examples": [
              0.8
            ]
          }
        }
      }
    },
    "tags": {
      "$id": "#/properties/tags",
      "type": "array",
      "title": "Tags",
      "description": "Tags",
      "default": [],
      "items": {
        "$id": "#/properties/tags/items",
        "type": "string",
        "title": "Tag",
        "description": "Tag",
        "default": "",
        "examples": [
          "tag1",
          "tag2"
        ]
      }
    },
    "classifications": {
      "$id": "#/properties/classifications",
      "type": "array",
      "title": "Classification",
      "description": "A list of data classifications",
      "default": [],
      "items": {
        "$id": "#/properties/classifications/items",
        "type": "object",
        "title": "Classification",
        "description": "Classification Object",
        "default": {},
        "examples": [
          {
            "timestamp": "2020-03-18T09:12:59.074Z",
            "selection": "selection",
            "origin": "user1",
            "confidence": 0.9,
            "label": "class1",
            "curated": false
          },
          {
            "curated": false,
            "timestamp": "2020-03-18T09:12:59.074Z",
            "selection": "selection",
            "origin": "user1",
            "confidence": 0.8,
            "label": "class2"
          }
        ],
        "required": [
          "label",
          "origin",
          "timestamp",
          "confidence"
        ],
        "properties": {
          "label": {
            "$id": "#/properties/classifications/items/properties/label",
            "type": "string",
            "title": "Label",
            "description": "Label describing the classification",
            "default": "",
            "examples": [
              "class1"
            ]
          },
          "origin": {
            "$id": "#/properties/classifications/items/properties/origin",
            "type": "string",
            "title": "Origin",
            "description": "Reference about the origin of the classification",
            "default": "",
            "examples": [
              "user1",
              "model1234"
            ]
          },
          "timestamp": {
            "$id": "#/properties/classifications/items/properties/timestamp",
            "type": "string",
            "title": "Timestamp",
            "description": "The timestamp of the classification represented in ISO 8601 UTC",
            "default": "",
            "examples": [
              "2020-03-18T09:12:59.074Z"
            ]
          },
          "confidence": {
            "$id": "#/properties/classifications/items/properties/confidence",
            "type": "number",
            "title": "Confidence",
            "description": "The confidence value associated with the classification",
            "default": 0,
            "examples": [
              0.9
            ]
          },
          "selection": {
            "$id": "#/properties/classifications/items/properties/selection",
            "type": "string",
            "title": "Selection",
            "description": "A string describing a subset of the data point associated to this classification",
            "default": "",
            "examples": [
              "selection"
            ]
          },
          "curated": {
            "$id": "#/properties/classifications/items/properties/curated",
            "type": "boolean",
            "title": "Curated",
            "description": "Specify if the classification has been curated by a person",
            "default": false,
            "examples": [
              false
            ]
          }
        }
      }
    }
  }
}