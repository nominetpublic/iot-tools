# IoT Tools
Welcome to the IoT Tools.

Nominet has conducted significant research and development in the last several years in the areas of 
Internet of Things (IoT) and Autonomous Vehicles. A key enabler of its research has been Nominet’s internally 
developed IoT data collection, analysis and insight platform - it’s IoT Tools.
  
These tools are now open sourced for all to use and take forward. 
 
![screenshot](https://bitbucket.org/nominetpublic/iot-tools/raw/master/images/IoTTools2Screenshot.jpg)

Sample screenshot from User Interface


## License
This software is released under Apache License 2.0

https://www.apache.org/licenses/LICENSE-2.0


## Requirements

- Java OpenJDK 1.8
- Maven 3.6.x
- Docker
- Docker compose
- Perl5 with additional modules: `libdatetime-timezone-perl` `libjson-perl` `libwww-perl`
- A token from a mapbox subscription: https://www.mapbox.com/ 

## Quickstart

#### Step 1: Package projects
`$> ./tz package`

Note: If the maven build fails you might need to allow more memory to the maven build process:
`export MAVEN_OPTS=-Xss4m`

#### Step 2: Initialise docker environment with data
`$> ./tz docker-init helloworld`

The helloworld environment contains some sample users and data. 
This sample environment include two users: 

- username: `tom` password: `tom`
- username: `bob` password: `bob`


#### Step 3: Start services
`$> ./tz docker-up`
NOTE: you might need to repeat this command if not all the services are running. Check with `docker-compose ps`


#### Step 4: Access tools
A number of web-based tools should be now accessible using a web browser:

- `http://localhost:8080/webapp/` the main web frontend the iot tools
- `http://localhost:8080/queues/` a page for monitoring message queues
- `http://localhost:8080/logs/` a tool for monitoring system logs
- `http://localhost:8080/kibana/` a tool developed by  Elasticsearch B.V. to access data stored in the Elasticsearch database. Documentation available at: https://www.elastic.co/guide/en/kibana/7.6/index.html 


## Package single profile ##
`$> ./tz package [profile]`

Available profiles:

- `all`  build all the services
- `backend` build only the backend services
- `registry` build the registry api
- `bridge` build the bridge data services
- `webapp` build the webapp
- `datastore` build the datastore
- `tools` build the database management tools
 
e.g.
`$> ./tz package datastore`

## Documentation

The source code for the documentation is available under thingzone-documentation.
To build the documentation use the following commands:

- `$> cd thingzone-documentation` 
- `$> npn install` install node dependencies
- `$> ./build.sh` build the documentation
- The documentation static pages should now be available under the `dist` folder
- `$> ./serve.sh` runs a simple webserver to serve the documentation pages locally.
- The documentation is now available at the following web address: `http://localhost:6767`



## Contributors
A number of people have worked on the development of this codebase over the last several 
years but contributors to the initial release of this open source version include:

Edoardo Pignotti

Julia Altenbuchner

Maryam Kamali

James Wilkinson

Bryan Marshall

